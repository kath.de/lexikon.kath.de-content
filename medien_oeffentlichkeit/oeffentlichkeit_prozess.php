<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>&Ouml;ffentlichkeit als Proze&szlig;</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="kaltefleiter.css" title="fonts">
</head>
<body bgcolor="#ffffff" leftmargin="6" topmargin="6" marginwidth="6" marginheight="6">
<table width="100%" border="0" cellspacing="0" cellpadding="6">
  <tr>
    <td width="100" align="left" valign="top"><table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b>Medien und &Ouml;ffentlichkeit</b></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td>
            <?php include("logo.html"); ?>
          </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
    <td rowspan="2" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2">
            <h1> <font face="Arial, Helvetica, sans-serif">&Ouml;ffentlichkeit
            als Proze&szlig;</font></h1>
          </td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="L12"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td valign="top" class="L12"><p class=MsoNormal><strong><font face="Arial, Helvetica, sans-serif">Der &ouml;ffentliche
                        Raum ist st&auml;ndig in Bewegung</font></strong> </P>
                  <p><font face="Arial, Helvetica, sans-serif">Der Kommunikationsraum &quot;&Ouml;ffentlichkeit&quot; ist
                      nicht statisch, sondern wird durch Meinungsbildungsprozesse
                      bestimmt. Dabei kann nicht von einer einheitlichen<a href="oeffentliche_meinung.php"> &ouml;ffentlichen
                      Meinung</a> ausgegangen werden. Denn nur wenige Themen,
                      Probleml&ouml;sungsvorschl&auml;ge, Projekte sind nicht
                      kontrovers. Das gilt bereits f&uuml;r eine Familie und
                      andere Gruppen, deren Leben sich nicht auf der B&uuml;hne
                      der &Ouml;ffentlichkeit abspielt. Am Stammtisch treffen
                      in der Regel unterschiedliche Meinungspositionen aufeinander,
                      noch mehr im Bereich einer Kommune oder der ganzen Gesellschaft.
                      Es sind nur wenige F&auml;lle, zu denen keine unterschiedlichen
                      Meinungspositionen ge&auml;u&szlig;ert werden, so bei Verbrechen
                      oder beim Auftreten eines gemeinsamen Feindes. <br>
  Es wird zwar h&auml;ufig von einer &ouml;ffentlichen Meinung gesprochen, die
  es aber selten gibt. Zu einem Problem, zu einer anstehenden Entscheidung gibt
  es fast immer mehrere Meinungspositionen, die auch in der &Ouml;ffentlichkeit
  ge&auml;u&szlig;ert werden. Der einzelne kann sich die unterschiedlichen Positionen
  anh&ouml;ren und sich dann der Position anschlie&szlig;en, die ihn am meisten &uuml;berzeugt
  bzw. seinen eigenen Interessen entgegenkommt. Der Proze&szlig; &#8222;&Ouml;ffentlichkeit&#8220; erm&ouml;glicht
  demjenigen, der an der &ouml;ffentlichen Auseinandersetzung durch Redebeitr&auml;ge
  wie auch durch Zuh&ouml;ren partizipiert, sich eine Meinung zu bilden und zu
  gegebener Zeit seine Optionen in Form einer Wahlentscheidung, durch Eintritt
  oder Austritt aus einem Verband, durch eine Geldspende u.a. Ausdruck zu verleihen.
  Da Fragen entschieden, Probleme gel&ouml;st werden m&uuml;ssen, kommen einige,
  jedoch nicht alle Meinungsbildungsprozesse zu einem Abschlu&szlig;. Wird eine
  Entscheidung getroffen, verliert die Diskussion an Intensit&auml;t und handelt
  nicht mehr &uuml;ber die Entscheidung selbst, sondern &uuml;ber die Folgen
  der Entscheidung. Viele Probleme werden nicht gel&ouml;st, viele notwendigen
  Entscheidungen werden nicht getroffen. Auch diese Themen r&uuml;cken aus dem
  Scheinwerferlicht der &ouml;ffentlichen Auseinandersetzung. Wenn es nicht eine
  Gruppe gibt, die sich f&uuml;r das Problem einsetzt und die Frage wachh&auml;lt,
  kann das ungel&ouml;ste Problem ganz aus den &ouml;ffentlichen Foren verschwinden.
  H&auml;tte die &Ouml;kologiebewegung nicht das Durchhalteverm&ouml;gen gehabt,
  w&auml;re der Umweltschutz aus dem &ouml;ffentlichen Disput verschwunden, weil
  das Thema sowieso vielen l&auml;stig war.</font></p>
                  <p><font face="Arial, Helvetica, sans-serif">Eckhard Bieger<br>
                  </font></p>
                <p class=MsoNormal><font face="Arial, Helvetica, sans-serif">&copy; <a href="http://www.kath.de/"><font size="2">www.kath.de</font></a></font></P></td>
                <td valign="top"><script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
google_ad_width = 160;
google_ad_height = 600;
google_ad_format = "160x600_as";
google_ad_type = "text_image";
//2007-09-05: Bildung und Erziehung
google_ad_channel = "3955521420";
google_ui_features = "rc:6";
//-->
            </script>
                    <script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
              </script>
                </td>
              </tr>
            </table>
            <p class=MsoNormal>&nbsp;</P>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
         <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td align="left" valign="top"><table width="216" border="0" cellpadding="0" cellspacing="0">
       <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10">
            <?php include("az.html"); ?>
          </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</body>
</html>
