<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Fernsehen im Internet - www.kath.de</title>
<meta name="title" content="Medien&Öffentlichkeit">
<meta name="author" content="kath.de">
<meta name="publisher" content="kath.de">
<meta name="copyright" content="kath.de">
<meta name="description" content="Stichworte über Medien und Öffentlichkeitsarbeit">
<meta name="abstract" content="Stichworte über Medien und Öffentlichkeitsarbeit">
<meta http-equiv="content-language" content="de">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="date" content="2006-00-02">
<meta name="robots" content="index,follow">
<meta name="revisit-after" content="30 days">
<meta name="revisit" content="after 30 days">
<meta name="DC.Title" content="Medien&Öffentlichkeit">
<meta name="DC.Creator" content="kath.de">
<meta name="DC.Contributor" content="N.N.">
<meta name="DC.Rights" content="kath.de">
<meta name="DC.Publisher" content="kath.de">
<meta name="DC.Date" content="2006-00-02">
<meta name="DC.Description" content="Stichworte über Medien und Öffentlichkeitsarbeit">
<meta name="DC.Language" content="de">
<meta name="DC.Type" content="Text">
<meta name="DC.Format" content="text/html">
<meta name="DC.Identifier" content="http://www.kath.de/kurs/medien_oeffentlichkeit/">
<meta name="keywords" lang="de" content="Theologiestudium, Medien, Kommuniaktionsberuf, Drehbücher, Lokalredaktion, Journalismus">

<link rel="stylesheet" type="text/css" href="kaltefleiter.css" title="fonts">
</head>
<body bgcolor="#ffffff" leftmargin="6" topmargin="6" marginwidth="6" marginheight="6">
<table width="100%" border="0" cellspacing="0" cellpadding="6">
  <tr>
    <td width="100" align="left" valign="top"><table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b>Medien und &Ouml;ffentlichkeit</b></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td>
            <?php include("logo.html"); ?>
          </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
    <td rowspan="2" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2">
            <h1> <font face="Arial, Helvetica, sans-serif">Medien&amp;&Ouml;ffentlichkeit</font></h1>
          </td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="L12"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td valign="top" class="L12"><p class="text"><span class="text"><strong><font face="Arial, Helvetica, sans-serif">Fernsehen
                        im Internet</font></strong></span></p>
                  <p class="text"><font face="Arial, Helvetica, sans-serif">Das
                      Internet, bisher ein Text- und Bildmedium, nutzt wie das
                      Fernsehen den Bildschirm. Irgendwann wird der
                    Surfer nicht mehr das Fernsehger&auml;t einschalten, um sich
                    eine Sendung anzusehen &#8211; n&auml;mlich wenn er das gleiche
                    Programm aus dem weltweiten, auf die verschiedenen Server
                    verteilten Archiv von Film- und Fernsehprogrammen abrufen
                    kann. Zwar wird er Filme und aufw&auml;ndige Shows am gro&szlig;en
                    Bildschirm im Wohnzimmer verfolgen, Nachrichten, Wetterbericht
                    auf dem Bildschirm am Arbeitsplatz, aber er wird sich nicht
                    mehr am bisher das Fernsehen steuernde Sendeschema orientieren.
                    Die schnelle Daten&uuml;bertragung, erm&ouml;glich durch
                    die DSL-Technik, macht Fernsehprogramme &uuml;ber das Telefonkabel
                    zug&auml;nglich. Es werden damit die Kommunikationsmuster
                    auch f&uuml;r das Fernsehen wirksam, die das Internet allgemein
                    durchsetzt. </font></p>
                  <p class="text"><font face="Arial, Helvetica, sans-serif"><strong>Abschied vom Programmschema</strong><br>
  Nicht mehr der Programmanbieter bestimmt, was um welche Tageszeit gelesen,
    gesehen werden kann, vielmehr ruft der Nutzer das Programm ab, das er lesen,
    sehen will, z.B. irgendwann am Abend die Nachrichtensendung oder eine Folge
    aus einer Fernsehserie. Das war fr&uuml;her anders. Da bestimmte Fernsehen
    sehr viel mehr den Tagesablauf. Denn Fernsehen war urspr&uuml;nglich eine
    Besch&auml;ftigung, der man in einer zeitlich berechenbar strukturierten
    Freizeit nachging. Das ist darin begr&uuml;ndet, dass Fernsehen dann Aufmerksamkeit
    gewinnen kann, wenn die Menschen beruflich nicht mehr eingespannt sind. Das
    war in den Zeiten noch einfach, als man am Nachmittag mit der Arbeit fertig
    war und seine Zeit dann dem Fernsehen widmen konnte. Durch die Flexibilisierung
    der Arbeit wie der privat organisierten Zeit gelingt es dem Fernsehen nicht
    mehr, wie noch in den achtziger Jahren, Menschen regelm&auml;&szlig;ig zu
    bestimmten Zeiten vor dem Ger&auml;t zu versammeln. In der einen Woche ist
    der einzelne frei, die Serienfolge zu sehen, in der anderen Woche hat er
    etwas vor. Je j&uuml;nger die Zielgruppe, desto weniger ist sie f&uuml;r
    das Fernsehen erreichbar. Anders das Fernsehen, das aus einem Programmarchiv
    abrufbar ist. Das erm&ouml;glicht es dem Nutzer, Nachrichten, Berichte, Fernsehspiele
    wie auch die w&ouml;chentliche Soap dann abzurufen, wenn er Zeit hat. Damit
    wird ein Fernsehsender zu einer Datenbank, auf die die Nutzer dann zugreifen,
    wenn sie gerade am PC sitzen bzw. in ihrem Wohnzimmer sich &uuml;ber den
    Internetkanal einen Film gegen Geb&uuml;hr downloaden oder eine bestimmte
    Serienfolge aus einem Programmarchiv abrufen. Daf&uuml;r muss der gr&ouml;&szlig;ere
    Bildschirm im Wohnzimmer nur mit dem Internet verbunden sein. Das ist nicht
    mehr das Fernsehen, an das die mittlere und &auml;ltere Generation gewohnt
    sind. Es entspricht aber dem Lebensstil der Postmoderne, seine Zeit nicht
    mehr zu verplanen, sondern mehr aus dem Stand zu entscheiden, was man tun,
    was man lesen, sehen, h&ouml;ren will, wenn man treffen m&ouml;chte. </font></p>
                  <p class="text"><font face="Arial, Helvetica, sans-serif"><strong>Die Empfehlungsstruktur des Internets </strong><br>
  Die neue Fernsehnutzung ist bereits durch die Amateurvideos auf Youtube vorbereitet.
    Diese Plattform hat bereits eine ganze Generation darauf programmiert, Videos
    nicht mehr entsprechend einem starren Sendeschema anzusehen, sondern auf
    Abruf. Damit wird Youtube durch eine Grundfunktion des Internets unterst&uuml;tzt,
    n&auml;mlich das Nutzer andere auf die Inhalte aufmerksam machen, die sie
    selbst f&uuml;r interessant ansehen. Diese Empfehlungsfunktion das Besondere
    wird auch die Nutzung von Fernsehprogrammen ver&auml;ndern. Es wird wenige
    Sendungen geben, die viele anschauen und viele, die ihre Nutzer suchen m&uuml;ssen,
    aber dann auch finden. Anders als einzelne Sendungen im Fernsehen, die einmal
    ausgestrahlt werden und dann nicht mehr greifbar sind, wird das Internet
    gerade f&uuml;r besondere und anspruchsvolle Sendungen eine Zuschauerschaft
    versammeln, nicht zur gleichen Stunde, aber daf&uuml;r &uuml;ber einen l&auml;ngeren
    Zeitraum.</font></p>
                  <p class="text"><font face="Arial, Helvetica, sans-serif"><strong>Das Ende von Pay-TV</strong><br>
  Diese Form des Fernsehens basiert auch auf einem Programmschema, das haupts&auml;chlich
  aus Kinoarchiven und neu produzierten Filmen gespeist wird. Auch das Pay-TV
  ist nach dem Sendeschema strukturiert. Aber f&uuml;r das Abspielen von Kinofilmen
  gegen Geb&uuml;hr ist das Internet viel geeigneter als ein Kanal, der die gleichen
  Filme zu bestimmten Zeiten nach einem Sendeschema ausstrahlt. Deshalb kann
  Pay-TV nur als Sportkanal &uuml;berleben. Die Geb&uuml;hr f&uuml;r die Kan&auml;le
  kann der Nutzer sehr viel gezielter f&uuml;r den Download einzelner Filme einsetzen.</font></p>
                  <p class="text"><font face="Arial, Helvetica, sans-serif"><strong>Die
                        Finanzierungsbasis der privaten Sender wird unterh&ouml;hlt</strong><br>
                    Das im Internet abrufbare Fernsehen wird zu erheblichen
                      finanziellen Einbu&szlig;en
  der werbefinanzierten Programme f&uuml;hren. Der Angriff auf die finanziellen
  Ressourcen des Fernsehens erfolgt anders als bei den Tageszeitungen, wird aber
  wie dort zu einem erheblichen Aderlass f&uuml;hren. Die Tageszeitungen haben
  ihre finanzielle Basis in den Rubrikanzeigen, das Fernsehen in der Markenartikelwerbung.
  Anders als bei den Rubrikanzeigen sucht der Nutzer, wenn er z.B. eine Fernsehserie
  verfolgt oder auf den Wetterbericht wartet, keine Vierzimmerwohnung, noch eine
  offene Stelle, noch einen Gebrauchtwagen. Deshalb muss das Fernsehprogramm
  so spannend sein oder die Leute m&uuml;ssen die Nachrichtensendung unbedingt
  sehen wollen, damit sie die Unterbrechung hinnehmen, um sich die Farbkraft
  eines Shampoos oder die technischen Neuerungen einer Automarke erkl&auml;ren
  lassen. Denn die Fernsehspots unterbrechen das Programm. Je spannender der
  Film, je wichtiger die folgende Sendung, je interessierter die Menschen an
  Wetter- und Sportberichten sind, desto weniger werden sie die Werbung wegschalten.
  Im Internet gibt es keine Veranlassung, sich Werbung anzuschauen. Man kann
  sie &uuml;berspringen, denn man muss nicht auf die Fortsetzung warten, diese
  wird ja nicht erst ausgestrahlt, sondern ist auf einem Server bereits gespeichert.
  Wenn die Zuschauer noch dazu &uuml;bergehen, Serien &uuml;ber einen Internetanbieter
  herunterzuladen, dann wird es f&uuml;r die Privatsender noch schwieriger. Denn
  diese verdienen ihr Geld haupts&auml;chlich mit den t&auml;glichen und w&ouml;chentlichen
  Serien, die den Alltag ihrer Zuschauer begleiten. <br>
  Wie bei den Zeitungen wird auch das Internetfernsehen nicht durch das Internet
  finanziert, sondern durch das traditionelle Medium. Es sind auch nicht neue
  Anbieter, die Serien und Fernsehspiele zug&auml;nglich machen, sondern die
  Sender selbst. Wie die Zeitungen m&uuml;ssen sie das Internet bedienen, wollen
  sie nicht die jungen Zielgruppen verlieren. Erwartungen, dass es Fernsehplattformen
  gibt, &uuml;ber die man Programme verschiedener Sender ansehen kann, haben
  sich nicht erf&uuml;llt. Wie die Zeitung will auch der Sender mit seiner &#8222;Marke&#8220; im
  Internet pr&auml;sent sein.</font></p>
                  <p class="text"><font face="Arial, Helvetica, sans-serif"><strong>Bewegtbildwerbung im Internet</strong><br>
  Das Fernsehen war als Werbemedium deshalb so erfolgreich, weil es nicht statische
    Werbebotschaften vermittelt, sondern in den Spots Handlungen und manchmal
    sogar kleine Geschichten zu sehen sind. In der Bl&uuml;tezeit des werbefinanzierten
    privaten Fernsehens fielen &uuml;ber 40% des gesamten Werbebudgets auf das
    Fernsehen. Da f&uuml;r das geb&uuml;hrenfinanzierte Fernsehen Werbung nur
    in sehr begrenztem Umfang erlaubt wurde, 20 Minuten pro Tag je f&uuml;r ARD
    und ZDF, war das Privatfernsehen in den achtziger und neunziger Jahren ein
    finanzieller Erfolg. Die strikte Eingrenzung der Werbung in den &ouml;ffentlich-rechtlichen
    Programmen hatte vorher dem deutschen Zeitschriftenmarkt einen ungeheuren
    Aufschwung beschert, denn die Markenartikelindustrie wie die gesamte Konsumwerbung
    brauchten die Zeitschriften, um ihre Botschaften an den Verbraucher zu bringen.
    Gerade die Markenartikler und Werbeagenturen dr&auml;ngten auf die Einf&uuml;hrung
    des privaten Fernsehens, weil Beobachtungen des Werbemarktes anderer L&auml;nder
    zeigten, dass sich die Werbeausgaben erh&ouml;hen w&uuml;rden, wenn das Fernsehen
    gen&uuml;gend Zeiten f&uuml;r Werbespots anbietet. Jetzt warten alle, welche
    Werbeform f&uuml;r das Internet gefunden wird. Bisher zeigt sich noch keine &uuml;berzeugende
    L&ouml;sung. Ein Kriterium l&auml;sst sich auf jeden Fall aufstellen: Je
    eher der Spot dazu anregt, das Surfer ihn anklicken, desto erfolgreicher
    f&uuml;r de Werbeagentur.</font></p>
                  <p class="text"><font face="Arial, Helvetica, sans-serif"><strong>Die technischen Voraussetzungen</strong><br>
  Ein normales Fernsehbild besteht aus 6.5 Millionen Bildpunkten, alle 25 Sekunden
    ein neues. Damit das Bild nicht flimmert, werden zuerst die ungeraden Zeilen,
    also die 1., 3., 5. usw. und dann zwischen diese Zeilen diejenigen mit geraden
    Zahlen projiziert. Das sind Datenmengen, die bisher nur durch ein dickes
    Kupferkabel geschleust werden konnten. Jedoch braucht ein Fernsehbild nicht
    jede Sekunde 25x6,5 Millionen Bildpunkte, denn es werden nicht jede Sekunde
    25 neue Bilder &uuml;bertragen, sondern an dem bestehenden Bild &auml;ndert
    sich nur wenig, z.B. wenn ein Nachrichtensprecher vorliest, oder mehr, wenn
    in einem Film eine Verfolgungsjagd gezeigt wird. Solange die Bilder analog &uuml;bertragen
    wurden, gab es keine M&ouml;glichkeit, die Datenmenge zu reduzieren. Erst
    die Digitalisierung macht es m&ouml;glich, nur noch die Ver&auml;nderungen
    zu &uuml;bertragen, die am Bild entstehen und damit einen weniger breiten
    Datenkanal zu besetzen. Oder die Pixelzahl wird wie bei einem Foto einfach
    reduziert, wenn nur ein kleines Bild auf dem Computerbildschirm gebracht
    werden muss. Werden Bildpunkte herausgerechnet, die sich ver&auml;ndern,
    dann muss auf der Empf&auml;ngerseite die gleiche Software aktiv werden,
    die die ver&auml;nderten Bildpunkte in das n&auml;chste Bild wieder einliest.</font></p>
                <p><font face="Arial, Helvetica, sans-serif"><span class="text">Eckhard Bieger</span></font></p></td>
                <td valign="top"><script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
google_ad_width = 160;
google_ad_height = 600;
google_ad_format = "160x600_as";
google_ad_type = "text_image";
//2007-09-05: Bildung und Erziehung
google_ad_channel = "3955521420";
google_ui_features = "rc:6";
//-->
            </script>
                    <script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
              </script>
                </td>
              </tr>
            </table>
            <p class=MsoNormal> <font face="Arial, Helvetica, sans-serif"><strong> </strong></font></P>
            <p class=MsoNormal>&copy; <font size="2" face="Arial, Helvetica, sans-serif">www.kath.de</font></P>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
         <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td align="left" valign="top"><table width="216" border="0" cellpadding="0" cellspacing="0">
       <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10">
            <?php include("az.html"); ?>
          </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</body>
</html>
