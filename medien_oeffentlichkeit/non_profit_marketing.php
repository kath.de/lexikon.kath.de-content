<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Non-Profit-Marketing</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="kaltefleiter.css" title="fonts">
</head>
<body bgcolor="#ffffff" leftmargin="6" topmargin="6" marginwidth="6" marginheight="6">
<table width="100%" border="0" cellspacing="0" cellpadding="6">
  <tr>
    <td width="100" align="left" valign="top"><table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b>Medien und &Ouml;ffentlichkeit</b></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td>
            <?php include("logo.html"); ?>
          </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
    <td rowspan="2" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><h1>
                   <font face="Arial, Helvetica, sans-serif">Non-Profit-Marketing</font>              </h1>
          </td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="L12"> 
                   
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td valign="top" class="L12"><p class=MsoNormal><font face="Arial, Helvetica, sans-serif">Marketing
                            zielt auf den Markt. Wer sich auf den Markt begibt,
                            bietet etwas an und erwartet daf&uuml;r Geld. Der
                            Markt vermitteln zwischen Anbietern und Nachfragern. &Uuml;ber
                            das Marktgeschehen wird die Qualit&auml;t des Angebotes
                            eingesch&auml;tzt und der Preis ausgehandelt. Markt
                            und Profit geh&ouml;ren daher zusammen. Was haben
                            aber Unternehmen, die wie Kinderg&auml;rten, Krankenh&auml;user,
                            Kirchengemeinden nicht auf Profit aus sind, mit Marketing
                            zu tun? Sie stehen wie Schuh- und Parfumhersteller
                            nicht alleine auf dem Markt. Mag in einem Land der
                            Dritten Welt eine kleines Krankenhaus oder eine Missionsschule
                            konkurrenzlos sein, so gilt das schon nicht mehr
                            f&uuml;r eine deutsche Kleinstadt. Deshalb m&uuml;ssen
                            Non-profit-Unternehmen Angebote machen, die konkurrenzf&auml;hig
                            sind. Sie m&uuml;ssen f&uuml;r ihre Angebote Werbung
                            betreiben und sich durch &Ouml;ffentlichkeitsarbeit
                            einen Platz im Bewu&szlig;tsein ihrer Zielgruppen
                            erobern. Das gilt auch f&uuml;r die Drittweltagenturen
                            z.B. der Kirchen, die mit anderen um das Spendenaufkommen
                            konkurrieren. Ohne &Ouml;ffentlichkeitsarbeit und
                            Sammelaktionen k&ouml;nnen sie ihre Aufgabe nicht
                            erf&uuml;llen. Da Non-profit-Organisationen auf der
                            Basis von <a href="http://www.wqmanagement.de/cms/">Werten</a> entstanden
                            sind, m&uuml;ssen sie die Werte, weswegen sie gegr&uuml;ndet
                            wurden, durch ihre Angebote umsetzen und die Werte
                            durch &Ouml;ffentlichkeitsarbeit so vermitteln, da&szlig; sie
                            Unterst&uuml;tzung finden, so durch Spenden, indem
                            Eltern ihr Kind in dem Kindergarten anmelden oder
                            Menschen in einem Gottesdienst ihrer Beziehung zu
                            Gott Ausdruck verleihen k&ouml;nnen. Non-Profit-Marketing
                            beinhaltet also im Kern die Umsetzung der Werte durch
                            entsprechende Angebote und die Kommunikation mit
                            den Zielgruppen &uuml;ber die Werthaltigkeit der
                            Angebote. Diese <a href="wertorientierung.php">Wertorientierung</a> ist
                            die Basis des Handelns und zugleich Inhalt der &Ouml;ffentlichkeitsarbeit.</font>
                        <p class=MsoNormal>&nbsp;
                        <p class=MsoNormal>&copy;<font size="2" face="Arial, Helvetica, sans-serif"><a href="http://cms.weiterbildung-live.de/entwicklung.html"> weiterbildung-live</a></font></td>
                      <td valign="top"><script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
google_ad_width = 160;
google_ad_height = 600;
google_ad_format = "160x600_as";
google_ad_type = "text_image";
//2007-09-05: Bildung und Erziehung
google_ad_channel = "3955521420";
google_ui_features = "rc:6";
//-->
                  </script>
                          <script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
                    </script>
                      </td>
                    </tr>
                  </table>
                  <p class=MsoNormal><BR>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td align="left" valign="top"><table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10">
            <?php include("az.html"); ?>
          </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</body>
</html>
