<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Mystagogische Bildregie</title>
<meta name="title" content="Medien&Öffentlichkeit">
<meta name="author" content="kath.de">
<meta name="publisher" content="kath.de">
<meta name="copyright" content="kath.de">
<meta name="description" content="Stichworte über Medien und Öffentlichkeitsarbeit">
<meta name="abstract" content="Stichworte über Medien und Öffentlichkeitsarbeit">
<meta http-equiv="content-language" content="de">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="date" content="2006-00-02">
<meta name="robots" content="index,follow">
<meta name="revisit-after" content="30 days">
<meta name="revisit" content="after 30 days">
<meta name="DC.Title" content="Medien&Öffentlichkeit">
<meta name="DC.Creator" content="kath.de">
<meta name="DC.Contributor" content="N.N.">
<meta name="DC.Rights" content="kath.de">
<meta name="DC.Publisher" content="kath.de">
<meta name="DC.Date" content="2006-00-02">
<meta name="DC.Description" content="Stichworte über Medien und Öffentlichkeitsarbeit">
<meta name="DC.Language" content="de">
<meta name="DC.Type" content="Text">
<meta name="DC.Format" content="text/html">
<meta name="DC.Identifier" content="http://www.kath.de/kurs/medien_oeffentlichkeit/">
<meta name="keywords" lang="de" content="Mystagogie, intentionale Teilnahme, Prozessionen, Kuppel, Gewölbe">

<link rel="stylesheet" type="text/css" href="kaltefleiter.css" title="fonts">
</head>
<body bgcolor="#ffffff" leftmargin="6" topmargin="6" marginwidth="6" marginheight="6">
<table width="100%" border="0" cellspacing="0" cellpadding="6">
  <tr>
    <td width="100" align="left" valign="top"><table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b>Medien und &Ouml;ffentlichkeit</b></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td>
            <?php include("logo.html"); ?>
          </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
    <td rowspan="2" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2">
            <h1> <font face="Arial, Helvetica, sans-serif">Mystagogische
            Bildregie</font></h1>
          </td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="L12"> 
            
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td valign="top" class="L12"><p class=MsoNormal><font face="Arial, Helvetica, sans-serif"><strong>Die
                        Erschlie&szlig;ung der katholischen Messfeier durch Kameraf&uuml;hrung
                        und Bildregie</strong></font></P>
                  <p><font face="Arial, Helvetica, sans-serif">Wenn ein Fernsehteam
                      mit Kameras und Mikrofonen einen Gottesdienst auf den Bildschirm
                      projiziert, bekommt der Zuschauer etwas zu sehen, das nur
                      mittelbar mit dem Geschehen im Kirchenraum zu tun hat.
                      Denn das Geschehen wird zuerst in Gro&szlig;aufnahmen und
                      Totalen, in Zooms und Schwenks auseinandergenommen, um
                      dann wieder neu zusammengesetzt zu werden. Das h&auml;ngt
                      nicht zuletzt mit der Situation des Zuschauers vor dem
                      Bildschirm zusammen. W&auml;re er in der Kirche, w&uuml;rde
                      er sich mal auf den Liedtext im Gesangbuch konzentrieren,
                      mal auf den Prediger blicken, seine Augen zum Altar wenden,
                      den Gabengang verfolgen, etwas in den Klingelbeutel werfen
                      und zum Empfang der Kommunion nach vorne gehen. Vor dem
                      Fernsehschirm bewegt sich der Zuschauer nicht. Damit er
                      den Gottesdienst nicht mit einem starren Auge mitverfolgen
                      mu&szlig;, wird das Geschehen bildlich zerlegt und neu
                      zusammengesetzt. Kann aber der Zuschauer durch all die
                      Technik hindurch das Geschehen als liturgisches verstehen
                      und im Fernsehen einen <a href="gottesdienste_fernsehen.php">Gottesdienst
                      intentional mitfeiern</a> oder wird aus dem Geschehen im
                      Kirchenraum ein Film, der ganz andere Akzente setzt und
                      damit aus dem Gottesdienst irgend etwas anderes macht?
                      Damit der Gottesdienst, wie es die &Uuml;bertragungen aus
                      Rom oft zeigen, nicht zu einer Personenshow wird, indem
                      alle bekannten Pers&ouml;nlichkeiten, die am Gottesdienst
                      teilnehmen, abgefilmt werden, darf die Kontinuit&auml;t
                      des liturgischen Ablaufs nicht willk&uuml;rlich unterbrochen
                      werden. Der Zuschauer soll bei der Liturgie bleiben k&ouml;nnen
                      und nicht immer wieder Menschen anschauen m&uuml;ssen.
                      Vergleichbares geschieht bei kommentierten Gottesdiensten.
                      Der Zuschauer hat sich auf den Einzug oder Gabenbereitung
                      konzentriert und wird durch den Kommentar herausgerissen,
                      weil ihm etwas zum liturgischen Ablauf, zur Bedeutung der
                      Gaben oder das theologischen Verst&auml;ndnis der Eucharistie
                      erl&auml;utert wird. Es geht deshalb darum, eine Bildregie
                      zu praktizieren, die dem Zuschauer die Mitfeier erm&ouml;glicht
                      und zugleich das Geschehen so erschlie&szlig;t, da&szlig; auch
                      jemand dem Gottesdienst folgen kann, der mit dem <a href="http://www.kath.de/lexikon/liturgie/messe_aufbau.php">Ablauf
                      der Messe</a> nicht so vertraut ist.<br>
  Das ZDF hat zusammen mit der <a href="http://www.kirche.tv/zdf/index.htm">Katholischen
  Fernseharbeit</a> eine Kommentierung entwickelt, die nicht durch einen Kommentator,
  sondern durch die Kameraf&uuml;hrung und die Bildregie geleistet wird. Das &uuml;berrascht,
  denn andere Liveprogramme wie ein Fu&szlig;ballspiel oder eine Show kennen
  einen Kommentator oder Moderator. Was macht den Ritus fernsehf&auml;hig, so
  da&szlig; der Zuschauer ohne Kommentar verstehen kann, was er auf dem Bildschirm
  sieht? Dieses Konzept der Bidlregie ist von den fr&uuml;hchristlichen <a href="http://www.kath.de/lexikon/liturgie/mystagogie.php">mystagogischen
  Katechesen</a> inspiriert.</font></p>
                  <p><font face="Arial, Helvetica, sans-serif"><strong>Prozessionen</strong><br>
  Der abendl&auml;ndische Ritus ist eine gro&szlig;e Prozession zum Altar und
  am Ende der Messe eine Entsendung in die Welt zur&uuml;ck, um vom Evangelium
  Zeugnis zu geben. Innerhalb des Gottesdienstes gibt es dann noch die Evangelien-
  und die Gabenprozession. Zieht die Altargruppe vom Westportal ein und singt
  die Gemeinde das in der r&ouml;mischen Liturgie vorgesehene Eingangslied, dann
  beginnt der Gottesdienst, nach Kniebeuge und Altarku&szlig;, wenn der Zelebrant
  an seinem Sitz angekommen und das Eingangslied gesungen ist. Wird am Ende der
  Auszug bis zum Westportal durchgef&uuml;hrt, hat der Gottesdienst einen Rahmen.
  Die Evangelienprozession macht deutlich, da&szlig; jetzt ein wichtiger Text
  verlesen wird. Wenn der Gabengang in der Mitte des Kirchenraums beginnt, verstehen
  die Zuschauer, da&szlig; der eucharistische Teil des Gottesdienstes mit Brot
  und Wein zu tun hat. Werden diese Prozessionen durch wechselnde Kameraeinstellung
  wiedergegeben, kann der Zuschauer der inneren Dramaturgie der Messe folgen.</font></p>
                  <p><font face="Arial, Helvetica, sans-serif"><strong>Der transzendente
                        Bezug des Geschehen</strong><br>
  Der christliche Gottesdienst ist auf die Person Jesu Christi bezogen, sein
  Ged&auml;chtnis wird gefeiert. Das Kyrie ist der zentrale Ruf der Er&ouml;ffnung,
  durch den Christus als der Herr begr&uuml;&szlig;t wird. Die Darstellung des
  Hirten in der Apsis fr&uuml;her Kirchen oder das Kreuz sind die Motive, mit
  der die Kamera dem Kyriegesang seinen Inhalt gibt. <br>
  Die Kirchenr&auml;ume sind bewu&szlig;t f&uuml;r die Feier des Gottesdienstes
  gestaltet worden. Weil der Gottesdienst des Abendlandes die Prozession als
  Grundform hat, sind die meisten Kirchen nicht als Zentralbau konzipiert, sondern
  haben ein Langhaus, dem der Chor als zentraler Ort des Geschehens vorgelagert
  ist. Mystagogische Bildregie l&auml;&szlig;t den Zuschauer durch den Raum schreiten.<br>
  Die Kuppel der romanischen Kirche, das Gew&ouml;lbe einer gotischen, die Deckengem&auml;lde
  einer Barock-Kirche symbolisieren den Himmel und erm&ouml;glichen es durch
  Kameraf&uuml;hrung und &Uuml;berblendung, eine wichtige Dimension des Gottesdienstes
  zu erschlie&szlig;en: Die Gemeinde feiert nicht nur f&uuml;r sich, sondern
  ist mit der himmlischen Liturgie verbunden, denn die eigentliche Liturgie wird
  im Himmel gefeiert, so wie es im letzten Buch der Bibel, in der Geheimen Offenbarung,
  zu lesen ist.<br>
  F&uuml;r das Gloria und das Sanctus bieten die Kirchenr&auml;ume die M&ouml;glichkeit,
  die Verbindung des irdischen Gesangs mit den himmlischen Ch&ouml;ren zu zeigen,
  indem die Kamera die Gew&ouml;lbe abf&auml;hrt und Chor und singende Gemeinde &uuml;berblendet
  werden. Die Gew&ouml;lbe und Kuppeln sind die Bildinterpretation der alten
  Ges&auml;nge.<br>
  Altarbilder, Skulpturen und andere Bildmotive erm&ouml;glichen es, mit der
  Kamera einzelne Passagen des Gottesdienstes zu interpretieren.</font></p>
                  <p><font face="Arial, Helvetica, sans-serif"><strong>Die innere
                        Dramaturgie der Eucharistiefeier</strong><br>
  Wer vor Gott hintritt, wird sich seiner Unw&uuml;rdigkeit bewu&szlig;t und
  erkennt, da&szlig; nur Gott ihn aus seiner s&uuml;ndigen Existenz befreien
  kann. Messe hei&szlig;t als Ganzes &#8222;Wandlung&#8220;. Aber wie stellt
  die Me&szlig;feier die Erl&ouml;sung dar? Daf&uuml;r braucht es eine Hinf&uuml;hrung.
  Zuerst begegnen die Gl&auml;ubigen Christus im Wort, denn die Zusage der Vergebung
  im Er&ouml;ffnungsteil k&ouml;nnen die Mitfeiernden nicht so einfach mit vollziehen.
  Jeder kommt aus einer Woche voller Konflikte und Entt&auml;uschungen. Kann
  Gott mich davon befreien, da&szlig; ich st&auml;ndig an mir selbst scheitere?
  Vom Wirken Gottes in Ihrem Leben haben die zum Gottesdienst Versammelten wenig
  gesp&uuml;rt. Das zeigt die Aufgabe der Predigt, denn durch die biblischen
  Texte und ihre Auslegung in der Predigt sollen die einzelnen nicht blo&szlig; deren
  urspr&uuml;nglichen Sinn erkennen, um zu wissen, wie die Texte richtig zu interpretieren
  sind. Jede Woche, ja jeden Tag mu&szlig; der Christ neu lernen, wie Gott auf
  krummen Zeilen gerade schreibt und auf seinen Wegen das Werk der Erl&ouml;sung
  vollbringt. Erst dann ist der Teilnehmer am Gottesdienst innerlich disponiert,
  das Hochgebet mitzuvollziehen, um dann Christus in der Kommunion zu begegnen.
  Die Fernsehdramaturgie zeigt ganz deutlich, da&szlig; nicht die Wandlung der
  H&ouml;hepunkt ist, sondern da&szlig; die Dramaturgie eines Gottesdienstes
  auf die pers&ouml;nliche Begegnung mit Christus hinl&auml;uft. An der Begegnung
  mit Christus kann der Zuschauer auch teilnehmen, wenn Kommunionhelfer seiner
  Heimatgemeinde ihm am Sonntag nach dem Gottesdienst die Kommunion bringen.
  Die Fernseh&uuml;bertragung behilft sich mit einer Meditation, die Bildmotive
  des Kirchenraumes einbezieht und zu einer Begegnung mit dem Auferstandenen
  einl&auml;dt, die Kommunionmeditation. </font></p>
                  <p><font face="Arial, Helvetica, sans-serif"><strong>Einbeziehung
                        des Zuschauers in die Feier</strong><br>
  F&uuml;r die von Wolfgang Fischer und anderen entwickelte &#8222;Mystagogische
  Bildregie&#8220; ist noch etwas bedeutsam: Damit die Zuschauer den Gottesdienst
  mitfeiern k&ouml;nnen und sich nicht als Voyeure f&uuml;hlen, die wie durch
  ein Schl&uuml;sselloch dem Geschehen folgen m&uuml;ssen, werden sie am Anfang
  als G&auml;ste angesprochen, bei Lesungen und der Predigt durch Blick in die
  Kamera einbezogen. In den F&uuml;rbitten werden ihre Anliegen aufgegriffen
  und in der Kommunionmeditation wird ihnen eine Begegnung mit Christus er&ouml;ffnet,
  die der Idee der geistlichen Kommunion nachgebildet ist.<br>
  Schlie&szlig;lich geh&ouml;rt die telefonische Erreichbarkeit der Gemeinde
  nach der &Uuml;bertragung zum Konzept einer Fernsehmystagogie, die den Zuschauer
  nicht als Au&szlig;enstehenden behandelt, sondern einbezieht.<br>
  Eckhard Bieger</font></p>
                  <p><font face="Arial, Helvetica, sans-serif">Literatur:<br>
  Bieger, Eckhard, Religi&ouml;se Rede im Fernsehen, K&ouml;ln 1995, S. 219ff<br>
  Bieger, Eckhard ,Norbert Blome, Heinz Heckwolf (Hrsg), Schnittpunkte zwischen
  Himmel und Erde, Kirche als Erfahrungsraum des Glaubens, Kevelaer 1998<br>
  Gilles, Beate, Durch das Auge der Kamera, Eine liturgie-theologische Untersuchung
  zur &Uuml;bertragung von Gottesdiensten im Fernsehen, M&uuml;nster, Reihe: &Auml;sthetik
  - Theologie &#8211; Liturgik, Bd. 16, 2001<br>
  Ohly, Friedrich Die Kathedrale als Zeitenraum. Zum Dom von Siena; in:<br>
  Fr&uuml;hmittelalterliche Studien, [FMSt 6, 1972, S. 94-158].<br>
  Die Ergebnisse vieler Arbeitsgespr&auml;che zur Bildregie bei Gottesdienst&uuml;bertragungen
  sind von Helmut B&uuml;sse in die Leitlinien f&uuml;r Gottesdienst&uuml;bertragungen:<br>
  Gottesdienst&uuml;bertragungen in H&ouml;rfunk und Fernsehen, Leitlinien und
  Empfehlungen 2002, Hrsg. Vom Sekretariat der Deutschen Bischofskonferenz in
  Zusammenarbeit mit den Liturgischen Instituten Deutschlands, &Ouml;sterreichs
  und der Schweiz, Nr. 4.9,S. 97-12</font></p>
                  <p><font face="Arial, Helvetica, sans-serif">Die liturgietheologische
                      Diskussion der achtziger Jahre findet sich im Liturgischen
                      Jahrbuch, abschlie&szlig;end: Michael B&ouml;hnke, Welche
                      Art von Teilnahme ist einem Zuschauer einer Fernseh&uuml;bertragung
                      von Gottesdiensten m&ouml;glich? LJ 37/1987/S.5-16</font></p>
                  <p></p>
                  <p><font face="Arial, Helvetica, sans-serif">Informationen
                      zu den <a href="http://www.fernsehgottesdienst.de/">Gottesdienst&uuml;bertragungen
                      im ZDF</a></font></p>
                  <p class=MsoNormal> <font face="Arial, Helvetica, sans-serif"><strong> </strong></font></P>
                <p class=MsoNormal>&copy; <font size="2" face="Arial, Helvetica, sans-serif">www.kath.de</font></P></td>
                <td valign="top"><script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
google_ad_width = 160;
google_ad_height = 600;
google_ad_format = "160x600_as";
google_ad_type = "text_image";
//2007-09-05: Bildung und Erziehung
google_ad_channel = "3955521420";
google_ui_features = "rc:6";
//-->
            </script>
                    <script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
              </script>
                </td>
              </tr>
            </table>
            <p class=MsoNormal>&nbsp;</P>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
         <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td align="left" valign="top"><table width="216" border="0" cellpadding="0" cellspacing="0">
       <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10">
            <?php include("az.html"); ?>
          </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</body>
</html>
