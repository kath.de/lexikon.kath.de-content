<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>&Ouml;ffentlichkeit</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="kaltefleiter.css" title="fonts">
</head>
<body bgcolor="#ffffff" leftmargin="6" topmargin="6" marginwidth="6" marginheight="6">
<table width="100%" border="0" cellspacing="0" cellpadding="6">
  <tr>
    <td width="100" align="left" valign="top"><table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b>Medien und &Ouml;ffentlichkeit</b></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td>
            <?php include("logo.html"); ?>
          </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
    <td rowspan="2" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2">
            <h1> <font face="Arial, Helvetica, sans-serif">&Ouml;ffentlichkeit</font></h1>
          </td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="L12"> 
         
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td valign="top" class="L12"><p class=MsoNormal><strong><font face="Arial, Helvetica, sans-serif">&Ouml;ffentlichkeit
                        ist der allen zug&auml;ngliche Raum</font></strong></P>
                  <p><font face="Arial, Helvetica, sans-serif">&Ouml;ffentlichkeit
                      entsteht durch Kommunikation. &Ouml;ffentlich ist die einzelne
                      Meinungs&auml;u&szlig;erung, die im Raum der &Ouml;ffentlichkeit
                      zu Geh&ouml;r gebracht wird. Die &Ouml;ffentlichkeit ist
                      somit einem Raum vergleichbar, der durch kommunikative
                      Aktivit&auml;ten der Beteiligten strukturiert und ausgef&uuml;llt
                      wird. Ein kommunikativer Raum wird dadurch &ouml;ffentlich,
                      da&szlig; er f&uuml;r m&ouml;glichst alle zug&auml;nglich
                      ist. Das verlangt eine Struktur und die <a href="oeffentlichkeit_prozess.php">periodische
                      Organisation</a> eines Forums bzw. eines Mediums, so z.B.
                      das Gespr&auml;ch auf dem Marktplatz oder im Wirtshaus
                      nach dem Gottesdienst, die Mitgliederversammlung eines
                      Vereins, der Landtag eines Schweizer Kantons, die t&auml;glich
                      erscheinende Zeitung, die Nachrichtensendung, die Podiumsdiskussion
                      in einem allen zug&auml;nglichen Saal, die <br>
  Diskussionsrunde im Fernsehen. <br>
&Ouml;ffentlichkeit braucht also immer Foren bzw. Medien, die den Raum vorgeben,
in dem dann &ouml;ffentliche Kommunikation stattfinden kann. &Ouml;ffentlichkeit
mu&szlig; von den Beteiligten t&auml;glich oder in anderen Rhythmen durch Kommunikationsleistungen &quot;hergestellt&quot; werden.
Pr&auml;senz im Raum der &Ouml;ffentlichkeit ist also abh&auml;ngig von der Aktivit&auml;t
der Beteiligten. Schweigt ein einzelner oder eine Gruppe, existiert er bzw. sie
nicht &ouml;ffentlich oder ist er nur in der Rolle des Zuh&ouml;rers, des Beobachters,
des Zeitungslesers und Fernsehzuschauers pr&auml;sent und wird nat&uuml;rlich
von denen, die sich in der &Ouml;ffentlichkeit zu Wort melden, als Zuh&ouml;rer
gemeint. Allerdings ist die Rolle des einzelnen in verschiedenen F&auml;llen
nicht die des blo&szlig;en Beobachters. Das Geschehen in der &Ouml;ffentlichkeit
ist durch unterschiedliche Identifikationen bestimmt. Das ist durch die Organisation
des Gespr&auml;chs in der &Ouml;ffentlichkeit bedingt. Denn nicht jeder kann
bei einer Versammlung reden, sonst w&uuml;rde ein Stimmengewirr entstehen. Deshalb
m&uuml;ssen die Sprechm&ouml;glichkeiten reguliert werden. Das ist Aufgabe und
Recht des Versammlungsleiters und bei technisch verbreitenden Medien Sache des
Redakteurs. Die begrenzten Sprechm&ouml;glichkeiten f&uuml;r den einzelnen f&uuml;hren
dazu, da&szlig; sich Zusammenschl&uuml;sse bilden, die als Gruppe durch einen
Sprecher ihre Position im &ouml;ffentlichen Raum zur Sprache bringen. Einzelne
haben es schwerer, in der &Ouml;ffentlichkeit zu Wort zu kommen. Sie m&uuml;ssen
daf&uuml;r eine bestimmte Qualifizierung mitbringen &#8211; als Fachmann, als
Meinungstr&auml;ger, die einer Position Ausdruck verleihen, die von keiner etablierten
Gruppierung vertreten wird, oder als &quot;Rebellen&quot;, die in einer Partei,
Gewerkschaft, Kirche Widerspruch gegen&uuml;ber den offiziellen Sprechern der
Gruppierung anmelden. Diese Rebellen genie&szlig;en die besondere Aufmerksamkeit
der Medien.</font></p>
                  <p><font face="Arial, Helvetica, sans-serif">Eckhard Bieger<br>
                  </font></p>
                  <p class=MsoNormal>&nbsp;</P>
                  <p class=MsoNormal>&nbsp;</P>
                  <p class=MsoNormal>&nbsp;</P>
                  <p class=MsoNormal>&nbsp;</P>
                <p class=MsoNormal><font face="Arial, Helvetica, sans-serif">&copy; www.kath.de</font></P></td>
                <td valign="top"><script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
google_ad_width = 160;
google_ad_height = 600;
google_ad_format = "160x600_as";
google_ad_type = "text_image";
//2007-09-05: Bildung und Erziehung
google_ad_channel = "3955521420";
google_ui_features = "rc:6";
//-->
            </script>
                    <script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
              </script>
                </td>
              </tr>
            </table>
            <p class=MsoNormal>&nbsp;</P>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
         <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td align="left" valign="top"><table width="216" border="0" cellpadding="0" cellspacing="0">
       <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10">
            <?php include("az.html"); ?>
          </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</body>
</html>
