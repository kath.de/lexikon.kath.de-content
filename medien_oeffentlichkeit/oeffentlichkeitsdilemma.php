<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>&Ouml;ffentlichkeitsdilemma</title>
<meta name="title" content="Medien&Öffentlichkeit">
<meta name="author" content="kath.de">
<meta name="publisher" content="kath.de">
<meta name="copyright" content="kath.de">
<meta name="description" content="Stichworte über Medien und Öffentlichkeitsarbeit">
<meta name="abstract" content="Stichworte über Medien und Öffentlichkeitsarbeit">
<meta http-equiv="content-language" content="de">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="date" content="2006-00-02">
<meta name="robots" content="index,follow">
<meta name="revisit-after" content="30 days">
<meta name="revisit" content="after 30 days">
<meta name="DC.Title" content="Medien&Öffentlichkeit">
<meta name="DC.Creator" content="kath.de">
<meta name="DC.Contributor" content="N.N.">
<meta name="DC.Rights" content="kath.de">
<meta name="DC.Publisher" content="kath.de">
<meta name="DC.Date" content="2006-00-02">
<meta name="DC.Description" content="Stichworte über Medien und Öffentlichkeitsarbeit">
<meta name="DC.Language" content="de">
<meta name="DC.Type" content="Text">
<meta name="DC.Format" content="text/html">
<meta name="DC.Identifier" content="http://www.kath.de/kurs/medien_oeffentlichkeit/">
<meta name="keywords" lang="de" content="überzeugen, Interview, Partei, Gewerkschaft, Kirche, Argumentationsschwäche">

<link rel="stylesheet" type="text/css" href="kaltefleiter.css" title="fonts">
</head>
<body bgcolor="#ffffff" leftmargin="6" topmargin="6" marginwidth="6" marginheight="6">
<table width="100%" border="0" cellspacing="0" cellpadding="6">
  <tr>
    <td width="100" align="left" valign="top"><table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b>Medien und &Ouml;ffentlichkeit</b></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td>
            <?php include("logo.html"); ?>
          </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
    <td rowspan="2" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2">
            <h1> <font face="Arial, Helvetica, sans-serif">&Ouml;ffentlichkeitsdilemma </font></h1>
          </td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="L12"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td valign="top" class="L12"><p class=MsoNormal><font face="Arial, Helvetica, sans-serif"><strong>Wenn
                        eine Institution in der &Ouml;ffentlichkeit nicht &uuml;berzeugt</strong></font></P>
                  <p><font face="Arial, Helvetica, sans-serif">Wer sich in die &Ouml;ffentlichkeit
                      begibt, ger&auml;t in ein Dilemma, wenn er mit seiner Argumentation,
                      mit seiner &Uuml;berzeugungskraft, seinem Charme in der &Ouml;ffentlichkeit
                      nicht &uuml;berzeugt. Dann schl&auml;gt sich die negative
                      Wirkung in der &Ouml;ffentlichkeit nicht nur in einem schlechten
                      Image nieder, sondern alle Mitglieder einer Institution
                      sind in ihren t&auml;glichen Aktivit&auml;ten vor Ort beeintr&auml;chtig.
                      Wenn ein Vertreter, einer Partei, einer Gewerkschaft, einer
                      Kirche in einer &ouml;ffentlichen Diskussion nicht &uuml;berzeugt,
                      in einem Interviewe nicht Rede und Antwort stehen kann,
                      dann hemmt das am n&auml;chsten Tag die Mitglieder der
                      Partei, die Funktion&auml;re einer Gewerkschaft, die haupt-
                      und ehrenamtlichen Mitarbeiter einer Kirche in ihrer Kommunikation
                      mit den Menschen am Arbeitsplatz, mit ihren Nachbarn, in
                      der eigenen Familie. Das Dilemma wird einer Partei, Kirche
                      oder Gewerkschaft nicht von au&szlig;en aufgezwungen, sondern
                      es wird durch die eigenen Repr&auml;sentanten verursacht.
                      Es ist ein Kommunikationsdilemma, weil es die Bereitschaft,
                      f&uuml;r die eigene Partei, Gewerkschaft, Kirche nach au&szlig;en
                      aktiv zu werden, abbaut. Je schlechter eine Institution
                      in der &Ouml;ffentlichkeit dasteht, desto mehr besch&auml;ftigen
                      sich ihre Gremien mit dem Zustand der Institution anstatt
                      die Nicht-Mitglieder vom Wert der Institution zu &uuml;berzeugen.
                      Das Kommunikationsdilemma entsteht nicht aus technischen
                      M&auml;ngeln oder fehlenden Mitarbeitern f&uuml;r die &Ouml;ffentlichkeitsarbeit,
                      sondern aus einer argumentativen Schw&auml;che. <br>
  Am Bespiel der katholischen Kirche zeigt der Autor, da&szlig; das Kommunikationsdilemma
  dieser Kirche in der mangelnden F&auml;higkeit besteht, zur Freiheitsthematik
  der Sp&auml;ten Moderne einen &uuml;berzeugenden Beitrag zu leisten.<br>
  Eckhard Bieger: <a href="http://www.kath.de/aktuell/Buchvorstellungen/Bieger/oeffentlichkeitsdilemma-katholisches-1.htm">Das &Ouml;ffentlichkeitsdilemma
  der katholischen Kirche</a><br>
                <a href="http://www.kathshop.de/wwwkathde/vlb.php?anbieter=17&products_id=188">zum
                Buchkauf</a> <br>
                  </font></p>
                  <p class=MsoNormal>&nbsp; </P>
                <p class=MsoNormal><font face="Arial, Helvetica, sans-serif">&copy; <font size="2"><a href="http://www.kath.de/">www.kath.de</a></font></font></P></td>
                <td valign="top"><script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
google_ad_width = 160;
google_ad_height = 600;
google_ad_format = "160x600_as";
google_ad_type = "text_image";
//2007-09-05: Bildung und Erziehung
google_ad_channel = "3955521420";
google_ui_features = "rc:6";
//-->
            </script>
                    <script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
              </script>
                </td>
              </tr>
            </table>
            <p class=MsoNormal>&nbsp;</P>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
         <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td align="left" valign="top"><table width="216" border="0" cellpadding="0" cellspacing="0">
       <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10">
            <?php include("az.html"); ?>
          </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</body>
</html>
