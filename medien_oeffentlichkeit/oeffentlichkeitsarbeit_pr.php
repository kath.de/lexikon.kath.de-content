<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>&Ouml;ffetnlichkeitsarbeit PR</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="kaltefleiter.css" title="fonts">
</head>
<body bgcolor="#ffffff" leftmargin="6" topmargin="6" marginwidth="6" marginheight="6">
<table width="100%" border="0" cellspacing="0" cellpadding="6">
  <tr>
    <td width="100" align="left" valign="top"><table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b>Medien und &Ouml;ffentlichkeit</b></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td>
            <?php include("logo.html"); ?>
          </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
    <td rowspan="2" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2">
            <h1> <font face="Arial, Helvetica, sans-serif">&Ouml;ffentlichkeitsarbeit PR</font></h1>
          </td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="L12"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td valign="top" class="L12"><p class=MsoNormal><font face="Arial, Helvetica, sans-serif"><strong>Die
                        Beziehung zur &Ouml;ffentlichkeit gestalten</strong></font></P>
                  <p class=MsoNormal><font face="Arial, Helvetica, sans-serif">Der
                      Wortteil &quot;arbeit&quot; in &Ouml;ffentlichkeitsarbeit
                      signalisiert, da&szlig; es Arbeit macht, mit der &Ouml;ffentlichkeit
                      im Gespr&auml;ch zu bleiben. Das leitet sich davon ab,
                      da&szlig; der &ouml;ffentliche Raum nicht einfach da ist
                      wie die Natur, wenn wir morgens erwachen, sondern hergestellt
                      werden mu&szlig;. D.h. es m&uuml;ssen Leute eine Zeitung
                      zusammengestellt, gedruckt und ausgetragen haben, damit
                      mir beim Fr&uuml;hst&uuml;ck der &ouml;ffentliche Raum
                      zug&auml;nglich wird. Die <a href="http://www.kath.de/kurs/medien_oeffentlichkeit/periodizitaet.php">Periodizit&auml;t</a> der
                      Medien stellt immer wieder <a href="oeffentlichkeit.php">&Ouml;ffentlichkeit</a> her.
                      Das Wort Public Relations besagt, was in der &Ouml;ffentlichkeitsarbeit
                      zu tun ist, n&auml;mlich die Beziehung zur &Ouml;ffentlichkeit
                      aktiv zu gestalten, indem es gelingt, da&szlig; die Medien &uuml;ber
                      das Unternehmen, die Einrichtung, die Person berichten.
                      Das gelingt mit gro&szlig;er Wahrscheinlichkeit, wenn &uuml;ber <a href="skandal_berichterstattung.php">Skandale</a> berichtet
                      werden kann. Da das ncith den Interessen des Unternehmens,
                      der Einrichtung entspricht, sucht die &Ouml;ffentlichkeitsarbeit
                      die positiven Seiten des Unternehmens, der Einrichtung
                      in die &Ouml;ffentlichkeit zu vermitteln. Ziel der &Ouml;ffentlichkeitsarbeit
                      ist daher, m&ouml;glichst gro&szlig;en R&uuml;ckhalt f&uuml;r
                      das Unternehmen, die Einrichtung zu gewinnen. Das gelingt
                      am besten, wenn die &Ouml;ffetnlichketisarbeit die Werte
                      transportiert, f&uuml;r die die Einrichtung, das Untenrehmen
                      steht.</font></P>
                  <p class=MsoNormal>&nbsp;</P>
                  <p class=MsoNormal>&nbsp;</P>
                  <p class=MsoNormal>&nbsp;</P>
                  <p class=MsoNormal>&nbsp;</P>
                <p class=MsoNormal><font face="Arial, Helvetica, sans-serif">&copy; <font size="2">www.kath.de</font></font></P></td>
                <td valign="top"><script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
google_ad_width = 160;
google_ad_height = 600;
google_ad_format = "160x600_as";
google_ad_type = "text_image";
//2007-09-05: Bildung und Erziehung
google_ad_channel = "3955521420";
google_ui_features = "rc:6";
//-->
            </script>
                    <script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
              </script>
                </td>
              </tr>
            </table>
            <p class=MsoNormal>&nbsp;</P>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
         <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td align="left" valign="top"><table width="216" border="0" cellpadding="0" cellspacing="0">
       <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10">
            <?php include("az.html"); ?>
          </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</body>
</html>
