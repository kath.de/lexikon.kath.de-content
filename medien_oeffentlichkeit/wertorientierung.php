<HTML><HEAD><TITLE>Wertorientierung</TITLE>
<META http-equiv=Content-Type content="text/html; charset=iso-8859-1"><LINK 
title=fonts href="kaltefleiter.css" type=text/css 
rel=stylesheet>
<META content="MSHTML 5.50.4134.600" name=GENERATOR></HEAD>
<BODY bgColor=#ffffff leftMargin=6 topMargin=6 marginheight="6" marginwidth="6">
<TABLE cellSpacing=0 cellPadding=6 width="100%" border=0>
  <TBODY>
  <TR>
    <TD vAlign=top align=left width=100> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif" height="14"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2" height="14"><b>Medien und &Ouml;ffentlichkeit</b></td>
          <td background="boxtopright.gif" height="14"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td> <?php include("logo.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10"> <?php include("az.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </TD>
    <TD vAlign=top rowSpan=2>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
        <TBODY>
        <TR vAlign=top align=left>
          <TD width=8><IMG height=8 alt="" 
            src="boxtopleftcorner.gif" width=8></TD>
          <TD background=boxtop.gif><IMG height=8 alt="" 
            src="boxtop.gif" width=8></TD>
          <TD width=8><IMG height=8 alt="" 
            src="boxtoprightcorner.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxtopleft.gif><IMG height=8 alt="" 
            src="boxtopleft.gif" width=8></TD>
          <TD bgColor=#e2e2e2>
            <H1>
                                            <font face="Arial, Helvetica, sans-serif">Wertorientierung
                                        </font></H1>
          </TD>
          <TD background=boxtopright.gif><IMG height=8 
            alt="" src="boxtopright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=13 alt="" src="boxdividerleft.gif" 
            width=8></TD>
          <TD background=boxdivider.gif><IMG height=13 
            alt="" src="boxdivider.gif" width=8></TD>
          <TD><IMG height=13 alt="" 
            src="boxdividerright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxleft.gif height="124"><IMG height=8 alt="" 
            src="boxleft.gif" width=8></TD>
          <TD class=L12 height="124"> 
            	<table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td valign="top" class="L12"><P STYLE="margin-bottom: 0cm"><FONT SIZE=3 face="Arial, Helvetica, sans-serif">Wer
                          sich am Nutzen des Kunden orientiert, verwirklicht
                          Wertorientierung, denn dem Kunden mu&szlig; es etwas &#034;bringen&#034;,
                          da&szlig; er ein bestimmtes Produkt kauft, sein Kind<FONT COLOR="#008000"> </FONT>ins
                          Jugendlager einer Kirchengemeinde schickt, sich gerade<FONT COLOR="#008000"> </FONT>in
                          diesem Krankenhaus behandeln l&auml;&szlig;t. Jedes
                          Mal kann er bestimmte Werte verwirklichen und erleben,
                          z.B. Gemeinschaft erfahren, sich in seiner Situation
                          als Kranker verstanden f&uuml;hlen und die Unterst&uuml;tzung
                          zu erhalten, um seine Gesundheit wieder zu st&auml;rken
                          oder eine spirituelle Praxis f&uuml;r sein Leben zu
                          entwickeln. Die Wertorientierung richtet sich an den
                          Vorstellungen der <A HREF="http://www.kath.de/WQMLexikon/zielgruppe.php">Zielgruppen</A> </FONT><font face="Arial, Helvetica, sans-serif"><FONT SIZE=3>aus
                          und setzt die besonderen Werte, f&uuml;r die die Institution,
                          die Einrichtung, das Unternehmen stehen, um. Daher
                          verspricht jedes Angebot, ob es sich um einen Konsumartikel
                          oder die Dienstleistung eines <A HREF="non_profit_marketing.php">Non-Profit-Unternehmen</A> handelt,
                          dem Kunden, dem Nutzer einen bestimmten Wert, der in
                          dem Profil des Unternehmens ausdr&uuml;cklich oder
                          implizit formuliert ist. Die im Profil oder Leitbild
                          formulierten Werte bilden die Grundlage f&uuml;r das <A HREF="http://www.kath.de/WQMLexikon/qualitaetsmanagement.php">Qualit&auml;tsmangement</A> </FONT> </font></P>
                      <P STYLE="margin-bottom: 0cm"> </P>
                    <P><FONT SIZE=3 face="Arial, Helvetica, sans-serif"><I><A HREF="http://www.weiterbildung-live.de/">&copy;</A> </I><font size="2"><a href="http://cms.weiterbildung-live.de/entwicklung.html">weiterbildung-live</a></font><a href="http://cms.weiterbildung-live.de/entwicklung.html"><I> </I></a></FONT></P></td>
                    <td valign="top"><script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
google_ad_width = 160;
google_ad_height = 600;
google_ad_format = "160x600_as";
google_ad_type = "text_image";
//2007-09-05: Bildung und Erziehung
google_ad_channel = "3955521420";
google_ui_features = "rc:6";
//-->
                </script>
                        <script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
                  </script>
                    </td>
                  </tr>
                </table>
            	<P STYLE="margin-bottom: 0cm"><FONT SIZE=3 face="Arial, Helvetica, sans-serif"><I>		</I></FONT>			</P>
			<P>
                                             
              </TD>
          <TD background=boxright.gif height="124"><IMG height=8 alt="" 
            src="boxright.gif" width=8></TD>
        </TR>
        <TR vAlign=top align=left>
          <TD><IMG height=8 alt="" src="boxbottomleft.gif" 
            width=8></TD>
          <TD background=boxbottom.gif><IMG height=8 alt="" 
            src="boxbottom.gif" width=8></TD>
          <TD><IMG height=8 alt="" src="boxbottomright.gif" 
            width=8></TD></TR></TBODY></TABLE></TD></TR>
  <TR>
    <TD vAlign=top align=left height="2">&nbsp; </TD>
  </TR></TBODY></TABLE></BODY></HTML>
