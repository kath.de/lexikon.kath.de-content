<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Diplomtheologen in den Medien</title>
<meta name="title" content="Medien&Öffentlichkeit">
<meta name="author" content="kath.de">
<meta name="publisher" content="kath.de">
<meta name="copyright" content="kath.de">
<meta name="description" content="Stichworte über Medien und Öffentlichkeitsarbeit">
<meta name="abstract" content="Stichworte über Medien und Öffentlichkeitsarbeit">
<meta http-equiv="content-language" content="de">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="date" content="2006-00-02">
<meta name="robots" content="index,follow">
<meta name="revisit-after" content="30 days">
<meta name="revisit" content="after 30 days">
<meta name="DC.Title" content="Medien&Öffentlichkeit">
<meta name="DC.Creator" content="kath.de">
<meta name="DC.Contributor" content="N.N.">
<meta name="DC.Rights" content="kath.de">
<meta name="DC.Publisher" content="kath.de">
<meta name="DC.Date" content="2006-00-02">
<meta name="DC.Description" content="Stichworte über Medien und Öffentlichkeitsarbeit">
<meta name="DC.Language" content="de">
<meta name="DC.Type" content="Text">
<meta name="DC.Format" content="text/html">
<meta name="DC.Identifier" content="http://www.kath.de/kurs/medien_oeffentlichkeit/">
<meta name="keywords" lang="de" content="Theologiestudium, Medien, Kommuniaktionsberuf, Drehbücher, Lokalredaktion, Journalismus">

<link rel="stylesheet" type="text/css" href="kaltefleiter.css" title="fonts">
</head>
<body bgcolor="#ffffff" leftmargin="6" topmargin="6" marginwidth="6" marginheight="6">
<table width="100%" border="0" cellspacing="0" cellpadding="6">
  <tr>
    <td width="100" align="left" valign="top"><table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b>Medien und &Ouml;ffentlichkeit</b></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td>
            <?php include("logo.html"); ?>
          </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
    <td rowspan="2" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2">
            <h1> <font face="Arial, Helvetica, sans-serif">Diplomtheologen
            in den Medien</font></h1>
          </td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="L12"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td valign="top" class="L12"><p class=MsoNormal><font face="Arial, Helvetica, sans-serif"><strong>Berufschancen
                        f&uuml;r Theologen und Theologinnen in den Medien</strong></font></P>
                  <p><font face="Arial, Helvetica, sans-serif">Wer ein Theologiestudium
                      macht, bereitet sich auf einen Kommunikationsberuf vor.
                      Er oder sie werden Geschichten erz&auml;hlen und auslegen,
                      Menschen zu einem Austausch &uuml;ber religi&ouml;se und
                      Lebensfragen, predigen, unterrichten. Nichts anderes tun
                      die Medien. Die Zeitungen berichten, was sich gezeitigt
                      hat, das Fernsehen erz&auml;hlt eine Geschichte nach der
                      anderen. Viele Shows sind nicht sehr viel anders aufgebaut
                      als ein Pfarrfest. Da Theologen und Theologinnen daf&uuml;r
                      ausgebildet sind, Kommunikation in Flu&szlig; zuhalten,
                      finden sie sich leichter als andere Berufsgruppen in den
                      Medien ein. Sie sind in Zeitungsredaktionen t&auml;tig,
                      moderieren Sendungen, schreiben Drehb&uuml;cher. Das einzige
                      Handicap, das sie aus dem Studium mitbringen, ist die eigenartige
                      Sprache, die sie von ihren Theologieprofessoren beigebracht
                      bekommen. Da die Professoren ihre umst&auml;ndliche Satzkonstruktionen
                      im Examen gerne wieder h&ouml;ren wollen, bringen sie den
                      Studierenden nicht nur philosophisches und theologisches
                      Denken bei, sondern auch eine Sprache, die au&szlig;erhalb
                      kirchlicher Kleingruppen niemand versteht. Deshalb empfiehlt
                      es sich f&uuml;r alle Studierenden, ob sie einen Beruf
                      in den Medien anstreben oder im kirchlichen Dienst, f&uuml;r
                      eine Lokalredaktion zu schreiben. Sie erh&ouml;hen damit
                      die Chance, da&szlig; sie sp&auml;ter von ein paar mehr
                      Leuten als den treuen Kirchg&auml;ngern verstanden werden.<br>
  Brauchen aber die Medien Theologen? Das aus mehreren Gr&uuml;nden. Das <a href="medienausbildung_theologen.php">Theologiestudium</a> ist
  ein besonders gutes Training f&uuml;r den journalistischen Alltag. Da kommt
  es auf gutes, fl&uuml;ssiges Denken an und kommunikative Kompetenz.<br>
  Seit Ende der achtziger Jahre haben die Medien entdeckt, da&szlig; man mit
  Religion &#8222;Quote&#8220; machen kann. Der polnische Papst aber auch Pfarrer
  als Serienhelden zeigten, da&szlig; beim Publikum ein bisher nicht vermutetes
  Interesse da ist. Dieses Interesse ist weltweit gewachsen, nicht nur in den
  USA und Asien, sondern auch immer mehr in Europa. Inzwischen versprechen religi&ouml;se
  Kinofilme eine sehr viel h&ouml;here Rendite als Aktionformate. Diese Kinoerfolge
  zeigen, da&szlig; Menschen bereit sind, f&uuml;r das Religi&ouml;se nicht nur
  Zeit aufzuwenden, sondern auch Geld.<br>
  Lokalredaktionen brauchen Theologen und Theologinnen dringend, weil die Kirche
  vor allem eine lokale Gr&ouml;&szlig;e ist. Jede Woche gibt es mehrere Veranstaltungen.
  Nun ist es aber sehr viel leichter, &uuml;ber einen Verkehrsunfall zu berichten
  als &uuml;ber eine Erstkommunion. Dar&uuml;ber mu&szlig; eine Lokalzeitung
  berichten, den die Gro&szlig;eltern und Eltern wollen am Montag in der Zeitung
  nicht nur ein Foto sehen, sondern etwas &uuml;ber die Feier lesen. Genau daf&uuml;r
  braucht sie Mitarbeiter und Mitarbeiterinnen, die das nicht nur fotografieren,
  sondern auch in Worte fassen k&ouml;nnen.</font></p>
                  <p><font face="Arial, Helvetica, sans-serif">Eckhard Bieger</font><br>
                  </p>
                  <p class=MsoNormal><font face="Arial, Helvetica, sans-serif"><a href="http://www.sankt-georgen.de/medien/index.htm">Vorbereitung
                        auf einen Medienberuf</a><strong> </strong></font></P>
                <p class=MsoNormal>&copy; <font size="2" face="Arial, Helvetica, sans-serif"><a href="http://www.kath.de/">www.kath.de</a></font></P></td>
                <td valign="top"><script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
google_ad_width = 160;
google_ad_height = 600;
google_ad_format = "160x600_as";
google_ad_type = "text_image";
//2007-09-05: Bildung und Erziehung
google_ad_channel = "3955521420";
google_ui_features = "rc:6";
//-->
            </script>
                    <script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
              </script>
                </td>
              </tr>
            </table>
            <p class=MsoNormal>&nbsp;</P>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
         <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td align="left" valign="top"><table width="216" border="0" cellpadding="0" cellspacing="0">
       <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10">
            <?php include("az.html"); ?>
          </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</body>
</html>
