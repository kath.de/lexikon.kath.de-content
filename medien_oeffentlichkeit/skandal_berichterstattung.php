<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Skandal Berichterstattung</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="kaltefleiter.css" title="fonts">
</head>
<body bgcolor="#ffffff" leftmargin="6" topmargin="6" marginwidth="6" marginheight="6">
<table width="100%" border="0" cellspacing="0" cellpadding="6">
  <tr>
    <td width="100" align="left" valign="top"><table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b>Medien und &Ouml;ffentlichkeit</b></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td>
            <?php include("logo.html"); ?>
          </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
    <td rowspan="2" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2">
            <h1> <font face="Arial, Helvetica, sans-serif"></font><font face="Arial, Helvetica, sans-serif">Skandal-Berichterstattung</font></h1>
          </td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="L12"> 
          
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td valign="top" class="L12"><p class=MsoNormal><font face="Arial, Helvetica, sans-serif"><strong>Skandale
                        interessieren die Leser</strong></font></P>
                  <p class=MsoNormal><font face="Arial, Helvetica, sans-serif">Skandale
                      geh&ouml;ren zu den Themen, &uuml;ber die &ouml;ffentlich
                      verhandelt wird. Erst die &Ouml;ffentlichkeit macht eine
                      Unterschlagung, einen sexuellen Mi&szlig;brauch, einen
                      Betrug zu einem Skandal. Die Medien gelten in der Einsch&auml;tzung
                      der meisten Institutionen deshalb als Gefahrenquelle, weil
                      sie mit der Berichterstattung &uuml;ber eine Unterschlagung,
                      sexuellen Mi&szlig;brauch u.a. das Interesse ihrer Leser,
                      Zuschauer und Zuh&ouml;rer gewinnen k&ouml;nnen. Deshalb
                      werden Reporter und Filmteams in der Regel mit Skepsis
                      empfangen, weil sie wahrscheinlich das f&uuml;r die Einrichtung
                      Negative herausbekommen und zum Inhalt des Berichtes machen
                      wollen. Es sind aus der Sicht der Verantwortlichen die
                      Medien, die f&uuml;r die Skandale verantwortlich sind.
                      Dabei &uuml;bersehen die Verantwortlichen, da&szlig; die
                      Kommunikationsstrategie der Institution in der Regel erst
                      den Skandal produziert. Daf&uuml;r mu&szlig; man eine Grudnvoraussetzugn
                      kennen, die die Arbeit der Journalisten bestimmt. Sie sehen
                      n&auml;mlich die Leitungskr&auml;fte einer Sozialeinrichtung,
                      einer Kirche, einer Bildungsinstitution als mit Macht ausgestattet,
                      weil diese &uuml;ber Personal, Befehlsstrukturen und Finanzmittel
                      verf&uuml;gen. Das hat zur Folge, da&szlig; Journalisten
                      die Personen, die mit Macht ausgestattet sind, erst einmal
                      kritisch und nicht wohlwollend sehen. Da die Leitungskr&auml;fte
                      auch die Kommunikation der Einrichtung nach au&szlig;en
                      bestimmen, h&auml;ngt es von ihnen ab, ob aus einem Fehlverhalten
                      ein Skandal wird.<br>
  Die Beobachtung zeigt, da&szlig; Skandale die notwendige Folge von Kommunikationsverweigerung
  sind. Die Verantwortlichen versuchen, den Vorfall zu vertuschen und wecken
  gerade dadurch den Jagdinstinkt der Journalisten, das herauszubekommen, was
  die Verantwortlichen nicht sagen. Und sie finden immer einen Mitarbeiter, eine
  Mitarbeiterin, die etwas wissen und es nicht mehr f&uuml;r sich behalten, wenn
  sie beobachten, da&szlig; Journalisten die Sache aufdecken. Denn wenn die Medien
  sich eines Mi&szlig;standes annehmen, f&uuml;hlt sich der einzelne gesch&uuml;tzt
  und ist daher bereit, den Journalisten zu informieren.<br>
  Institutionen und Verb&auml;nde, die sich gegen&uuml;ber den Medien abschotten,
  produzieren &uuml;ber kurz oder lang einen Skandal. Weil sie verhindern wollen,
  da&szlig; Journalisten Negatives berichten, gew&auml;hren sie diesen keinen
  Einblick, verweigern Interviews und lassen Kamerateams nicht drehen. Dadurch
  erwecken diese Einrichtungen mit Notwendigkeit den Eindruck, sie h&auml;tten
  etwas zu verbergen. Passiert dann etwas Negatives, was sich nicht mehr verheimlichen
  l&auml;&szlig;t, ist das f&uuml;r die Medienleute der Beweis, da&szlig; die
  Einrichtung sich deshalb so abweisend gegen&uuml;ber Interviewanfragen verhalten
  hatte, weil sie das Licht der &Ouml;ffentlichkeit scheuen mu&szlig;. Aus dem
  Negativen wird dann der Skandal konstruiert und zu einem noch gr&ouml;&szlig;eren
  Erfolg, wenn der Tatbestand erst einmal geleugnet und nur in Teilen offengelegt
  wird.<br>
  Fehltritte lassen sich nicht vermeiden, sie erhalten in der &Ouml;ffentlichkeit
  aber erst ein gr&ouml;&szlig;eres Gewicht, wenn <a href="oeffentlichkeitsarbeit_pr.php">&Ouml;ffentlichkeitsarbeit</a> nur
  z&ouml;gerlich oder abwehrend betrieben wird. Dann wird aus dem Fehlverhalten
  ein Skandal.</font></P>
                  <p><font face="Arial, Helvetica, sans-serif">Eckhard Bieger<br>
                  </font></p>
                <p class=MsoNormal>&copy; <font size="2" face="Arial, Helvetica, sans-serif">www.kath.de</font></P></td>
                <td valign="top"><script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
google_ad_width = 160;
google_ad_height = 600;
google_ad_format = "160x600_as";
google_ad_type = "text_image";
//2007-09-05: Bildung und Erziehung
google_ad_channel = "3955521420";
google_ui_features = "rc:6";
//-->
            </script>
                    <script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
              </script>
                </td>
              </tr>
            </table>
            <p class=MsoNormal>&nbsp;</P>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
         <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td align="left" valign="top"><table width="216" border="0" cellpadding="0" cellspacing="0">
       <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10">
            <?php include("az.html"); ?>
          </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</body>
</html>
