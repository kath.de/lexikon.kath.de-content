<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Format in den Medien</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="kaltefleiter.css" title="fonts">
</head>
<body bgcolor="#ffffff" leftmargin="6" topmargin="6" marginwidth="6" marginheight="6">
<table width="100%" border="0" cellspacing="0" cellpadding="6">
  <tr>
    <td width="100" align="left" valign="top"><table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b>Medien und &Ouml;ffentlichkeit</b></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td>
            <?php include("logo.html"); ?>
          </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
    <td rowspan="2" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2">
            <h1> <font face="Arial, Helvetica, sans-serif">Format in den Medien</font></h1>
          </td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="L12"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td valign="top" class="L12"><p class=MsoNormal><font face="Arial, Helvetica, sans-serif"><strong>Darstellungsformen
                        in den Medien</strong></font></P>
                  <p><font face="Arial, Helvetica, sans-serif">Es geht hier um
                      einen Begriff, der vor allem f&uuml;r das Fernsehen verwendet
                      wird, z.B. Nachrichten- oder Unterhaltungsformat. Was mit
                      Format gemeint ist, gibt es jedoch in jeder Kommunikation.
                      Z. B. ist der Jahresbericht bei eine Kommunikationsform,
                      die einen bestimmten Aufbau hat. Im Rahmen einer Mitgliederversammlung
                      geh&ouml;ren die Aussprache zum Jahresbericht, der Bericht
                      der Kassenpr&uuml;fer, Antr&auml;ge, zu denen es Wortmeldungen
                      und Abstimmungen gibt, zu dem geregelten Ablauf. Innerhalb
                      einer Mitgliederversammlung gibt es also bereits verschiedene
                      Formen, wie dieser <a href="oeffentlichkeit.php">Kommunikationsraum</a> gestaltet
                      wird. Gleiches gilt f&uuml;r eine Zeitung, ein H&ouml;rfunk-
                      oder Fernsehprogramm. Nachrichten, Kommentare, Reportagen,
                      Glossen und Werbung finden sich in den drei Medien, ebenfalls
                      Fortsetzungsgeschichten, im Fernsehen besonders ausgepr&auml;gt
                      als Serie; weiter Features, Wetterbericht, die Besprechung
                      von Neuerscheinungen, Theater- und Operninszenierungen,
                      von Kinofilmen, Konzerten bzw. Tontr&auml;gern. Diskussionsforen
                      gibt es im H&ouml;rfunk, im Fernsehen und in einzelnen
                      Zeitungen, weiter das Interview. Die Show ist im Fernsehen
                      eine h&auml;ufige Programmform. Die unterschiedlichen journalistischen
                      Formen und Genres fiktionaler Programme unterliegen jeweils
                      eigenen Baugesetzen. Diese Gesetze k&ouml;nnen als Kommunikationsregeln
                      beschrieben werden. Diese <a href="http://www.weiterbildung-live.de/59.html">Regeldimension</a> der
                      Kommunikation wurde in Unterscheidung zu ihrem Inhalts-
                      und Beziehungsaspekt als dritte Dimension herausgearbeitet,
                      weil sie eine bessere Bearbeitung von Kommunikationsst&ouml;rungen
                      erm&ouml;glicht. St&ouml;rungen sind n&auml;mlich nicht
                      nur durch Spannungen auf der Beziehungsebene bedingt, sondern
                      h&auml;ufig auch durch Versto&szlig; gegen Regeln, die
                      eine bestimmte Kommunikationsform steuern. Wenn ein Versammlungsleiter
                      keine Rednerliste f&uuml;hrt, wenn in der Berichterstattung
                      Nachricht und Kommentar vermischt werden, wenn in einer
                      Konferenz nicht sachbezogen argumentiert, sondern pers&ouml;nliche
                      Erlebnisse ausgetauscht werden, wird dies als St&ouml;rung
                      empfunden. Der Regelaspekt steuert die Baugesetze einer
                      Kommunikationsform wie auch die eines fiktionalen Genres.
                      Beschreibt der Regelaspekt die Struktur und den Aufbau
                      einer Kommunikationsform oder das Format, welches Printmedien,
                      Radio oder Fernsehen einsetzen, kommt auch in der durch
                      Medien vermittelten Kommunikation der Beziehungsaspekt
                      zum Tragen und d&uuml;rfte, wie in der Prim&auml;rkommunikation,
                      entscheidend sein. Bei Radio und Fernsehen suchen die Zuschauer
                      eine nahe Beziehung zu den redenden und handelnden Personen.
                      Regelm&auml;&szlig;ige Zuschauerinnen von Show-Sendungen
                      w&uuml;nschen sich Authentizit&auml;t und Emotionalit&auml;t.
                      Zuschauer von Gottesdienst&uuml;bertragungen m&ouml;chten
                      in das Geschehen hinein genommen werden. Sie unterscheiden
                      sich damit nicht von Zuschauern eines Films oder einer
                      Serie, die sich mit dem Helden identifizieren wollen, um
                      auf diese Weise die Handlung miterleben zu k&ouml;nnen.
                      Der Held mu&szlig; positiv gezeichnet sein, damit die Zuschauer
                      sich identifizieren k&ouml;nnen. Auch bei Diskussionssendungen,
                      Talkshows und Interviews will der Zuschauer eine Beziehung
                      zu der im Medium agierenden Person aufbauen, um sich dann
                      erst mit den Aussagen auseinanderzusetzen. Damit definiert
                      ein Medium die Beziehung zum Leser, H&ouml;rer, Zuschauer
                      nicht nur durch das Format des jeweiligen Beitrags, sondern
                      auch durch die Pr&auml;sentation.<br>
  W&auml;hrend das Format eine jeweils andere Beziehung des Lesers, Zuschauers
  zu den agierenden Personen erm&ouml;glicht und die Beziehungsebene eine wichtige
  Rolle bei der Medienrezeption spielt, ist die dritte Dimension in der Kommunikation,
  der Inhalt, f&uuml;r das Format kaum relevant. Zwischen Inhalten und Formaten
  gibt es offensichtlich keinen direkten Zusammenhang, d.h. da&szlig; bestimmte
  Inhalte nicht ausschlie&szlig;lich bestimmten Formaten zuzuordnen sind. So
  war das Thema &quot;Sexueller Mi&szlig;brauch von Kindern&quot; Gegenstand
  von Nachrichtensendungen, die &uuml;ber Gerichtsverhandlungen berichteten,
  es wurden dokumentarische Beitr&auml;ge gesendet, Diskussionsrunden veranstaltet,
  und schlie&szlig;lich tauchte das Thema in Serien auf, letztes notwendig zeitversetzt,
  da die Drehbuchentwicklung bei Serien Monate vor dem Ausstrahlungstermin abgeschlossen
  sein mu&szlig;. Die Drehbuchautoren und die Redakteure, die die fiktionalen
  Programme verantworten, k&ouml;nnen sich bei der Darstellung des Problems,
  des Konfliktes bereits auf einen Meinungstrend st&uuml;tzen, der zumindest
  mehrheitsf&auml;hig ist. Da vor allem Unterhaltungsprogramme wie die Serien
  auf hohe Einschaltquoten zielen und deshalb den Konsens bei den Zuschauern
  nicht verletzen d&uuml;rfen, sind diese Programme immer entsprechend den herrschenden
  Meinungstrends konstruiert. <br>
                <br>
  Eckhard Bieger</font><br>
                  </p>
                <p class=MsoNormal>&copy;<font size="2" face="Arial, Helvetica, sans-serif"> <a href="http://www.kath.de/">www.kath.de</a></font></P></td>
                <td valign="top"><script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
google_ad_width = 160;
google_ad_height = 600;
google_ad_format = "160x600_as";
google_ad_type = "text_image";
//2007-09-05: Bildung und Erziehung
google_ad_channel = "3955521420";
google_ui_features = "rc:6";
//-->
            </script>
                    <script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
              </script>
                </td>
              </tr>
            </table>
            <p class=MsoNormal>&nbsp;</P>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
         <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td align="left" valign="top"><table width="216" border="0" cellpadding="0" cellspacing="0">
       <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10">
            <?php include("az.html"); ?>
          </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</body>
</html>
