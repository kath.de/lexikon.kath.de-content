<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Agenda setting</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="kaltefleiter.css" title="fonts">
</head>
<body bgcolor="#ffffff" leftmargin="6" topmargin="6" marginwidth="6" marginheight="6">
<table width="100%" border="0" cellspacing="0" cellpadding="6">
  <tr>
    <td width="100" align="left" valign="top"><table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b>Medien und &Ouml;ffentlichkeit</b></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td>
            <?php include("logo.html"); ?>
          </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
    <td rowspan="2" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><h1><font face="Arial, Helvetica, sans-serif">Agenda
                Setting</font></h1>
          </td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="L12"> 
           
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td valign="top" class="L12"><p class=MsoNormal><font face="Arial, Helvetica, sans-serif"><strong>Ein
                        Thema auf die Tagesordnung der &Ouml;ffentlichkeit setzen</strong></font></P>
                  <p><font face="Arial, Helvetica, sans-serif">Im kommunikativen
                      Raum der &Ouml;ffentlichkeit, in dem die Gesellschaft insgesamt
                      ihre Fragen diskutiert, die Machtverteilung aushandelt
                      und Entscheidungen vorbereitet, k&ouml;nnen immer nur einige
                      Themen gleichzeitig behandelt werden, denn alle Themen
                      k&ouml;nnen nicht auf einmal zur Diskussion stehen. Damit
                      ein Thema auf die Tagesordnung der &Ouml;ffentlichkeit
                      gelangt, mu&szlig; es ausgew&auml;hlt werden, oder das
                      Thema mu&szlig; von au&szlig;en aufgezwungen sein, wie
                      z.B. ein Ungl&uuml;cksfall, ein Attentat, ein Umsturz oder
                      ein sich als unausweichlich herausstellendes Problem, mit
                      dem sich der Verband, die Kommune, die gesamte Gesellschaft
                      auseinandersetzen m&uuml;ssen. Manche Themen kommen automatisch
                      auf die Tagesordnung. Es gibt in einem bestimmten Rhythmus
                      Wahlen, der Etat mu&szlig; verabschiedet, die Neujahrsansprache
                      gehalten werden. Abgesehen von den Themen, die auf jeden
                      Fall zur Sprache kommen, h&auml;ngt es von den Agenten
                      ab, die sich in der &Ouml;ffentlichkeit zu Wort melden,
                      welches Thema auf die Tagesordnung kommt. Zweifellos haben
                      die Medien dabei eine wichtige Rolle, die m&ouml;glicherweise
                      aber &uuml;bersch&auml;tzt wird. So steht ein gro&szlig;er
                      Teil der Nachrichten bereits fest, weil z.B. eine Abstimmung
                      im Parlament ansteht, ein Partei- oder Gewerkschaftstag
                      stattfindet, &uuml;ber eine Wahl oder einen Staatsbesuch
                      zu berichten ist, die Zahl der Arbeitslosen f&uuml;r den
                      vergangenen Monat bekanntgegeben wird, die Leser, H&ouml;rer,
                      Zuschauer Informationen &uuml;ber die Staus an den Ferienwochenende, &uuml;ber
                      Sportereignisse und das Wetter von morgen erwarten.<br>
  Auf die Tagesordnung kann auch ein Thema kommen, wenn es Reaktionen in der &Ouml;ffentlichkeit
  gibt - durch Leserbriefe, Anrufe oder Stammtischgespr&auml;che, die Journalisten
  von einigen Zeitungen sogar systematisch verfolgen. <br>
  Die zunehmende Abh&auml;ngigkeit des Fernsehens von der Fernbedienung der Zuschauer
  sowie der Zeitungen und Zeitschriften von dem Kaufentscheid der Leser am Kiosk
  d&uuml;rfte zur Folge haben, da&szlig; die Regierung, Parteien und Verb&auml;nde
  weniger Einflu&szlig; darauf haben, was auf die Tagesordnung der <a href="oeffentlichkeit.php">&Ouml;ffentlichkeit</a> kommt.
  Die Medien sind gezwungen, sich an den Interessen ihrer Nutzer auszurichten.
  Das hat u.a. zur Folge, da&szlig; Ereignissen durch die Berichterstattung ein
  spektakul&auml;rer Charakter verliehen wird, um das Interesse der Leser bzw.
  Zuschauer zu gewinnen.<br>
  Es gibt Gruppen in der Gesellschaft, die das Durchhalteverm&ouml;gen haben, &uuml;ber
  einen langen Zeitraum die Behandlung eines Themas zu fordern. So hat die &Ouml;kologiebewegung
  durch Demonstrationen, die Ver&ouml;ffentlichung von Untersuchungen und direkte
  Kommunikation erreicht, da&szlig; der Umweltschutz auf die Tagesordnung gekommen
  ist. </font></p>
                <p>&copy; <a href="http://www.kath.de/"><font size="2" face="Arial, Helvetica, sans-serif">www.kath.de</font></a></p></td>
                <td valign="top"><script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
google_ad_width = 160;
google_ad_height = 600;
google_ad_format = "160x600_as";
google_ad_type = "text_image";
//2007-09-05: Bildung und Erziehung
google_ad_channel = "3955521420";
google_ui_features = "rc:6";
//-->
            </script>
                    <script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
              </script>
                </td>
              </tr>
            </table>
			<p class=MsoNormal><br>
	        </P>
		  <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
         <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td align="left" valign="top"><table width="216" border="0" cellpadding="0" cellspacing="0">
       <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10">
            <?php include("az.html"); ?>
          </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</body>
</html>
