<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <title>Radio Vatikan</title>


  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

  <link rel="stylesheet" type="text/css" href="kaltefleiter.css" title="fonts">

</head>


<body leftmargin="6" topmargin="6" style="background-color: rgb(255, 255, 255);" marginheight="6" marginwidth="6">

<table border="0" cellpadding="6" cellspacing="0" width="100%">

  <tbody>

    <tr>

      <td align="left" valign="top" width="100">
      <table border="0" cellpadding="0" cellspacing="0" width="216">

        <tbody>

          <tr align="left" valign="top">

            <td width="8"><img src="boxtopleftcorner.gif" alt="" height="8" width="8"></td>

            <td background="boxtop.gif" width="200"><img src="boxtop.gif" alt="" height="8" width="8"></td>

            <td width="8"><img src="boxtoprightcorner.gif" alt="" height="8" width="8"></td>

          </tr>

          <tr align="left" valign="top">

            <td background="boxtopleft.gif"><img src="boxtopleft.gif" alt="" height="8" width="8"></td>

            <td bgcolor="#e2e2e2"><b>Medien und
&Ouml;ffentlichkeit</b></td>

            <td background="boxtopright.gif"><img src="boxtopright.gif" alt="" height="8" width="8"></td>

          </tr>

          <tr align="left" valign="top">

            <td><img src="boxdividerleft.gif" alt="" height="13" width="8"></td>

            <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" height="13" width="8"></td>

            <td><img src="boxdividerright.gif" alt="" height="13" width="8"></td>

          </tr>

          <tr align="left" valign="top">

            <td background="boxleft.gif"><img src="boxleft.gif" alt="" height="8" width="8"></td>

            <td><?php include("logo.html"); ?> </td>

            <td background="boxright.gif"><img src="boxright.gif" alt="" height="8" width="8"></td>

          </tr>

          <tr align="left" valign="top">

            <td><img src="boxbottomleft.gif" alt="" height="8" width="8"></td>

            <td background="boxbottom.gif"><img src="boxbottom.gif" alt="" height="8" width="8"></td>

            <td><img src="boxbottomright.gif" alt="" height="8" width="8"></td>

          </tr>

        </tbody>
      </table>

      </td>

      <td rowspan="2" valign="top">
      <table border="0" cellpadding="0" cellspacing="0" width="100%">

        <tbody>

          <tr align="left" valign="top">

            <td width="8"><img src="boxtopleftcorner.gif" alt="" height="8" width="8"></td>

            <td background="boxtop.gif"><img src="boxtop.gif" alt="" height="8" width="8"></td>

            <td width="8"><img src="boxtoprightcorner.gif" alt="" height="8" width="8"></td>

          </tr>

          <tr align="left" valign="top">

            <td background="boxtopleft.gif"><img src="boxtopleft.gif" alt="" height="8" width="8"></td>

            <td bgcolor="#e2e2e2">
            <h1> <span style="font-family: Arial,Helvetica,sans-serif;">Radio
Vatikan</span></h1>

            </td>

            <td background="boxtopright.gif"><img src="boxtopright.gif" alt="" height="8" width="8"></td>

          </tr>

          <tr align="left" valign="top">

            <td><img src="boxdividerleft.gif" alt="" height="13" width="8"></td>

            <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" height="13" width="8"></td>

            <td><img src="boxdividerright.gif" alt="" height="13" width="8"></td>

          </tr>

          <tr align="left" valign="top">

            <td background="boxleft.gif"><img src="boxleft.gif" alt="" height="8" width="8"></td>

            <td class="L12">
            <table border="0" cellpadding="0" cellspacing="0" width="100%">

              <tbody>

                <tr>

                  <td style="font-family: Helvetica,Arial,sans-serif;" class="L12" valign="top">
                  <p class="MsoNormal" style="line-height: normal;">&bdquo;Die Stimme des
Papstes und der
Weltkirche&ldquo;</p>

                  <p class="MsoNormal" style="line-height: normal;"><b style=""><o:p>&nbsp;</o:p>Zur
Geschichte<o:p></o:p></b><br>

Radio Vatikan wurde 1931 unter
Papst Pius XI. ins Leben gerufen. Der italienische Radiopionier
Guglielmo
Marconistand f&uuml;r die Neugr&uuml;ndung Pate. Dieser
internationale H&ouml;rfunksenders
wurde dem Jesuitenorden &uuml;bertragen, der Radio Vatikan heute
noch leitet. Ziel
war es, Ereignisse rund um Papst und Kurie einer breiten
&Ouml;ffentlichkeit
zug&auml;nglich zu machen. Ebenso pr&auml;gten weltkirchliche
Themen von Anbeginn an das
Sendeprogramm des Papstsenders.<br>

W&auml;hrend des Zweiten Weltkrieges
wurden die Friedensbotschaften Papst Pius XII. &uuml;ber Radio
Vatikan in alle Welt
&uuml;bertragen. Noch in den Nachkriegsjahren sendete Radio Vatikan
Vermisstenmeldungen
rund um den Globus.<br>

Zur Zeit der Mauer des Schweigens
im Kalten Krieg erweiterte Radio Vatikan sein Sendeprogramm um
zahlreiche
osteurop&auml;ische Sprachen.<br>

Eine Rarit&auml;t unter den
Mittschnitten von Radio Vatikan stellen die Aufzeichnungen der
Beratungen des Zweiten
Vatikanischen Konzils (1962-1965) dar, die bis heute im vatikanischen
Archiv
ruhen.<br>

Mit der Visite Papst Paul VI. im
Heiligen Land begann 1964 die &Auml;ra der reisenden
P&auml;pste. 1979 t&auml;tigte Papst
Johannes Paul II. seine erste Auslandsreise nach Mexiko. Radio Vatikan
begleitete ihn auf seine mehr als 100 Auslandsbesuche.</p>

                  <p class="MsoNormal" style="line-height: normal;"><b style="">Radio
Vatikan heute<br>

                  </b>Aktuell arbeiten fast 400
H&ouml;rfunkredakteure, Techniker,
Verwaltungsangestellte plus Hauspersonal festangestellt im
Redaktionszentrum an
der Piazza Pia 3 in unmittelbarer N&auml;he zum Vatikan.Der
Gro&szlig;teil der
Besch&auml;ftigten von Radio Vatikan sind Laien. Das Haus ist in 40
Abteilungen
unterteilt, die angestellten Redakteure stammen aus 60 verschiedenen
Nationen. Radio
Vatikan sendet in 45 Sprachen.</p>

                  <p class="MsoNormal" style="line-height: normal;"><b style="">Die
deutschsprachige Abteilung <br>

                  </b>F&uuml;r das deutschsprachige Programm
arbeiten f&uuml;nf festangestellte
Redakteureaus Deutschland, &Ouml;sterreich und der Schweiz. Hinzu
kommen freie
Mitarbeiter in Rom, Auslandskorrespondenten in freier Mitarbeit, sowie
ein
Praktikant.<b style=""><o:p></o:p></b></p>

                  <p class="MsoNormal" style="line-height: normal;"><b style="">Das
Sendeprogramm<br>

                  </b>Insgesamt produziert Radio Vatikan 60
Sendestunden pro Tag, die &uuml;ber
Mittel- und Kurzwelle weltweit ausgestrahlt werden. Auf 30
verschiedenen
Sprachen ist Radio Vatikan als Podcast abrufbar.<b style=""><o:p></o:p></b><br>

In der deutschsprachigen
Abteilung werden t&auml;glich zwei Sendungen produziert. Dabei
handelt es sich um
die 15-min&uuml;tige Nachrichtensendung &bdquo;Treffpunkt
Weltkirche&ldquo;, die um 16 Uhr
ausgestrahlt wird und eine 19-min&uuml;tige Magazinsendung zur
Abendzeit mit
wechselnden Schwerpunktbereichen. Die aktuellen Inhalte der
Nachrichtensendung
werden t&auml;glich in Form eines kostenfreien Newsletters derzeit
an rund <span style="">&nbsp;</span>12.000
Abonnenten verschickt. Beide
Sendeformate sind 24 Stunden als Podcast abrufbar.<span style="">&nbsp;
                  </span>Die einzelnen Beitr&auml;ge
k&ouml;nnen zudem &uuml;ber
Twitter verfolgt werden.<br>

Dar&uuml;ber hinaus kommentieren die
Mitarbeiter von Radio Vatikan die Live&uuml;bertragungen von
Gro&szlig;ereignissen im
Vatikan, wie Generalaudienzen, Papstmessen oder anderen
Feierlichkeiten.
Ausschnitte hieraus werden regelm&auml;&szlig;ig mit deutscher
&Uuml;berstimme auf Youtube
bereitgestellt.<br>

Einmal in der Woche stellt die Redaktion
Nachrichten auf Latein zur Verf&uuml;gung.</p>

                  <p class="MsoNormal" style="line-height: normal;"><b style="">Die
Sendetechnik<br>

                  </b>Die Antenne f&uuml;r die in Kurzwelle
ausgestrahlten Programme wurdenurspr&uuml;nglich
in den vatikanischen G&auml;rten aufgerichtet. Seit 1957 befindet
sich ein Gro&szlig;teil
der Sendevorrichtungen auf vatikanischem Gel&auml;ndebei Santa
Maria di Galeria
au&szlig;erhalb von Rom. Auf dem n&ouml;rdlich der Stadt
gelegenenGrundst&uuml;ck stehen 36
Antennen und 20 Sendemasten f&uuml;r die weltweite Erreichbarkeit
der Programme.</p>

                  <p class="MsoNormal" style="line-height: normal;"><b style="">Die
Finanzierung<br>

                  </b>Radio Vatikan wird als Einrichtung des Hl.
Stuhles zum Gro&szlig;teil vom Vatikan
selbst getragen. J&auml;hrlich bedeutet das eine Belastung von 20
Millionen Euro. Die
deutschsprachige Abteilung bestreitet einen Teil ihrer Auslagen durch
F&ouml;rdermittel des &bdquo;Freunde von Radio Vatikan
e.V.&ldquo; und Spendengelder sowie eigene
CD-Produktionen.</p>

                  <p class="MsoNormal" style="line-height: normal;"><b style="">Die
Homepage<br>

                  </b>Die offizielle Homepage von Radio Vatikan
ist unter <a href="http://www.radiovaticana.org/index.html">http://www.radiovaticana.org/index.html</a>
abrufbar. </p>

                  <p class="MsoNormal" style="line-height: normal;">Die deutschsprachige Redaktion
ist unter <a href="http://www.oecumene.radiovaticana.org/ted/index.asp">http://www.oecumene.radiovaticana.org/ted/index.asp</a>
zu finden.</p>

                  <p class="MsoNormal" style="line-height: normal;"><o:p>&nbsp;</o:p></p>

                  <p class="MsoNormal" style="line-height: normal;">Stand: Januar 2011</p>

                  <p class="MsoNormal" style="line-height: normal;"><a name="_GoBack"></a>Veronica
Pohl</p>

                  <p><br>

                  <br>

 </p>

                  <p class="MsoNormal">&copy; <font size="2">www.kath.de</font></p>

                  </td>

                  <td valign="top">
                  <script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
google_ad_width = 160;
google_ad_height = 600;
google_ad_format = "160x600_as";
google_ad_type = "text_image";
//2007-09-05: Bildung und Erziehung
google_ad_channel = "3955521420";
google_ui_features = "rc:6";
//-->
                  </script>
                  <script type="text/javascript" src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
                  </script>
                  </td>

                </tr>

              </tbody>
            </table>

            <p class="MsoNormal">&nbsp;</p>

            </td>

            <td background="boxright.gif"><img src="boxright.gif" alt="" height="8" width="8"></td>

          </tr>

          <tr align="left" valign="top">

            <td><img src="boxbottomleft.gif" alt="" height="8" width="8"></td>

            <td background="boxbottom.gif"><img src="boxbottom.gif" alt="" height="8" width="8"></td>

            <td><img src="boxbottomright.gif" alt="" height="8" width="8"></td>

          </tr>

        </tbody>
      </table>

      </td>

    </tr>

    <tr>

      <td align="left" valign="top">
      <table border="0" cellpadding="0" cellspacing="0" width="216">

        <tbody>

          <tr align="left" valign="top">

            <td width="8"><img src="boxtopleftcorner.gif" alt="" height="8" width="8"></td>

            <td background="boxtop.gif" width="200"><img src="boxtop.gif" alt="" height="8" width="8"></td>

            <td width="8"><img src="boxtoprightcorner.gif" alt="" height="8" width="8"></td>

          </tr>

          <tr align="left" valign="top">

            <td background="boxtopleft.gif"><img src="boxtopleft.gif" alt="" height="8" width="8"></td>

            <td bgcolor="#e2e2e2"><strong>Begriff
anklicken</strong></td>

            <td background="boxtopright.gif"><img src="boxtopright.gif" alt="" height="8" width="8"></td>

          </tr>

          <tr align="left" valign="top">

            <td><img src="boxdividerleft.gif" alt="" height="13" width="8"></td>

            <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" height="13" width="8"></td>

            <td><img src="boxdividerright.gif" alt="" height="13" width="8"></td>

          </tr>

          <tr align="left" valign="top">

            <td background="boxleft.gif"><img src="boxleft.gif" alt="" height="8" width="8"></td>

            <td class="V10"><?php include("az.html"); ?> </td>

            <td background="boxright.gif"><img src="boxright.gif" alt="" height="8" width="8"></td>

          </tr>

          <tr align="left" valign="top">

            <td><img src="boxbottomleft.gif" alt="" height="8" width="8"></td>

            <td background="boxbottom.gif"><img src="boxbottom.gif" alt="" height="8" width="8"></td>

            <td><img src="boxbottomright.gif" alt="" height="8" width="8"></td>

          </tr>

        </tbody>
      </table>

      </td>

    </tr>

  </tbody>
</table>

</body>
</html>
