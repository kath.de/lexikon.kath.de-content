<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Periodizit&auml;t</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="kaltefleiter.css" title="fonts">
</head>
<body bgcolor="#ffffff" leftmargin="6" topmargin="6" marginwidth="6" marginheight="6">
<table width="100%" border="0" cellspacing="0" cellpadding="6">
  <tr>
    <td width="100" align="left" valign="top"><table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b>Medien und &Ouml;ffentlichkeit</b></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td>
            <?php include("logo.html"); ?>
          </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
    <td rowspan="2" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2">
            <h1> <font face="Arial, Helvetica, sans-serif"></font><font face="Arial, Helvetica, sans-serif">Periodizit&auml;t</font></h1>
          </td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="L12"> 
       
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td valign="top" class="L12"><p class=MsoNormal><font face="Arial, Helvetica, sans-serif"><strong>Regelm&auml;&szlig;iges
                        Erscheinen</strong></font></P>
                  <p><font face="Arial, Helvetica, sans-serif">Nachrichten strukturieren
                      ein H&ouml;rfunk- und Fernsehprogramm. Eine Zeitung wird
                      meistens in der Nacht gedruckt und am fr&uuml;hen Morgen
                      in die Briefk&auml;sten der Abonnenten geworfen. Die Medien
                      folgen einem Zeitrhythmus, so da&szlig; die Nutzer sich
                      an die Zeit f&uuml;r die Nachrichten, die Zeitungslekt&uuml;re
                      beim Fr&uuml;hst&uuml;ck oder in der Bahn gew&ouml;hnen
                      k&ouml;nnen. Periodizit&auml;t wird so erreicht und erm&ouml;glicht
                      damit <a href="oeffentlichkeit.php">&Ouml;ffentlichkeit</a>,
                      denn ohne Kommunikationsleistungen entsteht keine &Ouml;ffentlichkeit.
                      Wenn Medien nicht in einem bestimmten Zeitintervall erscheinen
                      bzw. senden, also sich nicht an bestimmte Perioden halten,
                      dann bricht das &ouml;ffentliche Gespr&auml;ch zusammen
                      oder verlagert sich wieder ganz in die Stammtische und
                      die &ouml;rtlichen Versammlungen. Periodizit&auml;t erm&ouml;glicht <a href="oeffentlichkeit_prozess.php">Meinungsbildungsprozesse</a>.
                      Denn auf die Stellungnahme z.B. der Gewerkschaft k&ouml;nnen
                      die Arbeitgeber in der n&auml;chsten Zeitungsausgabe reagieren
                      und darauf wieder die Gewerkschaften. Durch die Kommunikation,
                      in der sich die Sprecher einer gesellschaftlichen Gruppe
                      auf &Auml;u&szlig;erungen der Sprecher anderer Gruppen
                      beziehen, entstehen Meinungsbildugnsprozesse. </font></p>
                  <p><font face="Arial, Helvetica, sans-serif">Eckhard Bieger<br>
                  </font></p>
                  <p class=MsoNormal>&nbsp;</P>
                  <p class=MsoNormal>&nbsp;</P>
                  <p class=MsoNormal>&nbsp;</P>
                  <p class=MsoNormal>&nbsp;</P>
                <p class=MsoNormal>&copy; <font size="2" face="Arial, Helvetica, sans-serif">www.kath.de</font></P></td>
                <td valign="top"><script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
google_ad_width = 160;
google_ad_height = 600;
google_ad_format = "160x600_as";
google_ad_type = "text_image";
//2007-09-05: Bildung und Erziehung
google_ad_channel = "3955521420";
google_ui_features = "rc:6";
//-->
            </script>
                    <script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
              </script>
                </td>
              </tr>
            </table>
            <p class=MsoNormal>&nbsp;</P>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
         <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td align="left" valign="top"><table width="216" border="0" cellpadding="0" cellspacing="0">
       <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10">
            <?php include("az.html"); ?>
          </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</body>
</html>
