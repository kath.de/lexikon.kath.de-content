<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Medienpastoral</title>
<meta name="title" content="Medien&Öffentlichkeit">
<meta name="author" content="kath.de">
<meta name="publisher" content="kath.de">
<meta name="copyright" content="kath.de">
<meta name="description" content="Stichworte über Medien und Öffentlichkeitsarbeit">
<meta name="abstract" content="Stichworte über Medien und Öffentlichkeitsarbeit">
<meta http-equiv="content-language" content="de">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="date" content="2006-00-02">
<meta name="robots" content="index,follow">
<meta name="revisit-after" content="30 days">
<meta name="revisit" content="after 30 days">
<meta name="DC.Title" content="Medien&Öffentlichkeit">
<meta name="DC.Creator" content="kath.de">
<meta name="DC.Contributor" content="N.N.">
<meta name="DC.Rights" content="kath.de">
<meta name="DC.Publisher" content="kath.de">
<meta name="DC.Date" content="2006-00-02">
<meta name="DC.Description" content="Stichworte über Medien und Öffentlichkeitsarbeit">
<meta name="DC.Language" content="de">
<meta name="DC.Type" content="Text">
<meta name="DC.Format" content="text/html">
<meta name="DC.Identifier" content="http://www.kath.de/kurs/medien_oeffentlichkeit/">
<meta name="keywords" lang="de" content="Medien; Seelsorge, Mediengesellschaft, Kirche">

<link rel="stylesheet" type="text/css" href="kaltefleiter.css" title="fonts">
</head>
<body bgcolor="#ffffff" leftmargin="6" topmargin="6" marginwidth="6" marginheight="6">
<table width="100%" border="0" cellspacing="0" cellpadding="6">
  <tr>
    <td width="100" align="left" valign="top"><table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b>Medien und &Ouml;ffentlichkeit</b></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td>
            <?php include("logo.html"); ?>
          </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
    <td rowspan="2" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2">
            <h1> <font face="Arial, Helvetica, sans-serif">Medienpastoral</font></h1>
          </td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="L12"> 
           <font face="Arial, Helvetica, sans-serif"><strong>              </strong></font></P>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td valign="top" class="L12"><p class=MsoNormal><font face="Arial, Helvetica, sans-serif"><strong>Medien
                        als Bestandteil der Seeslorge</strong> </font></P>
                  <p class=MsoNormal><font face="Arial, Helvetica, sans-serif">Pastoral
                      ist die Theorie und Konzeption der Seelsorge, das Wort
                      leitet sich vom lateinischen Hirtenbegriff her. Warum Medien
                      integraler Bestandteil einer Pastoral sein m&uuml;ssen,
                      erl&auml;utert Ludger Verst: <br>
  Die Probleme der Kirche im Umgang mit kritischer &Ouml;ffentlichkeit, die Umst&auml;ndlichkeit
  ihrer Auftritte und ihre mangelhafte publizistische Pr&auml;senz auf der mittleren
  und unteren Organisationsebene d&uuml;rften nicht zuletzt durch ein grunds&auml;tzliches
  pastoralplanerisches Defizit verursacht sein: durch das Fehlen einer in der
  Pastoral verankerten Medienpraxis. Medien- und &Ouml;ffentlichkeitsarbeit erscheinen
  insgesamt als strategischer Annex zum Eigentlichen der Kirche, ihrer Seelsorge.
  Die Nachrangigkeit und weitgehende Beliebigkeit des publizistischen Engagements
  erkl&auml;rt sich dadurch, dass dieses Engagement selbst (noch) nicht als Teil
  eines umfassenden Verst&auml;ndnisses von Pastoral begriffen wird. Die Kommunikation
  des Evangeliums Jesu Christi, Beratung und Katechese sind aber in einer Medien
  rezipierenden, nachchristlichen Gesellschaft nur so erfolgreich, wie sie selbst
  zwar nicht ausschlie&szlig;lich, aber doch selbstverst&auml;ndlich durch Medien
  geschehen und in den gewachsenen Lebensr&auml;umen einer Stadt, eines Bezirks
  oder einer Region, also vornehmlich in lokalen Medien, pr&auml;sent sind. <br>
  Medienpastoral ist Evangelisierung mit Medien. Evangelisierung ist in der kirchlichen
  Theorie und Praxis inzwischen ein Schl&uuml;sselwort geworden. F&uuml;r den
  katholischen Bereich bezeichnet das Apostolische Schreiben &#8222;Evangelii
  nuntiandi&#8220; (1975) damit die Dynamik, &#8222;die Frohbotschaft in alle
  Bereiche der Menschheit zu tragen und sie durch deren Einfluss von innen her
  umzuwandeln&#8220; (Nr. 18). <br>
  Medienpastoral in mobiler Gesellschaft bedeutet daher: Heraus aus dem Muster
  einer &uuml;berraschungssicheren Kirche der Insider und hinein in die Passage,
  dorthin, wo gerade Publikum ist. Im urspr&uuml;nglichen Sinn &#8222;auf Sendung
  sein&#8220;, so dass aus der Bot-schaft Kund-schaft werden kann: in den Stra&szlig;encaf&eacute;s
  und Wohnsilos, vor dem City-Center, in der U-Bahn, &#8222;auf dem Marktplatz
  mit den zuf&auml;llig Anwesenden&#8220; (Apostelgeschichte 17,17) &#8211; getreu
  dem Motto: &#8222;Kommt her und seht, dann geht und sagt!&#8220;: Passantenkirche
  sozusagen. <br>
                <a href="http://www.kath.de/aktuell/serien/medienpastoral/medienpastoral1.htm">Mehr
                Informationen</a></font></P>
                  <p></p>
                  <p class=MsoNormal> <font face="Arial, Helvetica, sans-serif"><strong> </strong></font></P>
                <p class=MsoNormal>&copy;<a href="http://www.kath.de/"> <font size="2" face="Arial, Helvetica, sans-serif">www.kath.de</font></a></P></td>
                <td valign="top"><script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
google_ad_width = 160;
google_ad_height = 600;
google_ad_format = "160x600_as";
google_ad_type = "text_image";
//2007-09-05: Bildung und Erziehung
google_ad_channel = "3955521420";
google_ui_features = "rc:6";
//-->
            </script>
                    <script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
              </script>
                </td>
              </tr>
            </table>
            <p class=MsoNormal>&nbsp;</P>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
         <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td align="left" valign="top"><table width="216" border="0" cellpadding="0" cellspacing="0">
       <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10">
            <?php include("az.html"); ?>
          </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</body>
</html>
