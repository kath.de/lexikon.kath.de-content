<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Kommunikationsmuster &#8211; erm&ouml;glicht durch das Internet - www.kath.de</title>
<meta name="title" content="Medien&Öffentlichkeit">
<meta name="author" content="kath.de">
<meta name="publisher" content="kath.de">
<meta name="copyright" content="kath.de">
<meta name="description" content="Stichworte über Medien und Öffentlichkeitsarbeit">
<meta name="abstract" content="Stichworte über Medien und Öffentlichkeitsarbeit">
<meta http-equiv="content-language" content="de">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="date" content="2006-00-02">
<meta name="robots" content="index,follow">
<meta name="revisit-after" content="30 days">
<meta name="revisit" content="after 30 days">
<meta name="DC.Title" content="Medien&Öffentlichkeit">
<meta name="DC.Creator" content="kath.de">
<meta name="DC.Contributor" content="N.N.">
<meta name="DC.Rights" content="kath.de">
<meta name="DC.Publisher" content="kath.de">
<meta name="DC.Date" content="2006-00-02">
<meta name="DC.Description" content="Stichworte über Medien und Öffentlichkeitsarbeit">
<meta name="DC.Language" content="de">
<meta name="DC.Type" content="Text">
<meta name="DC.Format" content="text/html">
<meta name="DC.Identifier" content="http://www.kath.de/kurs/medien_oeffentlichkeit/">
<meta name="keywords" lang="de" content="Theologiestudium, Medien, Kommuniaktionsberuf, Drehbücher, Lokalredaktion, Journalismus">

<link rel="stylesheet" type="text/css" href="kaltefleiter.css" title="fonts">
</head>
<body bgcolor="#ffffff" leftmargin="6" topmargin="6" marginwidth="6" marginheight="6">
<table width="100%" border="0" cellspacing="0" cellpadding="6">
  <tr>
    <td width="100" align="left" valign="top"><table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b>Medien und &Ouml;ffentlichkeit</b></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td>
            <?php include("logo.html"); ?>
          </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
    <td rowspan="2" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2">
            <h1> <font face="Arial, Helvetica, sans-serif">Medien&amp;&Ouml;ffentlichkeit</font></h1>
          </td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="L12"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td valign="top" class="L12"><p class="text"><strong><font face="Arial, Helvetica, sans-serif">Kommunikationsmuster &#8211; erm&ouml;glicht
                      durch das Internet</font></strong></p>
                  <p class="text"><font face="Arial, Helvetica, sans-serif">Jede
                      Gesellschaft hat Medien, die bestimmte Kommunikationsmuster
                      bedienen. Zum Autoverkehr geh&ouml;rt
                    der Verkehrsfunk, zum Gesch&auml;ftsleben das Telefon. Die
                    Zeitung bl&auml;ttert man morgens durch, um sich einen &Uuml;berblick &uuml;ber
                    das zu verschaffen, was sich ver&auml;ndert hat, was passiert
                    ist, denn die Zeitung druckt, was sich &#8222;gezeitigt&#8220; hat.
                    Da die Medien eng mit den allt&auml;glichen Abl&auml;ufen
                    verwoben sind, ist den Nutzern kaum bewusst, wie die Medien
                    Einfluss aus&uuml;ben. Da das Internet jeden seiner t&auml;glich
                    wachsenden Inhalte, ob Textseiten, Videos, Verzeichnisse,
                    Emailadressen u.a. auf Abruf zur Verf&uuml;gung stellt, haben
                    die Nutzer das Gef&uuml;hl, dass nicht das Internet sie steuert,
                    sondern sie steuern sich selbst durch die digitalen Welten.
                    Traute man fr&uuml;her der Zeitung, dem Kino und dann dem
                    Fernsehen bei der Einf&uuml;hrung des jeweiligen Mediums
                    ein hohes Einflusspotential zu, so dem Internet kaum. Man
                    beklagt allenfalls, dass die Surfer soviel Zeit mit dem Medium
                    verlieren. Erstaunlich ist nur, dass sich keines der bisher
                    genutzten Medien dem Einfluss des Mediums entziehen kann.<br>
                    Die Zeitung muss nicht nur eine Onlineausgabe ins Netz stellen,
                    sie verliert einen gro&szlig;en Teil ihrer Einnahmen, weil
                    die Rubrikanzeigen auf Internetplattformen gewandert sind.<br>
  Das Fernsehen kann sich von seinem Programmschema verabschieden und wird zu
  einem Abrufmedium. Wie die Rubrikanzeigen der Zeitungen wird das Internet auch
  die Unterbrechung der Sendungen durch Fernsehspots au&szlig;er Kraft setzen,
  weil diese in einem Abrufmedium nicht mehr funktionieren.<br>
  Die Nutzer sind davon nur wenig betroffen, denn sie bekommen den Service, den
  die Zeitung oder das Fernsehen bisher geboten haben, durch das Internet noch
  besser zugeliefert. Sie brauchen nur einen Computer mit einem Telefonanschluss.
  Je mehr sich die Mediennutzung der bisherigen Zeitung, des bisherigen Fernsehens,
  der Werbung, des Musikkonsums wie auch des Briefes und des Telefons in das
  Internet verlagert, desto mehr pr&auml;gt das neue Medium die Kommunikationsmuster
  der Menschen um. Folgende Ver&auml;nderungen lassen sich beobachten:</font></p>
                  <p class="text"><font face="Arial, Helvetica, sans-serif"><strong>Orientierung &uuml;ber Ereignisse und
                      Ver&auml;nderungen</strong><br>
                    <br>
                    Je Internet-affiner die Nutzer, desto mehr geht die Nutzung
                      der Zeitung zur&uuml;ck.
  Das gilt nicht nur f&uuml;r die gedruckte Ausgabe, die bei den Unter-Drei&szlig;igj&auml;hrigen
  kaum noch Nutzer findet, sondern auch f&uuml;r die Onlineausgabe. Nicht mehr
  als 20% der Unterdrei&szlig;igj&auml;hrigen lesen die Onlineausgaben von Zeitungen.
  Die regelm&auml;&szlig;ige t&auml;gliche Zeitungslekt&uuml;re verfl&uuml;chtigt
  sich. Dieser Erkenntnis der Medienforschung liegt ein ver&auml;ndertes Kommunikationsmuster
  zugrunde. Man verschafft sich nicht mehr einmal am Tag mit einem durchschnittlichen
  Zeitaufwand von 15 Minuten f&uuml;r die Tagesschau und 25 Minuten f&uuml;r
  die Zeitungslekt&uuml;re einen &Uuml;berblick &uuml;ber das, was geschehen
  ist, sondern wei&szlig; die Nachrichten im Internet hinterlegt, so dass man
  jederzeit darauf zugreifen kann.<br>
  Fernsehen hei&szlig;t im Tagesablauf, dass man freie Zeit mit den Geschichten
  f&uuml;llt, die die verschiedenen Sender anliefern. Man kann dabei noch b&uuml;geln
  oder eine andere T&auml;tigkeit verrichten, die nicht zuviel Aufmerksamkeit
  erfordert. Bisher hat das Fernsehen die Zuschauer durch das Programmschema
  an sich gebunden und zugleich den Tag strukturiert. F&uuml;r die, die nachmittags
  schon vor dem Fernseher Platz nehmen k&ouml;nnen, wurden eigene Programme entwickelt,
  dann folgt das Vorabendprogramm und vor und um 21h die teuren Sendungen, weil
  hier die meisten Zuschauer zu erreichen sind.<br>
  Wie f&uuml;r die Medien, die sich mit ihrem Programm, ob in einer Zeitungsausgabe
  oder nach einem Programmschema geordnet, pr&auml;sentieren, hat auch das Internet
  Nutzungsmuster hervorgebracht. <br>
  Wie f&uuml;r die anderen Medien braucht es auch f&uuml;r das Internet nicht
  nur verf&uuml;gbare Zeit, sondern eine Ordnung der Inhalte. Diese sind im Internet
  nicht auf einer Zeitachse wie im Programmschema des Fernsehens, sondern im
  virtuellen Raum nebeneinander. Daher &auml;hnelt das Internet eher einer Bibliothek
  als einem klassischen Medium, das periodisch erscheint. In der Bibliothek stehen
  inzwischen auch Spiele, Videos, Musikst&uuml;cke. Hier haben sich zwei Ordnungsstrukturen
  entwickelt.<br>
                  <br>
  -	Die Favoriten sind eine private Sammlung von Fundorten.<br>
  -	Die Suchmaschinen sind eine Art Schlagwortregister.</font></p>
                  <p class="text"><font face="Arial, Helvetica, sans-serif">Da
                      die Suchmaschinen zu jedem Stichwort irgendwelche Fundorte
                      anbieten, vermitteln sie die Sicherheit, auf jede
                    Information in Sekunden zugreifen zu k&ouml;nnen. &Auml;hnlich
                    betritt man die Deutsche Bibliothek oder die Library of Congress
                    mit dem Gef&uuml;hl, in den B&uuml;chern oder Zeitschriften
                    alles zu finden, was es zu wissen gibt.</font></p>
                  <p class="text"><font face="Arial, Helvetica, sans-serif">Entscheidender
                      als die Nachschlagefunktion der Suchmaschinen ist die Verweisfunktion
                      des Internetnets. &Auml;hnlich
                    wie die Fu&szlig;noten in wissenschaftlichen Artikeln und
                    B&uuml;chern hat das Internet ein Verweissystem aufgebaut.
                    Nutzer machen sich auf andere Seiten, auf Musikst&uuml;cke
                    und Videos aufmerksam. Internet - das Besondere<br>
                    Diese Verweisfunktion bringt auf Youtube einzelnen Videos
                    viele Nutzer. F&uuml;r
  das Ranking von Internetseiten sind neben der Zahl der Nutzer die Links, die
  auf die Seite verweisen, entscheidend. </font></p>
                  <p><font face="Arial, Helvetica, sans-serif"><span class="text">Eckhard Bieger</span><br>
                </font>                </p></td>
                <td valign="top"><script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
google_ad_width = 160;
google_ad_height = 600;
google_ad_format = "160x600_as";
google_ad_type = "text_image";
//2007-09-05: Bildung und Erziehung
google_ad_channel = "3955521420";
google_ui_features = "rc:6";
//-->
            </script>
                    <script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
              </script>
                </td>
              </tr>
            </table>
            <p class=MsoNormal> <font face="Arial, Helvetica, sans-serif"><strong> </strong></font></P>
            <p class=MsoNormal>&copy; <font size="2" face="Arial, Helvetica, sans-serif">www.kath.de</font></P>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
         <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td align="left" valign="top"><table width="216" border="0" cellpadding="0" cellspacing="0">
       <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10">
            <?php include("az.html"); ?>
          </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</body>
</html>
