<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Medienausbildung f&uuml;r Theologen</title>
<meta name="title" content="Medien&Öffentlichkeit">
<meta name="author" content="kath.de">
<meta name="publisher" content="kath.de">
<meta name="copyright" content="kath.de">
<meta name="description" content="Stichworte über Medien und Öffentlichkeitsarbeit">
<meta name="abstract" content="Stichworte über Medien und Öffentlichkeitsarbeit">
<meta http-equiv="content-language" content="de">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="date" content="2006-00-02">
<meta name="robots" content="index,follow">
<meta name="revisit-after" content="30 days">
<meta name="revisit" content="after 30 days">
<meta name="DC.Title" content="Medien&Öffentlichkeit">
<meta name="DC.Creator" content="kath.de">
<meta name="DC.Contributor" content="N.N.">
<meta name="DC.Rights" content="kath.de">
<meta name="DC.Publisher" content="kath.de">
<meta name="DC.Date" content="2006-00-02">
<meta name="DC.Description" content="Stichworte über Medien und Öffentlichkeitsarbeit">
<meta name="DC.Language" content="de">
<meta name="DC.Type" content="Text">
<meta name="DC.Format" content="text/html">
<meta name="DC.Identifier" content="http://www.kath.de/kurs/medien_oeffentlichkeit/">
<meta name="keywords" lang="de" content="Theologiestudium, Medien, Literarische Gattungen, Formate, aktiver Wortschatz ">

<link rel="stylesheet" type="text/css" href="kaltefleiter.css" title="fonts">
</head>
<body bgcolor="#ffffff" leftmargin="6" topmargin="6" marginwidth="6" marginheight="6">
<table width="100%" border="0" cellspacing="0" cellpadding="6">
  <tr>
    <td width="100" align="left" valign="top"><table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b>Medien und &Ouml;ffentlichkeit</b></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td>
            <?php include("logo.html"); ?>
          </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
    <td rowspan="2" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2">
            <h1> <font face="Arial, Helvetica, sans-serif">Medienausbildung
            f&uuml;r Theologen</font></h1>
          </td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="L12"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td valign="top" class="L12"><p class=MsoNormal><font face="Arial, Helvetica, sans-serif"><strong>Literarische
                        Gattungen und Formate der Medien</strong></font></P>
                  <p><font face="Arial, Helvetica, sans-serif">Die Theologie
                      ist selbst schon eine Kommunikationswissenschaft. Die Bibelwissenschaft
                      unterscheidet literarische Genera oder Gattungen, im Fernsehen
                      heute Formate genannt. Kommunikation realisiert sich anders
                      in einer Novelle, anders in einem Psalmlied, wieder anders
                      in der Beschreibung einer Kulthandlung. Vergleichbar ist
                      die Unterscheidung von Nachricht, Reportage, Glosse, Kommentar.
                      Die <a href="format.php">Formate</a> des Fernsehens haben
                      wie die literarischen Genera der Bibel unterschiedliche
                      Baugesetze, ob es sich um ein Fernsehspiel, einen Dokumentarfilm,
                      Nachrichten, eine Quizshow handelt. So gibt es Talkshows,
                      in denen man Prominente im Gespr&auml;ch kennenlernt, Fernsehdiskussionen,
                      in denen ein kontroverses Thema im Mittelpunkt steht oder
                      Spieleshows, die den Wettkampf als Motor des Geschehens
                      haben.<br>
  Lernt man in den Bibelwissenschaften den Umgang mit Formaten, bekommt man durch
  die Philosophie- und Theologievorlesungen einen Blick daf&uuml;r, wie Ideen
  sich entwickeln, Einflu&szlig; gewinnen und mit vorherrschenden Meinungen in
  Konkurrenz treten. Das ist der Stoff, aus dem Zeitungen, politische Magazine,
  Kommentare gemacht werden.<br>
  Das analytische Training und damit eine schnelle Auffassungsgabe sind f&uuml;r
  einen Journalisten unentbehrlich.<br>
  Der Ideenreichtum und die Formen, wie Gedanken durch Bilder ausgedr&uuml;ckt
  werden, ist das Metier der Werbung. Sie hat aber noch lange nicht die Qualit&auml;ten
  der religi&ouml;sen Kunst erreicht. <br>
  Das Verst&auml;ndnis daf&uuml;r, wie Juristen denken, kann man sich in den
  Vorlesungen und den &Uuml;bungen in Kirchenrecht aneignen. Auch das bereitet
  auf den Alttag des Journalisten und die Ideenfindung des Krimiautors vor.<br>
  Ein gr&uuml;ndliches Studium der Philosophie und der Theologie und ihrer methodischen
  Instrumentarien ist die beste Vorbereitung auf einen Medienberuf. Hinzukommen
  mu&szlig; die Entwicklung des aktiven Wortschatzes. Ein Journalist wie ein
  Drehbuchautor werden bezahlt, wenn sie einen Artikel, ein Treatment, eine Reportage
  abgeben. Deshalb ist es sinnvoll, w&auml;hrend des <a href="diplomtheologen_medien.php">Studium</a> schon
  f&uuml;r den Lokalteil einer Zeitung, die Bistumszeitung zu schreiben oder
  im lokalen oder regionalen H&ouml;rfunk als freier Mitarbeiter, freie Mitarbeiterin
  aktiv zu werden. Dieses Training schl&auml;gt sich unmittelbar in einer besseren
  Note f&uuml;r die Diplomarbeit und das Schlu&szlig;examen nieder. Denn wie
  die Leser k&ouml;nnen die Professoren nicht erkennen, was der Journalist bzw.
  der Student wei&szlig;, sondern was er bzw. sie sprachlich ausgedr&uuml;ckt
  haben. Gegen alle landl&auml;ufige Meinung geht es in einer Pr&uuml;fung nur
  zu einem Teil um Wissen, der Inhalt der Pr&uuml;fung ist das, was der Pr&uuml;fling &#8222;zu
  sagen hat&#8220;.<br>
  Einige <a href="http://www.sankt-georgen.de/medien/index.htm">Fakult&auml;ten</a> bieten
  f&uuml;r Studierende der Theologie Vorbereitungskurse f&uuml;r eine sp&auml;teren
  Medienberuf. Manche Universit&auml;ten f&uuml;hren journalistische Ausbildungsprogramme
  durch.<br>
  Da in Deutschland der Zugang zum Journalismus durch ein zweij&auml;hrige Volontariat
  erfolgt, ist es sinnvoll, sich im Studium so zu qualifizieren, da&szlig; man
  die Zugangspr&uuml;fung zu einem Volontariat besteht. Hierf&uuml;r eignet sich
  die freie Mitarbeit bei einer Zietung oder einem Sender, die zum Ergebnis hat,
  da&szlig; man bei der Bewertung gedruckte Artikel oder gesendete Beitr&auml;ge
  vorweisen kann.<br>
  F&uuml;r Drehbuchautoren gibt es Kurse an Filmhochschulen wie auch frei ausgeschriebene
  Programme.<br>
  Eckhard Bieger<br>
                  </font></p>
                  <p class=MsoNormal> <font face="Arial, Helvetica, sans-serif"><strong> </strong></font></P>
                <p class=MsoNormal>&copy; <font size="2" face="Arial, Helvetica, sans-serif"><a href="http://www.kath.de/">www.kath.de</a></font></P></td>
                <td valign="top"><script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
google_ad_width = 160;
google_ad_height = 600;
google_ad_format = "160x600_as";
google_ad_type = "text_image";
//2007-09-05: Bildung und Erziehung
google_ad_channel = "3955521420";
google_ui_features = "rc:6";
//-->
            </script>
                    <script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
              </script>
                </td>
              </tr>
            </table>
            <p class=MsoNormal>&nbsp;</P>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
         <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td align="left" valign="top"><table width="216" border="0" cellpadding="0" cellspacing="0">
       <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10">
            <?php include("az.html"); ?>
          </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</body>
</html>
