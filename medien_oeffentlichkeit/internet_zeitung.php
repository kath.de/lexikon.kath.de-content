<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Internet und Zeitung - www.kath.de</title>
<meta name="title" content="Medien&Öffentlichkeit">
<meta name="author" content="kath.de">
<meta name="publisher" content="kath.de">
<meta name="copyright" content="kath.de">
<meta name="description" content="Stichworte über Medien und Öffentlichkeitsarbeit">
<meta name="abstract" content="Stichworte über Medien und Öffentlichkeitsarbeit">
<meta http-equiv="content-language" content="de">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="date" content="2006-00-02">
<meta name="robots" content="index,follow">
<meta name="revisit-after" content="30 days">
<meta name="revisit" content="after 30 days">
<meta name="DC.Title" content="Medien&Öffentlichkeit">
<meta name="DC.Creator" content="kath.de">
<meta name="DC.Contributor" content="N.N.">
<meta name="DC.Rights" content="kath.de">
<meta name="DC.Publisher" content="kath.de">
<meta name="DC.Date" content="2006-00-02">
<meta name="DC.Description" content="Stichworte über Medien und Öffentlichkeitsarbeit">
<meta name="DC.Language" content="de">
<meta name="DC.Type" content="Text">
<meta name="DC.Format" content="text/html">
<meta name="DC.Identifier" content="http://www.kath.de/kurs/medien_oeffentlichkeit/">
<meta name="keywords" lang="de" content="Theologiestudium, Medien, Kommuniaktionsberuf, Drehbücher, Lokalredaktion, Journalismus">

<link rel="stylesheet" type="text/css" href="kaltefleiter.css" title="fonts">
</head>
<body bgcolor="#ffffff" leftmargin="6" topmargin="6" marginwidth="6" marginheight="6">
<table width="100%" border="0" cellspacing="0" cellpadding="6">
  <tr>
    <td width="100" align="left" valign="top"><table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b>Medien und &Ouml;ffentlichkeit</b></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td>
            <?php include("logo.html"); ?>
          </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
    <td rowspan="2" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2">
            <h1> <font face="Arial, Helvetica, sans-serif">Medien&amp;&Ouml;ffentlichkeit</font></h1>
          </td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="L12"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td valign="top" class="L12"><p class="text"><font face="Arial, Helvetica, sans-serif"><strong>Internet und Zeitung</strong></font></p>
                  <p class="text"><font face="Arial, Helvetica, sans-serif">Das
                      Internet lebt von der Zeitung &#8211; das
                    meiste, was aktuell im Internet zu finden ist, stammt von
                    Zeitungsredaktionen. Die Onlineausgabe einer Zeitung muss,
                    im Unterschied zur einmal t&auml;glich erscheinenden Printausgabe,
                    mehrfach am Tag aktualisiert werden. Eine mehrfache erscheinungsweise
                    der Zeitung gab es noch im 20.Jahrhundert. Als das Radio
                    noch nicht so weit verbreitet war, erschienen Zeitungen mit
                    einer Morgen-, einer Mittags- und Abendausgabe.<br>
                    Die Zeitung ist von ihren Anf&auml;ngen an ein wichtiger Werbetr&auml;ger gewesen,
  die aktuellen Preise f&uuml;r Getreide, Metalle u.a. standen am Ursprung. Da
  die Kaufleute auch die Nachrichten mitbrachten, ist die Zeitung von Anfang
  an mit dem Handel verbunden. Die Zeitung finanziert sich zum gr&ouml;&szlig;eren
  Teil durch Werbung, vor allem durch die Rubrikanzeigen, Stellenmarkt, Gebrauchtwagenmarkt,
  Immobilien, Mietwohnungen und Ferienunterk&uuml;nfte als bezahlte Kleinanzeigen
  machen die finanzielle Basis der Zeitung aus. Diese Interessen kann das Internet
  mit seiner Suchfunktion sehr viel besser bedienen. Zudem ist die Ver&ouml;ffentlichung
  einer Anzeige im Internet sehr viel preiswerter als in der Zeitung. Um eine
  Wohnung zu vermieten, muss die Anzeige in der Zeitung je nach Auflage hunderttausendmal
  und mehr gedruckt werden, im Internet kosten neben den technischen Kosten,
  die Anzeige online zu stellen nur noch die Zugriffe. <br>
                  <br>
                  Damit ist das Internet zur Bedrohung f&uuml;r die Finanzierung von Zeitungen
  geworden, das aber nicht ohne Schuld der Zeitungen. Diese haben Ende der neunziger
  Jahre nicht erkannt, wo die St&auml;rken des Netzes liegen. Sie haben das neue
  Medium untersch&auml;tzt, weil die meisten Stellensuchenden, K&auml;ufer eines
  Gebrauchtwagens, Urlauber noch nicht gewohnt waren, im Netz nach g&uuml;nstigen
  Angeboten zu suchen. H&auml;tten die Zeitungen damals eine Kleinanzeige, ob
  eine Stellenausschreibungen oder eine Mietwohnung zugleich ins Netz gestellt,
  w&auml;ren sie heute finanziell gesichert. Stattdessen haben Studenten die
  Software f&uuml;r Mietwohnungen, Stellenausschreibungen oder Gebrauchtwagen
  entwickelt, die entsprechenden Plattformen haben heute einen Milliardenwert.</font></p>
                  <p></p>
                  <p><font face="Arial, Helvetica, sans-serif"><span class="text">Eckhard Bieger</span><br>
                  </font></p>
                <p class="text"></p></td>
                <td valign="top"><script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
google_ad_width = 160;
google_ad_height = 600;
google_ad_format = "160x600_as";
google_ad_type = "text_image";
//2007-09-05: Bildung und Erziehung
google_ad_channel = "3955521420";
google_ui_features = "rc:6";
//-->
            </script>
                    <script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
              </script>
                </td>
              </tr>
            </table>
            <p class=MsoNormal> <font face="Arial, Helvetica, sans-serif"><strong> </strong></font></P>
            <p class=MsoNormal>&copy; <font size="2" face="Arial, Helvetica, sans-serif">www.kath.de</font></P>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
         <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td align="left" valign="top"><table width="216" border="0" cellpadding="0" cellspacing="0">
       <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10">
            <?php include("az.html"); ?>
          </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</body>
</html>
