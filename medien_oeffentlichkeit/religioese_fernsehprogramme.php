<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Religi&ouml;se Fernsehprogramme</title>
<meta name="title" content="Medien&Öffentlichkeit">
<meta name="author" content="kath.de">
<meta name="publisher" content="kath.de">
<meta name="copyright" content="kath.de">
<meta name="description" content="Stichworte über Medien und Öffentlichkeitsarbeit">
<meta name="abstract" content="Stichworte über Medien und Öffentlichkeitsarbeit">
<meta http-equiv="content-language" content="de">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="date" content="2006-00-02">
<meta name="robots" content="index,follow">
<meta name="revisit-after" content="30 days">
<meta name="revisit" content="after 30 days">
<meta name="DC.Title" content="Medien&Öffentlichkeit">
<meta name="DC.Creator" content="kath.de">
<meta name="DC.Contributor" content="N.N.">
<meta name="DC.Rights" content="kath.de">
<meta name="DC.Publisher" content="kath.de">
<meta name="DC.Date" content="2006-00-02">
<meta name="DC.Description" content="Stichworte über Medien und Öffentlichkeitsarbeit">
<meta name="DC.Language" content="de">
<meta name="DC.Type" content="Text">
<meta name="DC.Format" content="text/html">
<meta name="DC.Identifier" content="http://www.kath.de/kurs/medien_oeffentlichkeit/">
<meta name="keywords" lang="de" content="<font face="Arial, Helvetica, sans-serif"><strong>Serien, Talkshows, Diskussionen Gottesdienst&uuml;bertragungen</strong></font>">

<link rel="stylesheet" type="text/css" href="kaltefleiter.css" title="fonts">
</head>
<body bgcolor="#ffffff" leftmargin="6" topmargin="6" marginwidth="6" marginheight="6">
<table width="100%" border="0" cellspacing="0" cellpadding="6">
  <tr>
    <td width="100" align="left" valign="top"><table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b>Medien und &Ouml;ffentlichkeit</b></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td>
            <?php include("logo.html"); ?>
          </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
    <td rowspan="2" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2">
            <h1> <font size="6" face="Arial, Helvetica, sans-serif">Religi&ouml;se
            </font><font size="6" face="Arial, Helvetica, sans-serif">Fernsehprogramme</font></h1>
          </td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="L12"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td valign="top" class="L12"><p class=MsoNormal><font face="Arial, Helvetica, sans-serif"><strong>Serien,
                        Talkshows, Diskussionen, Gottesdienst&uuml;bertragungen</strong></font></P>
                  <p><font size="3" face="Arial, Helvetica, sans-serif">Religion
                      hat alles, was das Fernsehen braucht: Helden und B&ouml;sewichter,
                      es geht um Leben und Tod, Schuld und Vergebung. Religi&ouml;se
                      Protagonisten vermitteln zwischen dieser Welt und dem Jenseits,
                      sie begleiten Menschen durch Gefahrenzonen. Pfarrer und
                      Nonnen sind daher wie &Auml;rzte und Kriminalbeamte f&uuml;r
                      die Zuschauer interessant. Da die Kirchen sich f&uuml;r
                      soziale Belange einsetzen, in den L&auml;ndern der Dritten
                      Welt t&auml;tig sind, sich zu wichtigen Fragen der Gesellschaft &auml;u&szlig;ern,
                      gibt es auch f&uuml;r die Kirchenberichterstattung viele
                      Themen. Bisch&ouml;fe und Kirchenpr&auml;sidenten, Theologieprofessoren
                      und Caritasdirektoren geh&ouml;ren wie selbstverst&auml;ndlich
                      in Talkshows und vom Fernsehen veranstaltete Diskussionen.
                      Selbst die Werbung ist religi&ouml;s infiziert, Engel,
                      Nonnen und M&ouml;nche, Kirchenr&auml;ume, Kerzen u.a.
                      lassen die religi&ouml;se Dimension anklingen. Versuchte
                      die Gesellschaft noch bis Anfang der achtziger Jahre, Religion
                      als St&ouml;rfaktor auf einige wenige Programmpl&auml;tze
                      einzugrenzen, durchdringt das Religi&ouml;se wieder das
                      Programm. Der Versuch, Religion und Gesellschaft strikt
                      zu trennen, ist nur kurzzeitig durchf&uuml;hrbar gewesen.
                      Als die Pfarrerserien Ende der achtziger Jahre Zuschauerrekorde
                      brachen und der polnische Papst zum Fernsehstar wurde,
                      haben die Fernsehredaktionen ihre Distanz zu Religion und
                      Kirchen &uuml;berwunden. Ein Krimi, der in einem Kloster
                      spielt, hat f&uuml;r die Zuschauer einen sehr viel gr&ouml;&szlig;eren
                      Reiz als die Wohnungen der Reichen oder das Drogenmilieu.</font></p>
                  <p><font size="3" face="Arial, Helvetica, sans-serif">Religion
                      findet im Fernsehen also nicht nur dann statt, wenn das
                      Typischste der Religion, die Feier eines <a href="gottesdienste_fernsehen.php">Gottesdienstes</a> &uuml;bertragen
                      wird. Da&szlig; die &Uuml;bertragungen ein Quotenerfolg
                      sind, zeigt auch, da&szlig; das Interesse der Zuschauer
                      an Religion zugenommen hat. Der Gottesdienst hat seinem
                      Platz im Sonntagsprogramm, an den Wochentagen findet der
                      Zuschauer die religi&ouml;sen Serienhelden, ob Nonnen oder
                      Pfarrer. Die Berichterstattung &uuml;ber die Kirche wie
                      die Fernsehdiskussionen finden sich im Programmschema meist
                      nach 21 Uhr oder am Sonntagnachmittag. Der <a href="http://www.kirche.tv/">Fernsehtipp</a> der
                      Katholischen Fernseharbeit gibt t&auml;glich Hinweise auf
                      religi&ouml;se Programme.<br>
                  </font></p>
                  <p class=MsoNormal> <font face="Arial, Helvetica, sans-serif"><strong> </strong></font></P>
                <p class=MsoNormal>&copy; <font size="2" face="Arial, Helvetica, sans-serif">www.kath.de</font></P></td>
                <td valign="top"><script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
google_ad_width = 160;
google_ad_height = 600;
google_ad_format = "160x600_as";
google_ad_type = "text_image";
//2007-09-05: Bildung und Erziehung
google_ad_channel = "3955521420";
google_ui_features = "rc:6";
//-->
            </script>
                    <script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
              </script>
                </td>
              </tr>
            </table>
            <p class=MsoNormal>&nbsp;</P>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
         <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td align="left" valign="top"><table width="216" border="0" cellpadding="0" cellspacing="0">
       <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10">
            <?php include("az.html"); ?>
          </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</body>
</html>
