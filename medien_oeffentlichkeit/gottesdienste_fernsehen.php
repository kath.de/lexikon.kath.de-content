<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Gottesdienste im Fernsehen</title>
<meta name="title" content="Medien&Öffentlichkeit">
<meta name="author" content="kath.de">
<meta name="publisher" content="kath.de">
<meta name="copyright" content="kath.de">
<meta name="description" content="Stichworte über Medien und Öffentlichkeitsarbeit">
<meta name="abstract" content="Stichworte über Medien und Öffentlichkeitsarbeit">
<meta http-equiv="content-language" content="de">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="date" content="2006-00-02">
<meta name="robots" content="index,follow">
<meta name="revisit-after" content="30 days">
<meta name="revisit" content="after 30 days">
<meta name="DC.Title" content="Medien&Öffentlichkeit">
<meta name="DC.Creator" content="kath.de">
<meta name="DC.Contributor" content="N.N.">
<meta name="DC.Rights" content="kath.de">
<meta name="DC.Publisher" content="kath.de">
<meta name="DC.Date" content="2006-00-02">
<meta name="DC.Description" content="Stichworte über Medien und Öffentlichkeitsarbeit">
<meta name="DC.Language" content="de">
<meta name="DC.Type" content="Text">
<meta name="DC.Format" content="text/html">
<meta name="DC.Identifier" content="http://www.kath.de/kurs/medien_oeffentlichkeit/">
<meta name="keywords" lang="de" content="mystagogsiche Bildregie, Kamerafahrten, Überblendung, intentionale Teilnahme, live">

<link rel="stylesheet" type="text/css" href="kaltefleiter.css" title="fonts">
</head>
<body bgcolor="#ffffff" leftmargin="6" topmargin="6" marginwidth="6" marginheight="6">
<table width="100%" border="0" cellspacing="0" cellpadding="6">
  <tr>
    <td width="100" align="left" valign="top"><table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b>Medien und &Ouml;ffentlichkeit</b></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td>
            <?php include("logo.html"); ?>
          </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
    <td rowspan="2" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2">
            <h1> <font face="Arial, Helvetica, sans-serif">Gottesdienste im Fernsehen</font></h1>
          </td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="L12"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td valign="top" class="L12"><p class=MsoNormal><font size="3" face="Arial, Helvetica, sans-serif"><strong>&Uuml;bertragungen
                        von Gottesdiensten</strong></font></P>
                  <p><font face="Arial, Helvetica, sans-serif">Gottesdienste
                      werden dadurch Teil des Fernsehprogramms, da&szlig; sie
                      aus einer Kirche, einem Stadion oder von einem anderen
                      Versammlungsort &uuml;bertragen werden. Unter dem Gesichtspunkt
                      des Mediums bedeutet das, da&szlig; mehrere <strong>Kameras
                      und Mikrofone</strong> aufgestellt werden und im &Uuml;bertragungswagen
                      die Kamerabilder &#8222;geschnitten&#8220;, d.h. jeweils
                      das Bild einer Kamera ausgew&auml;hlt wird und dann das
                      einer weiteren folgt, da&szlig; Gro&szlig;aufnahme und
                      Totale wechseln, Kamerafahrten, Zooms und Schwenks sowie &Uuml;berblendungen
                      f&uuml;r den Zuschauer das Geschehen darstellen. Das hat
                      zur Folge, da&szlig; der Zuschauer ein sehr viel abwechselreicheres
                      Bild zu sehen bekommt als der Teilnehmer in der Kirche
                      und da&szlig; der Zuschauer sehr viele n&auml;her das Geschehen
                      im Altarraum verfolgen, den Prediger in Gro&szlig;aufnahme
                      sehen kann und auch in der Regel die Einzelstimmen, ob
                      die des Lektors oder eines Solisten, in besserer Qualit&auml;t
                      h&ouml;rt als es die Tonanlage in der Kirche oder im Stadion
                  leisten. </font></p>
                  <p><font face="Arial, Helvetica, sans-serif"><br>
                    Die <strong>Bildregie</strong> des Fernsehens erm&ouml;glicht es, dem Zuschauer den Gottesdienst
    auch in seiner theologischen und spirituellen Dimensionen zu erschlie&szlig;en.
    Daf&uuml;r wurde vom ZDF und der Katholischen Fernseharbeit die mystagogische
    Bildregie entwickelt. Mit diesem Regiekonzept wird der Sinn einzelner Teile
    des Gottesdienstes, wie z.B. das Gloria oder Sanctus, nicht durch einen Kommentator
    erkl&auml;rt, sondern durch die Kameraf&uuml;hrung, durch Fahrten und Zooms
    sowie durch &Uuml;berblendungen. Das ist m&ouml;glich, weil die Symbolik des
    Kirchenraums, z.B. die Kuppel oder die S&auml;ulen, schon einen Kommentar zur
    Liturgie darstellen, der durch die <a href="mystagogische_bildregie.php">mystagogische
    Bildregie</a> in das Medium Fernsehen transponiert wird.<br>
    Ziel der Bild- und Tonregie ist es, da&szlig; der Zuschauer sich ganz auf das
    Geschehen des Gottesdienstes konzentrieren kann, ohne da&szlig; ein Kommentator
    oder eine nicht an der Liturgie orientierte Bildregie ihn ablenkt. Damit wird
    dem Zuschauer wie bei anderen &Uuml;bertragungen, z.B. eines Fu&szlig;ballspiels
    oder einer Show, eine Teilnahme am Geschehen erm&ouml;glicht. Diese Teilnahme
    ist auf der einen Seite privilegiert, weil der Zuschauer durch die Positionierung
    von Kameras in der N&auml;he des Altars das Geschehen sehr viel n&auml;her
    verfolgen kann. Auf der anderen Seite erlebt der Zuschauer nicht die Gemeinschaft
    in der Kirche oder in einem Stadion. Er ist auch nicht im vollen Sinne Teilnehmer
    des Geschehens. Eine Teilnahme, die sich z.B. durch Mitsingen, den Austausch
    des Friedensgru&szlig;es oder den Gang zur Kommunion ausdr&uuml;ckt, ist ihm
    nicht m&ouml;glich. Da der Zuschauer aber nicht einfach nur ein distanzierter
    Voyeur sein will, sondern das Programm einschaltet, weil er &#8222;dabei&#8220; sein
    will, so wie der Zuschauer einer Sport&uuml;bertragung oder einer Volksmusiksendung,
    ist er in gewisser Weise auch Teilnehmer. Damit der Zuschauer teilnehmen kann,
    wird der Gottesdienst &uuml;berhaupt &uuml;bertragen. Das ist bei Gro&szlig;veranstaltungen
    wie Kirchen- und Katholikentagen deshalb sinnvoll, weil nicht alle zu dem Treffen
    fahren k&ouml;nnen. Auch die &Uuml;bertragung aus einer Kirche an einem durchschnittlichen
    Sonntag will Teilnahme erm&ouml;glichen. Dem Zuschauer wird keine im vollen
    Sinne aktive Teilnahme erm&ouml;glicht, wohl aber eine intentionale, die ihre
    eigene Realit&auml;t hat. Er bezieht sich auf den Gottesdienst, den er vermittels
    des Fernsehens mitfeiern kann, durch seine <a href="http://www.kath.de/lexikon/liturgie/kommunionmeditation.php">Intention</a>,
    indem er sich auf das Geschehen und dessen Sinn konzentriert. Die Gemeinde
    l&auml;dt daher am besten den Zuschauer zu Beginn der &Uuml;bertragung durch
    direkte Ansprache in die Kamera zur Mitfeier ein.<br>
    Es g&auml;be auch ein anderes Modell: Der Gottesdienst wird im Studio gefeiert,
    nur von einem Priester und einer Assistenz und die Gemeinde w&uuml;rde durch
    die Zuschauer konstituiert. Das w&auml;re ein Fernsehgottesdienst, der weder
    eine Gemeinde vor Ort noch die Teilnehmer eines Kirchen- oder Katholikentages
    br&auml;uchte. Dann w&uuml;rde das Fernsehen eine eigene Gemeinde bilden, die
    durch das Medium Fernsehen verbunden, &ouml;rtlich zerstreut, trotzdem zusammen
    Gottesdienst feiern w&uuml;rde. Diese Form ist in England und Skandinavien
    erprobt worden, hat sich aber nicht durchgesetzt. Sie funktioniert genauso
    wenig wie eine Show, die nur von den Akteuren, ohne das Publikum in einer Halle
    durchgef&uuml;hrt w&uuml;rde.</font></p>                  <p><font face="Arial, Helvetica, sans-serif"><br>
                    Die Einladung an den Zuschauer zu einer intentionalen
                            Teilnahme legt es nahe, da&szlig; Gottesdienste im Fernsehen live &uuml;bertragen werden, damit der
    Zuschauer sich intentional auf ein Geschehen beziehen kann, das &#8222;jetzt&#8220; auch
    tats&auml;chlich stattfindet. Da Live-&Uuml;bertragungen auch im Sport wie
    auch im Showbereich f&uuml;r den Zuschauer einen h&ouml;heren Wert haben, ist
    es sinnvoll, da&szlig; die Kirchen auf dem Live-Charakter beharren.</font></p>
                  <p><font face="Arial, Helvetica, sans-serif"><br>
                    Da&szlig; die Gottesdienst&uuml;bertragungen ein typisches Feiertags- und Sonntagsprogramm
      darstellen, versteht sich aus ihrer Verwurzelung in den Kirchen. Da&szlig; sie
      beim Zuschauer ein Erfolg sind, verwundert, weil ein Gottesdienst sehr viel
      weniger Spannungselemente als ein Sportwettkampf und weniger Prominente als
      eine Show aufweisen kann. Die Gottesdienste an durchschnittlichen Sonntagen
      werden von durchschnittlichen Gemeinden mit durchschnittlichen Pfarrern gestaltet.
      Offensichtlich wollen die Zuschauer mit den Menschen feiern, die so sind wie
      sie. Die Zuschauerzahlen sind in den letzten Jahren erheblich gewachsen. W&auml;hrend
      die sonnt&auml;glichen &Uuml;bertragungen im ZDF Anfang der neunziger Jahre
      noch im Schnitt 360.000 Zuschauer erreichten, waren es Mitte der Neunziger
      schon 800.000, die Zahl ist auf 1 Million gestiegen.<br>
                  </font></p>
                  <p class=MsoNormal><font face="Arial, Helvetica, sans-serif">&Uuml;ber
                      die <a href="http://www.fernsehgottesdienst.de/">&Uuml;bertragungen
                      im ZDF</a> informiert jeweils eine Homepage </font> </P>
                  <p class=MsoNormal>&copy; <font size="2" face="Arial, Helvetica, sans-serif">www.kath.de</font></P></td>
                <td valign="top"><script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
google_ad_width = 160;
google_ad_height = 600;
google_ad_format = "160x600_as";
google_ad_type = "text_image";
//2007-09-05: Bildung und Erziehung
google_ad_channel = "3955521420";
google_ui_features = "rc:6";
//-->
            </script>
                    <script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
              </script>
                </td>
              </tr>
            </table>
            <p class=MsoNormal>&nbsp;</P>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
         <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td align="left" valign="top"><table width="216" border="0" cellpadding="0" cellspacing="0">
       <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10">
            <?php include("az.html"); ?>
          </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</body>
</html>
