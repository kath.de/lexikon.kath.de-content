<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Schweigespirale</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="kaltefleiter.css" title="fonts">
</head>
<body bgcolor="#ffffff" leftmargin="6" topmargin="6" marginwidth="6" marginheight="6">
<table width="100%" border="0" cellspacing="0" cellpadding="6">
  <tr>
    <td width="100" align="left" valign="top"><table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b>Medien und &Ouml;ffentlichkeit</b></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td>
            <?php include("logo.html"); ?>
          </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
    <td rowspan="2" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2">
            <h1> <font face="Arial, Helvetica, sans-serif"></font><font face="Arial, Helvetica, sans-serif">Schweigespirale</font></h1>
          </td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="L12"> 
        
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td valign="top" class="L12"><p class=MsoNormal><font face="Arial, Helvetica, sans-serif"><strong>Wechselverh&auml;ltnis
                        von direkter und Medien - Kommunikation</strong></font></P>
                  <p class=MsoNormal><font face="Arial, Helvetica, sans-serif">Der
                      Proze&szlig; der &ouml;ffentlichen Meinungsbildung wird
                      durch den kommunikativen Einsatz der Beteiligten vorangetrieben.
                      Die <a href="oeffentlichkeit_prozess.php">Meinungsbildungsprozesse</a> spielen
                      sich einmal auf der B&uuml;hne der Medien ab, zum anderen
                      in den K&ouml;pfen und Herzen der einzelnen, indem diese
                      sich der einen oder anderen Position mehr zuneigen. Diese
                      Meinungsbildungsprozesse verlaufen jedoch nicht nach naturwissenschaftlich
                      beschreibbaren Gesetzm&auml;&szlig;igkeiten ab, sondern
                      sind in hohem Ma&szlig;e psychologisch bestimmt. Entscheidend
                      ist die Selbsteinsch&auml;tzung der Beteiligten. Hier gibt
                      es einmal die &ouml;ffentlichen Foren, auf denen die Vertreter
                      einer Position ihre Argumente mehr oder weniger engagiert
                      vertreten. Die &Uuml;berzeugungskraft einzelner Sprecher,
                      z.B. nach einem Disput zwischen den Kanzlerkandidaten,
                      wird durch Umfragen erhoben. Es findet aber auch im direkten
                      Gespr&auml;ch ein Meinungsbildungsproze&szlig; statt. In
                      diesen direkten Gespr&auml;chen erfahren die einzelnen,
                      ob die Meinungsposition ihrer Partei, ihrer Kirche, ihres
                      Verbandes bei anderen auf Zustimmung, Reserviertheit oder
                      Ablehnung trifft. Wenn die einzelnen Ablehnung f&uuml;r
                      die Position ihrer Partei, ihrer Kirche nur vermuten, werden
                      sie eher schweigen als den Dissens mit ihren Bekannten
                      und Arbeitskollegen zu riskieren. Sie geben sich im Bekanntenkreis
                      nicht als Anh&auml;nger einer Meinung bzw. einer Partei
                      zu erkennen, wenn sie vermuten, da&szlig; diese Partei,
                      dieser Verband auf wenig Zustimmung oder sogar Ablehnung
                      st&ouml;&szlig;t. Elisabeth Noelle-Neumann spricht von
                      einer Schweigespirale, die durch die Einsch&auml;tzung
                      der Erfolgsaussichten in Gang gesetzt wird. F&uuml;hlt
                      z.B. eine Partei wachsenden Zuspruch, treten ihre Mitglieder
                      und Anh&auml;nger offensiver in der direkten Kommunikation
                      auf. Sp&uuml;ren Anh&auml;nger einer Partei oder einer
                      Kirche zur&uuml;ckgehende Unterst&uuml;tzung, werden sie
                      weniger offensiv auftreten oder sogar schweigen. Dieses
                      Verhalten ist nicht von der Gr&ouml;&szlig;e, z.B. vom
                      Prozentsatz der W&auml;hlerstimmen, abh&auml;ngig, sondern
                      nur von der Einsch&auml;tzung, ob man mehr oder weniger
                      Stimmen zu erwarten hat. <br>
  Die Spirale kommt so zustande, da&szlig; Zur&uuml;ckhaltung beim Vertreten
  der eigenen Position den Vertretern der anderen Positionen gr&ouml;&szlig;eren
  Raum in der &Ouml;ffentlichkeit freigibt, w&auml;hrend die eigenen Position
  immer weniger genannt wird. Sie verliert damit ihre Pr&auml;senz im Bewu&szlig;tsein
  der Zuh&ouml;rer. Die in Bezug auf eine Wahl noch Unentschiedenen werden weniger
  erreicht. Dadurch verliert eine Partei, eine Gewerkschaft, eine Kirche an Zustimmung.
  Erst wenn die eigenen Positionen wieder offensiver vertreten werden, kann eine
  Gruppe, ein Verband, eine Partei aus dem Abw&auml;rtstrend der Schweigespirale
  heraustreten. <br>
  Wenn die aktiven Mitglieder einer Partei, einer Kirche, eines Verbandes davon
  ausgehen, da&szlig; sie in der &Ouml;ffentlichkeit f&uuml;r ihre Positionen
  weniger Geh&ouml;r finden, dann haben sie sich bereits in die Schweigespirale
  begeben. Sie werden sich dann auch nicht &ouml;ffentlich exponieren, weil sie
  davon ausgehen, da&szlig; sie sich damit nur Mi&szlig;erfolge einhandeln. Sie
  werden nicht aktiv und k&ouml;nnen sich sogar daf&uuml;r best&auml;tigt f&uuml;hlen.
  Die mangelnde &ouml;ffentliche Aktivit&auml;t bewirkt das, was erwartet wurde:
  Die eigne Partei, Gewerkschaft, Kirche kommt in der &Ouml;ffentlichkeit immer
  weniger vor und wenn, dann mit Skandalen. Die <a href="skandal_berichterstattung.php">Skandale</a> in
  den Vordergrund zu stellen, ist aber die zu erwartnede Reaktion der Journalisten
  auf die Institution, die sich der &Ouml;ffentlichkeit entzieht.<br>
                <font size="2">Literaturhinweis: Elisabeth Noelle-Neumann, Die
                Schweigespirale, &Ouml;ffentliche Meinung &#8211; unsere soziale
                Haut, Wien, Berlin, 1982</font></font></P>
                  <p><font face="Arial, Helvetica, sans-serif">Eckhard Bieger<br>
                        <br>
                  </font> </p>
                <p class=MsoNormal>&copy; <font size="2" face="Arial, Helvetica, sans-serif">www.kath.de</font></P></td>
                <td valign="top"><script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
google_ad_width = 160;
google_ad_height = 600;
google_ad_format = "160x600_as";
google_ad_type = "text_image";
//2007-09-05: Bildung und Erziehung
google_ad_channel = "3955521420";
google_ui_features = "rc:6";
//-->
            </script>
                    <script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
              </script>
                </td>
              </tr>
            </table>
            <p class=MsoNormal>&nbsp;</P>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
         <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td align="left" valign="top"><table width="216" border="0" cellpadding="0" cellspacing="0">
       <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10">
            <?php include("az.html"); ?>
          </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</body>
</html>
