<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Log Tail - www.kath.de</title>
<meta name="title" content="Medien&Öffentlichkeit">
<meta name="author" content="kath.de">
<meta name="publisher" content="kath.de">
<meta name="copyright" content="kath.de">
<meta name="description" content="Stichworte über Medien und Öffentlichkeitsarbeit">
<meta name="abstract" content="Stichworte über Medien und Öffentlichkeitsarbeit">
<meta http-equiv="content-language" content="de">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="date" content="2006-00-02">
<meta name="robots" content="index,follow">
<meta name="revisit-after" content="30 days">
<meta name="revisit" content="after 30 days">
<meta name="DC.Title" content="Medien&Öffentlichkeit">
<meta name="DC.Creator" content="kath.de">
<meta name="DC.Contributor" content="N.N.">
<meta name="DC.Rights" content="kath.de">
<meta name="DC.Publisher" content="kath.de">
<meta name="DC.Date" content="2006-00-02">
<meta name="DC.Description" content="Stichworte über Medien und Öffentlichkeitsarbeit">
<meta name="DC.Language" content="de">
<meta name="DC.Type" content="Text">
<meta name="DC.Format" content="text/html">
<meta name="DC.Identifier" content="http://www.kath.de/kurs/medien_oeffentlichkeit/">
<meta name="keywords" lang="de" content="Theologiestudium, Medien, Kommuniaktionsberuf, Drehbücher, Lokalredaktion, Journalismus">

<link rel="stylesheet" type="text/css" href="kaltefleiter.css" title="fonts">
</head>
<body bgcolor="#ffffff" leftmargin="6" topmargin="6" marginwidth="6" marginheight="6">
<table width="100%" border="0" cellspacing="0" cellpadding="6">
  <tr>
    <td width="100" align="left" valign="top"><table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b>Medien und &Ouml;ffentlichkeit</b></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td>
            <?php include("logo.html"); ?>
          </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
    <td rowspan="2" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2">
            <h1> <font face="Arial, Helvetica, sans-serif">Medien&amp;&Ouml;ffentlichkeit</font></h1>
          </td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="L12"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td valign="top" class="L12"><font face="Arial, Helvetica, sans-serif"><strong>Long
                      Tail &#8211;Serienwirkung im
                    Internet </strong>
                  <p><img SRC="longtail.jpg" style="border: double 4px #009999" align="right" hspace="10" vspace="10" name="Long Tail" alt="Der Long Tail" ><strong>Den
                      Schwanz verl&auml;ngern </strong>&#8211; long tail, das gelingt
                    den Fernsehsendern durch die Serien. Bestimmte Charaktere
                    werden aufgebaut und mit immer neuen Herausforderungen konfrontiert. &Uuml;berzeugt
                    der Charakter, d.h. k&ouml;nnen sich Menschen mit ihm bzw.
                    ihr identifizieren, es baut sich eine sog. Zuschauergemeinde
                    auf. Das Ziel, ist jeweils die h&ouml;chstm&ouml;gliche Zuschauerzahl
                    f&uuml;r das eigene Programm zu gewinnen. <br>
                    <br>
                    Im Unterschied zum Fernsehen muss man beim Internet nicht
                    zur gleichen Zeit die Zuschauer vor dem Schirm versammeln,
                    sondern sie m&uuml;ssen nur irgendwann einmal das eingestellte
                    Video anschauen. Das kann man dadurch unterst&uuml;tzen,
                    indem man &uuml;ber die Suchmaschinen f&uuml;r ein eingestelltes
                    Video ein hohes Ranking erzielt. Ziel ist auch hier, f&uuml;r
                    das eigene Programm auf dem eigenen Internetauftritt m&ouml;glichst
                    viele Zuschauer zu versammeln. Das erh&ouml;ht wieder das
                    Ranking bei den Suchmaschinen. Was bringt zu dieser Strategie
                    die Long-Tail-Philosophie hinzu? <br>
                    <br>
                    Der Schwanz wird weiter dadurch verl&auml;ngert, dass das
                    Video nicht nur auf der eigenen Homepage zu finden ist, sondern
                    auf <strong>m&ouml;glichst vielen anderen</strong>. Das erscheint widersinnig,
                    denn dadurch schw&auml;cht man auf den ersten Blick die eigene
                    Homepage. Das w&auml;re so, als w&uuml;rde ein Fernsehsender
                    seine Serie anderen Kan&auml;len zur Verf&uuml;gung stellen,
                    damit diese dann mit dem Programm f&uuml;r sich selbst Werbegelder
                    generieren und Produktionskosten sparen. Das ist aber nur
                    auf den ersten Blick so, denn Long Tail verl&auml;ngert nicht
                    den Schwanz eines anderen Hundes, sondern den eigenen. Dem
                    Internet liegt n&auml;mlich eine <strong>andere Kommunikationsstruktur </strong>zugrunde als dem Fernsehen. Der Long-Tail-Effekt wirkt n&auml;mlich
                    auch so, dass das, was von der eigenen Homepage ausgeht,
                    wieder zur&uuml;ck kommt &#8211; &uuml;ber andere Links,
                    die auf die eigene Seite geschaltet werden. <br>
                    Bei Videos, die kostenlos zur Verf&uuml;gung gestellt werden,
                    ist sowieso Ziel, m&ouml;glichst viele Nutzer zu erreichen.
                    Wenn mit den Videos Werbung transportiert wird, erh&ouml;ht
                    das die Reichweite der Werbung. Dadurch werden mehr Klicks
                  eingesammelt. </p>
                </font>                  <p><font face="Arial, Helvetica, sans-serif">Der
                    Begriff Longtail (Hundeschwanz oder Rattenschwanz) leitet
                    sich von der Form
                      der <strong>Verteilungskurve</strong> ab. Am Beginn ist die Nutzung in der
                    Regel am h&ouml;chsten,
                    vor allem wenn das Angebot beworben wird, dann nehmen die
                    Zugriffe ab. Kann man den Schwanz verl&auml;ngern, addieren
                    sich auch kleine Nutzerzahlen.<br>
                    Die Longtail-Theory wurde 2004 von dem Journalisten <strong><a href="http://www.thelongtail.com/" target="_blank">Chris
                    Anderson</a></strong><a href="http://www.thelongtail.com/"> </a>vorgestellt. Die Konzeption geht davon aus, dass
                    Wettbewerber auf dem Markt durch das Anbieten von Nischenprodukten
                    Gewinn machen. F&uuml;hrt diese Strategie auf lokal begrenzten
                    M&auml;rkten nur schwerlich zu einem Gewinn, da die Nachfrage
                    hier meist gering ist, so ist die <strong>globale Nachfrage</strong> auf dem
                    Markt sehr flexibel und in Bezug auf das Nischenprodukt sehr
                    viel h&ouml;her. Durch das Internet erh&auml;lt der Anbieter
                    die M&ouml;glichkeit, mit seinem Nischenprodukt auf den globalen
                    Markt in viele Nischen zu sto&szlig;en. Dadurch, dass sein
                    Angebot im Internet gestreut wird, erreicht er potentielle
                    Nachfrager, die er lokal nicht erreichen w&uuml;rde. <br>
                    Bestes Beispiel hierf&uuml;r ist Ebay: W&uuml;rde jemand
                    versuchen, in seiner Nachbarschaft alte Schallplatten zu
                    verkaufen, wird er wahrscheinlich erfolglos sein. Stellt
                    er sie jedoch bei Ebay ein, erreicht er ein zerstreutes,
                    aber gro&szlig;es Publikum und damit wachsen seine Chancen,
                    diese Schallplatten zu verkaufen. Wenn die Nachfrage steigt,
                    kann auch der Preis steigen.</font><br>
                </p></td>
                <td valign="top"><script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
google_ad_width = 160;
google_ad_height = 600;
google_ad_format = "160x600_as";
google_ad_type = "text_image";
//2007-09-05: Bildung und Erziehung
google_ad_channel = "3955521420";
google_ui_features = "rc:6";
//-->
            </script>
                    <script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
              </script>
                </td>
              </tr>
            </table>
            <p class=MsoNormal>&copy; <font size="2" face="Arial, Helvetica, sans-serif">www.kath.de</font></P>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
         <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td align="left" valign="top"><table width="216" border="0" cellpadding="0" cellspacing="0">
       <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10">
            <?php include("az.html"); ?>
          </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</body>
</html>
