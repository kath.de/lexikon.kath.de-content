<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Internet - das Besondere des Mediums - www.kath.de</title>
<meta name="title" content="Medien&Öffentlichkeit">
<meta name="author" content="kath.de">
<meta name="publisher" content="kath.de">
<meta name="copyright" content="kath.de">
<meta name="description" content="Stichworte über Medien und Öffentlichkeitsarbeit">
<meta name="abstract" content="Stichworte über Medien und Öffentlichkeitsarbeit">
<meta http-equiv="content-language" content="de">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="date" content="2006-00-02">
<meta name="robots" content="index,follow">
<meta name="revisit-after" content="30 days">
<meta name="revisit" content="after 30 days">
<meta name="DC.Title" content="Medien&Öffentlichkeit">
<meta name="DC.Creator" content="kath.de">
<meta name="DC.Contributor" content="N.N.">
<meta name="DC.Rights" content="kath.de">
<meta name="DC.Publisher" content="kath.de">
<meta name="DC.Date" content="2006-00-02">
<meta name="DC.Description" content="Stichworte über Medien und Öffentlichkeitsarbeit">
<meta name="DC.Language" content="de">
<meta name="DC.Type" content="Text">
<meta name="DC.Format" content="text/html">
<meta name="DC.Identifier" content="http://www.kath.de/kurs/medien_oeffentlichkeit/">
<meta name="keywords" lang="de" content="Theologiestudium, Medien, Kommuniaktionsberuf, Drehbücher, Lokalredaktion, Journalismus">

<link rel="stylesheet" type="text/css" href="kaltefleiter.css" title="fonts">
</head>
<body bgcolor="#ffffff" leftmargin="6" topmargin="6" marginwidth="6" marginheight="6">
<table width="100%" border="0" cellspacing="0" cellpadding="6">
  <tr>
    <td width="100" align="left" valign="top"><table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b>Medien und &Ouml;ffentlichkeit</b></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td>
            <?php include("logo.html"); ?>
          </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
    <td rowspan="2" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2">
            <h1> <font face="Arial, Helvetica, sans-serif">Medien&amp;&Ouml;ffentlichkeit</font></h1>
          </td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="L12"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td valign="top" class="L12"><p class="text"><font face="Arial, Helvetica, sans-serif"><strong>Internet
                      - das Besondere des Mediums</strong></font></p>
                  <p class="text"><font face="Arial, Helvetica, sans-serif">Das
                      Internet hat eigentlich nicht so viel Eigenes. Die Homepage
                      ist nicht mehr als ein Schaukasten. Wikipedia
                    ist keine andere Idee, als das Wissen in Lexika zusammenzustellen.
                    Die Zeitungen ver&ouml;ffentlichen eine Onlineausgabe. Das
                    Forum ist erst mal auch nur eine Leserbriefseite. Diskussionen
                    hat es im Radio und Fernsehen auch schon gegeben, sogar Zuschauer
                    konnten zugeschaltet werden, als es noch kein Internet gab.
                    Auch wenn Musikst&uuml;cke, Filme und inzwischen Fernsehprogramme &uuml;ber
                    das zu DSL beschleunigte Telefonkabel auf den Bildschirm
                    und in die Lautsprecherboxen gespielt werden &#8211; es sind
                    alles Medienformen, Formate, die es vorher schon gab. Es
                    scheint, dass das Neue allein darin besteht, dass das Internet
                    alles auf den gleichen Bildschirm bringt. Dann w&auml;re
                    das Neue technisch zu verstehen: die Digitalisierung zerlegt
                    nicht nur Texte, sondern auch Fotos, Musikst&uuml;cke, Filmsequenzen
                    in Bits, so dass alles &uuml;ber ein Kabel den Weg auf jeden
                    Bildschirm der Welt findet, der an ein Telefonkabel angeschlossen
                    ist. Dass die Menschen das Internet nutzen, geschieht jedoch
                    nicht aus Begeisterung f&uuml;r digitale Daten&uuml;bertragung,
                    sondern es sind neue Formen der Kommunikation. Chatten, ein
                    Forum besuchen, sich bestimmte Musikst&uuml;cke herunterladen,
                    das alles ruht auf dem Vernetzungscharakter des Neuen Mediums
                    auf.</font></p>
                  <p class="text"><font face="Arial, Helvetica, sans-serif">Das
                      Internet ist kein neues Medium, sondern es ver&auml;ndert
                      die Mediennutzung<br>
                    Zeitung, ob gedruckt oder als Online-Ausgabe, Filme, Fernsehserien
                    und -Nachrichten kommen immer von au&szlig;en auf den Nutzer zu. Dieser holt sich die Zeitung
  aus dem Briefkasten, leiht sie sich als DVD aus, verfolgt eine Serie, die auf
  seinen Fernseher &uuml;bertragen wird. Es sind immer Angebote, auf die der
  einzelne zugreift oder nicht. In der Regel erf&auml;hrt er nicht, was andere
  mit den gleichen Inhalten machen. Es gibt nur einige Hinweise, so bei Musiktiteln
  die sog. Charts, die die erfolgreichen St&uuml;cke auflisten. Es gibt Film-
  und Fernsehkritiken und Zeitungen r&auml;umen den Lesern f&uuml;r ihre Zuschriften
  eine Leserbriefseite ein. Das Kino machte fr&uuml;her davon schon eine Ausnahme.
  Denn die Kinog&auml;nger sorgen bis heute durch Mund-zu-Mund-Propaganda daf&uuml;r,
  ob der Film ein Erfolg oder ein Flop wird. Das war und ist m&ouml;glich, weil
  der Film nicht nur einmal gezeigt wird, sondern mindestens eine Woche lang.
  Mund-zu-Mund-Propaganda kann daher wirksam werden. &Auml;hnlich h&auml;ngt
  der Erfolg eines Buchtitels von der Weiterempfehlung der Leser ab. Dieses Weitempfehlen
  f&uuml;hrt zu Erfolgen, die meist kein professioneller Kritiker vorausgesehen
  hat. Es gibt Filme, die die Kritik als unbedeutend eingestuft hat, die dann
  doch &uuml;ber Wochen in den Kinos laufen. Harry Potter ist das markanteste
  Beispiel im Buchmarkt. Niemand hat den B&uuml;chern &uuml;ber den Zauberlehrling
  einen solchen Erfolg vorausgesagt. Was bisher durch direkte Gespr&auml;che
  ausgel&ouml;st wurde, macht das Internet f&uuml;r alle Inhalte m&ouml;glich.
  Was bisher nur f&uuml;r das Kino und den Buchmarkt funktionierte, wird zum
  Prinzip des Neuen Mediums</font></p>
                  <p class="text"><font face="Arial, Helvetica, sans-serif">Das Internet ist das Medium gegenseitiger Empfehlungen
                    durch seine Nutzer <br>
                    Das Internet ist erst einmal ein riesiges Archiv ohne Archivar.
                    Jeder stellt seine Texte, Nachrichten, Fotos, Videos ins
                    Netz. Anf&auml;nglich hat man versucht,
  durch Kataloge Ordnung in den t&auml;glich wachsenden Bestand zu bringen. Wie
  in einer gro&szlig;en Bibliothek sollte man sich durch den Katalog und dann &uuml;ber
  die nutzerfreundlich aufgebaute Homepage hindurch bis zu dem Text vortasten,
  den man sucht. Dann kamen die Surfer. Sie durchstreiften das Netz ziellos,
  stie&szlig;en auf interessante Fundst&uuml;cke. Wenn sie ihre eigene Homepage
  interessanter machen wollten, verlinkten sie auf die Seiten, die sie f&uuml;r
  lesenswert hielten. So bildeten sich langsam kleine Inseln im gro&szlig;en
  Meer des Internets. Yahoo, Lycos und dann Google machten daraus eine Gesch&auml;ftsidee.
  Auf welche Seiten viele Links verweisen, die werden bei den Suchmaschinen oben
  gelistet. <br>
  Da immer mehr Menschen im Internet suchen, vor allem die j&uuml;ngeren Generationen,
  sind alle, die Gedanken, B&ouml;rsenkurse, Nachrichten, Seminare, B&uuml;cher,
  Autos, Reisen anbieten wollen, gezwungen, im Internet auffindbar zu sein. So
  zieht das Netz alle in seinen Einflussbereich. <br>
  Von den bisherigen Medien unterscheidet sich das Internet vor allem dadurch,
  dass es keine Gate-Keeper mehr gibt. Kein &#8222;T&uuml;rsteher&#8220; d.h.
  kein Lektor kann ein Buchmanuskript ablehnen, kein Redakteur entscheidet mehr
  dar&uuml;ber, ob ein Bericht &uuml;ber die Karnevalssitzung des Sportvereins,
  das Zeltlager der Jugendgruppe, den Vortrag des Bildungswerkes &#8222;erscheinen&#8220; darf.
  Seit die &Uuml;bertragungsgeschwindigkeit des Internets ausreicht, um Bewegtbilder
  herunterzuladen, kann auch jeder sein Video ins Netz stellen. Daraus entstanden
  mit Youtube u.a. bereits die ersten Internetsender.<br>
  Anders als die Zeitung liefert das Internet nicht t&auml;glich eine dosierte
  Menge Text, die man in etwa 30 Minuten Lesezeit, die die Deutschen durchschnittlich
  am Tag der Zeitung widmen, verarbeiten k&ouml;nnte. Die Nutzer k&ouml;nnen
  das Netz immer weniger &#8222;handhaben&#8220;, weil das Meer immer gr&ouml;&szlig;er.
  Denn es gibt niemand, der wie der Redakteur einer Zeitung die Text- und Bildmengen
  begrenzt, sondern jeder kann das Meer auff&uuml;llen. Das Datenmeer bleibt
  allerdings nicht sich selbst &uuml;berlassen, sondern wird laufend durch die
  Filteranlage der Suchmaschinen geschleust und in immer differenziertere Listen
  einsortiert. Wenn der Nutzer durch Wortkombinationen nicht blo&szlig; allgemeine
  Fragen z.B. nach &#8222;Gottesdienst&#8220;, sondern gezielt z.B. nach der &#8222;Leseordnung
  der evangelischen Kirche&#8220; sucht, findet er die Seiten, die seine Frage
  beantworten, auf den ersten Pl&auml;tzen der Suchliste. Anders als ein Bibliothekskatalog
  bieten die Suchmaschinen nicht nur den Zugang &uuml;ber den Namen des Autors,
  sondern, wie computergest&uuml;tzte Kataloge &uuml;ber Schlagw&ouml;rter, Teile
  des Buchtitels und noch viel mehr. Denn Begriffe, die nicht nur im Titel der
  Seite angezeigt werden, sondern sich in den ersten 15 Zeilen wiederholt finden,
  dienen als Anker, um die Seiten auf einer Suchliste anzuzeigen. <br>
  Wie Bibliotheken beliebig vergr&ouml;&szlig;erbar sind, kann auch das Internet
  beliebig viele Daten speichern und jedem Nutzer weltweit zug&auml;nglich machen.
  Wahrscheinlich bilden sich, wie bei der Entstehung unserer Erde, Kontinente
  heraus, nicht geographisch, sondern nach Wissensgebieten und, wie die Kontinente
  der Erde, nach V&ouml;lkern geordnet sind. Wahrscheinlich werden sich die Sprachgrenzen
  erhalten und nur einige der Internet-Gemeinschaften weltweit verzweigt sein,
  so wie heute die Naturwissenschaften und auch zunehmend die geisteswissenschaftliche
  Forschung. </font></p>
                  <p class="text"><font face="Arial, Helvetica, sans-serif">Mehr Nutzer werden zu Autoren <br>
  Durch die Verlinkungstechnik empfehlen sich die Nutzer gegenseitig Homepages,
    Musiktitel, Videofilme. Nur weil die Nutzer empfehlen, kommt es zu einem
    Ranking der Suchmaschinen. &Uuml;ber das Empfehlen hinaus k&ouml;nnen die
    Nutzer auch selbst produktiv werden. Technisch ist das kein Problem. Jeder
    kann Texte, Fotos, Videos ins Netz stellen. Aber das tut nicht jeder, einfach
    weil man im B&uuml;ro genug Arbeit hat oder der Haushalt gemacht werden will.
    Allenfalls die Textproduzenten wie Sch&uuml;ler, Studierende und Wissenschaftler
    stellen von dem, was sie sowieso erarbeitet haben, etwas ins Netz. Vorher
    muss die Barriere &uuml;berwunden werden, etwas in den PC zu tippen. Man
    muss nicht nur eine Idee haben, etwas erz&auml;hlen k&ouml;nnen, sondern
    auch die Fertigkeit, aus Gedachtem oder Erz&auml;hltem einen Text zu machen.
    Viele haben eine Videokamera, deren Qualit&auml;t f&uuml;r das Internet ausreicht,
    aber nur einige bearbeiten ihre Filme, um sie ins Netz zustellen. An Foren,
    die keine hohen Anforderungen an die Textqualit&auml;t stellen, ist zu beobachten,
    dass sehr viel mehr lesen als schreiben. Das Forum my-kath hat etwa bitte
    Zahl einsetzen Nutzer, nur etwa 300 setzen Beitr&auml;ge zu Themen in die
    daf&uuml;r vorgesehen Felder. Es wird also nicht jeder Nutzer zum Produzenten. <br>
    Da das Internet wie jedes andere Medium auf die Dauer von der Qualit&auml;t
  lebt, wird es auf gute Beitr&auml;ge ankommen. Journalisten wie Drehbuchautoren
  m&uuml;ssen sich an eine neue Konkurrenz gew&ouml;hnen. F&uuml;r die Institutionen,
  ob Kirchen, Gewerkschaften, Sportvereine wird das Netz aber genauso wie f&uuml;r
  die klassischen Medien ein Umdenken notwendig machen. </font></p>
                <p><font face="Arial, Helvetica, sans-serif"><span class="text">Eckhard Bieger</span></font></p></td>
                <td valign="top"><script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
google_ad_width = 160;
google_ad_height = 600;
google_ad_format = "160x600_as";
google_ad_type = "text_image";
//2007-09-05: Bildung und Erziehung
google_ad_channel = "3955521420";
google_ui_features = "rc:6";
//-->
            </script>
                    <script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
              </script>
                </td>
              </tr>
            </table>
            <p class=MsoNormal> <font face="Arial, Helvetica, sans-serif"><strong> </strong></font></P>
            <p class=MsoNormal>&copy; <font size="2" face="Arial, Helvetica, sans-serif">www.kath.de</font></P>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
         <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td align="left" valign="top"><table width="216" border="0" cellpadding="0" cellspacing="0">
       <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10">
            <?php include("az.html"); ?>
          </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</body>
</html>
