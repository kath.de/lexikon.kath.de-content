<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Kommunikative Funktionen des Internets</title>
<meta name="title" content="Medien&Öffentlichkeit">
<meta name="author" content="kath.de">
<meta name="publisher" content="kath.de">
<meta name="copyright" content="kath.de">
<meta name="description" content="Stichworte über Medien und Öffentlichkeitsarbeit">
<meta name="abstract" content="Stichworte über Medien und Öffentlichkeitsarbeit">
<meta http-equiv="content-language" content="de">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="date" content="2006-00-02">
<meta name="robots" content="index,follow">
<meta name="revisit-after" content="30 days">
<meta name="revisit" content="after 30 days">
<meta name="DC.Title" content="Medien&Öffentlichkeit">
<meta name="DC.Creator" content="kath.de">
<meta name="DC.Contributor" content="N.N.">
<meta name="DC.Rights" content="kath.de">
<meta name="DC.Publisher" content="kath.de">
<meta name="DC.Date" content="2006-00-02">
<meta name="DC.Description" content="Stichworte über Medien und Öffentlichkeitsarbeit">
<meta name="DC.Language" content="de">
<meta name="DC.Type" content="Text">
<meta name="DC.Format" content="text/html">
<meta name="DC.Identifier" content="http://www.kath.de/kurs/medien_oeffentlichkeit/">
<meta name="keywords" lang="de" content="Internet, Fernsehen, Zeitung, Kommunikation, Communites, Medienvergleich">

<link rel="stylesheet" type="text/css" href="kaltefleiter.css" title="fonts">
</head>
<body bgcolor="#ffffff" background="dstone1.gif" leftmargin="6" topmargin="6" marginwidth="6" marginheight="6">
<table width="100%" border="0" cellspacing="0" cellpadding="6">
  <tr>
    <td width="100" align="left" valign="top"><table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b>Medien und &Ouml;ffentlichkeit</b></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#FFFFFF">
            <?php include("logo.html"); ?>
          </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
    <td rowspan="2" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2">
            <h1> <font face="Arial, Helvetica, sans-serif">Die
            kommunikativen Funktionen des Internets</font></h1>
          </td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="L12"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td valign="top" bgcolor="#FFFFFF" class="L12"><p class=MsoNormal><font face="Arial, Helvetica, sans-serif" size="3"></font><br>
                  <font face="Arial, Helvetica, sans-serif"><strong>Die
                        Leistungen des Internets im Vergleich zu Zeitung und
                        Fernsehen</strong></font></P>
                  <p><font face="Arial, Helvetica, sans-serif">Um das Internet
                      zu verstehen, kann ein Vergleich mit den klassischen Medien
                      die Erkennungsmarken bereitstellen, mit denen die kommunikativen
                      Funktionen des Internets genauer erfa&szlig;t werden k&ouml;nnen.
                      Sie &auml;hneln in Vielem den Funktionen klassischer Medien,
                      jedoch ist das Internet etwas Neues, keines der bisher
                      gebrauchten Massenmedien. Es bietet sehr viel mehr Funktionen
                      und erm&ouml;glicht auch kleinsten Gruppen eine weltweite
                      Vernetzung. Das wird am Vergleich mit den Zeitung und Fernsehen
                      deutlich. </font></p>
                  <p><font face="Arial, Helvetica, sans-serif">Die <strong>Zeitung</strong> sagt
                      bereits im Begriff, was sie leistet, n&auml;mlich das darzustellen,
                      was sich &#8222;zeitigt&#8220;. Deshalb sind periodisches
                      Erscheinen, m&ouml;glichst jeden Tag, und Aktualit&auml;t
                      entscheidend f&uuml;r den Nutzwert der Zeitung. Sie deckt
                      vor allem das Lokale ab, weiter die politischen Entwicklungen,
                      die Vorg&auml;nge im Wirtschaftsleben, B&ouml;rsen- und
                      Rohstoffkurse sowie den Status einzelner Firmen. Kulturelle
                      Ereignisse wie Neuinszenierungen, Kinostarts, Konzert-
                      und Buchkritiken sowie Analysen der gesellschaftlichen
                      Entwicklungen finden sich im Feuilleton. Sport und Reiseinformationen
                      runden neben R&auml;tsel, Fortsetzungsroman u.a. das Angebot
                      der Zeitung ab.</font></p>
                  <p><font face="Arial, Helvetica, sans-serif">Das <strong>Fernsehen</strong> funktioniert
                      als Geschichtenerz&auml;hler. In den t&auml;glichen oder
                      w&ouml;chentlichen Serien werden Endlosgeschichten erz&auml;hlt.
                      Mit Fernsehspielen und Kinofilmen werden die Zuschauer &uuml;ber
                      gro&szlig;e Zeit-Strecken gewonnen. Auch Turniere und Sportwettk&auml;mpfe
                      sind in eine gr&ouml;&szlig;ere Inszenierung eingebaut,
                      denn es geht bei Turnieren um den Endkampf, bei Autorennen
                      und Fu&szlig;ballspielen um den Jahressieg. Viele Shows
                      haben Wettkampfelemente und damit eine Dramaturgie, die
                      sich um Sieg oder Niederlage dreht. Wenn das Fernsehen
                      preiswert gemacht werden mu&szlig;, sendet es Talkrunden
                      oder spielt Gerichtsprozesse nach. Hinter allem steht die
                      Frage des Gelingens. W&auml;hrend die Zeitung durch Wiedergabe
                      der relevanten Vorg&auml;nge bis hin zum Wetterbericht
                      Orientierung bietet, kann der Zuschauer mit dem Fernsehen
                      Gelingen und Mi&szlig;lingen durchspielen. Denn ohne das
                      Risiko des Mi&szlig;lingens gibt es keine Dramaturgie.
                      Da der Zuschauer sich mit Helden identifizieren kann, geht
                      es zuerst nicht um das Gelingen des eigenen Lebens, sondern
                      um das Leben des Helden. Doch im Durchspielen der Geschichten
                      spielt der Zuschauer auch sein eigenes Leben auf Gelingen
                      und Mi&szlig;lingen hin durch.</font></p>
                  <p><font face="Arial, Helvetica, sans-serif">Das <strong>Radio</strong> bietet
                      im Ablauf des Tages Nachrichten, f&uuml;r Autofahrer Staumeldungen
                      und den Wetterbericht.<br>
  Als nebenbei genutztes Medium dient es durch den Musikflu&szlig; der emotionalen
  Abst&uuml;tzung. Seine Funktion, Geschichten zu erz&auml;hlen, hat das Radio
  weitgehend an das Fernsehen verloren.</font></p>
                  <p><font face="Arial, Helvetica, sans-serif">Das Internet wurde
                      im Sinne der Wissensgesellschaft entwickelt. Alles Wissen
                      soll jedem zug&auml;nglich gemacht werden. Daraus folgt
                      die Funktion</font></p>
                  <p><font face="Arial, Helvetica, sans-serif"><strong>Verl&auml;ngertes
                        Gehirn</strong><br>
  Besser als eine Bibliothek bietet das Internet Zugang zu den verschiedensten
  Wissensquellen, auch zu Dokumenten, Zeitungsberichten und inzwischen auch zu
  Filmarchiven. Wikipedia, das von den Internetnutzern selbst ausgearbeitete
  Lexikon, steht f&uuml;r diese Funktion des Netzes.<br>
  Unter dem Stichwort &#8222;<a href="internet_medienvergleich.php">Das Internet
  im Medienvergleich</a>&#8220; wird aufgezeigt, wie das Internet in der Form
  von Onlinezeitungen, abrufbaren Radioprogrammen u.a. Verbreitungsplattform
  f&uuml;r die klassischen Medien geworden ist. Das Internet bleibt aber nicht
  Verteilstation, sondern bietet </font></p>
                  <p><font face="Arial, Helvetica, sans-serif"><strong>Kommunikation
                        und Communities</strong><br>
  Im Internet k&ouml;nnen Leser, H&ouml;rer und Zuschauer einen Bericht, ein
  Programm sehr viel einfacher diskutieren als wenn sie einen Leserbrief schreiben.
  Das Fernsehen mit seiner schwerf&auml;lligen Produktionsapparatur ist f&uuml;r
  die Kommunikation mit dem Zuschauer noch weniger geeignet. Es kann allerdings
  den &uuml;ber das Telefon verbundenen Zuschauer in eine Live-Sendung einspielen.
  Das Internet bietet mit Foren, Chats und Email sehr viel mehr M&ouml;glichkeiten
  der Beteiligung und hat die Schwelle, sich &uuml;berhaupt zu Wort zu melden,
  erheblich abgesenkt.<br>
  Weiter erm&ouml;glicht das Internet, Menschen zu treffen, die nicht in der
  Nachbarschaft wohnen. Interessengruppen, die sich z.B. um bestimmte Filme,
  Computerspiele, Reiseziele u.a. versammeln, k&ouml;nnen sich &uuml;ber&ouml;rtlich
  vernetzen. Menschen, die unter einer seltenen Krankheit leiden, k&ouml;nnen
  mittels des Internets eine Community bilden. Nicht zu untersch&auml;tzen ist
  die kommunikative Funktion des Netzes f&uuml;r Menschen, die nicht in einer
  Gro&szlig;stadt leben und dort zu Fu&szlig; oder mit dem &ouml;ffentlichen
  Nahverkehr spezialisierte Angebote wahrnehmen k&ouml;nnen.<br>
  Das Internet mit seinen neuen Formaten macht Bildung breiter zug&auml;nglich
  und bietet neue formen, Wissen zu vertiefen. s.<a href="http://www.kath.de/kurs/bildung_erziehung/internet_weiterbildung.php">Internet
  und Weiterbildung</a></font></p>
                  <p><font face="Arial, Helvetica, sans-serif"><strong>Medienproduzent
                        werden</strong><br>
  Anders als Zeitung, Radio und Fernsehen erm&ouml;glicht das Internet, selbst
  Beitr&auml;ge zu produzieren, auf einer eigenen Homepage, als Download von
  Gesprochenem und Gefilmten. Die Kosten f&uuml;r die Produktion sind so gering,
  da&szlig; niemand durch fehlende technische oder finanzielle Mittel gehindert
  werden kann, das zu ver&ouml;ffentlichen, was er zu sagen hat.</font></p>
                  <p><font face="Arial, Helvetica, sans-serif">Eckhard Bieger
                      in Zusammenarbeit mit Michael Belzer, Stefan Kemmerling,
                      Manfred Lay, J&uuml;rgen Pelzer</font></p>
                <p><font face="Arial, Helvetica, sans-serif">&copy; <font size="2">www.kath.de</font></font></p></td>
                <td valign="top"><script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
google_ad_width = 160;
google_ad_height = 600;
google_ad_format = "160x600_as";
google_ad_type = "text_image";
//2007-09-05: Bildung und Erziehung
google_ad_channel = "3955521420";
google_ui_features = "rc:6";
//-->
            </script>
                    <script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
              </script>
                </td>
              </tr>
            </table>
            <p class=MsoNormal>&nbsp;</P>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
         <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td align="left" valign="top"><table width="216" border="0" cellpadding="0" cellspacing="0">
       <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#FFFFFF" class="V10">
            <?php include("az.html"); ?>
          </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</body>
</html>
