<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<head profile="http://dublincore.org/documents/dcq-html/">

<link rel="schema.DC" href="http://purl.org/dc/elements/1.1/">
<link rel="schema.DCTERMS" href="http://purl.org/dc/terms/">
<link href="lexikon-kirche-internet.css" rel="stylesheet" type="text/css">

<title>Second Life :: Das Lexikon f&uuml;r Kirche&amp;Internet :: Kirche im Web 2.0</title>
<meta name="robots" content="index,follow,all" />
<meta name="description" content="Second Life, Kirche, Internet, Web 2.0, Religion, Glauben, Internetkirche, Netzkirche">
<meta name="author" content="kath.de">
<meta name="keywords" content="Second Life, Kirche, Internet, Web 2.0, Religion, Glauben, Internetkirche, Netzkirche">


<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="DC.title" content="Second Life, Kirche, Internet, Web 2.0, Religion, Glauben, Internetkirche, Netzkirche">

<meta name="DC.creator" content="Second Life, Kirche, Internet, Web 2.0, Religion, Glauben, Internetkirche, Netzkirche">
<meta name="DC.subject" content="Second Life, Kirche, Internet, Web 2.0, Religion, Glauben, Internetkirche, Netzkirche">
<meta name="DC.description" content="Second Life, Kirche, Internet, Web 2.0, Religion, Glauben, Internetkirche, Netzkirche">
<meta name="DC.publisher" content="kath.de">
<meta name="DC.contributor" content="kath.de">
<meta name="DC.date" content="2007-05-15T08:49:37+02:00" scheme="DCTERMS.W3CDTF">
<meta name="DC.type" content="Text" scheme="DCTERMS.DCMIType">
<meta name="DC.format" content="text/html" scheme="DCTERMS.IMT">
<meta name="DC.identifier"
      content="http://www.kath.de/lexikon/kircheundinternet/second_life_lexikon_kirche_im_internet.php"
      scheme="DCTERMS.URI">
<meta name="DC.source"
      content="http://www.w3.org/TR/html401/struct/global.html#h-7.4.4"
      scheme="DCTERMS.URI">
<meta name="DC.language" content="de" scheme="DCTERMS.RFC3066">
<meta name="DC.relation" content="http://dublincore.org/" scheme="DCTERMS.URI">
<meta name="DC.coverage" content="Munich" scheme="DCTERMS.TGN">
<meta name="DC.rights" content="Alle Rechte liegen beim Autor">
</head>

<body background="images-kirche-internet/kirche-internet-background.jpg" link="#0066FF" vlink="#666666" alink="#00FF00">
<table width="900" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="6">
      <tr>
        <td width="216" align="left" valign="top" height="3">
          <table width="216" border="0" cellpadding="0" cellspacing="0">
            <tr valign="top" align="left">
              <td width="8"><img src="boxtopleftcorner.jpg" width="8" height="8" alt=""></td>
              <td width="200" background="boxtop.jpg"><img src="boxtop.jpg" alt="" width="8" height="8"></td>
              <td width="8"><img src="boxtoprightcorner.jpg" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="boxtopleft.jpg"><img src="boxtopleft.jpg" width="8" height="8" alt=""></td>
              <td bgcolor="0080bb"><p><b><font color="#FFFFFF" face="Arial, Helvetica, sans-serif">Lexikon
                      Kirche&amp;Internet</font></b></p>
              </td>
              <td background="boxtopright.jpg"><img src="boxtopright.jpg" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="boxdividerleft.jpg" width="8" height="13" alt=""></td>
              <td background="boxdivider.jpg"><img src="boxdivider.jpg" alt="" width="8" height="13"></td>
              <td><img src="boxdividerright.jpg" width="8" height="13" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
              <td bgcolor="#ffffff">
                <?php include("logo.html"); ?>
              </td>
              <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
              <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
              <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
            </tr>
          </table>
          <br>
          <table width="216" border="0" cellpadding="0" cellspacing="0">
            <tr valign="top" align="left">
              <td width="8"><img src="boxtopleftcorner.jpg" width="8" height="8" alt=""></td>
              <td width="200" background="boxtop.jpg"><img src="boxtop.jpg" alt="" width="8" height="8"></td>
              <td width="8"><img src="boxtoprightcorner.jpg" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="boxtopleft.jpg"><img src="boxtopleft.jpg" width="8" height="8" alt=""></td>
              <td bgcolor="0080bb"><font color="#FFFFFF" face="Arial, Helvetica, sans-serif"><strong>In
                    diesem Artikel</strong></font></td>
              <td background="boxtopright.jpg"><img src="boxtopright.jpg" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="boxdividerleft.jpg" width="8" height="13" alt=""></td>
              <td background="boxdivider.jpg"><img src="boxdivider.jpg" alt="" width="8" height="13"></td>
              <td><img src="boxdividerright.jpg" width="8" height="13" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
              <td bgcolor="#ffffff" class="V10">
                <?php include("sprungmarken.html"); ?>
              </td>
              <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
              <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
              <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
            </tr>
          </table>          <br>
          <table width="216" border="0" cellpadding="0" cellspacing="0">
            <tr valign="top" align="left">
              <td width="8"><img src="boxtopleftcorner.jpg" width="8" height="8" alt=""></td>
              <td width="200" background="boxtop.jpg"><img src="boxtop.jpg" alt="" width="8" height="8"></td>
              <td width="8"><img src="boxtoprightcorner.jpg" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="boxtopleft.jpg"><img src="boxtopleft.jpg" width="8" height="8" alt=""></td>
              <td bgcolor="0080bb"><font color="#FFFFFF" face="Arial, Helvetica, sans-serif"><strong>Weitere
                    Artikel</strong></font></td>
              <td background="boxtopright.jpg"><img src="boxtopright.jpg" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="boxdividerleft.jpg" width="8" height="13" alt=""></td>
              <td background="boxdivider.jpg"><img src="boxdivider.jpg" alt="" width="8" height="13"></td>
              <td><img src="boxdividerright.jpg" width="8" height="13" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
              <td bgcolor="#ffffff" class="V10">
                <?php include("az.html"); ?>
              </td>
              <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
              <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
              <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
            </tr>
          </table>
        </td>
        <td width="530" rowspan="2" valign="top">          <img src="boxleft.gif" width="8" height="8" alt="">
          <table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr valign="top" align="left">
            <td width="8"><img src="boxtopleftcorner.jpg" width="8" height="8" alt=""></td>
            <td background="boxtop.jpg"><img src="boxtop.jpg" alt="" width="8" height="8"></td>
            <td width="8"><img src="boxtoprightcorner.jpg" width="8" height="8" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td background="boxtopleft.jpg" width="8"><img src="boxtopleft.jpg" width="8" height="8" alt=""></td>
            <td bgcolor="0080bb">
              <h1><font color="#FFFFFF" face="Arial, Helvetica, sans-serif">Second
                  Life &amp; Kirche<span class="&uuml;berschrift"><a name="inhaltsuebersicht"></a></span></font></h1>
            </td>
            <td background="boxtopright.jpg" width="8"><img src="boxtopright.jpg" width="8" height="8" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td width="8"><img src="boxdividerleft.jpg" width="8" height="13" alt=""></td>
            <td background="boxdivider.jpg"><img src="boxdivider.jpg" alt="" width="8" height="13"></td>
            <td width="8"><img src="boxdividerright.jpg" width="8" height="13" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td width="8" height="905" background="boxleft.gif">&nbsp;</td>
            <td bgcolor="#ffffff" class="L12">              <p class="text">    <img SRC="images-kirche-internet/second-life-kirche-internet.jpg" style="border: double 4px #009999" align="right" hspace="10" vspace="10" name="Second Life aus dem Lexikon Kirche und Internet" alt="Second Life aus dem Lexikon Kirche und Internet" ><span class="&uuml;berschrift"><img src="caritas_mannheim_iconpunkt.gif" width="12" height="12"> Warum
                ist es wichtig?<a name="wichtig"></a></span></p>
              <p class="text">Second Life (SL) und andere 3D Welten geben eine
                Antwort auf die Frage: Wie <strong>interaktiv</strong> kann Kommunikation im Internet
                sein? Psychologen sprechen von dem Effekt der <a href="http://de.wikipedia.org/wiki/Immersion_%28virtuelle_Realit%C3%A4t%29" target="_blank"><strong>Immersion</strong></a>,
                der bei solchen 3D Welten (und Spielen) auftritt: <br>
                <br>
                Immersion meint
                das<strong> Eintauchen </strong>in bzw. Verschmelzen des Nutzers
                mit den 3D Welten. Der Monitor ist somit nicht mehr das (trennende)
                Interface in der Kommunikation
                zwischen den Nutzern (oder zwischen dem Nutzer und einem Programm),
                sondern der Nutzer verschmilzt mit dem Avatar und agiert dadurch
                in einer dreidimensionalen Welt. Diese Verschmelzung des Nutzers
                mit seinem <strong>Avatar </strong>(eine 3D Figur) l&auml;sst
                sich an ganz banalen Beobachtungen festmachen: Etwa dann, wenn
                der
                Nutzer
                schreit,
                weil der Avatar sich
                den Kopf
                gesto&szlig;en
                hat. Dementsprechend liegt der gro&szlig;e Nutzen von Second
                Life in der zus&auml;tzlichen
                Erfahrbarkeit der Inhalte und in der Pr&auml;senz im virtuellen
                Raum. Zus&auml;tzlich sind in SL alle
                M&ouml;glichkeiten
                der Kommunikation von Nutzern untereinander vereint, wie Text-Chat,
                Voice-Chat, IM (Instant Messenger) etc.<br>
                <br>
                Der Zweite Reiz von SL besteht darin, Situationen und Muster
                auszuprobieren, die im realen Leben nicht oder nur schwer m&ouml;glich
                sind. So haben zum Beispiel einige Spieler einen Avatar des anderen
                Geschlechts.                </p>
              <p class="text">                    <span class="&uuml;berschrift"><img src="caritas_mannheim_iconpunkt.gif" width="12" height="12"> Wie
                  und von wem wird es genutzt?<a name="nutzung"></a> </span><span class="unterbild"><font size="1"><a href="#inhaltsuebersicht">(Inhaltsverzeichnis)</a></font></span></p>              
              <p class="text">Viele Nutzer haben einen spielersichen Zugang zu
                Second Life. Es steht das Interesse im Vordergrund, &#8222;eine
                neue Welt&#8220; zu erkunden. Das erkl&auml;rt auch, warum viele
                Nutzer sich nach einem ersten Besuch nicht mehr einloggen: Der
                Reiz ist verflogen, sie haben nichts Besonderes gefunden.<br>
                Die
                Nutzer, die sich h&auml;ufiger
                einloggen, sch&auml;tzen vor allem die M&ouml;glichkeit sich
                mit anderen Besuchern auf der ganzen Welt auszutauschen. Gerade
                die
                Gruppenbildung
                in Second Life ist sehr stark.<br>
                <br>
                <strong>Unternehmen</strong> versuchen auch die M&ouml;glichkeiten
                von Second Life zu testen. Allerdings haben nur wenige Erfolg.
                Viele Unternehmen
                haben Second Life mittlerweile wieder verlassen (Deutsche Post,
                BMW, u.a.). Manche Beobachter gehen davon aus, dass Second Life &uuml;berhaupt
                f&uuml;r Unternehmen uninteressant ist. Stattdessen sehen sie
                den Wert in den Businessanwendungsm&ouml;glichkeiten. So k&ouml;nnte
                Second Life dazu dienen, virtuelle Meetings und Pr&auml;sentationen
                abzuhalten oder aber die M&ouml;glichkeiten des 3D Raumes zu
                nutzen, um Produktfunktionsweisen zu erkl&auml;ren, indem
                etwa begehbare 3D Modelle von Maschinen gebaut werden.<br>
                <br>
                Auch die Nutzung von Second Life in der <strong>Weiterbildung</strong>                wird
                probiert. So hat etwas die VHS Goslar eine Dependance in Second
                Life gegr&uuml;ndet und viele <strong>Universit&auml;ten</strong> experimentieren
                in Second Life. Sie halten E-Lectures oder virtuelle Siminare.
                Besonders geeignet scheint SL zu sein, um Gruppeninteraktion
                zu simulieren und zu trainieren.<br>
                <br>
                Der <strong>WWF</strong> hat f&uuml;r sein Urwaldprojekt eine
                interessante Idee in SL umgesetzt: Am Fu&szlig;e eines Regenwalds
                nimmt ein Gorilla den Avatar an die Hand und f&uuml;hrt ihn durch
                den Regenwald, indem er dem Nutzer sein Lebensraum erkl&auml;rt.<br>
                <br>
                Eine Menschenrechtsorganisation hat einen virtuellen Nachbau
                von <strong>Guantanamo Bay</strong> in Second Life erstellt.
                Der Besucher kann nun als Avatar den Tagesablauf der Gefangenen
                in Second Life am eigenen
                &quot;digitalen Leib&quot; erfahren<br>
                    <br>
                    <span class="&uuml;berschrift"><img src="caritas_mannheim_iconpunkt.gif" width="12" height="12"> Gibt
                  es spezielle religi&ouml;se Nutzung?<a name="religioes"></a> </span><span class="unterbild"><font size="1"><a href="#inhaltsuebersicht">(Inhaltsverzeichnis)</a></font></span></p>
              <p class="text">Definitv: Ja. In Second Life finden sich viele
                Religionsgemeinschaften. Einige unterhalten Gottesh&auml;user
                oder Treffpunkte. Sehr aktiv sind die sog. Emerging churches
                aus den USA. Es gibt im christlichen
                Bereich viele religi&ouml;se Gruppen in Second Life. So gibt
                es allgemeine christliche Gruppen , die um die 500 Personen umfassen
                bis hin zu sehr speziellen Gruppen, wie etwa Rosenkranzgruppen
                mit um die 50 Mitglieder. Es werden Gottesdienste abgehalten,
                Gespr&auml;chskreise, Bibelstunden, Andachten, Gebetsrunden.
                Viele Nutzer die Second Life aus religi&ouml;sen Motiven nutzen
                sch&auml;tzen den Austausch mit (anders-) oder Gleichgl&auml;ubigen
                aus der ganzen Welt.</p>
              <p class="text">Second Life bietet die M&ouml;glichkeiten, Inhalte
                auf spielerische Weise erfahrbar zu machen &#8211; in einem sozialen
                Kontext. Jederzeit sind Begegnungen mit anderen Avataren, d.h.
                mit Personen aus der ganzen Welt m&ouml;glich.<br>
                Eine der interessantesten religi&ouml;sen Nutzungsm&ouml;glichkeiten
                ist die virtuelle Hajj. Das Protal IslamOnline.net hat ein 3D
                Nachbau von allen Stationen der traditionellen Pilgerfahrt nach
                Mekka (der Hajj) in Second Life f&uuml;r die &Ouml;ffentlichkeit
                freigeschaltet. An jeder Station der Reise kann der Nutzer mit
                seinem Avatar Informationen einholen, aber auch die Handlungen
                selbst mit seinem Avatar durchf&uuml;hren. Dabei trifft er unter
                Umst&auml;nden auf andere Avatare und kann mit diesen ein Gespr&auml;ch
                (Voice Chat) oder einen Chat beginnen.<br>
                    <br>
                    <span class="&uuml;berschrift"><img src="caritas_mannheim_iconpunkt.gif" width="12" height="12"> Was
                    sind die Implikationen f&uuml;r Kirche und Glaube?</span><a name="implikationen"></a><span class="&uuml;berschrift"> </span><span class="unterbild"><font size="1"><a href="#inhaltsuebersicht">(Inhaltsverzeichnis)</a></font></span><br>
                    <br>
                    Es ist zu beobachten, dass sich religi&ouml;s orientierte
                    Gemeinschaften und Gruppen in Second Life von selbst bilden.
                    Auch werden Andachtsr&auml;ume und Kirchen von Laien gebaut.
                    Diese Entwicklung war ebenfalls in anderen virtuellen Welten,
                    wie etwa Active Worlds oder der 2D Welt Funcity (ehemals
                    Funama) zu beobachten: Die Nutzer solcher Welten &quot;verlangen&quot; nach
                    Religi&ouml;sit&auml;t und aus diesem Verlangen heraus bilden
                    sich Gruppen und werden Treffpunkte geschaffen, an denen
                    Religi&ouml;sit&auml;t auch online gelebt werden kann.<br>
                    Gerade im interelligi&ouml;sen und intrareligi&ouml;sen Bereich
                    ist Second Life eine der spannensten Austauschplattformen
                    in
                    Internet.
                    Viele
                    Glaubensgemeinschaften
                    treffen in dieser virtuellen Welt aufeinander und diskutieren
                    miteinander. Aber auch innerhalb der eigenen Religion begegnen
                    sich Anh&auml;nger aus ganz unterschiedlichen Kulturkreisen
                    in Second Life und diskutieren &uuml;ber die Inhalte Ihres
                    Glaubens. <br>
                    Neue Formen der Glaubensaus&uuml;bung entstehen nicht, vielmehr
                    werden bestehende Formen ins SL &uuml;bernommen. F&uuml;r
                    die Kirche und den Glauben kann SL eine Art Jungbrunnen sein,
                    indem
                    SL eine Plattform bietet, die ein breites Panoptikum der
                    die Menschen bewegenden Themen und religi&ouml;sen Fragen
                    bietet. Die Entwicklungen in SL und anderen virtuellen Welten
                    zeigen
                    auch, dass es eine S&auml;kularisierung der Gesellschaft
                    nicht gibt, zumindest nicht in dem Sinne, dass Religion in
                    einer
                    immer moderner werdenden Gesellschaft verschwinden w&uuml;rde.</p>
              <p class="text"><span class="&uuml;berschrift"><img src="caritas_mannheim_iconpunkt.gif" width="12" height="12"> Wie
                  funktioniert es und was kann ich damit machen?<a name="funktionsweise"></a> </span><span class="unterbild"><font size="1"><a href="#inhaltsuebersicht">(Inhaltsverzeichnis)</a></font></span></p>
              <p class="text">Auf der Internetseite von SL l&auml;dt man sich nach
                einer Registrierung die Software herunter. Die Basis-Teilnahme
                an SL ist kostenlos. Nach der Installation der Software und der
                Registrierung kann man sich in die virtuelle 3D Welt von Second
                Life einloggen. Zun&auml;chst muss man sich einen sog. Avatar, also
                eine 3D Figur erstellen, mit der man durch die virtuelle Welt
                von SL navigiert. Man kann laufen, gehen, fliegen und sich von
                einem Ort zum anderen teleportieren.<br>
                <br>
                 Der Avatar kann mit den Gegenst&auml;nden und den anderen
                Avataren interagieren. Anders als bei einem online Spiel gibt
                es keine festgelegten Abl&auml;ufe, Regeln oder ein Spielziel.
                Die R&auml;ume und Orte in SL sind sehr unterschiedlich: Es gibt
                bestimmte Bereiche innerhalb SL&acute;s die im Stil etwa eines
                Romans gestaltet sind (z.B. Herr der Ringe), andere wiederum
                sind nach dem Vorbild
                einer realen Stadt wie etwa Frankfurt gestaltet. Der Avatar wird
                mittels der Tastatur und Maus gesteuert. Mit einem Headset ist
                es m&ouml;glich mit den anderen Avataren zu kommunizieren mittels
                Sprache (sog. Voice Chat).<br>
                    <br>
                    <span class="&uuml;berschrift"><img src="caritas_mannheim_iconpunkt.gif" width="12" height="12"> Welche
                    M&ouml;glichkeiten bietet es technisch?<a name="moeglichkeiten"></a> </span><span class="unterbild"><font size="1"><a href="#inhaltsuebersicht">(Inhaltsverzeichnis)</a></font></span></p>
              <p class="text">Second Life erm&ouml;gicht:</p>
              <ul>
                <li class="text"> Gruppenbildung &uuml;ber Freundeslisten</li>
                <li><span class="text">Suchfunktion
                    f&uuml;r Orte, Personen und Gruppen</span></li>
                <li class="text">TextChat
                    (&ouml;ffentlich und privat)</li>
                <li class="text">Instant Messaging (sowohl
                    innerhalb einer Gruppe als auch von Avatar zu Avatar)</li>
                <li class="text">3D
                    Umgebung und Interaktion (Erstellen von Gegenst&auml;nden,
                                  deren Nutzung, aber auch Aktionen mit dem Avatar
                    wie springen, tanzen, ankleiden etc.)</li>
                <li class="text">Voice Chat</li>
                <li class="text">Textmitteilungen mittels sog. Notecards</li>
                <li class="text">Ein eigenes Inventar
                    zu f&uuml;hren mit Landmarks. Landmarks
                                        sind Sprungmarken zu Orten innerhalb
                    SL&acute;s &#8211; also
                                        analog zu Favoriten im Internet Explorer,
                                        die einem helfen, bestimmte Websites
                    wiederzufinden, helfen
                                        Bookmarks in
                                        Second Life dabei,
                    bestimmte Orte wiederzufinden</li>
                <li class="text">Anlegen
                    eines Nutzerprofils</li>
                <li><span class="text">Einspeisen von Audio, Video und Multimediapr&auml;sentationen
                    in Second Life</span></ul>
                      <p><span class="&uuml;berschrift"><img src="caritas_mannheim_iconpunkt.gif" width="12" height="12"> Wohin
                  entwickelt es sich?<a name="entwicklung"></a> </span><span class="unterbild"><font size="1"><a href="#inhaltsuebersicht">(Inhaltsverzeichnis)</a></font></span></p>
                  <p class="text">Viele Firmen und Konkurrenzanbieter zu
                        Second Life arbeiten an eigenen virtuellen Welten. 3D
                        Welten an sich werden popul&auml;rer werden, vor allem, da
                        die technische Entwicklung immer weiter voranschreitet.                        <br>
                    Second Life ist technisch sehr anspruchsvoll und relativ
                        instabil. Dennoch ist es die bekannteste 3D Welt im europ&auml;ischen
                        Kulturraum. Das Problem betseht darin, dass man eine
                        eigene Clientsoftware installieren muss. Andere 3D Welten
                        arbeiten daran, ihre Programme browserbasiert aufzubauen,
                        so dass man keine zus&auml;tzliche Software installieren muss,
                        um sie zu nutzen.</p>                      
                      <p>                        <span class="&uuml;berschrift"><img src="caritas_mannheim_iconpunkt.gif" width="12" height="12"> Pro
                          - Contra?<a name="pro"></a> </span><span class="unterbild"><font size="1"><a href="#inhaltsuebersicht">(Inhaltsverzeichnis)</a></font></span></p>
                      <p class="text">Wer Second Life nicht selbst nutzt
                          reagiert schnell mit Unverst&auml;ndnis. Realit&auml;tsflucht,
                          Identit&auml;tsflucht
                        und Sucht sind Stichworte die in diesem Zusammenhang
                  oft  begegnen. Dem ist entgegenzuhalten,
                  dass es keine aussagekr&auml;ftigen Studien gibt, die solch
                  negative Einfl&uuml;sse auf die Nutzer belegen. Im Gegenteil
                  besagen Studien eher, dass Nutzer solcher Angebote sehr wohl
                  realit&auml;tsnah,
                  kontaktfreudig und sozial integriert sind und nicht in eine
                  virtuelle Welt fl&uuml;chten. Dementsprechend sehen die Nutzer
                  von SL die M&ouml;glichkeiten, sie sich bieten, als Erweiterung
                  an. <br>
                  Ein eher philosophisches Argument aus Kirchenkreisen gegen
                  SL ist, dass sich der Mensch hier in einem zweiten Leben zum
                  Sch&ouml;pfer seiner selbst erhebt und neu erfindet. Zugegebenerma&szlig;en
                  sind in Second Life nur wohlgeformte braungebrannte Avatare
                  unterwegs. D.h. die Nutzer stellen sich von Ihrer Schokoladenseite
                  dar und machen sich gerne mal gr&ouml;&szlig;er, schlanker,
                  athletischer als Sie es in Wirklichkeit sind. Nur: Das war
                  bereits immer
                  so, egal welcher Technik sich die Menschen bedient haben, ob
                  im Brief oder am Telefon. SL dient letzenendes mehr der Identit&auml;tsfindung
                  als der Erschaffung einer vom wirklichen Leben getrennten Zweitidentit&auml;t. </p>
                      <p><span class="text">Die wirklichen Schattenseiten liegen
                          zum einen in der Technik und zum anderen im Zeitfaktor:
                          Second Life stellt
                        zum einen hohe technische Anforderungen an die
                    Hardware des PC&acute;s. Zum anderen l&auml;uft es relativ
                    unstabil und hat eine etwas entt&auml;uschende Grafik . Die
                    Nutzerzahlen sind r&uuml;ckl&auml;ufig. Aufgrund der weltweiten
                    Verbreitung gibt es vor allem wegen der Zeitzonen vor allem
                    Kontakte innerhalb von drei gro&szlig;en Nutzergruppen: Die
                    des amerikanischen Kontinents, die des Eurasischen und des
                    Asiatischen Raumes.</span></p>
                      <p class="text"><span class="&uuml;berschrift"><img src="caritas_mannheim_iconpunkt.gif" width="12" height="12"> Anwedung
                          und Umsetzung</span> <a name="umsetzung" id="umsetzung"></a><span class="&uuml;berschrift"> </span><span class="unterbild"><font size="1"><a href="#inhaltsuebersicht">(Inhaltsverzeichnis)</a></font></span></p>
                      <p class="text">Die Einsatzm&ouml;glichkeiten von Second Life
                        im religi&ouml;sen Bereich sind noch sp&auml;rlich. Das Erzbistum
                        Freiburg hat einen Repr&auml;sentanz in SL, an der auch
                      die Komplet gebetet wird.<br>                        
                      </span><span class="&uuml;berschrift"><br>
                      <img src="caritas_mannheim_iconpunkt.gif" width="12" height="12"> Hardfacts&amp; Linktipps</span> <a name="hardfacts"></a><span class="&uuml;berschrift"> </span><span class="unterbild"><font size="1"><a href="#inhaltsuebersicht">(Inhaltsverzeichnis)</a></font></span><br>                    
                        <br>
                        <span class="text">Second
                                  Life wurde 2003 von der Firma Linden Labs gegr&uuml;ndet. Seit 2007
                          ist der Quellcode unter der GLP ver&ouml;ffentlicht und kann
                          somit von den Nutzern weiterentwickelt werden. Second Life kann
                          kostenlos genutzt werden und es sind ca. 13 Millionen Menschen
                          registriert, viele allerdings mehrmals. Die Zahl der tats&auml;chlich
                          aktiven Nutzer, die gleichzeitig online sind schwankt je nach
                          Tageszeit zwischen 14.000 und 65.000. Die Basismitgleidschaft
                          ist kostenlos. Die erweiterten Mitgliedschaften, die es unter
                          anderem erm&ouml;glichen Geb&auml;ude zu bauen und
                          Land zu erwerben, sind kostenpflichtig. </span><br>
                        <br>
                        <a href="http://www.netzinkulturation.de" target="_blank">                        <span class="&uuml;berschrift"><img src="caritas_mannheim_iconpunkt.gif" width="12" height="12"></span> Netzinkulturation.de
                                  - Die Anlaufstelle zu Infos rund um Kirche&amp;Internet
                      sowie spe</a><a href="http://www.netzinkulturation.de" target="_blank">ziell
                          zu Second Life</a><br>
                          <a href="http://de.wikipedia.org/wiki/Second_Life" target="_blank"><br>
                          <span class="&uuml;berschrift"><img src="caritas_mannheim_iconpunkt.gif" width="12" height="12"></span> Second
                        Life Artikel in der Wikipedia                        </a><br>
                        <br>
                        <span class="&uuml;berschrift"> </span><span class="unterbild"><font size="1"><a href="#inhaltsuebersicht">(Inhaltsverzeichnis)</a></font></span>                        <br>
                      </p>
                </td>
            <td background="boxright.gif" width="8"><img src="boxright.gif" width="8" height="8" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td width="8"><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
            <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
            <td width="8"><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
          </tr>
        </table></td>
        <td width="120" rowspan="2" valign="top">	<p><a href="http://www.kath.de/Workshop-flyerinternetwerkstatt2008-online.pdf" target="_blank"><img src="images-kirche-internet/banner-internetwerkstatt-kath-de-2008-236x60.jpg" width="236" height="60" border="0"></a>
          </p>
          <p>
            <script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
/* 120x600, Erstellt 11.06.08 */
google_ad_slot = "6662271223";
google_ad_width = 120;
google_ad_height = 600;
//-->
          </script>
            <script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
                  </script>
                  </p></td>
      </tr>
      <tr>
        <td width="216" height="1374" align="left" valign="top">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
</body>
</html>
