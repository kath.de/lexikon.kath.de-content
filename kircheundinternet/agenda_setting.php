<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<head profile="http://dublincore.org/documents/dcq-html/">

<link rel="schema.DC" href="http://purl.org/dc/elements/1.1/">
<link rel="schema.DCTERMS" href="http://purl.org/dc/terms/">
<link href="lexikon-kirche-internet.css" rel="stylesheet" type="text/css">

<title>Agenda Setting - Lexikon Medien und &Ouml;ffentlichkeit</title>
<meta name="robots" content="index,follow,all" />
<meta name="description" content="Second Life, Kirche, Internet, Web 2.0, Religion, Glauben, Internetkirche, Netzkirche">
<meta name="author" content="kath.de">
<meta name="keywords" content="Second Life, Kirche, Internet, Web 2.0, Religion, Glauben, Internetkirche, Netzkirche">


<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="DC.title" content="Second Life, Kirche, Internet, Web 2.0, Religion, Glauben, Internetkirche, Netzkirche">

<meta name="DC.creator" content="Second Life, Kirche, Internet, Web 2.0, Religion, Glauben, Internetkirche, Netzkirche">
<meta name="DC.subject" content="Second Life, Kirche, Internet, Web 2.0, Religion, Glauben, Internetkirche, Netzkirche">
<meta name="DC.description" content="Second Life, Kirche, Internet, Web 2.0, Religion, Glauben, Internetkirche, Netzkirche">
<meta name="DC.publisher" content="kath.de">
<meta name="DC.contributor" content="kath.de">
<meta name="DC.date" content="2007-05-15T08:49:37+02:00" scheme="DCTERMS.W3CDTF">
<meta name="DC.type" content="Text" scheme="DCTERMS.DCMIType">
<meta name="DC.format" content="text/html" scheme="DCTERMS.IMT">
<meta name="DC.identifier"
      content="http://www.kath.de/lexikon/kircheundinternet/second_life_lexikon_kirche_im_internet.php"
      scheme="DCTERMS.URI">
<meta name="DC.source"
      content="http://www.w3.org/TR/html401/struct/global.html#h-7.4.4"
      scheme="DCTERMS.URI">
<meta name="DC.language" content="de" scheme="DCTERMS.RFC3066">
<meta name="DC.relation" content="http://dublincore.org/" scheme="DCTERMS.URI">
<meta name="DC.coverage" content="Munich" scheme="DCTERMS.TGN">
<meta name="DC.rights" content="Alle Rechte liegen beim Autor">
</head>

<body bgcolor="#cbccd8">
<table width="900" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="6">
      <tr>
        <td width="216" align="left" valign="top" height="3">
          <table width="216" border="0" cellpadding="0" cellspacing="0">
            <tr valign="top" align="left">
              <td width="8"><img src="boxtopleftcorner.jpg" width="8" height="8" alt=""></td>
              <td width="200" background="boxtop.jpg"><img src="boxtop.jpg" alt="" width="8" height="8"></td>
              <td width="8"><img src="boxtoprightcorner.jpg" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="boxtopleft.jpg"><img src="boxtopleft.jpg" width="8" height="8" alt=""></td>
              <td bgcolor="7d82b0"><p><b><font color="#FFFFFF" face="Arial, Helvetica, sans-serif">Lexikon
                      Medien&amp;&Ouml;ffentlichkeit</font></b></p>
              </td>
              <td background="boxtopright.jpg"><img src="boxtopright.jpg" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="boxdividerleft.jpg" width="8" height="13" alt=""></td>
              <td background="boxdivider.jpg"><img src="boxdivider.jpg" alt="" width="8" height="13"></td>
              <td><img src="boxdividerright.jpg" width="8" height="13" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
              <td bgcolor="#ffffff">
                <?php include("logo.html"); ?>
              </td>
              <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
              <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
              <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
            </tr>
          </table>
          <br>
          <table width="216" border="0" cellpadding="0" cellspacing="0">
            <tr valign="top" align="left">
              <td width="8"><img src="boxtopleftcorner.jpg" width="8" height="8" alt=""></td>
              <td width="200" background="boxtop.jpg"><img src="boxtop.jpg" alt="" width="8" height="8"></td>
              <td width="8"><img src="boxtoprightcorner.jpg" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="boxtopleft.jpg"><img src="boxtopleft.jpg" width="8" height="8" alt=""></td>
              <td bgcolor="7d82b0"><font color="#FFFFFF" face="Arial, Helvetica, sans-serif"><strong>Weitere
                    Artikel</strong></font></td>
              <td background="boxtopright.jpg"><img src="boxtopright.jpg" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="boxdividerleft.jpg" width="8" height="13" alt=""></td>
              <td background="boxdivider.jpg"><img src="boxdivider.jpg" alt="" width="8" height="13"></td>
              <td><img src="boxdividerright.jpg" width="8" height="13" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
              <td bgcolor="#ffffff" class="V10">
                <?php include("az.html"); ?>
              </td>
              <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
              <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
              <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
            </tr>
          </table>
        </td>
        <td width="530" rowspan="2" valign="top">          <table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr valign="top" align="left">
            <td width="8"><img src="boxtopleftcorner.jpg" width="8" height="8" alt=""></td>
            <td background="boxtop.jpg"><img src="boxtop.jpg" alt="" width="8" height="8"></td>
            <td width="8"><img src="boxtoprightcorner.jpg" width="8" height="8" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td background="boxtopleft.jpg" width="8"><img src="boxtopleft.jpg" width="8" height="8" alt=""></td>
            <td bgcolor="7d82b0">
              <h1><font color="#FFFFFF" face="Arial, Helvetica, sans-serif">Agenda
                  Setting<span class="&uuml;berschrift"><a name="inhaltsuebersicht"></a></span></font></h1>
            </td>
            <td background="boxtopright.jpg" width="8"><img src="boxtopright.jpg" width="8" height="8" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td width="8"><img src="boxdividerleft.jpg" width="8" height="13" alt=""></td>
            <td background="boxdivider.jpg"><img src="boxdivider.jpg" alt="" width="8" height="13"></td>
            <td width="8"><img src="boxdividerright.jpg" width="8" height="13" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td width="8" height="905" background="boxleft.gif">&nbsp;</td>
            <td bgcolor="#ffffff" class="L12">              <p class="text">    <span class="&uuml;berschrift"><img src="caritas_mannheim_iconpunkt.gif" width="12" height="12"> <font face="Arial, Helvetica, sans-serif"><strong>Ein
                      Thema auf die Tagesordnung der &Ouml;ffentlichkeit setzen</strong></font><a name="wichtig"></a></span></p>
              <p class=MsoNormal><font face="Arial, Helvetica, sans-serif">Im
                  kommunikativen Raum der &Ouml;ffentlichkeit, in dem die Gesellschaft insgesamt
                  ihre Fragen diskutiert, die Machtverteilung aushandelt und
                  Entscheidungen vorbereitet, k&ouml;nnen immer nur einige Themen
                  gleichzeitig behandelt werden, denn alle Themen k&ouml;nnen
                  nicht auf einmal zur Diskussion stehen. Damit ein Thema auf
                  die Tagesordnung der &Ouml;ffentlichkeit gelangt, mu&szlig; es
                  ausgew&auml;hlt werden, oder das Thema mu&szlig; von au&szlig;en
                  aufgezwungen sein, wie z.B. ein Ungl&uuml;cksfall, ein Attentat,
                  ein Umsturz oder ein sich als unausweichlich herausstellendes
                  Problem, mit dem sich der Verband, die Kommune, die gesamte
                  Gesellschaft auseinandersetzen m&uuml;ssen. Manche Themen kommen
                  automatisch auf die Tagesordnung. Es gibt in einem bestimmten
                  Rhythmus Wahlen, der Etat mu&szlig; verabschiedet, die Neujahrsansprache
                  gehalten werden. Abgesehen von den Themen, die auf jeden Fall
                  zur Sprache kommen, h&auml;ngt es von den Agenten ab, die sich
                  in der &Ouml;ffentlichkeit zu Wort melden, welches Thema auf
                  die Tagesordnung kommt. Zweifellos haben die Medien dabei eine
                  wichtige Rolle, die m&ouml;glicherweise aber &uuml;bersch&auml;tzt
                  wird. So steht ein gro&szlig;er Teil der Nachrichten bereits
                  fest, weil z.B. eine Abstimmung im Parlament ansteht, ein Partei-
                  oder Gewerkschaftstag stattfindet, &uuml;ber eine Wahl oder
                  einen Staatsbesuch zu berichten ist, die Zahl der Arbeitslosen
                  f&uuml;r den vergangenen Monat bekanntgegeben wird, die Leser,
                  H&ouml;rer, Zuschauer Informationen &uuml;ber die Staus an
                  den Ferienwochenende, &uuml;ber Sportereignisse und das Wetter
                  von morgen erwarten.</font></P>
              <p><font face="Arial, Helvetica, sans-serif"> Auf
                    die Tagesordnung kann auch ein Thema kommen, wenn es Reaktionen
                    in der &Ouml;ffentlichkeit
      gibt - durch Leserbriefe, Anrufe oder Stammtischgespr&auml;che, die Journalisten
      von einigen Zeitungen sogar systematisch verfolgen. </font></p>              <p><font face="Arial, Helvetica, sans-serif">                    Die zunehmende Abh&auml;ngigkeit des Fernsehens von der Fernbedienung der Zuschauer
      sowie der Zeitungen und Zeitschriften von dem Kaufentscheid der Leser am Kiosk
      d&uuml;rfte zur Folge haben, da&szlig; die Regierung, Parteien und Verb&auml;nde
      weniger Einflu&szlig; darauf haben, was auf die Tagesordnung der <a href="oeffentlichkeit.php">&Ouml;ffentlichkeit</a> kommt.
      Die Medien sind gezwungen, sich an den Interessen ihrer Nutzer auszurichten.
      Das hat u.a. zur Folge, da&szlig; Ereignissen durch die Berichterstattung ein
      spektakul&auml;rer Charakter verliehen wird, um das Interesse der Leser bzw.
      Zuschauer zu gewinnen.</font></p>
      <p><font face="Arial, Helvetica, sans-serif">            Es gibt Gruppen in der Gesellschaft, die das Durchhalteverm&ouml;gen haben, &uuml;ber
        einen langen Zeitraum die Behandlung eines Themas zu fordern. So hat die &Ouml;kologiebewegung
        durch Demonstrationen, die Ver&ouml;ffentlichung von Untersuchungen und direkte
        Kommunikation erreicht, da&szlig; der Umweltschutz auf die Tagesordnung gekommen
        ist. </font></p>
      <p>&copy; <a href="http://www.kath.de/"><font size="2" face="Arial, Helvetica, sans-serif">www.kath.de</font></a></p>              </td>
            <td background="boxright.gif" width="8"><img src="boxright.gif" width="8" height="8" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td width="8"><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
            <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
            <td width="8"><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
          </tr>
        </table></td>
        <td width="120" rowspan="2" valign="top">	<p>
          <script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
/* 120x600, Erstellt 11.06.08 */
google_ad_slot = "6662271223";
google_ad_width = 120;
google_ad_height = 600;
//-->
          </script>
            <script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
                  </script>
            </p>
          </td>
      </tr>
      <tr>
        <td width="216" height="1374" align="left" valign="top">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
</body>
</html>
