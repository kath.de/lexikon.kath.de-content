<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<head profile="http://dublincore.org/documents/dcq-html/">

<link rel="schema.DC" href="http://purl.org/dc/elements/1.1/">
<link rel="schema.DCTERMS" href="http://purl.org/dc/terms/">
<link href="lexikon-kirche-internet.css" rel="stylesheet" type="text/css">

<title>Online Communities :: Das Lexikon f&uuml;r Kirche&amp;Internet :: Kirche im Web 2.0</title>
<meta name="robots" content="index,follow,all" />
<meta name="description" content="Chat, Kirche, Internet, Web 2.0, Religion, Glauben, Internetkirche, Netzkirche">
<meta name="author" content="kath.de">
<meta name="keywords" content="Chat, Kirche, Internet, Web 2.0, Religion, Glauben, Internetkirche, Netzkirche">


<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="DC.title" content="Chat, Kirche, Internet, Web 2.0, Religion, Glauben, Internetkirche, Netzkirche">

<meta name="DC.creator" content="Chat, Kirche, Internet, Web 2.0, Religion, Glauben, Internetkirche, Netzkirche">
<meta name="DC.subject" content="Chat, Kirche, Internet, Web 2.0, Religion, Glauben, Internetkirche, Netzkirche">
<meta name="DC.description" content="Chat, Kirche, Internet, Web 2.0, Religion, Glauben, Internetkirche, Netzkirche">
<meta name="DC.publisher" content="kath.de">
<meta name="DC.contributor" content="kath.de">
<meta name="DC.date" content="2007-05-15T08:49:37+02:00" scheme="DCTERMS.W3CDTF">
<meta name="DC.type" content="Text" scheme="DCTERMS.DCMIType">
<meta name="DC.format" content="text/html" scheme="DCTERMS.IMT">
<meta name="DC.identifier"
      content="http://www.kath.de/lexikon/kircheundinternet/chat_lexikon_kirche_im_internet.php"
      scheme="DCTERMS.URI">
<meta name="DC.source"
      content="http://www.w3.org/TR/html401/struct/global.html#h-7.4.4"
      scheme="DCTERMS.URI">
<meta name="DC.language" content="de" scheme="DCTERMS.RFC3066">
<meta name="DC.relation" content="http://dublincore.org/" scheme="DCTERMS.URI">
<meta name="DC.coverage" content="Munich" scheme="DCTERMS.TGN">
<meta name="DC.rights" content="Alle Rechte liegen beim Autor">
</head>

<body background="images-kirche-internet/kirche-internet-background.jpg" link="#0066FF" vlink="#666666" alink="#00FF00">
<table width="900" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="6">
      <tr>
        <td width="216" align="left" valign="top" height="3">
          <table width="216" border="0" cellpadding="0" cellspacing="0">
            <tr valign="top" align="left">
              <td width="8"><img src="boxtopleftcorner.jpg" width="8" height="8" alt=""></td>
              <td width="200" background="boxtop.jpg"><img src="boxtop.jpg" alt="" width="8" height="8"></td>
              <td width="8"><img src="boxtoprightcorner.jpg" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="boxtopleft.jpg"><img src="boxtopleft.jpg" width="8" height="8" alt=""></td>
              <td bgcolor="0080bb"><p><b><font color="#FFFFFF" face="Arial, Helvetica, sans-serif">Lexikon
                      Kirche&amp;Internet</font></b></p>
              </td>
              <td background="boxtopright.jpg"><img src="boxtopright.jpg" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="boxdividerleft.jpg" width="8" height="13" alt=""></td>
              <td background="boxdivider.jpg"><img src="boxdivider.jpg" alt="" width="8" height="13"></td>
              <td><img src="boxdividerright.jpg" width="8" height="13" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
              <td bgcolor="#ffffff">
                <?php include("logo.html"); ?>
              </td>
              <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
              <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
              <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
            </tr>
          </table>
          <br>
          <table width="216" border="0" cellpadding="0" cellspacing="0">
            <tr valign="top" align="left">
              <td width="8"><img src="boxtopleftcorner.jpg" width="8" height="8" alt=""></td>
              <td width="200" background="boxtop.jpg"><img src="boxtop.jpg" alt="" width="8" height="8"></td>
              <td width="8"><img src="boxtoprightcorner.jpg" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="boxtopleft.jpg"><img src="boxtopleft.jpg" width="8" height="8" alt=""></td>
              <td bgcolor="0080bb"><font color="#FFFFFF" face="Arial, Helvetica, sans-serif"><strong>In
                    diesem Artikel</strong></font></td>
              <td background="boxtopright.jpg"><img src="boxtopright.jpg" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="boxdividerleft.jpg" width="8" height="13" alt=""></td>
              <td background="boxdivider.jpg"><img src="boxdivider.jpg" alt="" width="8" height="13"></td>
              <td><img src="boxdividerright.jpg" width="8" height="13" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
              <td bgcolor="#ffffff" class="V10">
                <?php include("sprungmarken.html"); ?>
              </td>
              <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
              <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
              <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
            </tr>
          </table>          <br>
          <table width="216" border="0" cellpadding="0" cellspacing="0">
            <tr valign="top" align="left">
              <td width="8"><img src="boxtopleftcorner.jpg" width="8" height="8" alt=""></td>
              <td width="200" background="boxtop.jpg"><img src="boxtop.jpg" alt="" width="8" height="8"></td>
              <td width="8"><img src="boxtoprightcorner.jpg" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="boxtopleft.jpg"><img src="boxtopleft.jpg" width="8" height="8" alt=""></td>
              <td bgcolor="0080bb"><font color="#FFFFFF" face="Arial, Helvetica, sans-serif"><strong>Weitere
                    Artikel</strong></font></td>
              <td background="boxtopright.jpg"><img src="boxtopright.jpg" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="boxdividerleft.jpg" width="8" height="13" alt=""></td>
              <td background="boxdivider.jpg"><img src="boxdivider.jpg" alt="" width="8" height="13"></td>
              <td><img src="boxdividerright.jpg" width="8" height="13" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
              <td bgcolor="#ffffff" class="V10">
                <?php include("az.html"); ?>
              </td>
              <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
              <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
              <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
            </tr>
          </table>
        </td>
        <td width="530" rowspan="2" valign="top">          <img src="boxleft.gif" width="8" height="8" alt="">
          <table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr valign="top" align="left">
            <td width="8"><img src="boxtopleftcorner.jpg" width="8" height="8" alt=""></td>
            <td background="boxtop.jpg"><img src="boxtop.jpg" alt="" width="8" height="8"></td>
            <td width="8"><img src="boxtoprightcorner.jpg" width="8" height="8" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td background="boxtopleft.jpg" width="8"><img src="boxtopleft.jpg" width="8" height="8" alt=""></td>
            <td bgcolor="0080bb">
              <h1><font color="#FFFFFF" face="Arial, Helvetica, sans-serif">Online
                  Communities<span class="&uuml;berschrift"><a name="inhaltsuebersicht"></a></span></font></h1>
            </td>
            <td background="boxtopright.jpg" width="8"><img src="boxtopright.jpg" width="8" height="8" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td width="8"><img src="boxdividerleft.jpg" width="8" height="13" alt=""></td>
            <td background="boxdivider.jpg"><img src="boxdivider.jpg" alt="" width="8" height="13"></td>
            <td width="8"><img src="boxdividerright.jpg" width="8" height="13" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td width="8" height="905" background="boxleft.gif">&nbsp;</td>
            <td bgcolor="#ffffff" class="L12">              <p class="text">    <span class="&uuml;berschrift"><img src="caritas_mannheim_iconpunkt.gif" width="12" height="12"> Warum
                ist es wichtig?<a name="wichtig"></a></span></p>
              <p class="text">Online Communities sind Web 2.0, das &quot;Mitmachinternet&quot;,
                in Reinform: Die Inhalte werden fast ausschlie&szlig;lich von
                den Nutzern erstellt, es wird viel kommuniziert und der Austausch
                untereinander
                steht
                im Vordergrund. Oft werden moderne Internetapplikation wie Messenger
                in den Communities genutzt. Communities sind aber auch der kommunikative
                Kern der Internets, wie auch die ARD-ZDF Online-Studie zeigt. </p>
              <p class="text">                    <span class="&uuml;berschrift"><img src="caritas_mannheim_iconpunkt.gif" width="12" height="12"> Wie
                  und von wem wird es genutzt?<a name="nutzung"></a> </span><span class="unterbild"><font size="1"><a href="#inhaltsuebersicht">(Inhaltsverzeichnis)</a></font></span></p>              
              <p class="text">Es gibt sehr viele Communities. Manche orientieren
                sich thematisch, etwas Astronomie, Garten, ausgefallene Hobbies.
                Andere orientieren sich an demographischen Merkmalen wie etwa
                &quot;Feierabend.de&quot; - eine sehr gro&szlig;e Rentnercommunity.</p>
              <p class="text">Die gr&ouml;&szlig;ten Communities, die es gibt, bieten aber
                einfach die M&ouml;glichkeit, zu sehen und gesehen zu werden: StudiVZ,
                Wer-kennt-wen, myspace, facebook, lokalisten, Xing. <br>
                <br>
                Manche Communities haben dabei als technische Grundlage f&uuml;r Ihren
                Austausch nur ein Forum. Manch andere haben zus&auml;tzliche Tools,
                wie ein internes Nachrichtenboard, Instant Messanging etc. Herzst&uuml;ck
                jeder Community ist das Profil, in dem jeder Nutzer soviele Informationen
                von sich Preis geben kann, wie er gerne m&ouml;chte. Wenn eine Community
                nicht sehr speziell ausgerichtet ist, komtm es vor, dass sich
                in gro&szlig;en Communities Gruppen bilden. Man erkennt die Zugeh&ouml;rigkeit
                zu einer Gruppe daran, dass bei der betreffenden Person im Feld
                Gruppen (oder je nach Communitiy auch Netzwerk o.&auml; Bezeichnungen)
                die entsprechenden Mitgleidschaften aufgelistet werden. <br>
                Ein zweites Ph&auml;nomen neben der Gruppenbildung in thematisch unspezifischen
                gro&szlig;en Communities sind die Bildung von Regionalgruppen. XING
                und auch Feierabend.de sind hierf&uuml;r gute Beispiele: Die Nutzer
                sind innerhalb der Online Communities in regionalen Netzwerken
                / Gruppen organisiert, die sich auf regelm&auml;&szlig;iger Basis auch regional
                vor Ort treffen. <br>
                    <br>
                    <span class="&uuml;berschrift"><img src="caritas_mannheim_iconpunkt.gif" width="12" height="12"> Gibt
                    es spezielle religi&ouml;se Nutzung?<a name="religioes"></a> </span><span class="unterbild"><font size="1"><a href="#inhaltsuebersicht">(Inhaltsverzeichnis)</a></font></span></p>              
              <p class="text">Es gibt auch spezielle religi&ouml;se Communities. Vatican
                Friends ist zum Beispiel eine eher geschlossene Community mit
                klaren Aufnahmekriterien. <br>
                <br>
                In gro&szlig;en Communities bilden sich aber explizit religi&ouml;se Gruppen,
                die auch teilweise Themen des Glaubensleben aufgreifen und teilweise
                christlich oder auch konfessionell ausgerichtet sind.<br>
<br>
                    <br>
                    <span class="&uuml;berschrift"><img src="caritas_mannheim_iconpunkt.gif" width="12" height="12"> Was
                    sind die Implikationen f&uuml;r Kirche und Glaube?</span><a name="implikationen"></a><span class="&uuml;berschrift"> </span><span class="unterbild"><font size="1"><a href="#inhaltsuebersicht">(Inhaltsverzeichnis)</a></font></span><br>
                    <br>
                  Mittlerweile ist es keine Seltenheit mehr, wenn sich religi&ouml;se
                  Gruppen, wie z.B. Firmgruppen oder Messdienefahrten, Exerzitiengruppen
                  etc. &uuml;ber gro&szlig;e Netzwerke wie StudiVZ oder Wer-kennt-wen organisieren.
                  AUch sind erste Berichte zu h&ouml;ren, nach denen sich die n&auml;chste
                  Generation, also die Firmkatecheten &uuml;ber diese Netzwerke als
                  zentrales Kommunikationsinstrument organisieren.<br>
                  AUch in XING gibt es erste Projekte, etwa wenn Pfarrer gezielt
                  in XING die Menschen aus Ihrem Postleitzahlengebiet anschreiben. <br>
                  Gerade die Communities geben der Kirche ein praktisches neues
                  Kommunikationstool an die Hand. Sie sind zu vergelichen mit
                  der Weiterentwicklung des Telefons</p>
              <p class="text"><span class="&uuml;berschrift"><img src="caritas_mannheim_iconpunkt.gif" width="12" height="12"> Wie
                  funktioniert es und was kann ich damit machen?<a name="funktionsweise"></a> </span><span class="unterbild"><font size="1"><a href="#inhaltsuebersicht">(Inhaltsverzeichnis)</a></font></span></p>
              <p class="text">Die meisten Communities sind kostenlos. Man kann
                ein Profil anlegen und nach anderen Nutzern suchen, sich &uuml;ber
                Themen austauschen, sich verabreden oder einfach nur kommunizieren.
                Man kann alte Schulfreunde suchen, Gesch&auml;ftspartner oder aber
                einfach Menschen mit den gleichen Interessen.<br>
                    <br>
                    <span class="&uuml;berschrift"><img src="caritas_mannheim_iconpunkt.gif" width="12" height="12"> Welche
                  M&ouml;glichkeiten bietet es technisch?<a name="moeglichkeiten"></a> </span><span class="unterbild"><font size="1"><a href="#inhaltsuebersicht">(Inhaltsverzeichnis)</a></font></span></p>
              <p class="text">Technisch gesehen h&auml;ngt es von der jeweiligen Community
                ab, was geht: Es reicht vom Schreiben einzelner Beitr&auml;ge in einem
                Forum bis hin zu Voice und Videotelefonie. Ein Profil, im Sinne
                mit einer extra Seite mit Informationen zu meiner Person ist
                aber immer Bestandteil. </p>
              <p><span class="&uuml;berschrift"><img src="caritas_mannheim_iconpunkt.gif" width="12" height="12"> Wohin
                  entwickelt es sich?<a name="entwicklung"></a> </span><span class="unterbild"><font size="1"><a href="#inhaltsuebersicht">(Inhaltsverzeichnis)</a></font></span></p>
                      <p class="text">Sie Communities wachsen. Einer Studie zufolge
                        hat jeder dritte Deutsche bis 2012 ein Profil. Communities
                        werden nach und nach das Kommunikationsverhalten der
                        Bev&ouml;lkerung &auml;ndern und so selbstverst&auml;ndlich wie das
                        Telefon werden.</p>                      
                      <p>                        <span class="&uuml;berschrift"><img src="caritas_mannheim_iconpunkt.gif" width="12" height="12"> Pro
                          - Contra?<a name="pro"></a> </span><span class="unterbild"><font size="1"><a href="#inhaltsuebersicht">(Inhaltsverzeichnis)</a></font></span></p>
                      <p class="text">&nbsp;</p>
                  <p class="text"><span class="&uuml;berschrift"><img src="caritas_mannheim_iconpunkt.gif" width="12" height="12"> Anwedung
                          und Umsetzung</span> <a name="umsetzung" id="umsetzung"></a><span class="&uuml;berschrift"> </span><span class="unterbild"><font size="1"><a href="#inhaltsuebersicht">(Inhaltsverzeichnis)</a></font></span></p>
                  <p class="text">F&uuml;r die Kirche gibt es zwei Nutzungsvarianten:
                    Die eine besteht darin bestehende Netzwerke zu nutzen, wie
                    etwas zur Kommunikation innerhalb einer Gruppe, wie einer
                    Firmgruppe. Zum anderen aber kann die Kirche innerhalb gewisser
                    Gruppen auch eigene Communities anlegen, da dies mit Ning.com
                    und anderen Diensten mittlerweile einfach und kostenlos m&ouml;glich
                    ist.<br>
                    Eine kluge Entscheidung f&uuml;r eine der Alternativen h&auml;ngt von
                    den im Einzelfall gegebenen Faktoren ab.<br>
                    <br>
                    </span><span class="&uuml;berschrift"><br>
                    <img src="caritas_mannheim_iconpunkt.gif" width="12" height="12"> Hardfacts&amp; Linktipps</span> <a name="hardfacts"></a><span class="&uuml;berschrift"> </span><span class="unterbild"><font size="1"><a href="#inhaltsuebersicht">(Inhaltsverzeichnis)</a></font></span><br>                    
                    <br>
                    <br>
                    <a href="http://www.netzinkulturation.de" target="_blank">                        <span class="&uuml;berschrift"><img src="caritas_mannheim_iconpunkt.gif" width="12" height="12"></span> </a><br>
                    <a href="http://de.wikipedia.org/wiki/Second_Life" target="_blank"><br>
                    <span class="&uuml;berschrift"><img src="caritas_mannheim_iconpunkt.gif" width="12" height="12"></span> </a><br>
                    <br>
                    <span class="&uuml;berschrift"> </span><span class="unterbild"><font size="1"><a href="#inhaltsuebersicht">(Inhaltsverzeichnis)</a></font></span>                        <br>
                  </p>
                </td>
            <td background="boxright.gif" width="8"><img src="boxright.gif" width="8" height="8" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td width="8"><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
            <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
            <td width="8"><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
          </tr>
        </table></td>
        <td width="120" rowspan="2" valign="top">	<script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
/* 120x600, Erstellt 11.06.08 */
google_ad_slot = "6662271223";
google_ad_width = 120;
google_ad_height = 600;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
</td>
      </tr>
      <tr>
        <td width="216" height="1374" align="left" valign="top">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
</body>
</html>
