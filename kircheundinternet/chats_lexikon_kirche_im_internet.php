<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<head profile="http://dublincore.org/documents/dcq-html/">

<link rel="schema.DC" href="http://purl.org/dc/elements/1.1/">
<link rel="schema.DCTERMS" href="http://purl.org/dc/terms/">
<link href="lexikon-kirche-internet.css" rel="stylesheet" type="text/css">

<title>Chat :: Das Lexikon f&uuml;r Kirche&amp;Internet :: Kirche im Web 2.0</title>
<meta name="robots" content="index,follow,all" />
<meta name="description" content="Chat, Kirche, Internet, Web 2.0, Religion, Glauben, Internetkirche, Netzkirche">
<meta name="author" content="kath.de">
<meta name="keywords" content="Chat, Kirche, Internet, Web 2.0, Religion, Glauben, Internetkirche, Netzkirche">


<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="DC.title" content="Chat, Kirche, Internet, Web 2.0, Religion, Glauben, Internetkirche, Netzkirche">

<meta name="DC.creator" content="Chat, Kirche, Internet, Web 2.0, Religion, Glauben, Internetkirche, Netzkirche">
<meta name="DC.subject" content="Chat, Kirche, Internet, Web 2.0, Religion, Glauben, Internetkirche, Netzkirche">
<meta name="DC.description" content="Chat, Kirche, Internet, Web 2.0, Religion, Glauben, Internetkirche, Netzkirche">
<meta name="DC.publisher" content="kath.de">
<meta name="DC.contributor" content="kath.de">
<meta name="DC.date" content="2007-05-15T08:49:37+02:00" scheme="DCTERMS.W3CDTF">
<meta name="DC.type" content="Text" scheme="DCTERMS.DCMIType">
<meta name="DC.format" content="text/html" scheme="DCTERMS.IMT">
<meta name="DC.identifier"
      content="http://www.kath.de/lexikon/kircheundinternet/chat_lexikon_kirche_im_internet.php"
      scheme="DCTERMS.URI">
<meta name="DC.source"
      content="http://www.w3.org/TR/html401/struct/global.html#h-7.4.4"
      scheme="DCTERMS.URI">
<meta name="DC.language" content="de" scheme="DCTERMS.RFC3066">
<meta name="DC.relation" content="http://dublincore.org/" scheme="DCTERMS.URI">
<meta name="DC.coverage" content="Munich" scheme="DCTERMS.TGN">
<meta name="DC.rights" content="Alle Rechte liegen beim Autor">
</head>

<body background="images-kirche-internet/kirche-internet-background.jpg" link="#0066FF" vlink="#666666" alink="#00FF00">
<table width="900" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="6">
      <tr>
        <td width="216" align="left" valign="top" height="3">
          <table width="216" border="0" cellpadding="0" cellspacing="0">
            <tr valign="top" align="left">
              <td width="8"><img src="boxtopleftcorner.jpg" width="8" height="8" alt=""></td>
              <td width="200" background="boxtop.jpg"><img src="boxtop.jpg" alt="" width="8" height="8"></td>
              <td width="8"><img src="boxtoprightcorner.jpg" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="boxtopleft.jpg"><img src="boxtopleft.jpg" width="8" height="8" alt=""></td>
              <td bgcolor="0080bb"><p><b><font color="#FFFFFF" face="Arial, Helvetica, sans-serif">Lexikon
                      Kirche&amp;Internet</font></b></p>
              </td>
              <td background="boxtopright.jpg"><img src="boxtopright.jpg" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="boxdividerleft.jpg" width="8" height="13" alt=""></td>
              <td background="boxdivider.jpg"><img src="boxdivider.jpg" alt="" width="8" height="13"></td>
              <td><img src="boxdividerright.jpg" width="8" height="13" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
              <td bgcolor="#ffffff">
                <?php include("logo.html"); ?>
              </td>
              <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
              <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
              <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
            </tr>
          </table>
          <br>
          <table width="216" border="0" cellpadding="0" cellspacing="0">
            <tr valign="top" align="left">
              <td width="8"><img src="boxtopleftcorner.jpg" width="8" height="8" alt=""></td>
              <td width="200" background="boxtop.jpg"><img src="boxtop.jpg" alt="" width="8" height="8"></td>
              <td width="8"><img src="boxtoprightcorner.jpg" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="boxtopleft.jpg"><img src="boxtopleft.jpg" width="8" height="8" alt=""></td>
              <td bgcolor="0080bb"><font color="#FFFFFF" face="Arial, Helvetica, sans-serif"><strong>In
                    diesem Artikel</strong></font></td>
              <td background="boxtopright.jpg"><img src="boxtopright.jpg" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="boxdividerleft.jpg" width="8" height="13" alt=""></td>
              <td background="boxdivider.jpg"><img src="boxdivider.jpg" alt="" width="8" height="13"></td>
              <td><img src="boxdividerright.jpg" width="8" height="13" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
              <td bgcolor="#ffffff" class="V10">
                <?php include("sprungmarken.html"); ?>
              </td>
              <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
              <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
              <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
            </tr>
          </table>          <br>
          <table width="216" border="0" cellpadding="0" cellspacing="0">
            <tr valign="top" align="left">
              <td width="8"><img src="boxtopleftcorner.jpg" width="8" height="8" alt=""></td>
              <td width="200" background="boxtop.jpg"><img src="boxtop.jpg" alt="" width="8" height="8"></td>
              <td width="8"><img src="boxtoprightcorner.jpg" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="boxtopleft.jpg"><img src="boxtopleft.jpg" width="8" height="8" alt=""></td>
              <td bgcolor="0080bb"><font color="#FFFFFF" face="Arial, Helvetica, sans-serif"><strong>Weitere
                    Artikel</strong></font></td>
              <td background="boxtopright.jpg"><img src="boxtopright.jpg" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="boxdividerleft.jpg" width="8" height="13" alt=""></td>
              <td background="boxdivider.jpg"><img src="boxdivider.jpg" alt="" width="8" height="13"></td>
              <td><img src="boxdividerright.jpg" width="8" height="13" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
              <td bgcolor="#ffffff" class="V10">
                <?php include("az.html"); ?>
              </td>
              <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
              <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
              <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
            </tr>
          </table>
        </td>
        <td width="530" rowspan="2" valign="top">          <img src="boxleft.gif" width="8" height="8" alt="">
          <table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr valign="top" align="left">
            <td width="8"><img src="boxtopleftcorner.jpg" width="8" height="8" alt=""></td>
            <td background="boxtop.jpg"><img src="boxtop.jpg" alt="" width="8" height="8"></td>
            <td width="8"><img src="boxtoprightcorner.jpg" width="8" height="8" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td background="boxtopleft.jpg" width="8"><img src="boxtopleft.jpg" width="8" height="8" alt=""></td>
            <td bgcolor="0080bb">
              <h1><font color="#FFFFFF" face="Arial, Helvetica, sans-serif">Chat &amp; Kirche<span class="&uuml;berschrift"><a name="inhaltsuebersicht"></a></span></font></h1>
            </td>
            <td background="boxtopright.jpg" width="8"><img src="boxtopright.jpg" width="8" height="8" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td width="8"><img src="boxdividerleft.jpg" width="8" height="13" alt=""></td>
            <td background="boxdivider.jpg"><img src="boxdivider.jpg" alt="" width="8" height="13"></td>
            <td width="8"><img src="boxdividerright.jpg" width="8" height="13" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td width="8" height="905" background="boxleft.gif">&nbsp;</td>
            <td bgcolor="#ffffff" class="L12">              <p class="text">    <img SRC="images-kirche-internet/chat-kirche-internet.jpg" style="border: double 4px #009999" align="right" hspace="10" vspace="10" name="Second Life aus dem Lexikon Kirche und Internet" alt="Second Life aus dem Lexikon Kirche und Internet" ><span class="&uuml;berschrift"><img src="caritas_mannheim_iconpunkt.gif" width="12" height="12"> Warum
                ist es wichtig?<a name="wichtig"></a></span></p>
              <p class="text">Chatten (engl. to chat = plaudern) ist f&uuml;r
                die meisten Jugendlichen selbstverst&auml;ndlich
                und ein fester Bestandteil Ihres Kommunikationsverhaltens. Wenn
                die Kirche die Jugendlichen &uuml;ber den Chat anspricht, ist
                das zum einen eine Wertsch&auml;tzung zum anderen aber auch das
                Zugehen der Kirche auf die Kultur der Jugendlichen. Vor allem
                aber kommt im Chat ein Gespr&auml;ch zustande, was bei einem
                realen Treffen, etwa in der Firmvorbereitung nicht immer der
                Fall ist.<br>
                Chats erm&ouml;glichen einen niederschwelligen ersten Kontakt.
                Durch den Schutz der Anonymit&auml;t bzw. durch das nicht pers&ouml;nlich
                vor Ort sein der Chattenden k&ouml;nnen auch heiklere Fragen
                er&ouml;rtert werden. </p>
              <p class="text">                    <span class="&uuml;berschrift"><img src="caritas_mannheim_iconpunkt.gif" width="12" height="12"> Wie
                  und von wem wird es genutzt?<a name="nutzung"></a> </span><span class="unterbild"><font size="1"><a href="#inhaltsuebersicht">(Inhaltsverzeichnis)</a></font></span></p>              
              <p class="text">Chats werden sehr vielf&auml;ltig genutzt. Nahezu
                jede Community bietet Chatfunktionen. Es gibt sogar spezielles
                Chatcommunitys wie etwa www.spin.de <br>
                    <br>
                    <span class="&uuml;berschrift"><img src="caritas_mannheim_iconpunkt.gif" width="12" height="12"> Gibt
                  es spezielle religi&ouml;se Nutzung?<a name="religioes"></a> </span><span class="unterbild"><font size="1"><a href="#inhaltsuebersicht">(Inhaltsverzeichnis)</a></font></span></p>
              <p class="text">Definitv: Ja. Chats werden vor allem als Instrument
                in der Seelsorge eingesetzt, wie z.B. bei der www.chatseelsorge.de
                Der gro&szlig;e Vorteil er Chats in der Seelsorge liegt darin,
                dass die Anonymit&auml;t es den Ratsuchenden erm&ouml;glicht
                offener &uuml;ber Ihre Probleme zu sprechen und leichter den
                Kontakt zu suchen. Der Chat bewahrt sprichw&ouml;rtlich vor dem
                Gesichtsverlust in einer face-2-face Situation eines normalen
                Gespr&auml;ches. <br>
                Ein gro&szlig;er religi&ouml;ser Chatraum im Internet ist der
                Kirchenchat in der virtuellen 2D Welt Funcity. Hier findet zweimal
                w&ouml;chentlich ein 2 Stunden Chat zu religi&ouml;sen Themen
                statt, der von ca. 20-30 Personen besucht wird.<br>
                Chats werden auch auf Gemeindeebene eingesetzt: So z.B. auf der
                Seite www.joerg-sieger.de in der Gemeinde Bruchsaal, wo der Pfarrer
                jeden Samstag einen Internetchat f&uuml;r die Gemeinde anbietet
                und Themen des Glaubens aber auch des Gemeindelebens er&ouml;rtert
                werden. <br>
Chats eignen sich f&uuml;r viele Gelegenheiten, wie z.B. die Firmvorbereitung.
Ehe ein erstes Treffen zwischen den Firmlingen und dem Weihbschof stattfindet
kann ein zweist&uuml;ndiger oder einst&uuml;ndiger Chat ein erstes Kennenlernen
erm&ouml;glichen. <br>
<br>
                    <br>
                    <span class="&uuml;berschrift"><img src="caritas_mannheim_iconpunkt.gif" width="12" height="12"> Was
                    sind die Implikationen f&uuml;r Kirche und Glaube?</span><a name="implikationen"></a><span class="&uuml;berschrift"> </span><span class="unterbild"><font size="1"><a href="#inhaltsuebersicht">(Inhaltsverzeichnis)</a></font></span><br>
                    <br>
                    Chats helfen dabei, pers&ouml;nliche Beziehungen zu initieren,
                    das Gruppengef&uuml;hl zu st&auml;rken und Beziehungsnetze
                    zu spannen. Sie sparen ebenfalls Zeit, wenn z.B. der Weihbischof
                    nicht zu einem Firmtreffen anreisen muss, sondern sich im
                    Chat mit den Jugendlichen trifft. Um die Jugend zu erreichen
                    stellen Chats ein ausgezeichnetes Mittel dar, da Chats zur
                    Kommunikationskultur der Jugendlichen dazugeh&ouml;ren. Die
                    Nutzung von Chats wird oft unter dem Aspekt der Seelsorge
                    (Anonymit&auml;t) gesehen. Die Bandbreite an Einsatzm&ouml;glichkeiten
                    von Chats ist allerdings enorm, vor allem in der Gemeinde
                    und in Gruppenprozessen, wie Firmungen, eigenen sich Chats
                    hervorragend, um einen ersten Kontakt herzustellen. Gerade
                    bei Firmgruppen und deren Begegnung mit der Amtskirche prallen
                    oftmals zwei Welten aufeinander, was sich dann so &auml;u&szlig;ert,
                    dass die Jugendlichen sehr schweigsam sind, wenn sie dem
                    Weihbischof in einem ersten Treffen gegen&uuml;bersitzen.
                    Durch einen Chat ist die Stimmung bei einem ersten Realtreffen
                    direkt wesentlich angenehmer: Die Jugendlichen gehen auf
                    den Bischof zu und begr&uuml;&szlig;en ihn mit &#8222;Du&#8220;,
                    was ja nicht ein Zeichen von Respektlosigkeit sondern von
                    Anerkennung ist. <br>
                    Im Chat selbst stellen die Jugendlichen auch heikle Fragen,
                    die sie besch&auml;ftigen, die sie aber im direkten Kontakt
                  bei einem ersten Treffen nicht stellen w&uuml;rden. </p>
              <p class="text"><span class="&uuml;berschrift"><img src="caritas_mannheim_iconpunkt.gif" width="12" height="12"> Wie
                  funktioniert es und was kann ich damit machen?<a name="funktionsweise"></a> </span><span class="unterbild"><font size="1"><a href="#inhaltsuebersicht">(Inhaltsverzeichnis)</a></font></span></p>
              <p class="text">Chats basieren darauf, dass man sich in Echtzeit
                Textnachrichten sendet. Diese werden auf dem Bildschirm in einem
                eigenen Fenster (Chatraum) angezeigt, so dass jeder Beteiligte
                diese lesen kann und reagieren kann, indem er selbst eine Textnachricht
                verfasst. In einem zweiten Fenster werden die Namen aller Beteiligten
                angezeigt. Das k&ouml;nnen die richtigen Namen sein oder anonyme
                Phantasienamen (sog. Nicknames oder kurz Nicks). Die Anzahl an
                Teilnehmern ist generell nicht beschr&auml;nkt, allerdings wird
                eine Unterhaltung ab 30 Personen fast unm&ouml;glich. Es gibt
                unmoderierte Chats, in die jeder einfach hineinschreiben kann
                oder moderierte Chats, wo ein Moderator den Ablauf steuert, indem
                er z.B. Teilnehmer sperrt oder ein Thema vorgibt, Fragen stellt
                etc.<br>
                Eine besondere Form stellt der Expertenchat dar. Dabei stellen
                die Teilnehmer dem Experten Fragen, auf die dieser antwortet.
                Den Teilnehmern ist es in der Regel m&ouml;glich, auch gezielt
                nur einzelne Teilnehmer anzuschreiben. Diese Nachrichten k&ouml;nnen
                dann nicht von dem Rest der Teilnehmer eingesehen werden (sog.
                fl&uuml;stern)<br>
                    <br>
                    <span class="&uuml;berschrift"><img src="caritas_mannheim_iconpunkt.gif" width="12" height="12"> Welche
                  M&ouml;glichkeiten bietet es technisch?<a name="moeglichkeiten"></a> </span><span class="unterbild"><font size="1"><a href="#inhaltsuebersicht">(Inhaltsverzeichnis)</a></font></span></p>
              <ul>
                <li class="text">Textnachrichten versenden</li>
                <li class="text"> evtl. Privatchats mit einzelnen Teilnehmern
                    (sog. fl&uuml;stern)</li>
                <li><span class="text"> evtl. Das Profil anderer Chatter ansehen (vor allem in Communities)</span>                </li>
              </ul>
                      <p><span class="&uuml;berschrift"><img src="caritas_mannheim_iconpunkt.gif" width="12" height="12"> Wohin
                  entwickelt es sich?<a name="entwicklung"></a> </span><span class="unterbild"><font size="1"><a href="#inhaltsuebersicht">(Inhaltsverzeichnis)</a></font></span></p>
                      <p class="text">Chats sind und waren immer ein fester Bestandteil
                        der Internetkommunikation. Sie werden auf absehbare Zeit
                        Ihren Reiz behalten und villeicht in Bezug auf die Handykommunikation
                        gr&ouml;&szlig;ere Bedeutung erfahren.</p>                      
                      <p>                        <span class="&uuml;berschrift"><img src="caritas_mannheim_iconpunkt.gif" width="12" height="12"> Pro
                          - Contra?<a name="pro"></a> </span><span class="unterbild"><font size="1"><a href="#inhaltsuebersicht">(Inhaltsverzeichnis)</a></font></span></p>
                      <p class="text">In drei Musterprojekten wurde der EInsatz
                        von Chats in der Firmvorbereitung getestet. Alles drei
                        Projekte brachten diesselben Ergebnisse: Der Weihbischof
                        war zufrieden, dass die Jugendlichen offen und interessiert
                        waren. Bei der ersten Begegnung war das Eis direkt geborchen
                        und es gab Gespr&auml;chstehmen, die nicht erst k&uuml;nstlich
                        erzeugt werden mussten. In seiner Predigt konnte er aufgrund
                        der Chaterfahrungen direkt auf die Lebenswelt der Jugendlichen
                        eingehen.<br>
                        Die Jugendlichen f&uuml;hlten sich wertgesch&auml;tzt, da der Bischof
                        sie in Ihrem bekannten Terrain, dem Chat, aufsuchte.
                      Sie konnten Ihre Fragen stellen.</p>
                      <p class="text">Auch andere Eins&auml;tze von Chats, z.B. auf
                        der Gemeindeebene, haben positive Ergebnisse gebracht:
                  Die Nutzer sch&auml;tzen die Eigenheiten des Mediums Chat.</p>
                      <p class="text"><span class="&uuml;berschrift"><img src="caritas_mannheim_iconpunkt.gif" width="12" height="12"> Anwedung
                          und Umsetzung</span> <a name="umsetzung" id="umsetzung"></a><span class="&uuml;berschrift"> </span><span class="unterbild"><font size="1"><a href="#inhaltsuebersicht">(Inhaltsverzeichnis)</a></font></span></p>
                  <p class="text">Chats lassen sich auf drei Weisen realisieren:</p>
                      <ul>
                        <li class="text">                          Man kann einen Chatraum
                          bei einem Anbieter kostenlos mieten, dann hat man meist
                          geringen Funktionsumfang,
                      u.U. Werbung, wie z.B. mainchat</li>
                        <li class="text">Man kann z.B. bei Parachat einen Chatraum
                            mieten (montaliche Basis) und diesen auf seiner Homepage
                          einbauen</li>
                        <li class="text">Sie k&ouml;nnen einen eigenen Chatserver betreiben
                            (relativ aufwendig)</li>
                      </ul>                      
                      <p class="text">                        Wenn Sie Fragen
                        dazu haben, wie SIe in Ihrem Arbeitsbereich Chats sinnvoll
                        einsetzen k&ouml;nnen,
                            und wie sich diese g&uuml;nstig realisieren lassen, sprechen
                        Sie uns gerne
                                an, wir haben bereits einige Chat-Projekte realisiert
                        und verf&uuml;gen &uuml;ber Erfahrung und ein kompetentes Team
                        an Technikern und Theologen. Wir bieten Ihnen auch Seminare
                        und Vortr&auml;ge zum Thema Kirche im Internet an:<br>
                        <br>
                        J&uuml;rgen Pelzer, emediage Medienentwicklungs und -beratungs
                        GmbH, Tel. 069 66370865, mail(at)juergenpelzer.de<br>
                        <br>
                        </span><span class="&uuml;berschrift"><br>
                        <img src="caritas_mannheim_iconpunkt.gif" width="12" height="12"> Hardfacts&amp; Linktipps</span> <a name="hardfacts"></a><span class="&uuml;berschrift"> </span><span class="unterbild"><font size="1"><a href="#inhaltsuebersicht">(Inhaltsverzeichnis)</a></font></span><br>                    
                        <br>
                        Kirchenchat in Funcity:</p>
                      <p class="text">Pfarrer Sieger und der Gemeindechat:<br>
                        <br>
                        <a href="http://www.netzinkulturation.de" target="_blank">                        <span class="&uuml;berschrift"><img src="caritas_mannheim_iconpunkt.gif" width="12" height="12"></span> Netzinkulturation.de
                                      - Die Anlaufstelle zu Infos rund um Kirche&amp;Internet
                        sowie speziell zu Chats im Kirchenbereich</a><br>
                        <a href="http://de.wikipedia.org/wiki/Second_Life" target="_blank"><br>
                        <span class="&uuml;berschrift"><img src="caritas_mannheim_iconpunkt.gif" width="12" height="12"></span> Second
                        Life Artikel in der Wikipedia                        </a><br>
                        <br>
                        <span class="&uuml;berschrift"> </span><span class="unterbild"><font size="1"><a href="#inhaltsuebersicht">(Inhaltsverzeichnis)</a></font></span>                        <br>
                      </p>
                </td>
            <td background="boxright.gif" width="8"><img src="boxright.gif" width="8" height="8" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td width="8"><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
            <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
            <td width="8"><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
          </tr>
        </table></td>
        <td width="120" rowspan="2" valign="top">	<script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
/* 120x600, Erstellt 11.06.08 */
google_ad_slot = "6662271223";
google_ad_width = 120;
google_ad_height = 600;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
</td>
      </tr>
      <tr>
        <td width="216" height="1374" align="left" valign="top">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
</body>
</html>
