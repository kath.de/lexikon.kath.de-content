<HTML><HEAD><TITLE>Kreuzweg - 14 Stationen</TITLE>
<META http-equiv=Content-Type content="text/html; charset=iso-8859-1"><LINK
title=fonts href="../philosophie_theologie/kaltefleiter.css" type=text/css
rel=stylesheet>
<META content="MSHTML 5.50.4134.600" name=GENERATOR></HEAD>
<BODY bgColor=#ffffff leftMargin=6 topMargin=6 marginheight="6" marginwidth="6">
<TABLE cellSpacing=0 cellPadding=6 width="100%" border=0>
  <TBODY>
  <TR>
    <TD vAlign=top align=left width=100>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left">
          <td width="8"><img src="../philosophie_theologie/boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="../philosophie_theologie/boxtop.gif"><img src="../philosophie_theologie/boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="../philosophie_theologie/boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="../philosophie_theologie/boxtopleft.gif"><img src="../philosophie_theologie/boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b>Religi&ouml;ses Leben</b></td>
          <td background="../philosophie_theologie/boxtopright.gif"><img src="../philosophie_theologie/boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="../philosophie_theologie/boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="../philosophie_theologie/boxdivider.gif"><img src="../philosophie_theologie/boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="../philosophie_theologie/boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="../philosophie_theologie/boxleft.gif"><img src="../philosophie_theologie/boxleft.gif" width="8" height="8" alt=""></td>
          <td> <?php include("logo.html"); ?> </td>
          <td background="../philosophie_theologie/boxright.gif"><img src="../philosophie_theologie/boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="../philosophie_theologie/boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="../philosophie_theologie/boxbottom.gif"><img src="../philosophie_theologie/boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="../philosophie_theologie/boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left">
          <td width="8"><img src="../philosophie_theologie/boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="../philosophie_theologie/boxtop.gif"><img src="../philosophie_theologie/boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="../philosophie_theologie/boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="../philosophie_theologie/boxtopleft.gif"><img src="../philosophie_theologie/boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="../philosophie_theologie/boxtopright.gif"><img src="../philosophie_theologie/boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="../philosophie_theologie/boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="../philosophie_theologie/boxdivider.gif"><img src="../philosophie_theologie/boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="../philosophie_theologie/boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="../philosophie_theologie/boxleft.gif"><img src="../philosophie_theologie/boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10"> <?php include("az.html"); ?> </td>
          <td background="../philosophie_theologie/boxright.gif"><img src="../philosophie_theologie/boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="../philosophie_theologie/boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="../philosophie_theologie/boxbottom.gif"><img src="../philosophie_theologie/boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="../philosophie_theologie/boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </TD>
    <TD vAlign=top rowSpan=2>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
        <TBODY>
        <TR vAlign=top align=left>
          <TD width=8><IMG height=8 alt=""
            src="../philosophie_theologie/boxtopleftcorner.gif" width=8></TD>
          <TD background=../philosophie_theologie/boxtop.gif><IMG height=8 alt=""
            src="../philosophie_theologie/boxtop.gif" width=8></TD>
          <TD width=8><IMG height=8 alt=""
            src="../philosophie_theologie/boxtoprightcorner.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=../philosophie_theologie/boxtopleft.gif><IMG height=8 alt=""
            src="../philosophie_theologie/boxtopleft.gif" width=8></TD>
          <TD bgColor=#e2e2e2>
            <H1><font face="Arial, Helvetica, sans-serif">Kreuzweg</font></H1>
          </TD>
          <TD background=../philosophie_theologie/boxtopright.gif><IMG height=8
            alt="" src="../philosophie_theologie/boxtopright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=13 alt="" src="../philosophie_theologie/boxdividerleft.gif"
            width=8></TD>
          <TD background=../philosophie_theologie/boxdivider.gif><IMG height=13
            alt="" src="../philosophie_theologie/boxdivider.gif" width=8></TD>
          <TD><IMG height=13 alt=""
            src="../philosophie_theologie/boxdividerright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=../philosophie_theologie/boxleft.gif><IMG height=8 alt=""
            src="../philosophie_theologie/boxleft.gif" width=8></TD>
          <TD class=L12>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td valign="top" class="L12"><P><STRONG><font face="Arial, Helvetica, sans-serif">14
                        Stationen des Leisdensweges Jesu</font></STRONG></P>
                  <P><font face="Arial, Helvetica, sans-serif">In den meisten
                      katholischen Kirchen finden sich entlang der Seitenw&auml;nde
                      Kreuzwegbilder oder auch Stationen. Anf&auml;nglich waren
                      es nur 7 Stationen, heute sind in den Kirchen meist 14
                      Stationen aufgeh&auml;ngt, die auf eine Tradition in Jerusalem
                      zur&uuml;ckgehen. Dort erinnert die Via Dolorosa, die Schmerzensstra&szlig;e,
                      an den Weg, auf dem Jesus das Kreuz aus der Stadt auf die
                      Anh&ouml;he Golgotha getragen hat. Am Karfreitag und anderen
                      Tagen gehen die Pilger seit Jahrhunderten diesen Weg. <br>
  Die Kreuzweg-Stationen sind: <br>
  1. Station: Jesus wird zum Tode verurteilt <br>
  2. Station: Jesus nimmt das Kreuz auf seine Schultern <br>
  3. Station: Jesus f&auml;llt zum ersten Mal unter dem Kreuz <br>
  4. Station: Jesus begegnet seiner Mutter <br>
  5. Station: Simon von Cyrene hilft Jesus das Kreuz tragen <br>
  6. Station: Veronika reicht Jesus das Schwei&szlig;tuch <br>
  7. Station: Jesus f&auml;llt zum zweiten Mal unter dem Kreuz <br>
  8. Station: Jesus begegnet den weinenden Frauen <br>
  9. Station: Jesus f&auml;llt zum dritten Mal unter dem Kreuz <br>
  10. Station: Jesus wird seiner Kleider beraubt <br>
  11. Station: Jesus wird an das Kreuz genagelt <br>
  12. Station: Jesus stirbt am Kreuz <br>
  13. Station: Jesus wird vom Kreuz abgenommen und in den Scho&szlig; seiner
  Mutter gelegt <br>
  14. Station: Der heilige Leichnam Jesu wird in das Grab gelegt. <br>
  Wenn keine Bilder vorhanden sind, finden sich zumindest kleine Kreuze an Stelle
  der Stationen. Die Gl&auml;ubigen gehen den Kreuzweg f&uuml;r sich allein.
  Bei Kreuzwegandachten geht der Vorbeter in der Kirche die Stationen ab und
  betet jeweils laut einen Text. Neuere Kreuzwege sehen im Schicksal Verfolgter
  und Gefolterter den Leidensweg Jesu. F&uuml;r den Kreuzweg gibt es viele Textvorlagen,
  die meist als kleine B&uuml;cher oder Schriften ver&ouml;ffentlicht sind. <br>
  Der Kreuzweg ist seit einigen Jahren von der Jugend wieder aufgegriffen worden.
  Der Bund der Deutschen Katholischen Jugend gibt dazu Bilder und Texte f&uuml;r
  den Kreuzweg heraus. <br>
  Kreuzwege finden sich auch au&szlig;erhalb der Kirchen. Die Stationen sind
  entlang eines Weges aufgestellt, der oft auf einen Berg f&uuml;hrt, der dann
  meist Kalvarienberg hei&szlig;t.<br>
                <font size="2">Eckhard Bieger</font><br>
                  </font> </P>
                  <P>&copy;<a href="http://www.kath.de"> <font face="Arial, Helvetica, sans-serif">www.kath.de</font></a></P></td>
                <td valign="top">

<a href="http://www.echter.konkordanz.de/product_info.php?info=p16620_Kreuzweg---Stationen-eines-Lebens.html">
            <h4>Das Buch zum Thema: "Kreuzweg - Stationen eines Lebens"</h4>

                <script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
google_ad_width = 160;
google_ad_height = 600;
google_ad_format = "160x600_as";
google_ad_type = "text_image";
//2007-09-05: Bildung und Erziehung
google_ad_channel = "3955521420";
google_ui_features = "rc:6";
//-->
            </script>
                    <script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
              </script>
                </td>
              </tr>
            </table>
            <P>&nbsp;</P>
            </TD>
          <TD background=../philosophie_theologie/boxright.gif><IMG height=8 alt=""
            src="../philosophie_theologie/boxright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=8 alt="" src="../philosophie_theologie/boxbottomleft.gif"
            width=8></TD>
          <TD background=../philosophie_theologie/boxbottom.gif><IMG height=8 alt=""
            src="../philosophie_theologie/boxbottom.gif" width=8></TD>
          <TD><IMG height=8 alt="" src="../philosophie_theologie/boxbottomright.gif"
            width=8></TD></TR></TBODY></TABLE></TD></TR>
  <TR>
    <TD vAlign=top align=left>&nbsp; </TD>
  </TR></TBODY></TABLE></BODY></HTML>