<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Orden</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="kaltefleiter.css" title="fonts">
</head>
<body bgcolor="#ffffff" leftmargin="6" topmargin="6" marginwidth="6" marginheight="6">
<table width="100%" border="0" cellspacing="0" cellpadding="6">
  <tr>
    <td width="100" align="left" valign="top"><table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b>religi&ouml;ses Leben</b></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td>
            <?php include("logo.html"); ?>
          </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>  
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10">
            <?php include("az.html"); ?>
          </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table></td>
    <td rowspan="2" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><h1>
                   <font face="Arial, Helvetica, sans-serif">Orden </font>              </h1>
          </td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="L12"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td valign="top" class="L12"><p class=MsoNormal><font face="Arial, Helvetica, sans-serif">Orden
                      sind Initiativen von Christen, die sich zu einem gemeinsamen
                      Leben und f&uuml;r besondere Aufgaben zusammenfinden. Im
                    Laufe der Entwicklung einer Gemeinschaft bildet sich eine
                    Ordnung f&uuml;r das Zusammenleben heraus. Davon leitet sich
                    der Name &#8222;Orden&#8220; ab. Die Benediktiner beispielsweise
                    nennen sich Ordo Sancti Benedicti (OSB) und erinnern so an
                    ihren Gr&uuml;nder, den Heiligen Benedikt von Nursia. Er
                    schrieb 529 seine Regel. Orden meint also zuerst die Ordnung
                    und dann erst den Orden, Der Benediktinerorden lebt nach
                    einer Ordnung, auch &#8222;Regel&#8220; genannt, die Benedikt
                    f&uuml;r das Kloster Monte Cassino, nachdem er mit Mitbr&uuml;dern
                    aus 12 kleineren Kl&ouml;stern dorthin &uuml;bergesiedelt
                    war ausarbeitete. Grundlegende Basis jeder Ordensgemeinschaft
                    ist nicht nur die Regel, sondern die regelm&auml;&szlig;ige
                    Lekt&uuml;re der Bibel, um das Leben an ihren Prinzipien
                    auszurichten.</font>
                  <p><font face="Arial, Helvetica, sans-serif">  Orden spiegeln jeweils die Anforderungen ihrer Zeit wider.
                    In der langen Geschichte der Kirche ist daher eine gro&szlig;e
                    Zahl verschiedener Gemeinschaften entstanden. Die Orden versuchen,
                    sowohl dem Ruf des Evangeliums als auch den Zeichen der Zeit
                    zu folgen und geben so mit ihrer Gr&uuml;ndung, ihrer Existenz
                    und ihrem Erfolg Antwort auf die Sorgen der jeweiligen Zeit. </font></p>
                  <p><font face="Arial, Helvetica, sans-serif"><strong>Inspiriert durch die Bibel:</strong><br>
                    <br>
                  </font><font face="Arial, Helvetica, sans-serif"> Der
                      biblische Grundgedanke des Ordenslebens ist das Leben in
                      Gemeinschaft. Alle vier Evangelien berichten davon,
                      wie Jesus um sich eine J&uuml;ngergemeinschaft sammelt
                      und mit den engsten von ihnen die Gemeinschaft der Zw&ouml;lf
                      bildet. Die Zw&ouml;lf repr&auml;sentieren das Zw&ouml;lf-St&auml;mme-Volk
                      Israel, das Gott im Alten Bund auserw&auml;hlt hat, um
                      so alle V&ouml;lker zum Heil zu f&uuml;hren. Nach seinem
                      Tod und seiner Auferstehung geht seine Aufgabe auf die
                      Zw&ouml;lf &uuml;ber. Der Auferstandene sendet sie aus,
                      um die Heilsbotschaft allen, Juden wie Heiden, zu verk&uuml;nden.
                      Deshalb wollen alle Orden die Vita apostolica, das apostolische
                      Leben der ersten Generation der Christen fortsetzen.<br>
                    Altes und Neues Testament zeigen, dass Gott Gemeinschaft
                    mit den Menschen will. Ebenso zeigt die Heilige Schrift,
                    dass der Mensch, der an Gott glaubt und mit ihm Gemeinschaft
                    sucht, selbst eine Gemeinschaft braucht. Auch wenn Jesus
                    einzelne Menschen anspricht oder heilt, wird an ihnen stellvertretend
                    das Heil f&uuml;r alle Menschen sichtbar. Der Einzelne bekundet
                    seinen Glauben an Gott nie allein. Maria beispielsweise sagt
                    ihr &#8222;Ja&#8220; zu Gottes Plan, wird aber im Folgenden
                    immer in Gemeinschaft dargestellt. Mit ihr ist Josef, dann
                    auch Elisabeth, unter dem Kreuz ist der Lieblingsj&uuml;nger
                    Jesu bei ihr und in der Urgemeinde wohnt sie zusammen mit
                    den J&uuml;ngern. Christentum ist somit immer gemeinsamer
                  Glaube an Gott. </font></p>
                  <p><font face="Arial, Helvetica, sans-serif"><br>
  Dieser Grundgedanke inspiriert jede Ordensgr&uuml;ndung,
                    so verschieden sie untereinander auch sein m&ouml;gen. Es
                    geht darum, Jesus nachzufolgen, so wie ihm schon die J&uuml;nger
                    nachgefolgt sind, n&auml;mlich in Gemeinschaft. &#8222;Wo
                    zwei oder drei in meinem Namen versammelt sind, da bin ich
                    mitten unter ihnen.&#8220; (Mt 18,20) Deshalb stellen die
                    verschiedenen Ordensgemeinschaften keine Randerscheinung
                    in der Kirche dar, sondern sind wesentlicher Bestandteil
                    des kirchlichen Lebens. Sie betonen durch alle Zeiten hindurch
                    den Ruf Jesu: &#8222;Kommt her, folgt mir nach! Ich werde
                    euch zu Menschenfischern machen.&#8220; (Mt 4,19) Mit ihrer
                    Existenz stellen sie aber nicht nur die f&uuml;r den Glauben
                    wesensnotwendige Seite der Nachfolge in Gemeinschaft dar.
                    Mit ihren verschiedenen T&auml;tigkeiten verwirklichen sie
                    seit 2000 Jahren konsequent das Evangelium Jesu. Die Kirche
                    sieht daher in den Orden das sichtbare Zeichen f&uuml;r das
                    Wirken des Heiligen Geistes, der die Kirche inspiriert und
                    nicht zuletzt in neuen Ordensgr&uuml;ndungen Antworten auf
                    die Herausforderungen der Zeit gibt. Es wird daher in jeder
                    k&uuml;nftigen Epoche auch neue Ordensgr&uuml;ndungen geben.
                    Diese Vielfalt an Gemeinschaften h&auml;lt die Kirche am
                    Leben, garantiert ihre Offenheit f&uuml;r das Streben des
                    Heiligen Geistes, ruft sie beharrlich zur Einheit und Geschwisterlichkeit
                    und macht sie fruchtbar f&uuml;r Neues.</font></p>
                  <p><font face="Arial, Helvetica, sans-serif"><strong>Die Orden in Kirche und Gesellschaft</strong><br>
                    <br>
                  </font><font face="Arial, Helvetica, sans-serif"> Das
                      Ordensleben ist eine auf Dauer angelegte Lebensform in
                      einer von der katholischen Kirche anerkannten Gemeinschaft.
                      Somit stehen die Orden in Einheit mit der Gesamt- und Ortskirche,
                      aber auch in gesunder Spannung zu ihr. Alle Ordenschristen
                      leben die klassischen Evangelischen R&auml;te der Armut,
                      der Ehelosigkeit und des Gehorsams. Damit sind Ordensleute
                      eine Herausforderung an die jeweilige Gesellschaft, in
                      der Verg&ouml;tzung von Sexualit&auml;t, Besitz und Eigentum
                      oder die &Uuml;berbewertung der Macht die gro&szlig;en
                      Themen sind. Die Ordensleute verk&ouml;rpern mit ihrem
                      geweihten Leben und ihrer apostolischen T&auml;tigkeit
                      am Menschen und seinen N&ouml;ten die Z&auml;rtlichkeit
                      Gottes. Sie geben eine wichtige Antwort auf die Sehnsucht
                      des Menschen nach Liebe, Gl&uuml;ck und Heil, nach dem
                      Unendlichen, nach Gott. <br>
                    Die Orden richten ihr Leben an Bibel, Ordensregel und an
                    der Spiritualit&auml;t des Gr&uuml;nders aus. Dabei nimmt
                    bei allen Gemeinschaften die Sorge um die Armen einen besonderen
                    Platz ein. Gebet und Feier des Gottesdienstes dr&auml;ngen
                    zum Dienst an Notleidenden, weil gilt: &#8222;Was ihr einen
                    meiner geringsten Br&uuml;der getan habt, habt ihr mir getan.&#8220; (Mt
                    25,40). Deshalb waren die Orden durch einen Gro&szlig;teil
                    der Geschichte hindurch das tragende Sozialsystem, lange
                    bevor es f&uuml;r die Gesellschaft eine staatliche Sozialversicherung
                    gab. <br>
                    Neben der praktizierten N&auml;chstenliebe sind die Gemeinschaften
                    auch Orte des Studiums der biblischen Schriften und der Weitergabe
                    des Glaubens. Jede der Gemeinschaften betont einen besonderen
                    Akzent der christlichen Glaubenslehre und verhindert so,
                    dass dieser aus der F&uuml;lle des Glaubens verschwindet.
                    Zudem sind viele Orden Werkzeuge einer inkulturierten Evangelisation,
                    weil sich ihre Mitglieder aus verschiedenen Nationen, Kulturen
                    und St&auml;nden zusammensetzen. Sie werden dort Sauerteig
                    der Herrschaft Gottes, wo kulturelle Werte von Minderheiten
                    unterdr&uuml;ckt werden. Gleichzeitig bildet ihre weltumspannende
                    Verbindung die Grundlage zum &ouml;kumenischen, interreligi&ouml;sen
                    oder interkulturellen Dialog. Damit geben die Orden auch
                  in Zukunft wichtige Antworten auf die Fragen der Zeit.</font></p>
                  <p><font face="Arial, Helvetica, sans-serif"><strong>Der Begriff &#8222;Orden&#8220;</strong><br>
                    <br>
                  </font><font face="Arial, Helvetica, sans-serif"> Das
                      Wort &#8222;Orden&#8220; kommt vom Lateinischen &#8222;ordo&#8220;,
                    was in diesem Fall mit &#8222;Stand&#8220; zu &uuml;bersetzen
                    ist. Das deutsche Wort &#8222;Ordnung&#8220; hat den gleichen
                    Ursprung, bezeichnet doch das lateinische &#8222;ordo&#8220; auch
                    die Reihenfolge, die Ordnung, das Glied, die Regel und die
                    Abteilung (z. B. eine Zenturie). Die Tradition der katholischen
                    Kirche fasst alle Ordensleute unter dem Begriff &#8222;Ordensstand&#8220; (lat. &#8222;status
                    religiosus&#8220;) zusammen, wobei das Wort &#8222;Ordensstand&#8220; eine
                    unn&ouml;tige Verdoppelung darstellt, schlie&szlig;lich ist &#8222;Stand&#8220; die &Uuml;bersetzung
                    von &#8222;Orden&#8220;. &#8222;status religiosus&#8220; m&uuml;sste
                    im Deutschen richtig &uuml;bersetzt &#8222;Stand der Religiosen&#8220; lauten,
                    womit die Ordenschristen, wie in anderen europ&auml;ischen
                    Sprachen &uuml;blich, als Religiosen zu bezeichnen w&auml;ren.
                    Im Deutschen haben sich aber entweder die Einzeltitel (Schwester,
                    Pater, Bruder) oder die allgemeinen Bezeichnungen Ordensfrauen,
                    Ordenm&auml;nner, M&ouml;nche und Nonnen durchgesetzt. <br>
                    Das Zweite Vatikanische Konzil betont das allgemeine Priestertum
                    aller Gl&auml;ubigen neu und verzichtet in diesem Zusammenhang
                    auf die Einteilung in verschiedene St&auml;nde. Vielmehr
                    sind alle Gl&auml;ubigen, jeder gem&auml;&szlig; dem an ihn
                    ergangenen Ruf Gottes, zum Stand der Vollkommenheit berufen.
                    Als Hilfe auf diesem Weg sollen alle versuchen, die evangelischen
                    R&auml;te Armut, Keuschheit, Gehorsam zu leben. Sie sind
                    demnach nicht mehr nur auf die Ordensleute beschr&auml;nkt,
                    sondern sollen von allen als &#8222;Werkzeuge zur Vollkommenheit&#8220; (Thomas
                    von Aquin) genutzt werden. Die Orden werden nach dem Zweiten
                    Vatikanum mit einem neuen Begriff bezeichnet. Anstelle des
                    traditionellen &#8222;Ordensstandes&#8220; wird bevorzugt
                    vom &#8222;geweihten Leben&#8220; (&#8222;vita consecrata&#8220;)
                  gesprochen, so z.B. im neuen Kirchenrecht.</font></p>
                  <p><font face="Arial, Helvetica, sans-serif"><strong>Formen
                        und Geschichte: ein kurzer &Uuml;berblick</strong><br>
                  <br>
                  </font><font face="Arial, Helvetica, sans-serif"> Nach
                  einer Einf&uuml;hrung von meistens einem Jahr (Noviziat)
                    und einer Probezeit von mindestens drei Jahren wird der Bewerber
                    volles Mitglied, indem er &ouml;ffentlich die Gel&uuml;bde
                    ablegt. Trotz ihrer unterschiedlichen Gestalt und Betonung
                    gehen die Gel&uuml;bde grunds&auml;tzlich auf die Evangelischen
                    R&auml;te der Ehelosigkeit, der Armut und des Gehorsams zur&uuml;ck.
                    Sie sollen in die vertiefte Christusnachfolge einf&uuml;hren. <br>
                    Bei den weiteren Erscheinungsformen gibt es viele Unterschiede.
                    Bei einigen Gemeinschaften legt der Betreffende in mehreren
                    Stufen unterschiedliche Gel&uuml;bde ab, bis er vollst&auml;ndig
                    in den Orden aufgenommen ist. Auch sind je nach Orden weitere
                    Riten mit dem feierlichen Eintritt verbunden. So verpflichtet
                    sich beispielsweise ein neuer Benediktiner sein Leben lang
                    in einem bestimmten Kloster zu leben (stabilitas loci), in
                    dem ein Bereich f&uuml;r die Au&szlig;enwelt unzug&auml;nglich
                    ist (Klausur). Dar&uuml;ber hinaus sind die t&auml;gliche
                    Feier des Stundengebets und der Eucharistie wichtige Akzente
                    dieser Ordensgemeinschaft. Als &auml;u&szlig;eres Zeichen
                    f&uuml;r ihr geweihtes Leben tragen viele Ordenschristen
                    ein Gewand (Habit), das sie mit Eintritt in den Orden erhalten
                    (Einkleidung). Andere hingegen tragen &uuml;berwiegend Zivilkleidung
                    (teilweise mit einem sichtbaren kleinen Kreuz). Einige Schwerpunkte
                    von Ordensgemeinschaften sind beispielsweise Erziehung und
                    Unterricht von Kindern, das Studium und die Lehre des katholischen
                    Glaubens, die Pflege von Kranken, die Versorgung Obdachloser
                    oder die Betreuung jugendlicher Stra&szlig;enkinder.<br>
                    Die Organisationsformen der Gemeinschaften sind ebenso verschieden.
                    Die rechtliche Verfassungsform l&auml;sst sich in f&ouml;deralistische
                    und zentralistische Ordnungsstrukturen unterscheiden. Als
                    f&ouml;deralistische Verfassungsform bezeichnet man rechtlich
                    selbstst&auml;ndige Kl&ouml;ster. Zentralistisch verfasst
                    sind mehrere unselbstst&auml;ndige Niederlassungen, die zu
                    einer Provinz zusammengefasst sind. Diese untersteht einem
                  Ordensoberen, der den ganzen Orden repr&auml;sentiert. </font></p>
                  <p><font face="Arial, Helvetica, sans-serif"><br>
  Diese organisierten Gemeinschaften entstehen erst im hohen
                    Mittelalter. Aus der mittelalterlichen Reformbewegungen (z.B.
                    Cluny) heraus bilden sich verschiedene Verb&auml;nde und
                    Reformgruppen, von denen einige das monastische Gemeinschaftsleben
                    mit dem eremitischen Leben verbinden. Bei all diesen Bewegungen
                    ist die Bindung an eine Regel entscheidend. Viele Orden,
                    so die Zisterzienser und Trappisten, leben nach der Benediktregel,
                    deren Befolgung in den so genannten Consuetudines konkretisiert
                    ist. Des Weiteren entwickelten sich im Mittelalter Priestergemeinschaften,
                    die sich in sogenannten Stiften zusammenschlossen und sich
                    meist an der Augustinus-Regel orientierten. Im Mittelalter
                    ver&auml;ndert nicht nur die Vielzahl der Orden die religi&ouml;se
                    Landkarte, sondern auch das vermehrte Auftreten von weiblichen
                    Gemeinschaften. <br>
                    Ein entscheidender Schirtt stellen die Bettelorden des 13.
                    Jahrhunderts dar (Franziskaner, Dominikaner, Augustiner-Eremiten,
                    Karmeliten). Sie sind das Ergebnis der sozialen, wirtschaftlichen
                    und kulturellen Wandlungen des 12. und 13. Jahrhunderts.
                    Auf den Aufschwung der St&auml;dte und die Not der armen
                    Bev&ouml;lkerung geben die Bettelorden eine Antwort. Sie
                    interpretieren das Ordensleben f&uuml;r die Stadt um, indem
                    sie sich in neuen Organisationsformen (ortsunabh&auml;ngige
                    Personalverb&auml;nde) einer apostolischen T&auml;tigkeit
                    durch p&auml;pstlichen Auftrag zuwenden und dabei auf jeglichen
                    Besitz und feste Eink&uuml;nfte verzichten. In Zuge ihrer
                    Gr&uuml;ndung schlie&szlig;en sich weibliche Ordenszweige
                    an, die als Zweite Orden der Verbandsstruktur angegliedert
                    werden. Im weiteren Umfeld schlie&szlig;en sich Laien der
                    Bewegung an, die als so genannte Dritte Orden entweder bruderschafts&auml;hnlich
                    in der Welt oder innerhalb einer kl&ouml;sterlichen Gemeinschaft
                    leben.</font></p>
                  <p><font face="Arial, Helvetica, sans-serif"><br>
  Die Reformbewegungen des sp&auml;ten Mittelalters lassen
                    die Regularkleriker entstehen (Kleriker, die nach einer gemeinsamen
                    Regel leben). Diese Gemeinschaften von Priestern wenden sich
                    pastoralen T&auml;tigkeiten zu und verzichten auf traditionelle
                    Formen des Ordenslebens (Ordenstracht, gemeinsames Chorgebet).
                    Die Hochform dieses Typs sind die Jesuiten. Diese Gemeinschaften
                    pr&auml;gen das Bild der katholischen Kirche nach der Reformation.
                    Dazu geh&ouml;ren auch Frauengemeinschaften, die das traditionelle
                    weibliche Ordensleben umgestalten.</font></p>
                  <p><font face="Arial, Helvetica, sans-serif"><br>
  Ab dem 17. Jahrhundert spricht die Kirchengeschichte nicht
                    mehr von Ordensgr&uuml;ndungen, sondern von Kongregationen.
                    Sie nehmen die Verfassung, den Lebensstil und die apostolische
                    T&auml;tigkeit der Regularkleriker auf und legen beim Eintritt
                    in die Gemeinschaft nur einfache Gel&uuml;bde ab. Im Vordergrund
                    ihrer T&auml;tigkeit steht die Pastoral, wie z. B. Volksmission,
                    religi&ouml;se Erneuerung, Schult&auml;tigkeit, Priesterausbildung,
                    Missionsarbeit in den &Uuml;bersee-Kolonien. Die Kongregationen
                    unterscheiden sich untereinander durch ihre je eigene Spiritualit&auml;t,
                    die zwar im Zusammenhang mit der kirchlichen Tradition steht,
                    aber besondere Schwerpunkte setzt (z.B. Eucharistie, Maria,
                    Herz Jesu). Ab dem 19. Jahrhundert f&uuml;hrt die Industrialisierung
                    zur Gr&uuml;ndung von Kongregationen mit sozial-caritativem
                    und erzieherischem Apostolat. In dieser Zeit wurden die meisten
                    weiblichen Gemeinschaften in der ganzen Ordensgeschichte
                    gegr&uuml;ndet. Organisiert in zentralen Verb&auml;nden mit
                    einer Generaloberin werden sie bis ins 20. Jahrhundert hinein
                    zu wichtigen Tr&auml;gerinnen des kirchlichen Ansehens. Durch
                    ihre Arbeit in Schule, Armenf&uuml;rsorge und Krankenpflege
                    leisteten sie einen entscheidenden Beitrag f&uuml;r den sozialen
                    Aufstieg der Frau in der Gesellschaft und in der Kirche. </font></p>
                  <p><font face="Arial, Helvetica, sans-serif"><br>
                      <strong>In j&uuml;ngster Zeit &#8211; nach dem Zweiten Vatikanischen
                    Konzil </strong>&#8211; treten modifizierte Formen der Kongregationen
                    auf. Man nennt sie zum einen &#8222;Gesellschaften des apostolischen
                    Lebens&#8220;, weil sie Gemeinschaften ohne &ouml;ffentliche
                    Gel&uuml;bde sind, und zum anderen S&auml;kularinstitute.
                    Daneben gibt es viele Gemeinschaften, die noch nicht voll
                    kirchlich anerkannt sind. </font></p>
                  <p>Sebastian Pilz<br>
                  </p>
                </td>
                <td valign="top"><p><a href="www.alexianerkloster.de" target="_blank"><img src="banner-alexianer-lexika-07-09.jpg" width="286" height="70" border="0"></a>
                  </p>
                  <p>
                    <script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
google_ad_width = 160;
google_ad_height = 600;
google_ad_format = "160x600_as";
google_ad_type = "text_image";
//2007-09-05: Bildung und Erziehung
google_ad_channel = "3955521420";
google_ui_features = "rc:6";
//-->
                      </script>
                      <script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
                            </script>
                  </p></td>
              </tr>
            </table>
            <p class=MsoNormal><BR>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
</table>
</body>
</html>
