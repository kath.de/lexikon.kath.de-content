<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Krankenbruder</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="kaltefleiter.css" title="fonts">
</head>
<body bgcolor="#ffffff" leftmargin="6" topmargin="6" marginwidth="6" marginheight="6">
<table width="100%" border="0" cellspacing="0" cellpadding="6">
  <tr>
    <td width="100" align="left" valign="top"><table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b>Religi&ouml;ses Leben</b></td>
         <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td>
            <?php include("logo.html"); ?>
          </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
    <td rowspan="2" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><h1>
                                    Krankenbruder</h1>
          </td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="L12"> <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td valign="top" class="L12"><p class=MsoNormal><font face="Arial, Helvetica, sans-serif">Pflegen
                    aus christlicher Motivation </font>
                <P><font face="Arial, Helvetica, sans-serif">Krankenpflege scheint
                    eine Aufgabe von Frauen zu sein. Erst in den letzten Jahren
                    w&auml;hlen auch M&auml;nner diesen Beruf und qualifizieren
                    sich mit Frauen in den Krankenpflegeschulen in einer dreij&auml;hrigen
                    Ausbildung. Es scheint so, da&szlig; M&auml;nner diesen Beruf
                    erst heute entdecken. Blickt man weiter in der Geschichte
                    zur&uuml;ck, waren M&auml;nner aktiv in der Krankenpflege
                    t&auml;tig. Oft verstanden sie es als Auftrag Jesu, sich
                    um um die Armen zu k&uuml;mmern und die Kranken zu besuchen.
                    Angso&szlig;en durch diese christliche Motivation entdeckten
                    M&auml;nner, da&szlig; sie f&uuml;r diese Aufgabe auch eine
                    Begabung haben. Auf dem Boden des Christentum hat es viele
                    Initiativen gegeben, die Kranken nicht unversorgt zu lassen,
                    denn Jesus hatte ein Herz f&uuml;r die Kranken und hat viele
                    geheilt, darunter waren sicher viele, deren Krankheit psychisch
                    bedingt war. Schon in den ersten Jahrhunderten, noch in der
                    Zeit der Verfolgung unterhielten Christen kleine Krankenh&auml;user.
                    Als im Mittelalter in den St&auml;dten die Kranken, vor die
                    psychisch Kranken niemanden fanden, der sich ihrer annahm,
                    waren es im Rheinland und in den Niederlanden die Beginen
                    und die <a href="begarden.php">Begarden</a>, die eine Berufung
                    f&uuml;r diese Aufgabe erkannten. Sie schlossen sich in kleinen
                    Gruppen zusammen, richteten Hospize ein, bauten Kapellen
                    und wurden langsam zu einem Orden, der 1472 durch Papst Sixtus
                    IV. endg&uuml;ltig anerkannt wurde. Da die Begarden sich
                    den hl. Alexius als Patron w&auml;hlten, hei&szlig;en bis
                    heute <a href="alexianer.php">Alexianer</a>. Sie sind Krankenbr&uuml;der,
                    d.h. sie &uuml;ben den Beruf der Krankenpflege innerhalb
                    einer Ordensgemeinschaft aus und verbinden den Pflegedienst
                    mit gemeinsamen Gebetszeiten und einer gemeinsamen Lebensordnung,
                    die sie aus der Augstinusregel entwickelt haben.</font></P>
                <P><font face="Arial, Helvetica, sans-serif"><a href="http://www.alexianerkloster.de/alexianerkloster/index.php">Alexianer
                      Orden</a></font></P>
                <P><font size="2" face="Arial, Helvetica, sans-serif">&copy; kath.de</font><font face="Arial, Helvetica, sans-serif"></font></P></td>
              <td valign="top"><script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
google_ad_width = 160;
google_ad_height = 600;
google_ad_format = "160x600_as";
google_ad_type = "text_image";
//2007-09-05: Bildung und Erziehung
google_ad_channel = "3955521420";
google_ui_features = "rc:6";
//-->
          </script>
                  <script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
            </script>
              </td>
            </tr>
          </table>            <p class=MsoNormal><font face="Arial, Helvetica, sans-serif"><br>
          </font>          
          <BR>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td align="left" valign="top"><table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10">
            <?php include("az.html"); ?>
          </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</body>
</html>
