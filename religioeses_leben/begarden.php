<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Begarden</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="kaltefleiter.css" title="fonts">
</head>
<body bgcolor="#ffffff" leftmargin="6" topmargin="6" marginwidth="6" marginheight="6">
<table width="100%" border="0" cellspacing="0" cellpadding="6">
  <tr>
    <td width="100" align="left" valign="top"><table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b>Religi&ouml;ses Leben</b></td>
         <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td>
            <?php include("logo.html"); ?>
          </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
    <td rowspan="2" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><h1>
                                    <font face="Arial, Helvetica, sans-serif">Begarden</font></h1>
          </td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="L12"><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td valign="top" class="L12"><font face="Arial, Helvetica, sans-serif">Eine
                  mittelalterliche Sozialbewegung
                  <p class=MsoNormal>Jede Gesellschaft schlie&szlig;t Menschen
                    aus, weil sie anders sind oder nicht mithalten k&ouml;nnen.
                    Opfer dieses Mechanismus sind Menschen mit psychischen St&ouml;rungen.
                    Wer krank wird, wird von anderen als Last empfunden. Es gibt
                    auch immer wieder Menschen, die sich der Kranken und Armen
                    annehmen. Die Botschaft Jesu motiviert in besonderer Weise
                    dazu, gerade im Armen und im Kranken Jesus selbst erkennen.<br>
  Als im Mittelalter viele Menschen in die St&auml;dte zogen, gelang es den einen,
  als Handwerker und Kaufleute reich zu werden. Aber es entstand auch damals
  schon ein Proletariat. Um die Wende zum 14. Jahrhundert widmeten sich M&auml;nner
  und Frauen der Kranken und Armen, sie gr&uuml;ndeten Hospize und gaben den
  Armen zu Essen. Im Rheinland und in den Niederlanden, zu denen das heutige
  Belgien geh&ouml;rt, wurde die Frauen Beginen und die M&auml;nner Begarden
  oder z.B. in Antwerpen Celliten genannt. Sie lebten in Gruppen zusammen. Als
  im 14. Jahrhundert die gro&szlig;en Pestepedemien besonders in den St&auml;dten
  w&uuml;teten, war es die innere spirituelle Kraft der Begarden, die sie bei
  den Kranken ausharren lie&szlig;. Sie versorgten und begruben sie. Viele der
  Br&uuml;der steckten sich an und manche Gemeinschaft wurde durch die Pest fast
  ausgel&ouml;scht. Die Stadtv&auml;ter erkannten, wie wichtig der Dienst dieser
  Freiwilligen war und &uuml;bertragen ihnen das Bestattungswesen. Bereits im
  Mittelalter gingen die Begarden mit dem psychisch kranken Menschen anders als
  die Gesellschaft um. Sie hatten erkannt, da&szlig; diese nicht von D&auml;monen
  besessen waren und nicht in T&uuml;rme und Verlie&szlig;en wie Verbecher angekettet
  und hinter Gittern verschlossen werden mu&szlig;ten. Sie nahmen solche Menschen
  in ihre Hospize auf. Diese Tradition haben die <a href="alexianer.php">Alexianer</a> fortgef&uuml;hrt.
  Das ist ein Krankenpflegeorden, der aus der Bewegung der Begarden hervorgegangen
  ist und heute Krankenh&auml;user betreibt.<br>
                                </font>
                <p class=MsoNormal><font size="2" face="Arial, Helvetica, sans-serif">&copy; kath.de </font></td>
              <td valign="top"><script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
google_ad_width = 160;
google_ad_height = 600;
google_ad_format = "160x600_as";
google_ad_type = "text_image";
//2007-09-05: Bildung und Erziehung
google_ad_channel = "3955521420";
google_ui_features = "rc:6";
//-->
          </script>
                  <script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
            </script>
              </td>
            </tr>
          </table>            <p class=MsoNormal>&nbsp;             
            <P STYLE="margin-bottom: 0cm"><BR>
          </P>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td align="left" valign="top"><table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10">
            <?php include("az.html"); ?>
          </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</body>
</html>
