<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Alexianer</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="kaltefleiter.css" title="fonts">
</head>
<body bgcolor="#ffffff" leftmargin="6" topmargin="6" marginwidth="6" marginheight="6">
<table width="100%" border="0" cellspacing="0" cellpadding="6">
  <tr>
    <td width="100" align="left" valign="top"><table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b>Religi&ouml;ses Leben</b></td>
         <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td>
            <?php include("logo.html"); ?>
          </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
    <td rowspan="2" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><h1>
                                    <font face="Arial, Helvetica, sans-serif">Alexianer </font></h1>
          </td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="L12"> <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td valign="top" class="L12"><p class=MsoNormal><font face="Arial, Helvetica, sans-serif">Soziale
                    Initiatve des Mittelalters </font>
                <P STYLE="margin-bottom: 0cm"><font face="Arial, Helvetica, sans-serif">Dieser
                    Krankenpflegeorden hat seinen Wurzeln in der mittelalterlichen <a href="begarden.php">Begarden-Bewegung</a>.
                    Wie Frauen sich im Mittelalter als Beginen zusammenschlossen, &uuml;bernahmen
                    auch M&auml;nner Dienste an Kranken und waren in den Pestzeiten
                    des 14. Jahrhunderts diejenigen, die die Kranken versorgten
                    und die Toten bestatteten. Sie nahmen sich auch der Menschen
                    an, um die sich niemand k&uuml;mmerte, die geistig Kranken.
                    Da die Begarden Hospize betrieben und Ressourcen f&uuml;r
                    die Armenspeisung brauchten, mu&szlig;ten sie ihrem Leben
                    eine Form geben. Da sie aus einem christlichen Geist heraus
                    handelten, wollten sie die Anerkennung der Kirchen. Sie entschieden
                    sich f&uuml;r die Regel, die der nordafrikanische Bischof
                    Augustinus entwickelt hatte und die im Mittelalter von vielen
                    Gemeinschaften als Grundlage ihrer Lebensform gew&auml;hlt
                    wurde. Luther geh&ouml;rte zu einem Orden, der sich die Augustinusregel
                    zu eigen gemacht hatte.<br>
  Da Augustinus als gelehrter Bischof nicht als Identifikationsfigur f&uuml;r
  einen Orden dienen konnte, der sein Ideal in der praktischen T&auml;tigkeit
  sieht und nicht im Bereich der Theologie, suchten die Begarden nach einem Vorbild
  und w&auml;hlten den damals h&auml;ufig verehrten Alexius. Fasziniert hat sie
  an diesem Mann, da&szlig; er auf ein gro&szlig;es Erbe und ein Leben als hochangesehener
  B&uuml;rger verzichtet hat. Er soll Sohn des r&ouml;mischen Senators Euphemianus
  gewesen sein. Am Tag seiner Hochzeit, es ist um das Jahr 400 herum, h&ouml;rt
  er einen Ruf, geht in den Osten und lebt in Edessa. Dort verschenkt er seine
  Habe und k&uuml;mmert sich um die Armen. Er kehrt nach Rom zur&uuml;ck, sein
  Vater erkennt ihn in der abgerissenen Gestalt nicht wieder, nimmt ihn aber
  in sein Haus auf. Dort verrichtet er 17 Jahre lang niedere Dienste. Seine Kammer
  liegt unter einer Treppe. Als er stirbt, sollen die Glocken von Rom gel&auml;utet
  haben. Die Menschen h&ouml;rten eine Stimme aus dem Himmel: &#8222;Sucht den
  Mann Gottes, damit er f&uuml;r Rom bete.&#8220; Aus seinen Aufzeichnungen erkennt
  die Familie, da&szlig; sie ihn unerkannt beherbergt hat. Dieser Heilige ist
  wahrscheinlich deshalb f&uuml;r die Begarden so faszinierend gewesen, weil
  auch sie meist in der Stadt t&auml;tig waren, wo ihre Verwandten wohnten. Auch
  die Begarden hatten sich den einfachen Diensten verschrieben, wollten nicht
  in der &Ouml;ffentlichkeit auftreten, brauchten aber zugleich die Unterst&uuml;tzung
  der Wohlhabenden, um den Armen zu Essen zu geben und die Kranken zu versorgen.
  Die heutigen <a href="http://www.alexianerkloster.de/alexianerkloster/index.php">Alexianer</a> k&uuml;mmern
  sich um die psychisch Kranken. Der heilige Alexius ist ihnen deshalb Vorbild,
  weil an seinem Leben deutlich wird, da&szlig; der unauff&auml;llige Dienst
  dem Wohl der Stadt dient.</font></P>
                <P STYLE="margin-bottom: 0cm">&nbsp;</P>
                <P STYLE="margin-bottom: 0cm"><font size="2" face="Arial, Helvetica, sans-serif">&copy; kath.de</font></P></td>
              <td valign="top"><script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
google_ad_width = 160;
google_ad_height = 600;
google_ad_format = "160x600_as";
google_ad_type = "text_image";
//2007-09-05: Bildung und Erziehung
google_ad_channel = "3955521420";
google_ui_features = "rc:6";
//-->
          </script>
                  <script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
            </script>
              </td>
            </tr>
          </table>            <p class=MsoNormal><br>
            <BR>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td align="left" valign="top"><table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10">
            <?php include("az.html"); ?>
          </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</body>
</html>
