<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Gebetsanliegen</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="kaltefleiter.css" title="fonts">
</head>
<body bgcolor="#ffffff" leftmargin="6" topmargin="6" marginwidth="6" marginheight="6">
<table width="100%" border="0" cellspacing="0" cellpadding="6">
  <tr>
    <td width="100" align="left" valign="top"><table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b>Religi&ouml;ses Leben</b></td>
         <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td>
            <?php include("logo.html"); ?>
          </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
    <td rowspan="2" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><h1>
                   <font face="Arial, Helvetica, sans-serif">Gebetsanliegen
                   </font></h1>
          </td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="L12"><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td valign="top" class="L12"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td valign="top" class="L12"><strong><font face="Arial, Helvetica, sans-serif">Anliegen
                        im Gebet vor Gott bringen</font></strong>
                    <p class=MsoNormal style="margin-bottom: 0cm"><FONT COLOR="#000000" face="Arial, Helvetica, sans-serif">Gebetsanliegen
                        bzw. Gebetsmeinungen nennt man die Sorgen und N&ouml;te,
                        W&uuml;nsche und Hoffnungen, aber auch Danksagungen,
                        die im individuellen oder gemeinschaftlichen Gebet in
                        der Liturgie, z.B. bei den F&uuml;rbitten in der in der
                        Messe, bei den Laudes oder der Vesper, oder au&szlig;erhalb
                        des Gottesdienstes vor Gott getragen werden. Im engeren
                        Sinn sind zumeist Anliegen gemeint, die anderen Personen
                        oder einer Gemeinschaft zur F&uuml;rbitte anvertraut
                        werden. </FONT>
                    <P STYLE="margin-bottom: 0cm"><font face="Arial, Helvetica, sans-serif"><strong>Wirkung
                          des f&uuml;rbittenden Gebets</strong><br>
  Bittgebet und F&uuml;rbitte f&uuml;r andere geh&ouml;ren von Anfang an zur
  christlichen Gebetstradition und sind biblisch bezeugt (vgl. vor allem Mt 7,7-11
  und Jak 5,13-18). Das christliche F&uuml;rbittgebet wird im Vertrauen auf Gott
  gesprochen: Die Welt und das Schicksal des Einzelnen liegen in Gottes Hand,
  und Gott wird sich dem gerechten Anliegen eines Beters nicht verschlie&szlig;en
  (vgl. Mt 7,11). Jedes wirklich christliche Gebet erkennt die Souver&auml;nit&auml;t
  und Freiheit Gottes an, so wie es in der Vaterunser-Bitte &quot;Dein Wille
  geschehe&quot; ausgedr&uuml;ckt ist. Die Idee eines gleichsam magischen &quot;Erf&uuml;llungsautomatismus&quot; entspricht
  nicht dem christlichen Gottes- und Menschenbild. Dennoch glauben Christen an
  die objektive Wirksamkeit des Gebetes und nehmen nicht blo&szlig; eine stabilisierende
  psychologische Wirkung beim Beter an. <br>
  Der vertrauensvoll bittende Anruf Gottes ersetzt nicht das menschliche Handeln
  - weder die eigene Anstrengung noch den t&auml;tigen Einsatz f&uuml;r den N&auml;chsten.
  Das Gebet erg&auml;nzt und begleitet dieses Handeln vielmehr. Das f&uuml;rbittende
  Gebet ist daher als konkreter Akt christlicher Solidarit&auml;t zu verstehen.
  Zugleich ist es eine Absage gegen jede Ideologie der &quot;Machbarkeit&quot;,
  denn wer betet, geht davon aus, da&szlig; nicht alles in seiner Hand liegt.</font></P>
                    <P STYLE="margin-bottom: 0cm"><font face="Arial, Helvetica, sans-serif"><strong>Wem
                          werden die Gebetsanliegen anvertraut?<br>
                      </strong></font><font color="#000000" face="Arial, Helvetica, sans-serif">In
                      der Regel wird jedes f&uuml;rbittende Gebet als hilfreich
                      empfunden. Doch gem&auml;&szlig; der biblischen Aussage &quot;viel
                      vermag das inst&auml;ndige Gebet des Gerechten&quot; (Jak
                      5,16) werden Gebetsanliegen vorzugsweise Personen anvertraut,
                      die im Glauben verwurzelt sind und aus der Erfahrung der
                      N&auml;he Gottes leben. Daraus erkl&auml;rt sich der Brauch,
                      eigene Gebetsanliegen den Ordensgemeinschaften, vor allem
                      kontemplativen Kl&ouml;stern, anzuvertrauen, also jenen
                      Frauen und M&auml;nnern, die aus dem Gebet heraus ganz
                      auf Gott hin leben. In vielen Kl&ouml;stern liegt ein &quot;F&uuml;rbittbuch&quot; aus
                      oder ist ein besonderer Briefkasten f&uuml;r Gebetsanliegen
                      installiert. Zunehmend bieten Kl&ouml;ster auch die M&ouml;glichkeit
                      an, Gebetsanliegen online einzutragen (in der <a href="http://www.erzbistum-freiburg.de/66.0.html">Erzdi&ouml;zese
                      Freiburg</a></font></P>
                    <P STYLE="margin-bottom: 0cm"><FONT COLOR="#000000" face="Arial, Helvetica, sans-serif">Die
                        katholische Tradition kennt nicht nur das das f&uuml;rbittende
                        Gebet der lebenden Mitchristen, sondern auch der Heiligen
                        und M&auml;rtyrer sowie insbesondere der seligen Jungfrau
                        und Gottesmutter Maria, die ihrerseits um F&uuml;rbitte
                        und F&uuml;rsprache bei Gott angerufen werden. </FONT></P>
                    <P STYLE="margin-bottom: 0cm"><strong><font face="Arial, Helvetica, sans-serif">F&uuml;rbittendes
                          Gebet in anderen Religionen<br>
                      </font></strong><FONT COLOR="#000000" face="Arial, Helvetica, sans-serif">Da&szlig; das
                      Beten und auch das f&uuml;rbittende Gebet zum Menschen
                      geh&ouml;rt zeigen die Gebetsf&auml;hnchen der Tibeter
                      oder das Gebet der Juden an der Klagemauer des Tempelbergs
                      in Jerusalem. Sie stecken Zettel mit ihrem Gebetsanliegen
                      zwischen die Steine.</FONT></P>
                    <P STYLE="margin-bottom: 0cm">&nbsp;</P>
                    <P STYLE="margin-bottom: 0cm"><font color="#000000" face="Arial, Helvetica, sans-serif"><a href="http://www.erzbistum-freiburg.de">Dr.
                      Norbert Kebekus</a></font></P></td>
                  <td valign="top"><p>&nbsp;</p>
                    </td>
                </tr>
              </table>                <P STYLE="margin-bottom: 0cm">&nbsp;</P></td>
              <td valign="top"><p><a href="http://www.update-seele.de" target="_blank"><img src="banner-update-seele-02.jpg" width="286" height="70" border="0"></a>
                </p>
                <p>
                    <script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
google_ad_width = 160;
google_ad_height = 600;
google_ad_format = "160x600_as";
google_ad_type = "text_image";
//2007-09-05: Bildung und Erziehung
google_ad_channel = "3955521420";
google_ui_features = "rc:6";
//-->
                    </script>
                    <script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
                          </script>
                </p></td>
            </tr>
          </table>            <P STYLE="margin-bottom: 0cm">&nbsp;</P>            
            <P STYLE="margin-bottom: 0cm">&nbsp;</P>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td align="left" valign="top"><table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10">
            <?php include("az.html"); ?>
          </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</body>
</html>
