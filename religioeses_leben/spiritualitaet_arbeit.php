<HTML><HEAD><TITLE>Spiritualit&auml;t und Arbeit</TITLE>
<META http-equiv=Content-Type content="text/html; charset=iso-8859-1"><LINK
title=fonts href="../philosophie_theologie/kaltefleiter.css" type=text/css
rel=stylesheet>
<META content="MSHTML 5.50.4134.600" name=GENERATOR></HEAD>
<BODY bgColor=#ffffff leftMargin=6 topMargin=6 marginheight="6" marginwidth="6">
<TABLE cellSpacing=0 cellPadding=6 width="100%" border=0>
  <TBODY>
  <TR>
    <TD vAlign=top align=left width=100>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left">
          <td width="8"><img src="../philosophie_theologie/boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="../philosophie_theologie/boxtop.gif"><img src="../philosophie_theologie/boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="../philosophie_theologie/boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="../philosophie_theologie/boxtopleft.gif"><img src="../philosophie_theologie/boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b>Religi&ouml;ses Leben</b></td>
          <td background="../philosophie_theologie/boxtopright.gif"><img src="../philosophie_theologie/boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="../philosophie_theologie/boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="../philosophie_theologie/boxdivider.gif"><img src="../philosophie_theologie/boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="../philosophie_theologie/boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="../philosophie_theologie/boxleft.gif"><img src="../philosophie_theologie/boxleft.gif" width="8" height="8" alt=""></td>
          <td> <?php include("logo.html"); ?> </td>
          <td background="../philosophie_theologie/boxright.gif"><img src="../philosophie_theologie/boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="../philosophie_theologie/boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="../philosophie_theologie/boxbottom.gif"><img src="../philosophie_theologie/boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="../philosophie_theologie/boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left">
          <td width="8"><img src="../philosophie_theologie/boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="../philosophie_theologie/boxtop.gif"><img src="../philosophie_theologie/boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="../philosophie_theologie/boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="../philosophie_theologie/boxtopleft.gif"><img src="../philosophie_theologie/boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="../philosophie_theologie/boxtopright.gif"><img src="../philosophie_theologie/boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="../philosophie_theologie/boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="../philosophie_theologie/boxdivider.gif"><img src="../philosophie_theologie/boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="../philosophie_theologie/boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="../philosophie_theologie/boxleft.gif"><img src="../philosophie_theologie/boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10"> <?php include("az.html"); ?> </td>
          <td background="../philosophie_theologie/boxright.gif"><img src="../philosophie_theologie/boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="../philosophie_theologie/boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="../philosophie_theologie/boxbottom.gif"><img src="../philosophie_theologie/boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="../philosophie_theologie/boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </TD>
    <TD vAlign=top rowSpan=2>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
        <TBODY>
        <TR vAlign=top align=left>
          <TD width=8><IMG height=8 alt=""
            src="../philosophie_theologie/boxtopleftcorner.gif" width=8></TD>
          <TD background=../philosophie_theologie/boxtop.gif><IMG height=8 alt=""
            src="../philosophie_theologie/boxtop.gif" width=8></TD>
          <TD width=8><IMG height=8 alt=""
            src="../philosophie_theologie/boxtoprightcorner.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=../philosophie_theologie/boxtopleft.gif><IMG height=8 alt=""
            src="../philosophie_theologie/boxtopleft.gif" width=8></TD>
          <TD bgColor=#e2e2e2>
            <H1><font face="Arial, Helvetica, sans-serif">Spiritualit&auml;t
                und Arbeit</font></H1>
          </TD>
          <TD background=../philosophie_theologie/boxtopright.gif><IMG height=8
            alt="" src="../philosophie_theologie/boxtopright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=13 alt="" src="../philosophie_theologie/boxdividerleft.gif"
            width=8></TD>
          <TD background=../philosophie_theologie/boxdivider.gif><IMG height=13
            alt="" src="../philosophie_theologie/boxdivider.gif" width=8></TD>
          <TD><IMG height=13 alt=""
            src="../philosophie_theologie/boxdividerright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=../philosophie_theologie/boxleft.gif><IMG height=8 alt=""
            src="../philosophie_theologie/boxleft.gif" width=8></TD>
          <TD class=L12>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td valign="top" class="L12"><P><STRONG><font face="Arial, Helvetica, sans-serif">Spirtualit&auml;t
                        ist mehr als Mu&szlig;e</font></STRONG></P>
                  <P><font face="Arial, Helvetica, sans-serif">Spiritualit&auml;t
                      meint zuerst einen gl&auml;ubigen Umgang mit der Wirklichkeit.
                      Wenn heute Spiritualit&auml;t als Aufforderung verstanden
                      wird, sich zur&uuml;ckzuziehen und den Kontakt zur Umwelt
                      zu unterbrechen, war die christliche Tradition nie so sehr
                      davon gepr&auml;gt, gen&uuml;gend Ruhe und Mu&szlig;e f&uuml;r
                      ein spirituelles Leben zu fordern. Gerade in den ersten
                      12 Jahrhunderten begegnen kaum bestimmte Meditationstechniken,
                      Gebetsanleitungen oder gar Entspannungs&uuml;bungen. Vielmehr
                      geht es darum, gen&uuml;gend zu arbeiten, und eben dort
                      einen Zugang zu Gott zu finden. Die zentrale spirituelle
                      Praxis ist das Gebet der Psalmen und die Lekt&uuml;re der
                      Bibel. Ehe im Mittelalter die theologischen Themen systematisch
                      dargestellt wurden, bestand Theologie vor allem in der
                      Kommentierung der biblischen B&uuml;cher. In der christlichen
                      Spiritualit&auml;t hat die Arbeit einen besonderen Stellenwert,
                      denn im Altertum arbeiteten die freien B&uuml;rger m&ouml;glichst
                      nicht, sondern die Sklaven und Tagel&ouml;hner. Mu&szlig;e
                      war das Privileg der Wohlhabenden, die oft Sklaven f&uuml;r
                      die k&ouml;rperlichen Arbeiten hatten. Da Jesus selbst
                      als Zimmermann gearbeitet hatte, viele Apostel Fischer
                      waren und Paulus als Zeltmacher seinen Lebensunterhalt
                      verdiente, erhielt die Arbeit einen neuen Stellenwert.
                      Pr&auml;gend f&uuml;r die abendl&auml;ndische Kultur wurde
                      die benediktinische M&ouml;nchstradition mit ihrem Anspruch &#8222;ora
                      et labora&#8220; &#8211; bete und arbeite. Nicht im M&uuml;&szlig;iggang,
                      sondern in der t&auml;glichen Arbeit w&auml;chst die Verbundenheit
                      mir Gott. So ist Arbeit und Gebet kein Widerspruch, vielmehr
                      soll Arbeit selbst als Gottesdienst verstanden werden. </font></P>
                  <p><font face="Arial, Helvetica, sans-serif">Zitate:<br>
  Alfred Delp SJ: &#8222;Die Welt ist Gottes so voll. Aus allen Poren der Dinge
  quillt es gleichsam uns entgegen. [...] In allem will Gott Begegnung feiern
  und fragt und will die anbetende, hingebende Antwort.&#8220; </font></p>
                  <p><font face="Arial, Helvetica, sans-serif">Bernhard von Clairvaux: &#8222;Ich
                      habe es erfahren, glaube es mir: In den W&auml;ldern findest
                      du mehr als in den B&uuml;chern. Holz und Steine werden
                      dich &uuml;ber Dinge belehren, von denen du bei den Lehrern
                      nichts h&ouml;ren kannst&#8220;</font></p>
                  <p><font face="Arial, Helvetica, sans-serif">Aus der Regel
                      von Taiz&eacute;: &#8222;Damit dein Gebet wahrhaftig sei,
                      mu&szlig;t du in harter Arbeit stehen. Begn&uuml;gtest
                      du dich mit dilettantischer L&auml;ssigkeit, so w&auml;rest
                      du unf&auml;hig, wirklich F&uuml;rbitte zu tun. Dein Gebet
                      findet zur Ganzheit, wenn es eins ist mit deiner Arbeit&#8220;</font></p>
                  <p><font face="Arial, Helvetica, sans-serif">J&uuml;rgen Pelzer</font></p>
                  <p><br>
                  </p>
                  <P>&copy;<a href="http://www.kath.de"> <font face="Arial, Helvetica, sans-serif">www.kath.de</font></a></P></td>
                <td valign="top"><p><a href="http://www.update-seele.de" target="_blank"><img src="banner-update-seele-02.jpg" width="286" height="70" border="0"></a>
                <p><a href="http://www.menschen-fuehren.de/" target="_blank"><img src="fuehrungsseminare.jpg" width="200" height="185" border="0"></a>
                  </p>
                  <p>
                        <script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
google_ad_width = 160;
google_ad_height = 600;
google_ad_format = "160x600_as";
google_ad_type = "text_image";
//2007-09-05: Bildung und Erziehung
google_ad_channel = "3955521420";
google_ui_features = "rc:6";
//-->
                      </script>
                        <script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
                            </script>
                  </p></td>
              </tr>
            </table>
            <P>&nbsp;</P>
            </TD>
          <TD background=../philosophie_theologie/boxright.gif><IMG height=8 alt=""
            src="../philosophie_theologie/boxright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=8 alt="" src="../philosophie_theologie/boxbottomleft.gif"
            width=8></TD>
          <TD background=../philosophie_theologie/boxbottom.gif><IMG height=8 alt=""
            src="../philosophie_theologie/boxbottom.gif" width=8></TD>
          <TD><IMG height=8 alt="" src="../philosophie_theologie/boxbottomright.gif"
            width=8></TD></TR></TBODY></TABLE></TD></TR>
  <TR>
    <TD vAlign=top align=left>&nbsp; </TD>
  </TR></TBODY></TABLE></BODY></HTML>