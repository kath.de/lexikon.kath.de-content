<HTML><HEAD><TITLE>Freiheit - Gelingen des Lebens</TITLE>
<META http-equiv=Content-Type content="text/html; charset=iso-8859-1"><LINK 
title=fonts href="../philosophie_theologie/kaltefleiter.css" type=text/css 
rel=stylesheet>
<META content="MSHTML 5.50.4134.600" name=GENERATOR></HEAD>
<BODY bgColor=#ffffff leftMargin=6 topMargin=6 marginheight="6" marginwidth="6">
<TABLE cellSpacing=0 cellPadding=6 width="100%" border=0>
  <TBODY>
  <TR>
    <TD vAlign=top align=left width=100> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="../philosophie_theologie/boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="../philosophie_theologie/boxtop.gif"><img src="../philosophie_theologie/boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="../philosophie_theologie/boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="../philosophie_theologie/boxtopleft.gif"><img src="../philosophie_theologie/boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b>Religi&ouml;ses Leben</b></td>
          <td background="../philosophie_theologie/boxtopright.gif"><img src="../philosophie_theologie/boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="../philosophie_theologie/boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="../philosophie_theologie/boxdivider.gif"><img src="../philosophie_theologie/boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="../philosophie_theologie/boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="../philosophie_theologie/boxleft.gif"><img src="../philosophie_theologie/boxleft.gif" width="8" height="8" alt=""></td>
          <td> <?php include("logo.html"); ?> </td>
          <td background="../philosophie_theologie/boxright.gif"><img src="../philosophie_theologie/boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="../philosophie_theologie/boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="../philosophie_theologie/boxbottom.gif"><img src="../philosophie_theologie/boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="../philosophie_theologie/boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="../philosophie_theologie/boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="../philosophie_theologie/boxtop.gif"><img src="../philosophie_theologie/boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="../philosophie_theologie/boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="../philosophie_theologie/boxtopleft.gif"><img src="../philosophie_theologie/boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="../philosophie_theologie/boxtopright.gif"><img src="../philosophie_theologie/boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="../philosophie_theologie/boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="../philosophie_theologie/boxdivider.gif"><img src="../philosophie_theologie/boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="../philosophie_theologie/boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="../philosophie_theologie/boxleft.gif"><img src="../philosophie_theologie/boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10"> <?php include("az.html"); ?> </td>
          <td background="../philosophie_theologie/boxright.gif"><img src="../philosophie_theologie/boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="../philosophie_theologie/boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="../philosophie_theologie/boxbottom.gif"><img src="../philosophie_theologie/boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="../philosophie_theologie/boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </TD>
    <TD vAlign=top rowSpan=2>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
        <TBODY>
        <TR vAlign=top align=left>
          <TD width=8><IMG height=8 alt="" 
            src="../philosophie_theologie/boxtopleftcorner.gif" width=8></TD>
          <TD background=../philosophie_theologie/boxtop.gif><IMG height=8 alt="" 
            src="../philosophie_theologie/boxtop.gif" width=8></TD>
          <TD width=8><IMG height=8 alt="" 
            src="../philosophie_theologie/boxtoprightcorner.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=../philosophie_theologie/boxtopleft.gif><IMG height=8 alt="" 
            src="../philosophie_theologie/boxtopleft.gif" width=8></TD>
          <TD bgColor=#e2e2e2> 
            <H1><font face="Arial, Helvetica, sans-serif">Freiheit und Gelingen
                des Lebens</font></H1>
          </TD>
          <TD background=../philosophie_theologie/boxtopright.gif><IMG height=8 
            alt="" src="../philosophie_theologie/boxtopright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=13 alt="" src="../philosophie_theologie/boxdividerleft.gif" 
            width=8></TD>
          <TD background=../philosophie_theologie/boxdivider.gif><IMG height=13 
            alt="" src="../philosophie_theologie/boxdivider.gif" width=8></TD>
          <TD><IMG height=13 alt="" 
            src="../philosophie_theologie/boxdividerright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=../philosophie_theologie/boxleft.gif><IMG height=8 alt="" 
            src="../philosophie_theologie/boxleft.gif" width=8></TD>
          <TD class=L12>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td valign="top" class="L12"><P><STRONG><font face="Arial, Helvetica, sans-serif">Von
                        unserer Freiheit h&auml;ngt es ab, wie unser Leben gelingt</font></STRONG></P>
                  <p><font face="Arial, Helvetica, sans-serif">Mit den wichtigen
                      Entscheidungen gibt die Freiheit dem Leben die gewollte
                      Richtung. Da die Freiheit nicht im Hafen bleibt, wenn das
                      Lebensschiff auf seine Reise geht, sondern mitf&auml;hrt,
                      ist sie immer dabei, wenn die Konsequenzen einer Entscheidung
                      eintreten. Deshalb ist die Freiheit nicht nur daran interessiert,
                      da&szlig; die wichtigen Lebensentscheidungen von ihr und
                      nicht von anderen getroffen werden, sie ist auch am Gelingen
                      dessen interessiert, was mit der Entscheidung angezielt
                      wurde. Denn mit einer <a href="http://www.kath.de/lexikon/philosophie_theologie/freiheit_entscheidung.php">Entscheidung</a> will
                      der einzelne etwas erreichen &#8211; Erfolg, Gl&uuml;ck,
                      sich selbst verwirklichen. Die lebensbestimmenden Entscheidungen
                      beziehen sich auch auf andere &#8211; die eigenen Kinder
                      sollen im Leben gut zurecht kommen, Freunde finden. Wer
                      Lehrer wird, verfolgt das Ziel, anderen Wissen zu vermitteln,
                      der Arzt will heilen, der Prediger seine H&ouml;rer von
                      der christlichen, muslimischen oder einer anderen Botschaft &uuml;berzeugen.
                      Der Ingenieur will eine Br&uuml;cke bauen, der Pilot ein
                      Flugzeug fliegen. All das, was durch Entscheidungen angesteuert
                      wird, kann gelingen, kann aber auch nicht erreicht, durch
                      mangelnden Einsatz nicht verwirklicht oder durch andere
                      hintertrieben, vereitelt werden. Wer in einem Wettkampf
                      antritt, mu&szlig; damit rechnen, da&szlig; er nicht auf
                      dem Siegertreppchen steht. Es wird deutlich, da&szlig; die
                      Freiheit den Weg zum Gelingen bestimmen kann, das Gelingen
                      selbst jedoch auch von anderen Faktoren abh&auml;ngt, nicht
                      zuletzt davon, ob der einzelne auch alles daran setzt,
                      was er mit einer <a href="http://www.kath.de/lexikon/philosophie_theologie/freiheit.php">Entscheidung</a> sich
                      vorgenommen hat. </font></p>
                  <p><font face="Arial, Helvetica, sans-serif">Eckhard Bieger
                      S.J.</font></p>
                  <p><font face="Arial, Helvetica, sans-serif"> <a href="http://www.kathshop.de/wwwkathde/product_info.php?cPath=&products_id=195&anbieter=17&page=&">Buchtipp</a></font><br>
                  </p>
                  <P>&copy;<a href="http://www.kath.de"> <font face="Arial, Helvetica, sans-serif">www.kath.de</font></a></P></td>
                <td valign="top"><p><a href="http://www.update-seele.de" target="_blank"><img src="banner-update-seele-02.jpg" width="286" height="70" border="0"></a>
                  </p>
                  <p>
                        <script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
google_ad_width = 160;
google_ad_height = 600;
google_ad_format = "160x600_as";
google_ad_type = "text_image";
//2007-09-05: Bildung und Erziehung
google_ad_channel = "3955521420";
google_ui_features = "rc:6";
//-->
                      </script>
                        <script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
                            </script>
                  </p></td>
              </tr>
            </table>
            <P>&nbsp;</P>
            </TD>
          <TD background=../philosophie_theologie/boxright.gif><IMG height=8 alt="" 
            src="../philosophie_theologie/boxright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=8 alt="" src="../philosophie_theologie/boxbottomleft.gif" 
            width=8></TD>
          <TD background=../philosophie_theologie/boxbottom.gif><IMG height=8 alt="" 
            src="../philosophie_theologie/boxbottom.gif" width=8></TD>
          <TD><IMG height=8 alt="" src="../philosophie_theologie/boxbottomright.gif" 
            width=8></TD></TR></TBODY></TABLE></TD></TR>
  <TR>
    <TD vAlign=top align=left>&nbsp; </TD>
  </TR></TBODY></TABLE></BODY></HTML>
