<HTML><HEAD><TITLE>F&uuml;rbittgebet</TITLE>
<meta name="title" content="Religi&ouml;ses Leben">
<meta name="author" content="Redaktion kath.de">
<meta name="publisher" content="kath.de">
<meta name="copyright" content="kath.de">
<meta name="description" content="">
<meta name="abstract" content="Lexikon &uuml;ber Religi&ouml;ses Leben">
<meta http-equiv="content-language" content="de">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="date" content="2006-00-01">
<meta name="robots" content="index,follow">
<meta name="revisit-after" content="10 days">
<meta name="revisit" content="after 10 days">
<meta name="DC.Title" content="Religi&ouml;ses Leben">
<meta name="DC.Creator" content="Redaktion kath.de">
<meta name="DC.Contributor" content="J�rgen Pelzer">
<meta name="DC.Rights" content="kath.de">
<meta name="DC.Publisher" content="kath.de">
<meta name="DC.Date" content="2006-00-01">
<meta name="DC.Description" content="Lexikon &uuml;ber Religi&ouml;ses Leben">
<meta name="DC.Language" content="de">
<meta name="DC.Type" content="Text">
<meta name="DC.Format" content="text/html">
<meta name="DC.Identifier" content="http://www.kath.de/lexikon/religioeses_leben/">
<meta name="keywords" lang="de" content="Eucharistiefeier, Messbuch, II. Vatikanisches Konzil, gemeinsames Priestertum">
</HEAD>
<BODY bgColor=#ffffff leftMargin=6 topMargin=6 marginheight="6" marginwidth="6">
<TABLE cellSpacing=0 cellPadding=6 width="100%" border=0>
  <TBODY>
  <TR>
    <TD vAlign=top align=left width=100> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="../philosophie_theologie/boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="../philosophie_theologie/boxtop.gif"><img src="../philosophie_theologie/boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="../philosophie_theologie/boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="../philosophie_theologie/boxtopleft.gif"><img src="../philosophie_theologie/boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b>Religi&ouml;ses Leben</b></td>
          <td background="../philosophie_theologie/boxtopright.gif"><img src="../philosophie_theologie/boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="../philosophie_theologie/boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="../philosophie_theologie/boxdivider.gif"><img src="../philosophie_theologie/boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="../philosophie_theologie/boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="../philosophie_theologie/boxleft.gif"><img src="../philosophie_theologie/boxleft.gif" width="8" height="8" alt=""></td>
          <td> <?php include("logo.html"); ?> </td>
          <td background="../philosophie_theologie/boxright.gif"><img src="../philosophie_theologie/boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="../philosophie_theologie/boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="../philosophie_theologie/boxbottom.gif"><img src="../philosophie_theologie/boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="../philosophie_theologie/boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="../philosophie_theologie/boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="../philosophie_theologie/boxtop.gif"><img src="../philosophie_theologie/boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="../philosophie_theologie/boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="../philosophie_theologie/boxtopleft.gif"><img src="../philosophie_theologie/boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="../philosophie_theologie/boxtopright.gif"><img src="../philosophie_theologie/boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="../philosophie_theologie/boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="../philosophie_theologie/boxdivider.gif"><img src="../philosophie_theologie/boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="../philosophie_theologie/boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="../philosophie_theologie/boxleft.gif"><img src="../philosophie_theologie/boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10"> <?php include("az.html"); ?> </td>
          <td background="../philosophie_theologie/boxright.gif"><img src="../philosophie_theologie/boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="../philosophie_theologie/boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="../philosophie_theologie/boxbottom.gif"><img src="../philosophie_theologie/boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="../philosophie_theologie/boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </TD>
    <TD vAlign=top rowSpan=2>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
        <TBODY>
        <TR vAlign=top align=left>
          <TD width=8><IMG height=8 alt="" 
            src="../philosophie_theologie/boxtopleftcorner.gif" width=8></TD>
          <TD background=../philosophie_theologie/boxtop.gif><IMG height=8 alt="" 
            src="../philosophie_theologie/boxtop.gif" width=8></TD>
          <TD width=8><IMG height=8 alt="" 
            src="../philosophie_theologie/boxtoprightcorner.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=../philosophie_theologie/boxtopleft.gif><IMG height=8 alt="" 
            src="../philosophie_theologie/boxtopleft.gif" width=8></TD>
          <TD bgColor=#e2e2e2> 
            <H1><font face="Arial, Helvetica, sans-serif">F&uuml;rbittgebet</font></H1>
          </TD>
          <TD background=../philosophie_theologie/boxtopright.gif><IMG height=8 
            alt="" src="../philosophie_theologie/boxtopright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=13 alt="" src="../philosophie_theologie/boxdividerleft.gif" 
            width=8></TD>
          <TD background=../philosophie_theologie/boxdivider.gif><IMG height=13 
            alt="" src="../philosophie_theologie/boxdivider.gif" width=8></TD>
          <TD><IMG height=13 alt="" 
            src="../philosophie_theologie/boxdividerright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=../philosophie_theologie/boxleft.gif><IMG height=8 alt="" 
            src="../philosophie_theologie/boxleft.gif" width=8></TD>
          <TD class=L12>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td valign="top" class="L12"><P><STRONG><font face="Arial, Helvetica, sans-serif">Teil
                        der Eucharistiefeier</font></STRONG></P>
                  <P><font face="Arial, Helvetica, sans-serif">Nach dem Vorbild
                      und dem Auftrag des Herrn ist der Christ gerufen, sich
                      die Not aller Menschen zu Herzen zu nehmen und zu helfen,
                      so weit er kann. Vordringlich soll das durch die F&uuml;rbitte
                      geschehen. Darin liegt die Chance, die Sorgen und Anliegen
                      der Menschen im Gro&szlig;en wie im Kleinen vor Gott zu
                      bringen. Der entscheidende Impuls, das F&uuml;rbittgebet
                      wieder zu erneuern und in die Messfeier aufzunehmen, ist
                      vom II. Vatikanischen Konzil ausgegangen. Seitdem ist es
                      fester Bestandteil jeder Eucharistiefeier.</font></P>
                  <p><font face="Arial, Helvetica, sans-serif"><strong>Vom Sinn
                        der F&uuml;rbitten</strong><br>
  In den ersten christlichen Jahrhunderten wurde das F&uuml;rbittgebet als &#8222;das
  Gebet der Gl&auml;ubigen&#8220; schlechthin bezeichnet. Von allen Volksgebeten
  bei der Messfeier war das F&uuml;rbittgebet am bedeutsamsten. Die im Namen
  Jesu versammelte Gemeinde versteht sich als das lebendige, sichtbare Zeichen
  des Herrn. So wie Jesus, der erh&ouml;hte Herr, unabl&auml;ssig f&uuml;r uns
  beim Vater eintritt (vgl. R&ouml;m 8,27), so findet dieses Eintreten des Sohnes
  beim Vater im f&uuml;rbittenden Gebet der Kirche sichtbaren Ausdruck. Wenn
  darum der Priester zusammen mit der Gemeinde f&uuml;rbittend f&uuml;r alle
  Menschen vor Gott hintritt, dann tritt damit eigentlich Christus selbst vor
  den Vater. Aber auch der Vater seinerseits erblickt in der betenden Gemeinde
  seinen eigenen Sohn wieder. </font></p>
                  <p><font face="Arial, Helvetica, sans-serif">Von daher wird
                      die grundlegende Aussage &uuml;ber das F&uuml;rbittgebet
                      verst&auml;ndlich: &#8222;In den F&uuml;rbitten &uuml;bt
                      die Gemeinde durch ihr Beten f&uuml;r alle Menschen ihr
                      priesterliches Amt aus&#8220; (Einf&uuml;hrung in das Messbuch).
                      So hei&szlig;t &#8222;F&uuml;rbitten&#8220; nichts anderes
                      als: die Berufung aller Getauften zum gemeinsamen Priestertum
                      in die Tat umsetzen.</font></p>
                  <p><font face="Arial, Helvetica, sans-serif"><strong>Kennzeichen
                        des F&uuml;rbittgebetes</strong><br>
  Gerade im F&uuml;rbittgebet muss sich der Horizont der Gl&auml;ubigen auf die
  Welt und die Menschheit hin weiten. Hier muss sich gewisserma&szlig;en ein &#8222;Fenster
  in die Welt&#8220; auftun, denn f&uuml;r diese Welt tr&auml;gt der Christ Verantwortung
  vor Gott. Entsprechend soll das F&uuml;rbittgebet von zwei Kennzeichen gepr&auml;gt
  sein: von der Universalit&auml;t und von der Aktualit&auml;t. Es gibt kein
  Anliegen der Welt und der Menschheit, das in das Gebet vor Gott nicht eingebracht
  werden k&ouml;nnte und sollte. Immer wieder stehen wir in der Gefahr, &uuml;ber
  den eigenen Anliegen die ganze Vielfalt menschlicher Bedr&auml;ngnisse und
  N&ouml;te zu vergessen.<br>
  Neben der Universalit&auml;t werden die F&uuml;rbitten aber auch von der konkreten
  gegenw&auml;rtigen Situation bestimmt sein m&uuml;ssen. Dabei geht es nicht
  um ein Modernsein um jeden Preis. Vielmehr spiegelt das aktuelle F&uuml;rbittgebet
  die Verantwortung des Christen wieder im Blick auf das, was hier und jetzt
  die Welt bedr&auml;ngt und bedr&uuml;ckt. Was in der Welt geschieht, ist darum
  auch die wichtigste &#8222;Fundstelle&#8220; des F&uuml;rbittgebetes.<br>
  Die beiden Kennzeichen Universalit&auml;t und Aktualit&auml;t f&uuml;r das
  F&uuml;rbittgebet ergeben sich aus der entscheidenden biblischen Stelle im
  1. Brief des Apostel Paulus an Timotheus: &#8222;Als erstes fordere ich zu
  Bitten und Gebeten auf, zu F&uuml;rbitten und Danksagungen f&uuml;r alle Menschen:
  f&uuml;r die Herrscher und f&uuml;r alle, die Macht haben, damit wir in aller
  Fr&ouml;mmigkeit und Rechtschaffenheit ungest&ouml;rt und ruhig leben k&ouml;nnen&quot; (1
  Tim 2,1f).<br>
  Entsprechend dieser Weisung des Apostels Paulus soll das F&uuml;rbittgebet
  in der Eucharistie gestaltet werden. In der Regel sollen die <a href="gebetsanliegen.php">Anliegen</a> der
  Kirche ebenso wie die Bitten f&uuml;r alle, die in Politik und Gesellschaft
  Verantwortung tragen, laut werden; nicht weniger die Bitte f&uuml;r alle von
  verschiedener Not Bedr&uuml;ckten und f&uuml;r die Ortsgemeinde selbst, ja
  auch f&uuml;r die, die nicht mehr glauben und beten k&ouml;nnen. Es ist so,
  wie es Martin Buber einmal gesagt hat: &#8222;Von der Not eines jeden Menschen
  bleibt eine Spur in meinem Herzen eingeritzt. In der Stunde des Gebetes &ouml;ffne
  ich meine Herz und sage: &quot;Herr der Welt, sieh an, was hier geschrieben
  steht&#8217;&#8220;.</font></p>
                  <p><font face="Arial, Helvetica, sans-serif"><br>
  Dr. Klaus Stadel<br>
  Domkapitular und Liturgiereferent der Erzdi&ouml;zese Freiburg</font></p>
                  <p><font face="Arial, Helvetica, sans-serif"><a href="http://www.erzbistum-freiburg.de"> &copy; Erzdi&ouml;zese
                    Freiburg</a> </font></p></td>
                <td valign="top"><script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
google_ad_width = 160;
google_ad_height = 600;
google_ad_format = "160x600_as";
google_ad_type = "text_image";
//2007-09-05: Bildung und Erziehung
google_ad_channel = "3955521420";
google_ui_features = "rc:6";
//-->
            </script>
                    <script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
              </script>
                </td>
              </tr>
            </table>
            <P><font face="Arial, Helvetica, sans-serif"><br>
            </font> </P>
            <P>&nbsp;</P>
            </TD>
          <TD background=../philosophie_theologie/boxright.gif><IMG height=8 alt="" 
            src="../philosophie_theologie/boxright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=8 alt="" src="../philosophie_theologie/boxbottomleft.gif" 
            width=8></TD>
          <TD background=../philosophie_theologie/boxbottom.gif><IMG height=8 alt="" 
            src="../philosophie_theologie/boxbottom.gif" width=8></TD>
          <TD><IMG height=8 alt="" src="../philosophie_theologie/boxbottomright.gif" 
            width=8></TD></TR></TBODY></TABLE></TD></TR>
  <TR>
    <TD vAlign=top align=left>&nbsp; </TD>
  </TR></TBODY></TABLE></BODY></HTML>
