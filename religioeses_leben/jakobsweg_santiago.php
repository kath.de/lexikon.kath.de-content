<HTML><HEAD><TITLE>Jakobsweg - Santiago</TITLE>
<META http-equiv=Content-Type content="text/html; charset=iso-8859-1"><LINK 
title=fonts href="../philosophie_theologie/kaltefleiter.css" type=text/css 
rel=stylesheet>
<META content="MSHTML 5.50.4134.600" name=GENERATOR></HEAD>
<BODY bgColor=#ffffff leftMargin=6 topMargin=6 marginheight="6" marginwidth="6">
<TABLE cellSpacing=0 cellPadding=6 width="100%" border=0>
  <TBODY>
  <TR>
    <TD vAlign=top align=left width=100> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="../philosophie_theologie/boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="../philosophie_theologie/boxtop.gif"><img src="../philosophie_theologie/boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="../philosophie_theologie/boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="../philosophie_theologie/boxtopleft.gif"><img src="../philosophie_theologie/boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b>Religi&ouml;ses Leben</b></td>
          <td background="../philosophie_theologie/boxtopright.gif"><img src="../philosophie_theologie/boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="../philosophie_theologie/boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="../philosophie_theologie/boxdivider.gif"><img src="../philosophie_theologie/boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="../philosophie_theologie/boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="../philosophie_theologie/boxleft.gif"><img src="../philosophie_theologie/boxleft.gif" width="8" height="8" alt=""></td>
          <td> <?php include("logo.html"); ?> </td>
          <td background="../philosophie_theologie/boxright.gif"><img src="../philosophie_theologie/boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="../philosophie_theologie/boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="../philosophie_theologie/boxbottom.gif"><img src="../philosophie_theologie/boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="../philosophie_theologie/boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="../philosophie_theologie/boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="../philosophie_theologie/boxtop.gif"><img src="../philosophie_theologie/boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="../philosophie_theologie/boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="../philosophie_theologie/boxtopleft.gif"><img src="../philosophie_theologie/boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="../philosophie_theologie/boxtopright.gif"><img src="../philosophie_theologie/boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="../philosophie_theologie/boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="../philosophie_theologie/boxdivider.gif"><img src="../philosophie_theologie/boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="../philosophie_theologie/boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="../philosophie_theologie/boxleft.gif"><img src="../philosophie_theologie/boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10"> <?php include("az.html"); ?> </td>
          <td background="../philosophie_theologie/boxright.gif"><img src="../philosophie_theologie/boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="../philosophie_theologie/boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="../philosophie_theologie/boxbottom.gif"><img src="../philosophie_theologie/boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="../philosophie_theologie/boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </TD>
    <TD vAlign=top rowSpan=2>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
        <TBODY>
        <TR vAlign=top align=left>
          <TD width=8><IMG height=8 alt="" 
            src="../philosophie_theologie/boxtopleftcorner.gif" width=8></TD>
          <TD background=../philosophie_theologie/boxtop.gif><IMG height=8 alt="" 
            src="../philosophie_theologie/boxtop.gif" width=8></TD>
          <TD width=8><IMG height=8 alt="" 
            src="../philosophie_theologie/boxtoprightcorner.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=../philosophie_theologie/boxtopleft.gif><IMG height=8 alt="" 
            src="../philosophie_theologie/boxtopleft.gif" width=8></TD>
          <TD bgColor=#e2e2e2><H1><font face="Arial, Helvetica, sans-serif">Jakobsweg
                - Santiago
                </font></H1>
          </TD>
          <TD background=../philosophie_theologie/boxtopright.gif><IMG height=8 
            alt="" src="../philosophie_theologie/boxtopright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=13 alt="" src="../philosophie_theologie/boxdividerleft.gif" 
            width=8></TD>
          <TD background=../philosophie_theologie/boxdivider.gif><IMG height=13 
            alt="" src="../philosophie_theologie/boxdivider.gif" width=8></TD>
          <TD><IMG height=13 alt="" 
            src="../philosophie_theologie/boxdividerright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=../philosophie_theologie/boxleft.gif><IMG height=8 alt="" 
            src="../philosophie_theologie/boxleft.gif" width=8></TD>
          <TD class=L12>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td valign="top" class="L12"><P><STRONG><font face="Arial, Helvetica, sans-serif">Die
                        Wallfahrt nach Santiago de Compostella</font></STRONG><font face="Arial, Helvetica, sans-serif"><br>
                    <br>
                    </font><font face="Arial, Helvetica, sans-serif">Die gro&szlig;en
                    Verkehrswege des heutigen Europa verlaufen meist von Norden
                    nach S&uuml;den. Die Alpen&uuml;berg&auml;nge sind wie zur
                    Zeit der R&ouml;mer wichtig. Es gab aber, bevor die R&ouml;mer
                    das Gesicht Europas formten, bedeutende <a href="http://www.kath.de/quodlibe/santiago/santia10.htm">Wege
                    von West nach Ost</a>. Einer lebt in der gro&szlig;en Wallfahrt
                    nach Santiago des Compostella weiter. </font></P>
                  <P><font face="Arial, Helvetica, sans-serif">Im <a href="http://www.kath.de/quodlibe/santiago/santia05.htm">Mittelalter</a> pilgerten
                      Millionen Menschen nach Santiago de Compostella, aus religi&ouml;sem
                      Antrieb, als Bu&szlig;e, die <a href="http://www.kath.de/quodlibe/santiago/santia04.htm">Wallfahrt</a> konnte
                      anstelle einer Strafe unternommen werden &#8211; die Pilgerschaft
                      wog die Todesstrafe auf. Sicher spielte auch Abenteuerlust
                      eine Rolle. Auch f&uuml;r die Menschen heute ist der lange
                      Weg ein Sinnbild f&uuml;r das eigene Leben, sie machen
                      sich auf den Weg, um die eigene Mitte zu finden. Sie gehen
                      an die Grenze der bewohnten Welt, dorthin, wo die Sonne
                      untergeht. Die Wallfahrt schlie&szlig;t eigentlich nicht
                      in Santiago, sondern 80 km in Fisterra, Finis Terrae &#8211; Ende
                      der Welt, wo z&uuml;nftige Pilger ihre Kleider verbrennen.<br>
  Santiago de Compostella hei&szlig;t Sanct Jakobus in Campo stellae &#8211; im
  Sternenfeld. Das war die Bezeichnung f&uuml;r die Flur, in der die Reliquien
  gefunden wurden und Anla&szlig;, den Weg der Wallfahrer mit der Milchstra&szlig;e
  in Beziehung zu setzen. Viele Jakobskirchen in Europa liegen an den Pilgerwegen. <br>
  Jakobus ist der nach Stephanus der zweite M&auml;rtyrer. &#8222;Um jene Zeit
  lie&szlig; der K&ouml;nig Herodes einige aus der Gemeinde verhaften und mi&szlig;handeln.
  Jakobus, den Bruder des Johannes, lie&szlig; er mit dem Schwert hinrichten.
  Als er sah, da&szlig; es den Juden gefiel, lie&szlig; er auch Petrus festnehmen;&#8220; wird
  im 12. Kapitel der Apostelgeschichte berichtet. Petrus wird durch einen Engel
  aus dem Gef&auml;ngnis befreit. Jakobus soll nach der Legende vor seinem Tod
  bereits in Spanien missioniert haben und auf wundersame Weise mit dem Schiff
  dorthin zur&uuml;ckgekehrt sein. Ein Einsiedler hatte einen Traum, auf Grund
  dessen die Gebeine im Jahre 813 wieder gefunden wurden. Jakobus wurde der Schutzpatron
  Spaniens und seinem Schutz wird die <a href="http://www.kath.de/quodlibe/santiago/santia06.htm">R&uuml;ckeroberung
  des Landes</a> aus der Hand der Mauren zugeschrieben. So erschien er nach der
  Legende 844 in der Schlacht von Clavijo in Nordspanien und f&uuml;hrte die
  Christen zum Sieg.<br>
  Nach Jakob werden im Rheinland die Kellner &#8222;K&ouml;bes&#8220; und in
  England die Butler James genannt. Das geht wohl auf die M&auml;nner zur&uuml;ck,
  die in den Herbergen die Jakobspilger bewirteten. Der &#8222;wahre Jakob&#8220; k&ouml;nnte
  sich davon herleiten, da&szlig; im Mittelalter einige St&auml;dte behaupteten,
  sie beherbergten die Gebeine des Apostels, was f&uuml;r die Pilger den Vorteil
  hatte, nicht bis an die Westk&uuml;ste Spaniens laufen zu m&uuml;ssen.<br>
  Die Muschel geht auch auf einen legendarischen Bericht zur&uuml;ck, n&auml;mlich
  da&szlig; ein Ritter dem Schiff, das den Leichnam des Apostels nach Spanien
  brachte, entgegenritt. Er drohte, unterzugehen, der Heilige rettete ihn. Als
  der Ritter aus dem Wasser steigt, war er mit Muscheln bedeckt. Zudem findet
  man an der K&uuml;ste, zu der die Pilger von Santiago noch weitergehen, viele
  Jakobsmuscheln.<br>
  Der 25. Juli ist der Gedenktag des hl. Jakobus, wenn er auf einen Sonntag f&auml;llt,
  zieht Santiago besonders viele Pilger an.</font></P>
                  <p><font size="2" face="Arial, Helvetica, sans-serif">Eckhard
                      Bieger</font><font face="Arial, Helvetica, sans-serif"> <br>
                  </font></p>
                  <p><font size="2" face="Arial, Helvetica, sans-serif"><a href="http://www.kath.de/quodlibe/santiago/santiago.htm">Weitere
                        Informationen</a></font> u.a.<br>
                    <a href="http://www.kath.de/quodlibe/santiago/santia07.htm">Rechtsschutz
                    der Pilger im Mittelalter</a> <br>
                    <a href="http://www.kath.de/quodlibe/santiago/santia08.htm">Aufnahme
                    und Versorgung der Pilger</a> <br>
              <a href="http://www.kath.de/quodlibe/santiago/santia09.htm">Die
              wirtschaftliche Seite des Pilgerwesens</a> </p>
                  <p></p>
                  <p></p>
                  <P>&copy;<a href="http://www.kath.de"> <font face="Arial, Helvetica, sans-serif">www.kath.de</font></a></P></td>
                <td valign="top"><script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
google_ad_width = 160;
google_ad_height = 600;
google_ad_format = "160x600_as";
google_ad_type = "text_image";
//2007-09-05: Bildung und Erziehung
google_ad_channel = "3955521420";
google_ui_features = "rc:6";
//-->
            </script>
                    <script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
              </script>
                </td>
              </tr>
            </table>
            <P>&nbsp;</P>
            </TD>
          <TD background=../philosophie_theologie/boxright.gif><IMG height=8 alt="" 
            src="../philosophie_theologie/boxright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=8 alt="" src="../philosophie_theologie/boxbottomleft.gif" 
            width=8></TD>
          <TD background=../philosophie_theologie/boxbottom.gif><IMG height=8 alt="" 
            src="../philosophie_theologie/boxbottom.gif" width=8></TD>
          <TD><IMG height=8 alt="" src="../philosophie_theologie/boxbottomright.gif" 
            width=8></TD></TR></TBODY></TABLE></TD></TR>
  <TR>
    <TD vAlign=top align=left>&nbsp; </TD>
  </TR></TBODY></TABLE></BODY></HTML>
