<HTML><HEAD><TITLE>Spiritualit&auml;t - geistliches Leben</TITLE>
<META http-equiv=Content-Type content="text/html; charset=iso-8859-1"><LINK
title=fonts href="../philosophie_theologie/kaltefleiter.css" type=text/css
rel=stylesheet>
<META content="MSHTML 5.50.4134.600" name=GENERATOR></HEAD>
<BODY bgColor=#ffffff leftMargin=6 topMargin=6 marginheight="6" marginwidth="6">
<TABLE cellSpacing=0 cellPadding=6 width="100%" border=0>
  <TBODY>
  <TR>
    <TD vAlign=top align=left width=100>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left">
          <td width="8"><img src="../philosophie_theologie/boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="../philosophie_theologie/boxtop.gif"><img src="../philosophie_theologie/boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="../philosophie_theologie/boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="../philosophie_theologie/boxtopleft.gif"><img src="../philosophie_theologie/boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b>Religi&ouml;ses Leben</b></td>
          <td background="../philosophie_theologie/boxtopright.gif"><img src="../philosophie_theologie/boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="../philosophie_theologie/boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="../philosophie_theologie/boxdivider.gif"><img src="../philosophie_theologie/boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="../philosophie_theologie/boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="../philosophie_theologie/boxleft.gif"><img src="../philosophie_theologie/boxleft.gif" width="8" height="8" alt=""></td>
          <td> <?php include("logo.html"); ?> </td>
          <td background="../philosophie_theologie/boxright.gif"><img src="../philosophie_theologie/boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="../philosophie_theologie/boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="../philosophie_theologie/boxbottom.gif"><img src="../philosophie_theologie/boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="../philosophie_theologie/boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left">
          <td width="8"><img src="../philosophie_theologie/boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="../philosophie_theologie/boxtop.gif"><img src="../philosophie_theologie/boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="../philosophie_theologie/boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="../philosophie_theologie/boxtopleft.gif"><img src="../philosophie_theologie/boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="../philosophie_theologie/boxtopright.gif"><img src="../philosophie_theologie/boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="../philosophie_theologie/boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="../philosophie_theologie/boxdivider.gif"><img src="../philosophie_theologie/boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="../philosophie_theologie/boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="../philosophie_theologie/boxleft.gif"><img src="../philosophie_theologie/boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10"> <?php include("az.html"); ?> </td>
          <td background="../philosophie_theologie/boxright.gif"><img src="../philosophie_theologie/boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="../philosophie_theologie/boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="../philosophie_theologie/boxbottom.gif"><img src="../philosophie_theologie/boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="../philosophie_theologie/boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </TD>
    <TD vAlign=top rowSpan=2>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
        <TBODY>
        <TR vAlign=top align=left>
          <TD width=8><IMG height=8 alt=""
            src="../philosophie_theologie/boxtopleftcorner.gif" width=8></TD>
          <TD background=../philosophie_theologie/boxtop.gif><IMG height=8 alt=""
            src="../philosophie_theologie/boxtop.gif" width=8></TD>
          <TD width=8><IMG height=8 alt=""
            src="../philosophie_theologie/boxtoprightcorner.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=../philosophie_theologie/boxtopleft.gif><IMG height=8 alt=""
            src="../philosophie_theologie/boxtopleft.gif" width=8></TD>
          <TD bgColor=#e2e2e2>
            <H1><font face="Arial, Helvetica, sans-serif">Spiritualit&auml;t</font></H1>
          </TD>
          <TD background=../philosophie_theologie/boxtopright.gif><IMG height=8
            alt="" src="../philosophie_theologie/boxtopright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=13 alt="" src="../philosophie_theologie/boxdividerleft.gif"
            width=8></TD>
          <TD background=../philosophie_theologie/boxdivider.gif><IMG height=13
            alt="" src="../philosophie_theologie/boxdivider.gif" width=8></TD>
          <TD><IMG height=13 alt=""
            src="../philosophie_theologie/boxdividerright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=../philosophie_theologie/boxleft.gif><IMG height=8 alt=""
            src="../philosophie_theologie/boxleft.gif" width=8></TD>
          <TD class=L12>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td valign="top" class="L12"><P><STRONG><font face="Arial, Helvetica, sans-serif">Geistliches
                        Leben</font></STRONG></P>
                  <P><font face="Arial, Helvetica, sans-serif">Religion tritt
                      uns von au&szlig;en als Kirchengeb&auml;ude, in Gottesdiensten,
                      Wallfahrten und verschiedenen Riten entgegen. Religion
                      ist aber zuerst etwas im Inneren des Menschen. Sein Herz,
                      seine Seele wird von etwas ber&uuml;hrt, das nicht sichtbar
                      ist. Wie gestaltet sich dieses innere Leben? Der Mensch
                      setzt sich in Beziehung zu dem, was ihn religi&ouml;s bewegt.
                      Das kann er auf verschiedene Weise tun. Deshalb gibt es
                      nicht nur eine Spiritualit&auml;t. Das Wort kann daher
                      auch im Plural gebraucht werden.</font></P>
                  <P><font face="Arial, Helvetica, sans-serif"><strong>Worum
                        geht es?</strong><br>
  Spiritualit&auml;t hei&szlig;t nicht nur etwas wissen, was einmal sich erworben
  hat, sondern eine t&auml;gliche Praxis. Alle spirituellen Schulen gehen davon
  aus, da&szlig; der einzelne dem inneren religi&ouml;sen Leben eine bestimmte
  Zeit widmet. Daf&uuml;r kann sich jeder eine Form w&auml;hlen. In Gebeten,
  in Meditation, auch in K&ouml;rper&uuml;bungen wie dem Yoga setzt sich der
  einzelne mit dem Urgrund des Lebens in Beziehung. Daher hat die jeweilige Religion
  bzw. Weltanschauung einen bestimmenden Einflu&szlig; auf die jeweilige Spiritualit&auml;t.</font></P>
                  <P><strong><font face="Arial, Helvetica, sans-serif">Geist
                        ist Beziehung<br>
                    </font></strong><font face="Arial, Helvetica, sans-serif">Das
                    Wort Spiritualit&auml;t kommt von &#8222;Spiritus&#8220;,
                    das lateinische Wort f&uuml;r Geist. Geist meint im christlichen
                    Verst&auml;ndnis nicht das in sich ruhende &#8222;Ich&#8220;,
                    das sich in seinem Bewu&szlig;tsein selbst begegnet, sondern
                    Beziehung. Der Geist ist der Geist Gottes, der Geist Jesu,
                    weil dieser den Menschen mit Jesus Christus in Beziehung
                    setzt. Der Geist ist durch Taufe und Firmung &#8222;eingegossen&#8220; in
                    das Herz des Menschen und er&ouml;ffnet ihm so den Zugang
                    zu Gott. An Pfingsten haben die Anh&auml;nger Jesu die Erfahrung
                    des Geistes gemacht, er kam in Feuerzungen auf sie herab.
                    Die erste Wirkung der Geistbegabung war, da&szlig; die Apostel
                    ihre Angst &uuml;berwanden und &ouml;ffentlich &uuml;ber
                    Jesus, seinen Tod und seine Auferstehung sprachen.<br>
  Christlich hei&szlig;t Spiritualit&auml;t, im Geist Jesu zu leben. Christentum
  ist nicht zuerst eine Moral, die es zu befolgen gibt, sondern bedeutet, geistlich
  zu leben, sich auf den Geist einzulassen, sich von ihm f&uuml;hren zu lassen.</font></P>
                  <P><font face="Arial, Helvetica, sans-serif"> <strong>Konkrete
                        Praxis </strong><br>
  Spiritualit&auml;t hat als Basis die Religion mit ihren bestimmten Inhalten.
  F&uuml;r die christliche Spiritualit&auml;t ist die Bibel die Grundlage. Aus
  ihr werden viele Gebete &uuml;bernommen, so die Psalmen, das Vater unser, welches
  Matth&auml;us in der Bergpredigt &uuml;berliefert, den Lobgesang des Zacharias
  und das Magnifikat, das von Maria stammt und im Lukasevangelium &uuml;berliefert
  ist. (Kap. 1,46-55) Neben dem Psalmengebet ist die Meditation biblischer Stoffe
  eine wichtige Praxis. Hinzukommen ein Tagesr&uuml;ckblick mit dem Dank f&uuml;r
  das Gute, das der Beter erfahren hat und die Bitte um Vergebung, wo er sich
  verfehlt hat. Eine christliche Spiritualit&auml;t integriert das Handeln im <a href="spiritualitaet_arbeit.php">Alltag</a>.
  Das geistliche Leben bietet die Basis, im Alltag vom Geist Jesu inspiriert
  zu entscheiden und zu handeln. Damit basiert die konkrete spirituelle Praxis
  auf der menschlichen Freiheit, sie ist <a href="http://www.kath.de/aktuell/Buchvorstellungen/freiheit-wurzelgrund/freiheit-wurzelgrund1.htm">Freiheitpraxis.</a><br>
  In der buddhistischen Tradition, vor allem in der japanischen Zenmeditation
  werden nicht bestimmte Inhalte meditiert, sondern der Geist leerger&auml;umt,
  um sich auf diesem Weg mit dem Nirwana zu verbinden.<br>
                  </font></P>
                  <P><font face="Arial, Helvetica, sans-serif"><strong>Zitate</strong><br>
  Sendest du deinen Geist aus, so werden sie (alle Lebewesen) erschaffen und
      du erneuerst das Antlitz der Erde. <br>
  Psalm 104,30</font></P>
                  <p><font face="Arial, Helvetica, sans-serif">Auch die Sch&ouml;pfung
                      soll von der Sklaverei und Verlorenheit befreit werden
                      zur Freiheit und Herrlichkeit der Kinder Gottes. Denn wir
                      wissen, da&szlig; die gesamte Sch&ouml;pfung bis zum heutigen
                      Tag seufzt und in Geburtswehen liegt. Aber auch wir, obwohl
                      wir als Erstlingsgabe den Geist haben, seufzen in unseren
                      Herzen und warten darauf, da&szlig; wir mit der Erl&ouml;sung
                      unseres Leibes als S&ouml;hne offenbar werden. &#8230; so
                      nimmt sich auch der Geist unserer Schwachheit an. Denn
                      wir wissen nicht, worum wir in rechter Weise beten sollen;
                      der Geist selbst tritt jedoch f&uuml;r uns ein mit Seufzen,
                      die wir nicht in Worte fassen k&ouml;nnen. <br>
  R&ouml;merbrief 8, 21, 23, 26</font></p>
                  <p><font face="Arial, Helvetica, sans-serif">Denn uns hat es
                      Gott enth&uuml;llt durch den Geist. Der Geist ergr&uuml;ndet
                      n&auml;mlich alles, auch die Tiefen Gottes. Wer von den
                      Menschen kennt den Menschen, wenn nicht der Geist des Menschen,
                      der in ihm ist? So erkennt auch keiner Gott &#8211; nur
                      der Geist Gottes. &#8230;.. Der irdisch gesinnte Mensch
                      aber l&auml;&szlig;t sich nicht auf das ein, was vom Geist
                      Gottes kommt. Torheit ist es f&uuml;r ihn, und er kann
                      es nicht versehen, weil es nur mit Hilfe des Geistes beurteilt
                      werden kann. Der geisterf&uuml;llte Mensch urteilt &uuml;ber
                      alles, ihn aber vermag niemand zu beurteilen. Denn wer
                      begreift den Geist des Herrn? Wer kann ihn belehren? Wir
                      aber haben den Geist Christie. <br>
  1. Korintherbrief 2, 10-11, 14-16</font></p>
                  <p><font face="Arial, Helvetica, sans-serif">Eckhard Bieger</font></p>
                  <P>&copy;<a href="http://www.kath.de"> <font face="Arial, Helvetica, sans-serif">www.kath.de</font></a></P></td>
                <td valign="top"><p><a href="http://www.einfachlebenbrief.de/bestellen.html?elwkz=LEWWW51" target="_blank"><img src="einfachleben_268x70.jpg" width="268" height="70" border="0"></a></p>
                  <p><a href="http://www.abtei-muensterschwarzach.de/ams/startseite/index.html" target="_blank"><img src="abtei.jpg" width="300" height="285" border="0"></a>
                  <p><a href="http://www.update-seele.de" target="_blank"><img src="banner-update-seele-02.jpg" width="286" height="70" border="0"></a>
                  <p><a href="http://www.neufeld-verlag.de/" target="_blank"><img src="neufeld.jpg" width="286" height="238" border="0"></a>
                  </p>
                  <p>
                        <script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
google_ad_width = 160;
google_ad_height = 600;
google_ad_format = "160x600_as";
google_ad_type = "text_image";
//2007-09-05: Bildung und Erziehung
google_ad_channel = "3955521420";
google_ui_features = "rc:6";
//-->
                      </script>
                        <script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
                            </script>
                  </p></td>
              </tr>
            </table>
            <P>&nbsp;</P>
            </TD>
          <TD background=../philosophie_theologie/boxright.gif><IMG height=8 alt=""
            src="../philosophie_theologie/boxright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=8 alt="" src="../philosophie_theologie/boxbottomleft.gif"
            width=8></TD>
          <TD background=../philosophie_theologie/boxbottom.gif><IMG height=8 alt=""
            src="../philosophie_theologie/boxbottom.gif" width=8></TD>
          <TD><IMG height=8 alt="" src="../philosophie_theologie/boxbottomright.gif"
            width=8></TD></TR></TBODY></TABLE></TD></TR>
  <TR>
    <TD vAlign=top align=left>&nbsp; </TD>
  </TR></TBODY></TABLE></BODY></HTML>