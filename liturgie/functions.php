<?php

	// Lexikon-Script by Andreas Dewald
	// 03.2007

	// checks if the given string $str contains the string $search
	function stringContainsSearch($str, $search){
		$searchingin="  ,".$str.",";
        $spos = strpos($searchingin, $search);
		if($spos > 0){
			return TRUE;
		}else{
			return FALSE;
		}
	}

	// verifies if given file is part of the script
	function isInvalid($page){
		if( stringContainsSearch($page, "index.php") || stringContainsSearch($page, "functions.php") || stringContainsSearch($page, "az.php") || stringContainsSearch($page, "googleadd.php") || stringContainsSearch($page, "../") ){
			return TRUE;
		}else{
			return FALSE;
		}
	}

	// returns the content of the given file
	function readcontent($pagepath){
		$thecontent="";
	    $datei=$pagepath;
        $file2 = @fopen($datei, "r");
        if(!$file2) {
            echo "Error reading file.<br>";
        }else{
            while(!feof($file2)) {
                $temp = fgets($file2);
                $temp1 = $temp;
                $temp = "  ".substr($temp,0,strlen($temp));
                $content=$temp;
                $propstart=strpos($content,"titel:");
                $propstart2=strpos($content,"stichworte:");
                $propstart3=strpos($content,"bild:");
                $propstart4=strpos($content,"rightpanel:");
                $propend=strlen($content);

                if( propstartIsNull($propstart) && propstartIsNull($propstart2) && propstartIsNull($propstart3) && propstartIsNull($propstart4) ){
                    $thecontent .= "<br>".$temp1;
                }else{
                    // in this line there is the title
                }
            }
        }
        return $thecontent;
	}

	function propstartIsNull($propstart){
		if( ($propstart == "false") || ($propstart == "") || empty($propstart) || ($propstart == 0) || (!$propstart) ){
			return TRUE;
		}else{
			return FALSE;
		}
	}

	// reads title of the given file, title should be marked as follows:
	// title:ThisIsTheTitle
	function readtitle($pagepath){
	   $searchstring = "titel:";
	    $answer = readfrompage($pagepath, $searchstring);
	    return $answer;
	}

	function readkeywords($pagepath){
		$searchstring = "stichworte:";
	    $answer = readfrompage($pagepath, $searchstring);

	    if (($answer != "") && ($answer != " ") && ($answer != "Error: not found.")){
	    	;
	    }else{
	    	$answer = "";
	    }

	    return $answer;
	}

	function readrightpanel($pagepath){

		include "config.php";

		$searchstring = "bild:";
	    $answer = readfrompage($pagepath, $searchstring);
	    if (($answer != "") && ($answer != " ") && ($answer != "Error: not found.") && (strlen($answer) > 3) ){
	    	$answer = "<img src='".$userpicturedir."/".$answer."' border=0>";
	    }else{
	    	$answer = "";
	    }

	    return $answer;
	}
	
	function readrightpanelhtml($pagepath){

		include "config.php";

		$searchstring = "rightpanel:";
	    $answer = readfrompage($pagepath, $searchstring);
	    if (($answer != "") && ($answer != " ") && ($answer != "Error: not found.")  ){
	    	;
	    }else{
	    	$answer = "";
	    }

	    return $answer;
	}

	function readfrompage($pagepath, $search){
		$searchstring = $search;
	    $datei=$pagepath;
        $file2 = @fopen($datei, "r");
        if(!$file2) {
            echo "Error reading file.<br>";
        }else{
            while(!feof($file2)) {
                $temp = fgets($file2);
                $temp = "  ".substr($temp,0,strlen($temp));
                $content=$temp;
                $propstart=strpos($content,$searchstring);
                $propend=strlen($content);
				//echo "--$content--";
                if( ($propstart == "false") || ($propend == "false") || ($propstart == "") || empty($propstart) || ($propstart == 0) ){
                    ;
                }else{
                    $title = substr($content, $propstart + strlen($searchstring));
                    $title = strip_tags($title);
                    return $title;
                }
            }
        }
        return "Error: not found.";
	}


?>