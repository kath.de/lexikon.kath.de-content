titel:H�nde aufgelegen
stichworte:H�nde aufgelegen, Weihe, kath
bild:bild.jpg

Mit der Auflegung der H�nde, meist auf den Kopf, wird ein Amt �bertragen. "H�nde auflegen" ist schon im Neuen Testament der Begriff f�r eine Weihehandlung. Im 1. Timotheusbrief findet sich folgende Mahnung "Lege keinem vorschnell die H�nde auf und mach dich nicht mitschuldig an fremden S�nden; bewahre dich rein!"