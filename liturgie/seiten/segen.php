titel:Segen
stichworte:Segen, Handauflegung, Kreuzzeichen, kath
bild:bild.jpg

Der Segen wird im Namen Gottes einem Menschen zugesprochen, einmal durch Handauflegung und im christlichen Gottesdienst durch den Priester, indem dieser das Kreuzzeichen mit der Hand nicht auf sich, sondern zu den Gläubigen hin gerichtet spricht: Es segne euch der allmächtige Gott, der Vater, der Sohn und der Heilige Geist."