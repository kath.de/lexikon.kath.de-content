titel:Bischofsstab
stichworte:Bischofsstab, Hirtenstab, Investitur, F�rstenbistum, kath
bild:bild.jpg

Ein gekr�mmter Stab, den der Bischof beim Ein- und Auszug aus der Kirche tr�gt, sowie bei der Verlesung des Evangeliums, bei der Predigt und zum abschlie�enden Segen. Der Bischofsstab leitet sich vom Hirtenstab her und ist Ausdruck der Amtsvollmacht. Im Mittelalter war mit der �bergabe des Stabes durch den K�nig die Investitur, d.h. die �bergabe der weltlichen Macht �ber das Gebiet des Bistums, verbunden. Das Sprichwort "Unter dem Krummstab l�sst sich gut leben" bezieht sich auf die meist besseren Lebensbedingungen in den ehemaligen F�rstbist�mern, weil die Landesherren, die Bisch�fe, etwas weniger Kriege f�hrten.