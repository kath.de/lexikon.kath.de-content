titel:Angelusl�uten - Engel des Herrn
stichworte:Angelusl�uten, Engel des Herrn, Liturgie, Hore, Stundengebet, kath
bild:bild.jpg

Dreimal am Tag l�utet die Glocke zum "Engel des Herrn", jeweils morgens, zur Mittagszeit und am Abend. Das Gebet beginnt mit dem Gru� des Engels an Maria, daher "Angelus", lateinisch Engel.

Der Engel des Herrn brachte Maria die Botschaft -
und sie empfing vom Heiligen Geist
Gegr��et seist du Maria �
Maria sprach: Siehe ich bin die Magd des Herrn -
mir geschehe nach deinem Wort.
Gegr��et seist du Maria �
Und das Wort ist Fleisch geworden -
und hat unter uns gewohnt
Gegr��et seist du Maria �.
Bitte f�r uns, heilige Gottesmutter;
dass wir w�rdig werden der Verhei�ungen Christi.
Lasset uns beten: Allm�chtiger Gott, gie�e deine Gnade in unsere Herzen ein. Durch die Botschaft des Engels haben wir die Menschwerdung Christi, deines Sohnes, erkannt. Lass uns durch sein Leiden und Kreuz zur Herrlichkeit der Auferstehung gelangen. Darum bitten wir durch Christus unsern Herrn. Amen

Das abschlie�ende Gebet spannt den Bogen von der Menschwerdung des Sohnes Gottes bis zu seinem Kreuz und seiner Auferstehung. Darauf hinzuweisen ist, dass dieses Gebet sich nicht mehr wie das "Gegr��et seist du" an Maria richtet, sondern an den Vater, zu dem die Christen "durch" Jesus Christus, den Mittler zwischen den Menschen und Gott, beten.

Der Engel des Herrn ist ein kleines Stundengebet, das man auf der Stra�e, auch im B�ro oder vor dem Essen beten kann. Morgens, mittags und abends sind auch die drei Hauptzeiten des <a href="index.php?page=stundengebet.php">Stundengebets</a> Die <a href="index.php?page=laudes.php">Laudes</a>, das Morgenlob, die <a href="index.php?page=kleine_hore.php">Sext</a> Mittags zur sechsten Stunde um 12 Uhr und das Abendgebet, die <a href="index.php?page=vesper.php">Vesper</a>.
Von dem Stundegebet �bernimmt der Engel des Herrn auch den Wechsel, nicht wie im Stundengebet zwischen zwei Ch�ren, sondern zwischen Vorbeter und den Mitbetenden. Bei den Drei S�tzen antworten die Mitbetenden nach dem Bindestrich, beim Gegr��et seist Du Maria sprechen sie die abschlie�ende Bitte:
Heilige Maria, Mutter Gottes, bitte f�r uns S�nder jetzt und in der Stunde unseres Todes. Amen

<a href="http://www.kath.de/seiten/team.html#eb">Eckhard Bieger</a>
