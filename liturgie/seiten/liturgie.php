titel:Liturgie
stichworte:Liturgie, Messe, Stundengebet, kath
bild:bild.jpg

Liturgie kommt von dem griechischen zusammengesetzten Wort f�r Laos-Volk bzw. die Vielen und Ergon-Tun.
Liturgie ist also ein Werk von zumindest Mehreren.

Als Fachbegriff bezeichnet Liturgie das gottesdienstliche Handeln ins seiner Vielfalt.

Die Eucharistiefeier ist der Kern wie auch der H�hepunkt der Liturgie. Auch Taufe, Hochzeit und Beerdigung sind Liturgie.
Firmung und Priesterweihe werden jeweils in einer Messfeier gespendet.
Auch das Stundengebet ist Liturgie, Tagzeitenliturgie genannt.

Die Liturgie bezieht den Menschen auf Gott, sie ist Lob, Dank, Bitte und in den Sakramenten das auch in einem �u�eren Zeichen, Wasser, Brot, Wein, Chrisam, Handauflegung zugesagte Heil.

Das Wort hat bereits die ins Griechische �bersetzte Bibel der Juden, die Septuaginta, f�r den Tempeldienst verwendet. Als Liturgie konnte damals auch die Armenspeisung als von Gott aufgetragenes Werk verstanden werden.