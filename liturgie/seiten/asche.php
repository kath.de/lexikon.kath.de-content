titel:Asche
stichworte:Asche, Aschermittwoch, Aschekreuz, kath
bild:bild.jpg

Aus den Zweigen, die am Palmsonntag geweiht wurden, wird im darauf folgenden Jahr die Asche gewonnen, die den Gl�ubigen am Aschermittwoch nach dem Verlesen des Evangeliums auf das Haupt gestreut oder in Form eines Kreuzes auf die Stirn gezeichnet wird. "Bedenke Mensch, dass du Staub bist und wieder zum Staub zur�ckkehren wirst", h�rt der Gl�ubige. Im Sinne der Fastenzeit kann die Bezeichnung mit dem Aschenkreuz auch so lauten: "Bekehrt euch und glaubt an das Evangelium."