titel:Mozetta
stichworte:Mozetta, Umhang, Bishof, Kardinal, kath
bild:bild.jpg

Ein Umhang, der die Schultern bedeckt. Er wird von Kardin�len und Bisch�fen �ber der <a href="index.php?page=soutane.php">Soutane</a> getragen, aber auch vom h�heren Klerus, so den Mitgliedern des Domkapitels und von einigen Orden getragen. Die Mozetta der Kardin�le hat eine kleine Kapuze. Das Wort kommt vom italienischen "abgeschnitten".