titel:Magnificat
stichworte:Magnifikat, Lukas, Maria, Elisabeth, kath
bild:bild.jpg

Dieses Lied wird von Lukas �berliefert, Kap. 1,46-55. Maria hat es gesprochen, als sie ihre Kusine Elisabeth besuchte, die im 6. Monat schwanger war. Das Magnifikat wird in jeder Vesper nach dem kurzen Lesungstext gebetet. Magnifikat kommt von "magnus", gro� und "preisen". Im Text hei�t es: "Meine Seele preist die Gr��e des Herrn."
Da sagte Maria: Meine Seele preist die Gr��e des Herrn, und mein Geist jubelt �ber Gott, meinen Retter.
Denn auf die Niedrigkeit seiner Magd hat er geschaut. Siehe, von nun an preisen mich selig alle Geschlechter.
Denn der M�chtige hat Gro�es an mir getan und sein Name ist heilig.
Er erbarmt sich von Geschlecht zu Geschlecht �ber alle, die ihn f�rchten.
Er vollbringt mit seinem Arm machtvolle Taten: Er zerstreut, die im Herzen voll Hochmut sind;
er st�rzt die M�chtigen vom Thron / und erh�ht die Niedrigen.
Die Hungernden beschenkt er mit seinen Gaben und l�sst die Reichen leer ausgehen.
Er nimmt sich seines Knechtes Israel an und denkt an sein Erbarmen, das er unsern V�tern verhei�en hat, Abraham und seinen Nachkommen auf ewig.
Lukas 1, 46-55

<b><a href="http://www.magnificat-das-stundenbuch.de/de/Inhalte.html?utm_source=Onlinewerbung&utm_medium=Bannerwerbung&utm_campaign=kath_de_Inhalte">Weitere Informationen zu diesem Thema finden Sie hier bei Magnificat.</a></b>