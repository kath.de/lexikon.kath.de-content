titel:Kleine Hore
stichworte:Kleine Hore, Terz, Sext, Non, Stundengebet, kath
bild:bild.jpg

Die kleinen Horen sind Teile des <a href="index.php?page=stundengebet.php">Stundengebets</a>. Sie werden so genannt, weil sie k�rzer, als die anderen Gebetszeiten sind. Sie sind nach der Zeit in der antiken Einteilung benannt, in der sie gebetet werden: Die Terz zur dritten (6:00 Uhr), die Sext zur sechsten (12:00 Uhr) und die Non zur neunten Stunde (15:00 Uhr).
Die Zeiten ergeben sich aus besonderen Anl�ssen:
        Zur dritten Stunde erhielten die Apostel den Heiligen Geist und begann die Verk�ndigung der frohen Botschaft.
        Die Sext ist das Mittagsgebet der Kirche
        Zur neunten Stunde starb Christus am Kreuz. Deshalb wird zu der Stunde besonders der Toten gedacht.

Wer nicht zum monastischen Gebet verpflichtet ist, kann sich eine der drei Horen aussuchen, die er beten will.


Der Aufbau der drei Horen ist gleich:

Er�ffnung
Hymnus
Psalm
Psalm
Psalm
Kurzlesung
Versikel
Oration
Entlassung

Die kleinen Horen haben, wie andere Teile des Stundengebetes, eine charakteristische Er�ffnung: Der Vorbeter/ Priester beginnt:
Vorbeter:         O Gott, komm mir zu Hilfe
Gemeinde:         Herr, eile mir zu helfen
V:                 Ehre sei dem Vater und dem Sohne und des Heiligen Geiste. G: Wie es war im
G:                 Anfang, so auch jetzt und alle Zeit und in Ewigkeit Amen. Halleluja.
Die Hymnen werden aus der christlichen Tradition ausgew�hlt und spiegeln den Schwerpunkt der jeweiligen Hore wider.
Aus dem Buch der Psalmen werden jeweils drei Psalmen ausgew�hlt oder ein l�ngerer Psalm aufgeteilt. Neben den Psalmen f�r den jeweiligen Tag gibt es noch eine Erg�nzungspsalmodie, die verwendet werden kann, wenn mehr als eine kleine Hore am Tag gebetet wird. Gerahmt werden die Psalmen durch <--a hfref="index.php?page=antiphon.php"-->Antiphonen<--/a-->, die den Grundgedanken der Ges�nge aufgreifen und miteinander verbinden. Die Lesung ist kurz, einige Verse aus der den Prophetenb�chern des Alten Testaments. Auch sie unterscheidet sich, je nachdem, zu welcher Tageszeit gebetet wird. Auf die Lesung folgt ein Versikel, ein Doppelvers aus einem Psalm, dessen erste H�lfte vom Vorbeter und zweite H�lfte von der Gemeinde gebetet wird.
Die kleinen Horen enden mit der Entlassung:
Vorbeter:        Singet Lob und Preis.
Gemeinde:        Dank sei Gott, dem Herrn.
