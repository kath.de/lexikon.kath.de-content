titel:Exsultet
stichworte:Exsultet, Osternacht, Lobpreis, Osterlob, kath
bild:bild.jpg

<h2>das Osterlob der Osternachtfeier er�ffnet</h2>

Die Feier der Osternacht beginnt erst, wenn es dunkel geworden ist. Dann zog sie sich in den fr�hen Jahrhunderten durch die ganze Nacht. Wenn morgens dann die Sonne aufging, wurde das Oster-Halleluja angestimmt und das Osterevangelium verk�ndet. Durch die ganze Nacht hindurch wachte die Kirche dieser Botschaft entgegen. Lesungen und Ges�nge aus dem Alten Testament f�llten die Nacht, dazu die Taufe der neuen Christen. Naturnotwendig geh�rt zu solchen n�chtlichen Gottesdiensten die Symbolkraft des Lichtes. Auch andere n�chtliche Gottesdienste w�hrend des Jahres wurden durch eine Segnung des Lichtes und ein Loblied auf Christus, der unser Licht ist, er�ffnet, und in manchen Kl�stern geschieht das heute noch. Doch nie war das Lob des Lichtes so festlich wie in der Osternacht. Feierlich wurde und wird heute noch die entz�ndete Osterkerze in die dunkle, menschengef�llte Kirchenhalle hineingetragen. Dreimal singt der Diakon, jedesmal in h�herem Ton: "Lumen Christi" - "Das Licht des Christus." Alle antworten: "Deo gratias" - "Gott sei gedankt." Die Gl�ubigen entz�nden ihre Kerzen am Licht der Osterkerze. Die Osterkerze wird auf einen gro�en Leuchter gestellt. Der Diakon tritt vor sie hin und singt nun den sch�nsten kantillierenden Gesang, den die lateinische Liturgie kennt, das "Exsultet". Es stammt aus dem Sch�lerkreis des heiligen Ambrosius von Mailand. Es hatte viele Vertonungen, aber eine hat sich am Ende durchgesetzt, sicher die sch�nste.

<h2>Theologie des Exsultet</h2>
Es ist ein Lobgesang auf das Licht. Doch zugleich ist es ein Lobgesang auf das ganze Ostergeheimnis, und im Kern ist es ein Lobgesang auf den erstandenen Christus, den die Osterkerze zeichenhaft darstellt. Zugleich aber ist es der deutende Schl�ssel f�r all die Lesungen aus dem Alten Testament, die jetzt bald in der n�chtlichen Feier gelesen werden. Was da, mit der Sch�pfung beginnend, vorgelesen wird, vor allem der Auszug aus �gypten, ist schon der Anfang, ja ein inneres St�ck des Ostergeheimnisses. Dieses Festgeheimnis beschr�nkt sich nicht auf Jesu Tod und Auferstehung, sondern reicht zur�ck bis zu den Anf�ngen der Welt, und es reicht voraus bis zur Wiederkunft Christi, des jetzt im Himmel Erh�hten. Mit Erwartung an sein Kommen endet das Exsultet - die fr�he Christenheit war �berzeugt, Christus werde in einer Osternacht wiederkommen. Der Morgen, dem die n�chtliche Feier entgegenschreitet, ist f�r die Feiernden der Morgen am Ende der Weltgeschichte.

<h2>Das Exsultet ist ein Loblied auf die Taten Gottes</h2>
Zuerst zieht der Diakon den Vorhang auf: Er besingt den Einzug des Erstandenen in den Raum des Himmels und das Licht, das sich dadurch in der ganzen Sch�pfung ausbreitet, vor allem auch �ber die jetzt versammelte Gemeinde. Dann bittet er, f�r ihn selbst zu beten, damit aus seinem Gesang auch wirklich Lob Gottes entsteht. Das ist gewisserma�en die Ouvert�re. Dann folgen die Wechselrufe wie bei jeder Pr�fation, und im Stil von Pr�fationen, nur viel festlicher, geht es dann auch weiter. Der Preis geht auf Gott, den unsichtbaren Vater, gleitet �ber auf seinen Sohn Jesus Christus, auf dessen Tod als das "wahre Lamm", und gelangt so zur jetzigen Nacht. Immer wieder hei�t es: "Dies ist die Nacht" - und dabei schieben sich die verschiedenen N�chte ineinander zu einer einzigen, die Nacht des Auszugs aus �gypten, des Durchzugs durchs Meer, die vielen N�chte des W�stenzugs Israels hinter der Feuers�ule her, die Nacht der Auferstehung Jesu von den Toten, die jetzige Nacht, in der in der ganzen Welt Menschen durch die Taufe "den Heiligen zugesellt" werden. Mitten in diesen Preis der Verschmelzung der N�chte schieben sich, immer wieder mit "O" beginnend, Freudenrufe �ber das wunderbare Handeln Gottes ein. Dieses Loblied ist ein "Abendopfer". In der Gestalt der Kerze bringt der Diakon es Gott dar und bittet ihn, dieses Opfer anzunehmen. Nochmals klingt das Lob der Nacht auf, und dann folgt die abschlie�ende Bitte: Das Licht der Osterkerze, in dem sich die Hingabe aller Versammelten verdichtet, m�ge mitten unter den himmlischen Lichtern weiterleuchten bis zum Morgen, wenn die Sonne aufgeht - die Sonne, die Christus ist, der am Ende der Zeiten wiederkehren wird.

<h2>Hinf�hrung auf die Lesungen im Osternachgottesdienst</h2>
Dies ist hohe Dichtung, zugleich ganz durchsetzt mit Bildern und W�rtern der Heiligen Schrift, am Ende vor allem des Hohenliedes der Liebe. Mit diesem Osterlob im Ohr wird man die Lesungen aus dem Alten Testament, die jetzt aufeinander folgen, ganz neu h�ren k�nnen. Sie werden alle in das eine gro�e Geheimnis der Ostern hineingenommen.
Die hier vorgelegte �bersetzung ist genauer und bibeln�her als die �bersetzung in den liturgischen B�chern. Ferner ist sie sprachlich so angelegt, da� sie sich in gr��erer Entsprechung zur Tonf�hrung des lateinischen Exsultet singen l��t. Die aus diesem Anliegen stammende Vertonung (durch Erwin B�cken) finden Sie hier Link zu www.schimmelpfeng.org/kantill/exsult02.htm

<b>Norbert Lohfink S.J.</b>

<h2>Das Exsultet</h2>
<ol><li>Schon juble in den Himmeln die Menge der Engel,
es juble die Schar der g�ttlichen Dienste,
und zu solch eines K�nigs Einzug k�nde Sieg die Trompete.</li>
<li>Da freue sich auch der Erdkreis, erhellt von leuchtenden Blitzen,
und, angestrahlt von der Pracht des ewigen K�nigs,
ersp�re er, da� er befreit ist vom Dunkel, das alles deckte.</li>
<li>Gl�ckselig sei auch die Mutter Kirche, geschm�ckt mit solch blitzendem Lichte,
und vom lauten Jubel der V�lker t�ne wider diese Halle.</li>
<li>So bitte ich euch, liebste Br�der und Schwestern,
die ihr steht beim herrlichen Glanz dieses heiligen Lichtes:
Ruft mit mir zu Gott, dem Allm�chtigen,
er m�ge sich meiner erbarmen:</li>
<li>Da� er, der mich von sich aus in die Zahl der Leviten gerufen hat,
mich f�lle mit dem Glanz seines Lichtes
und durch mich das Lob dieser Kerze wirke.</li></ol>

Der Herr sei mit euch.
Und mit deinem Geiste.

Erhebet die Herzen.
Wir haben sie beim Herrn.

Lasset uns danken dem Herrn, unserm Gott.
Das ist w�rdig und recht.

<ol><li>Wahrhaft w�rdig und recht ist es, den unsichtbaren Gott, den allm�chtigen Vater,
und seinen eingeborenen Sohn, unsern Herrn Jesus Christus,
mit aller Inbrunst des Herzens und Geistes,
im Dienste des Wortes, mit lauter Stimme zu preisen -</li>
<li>ihn, der f�r uns beim ewigen Vater die Schulden Adams bezahlt hat
und ausgel�scht hat den uralten Schuldbrief
mit Blut des Erbarmens.</li>
<li>Dies ist ja das Fest der Ostern,
an dem jenes wahre Lamm get�tet wird,
durch dessen Blut die T�ren der Gl�ubigen gefeit sind.</li>
<li>Dies ist die Nacht, in der du am Anfang unsere V�ter, die Nachkommen Israels,
nachdem sie herausgef�hrt waren aus �gypten,
trockenen Fu�es durch das Schilfmeer geleitet hast.</li>
<li>Dies also ist die Nacht, welche die Finsternis der S�nden
durch der Feuers�ule Erleuchtung verscheucht hat.</li>
<li>Dies ist die Nacht, die heute auf der ganzen Erde Menschen, die zum Glauben an Christus
gekommen sind, losgel�st von den Lastern der Welt und vom Dunkel der S�nde,
heimf�hrt zur Gnade und den Heiligen zugesellt.</li>
<li>Dies ist die Nacht, da Christus die Fesseln des Todes gesprengt hat
und aus denen, die unter der Erde sind, als Sieger emporstieg.</li>
<li>Denn umsonst w�ren wir geboren,
w�re keiner gekommen, uns zu erl�sen.</li>
<li>O wie du dich �ber uns neigest
in staunenswertem Erbarmen!</li>
<li>O unerwartbare Zuwendung der Liebe:
Um den Knecht zu erl�sen, gabst du den Sohn dahin!</li>
<li>O wahrhaft n�tige S�nde Adams,
die getilgt ward vom Tode Christi!</li>
<li>O gl�ckliche Schuld,
der solch ein gro�er Erl�ser geziemte!</li>
<li>O wahrhaft selige Nacht, der einzig es ziemte, die Zeit und die Stunde zu kennen,
da Christus erstanden ist aus denen, die unter der Erde sind!</li>
<li>Dies ist die Nacht, von der geschrieben steht:
"und die Nacht - wie der Tag wird sie leuchten,"
und: "die Nacht ist meine Erleuchtung, sie wird mir zur Wonne."</li>
<li>Die Heiligung also, die in dieser Nacht sich ereignet,
jagt die Verbrechen fort,
sp�lt weg jede Schuld,
gibt Gestrauchelten wieder die Unschuld
und Trauernden Freude.
Feindschaft jagt sie fort,
bereitet die Eintracht
und beugt die Gewalten.</li>
<li>In deiner Gnade also, die diese Nacht durchwaltet,
nimm an, heiliger Vater, das Abendopfer dieses Loblieds,
das dir in dieser Kerze festlicher Darbringung, durch die H�nde deiner Diener, aus der Arbeit
der Bienen, entrichtet die hochheilige Kirche.</li>
<li>Doch schon wissen wir, wie sich der Heroldsruf dieser S�ule verbreitet,
die das goldene Feuer zur Ehre Gottes entz�ndet hat:</li>
<li>Wenn es auch vielfach geteilt ist,
wei� es dennoch von keiner Schw�chung des weitergereichten Lichtes.</li>
<li>Es n�hrt sich n�mlich vom schmelzenden Wachse,
das als den Reichtum dieser kostbaren Fackel
die Mutter Biene bereitet hat.</li>
<li>O wahrhaft selige Nacht, da werden verbunden
Irdischem Himmlisches, Menschlichem G�ttliches.</li>
<li>So bitten wir dich, o Herr:
Diese Kerze, geweiht zur Ehre deines Namens, brenne unerm�dlich weiter,
um das Dunkel dieser Nacht zu vernichten.
Als lieblicher Opferduft entgegengenommen,
mische sie sich unter die Lichter am Himmel.</li>
<li>Lodernde Flamme - so soll sie finden der Morgenstern.
Jener Morgenstern n�mlich, der keinen Untergang kennt:
Christus, dein Sohn, der, zur�ckgekehrt aus denen, die unter der Erde sind,
dem Menschengeschlechte heiter aufging
und der lebt und herrscht in alle Ewigkeit.</li></ol>
Amen.
