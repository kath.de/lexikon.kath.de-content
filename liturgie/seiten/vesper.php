titel:Vesper
stichworte:Vesper, Gebetszeit, Stundengebet, Liturgie, kath
bild:bild.jpg

Die Vesper ist ein Teil des <a href="index.php?page=stundengebet.php">Stundengebets</a>. Der Begriff stammt vom lateinischen vespera und bedeutet Abend. Die Vesper ist das Gebet, das nach Abschluss der Arbeit des Tages gebetet wird.

<b>Aufbau</b>
<blockquote>Er�ffnung
Hymnus
Psalm
Psalm
Canticum
Kurzlesung
Responsorium
Magnifikat
F�rbitten
Vater unser
Oration
Segen
Entlassung</blockquote>

In der Regel des hl. Benedikt ist die Vesper um 18:00 Uhr angesetzt. Sie geh�rt zu den gro�en <a href="index.php?page=hore.php">Horen</a> und wird auch Abendhore genannt. Neben den <a href="index.php?page=laudes">Laudes</a> ist dieses Abendgebet die wichtigste Stundegebetszeit.   Die Vesper, wie andere Teile des Stundengebetes, habt eine charakteristische Er�ffnung: Der Vorbeter/ Priester beginnt:
<blockquote>Vorbeter: O Gott, komm mir zu Hilfe
Gemeinde: Herr, eile mir zu helfen
V: Ehre sei dem Vater und dem Sohne und des Heiligen Geiste.
G: Wie es war im Anfang, so auch jetzt und alle Zeit und in Ewigkeit Amen. Halleluja.</blockquote>
Die Hymnen werden aus der christlichen Tradition ausgew�hlt und geben diesem Abendgebet vor allem an Festtagen eine eigene F�rbung.
Aus dem Buch der Psalmen werden jeweils zwei Psalmen ausgew�hlt, aus den psalm�hnlichen Ges�ngen der anderen B�chern des Neuen Testaments ein Canticum, ein Lied. Gerahmt werden die Psalmen und das Canticum durch <a href="index.php?page=ant">Antiphonen</a>, die Den Grundgedanken der Ges�nge aufgreifen.
Die Lesung ist kurz, einige Verse aus der Briefliteratur des Neuen Testaments. Sie wird mit dem <a href="index.php?page=responsorium.php">Responsorium</a>, einem Antwortgesang beantwortet. Mit ihm wird die zentrale Aussage der Kurzlesung aufgegriffen. Es hat eine charakteristische Form
<blockquote>Vorbeter: Doppelvers
Gemeinde: Wiederholung des Doppelverses
V: Halbvers
G: zweite H�lfte des Doppelverses vom Anfang
V: Ehre sei dem Vater und dem Sohne und dem Heiligen Geiste
G:Doppelvers vom Anfang</blockquote>
Das <a href="index.php?page=magnifikat.php">Magnifikat</a> wird in jeder Vesper gebetet bzw. gesungen. Es ist der Lobpreis den Maria anstimmt, als sie von Elisabeth als die Mutter des Herrn erkannt wird. Lukas �berliefert ihn in Kap.1, 46-55. Auch dieser Gesang wird durch eine Antiphon eingerahmt. Diese greift das Grundthema des Tages auf.
Die F�rbitten haben den gleichen Aufbau wie in der <a href="index.php?page=eucharestie.php">Eucharistiefeier</a>. W�hren die F�rbitten der Laudes eher auf die christliche Gemeinde bezogen sind, richten die F�rbitten der Vesper den Blick auf die Probleme au�erhalb des Kreises der Beter. Mit ihnen werden Anliegen von Staat und Kirche vor Gott gebracht. Das Bittgebet folgt der Aufforderung des 1. Timotheusbriefes: " Vor allem fordere ich zu Bitten und Gebeten, zu F�rbitten und Danksagung auf, und zwar f�r alle Menschen, f�r die Herrscher und alle, die Macht aus�ben, damit wir in aller Fr�mmigkeit und Rechtschaffenheit ungest�rt und ruhig leben k�nnen" (1 Tim 2,1f). Es handelt sich bei den Bitten bzw. F�rbitten um Anregungen, die die Betenden durch eigene Anliegen erg�nzen k�nnen. Die Bitten und F�rbitten m�nden in das Gebet des Herrn, das Vater unser, das gro�e Schlussgebet von Morgen- und Abendlob. Es ist das Gebet, das Jesus seine J�nger gelehrt hat, in dem er die Betenden hineinnimmt in sein Beten zum Vater. Das Vaterunser ist die Urgestalt christlichen Betens, das alle christlichen Konfessionen verbindet. Anders als in der Eucharestiefeier schlie�t das Vater Unser nicht mit der <a href="index.php?page=doxologie.php">Doxologie</a> "denn dein ist das Reich�", sondern es folgt unmittelbar das Tagesgebet, das aus dem Messbuch �bernommen wird und identisch mit dem Tagesgebet der Eucharistiefeier ist. Ein Segenswort m�ndet in die Entlassung: Gehet in Frieden
Wenn der Vesper nicht ein Priester und auch kein Diakon vorsteht, wird das Abendgebet mit folgenden Worten abgeschlossen: V: Der Herr segne uns, er bewahre uns vor Unheil und f�hre uns zum ewigen Leben. G: Amen.
Wenn die <a href="index.php?page=komplet">Komplet</a> nicht in Gemeinschaft gebetet wird, folgt �blicherweise eine <a href="index.php?page=antiphon_marianisch.php">Marianische Antiphon</a>


<i><b>Zitate</b>
II. Vatikanisches Konzil:
Die Laudes als Morgengebet und die Vesper als Abendgebet, nach der ehrw�rdigen �berlieferung der Gesamtkirche die beiden Angelpunkte des t�glichen Stundengebetes, sollen als die vornehmsten Gebetsstunden angesehen und als solche gefeiert werden. (Dokument �ber die Liturgei, Sacrosanctum Concilium, 89a)</i>