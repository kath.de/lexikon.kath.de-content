titel:Rochett
stichworte:Rochett, Rock, kath
bild:bild.jpg

Das Wort kommt eigentlich von Rock, bezeichnet aber ein, meist mit Stickereien, verziertes Hemd, das �ber dem <a href="http://www.kath.de/lexikon/liturgie/index.php?page=talar.php">Talar</a> getragen wird. Pr�laten, Bisch�fe und Kardin�le tragen auch au�erhalb liturgischer Vollz�ge ein Rochett �ber der <a href="http://www.kath.de/lexikon/liturgie/index.php?page=soutane.php">Soutane</a>.