titel:Kreuzzeichen
stichworte:Kreuzzeichen, Segen, Priester, kath
bild:bild.jpg

Das Kreuzzeichen wird vom Priester als Segensgestus eingesetzt, aber Priester und Gl�ubige bezeichnen sich selbst mit dem Kreuzzeichen, das auf den Dreieinigen Gott hin gedeutet wird: "Im Namen des Vaters und des Sohnes und des Heiligen Geistes." In den orthodoxen Kirchen legt der Betende die Spitzen von Daumen, Zeige- und Mittelfinger als Hinweis auf den dreifaltigen Gott zusammen und beginnt wie im Westen damit, seine Stirn zu ber�hren und die Brust zu ber�hren, dann ber�hrt er aber zuerst die linke Schulter und dann erst die rechte.