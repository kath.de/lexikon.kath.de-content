titel:Hochgebet
stichworte:Hochgebet, Eucharestie, Einsetzungsworte, Wandlung, Sanctus, Epiklese, kath
bild:bild.jpg

Im Zentrum der Eucharistiefeier steht ein l�ngeres Gebet, in das die Einsetzungsworte eingef�gt sind. Diese Worte haben ihre Bezeichnung davon, dass Jesus mit ihnen das Mahl "eingesetzt" hat, das zu seinem Ged�chtnis gefeiert wird. Mit den Einsetzungsworten bezeichnet Jesu das Brot als seinen Leib und den Wein als sein Blut. Damit hat er den Auftrag verbunden, das Mahl zu seinem Ged�chtnis zu feiern. So bleibt Jesus durch seine Worte, die im Wortgottesdienst vorgelesen werden, sowie durch die Mahlfeier, in der der Priester seine Einsetzungsworte spricht, gegenw�rtig. Das Mahl, das Jesus mit den J�ngern gefeiert hatte, war sein letztes. Jesus hatte sich anl��lich des Passahfestes in einer Jerusalemer Wohnung mit den 12 Aposteln zusammengefunden und innerhalb des j�dischen Mahles die Worte �ber das Brot und den Becher mit Wein gesprochen und diese dann herumgereicht. An die Mahlfeier erinnert der Gr�ndonnerstag. Bereits das j�dische Mahl zum Passahfest war ein Ged�chtnismahl, das an den Auszug der Juden aus �gypten, an die Befreiung aus der Sklaverei erinnerte. Gott wurde f�r seine Rettungstat gedankt, es gab Lobges�nge und auch bereits das Halleluja. Im Johannesevangelium sind im Zusammenhang mit dem Bericht von dem Mahl zum Passahfest die sog. Abschiedsreden Jesu aufgezeichnet. Sie f�gen sich auch in die j�dische Tradition ein, dass nicht nur gegessen, sondern �ber zentrale Fragen gesprochen wurde. Jesus hat nicht nur das Mahl gefeiert, sondern es auch erkl�rt und sich dabei an die j�dische Grundstruktur des Lobes und Dankes gehalten.
Im Markusevangelium hei�t es: "W�hrend des Mahls nahm er das Brot und sprach den <b>Lobpreis</b> � Dann nahm er den Kelch und sprach das <b>Dankgebet</b>. �" Kap 14,22, 23 Die Texte zeigen auch, dass er dieses letzte Mahl mit seiner Sendung verbunden hat. So hei�t es auch bei Markus: "Und er sagte zu ihnen: Das ist mein Blut, das Blut des Neues Bundes, das f�r viele vergossen wird. Amen ich sage euch: Ich werde nicht mehr von der Frucht des Weinstocks trinken bis zu dem Tag, an dem ich von Neuem davon trinken werde im Reich Gottes." Kap. 14, 24-25

Die Christen konnten an diese Gebetsstruktur ankn�pfen. Sie haben nicht nur die in der j�dischen Mahlfeier entwickelten Elemente �bernommen, sondern auch die Beziehungsstruktur des Gebetes. Der Priester richtet es an Gott Vater, ihm wird gedankt, ihm gegen�ber wird das Lob zum Ausdruck gebracht. Diese Beziehungsstruktur findet sich in den Gebeten, die das Johannesevangelium von der Abendmahlsfeier �berliefert. (s.u. bei Zitaten).
Der Priester dankt Gott f�r die Erl�sung durch Christus, die Gemeinde lobt Gott, vor allem im Sanctus, sie erinnert sich an den Tod Jesu, an seine Auferstehung und Himmelfahrt und erf�hrt, dass sie mit dem Heiligen Geist begabt ist. Dank, Lob und Erinnerung m�nden bereits in der j�dischen Mahlfeier in Bitten f�r das Volk und andere Anliegen. Diese Elemente finden sich in den Liturgien des Ostens wie des Westens. Das Hochgebet der r�mischen Kirche hat in der Regel folgenden Aufbau:

<blockquote>1.   Pr�fation
2.   Sanctusgesang
3.   Herabrufung des Heiligen Geistes auf Brot und Wein (Epiklese)
4.   Einsetzungsworte
5.   Erinnerung an Jesu Tod und Auferstehung (Anamnese)
6.   Gebet f�r die Kirche, um die Verbindung mit dem Papst und dem Ortsbischof auszudr�cken, werden der Papst und der Bischof der Di�zese mit ihrem Vornamen genannt.
7.   F�rbitten f�r Lebende und Verstorbene (Interncessiones)
8.   Feierlicher Abschlu� des Hochgebetes (Doxologie)</blockquote>

Diese Teile des Hochgebetes finden sich in einem liturgischen Text, der dem Priester Hippolyt zugeschrieben wird. Er hat die liturgischen Texte aufgeschrieben, die zu seiner Zeit, in der zweiten H�lfte des 3. Jahrhunderts, in der r�mischen Kirche in Gebrauch waren. Das heute in der katholischen Kirche benutzte II. Hochgebet geht auf diesen alten Text der r�mischen Kirche zur�ck. In seiner klaren Gliederung wird die oben beschriebene Grundstruktur erkennbar:

<i>Einladung zur Pr�fation:</i>
Priester:    Der Herr sei mit euch
Gemeinde:    Und mit deinem Geiste
Priester:    Erhebet die Herzen.
Gemeinde:    Wir haben sie beim Herrn
Priester:    Lasset uns danken dem Herrn, unserem Gott
Gemeinde:    Das ist w�rdig und recht

<i>Pr�fation</i>
In Wahrheit ist es w�rdig und recht,
dir, Herr, heiliger Vater, immer und �berall zu danken,
durch deinen geliebten Sohn Jesus Christus.
Er ist dein Wort, durch ihn hast du alles erschaffen.
Ihn hast du gesandt als unseren Erl�ser und Heiland.
Er ist Mensch geworden durch den Heiligen Geist,
geboren von der Jungfrau Maria.
Um deinen Ratschlu� zu erf�llen
und dir ein heiliges Volk zu erwerben,
hat er sterbend die Arme ausgebreitet
am Holze des Kreuzes.
Er hat die Macht des Todes gebrochen
und die Auferstehung kundgetan.
Darum preisen wir dich mit allen Engeln
und Heiligen und singen vereint
mit ihnen das Lob deiner Herrlichkeit:

<i>Sanctus</i>
Heilig, heilig, heilig
Gott, Herr aller M�chte und Gewalten.
Erf�llt sind Himmel und Erde von deiner Herrlichkeit.
Hosanna in der H�he.
Hochgelobt sei, der da kommt im Namen des Herrn.
Hosanna in der H�he.

<i>Epiklese</i>
Ja, du bist heilig, gro�er Gott,
du bist der Quell aller Heiligkeit.
Darum bitten wir dich:
Sende deinen Geist auf diese Gaben herab und heilige sie,
damit sie uns werden Leib und Blut deines Sohnes,
unseres Herrn Jesus Christus.

<i>Einsetzungsworte</i>
Denn am Abend, an dem er ausgeliefert wurde und sich aus freiem Willen
dem Leiden unterwarf,
nahm er das Brot und sagte Dank, brach es,
reichte es seinen J�ngern und sprach:

NEHMET UND ESSET ALLE DAVON:
DAS IST MEIN LEIB,
DER F�R EUCH HINGEGEBEN WIRD.

Ebenso nahm er nach dem Mahl den Kelch,
dankte wiederum, reichte ihn seinen J�ngern und sprach:

NEHMET UND TRINKET ALLE DARAUS:
DAS IST DER KELCH DES NEUEN UND EWIGEN BUNDES,
MEIN BLUT, DAS F�R EUCH UND F�R ALLE VERGOSSEN WIRD ZUR VERGEBUNG DER S�NDEN.
TUT DIES ZU MEINEM GED�CHTNIS.

Geheimnis des Glaubens.
<i>Akklamation der Gemeinde:</i>
Deinen Tod, o Herr, verk�nden wir,
und deine Auferstehung preisen wir,
bis du kommst in Herrlichkeit.

<i>Anamnese</i>
Darum, g�tiger Vater, feiern wir das Ged�chtnis des Todes und der Auferstehung deines Sohnes und bringen dir so das Brot des Lebens und den Kelch des Heiles dar.
Wir danken dir, da� du uns berufen hast,
vor dir zu stehen und dir zu dienen.
Wir bitten dich: Schenke uns Anteil an Christi Leib und Blut,
und la� uns eins werden durch den Heiligen Geist.

<i>Gebet f�r Papst, Bischof und die Kirche</i>
Gedenke deiner Kirche auf der ganzen Erde
und vollende dein Volk in der Liebe,
vereint mit unserem Papst N.,
unserem Bischof N. und allen Bisch�fen,
unseren Priestern und Diakonen
und mit allen, die zum Dienst in der Kirche bestellt sind.

<i>Gebet f�r die Verstorbenen</i>
Gedenke (aller) unsere Br�der und Schwestern,
die entschlafen sind in der Hoffnung, dass sie auferstehen.
Nimm sie und alle, die in deiner Gnade aus dieser Welt geschieden sind, in dein Reich auf, wo sie dich schauen von Angesicht zu Angesicht.

<i>Gebet f�r die Lebenden</i>
Vater, erbarme dich �ber uns alle, damit uns das ewige Leben zuteil wird in der Gemeinschaft mit der seligen Jungfrau und Gottesmutter Maria, mit deinen Aposteln und mit allen, die bei dir Gnade gefunden haben von Anbeginn der Welt, dass wir dich loben und preisen durch deinen Sohn Jesus Christus.

<i>Schlu�doxologie</i>
Durch ihn und mit ihm und in ihm ist dir,
Gott, allm�chtiger Vater,
in der Einheit des Heiligen Geistes
alle Herrlichkeit und Ehre jetzt und in Ewigkeit.
Amen.


<i><b>Zitate</b>
Von Johannes �berliefertes Gebet Jesu beim Letzten Abendmahl:
Und Jesus erhob seine Augen zum Himmel und sprach:
Vater, die Stunde ist da. Verherrliche deinen Sohn, damit der Sohn dich verherrlicht. Denn du hast ihm Macht �ber alle Menschen gegeben, damit er allen, die du ihm gegeben hast, ewiges Leben schenkt. Das ist das ewige Leben: dich, den einzigen wahren Gott, zu erkennen und Jesus Christus, den du gesandt hast. Ich habe dich auf Erden verherrlicht und das Werk zu Ende gef�hrt, das du mir aufgetragen hast. Vater, verherrliche mich jetzt bei dir mit der Herrlichkeit, die ich hatte, bevor die Welt war.
Kap. 17,1-5

Auf diesen Lobpreis folgt ein Gebet Jesu f�r die J�nger, das dem Gebet f�r die Kirche, die Gl�ubigen und die Verstorbenen in den verschiedenen Hochgebeten entspricht.</i>

<a href="http://www.kath.de/seiten/team.html#eb">Eckhard Bieger S.J.</a>

Hinweise zu den <a href="http://www.fernsehgottesdienst.de">Gottesdienst�bertragungen im ZDF</a>
