titel:Asperges
stichworte:Asperges, besprengen, Taufe, Ysop, kath
bild:bild.jpg

Dieses lateinische Wort f�r "besprengen" bezeichnet einen Ritus, der vor Beginn des Sonntagsgottesdienstes seinen Platz hat. Der Priester geht durch die Reihen und besprengt die Gl�ubigen mit Weihwasser. Der Ritus ist eine Erinnerung an die Taufe. Der Vers, der dabei von Chor und Gemeinde gesungen wird, hei�t �bersetzt: "Besprenge mich Herr mit Ysop und ich werde rein. Wasche mich und ich werde wei�er als Schnee." Dieser Vers steht in dem Bu�psalm 51,9