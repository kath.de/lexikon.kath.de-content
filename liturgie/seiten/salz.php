titel:Salz
stichworte:Salz, Weihe des Taufwassers, Glaube, kath
bild:bild.jpg

Das Salz ist nach einem Wort Jesu ein Bild f�r die Glaubens�berzeugung, die seine J�nger haben. "Ihr seid das Salz der Erde. Wenn das Salz seinen Geschmack verliert, womit kann man es wieder salzig machen? Es taugt zu nichts mehr; es wird weggeworfen und von den Leuten zertreten." (Matth�us 5,13) Salz geh�rt daher zur Weihe des Taufwassers.