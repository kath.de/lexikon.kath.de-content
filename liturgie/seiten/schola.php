titel:Schola
stichworte:Schola, Vorsängergruppe, Gregorianischer Choral, kath
bild:bild.jpg

Vorsängergruppe im <a href="http://www.kath.de/lexikon/liturgie/index.php?page=choral_gregorianisch.php">Gregorianischen Choral</a>. Das Wort kommt von Schola Cantorum, Singschule. Die Mitglieder Schola tragen beim Gottesdienst einen <a href="http://www.kath.de/lexikon/liturgie/index.php?page=talar.php">Talar</a> mit <a href="http://www.kath.de/lexikon/liturgie/index.php?page=rochett.php">Rochett</a>