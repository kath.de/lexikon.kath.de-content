titel:Klapper
stichworte:Klapper, kath
bild:bild.jpg

Am Gründonnerstag verstummen die Glocken und auch die von den Messdienern benutzten Schellen oder Klingeln. Diese werden bis zur Osternacht durch Holzklappern ersetzt.