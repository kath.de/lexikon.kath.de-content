titel:Quadragesima
stichworte:Quadragesima, vierzig, Fastenzeit, Ostern, kath
bild:bild.jpg

Der Name Quadragesima (f�r lateinisch vierzig) ist in Rom entstanden und bezeichnet die 40 Tage der Fastenzeit. Im griechischen Raum wird die Zeit auch als Tessarakoste bezeichnet. In den germanischen Sprachen wird das Moment des Fastens mehr �berbetont. Die Zahl 40 leitet sich von den 40 Tagen her, die Jesus fastend in der W�ste verbrachte. 40 Jahre wanderte Israel in der W�ste, vierzig Tage war Moses auf dem Sinai.
Die Erw�hnung dieser Zeit als Vorbereitungszeit f�r Ostern ist erstmals in einem Hirtenbrief des Athanasius von Alexandrien um 334 belegt. Ende des 4. Jahrhunderts war die Quadragesima allgemein eingef�hrt. Sie erhielt deshalb eine weitere Bedeutung, weil sie der Vorbereitung auf die Taufe gewidmet war. Die Taufe erfolgte in der Osternacht. Als sich die Kindertaufe durchsetzte, verlor die Fastenzeit ihren Charakter als Vorbereitung auf die Taufe.
Die Fastenzeit war 1. Jahrtausends dem Bussakrament gewidmet. Man konnte in diesen Jahrhunderten nur einmal im Leben das Sakrament empfangen. Die B��er nahmen am Beginn der Fastenzeit vom Bischof das Bu�gewand entgegen, waren von der Eucharistiefeier ausgeschlossen. Die Asche, die am Aschermittwoch an die Gl�ubigen ausgeteilt wird, wurde bis zum 11. Jahrhundert nur an die B��er ausgeteilt. Am Gr�ndonnerstag wurden die B��er wieder in die Gottesdienstgemeinschaft aufgenommen
Die Fastenzeit beginnt deshalb am Aschermittwoch, weil die Sonntage als Gedenktage f�r Ostern nicht mitgez�hlt werden. Sie endet am Karsamstag um 18 Uhr, denn der Vorabend geh�rt bereits zum Feiertag.
Seit dem II. Vatikanum spricht man von der �sterlichen Bu�zeit. In ihr werden zwei Themen wichtig: Die Tauferinnerung und die Bu�e und Umkehr. Im Laufe der drei Lesejahre spiegeln sich die drei Hauptthemen der Quadragesima: A Initiation (Taufe), B das Paschamysterium (Passion Jesu) und C Bu�e.
Die �sterliche Bu�zeit ist neben dem Advent eine Zeit f�r Exerzitien.
Die Farbe der Quadragesima ist Violett, so wie im Advent. Am 4. Sonntag der Fastenzeit beginnt das Eingangslied mit Laetare, freue dich, deshalb wird an diesem Sonntag ein rosafarbiges und kein violettes Messgewand getragen.
Eine Z�sur bildet der 5. Fastensonntag als Passionssonntag, wo die Kreuze und Bilder verh�llt werden. Die Kreuzesverh�llung ist ein verehrendes Vorgehen, denn was man liebt, das verh�llt man. Am Karfreitag wir das Kreuz enth�llt und an den Altarstufen den Gl�ubigen zur Verehrung durch einen Kuss entgegen gehaltne.

J�rgen Pelzer