titel:Kommunion
stichworte:Kommunion, Eucharestie, Messe, Einsetzung, Jesus, Gemeinschaft, kath
bild:bild.jpg

Kommunion hei�t, in eine Gemeinschaft eintreten. Das kann dadurch geschehen, dass man seine Gedanken austauscht und �bereinstimmung erkennt. Andere M�glichkeiten der Kommunion sind der gemeinsame Gesang oder das gemeinsame Gebet. Diesen Weg in eine gr��ere Gemeinschaft ist die Gottesdienstgemeinde bereits im ersten Teil der Messe gegangen. Im zweiten, im eucharistischen Teil der Messe, wird die Gemeinschaft auf eine Person hin intensiviert. Das erm�glicht das Mahl, die Grundgestalt der Eucharistie. Die Gl�ubigen sind von Jesus zu einem Mahl eingeladen, das er zu seinem Ged�chtnis eingesetzt hat. Wie bei anderen herausgehobenen M�hlern, z.B. einer Geburtstagsfeier, soll das Mahl die Verbundenheit mit der zentralen Person, dem Geburtstagskind vertiefen. Deshalb mu� man als Mitfeiernder nicht zahlen, sondern ist eingeladen. Der Einladende gibt von Seinem, um eine tiefere Gemeinschaft mit ihm zu erm�glichen. Anteil an sich selber geben, wollte Jesus mit dem letzten Mahl, dem Abendmahl, das er mit seinen J�ngern gefeiert hat. Das, was zu diesem Mahl aufgetragen wurde, hatte die Gruppe, die sich um den Prediger aus Nazareth gesammelt hatte, wohl aus Spenden erhalten. Deshalb ist das, was Jesus gibt, nicht das Essen selbst. Er kn�pft an den Anla� f�r das Mahl an, das Ged�chtnis der Juden an den Auszug aus �gypten. Pascha ist das Fest der Befreiung aus dem Frondienst des Pharao. Dieses Mahl hat bis heute als Hauptgang ein gebratenes Lamm. Jesus hat dieses Mahl neu akzentuiert. Er hat Brot und den Becher mit Wein zu seinem Zentrum gemacht. Deshalb stehen bis heute Brot und Wein im Zentrum der Eucharistie. An die Stelle des Lammes, eines der Opfertiere im j�dischen Kult, ist Jesus selbst getreten. Er ist das Lamm Gottes, das die Schuld hinweg nimmt. "Lamm" hatte der Vorl�ufer Jesu, Johannes der T�ufer, den Messias genannt. (Johannesevangelium 1,29)
Weil Jesus sein Leben hingegeben hat, ist das eucharistische Mahl unersch�pflich. Wer die gewandelten Gaben von Brot und Wein zu sich nimmt, tritt in eine tiefere Gemeinschaft mit dem Spender der Gaben ein. Jesus hat im Abendmahlssaal von sich gesagt, da� das Brot zu seinem Leib und der Wein zu seinem Blut wird. Was der Einladende bei einem Geburtstagsessen versucht, eine tiefere Gemeinschaft mit ihm als Person zu erschlie�en, das gelingt in der Eucharistiefeier in einer noch anderen Dimension, die dadurch gegeben ist, da� Jesus den Menschen mit seinem Vater, dem Sch�pfergott in eine tiefere Beziehung bringt.

Wie bei einem Geburtstags- oder Hochzeitsessen vertieft sich auch die Gemeinschaft der Feiernden untereinander - und so entsteht aus jeder Mahlfeier die kirchliche Gemeinschaft neu.

Die Bedeutung des Mahles hat sich bereits im <a href="index.php?page=hochgebet.php">Hochgebet</a> erschlossen, dem mittleren Teil der Eucharistiefeier, dem die Gabenbereitung vorausgegangen ist. Das meist vom Priester gesprochene Wort pr�gt die Gestalt des eucharistischen Mahles

Anders als bei einem Geburtstagsessen ist das eucharistische Mahl in hohem Ma�e stilisiert. Die Gl�ubigen empfangen das Brot in der Form einer kleinen Hostie, von dem Wein nur einen Schluck.

Aufbau des Kommunionteils der Messe
<blockquote>1.   Vater unser
2.   Embolismus
3.   Friedensgebet und Friedensgru�
4.   Brotbrechung mit Agnus-Dei-Gesang
5.   Pr�sentation der eucharistischen Gaben
6.   Kommuniongang
7.   Danklied
8.   Schlu�gebet</blockquote>

Zu 1: Der Kommunionteil der Messfeier wird durch das "<b>Vater unser</b>" eingeleitet, das von allen gebetet wird. Es dr�ckt die Bitte sowohl um das Kommen des Reiches Gottes aus wie um das t�gliche Brot und die t�glich neu notwendige Vergebung der pers�nlichen Schuld.

Zu 2: Die Bitten des "Vater unser" werden durch den sog. <b>Embolismus erweitert</b>.

Zu 3: Dann folgt ein Gebet um <b>Frieden</b>, das den Gedankengang, der der Messe insgesamt zugrunde liegt, weiterf�hrt: Das Heil, das Jesus geschenkt hat, m�ndet in einen Frieden und wird einem Friedensgru� ausgedr�ckt, der durch den Austausch des Friedensgru�es mit den Banknachbarn fortgesetzt wird.

Zu 4: Das <b>Brotbrechen</b> ist im Neuen Testament ein Wort, das das Ged�chtnismahl Jesu bezeichnet. Es leitet sich davon her, da� ein gr��eres St�ck Brot geteilt wurde. Diese Bedeutung ist heute kaum erlebbar, weil das Brot beim Gabengang bereits als kleine Hostien zum Altar gebracht wird oder bei den orthodoxen Kirchen bereits in kleine St�cke geschnitten ist. Im Bericht �ber das erste Abendmahl hei�t es: "W�hrend des Mahls nahm Jesus das Brot und sprach den Lobpreis; dann brach er das Brot, reichte es den J�ngern und sagte: Nehmt und e�t, das ist mein Leib." (Matth�us 26,26) Im Brechen des Brotes wird die Hinrichtung Jesu gesehen, sein Leib wurde durch die Gei�elung und die Kreuzigung "gebrochen". Zum Brotbrechen wird das Agnus die, Lamm Gottes gesungen und zwar dreimal.



Zu 5: <b>Pr�sentation der eucharistischen Gaben</b>
Die Gemeinde wird vom Priester mit folgenden Worten eingeladen:
"Seht das Lamm Gottes, das hinwegnimmt die S�nden der Welt". Der Satz ist dem Johannesevangelium entnommen (Kap. 1,29)
Zu 6: Die Gl�ubigen gehen nach vorne, um den Leib des Herrn, "die Kommunion" zu empfangen. Da es sich um eine Prozession handelt, geh�rt ein Wechselgesang zum Kommuniongang, der im Gregorianischen Choral "Communio" genannt wird.

Zu 7: <b>Danklied</b>
Wie es bei einer Geburtstagsfeier einen Dank an den Einladenden gibt, dankt auch die Gemeinde f�r die empfangende Gabe durch ein Lied.

Zu 8: <b>Schlussgebet</b>
Der Kommunionteil der Messe wird durch ein Gebet abgeschlossen, das im Lateinischen "Postcommunio" hei�t. Das Gebet beinhaltet Dank und leitet auf die Entlassung aus der Feier �ber, n�mlich dass die Gl�ubigen, das, was sie empfangen haben, im Alltag verwirklichen.

<i><b>Zitate:</b>

<b>Embolismus:</b>
Erl�se uns Herr, allm�chtiger Vater, von allem B�sen
Und gib Frieden in unseren Tagen. Komm uns zu Hilfe mit deinem Erbarmen und bewahre uns vor Verwirrung und S�nde,
damit wir voll Zuversicht das Kommen unseres Erl�sers Jesus Christus erwarten.
Denn dein ist das Reich und die Kraft und die Herrlichkeit in Ewigkeit. Amen

<b>Das Friedensgebet, wie es in der Osterzeit gebetet wird:</b>
Am Ostertag trat Jesus in die Mitte seiner J�nger und sprach den Friedensgru�.
Deshalb bitten wir:
Herr Jesu, du Sieger �ber S�nde und Tod,
schau nicht auf unsere S�nden,
sondern auf den Glauben deiner Kirche
und schenke ihr nach deinem Willen Einheit und Frieden.
Der Priester breitet die H�nde aus und ruft:
Der Friede des Herrn sei allezeit mit euch.
Gemeinde: Und mit deinem Geiste.
Priester oder Diakon: Gebt einander ein Zeichen des Friedens und der Vers�hnung.
Die Gl�ubigen tauschen den Friedensgru� aus.


<b>W�hrend des Brotbrechens wird ein dreifacher Ruf gesprochen oder gesungen, der in eine Friedensbitte m�ndet:</b>
Lamm Gottes, du nimmst hinweg die S�nden der Welt,
erbarme dich unser.
Lamm Gottes, du nimmst hinweg die S�nden der Welt,
erbarme dich unser
Lamm Gottes, du nimmst hinweg die S�nden der Welt,
gib uns deinen Frieden.

<b>Lateinisch:</b>
Agnus Dei, qui tollis peccata mundi, miserere nobis
Agnus Dei, qui tollis peccata mundi, miserere nobis
Agnus Dei, qui tollis peccata mundi, dona nobis pacem

<b>Schlu�gebet (Postcommunio) vom Pfingstsonntag</b>
Der Priester betet:
Lasset und beten:
Herr, unser Gott,
du hast deine Kirche mit himmlischen Gaben beschenkt.
Erhalte ihr deine Gnade,
damit die Kraft aus der H�he, der Heilige Geist,
in ihr weiterwirkt und die geistliche Speise sie n�hrt bis zur Vollendung.
Darum bitten wir durch Christus, unseren Herrn.
Gemeinde: Amen

<b>Schlu�gebet vom 8. Sonntag im Jahreskreis</b>
Barmherziger Gott,
du hast uns in diesem Mahl die Gabe des Heils geschenkt.
Dein Sakrament gebe uns Kraft in dieser Zeit
Und in der kommenden Welt das ewige Leben.
Darum bitten wir durch Christus, unseren Herrn.
Amen

<b>Schlu�gebet vom 16. Sonntag</b>
Barmherziger Gott, h�re unser Gebet.
Du hast uns im Sakrament das Brot des Himmels gegeben,
damit wir an Leib und Seele gesunden.
Gib, da� wir die Gewohnheiten des alten Menschen ablegen
Und als neue Menschen leben.
Darum bitten wir durch Christus, unsern Herrn.
Amen</i>

<a href="http://www.kath.de/seiten/team.html#eb">Eckhard Bieger S.J.</b>

Hinweise zu den <a href="http://www.fernsehgottesdienst.de">aktuellen Gottesdienst�bertragungen im ZDF</a>