titel:Lektor
stichworte:Lektor, Vorleser, Lesung, kath
bild:bild.jpg

In der Kirche ein Amt, fr�her mit einer eigenen Weihe verliehen, heute mit einer feierlichen Beauftragung zum Verlesen der biblischen Texte. Allerdings ist die Verlesung des Evangeliums die besondere Aufgabe des <a href="http://www.kath.de/lexikon/liturgie/index.php?page=diakon.php">Diakons</a> und, wenn kein Diakon anwesend ist, dem Priester vorbehalten.