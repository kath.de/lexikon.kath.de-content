titel:Ciborium auch Ziborium
stichworte:Ciborium, Ziborium, Kelch, Hostie, kath
bild:bild.jpg

In Kelchform oder auch ohne Stil genutztes Gef�� f�r die Aufbewahrung konsekrierter, d.h. in der Messe �briggebliebener Hostien, die im Tabernakel aufgehoben werden. Das Ciborium ist meist mit einem kleinen Stoffst�ck umkleidet, auch Velum, Segel genannt. In der Baukunst nennt man Ciborium auch die Kuppel �ber der Vierung. Das Wort leitet sich vom lateinischen "Trinkbecher" her