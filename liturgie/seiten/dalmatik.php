titel:Dalmatik
stichworte:Dalmatik, Messgewand, Diakon, kath
bild:bild.jpg

Das Messgewand, die <a href="http://www.kath.de/lexikon/liturgie/index.php?page=kasel.php">Kasel</a>, die der Diakon tr�gt, der Name leitet sich von Dalmatien her, weil die Wolle, aus der das Gewand gefertigt wurde, aus Dalmatien bezogen wurde. Das Messgewand des Diakons ist daran zu erkennen, dass seitlich zwei Streifen senkrecht von oben nach unten �ber die vordere wie die hintere Seite des Gewandes fallen.