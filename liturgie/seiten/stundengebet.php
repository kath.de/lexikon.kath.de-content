titel:Stundengebet, Stundenbuch
stichworte:Stundengebet ,Stundenbuch, Liturgie, Chor, Gebetszeiten, kath
bild:bild.jpg

Im Chor von Abteien und Stiften wird zu bestimmten Zeiten gemeinsam gebetet. Im Rhythmus des Tagesablaufs sind der Morgen, der Mittag, der Abend Gebetszeiten. In seinem Buch �ber das Gebet schl�gt der Theologe Origines (185-254), der in Alexandrien lehrte, das dreimalige Gebet vor. Diese Ordnung des Stundengebets ist seit der Liturgiereform nach dem II. Vatikanischen Konzil wieder �blich. �ber Jahrhunderte leitete sich die Ordnung des Stundengebets vom Vers 164 des 119 Psalms ab: "Siebenmal am Tag singe ich dein Lob wegen deiner gerechten Entscheide." Die Gebetszeiten sind lateinisch benannt und hier mit den Zeiten wiedergeben, die vor der Einf�hrung der Elektrizit�t �blich waren::
<blockquote>3 h Laudes,   Loblieder
6 h Prim von lateinisch f�r die erste Stunde, diese Zeit ist seit der Liturgiereform nach dem II. Vatikanum abgeschafft
9 h Terz, 3. Stunde
12 h  Sext, 6. Stunde
15 h  Non,  9. Stunde
18 h  Vesper - Abendgebet
21 h  Komplet - Nachtgebet
in der Nacht die Matutin, heute Lesehore genannt</blockquote>
Das Stundengebet st�tzt sich auf die 150 Psalmen des Alten Testaments, hat weiter Hymnen, kurze Lesungen, F�rbitten. In jeder Laudes hat das <a href="http://www.kath.de/lexikon/liturgie/index.php?page=benediktus.php">Benedictus</a>, das Gebet, das Zacharias, der Vater Johannes' des T�ufers bei dessen Geburt betete, seinen Platz. In jeder Vesper wird das <a href="http://www.kath.de/lexikon/liturgie/index.php?page=magnifikat.php">Magnifikat</a> gebetet, das Maria bei Ihrem Besuch bei Elisabeth, der Mutter des Johannes' betete.
Hymnen, Psalmen wie auch Benedictus und Magnifikat werden im Wechsel gebetet. Daher ist das <a href="http://www.kath.de/lexikon/liturgie/index.php?page=chorgestuehl.php">Chorgest�hl</a> nicht in Richtung Altar aufgestellt, sondern, meist in zwei Reihen, entlang den Seitenw�nden des Chorraums.
Damit die beiden Ch�re jeweils "mit einer Stimme" beten, wird das Chorgebet nach M�glichkeit gesungen. Daf�r gibt es einfache Tonfolgen, die eine Art Sprechgesang erm�glichen. Schwierige Melodien werden von einer <a href="http://www.kath.de/lexikon/liturgie/index.php?page=schola.php">Schola</a> vorgetragen.

<b><a href="http://www.magnificat-das-stundenbuch.de/de/Das-Stundengebet.html?utm_source=Onlinewerbung&utm_medium=Bannerwerbung&utm_campaign=kath_de_Stundengebet">Weitere Informationen zu diesem Thema finden Sie hier bei Magnificat.</a></b>