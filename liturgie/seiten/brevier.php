titel:Brevier
stichworte:Brevier, kurz, Stundengebet, Liturgie, kath
bild:bild.jpg

Das Brevier ist ein kurzes Buch (von lat. brevis = kurz) und enth�lt das Stundengebet der r�misch-katholischen Kirche. Es entstand aus dem Anliegen, die wichtigsten Texte in einer Textvorlage zu konzentrieren. Denn bis dahin war es �blich, dass man bis zu 12 B�cher brauchte um einen Gottesdienst zu feiern, was zur Folge hatte, dass nur gro�e Kirchen sich diesen Aufwand leisten konnten. Hintergrund war, dass die Stundenliturgie sich im Mittelalter sehr ausgeweitet hatte. Das <a href="index.php?page=stundengebet.php">Stundengebet</a> besteht aus 7 �ber den Tag verteilten Gebetszeiten, f�r die das Brevier die Texte f�r den jeweiligen Tag enth�lt: Die Lesehore, Laudes, Terz, Sext, Non, Vesper und Komplet.
Die Ausgabe von 1970 unter Papst Paul VI. wurde im Zuge der liturgischen Erneuerung des II. Vatikanischen Konzils eingef�hrt. Zusammengesetzt ist dieses Brevier besteht ncith mehr wie das davor aus einem Band, sondern aus 16 Lektionaren. Diese Lektionare enthalten die Lesungen f�r die Lesehore. Jeweils 8 Lektionare werden pro Jahr verwendet, so dass sich f�r jedes Lektionar ein zweij�hriger Rhythmus der Verwendung ergibt.
Der zweite Bestandteil des Breviers bilden die 3-b�ndigen Stundenb�cher: Band 1 enth�lt die Texte f�r die Advents und Weihnachtszeit, Band 2 enth�lt die Texte f�r die Fasten- und Osterzeit und Band 3 enth�lt die Texte f�r die Zeit im Jahreskreis.

Ausgaben:
1568 Pius V
1970 Papst Paul VI
Das Wort Brevier wird auch synonym verwendet f�r das Stundengebet selbst, welches auch als Offizium oder liturgia horarum bezeichnet wird.

J�rgen Pelzer