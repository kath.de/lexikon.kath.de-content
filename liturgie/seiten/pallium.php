titel:Pallium
stichworte:Pallium, Erzbischof, kath
bild:bild.jpg

Ein Stoffband, das von Erzbisch�fen um den Hals �ber dem Messgewand getragen wird. Die Wolle f�r die Pallien der neu ernannten Erzbisch�fe wird am 21. Januar, am Gedenktag der hl. Agnes, vom Papst gesegnet und am Fest der heiligen Petrus und Paulus am 29.Juni verliehen. Das Pallium war im r�mischen Reich ein Rangabzeichen. Seine urspr�ngliche Form ist ein Stoffsteifen, der um den Hals gelegt wird und dessen Ende an der linken Seite herabh�ngt. Diese Form ist in der Ostkirche in Gebrauch geblieben, im Mittelalter r�ckte der herabh�ngende Stoffstreifen in die Mitte, so dass das Pallium von vorne wie ein Ypsilon aussieht. Papst Benedikt XVI. tr�gt das Pallium wieder in der klassischen Form.