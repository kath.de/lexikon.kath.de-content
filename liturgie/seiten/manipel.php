titel:Manipel
stichworte:Manipel, Schwei�tuch, kath
bild:bild.jpg

Urspr�nglich das Tuch, das in den �rmel gesteckt wurde, um den Schwei� abzuwischen, wurde es sp�ter stilisiert und vom Priester �ber das linke Handgelenk gezogen. Nach der Liturgiereform 1969 wird es kaum noch getragen. Im tridentinischen Ritus geh�rt es zu den liturgischen Gew�ndern und ist wie die Stola farblich an das Messgewand angepasst. Auch der Diakon trug ein Manipel.