titel:Hore
stichworte:Hore, Stundengebet, kath
bild:bild.jpg

Das Wort kommt vom lateinischen hora - Stunde. Zu bestimmten Stunden versammeln sich M�nche und Kleriker zum <a href="http://www.kath.de/lexikon/liturgie/index.php?page=stundengebet.php">Stundengebet</a> im Chor der Kirche. Einige der Gebete werden entsprechend der Tageszeit bezeichnet, so Terz, Sext, Non, die zur dritten, sechsten, neunten Stunde der antiken Zeitrechnung gebetet werden, n�mlich um 9h, 12h und 15h