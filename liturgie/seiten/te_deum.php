titel:Te Deum
stichworte:Te Deum, Lobgesng, Ambrosius, kath
bild:bild.jpg

Te Deum, lat. f�r: Dich, Gott, sind die Anfangsbuchstaben eines Textes, der als "ambrosianischer Lobgesang" bekannt ist. Er wird dem Ambrosius von Mailand zugeschrieben. Der tats�chliche Autor ist aber unbekannt.
Die deutsche �bersetzung stammt von Romano Guardini.

Es wird an kirchlichen Festtagen gesungen aber auch bei anderen festlichen Anl�ssen, besonders wen sie Anlass zur Danksagung geben. Die Melodie des Te Deums bildet auch die Grundlage f�r die Lieder "Gro�er Gott, wir loben dich" und "Herr Gott, Dich loben wir".
Im Stundengebet schlie�t an Festen und Hochfesten das Te Deum die <--a href="index.php?page=lesehore.php"-->Lesehore<--/a--> ab.

Mehrstimmige Fassungen des Te Deums stammen aus dem 13. Jahrhundert. Sie wurden abwechselnd von Gemeinde und Chor gesungen. Ab dem 18 Jahrhundert gibt es dann auch Variationen im konzertanten Stil.

Dich, Gott, loben wir, dich, Herr, preisen wir.
Dir, dem ewigen Vater, huldigt das Erdenrund.

<b>Das Te Deum</b>
Dir rufen die Engel alle,
dir Himmel und M�chte insgesamt,
die Cherubim dir und die Seraphim
mit niemals endender Stimme zu:
Heilig, heilig, heilig
der Herr, der Gott der Scharen!
Voll sind Himmel und Erde
von deiner hohen Herrlichkeit.

Dich preist der glorreiche Chor der Apostel;
dich der Propheten lobw�rdige Zahl;
dich der M�rtyrer leuchtendes Heer;
dich preist �ber das Erdenrund
die heilige Kirche;
dich, den Vater unerme�barer Majest�t;
deinen wahren und einzigen Sohn;
und den Heiligen F�rsprecher Geist.

Du K�nig der Herrlichkeit, Christus.
Du bist des Vaters allewiger Sohn.
Du hast der Jungfrau Scho� nicht verschm�ht,
bist Mensch geworden,
den Menschen zu befreien.
Du hast bezwungen des Todes Stachel
und denen, die glauben,
die Reiche der Himmel aufgetan.
Du sitzest zur Rechten Gottes
in deines Vaters Herrlichkeit.
Als Richter, so glauben wir,
kehrst du einst wieder.

Dich bitten wir denn,
komm deinen Dienern zu Hilfe,
die du erl�st mit kostbarem Blut.
In der ewigen Herrlichkeit
z�hle uns deinen Heiligen zu.
Rette dein Volk, o Herr, und segne dein Erbe;
und f�hre sie und erhebe sie bis in Ewigkeit.

An jedem Tag benedeien wir dich
und loben in Ewigkeit deinen Namen,
ja in der ewigen Ewigkeit.
In Gnaden wollest du, Herr,
an diesem Tag uns ohne Schuld bewahren.
Erbarme dich unser, o Herr, erbarme dich unser.
Lass �ber uns dein Erbarmen geschehn,
wie wir gehofft auf dich.

Auf dich, o Herr,
habe ich meine Hoffnung gesetzt.
In Ewigkeit werde ich nicht zuschanden.

J�rgen Pelzer