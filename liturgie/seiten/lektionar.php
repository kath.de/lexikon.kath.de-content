titel:Lektionar
stichworte:Lektionar, Lesung, Evangelium, kath
bild:lektionar.jpg

Vom lateinischen Wort f�r Lesen abgleitet ist es das Buch, aus dem die Lesungen und Evangelientexte w�hrend des Gottesdienstes vorgetragen erden. Das Lektionar wird vom Diakon beim Einzug zum Gottesdienst  hereingetragen, der das Buch mit beiden H�nden hoch h�lt. Wenn in einem Lektionar nur die Evangelientexte enthalten sind, wird das Buch <a href="http://www.kath.de/lexikon/liturgie/index.php?page=evangeliar.php">Evangeliar</a> genannt.