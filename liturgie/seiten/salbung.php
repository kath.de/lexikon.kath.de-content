titel:Salbung
stichworte:Salbung, Sakrament, Weihe, Firmung, kath
bild:bild.jpg

Mit geweihten �len werden der T�ufling, der Firmling, der f�r ein Amt zu Weihende wie auch der Altar und die Glocken gesalbt. Siehe auch <a href="http://www.kath.de/lexikon/liturgie/index.php?page=chrisam.php">Chrisam</a>