titel:Benedictus
stichworte:Benedictus, Lobgesang, Zacharias, Hymnus, kath
bild:bild.jpg

Der Lobgesang des Zacharias, den er als Vater Johannes des T�ufers bei dessen Geburt angestimmt hat, beginnt mit dem Wort "Gepriesen sei der Herr", lateinisch benedicere. Das Benedictus wird bei jeder Laudes gesungen bzw. gebetet. Der Hymnus ist bei Lukas in Kap. 1, 67-79 �berliefert
Sein Vater Zacharias wurde vom Heiligen Geist erf�llt und begann prophetisch zu reden:
Gepriesen sei der Herr, der Gott Israels! Denn er hat sein Volk besucht und ihm Erl�sung geschaffen; hat uns einen starken Retter erweckt im Hause seines Knechtes David.
So hat er verhei�en von alters her durch den Mund seiner heiligen Propheten.
Er hat uns errettet vor unseren Feinden und aus der Hand aller, die uns hassen;
er hat das Erbarmen mit den V�tern an uns vollendet und an seinen heiligen Bund gedacht,
an den Eid, den er unserm Vater Abraham geschworen hat; er hat uns geschenkt, dass wir, aus Feindeshand befreit, ihm furchtlos dienen in Heiligkeit und Gerechtigkeit vor seinem Angesicht all unsre Tage.
Und du, Kind, wirst Prophet des H�chsten hei�en; denn du wirst dem Herrn vorangehen und ihm den Weg bereiten.
Du wirst sein Volk mit der Erfahrung des Heils beschenken in der Vergebung der S�nden.
Durch die barmherzige Liebe unseres Gottes wird uns besuchen das aufstrahlende Licht aus der H�he, um allen zu leuchten, die in Finsternis sitzen und im Schatten des Todes, / und unsre Schritte zu lenken auf den Weg des Friedens.