titel:Psalter
stichworte:Psalter, Stundengebet, Psalm, kath
bild:bild.jpg

Das Alte Testament als Sammlung mehrer B�cher, so der f�nf B�cher Moses, der B�cher der Propheten. Es enth�lt auch das Buch der 150 Psalmen. Diese Gebete sind die Basis des <a href="http://www.kath.de/lexikon/liturgie/index.php?page=stundengebet.php">Stundengebets</a>. Der Psalter oder ein Psalterium, das in der Kirche gebraucht wird, enth�lt die Psalmen sowie Hymnen und andere Texte entsprechend der Ordnung des Stundengebets.