titel:Knien, Kniebeuge
stichworte:Knien, Kniebeuge, Ehrerbietung, kath
bild:bild.jpg

Vor jemanden hinknien ist Ausdruck der Ehrerbietung. Im christlichen Kult gilt die Kniebeuge nur Gott. Man kniet auch, um etwas zu empfangen, so die Hostie oder bei einer Weihe die Handauflegung des Bischofs. Beim Betreten der Kirche wird der im Tabernakel in der Gestalt des Brotes anwesende Christus durch eine Kniebeuge geehrt.