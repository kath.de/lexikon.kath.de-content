titel:Chormantel
stichworte:Chormantel, Prozession, Andacht, Bischof, Priester, kath
bild:bild.jpg

W�hrend die <a href="http://www.kath.de/lexikon/liturgie/index.php?page=kasel.php">Kasel</a> bei der Messe getragen wird, ist der Chormantel, fr�her auch "Vespermantel" oder Rauchmantel, (lat. pluviale "Regengewand") f�r Prozessionen und den Gang zum Friedhof entwickelt. Bei feierlichen Andachten tr�gt der Priester oder Bischof ebenfalls einen Chormantel. W�hren die Kasel so geschnitten ist, das die Arme frei beweglich sind, umschlie�t der Chormantel wie ein �berwurf die Arme und muss daher vorne offen sein.