titel:Osterwasser
stichworte:Osterwasser, Osternacht, Taufe, kath
bild:osterwasser.jpg

In der fr�hen Kirche wurden die Katechumenen, die Menschen, die Christen werden wollten, in der Fastenzeit auf die Taufe vorbereitet und in der Osternacht getauft. Die Taufe und damit das Wasser pr�gen die �sterliche Liturgie. Das Osterwasser wird in der Ostnachtfeier durch Eintauchen der Osterkerze geweiht und f�r die Taufe verwendet. Von den Gl�ubigen wird es als Segenszeichen mit nach Hause genommen.