titel:Laudes
stichworte:Laudes, Gebetszeit, Stundengebet, Liturgie, kath
bild:bild.jpg

Die Laudes ist ein Teil des <a href="index.php?page=stundengebet.php">Stundengebets</a>. Der Begriff stammt vom lateinischen laus und bedeutet Lob (Laudes ist der Plural von laus und bedeutet damit "Loblieder"). Die Laudes sind das Morgengebet der Kirche. Mit dem neuen Morgen wird die Auferstehung gepriesen, denn so, wie die aufgehende Sonne die Nacht vertreibt, ist Christus aus der Nacht des Todes auferstanden und hat den Tod so besiegt. Und wie der Mensch nach seiner Auferstehung in den himmlischen Lobpreis einstimmt, begr��en die Gl�ubigen den neuen Tag.

<b>Aufbau</b>
<blockquote>Er�ffnung
Hymnus
Psalm
Canticum
Psalm
Kurzlesung
Responsorium
Benedictus
F�rbitten
Vater unser
Oration
Segen
Entlassung</blockquote>

In der Regel des hl. Benedikt sind die Laudes um 03.00 Uhr angesetzt. Sie geh�rt zu den gro�en <a href"index.php?page=hore.php">Horen</a> und wird auch Morgenhore genannt. Neben der <a href="index.php?page=vesper.php">Vesper</a> ist dieses Morgengebet die wichtigste Stundegebetszeit.

Laudes wie andere Teile des Stundengebetes haben eine charakteristische Er�ffnung: Der Vorbeter/ Priester beginnt:
<blockquote>V (Vorbeter): O Gott, komm mir zu Hilfe
Gemeinde: Herr, eile mir zu helfen
V: Ehre sei dem Vater und dem Sohne und des Heiligen Geiste.
G: Wie es war im Anfang, so auch jetzt und alle Zeit und in Ewigkeit Amen. Halleluja.</blockquote>
Wenn die Laudes die erste Hore sind, wird Anstelle der Er�ffnung das <a href="index.php?page=invitorium.php">Invitorium</a> gebetet.

Die Hymnen werden aus der christlichen Tradition ausgew�hlt und geben diesem Morgengebet vor allem an Festtagen eine eigene F�rbung.
Aus dem Buch der Psalmen werden jeweils zwei Psalmen ausgew�hlt, aus den psalm�hnlichen Ges�ngen der anderen B�chern des Alten Testaments ein Canticum, ein Lied. Gerahmt werden die Psalmen und das Canticum durch <a href="index.php?page=antiphon.php">Antiphoneny</a>, die Den Grundgedanken der Ges�nge aufgreifen und miteinander verbinden.
Die Lesung ist kurz, einige Verse aus der Briefliteratur des Neuen Testaments. Sie wird mit dem <a href="index.php?page=responsorium.php">Responsorium</a>, einem Antwortgesang beantwortet. Mit ihm wird die zentrale Aussage der Kurzlesung aufgegriffen und verinnerlicht. Es hat eine charakristische Form:
<blockquote>Doppelvers G. Wiederholung des Doppelverses
V: Halbvers G: zweite H�lfte des Doppelverses vom Anfang
V: Ehre sei dem � G:Doppelvers vom Anfang</blockquote>
Das <a href=index.php?page=benedictus.php>Benedictus</a> ist ein Charakteristikum der Laudes, es ist der Gesang des Zacharias bei der Geburt seines Sohnes Johannes, dem sp�teren T�ufer, den Lukas in Kap.1, 68-79 �berliefert. Auch dieser Gesang wird durch eine Antiphon eingerahmt. Diese greift das Grundthema des Tages auf.
Die F�rbitten haben den gleichen Aufbau wie in der <a href="index.php?page=eucharestie.php">Eucharistiefeier</a>. W�hrend die F�rbitten der Vesper Anliegen von Staat und Kirche zum Thema haben, bitten die Beter in den Laudes um Beistand f�r sich und seine Mitmenschen. Das Bittgebet folgt der Aufforderung des Jakobusbriefes: " Betet f�reinander, damit ihr geheiligt werdet. Viel vermag das inst�ndige Gebet eines Gerechten" (Jak 5,16). Es handelt sich bei den Bitten bzw. F�rbitten um Anregungen, die die Betenden durch eigene Anliegen erg�nzen k�nnen. Die Bitten und F�rbitten m�nden in das Gebet des Herrn, das Vater unser, das gro�e Schlussgebet von Morgen- und Abendlob. Es ist das Gebet, das Jesus seine J�nger gelehrt hat, in dem er die Betenden hineinnimmt in sein Beten zum Vater. Das Vaterunser ist die Urgestalt jeglichen christlichen Betens, das alle christlichen Konfessionen verbindet. Anders als in der Eucharestiefeier schlie�t das Vater Unser nicht mit der <a href="index.php?page=doxologie.php">Doxologie</a> "Denn dein ist das Reich�", sondern es folgt unmittelbar die Oration, die aus dem Messbuch �bernommen wird und identisch mit dem Tagesgebet der Eucharistiefeier ist. Ein Segenswort m�ndet in die Entlassung: Gehet in Frieden   Wenn den Laudes nicht ein Priester und auch kein Diakon vorsteht, wird das Morgengebet mit folgenden Worten abgeschlossen: V: Der Herr segne uns, er bewahre uns vor Unheil und f�hre uns zum ewigen Leben. G: Amen.


<i><b>Zitate</b>
II. Vatikanisches Konzil:
Die Laudes als Morgengebet und die Vesper als Abendgebet, nach der ehrw�rdigen �berlieferung der Gesamtkirche die beiden Angelpunkte des t�glichen Stundengebetes, sollen als die vornehmsten Gebetsstunden angesehen und als solche gefeiert werden. (Sacrosanctum Concilium, 89a)</i>