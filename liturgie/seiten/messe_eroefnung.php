titel:Die Er�ffnung der Messe
stichworte:Messe, Einzug, Verehrung des Altars, Eingangsgru�, Bu�akt, Kyrie, Gloria, Tagesgebet, kath
bild:bild.jpg

<b>Einzug:</b>
Bis zum 5. Jahrhundert begann die Messe mit einem stillen Gebet der Kleriker, die sich dazu vor den Altar niederwarfen. Dies findet sich heute noch in der Karfreitagsliturgie. Der Fachausdruck daf�r ist die Proskynese. Durch dieses stille Gebet wurde der Einzug abgeschlossen. Seit Gregor dem Gro�en werden dabei Psalmen bzw. Lieder gesungen. Die Gestalt des heute �blichen Einzugs entwickelte sich im Mittelalter: Wenn der Papst feierlich in eine Basilika einzog, wurden ihm sieben Leuchter und Weihrauch vorweggetragen.
Der Einzug muss dabei vom Altar aus gesehen werden: Das ganze Volk geht auf den Altar, also auf Gott zu. Die Gemeinde beteiligt sich durch den Gesang am Einzug der Altargruppe. Die Mitwirkenden am Gottesdienst sollen mit dem Priester einziehen, ihnen wird ein Kreuz vorangetragen.

<b>Verehrung des Altars:</b>
Der Priester k�sst den Altar nach dem Einzug. Die in diesem Vorbereitungsritus gezeigte Verehrung gilt Christus, der durch den Altar symbolisiert wird. Noch im 13. Jahrhundert war es �blich, den Altar 21 mal zu k�ssen. Dabei k�nnen der Altar und das Kreuz auch mit Weihrauch inzensiert werden. Das meint, dass der Priester diese mit dem Weihrauchfa� ber�uchert. Dies findet sich seit dem 11. Jahrhundert. Die Verwendung von Weihrauch im Gottesdienst ist fakultativ, so dass es mancherorts zur Abschaffung kam, anderenorts zu einer fast t�glichen Verwendung von Weihrauch im Gottesdienst.

<b>Eingangsgru�</b>
Die Messe beginnt mit den Worten "Im Namen des Vaters, des Sohnes und des heiligen Geistes". Das dabei von dem Priester und den Gl�ubigen gemachte Kreuzzeichen ist seit dem 14. Jahrhundert belegt. Danach spricht der Priester "Der Herr Sei mit euch" (lat. Dominus vobiscum). Diese Formel ist eine Zitation aus Rut 2,4. Sie war bei den Juden eine g�ngige Gru�formel. Wenn der Priester dabei die Arme ausbreitet, soll das eine Umarmung andeuten.
Die Gemeinde antwortet "Und mit deinem Geiste" (lat. Et cum spiritu tuo). Dies k�nnte den Sinn von "Und auch mit dir" haben. Wahrscheinlicher ist jedoch, dass mit dem Wort Geist hier die Gabe des Heiligen Geistes gemeint, die der Priester als Amtstr�ger in besonderer Weise braucht. Dementsprechend geht es nicht um die Kopie eines allt�glichen Begr��ungsrituals, sondern um eine erste Hinf�hrung in das Mysterium, das in der Messe gefeiert werden soll.

<b>Bu�akt - auch: Allgemeines Schuldbekenntnis</b>
Wer sich Gott n�hert, wird sich bewusst, dass er daf�r nicht vorbereitet ist, Fehlhaltungen, Nachl�ssigkeiten und bewu�te Verletzung anderer Menschen stehen der Begegnung im Wege. Im Schuldbekenntnis stehen die Feiernden zu ihren Fehlern und lassen sich vom Priester die von Gott gew�hrte Vers�hnung zusprechen. Der Bu�akt kann bei Messen von besonderer Festlichkeit entfallen.

<b>Kyrie</b>
Im Kyrie-Ruf (griechisch f�r Herr) begr��t die Gemeinde Christus als Retter.
Kyrie eleison / Christe eleison - Herr errette uns / Christus errette uns lautet der Kyrie Ruf. Diesen Ruf haben die Christen aus ihrer Umwelt �bernommen. Er galt als Huldigung einem Gott oder einen Herrscher. Zugleich ist der Kyrios Ruf eine der k�rzesten Glaubensformeln. Christus ist der auferstandene Herr (s. der Hymnus im Philipperbrief 2,6-11). Kyrios geht zur�ck auf den alttestamentlichen Gottestitel Adonai, mit dem der unausgesprochene Gottesname JHWH in den Texten des Alten Testamentes umschrieben wurde. Um 500 �bernahm man in Rom die Kyrielitanei aus dem Osten. Dabei wurden die Anliegen der Gemeinde von dem Diakon vorgetragen und die Gemeinde antwortete mit dem Kyrie eleison. Unter Papst Gregor I. (+ 604) wurden die Anliegen dann weggelassen. Der nun �brig gebliebene Kyrie Ruf wurde im Laufe der Zeit noch um das Christe Eleison erweitert. Bis zur Liturgiereform des II. Vatikanischen Konzils gab es neun Kyrierufe.
Urspr�nglich war die Zahl der Kyrierufe nicht begrenzt, es wurde n�mlich so lange gesungen, bis der Papst an seinem Sitz angelangt war. Dann gab er den S�ngern ein Zeichen. Der Neunzahl begegnen wir zum ersten Mal in Gallien. Im Mittelalter werden die Kyrierufe dann zunehmend trinitarisch gedeutet, d.h. das erste Kyrie gilt Gott Vater, das Christe eleison dem Sohn, das dritte Kyrie dem Heiligen Geist.

<b>Gloria</b>
Das Gloria ist einer der wenigen �berlieferten Hymnen mit griechischem Ursprung. Durch das Konzil von Laodiz�a (zwischen 341 und 380) wurden die meisten Hymnen verboten, weil sie man in ihnen h�retisches Gedankengut sah. Das Gloria muss damals schon so angesehen gewesen sein, dass es nicht diesem Verbot zum Opfer fiel. Gloria meint �bersetzt soviel wie Ruhm, Glanz, Herrlichkeit (hebr�isch. kabod griechisch Doxa)
Es ist somit ein Lobgesang auf die Herrlichkeit Gottes. Anf�nglich war es kein Bestandteil der Messe, wurde aber schon sehr fr�h in die Mitternachtsmesse von Weihnachten aufgenommen. Seit dem 11. Jahrhundert wird es dann an allen Festtagen und Sonntagen gebetet. Text s.u.

<b>Tagesgebet</b>
Auf lateinisch hei�t Gebet collecta. Es schlie�t den Er�ffnungsteil der Messe ab. Collectare meint sammeln, ein Gebet, das der Priester �ber die versammelte Gemeinde ausspricht. Zum anderen bedeutet es auch ein Gebet, in dem der Priester die Anliegen der Gemeinde sammelt und zusammenfa�t. Begonnen wird das Tagesgebet mit dem lateinischen Oremus (Beten wir bzw. lasset uns beten). Es folgt ein kurzer Moment des Schweigens, um sich zu sammeln.
Das Gebet selbst singt der Priester in der althergebrachten Gebetshaltung: Mit erhobenen H�nden und aufrecht stehend. Dieses Gebet hei�t auch Oration.
Das Gebet richtet der Priester an Gott Vater, in der Schlu�formel werden immer die drei Personen der Trinit�t, Vater, Sohn und Heiliger Geist erw�hnt.
Das Tagesgebet wechselt je nach Fest- oder Wochentag bzw. Heiligengedenktag, ebenso wie das Gebet am Ende der Gabenbereitung und das nach der Kommunionausteilung. Diese drei Gebete werden auch Gebete des Vorstehers genannt, weil der Priester als Vorsteher sie im Namen der Gemeinde an Gott richtet.
J�rgen Pelzer

<i><b>Das Tagesgebet vom 1. Adventssonntag</b>
Lasset uns beten:
Her, unser Gott,
alles steht in deiner Macht;
du schenkst das Wollen und das Vollbringen.
Hilf uns, da� wir auf dem Weg der Gerechtigkeit Christus entgegengehen
und uns durch Taten der Liebe auf seine Ankunft vorbereiten,
damit wir den Platz zu seiner Rechten erhalten,
wenn er wiederkommt in Herrlichkeit.
Er, der in der Einheit des Heiligen Geistes
Mit dir lebt und herrscht in Ewigkeit.
Amen

<b>Das Gloria
Lateinisch:</b>
Gloria in excelsis Deo et in terra pax hominibus bonae voluntatis.
Laudamus te, benedicimus te, adoramus te, glorificamus te,
gratias agimus tibi propter magnam gloriam tuam,
Domine Deus, Rex caelestis Deus Pater omnipotens,
Domine Fili unigenite, Iesu Christe, Domine Deus, Agnus Dei,
Filius Patris, qui tollis peccata mundi, miserere nobis;
qui tollis peccata mundi,
suscipe deprecationem nostram.
Qui sedes ad dexteram Patris, miserere nobis.
Quoniam tu solus Sanctus, tu solus Dominus, tu solus Altissimus,
Iesu Christe, cum Sancto Spiritu: in gloria Dei Patris. Amen.

<b>Deutsch:</b>
Ehre sei Gott in der H�he und Friede auf Erden den Menschen seiner Gnade.
Wir loben dich, wir preisen dich, wir beten dich an, wir r�hmen dich und danken dir,
denn gro� ist deine Herrlichkeit:
Herr und Gott, K�nig des Himmels, Gott und Vater, Herrscher �ber das All,
Herr, eingeborener Sohn, Jesus Christus.
Herr und Gott, Lamm Gottes, Sohn des Vaters, du nimmst hinweg die S�nde der Welt: erbarme dich unser;
du nimmst hinweg die S�nde der Welt: nimm an unser Gebet;
du sitzest zur Rechten des Vaters: erbarme dich unser.
Denn du allein bist der Heilige, du allein der Herr, du allein der H�chste, Jesus Christus, mit dem Heiligen Geist, zur Ehre Gottes des Vaters. Amen.</i>

Hinweise zu den <a href="http://www.fernsehgottesdienst.de">aktuellen Gottesdienst�bertragungen im ZDF</a>