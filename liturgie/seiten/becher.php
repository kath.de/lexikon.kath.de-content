titel:Becher
stichworte:Becher, Kelch, Passah, kath
bild:bild.jpg

Beim j�dischen Passahfest, aus dem das Abendmahl hervorgegangen ist, wird Wein in einem Becher herumgereicht worden. Alle tranken aus dem gleichen Becher so wie heute aus dem Kelch. Der Becher wird n�mlich im Gottesdienst meist mit einem Stil als <a href="index.php?page=kelch.php">Kelch</a> benutzt. Becher kann auch im �bertragnen Sinne gebraucht werden, wenn z.B. ein "Becher des Zornes" ausgegossen wird.