titel:Benediktionale
stichworte:Benediktionale, kath
bild:bild.jpg

Segnen ist die Konkretisierung des Segens, den Gott den Menschen zuspricht. Was gesegnet wird, wird dem besonderen Schutz Gottes anvertraut. Es �soll Segen auf den Personen, den Geb�uden, den Gegenst�nden liegen.�
Gesegnet und geweiht werden erst einmal R�ume und liturgische Gegenst�nde und Gew�nder, um diese herauszuheben und f�r die Nutzung in Gottesdiensten vorzubehalten. Jedoch ist der Segen nicht nur f�r Personen, R�ume und Gegenst�nde vorgesehen, die mit dem Gottesdienst zu tun haben. Der Segen umfasst die ganze Welt des Menschen und reicht bis in Fabriken und Feuerwehrwagen.

F�r den Segen, �benedicere�, eigentlich gibt es ein kirchliches Buch, das Benediktionale, das f�r die verschiedenen Segnungen jeweils Vorlagen zur Verf�gung stellt.

Eine Segnung ist wie ein Gottesdienst aufgebaut.
-	nach der Begr��ung folgt
-	ein Text aus der Schrift, der
-	ausgelegt wird.
-	Der eigentliche Segen wird durch Lobpreis und Wechselgebet eingeleitet.
-	Der Segen selbst wird als Bitte formuliert.
-	Den Abschluss bilden F�rbitten und ein
-	allgemeiner Segen

Gesegnet wird in der Form des Kreuzzeichens
Weihwasser und Weihrauch verdeutlichen zeichenhaft den Segen.  
Wenn ein Mensch gesegnet wird, legt der Priester oder ein anderer diesem die H�nde auf.

Segnungen sind f�r folgende Bereiche vorgesehen

Segnungen zu Festen und herausgehobenen Zeiten
Segnung des Adventskranzes
Kindersegen an Weihnachten
Segnung des Johannisweins am 27.12.
Segnungen der Sternsinger 
Blasiussegen,
Speisesegnung an Ostern
Wettersegen
Kr�utersegen an Mari Himmelfahrt
Segnung der Ernte
Segnung der Gr�ber an Allerheiligen, Allerseelen
Kinder- und Lichtersegnung am Martinsfest
Brotsegnung an einzelnen Heiligenfesten
Feuersegnung am Johannistag oder an anderen Festen

Segnungen f�r Personen
Muttersegen vor und nach der Geburt
Kindersegnung
Segnung der Schulanf�nger
Krankensegnung
Segnung alter Menschen
Segnungen stellvertretend f�r den Papst
Primizsegen
Segen zur silbernen und goldenen Hochzeit
Pilgersegen
Reisesegen

Segnung von R�umen und Gegenst�nden f�r den Gottesdienst
Weihe einer Kapelle und eines Altares
Weihe eines Kreuzes
Segnung beim Glockenguss
Glockenweihe
Orgelweihe
Weihe einer Hostienschale oder eines Kelches
Weihe von Gew�ndern, die f�r den Gottesdienst bestimmt sind
Segnung eines Grundsteins f�r ein kirchliches Geb�ude
Segnung eines kirchlichen Gemeindehauses
Friedhofsweihe
Segnung einer Friedhofshalle

Segnung religi�ser Zeichen
Weihwasser
Kreuz oder Christusbild
Marienbild 
Heiligenbild
Christopherusplakette
Rosenkranz 
Religi�se Abzeichen und Medaillen
Fahne
Kerzen
Sterbekreuz und Sterbekerze
Andere religi�se Zeichen

Segnungen f�r Familien
Segnung einer Familie
eines Kindes
eines kranken Kindes
f�r Jugendliche bei neuen Lebensabschnitten
Verlobung
Segnung eines Kranken
Tischsegen
Brotsegnung
Segnung eines Hauses
Segnung einer Wohnung

Segnungen von �ffentlichen Einrichtungen
Rathaus
Amtsgeb�ude 
Krankenhaus, Sanatorium
Altenheims
Feuerwehr
Wasseranlage
Kl�ranlage

Arbeitst�tten
Segnung eines Industriebetriebes
eines landwirtschaftlichen Betriebs
eines handwerklichen Betriebs
Eines Kaufhauses oder Gesch�fts
einer Buchhandlung
von B�ror�umen
einer Arztpraxis, einer Apotheke
einer Bank
einer Gastst�tte, eines Hotels
von Tieren
von Maschinen und Ger�ten
von Feldern, Weiden, Weing�rten

Bildungseinrichtungen
Segnung eines Kindergartens
einer Schule
eines Jugendheimes
einer Bildungsst�tte
einer B�cherei oder Bibliothek

Segnung von Verkehrsmitteln
von Fahrzeugen
von �ffentlichen Verkehrsmittel
eines Schiffes
eines Flugzeuges
eines Sanit�tsfahrzeuges
eines Einsatzfahrzeuges
einer Seilbahn, eines Liftes
einer Stra�e
einer Br�cke

Musikinstrumente, Sportanlagen, Tourismus
Sengung von Musikinstrumenten
Sportanlage
Berg- oder Sch�tzh�tte
Bergsteigerausr�stung


Segensgebet f�r einen Feuerwehrwagen
Herr, unser Gott, du hast die Welt erschaffen und alles wunderbar geordnet. Die Elemente stehen in deinem Dienst und erhalten den Menschen am Leben. Das Feuer spendet W�rme und Licht, es schmilzt das Erz und l�utert das Gold. Es kann auch zur Gefahr werden, f�r Mensch und Tier, f�r Hab und Gut.
Segne, wir bitten dich, dieses Feuerwehrfahrzeug, das wir heute in Dienst stellen. In der Gefahr trage es dazu bei, die zerst�rerische Gewalt des Feuers zu brechen, Ungl�ck und Naturkatastrophen abzuwehren. Sch�tze die Menschen, die sich seiner bedienen und als Feuerwehrleute ihren Dienst zum Wohl der Gemeinschaft verrichten. Bewahre uns vor Schaden und mache uns alle bereit zu aufrichtiger Zusammenarbeit, zu br�derlicher Hilfeleistung und zum Dienst am N�chsten

Dann wird noch der Patron der Feuerwehrleute, der hl. Florian, angerufen:

Last uns auch um den Schutz des heiligen Florian bitten:
Gro�er und Starker Gott, du hast den heiligen Florian und seinen Gef�hrten die Gnade geschenkt, den Glauben an Christus durch ihr Sterben zu bezeugen.
Gew�hre uns auf ihre F�rsprache Schutz und Hilfe und gib auch uns den Mut, den Glauben unerschrocken zu bekennen. 

F�rbitten
Wir beten zu unserem Herrn und Gott und rufen zu ihm:
�	Allm�chtiger Gott, lohne allen Feuerwehrm�nnern ihren Einsatz f�r die Gemeinschaft.
Wir bitten dich, erh�re uns!
�	Bewahre uns vor Feuersnot und Katastrophen.
Wir bitten dich, erh�re uns!
�	Lass unter uns die gegenseitige Verst�ndigung und Hilfsbereitschaft wachsen.
Wir bitten dich, erh�re uns!



