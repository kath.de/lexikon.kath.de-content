titel:Zingulum
stichworte:Zingulum, G�rtel, Strick, kath
bild:bild.jpg

Das Wort bedeutet "G�rtel", heute eher einem Strick vergleichbar, mit dem die <a href="http://lexikon/liturgie/index.php?page=albe.php">Albe</a> zusammengehalten wird. Auch Messdiener und Messdienerinnen binden sich ein Zingulum um ihr Gewand. Der <--a href="/symbole_kirchenraum/index.php?page=guertel.php"-->G�rtel<--/a-->, den M�nche tragen, wird auch Zingulum genannt.