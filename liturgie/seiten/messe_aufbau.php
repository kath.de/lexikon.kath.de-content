titel:Aufbau der Messe
stichworte:Messe, Aufbau, Wortgottesdienst, Eucharestie, Kommunion, Hochgebet, Sanctus, Gloria, Kyrie, kath
bild:bild.jpg

Die Messe, wie sie Katholiken feiern, hat sich �ber Jahrhunderte entwickelt. Im Abendland hat die in Rom entwickelte Form der Messe nicht zuletzt deshalb durchgesetzt, weil Karl d. Gr. sie f�r das Frankenreich verbindlich gemacht hat. In der Struktur unterscheidet sich die Form nicht wesentlich von den Liturgien der orthodoxen Kirchen und auch nicht von der Mail�nder oder der mozarabischen, in S�dspanien gefeierten Liturgie. Bis in die sechziger Jahre des 20. Jahrhunderts wurde die Messe lateinisch gefeiert, bis das II. Vatikanische Konzil die Feier in der jeweiligen Volkssprache einf�hrte. Der Vorteil f�r die Katholiken besteht darin, da� sie im Ausland dem Ablauf einer Messe folgen k�nnen, ohne alle Texte zu verstehen, denn die Messe hat immer den gleichen Aufbau.
Der �ltere Teil der Messe ist der Mahlteil, der eucharistische Gottesdienst. Das Neue Testament berichtet, da� die J�ngergemeinde sich auch nach Jesu Tod im Abendmahlsaal getroffen hat. Aus dem 1. Korintherbrief des Paulus ist zu entnehmen, da� die Christen sich zu dem Ged�chtnismahl trafen, das Jesus am <a href="http://kath.de/Kirchenjahr/gruendonnerstag.php">Gr�ndonnerstag</a> eingesetzt hatte. Da die Christen sich anfangs noch als Teil des Judentums verstanden, besuchten sie die Synagogengottesdienste am Sabbat-Samstag und feierten das Ged�chtnismahl am 1. Tag der Woche, der dann zum Wochenfeiertag, dem Sonntag wurde. Der Synagogengottesdienst bestand aus Lesungen und Ges�ngen und kannte auch die Auslegung der gelesenen Texte in einer Predigt. Mit dem Entstehen eigener christlicher Texte in der zweiten H�lfte des 1. Jahrhunderts und der wachsenden Distanz zwischen Synagoge und Kirche kamen zu dem Ged�chtnismahl die Lesungen, Ges�nge und Gebete des Wortgottesdienstes hinzu.
Die Messe in Rom wurde in den ersten Jahrhunderten auf Griechisch gefeiert. Papst Damasus (366-384) f�hrte die lateinische Sprache ein. Immer wieder mu�te das liturgische Leben und damit auch die Messe von �berwucherungen befreit und neu geordnet werden. Papst Gregor I. gab um 600 der Liturgie f�r die Stadt Rom eine so schl�ssige Gestalt, da� die r�mische Liturgie f�r das Abendland ma�gebend und von den Germanen �bernommen wurde. Nach ihm ist auch der <a href="http://www.kath.de/lexikon/symbole_kirchenraum/index.php?page=choral_gregorianisch.php">gregorianische Gesang</a> benannt, der im Mittelalter im Frankenreich zu der Vollendung gef�hrt wurde, die heute noch die H�rer fasziniert. Das Konzil von Trient (1551 wurde das Dekret �ber die Liturgie verabschiedet) regte eine Liturgiereform an, die von Pius V. umgesetzt wurde. Die Liturgiereform, die das II. Vatikanische Konzil in Grundz�gen beschlossen hatte, wurde von Paul VI. in eine Form gegossen. An der Grundstruktur der r�mischen Messe haben diese Reformen nichts ge�ndert.
<table border="1"><tr><th>Er�ffnung</th><th>Wortgottesdienst an Sonntagen</th><th>Eucharestischer Gottesdienst</th><th>Entlassung</th></tr>
<tr><td>Einzug
Begr��ung
Schuld-
Bekenntnis
Kyrie
Gloria
Tagesgebet</td>
<td><b>Erste Lesung</b>
meist aus dem Alten Testament

<b>Psalm</b>

<b>Zweite Lesung</b>
meist aus neutesta-mentlichen Briefen

<b>Halleluja</b>

Evangelientext

Predigt
Glaubensbekenntnis
F�rbitten</td>
<td>Kollekte und <b>Gabenbereitung</b>

<b><a href="index.php?page=hochgebet.php">Hochgebet</a></b>:
Pr�fation
Sanctus
Epiklese
Einsetzungsworte
Anamnese
Intercessiones
Doxologie

<b><a href="index.php?page=kommunion.php">Kommunionteil</a></b>
Vater unser
Embolismus
Friedensgebet
Brotbrechen mit Agnus Dei-Gesang
Kommunionausteilung</td>
<td>Schlu�gebet
Vermel-dungen
Segen
Entlassung :
Ite Missa est</td></tr></table>

An Werktagen und bei den meisten Heiligenfesten entfallen das Gloria, die Zweite Lesung, das Glaubensbekenntnis und meist auch die Predigt. Festliche Messen unterscheiden sich nur durch die musikalische Gestaltung.

Was erst einmal wie aufeinander folgende Einzelteile erscheint, hat eine innere Dramaturgie, die mit dem Thema der Feier zusammenh�ngt. Das Thema ist die Beziehung des Menschen zu Gott unter dem Vorzeichen, dass der Mensch nicht wie selbstverst�ndlich in dieser Beziehung lebt. Der Mensch hat sich verloren, so da� Gott ihn heimholen musste - durch die Sendung seines Sohnes.

In der <b>Er�ffnung</b> geht es darum, da� das Thema jeder Messe bereits anklingt. Der Vorsteher er�ffnet nicht nur die Messe, sondern bringt die Gemeinde in Beziehung zu Gott. Die Gemeinde versteht sich im S�ndenbekenntnis als erl�sungsbed�rftig. Sie begr��t ihren Herrn (Kyrios ist das griechische Wort f�r Herr) und preist ihn im anschlie�enden Gloria.
Im Tagesgebet wird der Er�ffnungsteil zusammengefa�t, deshalb hie� dieses Gebet fr�her "Collecta" von "Sammeln", das gleiche Wort wie Kollekte, in der bei der Gabenbereitung Geld gesammelt wird.

Der <b>Wortgottesdienst</b>  thematisiert die Zuwendung Gottes und holt die Gemeinde da ab, wo sie herkommt: Dass die einzelnen w�hrend der Woche viele Erfahrungen machen mussten, die sie mit einer nicht-erl�sten Welt und der eigenen Fehlerhaftigkeit und Bosheit konfrontierten. Wie kann Gott mich erl�sen, wo doch viele Erfahrungen mir das Gef�hl geben mussten, dass ich alles andere als erl�st bin? Die Lesungen, Ges�nge, der Abschnitt, der aus einem der vier Evangelien verlesen wird und die Predigt sollen den einzelnen �berzeugen, dass Gott, sicher anders als die Menschen erwarten, sein Werk der Erl�sung vollbringt. Im Glaubensbekenntnis (Credo - Ich glaube) antwortet die Gemeinde auf die Lesungen und die Predigt und betet dann f�r die Anliegen der Zeit und der Kirche in den F�rbitten.

Er�ffnung und Wortgottesdienst haben die Gl�ubigen in die Feier der Erl�sung eingestimmt. Sie sind wieder in der Lage, Gott f�r Jesus zu danken, der die Erl�sung in seinem Tod und seiner Auferstehung vollbracht hat und durch die Sendung des Geistes  Anteil an dem neuen Leben gew�hrt, in das die Christen durch die Taufe bereits eingetreten sind. Die <a href="index.php?page=eucharestie.php">Eucharistie</a>, die Danksagung, hat von Jesus die Form eines Mahles erhalten

Der <b>eucharistische Teil</b> der Messe hat als Grundstruktur ein Mahl, das nicht der S�ttigung dient, sondern im Ged�chtnis an Jesus Christus gefeiert wird. Es  untergliedert sich in drei Teile:
<ol><li><b>Gabenbereitung</b>
Brot und Wein werden zum Altar gebracht. W�hrenddessen findet eine Geldsammlung (Kollekte) statt. Die Gabenbereitung schlie�t mit einem eigenen Gebet, dem Gabengebet.</li>
<li><b><a href="index.php?page=hochgebet.php">Hochgebet</a></b>
Der Priester dankt im Namen der Gemeinde Gott f�r die Erl�sungstat Jesu (Pr�fation), ruft den Heiligen Geist �ber Brot und Wein (Epiklese), spricht die Worte, die Jesus im Abendmahlssaal �ber Brot und Wein gesprochen hat (Einsetzungsworte), erinnert an Tod, Auferstehung, Himmelfahrt und Geistsendung (Anamnese), und bittet in verschiedenen Anliegen (Intercessionen).
Das Hochgebet schlie�t mit einem feierlichen Lobpreis, der Doxologie.</li>
<li>Der <b><a href="index.php?page=kommunion.php">Kommunionteil</a></b>, in dem die Feiernden das gewandelte Brot und an einigen Tagen auch den gewandelten Wein empfangen, wird mit dem Vater Unser und einer Fortf�hrung der Bitten des Vaterunsers, dem Embolismus eingeleitet. Es folgen ein Friedensgebet und der Friedensgru�. W�hrend des Brotbrechens wird das Agnus Dei (Lamm Gottes, erbarme dich unser) gesungen. Dann empfangen die Feiernden Jesus Christus in den Gestalten von Brot und Wein. Auf die Kommunionausteilung folgt ein Danklied.</li></ol>

<b>Entlassung</b>
An das Schlu�gebet, nach dem Ank�ndigungen f�r die Gemeinde ihren Platz haben, folgen Segen und Entlassung. Die Entlassung schlie�t nicht nur die Messe ab, sondern gibt ihr auch den Namen. Denn der Entlassungsruf "Gehet hin in Frieden" hei�t lateinisch "Ite, missa est": Geht, es ist Sendung. Die Gl�ubigen sollen gest�rkt in dem Glauben, dass Gott die Menschen liebt und sie in einem gro�en, die Zeiten �berspannenden Erl�sungsgeschehen heimholt, sich neu im Alttag bew�hren und die f�r das reich Gottes gewinnen, mit denen sie w�hrend der Woche in Kontakt kommen.

<a href="/seiten/team.html#eb">Eckhard Bieger S.J.</a>

Hinweise zu den <a href="http://www.fernseharbeit.de" target="_BLANK">Gottesdienst�bertragungen im ZDF</a>