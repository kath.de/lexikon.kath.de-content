titel:Missale
stichworte:Missale,Buch, Gebet, kath
bild:missale.jpg

Das Buch, aus dem die Gebete der Messe gesprochen oder gesungen werden. Im <a href="index.php?page=ritus_tridentinisch.php">tridentinischen Ritus</a> enthielt das Missale auch die Lesungen. Da nach dem II. Vatikanischen Konzil sehr viel mehr biblische Texte, �ber drei Jahre an den Sonntagen, �ber zwei Jahre an den Werktagen verteilt, gelesen werden, gibt es f�r den katholischen Gottesdienst mehre B�cher f�r die Lesungen, <a href="http://www.kath.de/lexikon/liturgie/index.php?page=lektionar.php">Lektionare</a>. In der evangelischen Kirche hei�t das Gottesdienstbuch Agende, in den orthodoxen Kirchen Euchologion oder Leiturgikon.