titel:Mitra
stichworte:Mitra, Bischof, Abt, kath
bild:bild.jpg

Das Wort leitet sich von griechisch "Stirnbinde" her, ist aber eine hohe M�tze, die Bisch�fe und �bte seit dem 11. Jahrhundert in dieser Form tragen. In der anglikanischen und in einigen lutherischen Kirchen hat sich die Mitra des Bischofs erhalten. Sie w�lbt sich mit  einem schild�hnlichen Vorder- und R�ckseite nach oben und hat nach hinten zwei B�nder.  Die M�tzenform hat sich in den Kirchen des Ostens deutlicher erhalten. Der Papst tr�gt ebenfalls eine Mitra, nicht die bis zum II. Vatikanischen Konzil �bliche <a href="http://www.kath.de/lexikon/liturgie/index.php?page=tiara.php">Tiara</a>