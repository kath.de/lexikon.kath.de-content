titel:Albe
stichworte:Albe, Unterkleid, Priester, Diakon, kath
bild:bild.jpg

Ein wei�es (lateinisch albus) Unterkleid, das bis zu den F��en reicht. Die Albe wird durch ein <a href="http://www.kath.de/lexikon/liturgie/index.php?page=zingulum.php">Zingulum</a>, heute eine Art Strick, zusammengehalten und von Priestern und Diakonen unter dem Messgewand bei den Gottesdiensten getragen.