titel:Purpur
stichworte:Purpur, Kardinal, Meeresschnecke, kath
bild:bild.jpg

Das aus einer Meeresschnecke gewonnene Rot war von hohem Wert. Da die römischen Patrizier Purpurgewänder trugen, ist Purpur die Farbe der Kardinäle. Da Kardinäle zu ihrer Ernennung das neue Gewand erhalten, bedeutet "den Purpur erhalten", zum Kardinal ernannt werden.