titel:Akolyth
stichworte:Akolyth, Messdiener, Priesteramtskandidat, kath
bild:bild.jpg

Fr�her eine Weihestufe, heute als Beauftragung gestaltete Einweisung in das Amt des Messdieners. Diese feierliche Einf�hrung wird allerdings nicht Messdienern zuteil, sondern Priesteramtskandidaten auf ihrem Weg zur Priesterweihe.