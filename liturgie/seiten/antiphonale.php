titel:Antiphonale
stichworte:Antiphonale, Psalm, Stundengebet, kath
bild:bild.jpg

Buch mit den Melodien für die Antiphonen, Verse, mit denen ein Psalm jeweils eingerahmt wird. Der Psalm selbst wird in einer einfacheren Melodie gesungen. Im Antiphonale stehen meist auch die Psalmen des <a href="http://www.kath.de/lexikon/liturgie/index.php?page=stundengebet.php">Stundengebets</a>. Da die Benediktiner sich am intensivsten dem liturgischen Gebet du Gesang widmen, sind die Bücher meist von Benediktinerklöstern herausgegeben.