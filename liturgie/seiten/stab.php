titel:Stab
stichworte:Stab, Bischof, Abt, Hirte, Moses, kath
bild:bild.jpg

Bisch�fe und �bte tragen als Zeichen ihrer Amtsvollmacht einen dem Hirtenstab nachbildeten Bischofs- oder Abtsstab. Moses trug auch einen Stab, mit dem er das Rote Meer teilte, damit die Israeliten so dem nachr�ckenden Heer des Pharao entkommen konnten (Buch Exodus 4,1-5, 14, 16.) Mit seinem Stab schlug Moses Wasser aus dem Felsen (Buch Numeri, 20, 7-11) F�r Jakobspilger und andere Wallfahrer geh�rt der Stab zur Ausr�stung.