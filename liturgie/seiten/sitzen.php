titel:Sitzen
stichworte:Sitzen, Stehen, Zuh�ren, kath
bild:bild.jpg

In der Liturgie entspricht das Sitzen dem Zuh�ren der Lesungen und der Predigt. Aus Ehrfurcht vor den Worten und Taten Jesu wird das Evangelium stehend geh�rt. In der fr�hen Kirche predigten die Bisch�fe von ihrem Platz in der Apsis sitzend. Die der Liturgie entsprechende Haltung ist das <a href="http://www.kath.de/lexikon/liturgie/index.php?page=stehen.php">Stehen</a>, das gilt auch f�r das Chorgebet. Zum Ausruhen gibt es im Chorgest�hl die <a href="http://www.kath.de/lexikon/liturgie/index.php?page=misericordien.php">Misericordien</a>