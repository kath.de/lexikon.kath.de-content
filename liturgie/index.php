<?php

if($page == 'eucharestie.php') {
  header('Location: http://www.kath.de/lexikon/liturgie/index.php?page=eucharistie.php', true, 301);
}

        // Lexikon-Script by Andreas Dewald
        // 03.2007

        include_once("functions.php");

        // please edit config.php to configure this script for your needs
        include_once("config.php");

        if( empty($page) || isInvalid($page) ){
                $page=$startpage;
        }

        $page = $pagedirectory."/".$page;
        $inhalt = readcontent($page);
        $title = readtitle($page);
        $keywords = readkeywords($page);
        $rightpanel = readrightpanel($page);
        $rightpanelhtml = readrightpanelhtml($page);


// feel free to edit the following to change the layout

?>

<HTML>
        <HEAD>
                <TITLE>

                <?php
                        echo $title;
                ?>
                | kath.de Liturgie-Lexikon
                </TITLE>

                <meta http-equiv="content-language" content="de">
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

        <?php
                        echo "<meta name='title' content='$title'>";
                echo "<meta name='author' content='Redaktion kath.de'>";
                echo "<meta name='publisher' content='kath.de'>";
                echo "<meta name='copyright' content='kath.de'>";
                echo "<meta name='robots' content='index,follow'>";
                echo "<meta name='revisit-after' content='10 days'>";
                echo "<meta name='revisit' content='after 10 days'>";
                echo "<meta name='keywords' lang='de' content='$keywords'>";
        ?>

                <LINK title=fonts href="kaltefleiter.css" type=text/css rel=stylesheet>
        </HEAD>
        <BODY bgColor=#ffffff leftMargin=6 topMargin=6 marginheight="6" marginwidth="6">
                <TABLE cellSpacing=0 cellPadding=6 width="100%" border=0>
              <TR>
        <TD vAlign=top align=left width=100>
          <table width="216" border="0" cellpadding="0" cellspacing="0">
            <tr valign="top" align="left">
              <td width="8"><img src="pictures/boxtopleftcorner.gif" width="8" height="8" alt=""></td>
              <td width="200" background="pictures/boxtop.gif"><img src="pictures/boxtop.gif" alt="" width="8" height="8"></td>
              <td width="8"><img src="pictures/boxtoprightcorner.gif" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="pictures/boxtopleft.gif"><img src="pictures/boxtopleft.gif" width="8" height="8" alt=""></td>
              <td bgcolor="#E2E2E2"><b>

              <?php
                      echo $lexikonname;
              ?>

              </b></td>
              <td background="pictures/boxtopright.gif"><img src="pictures/boxtopright.gif" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="pictures/boxdividerleft.gif" width="8" height="13" alt=""></td>
              <td background="pictures/boxdivider.gif"><img src="pictures/boxdivider.gif" alt="" width="8" height="13"></td>
              <td><img src="pictures/boxdividerright.gif" width="8" height="13" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="pictures/boxleft.gif"><img src="pictures/boxleft.gif" width="8" height="8" alt=""></td>
              <td><?php include("logo.html"); ?> </td>
              <td background="pictures/boxright.gif"><img src="pictures/boxright.gif" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="pictures/boxbottomleft.gif" width="8" height="8" alt=""></td>
              <td background="pictures/boxbottom.gif"><img src="pictures/boxbottom.gif" width="8" height="8" alt=""></td>
              <td><img src="pictures/boxbottomright.gif" width="8" height="8" alt=""></td>
            </tr>
          </table>
          <br>
          <table width="216" border="0" cellpadding="0" cellspacing="0">
            <tr valign="top" align="left">
              <td width="8"><img src="pictures/boxtopleftcorner.gif" width="8" height="8" alt=""></td>
              <td width="200" background="pictures/boxtop.gif"><img src="pictures/boxtop.gif" alt="" width="8" height="8"></td>
              <td width="8"><img src="pictures/boxtoprightcorner.gif" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="pictures/boxtopleft.gif"><img src="pictures/boxtopleft.gif" width="8" height="8" alt=""></td>
              <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
              <td background="pictures/boxtopright.gif"><img src="pictures/boxtopright.gif" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="pictures/boxdividerleft.gif" width="8" height="13" alt=""></td>
              <td background="pictures/boxdivider.gif"><img src="pictures/boxdivider.gif" alt="" width="8" height="13"></td>
              <td><img src="pictures/boxdividerright.gif" width="8" height="13" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="pictures/boxleft.gif"><img src="pictures/boxleft.gif" width="8" height="8" alt=""></td>
              <td class="V10">

              <?php
                      include("az.php");
              ?>

              </td>
              <td background="pictures/boxright.gif"><img src="pictures/boxright.gif" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="pictures/boxbottomleft.gif" width="8" height="8" alt=""></td>
              <td background="pictures/boxbottom.gif"><img src="pictures/boxbottom.gif" width="8" height="8" alt=""></td>
              <td><img src="pictures/boxbottomright.gif" width="8" height="8" alt=""></td>
            </tr>
          </table>
        </TD>
        <TD vAlign=top rowSpan=2 width=600;>
          <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
            <TBODY>
            <TR vAlign=top align=left>
              <TD width=8><IMG height=8 alt=""
                src="pictures/boxtopleftcorner.gif" width=8></TD>
              <TD background=pictures/boxtop.gif><IMG height=8 alt=""
                src="pictures/boxtop.gif" width=8></TD>
              <TD width=8><IMG height=8 alt=""
                src="pictures/boxtoprightcorner.gif" width=8></TD></TR>
            <TR vAlign=top align=left>
              <TD background=pictures/boxtopleft.gif><IMG height=8 alt=""
                src="pictures/boxtopleft.gif" width=8></TD>
              <TD bgColor=#e2e2e2>
                <H1><font face="Arial, Helvetica, sans-serif">

                <?php
                        echo $title;
                ?>

                </font></H1>
              </TD>
              <TD background=pictures/boxtopright.gif><IMG height=8
                alt="" src="pictures/boxtopright.gif" width=8></TD></TR>
            <TR vAlign=top align=left>
              <TD><IMG height=13 alt="" src="pictures/boxdividerleft.gif"
                width=8></TD>
              <TD background=pictures/boxdivider.gif><IMG height=13
                alt="" src="pictures/boxdivider.gif" width=8></TD>
              <TD><IMG height=13 alt=""
                src="pictures/boxdividerright.gif" width=8></TD></TR>
            <TR vAlign=top align=left>
              <TD background=pictures/boxleft.gif><IMG height=8 alt=""
                src="pictures/boxleft.gif" width=8></TD>
              <TD class=L12>
                <P><font face="Arial, Helvetica, sans-serif">

                <?php
                        echo $inhalt;
                ?>

                </font></P>
                <P>&nbsp;</P>
                <P>&copy;<a href="http://www.kath.de"> <font face="Arial, Helvetica, sans-serif">www.kath.de</font></a></P>
                </TD>
              <TD background=pictures/boxright.gif><IMG height=8 alt=""
                src="pictures/boxright.gif" width=8></TD></TR>
            <TR vAlign=top align=left>
              <TD><IMG height=8 alt="" src="pictures/boxbottomleft.gif"
                width=8></TD>
              <TD background=pictures/boxbottom.gif><IMG height=8 alt=""
                src="pictures/boxbottom.gif" width=8></TD>
              <TD><IMG height=8 alt="" src="pictures/boxbottomright.gif"
                width=8></TD></TR></TBODY></TABLE></TD>



    <td style='vertical-align:top;'>

    <a href="http://www.abtei-muensterschwarzach.de/ams/startseite/Gottesdienst/gottesdienstordnung.html" target="_blank"><img
    src="http://kath.de/lexikon/liturgie/Liturgie.jpg"></a></a>

     <?php
             include "googleadd.php";
     ?>

     <br>

     <?php
        echo $rightpanel;

        echo $rightpanelhtml;
     ?>

    </td></TR>
      <TR>
        <TD vAlign=top align=left>
    </TD>
      </TR></TBODY></TABLE></BODY></HTML>
