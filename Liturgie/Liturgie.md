---
title: Liturgie
tags: Liturgie, Messe, Stundengebet, kath
---

Liturgie kommt von dem griechischen zusammengesetzten Wort für Laos-Volk bzw. die Vielen und Ergon-Tun.
Liturgie ist also ein Werk von zumindest Mehreren.

Als Fachbegriff bezeichnet Liturgie das gottesdienstliche Handeln ins seiner Vielfalt.

Die Eucharistiefeier ist der Kern wie auch der Höhepunkt der Liturgie. Auch Taufe, Hochzeit und Beerdigung sind Liturgie.
Firmung und Priesterweihe werden jeweils in einer Messfeier gespendet.
Auch das Stundengebet ist Liturgie, Tagzeitenliturgie genannt.

Die Liturgie bezieht den Menschen auf Gott, sie ist Lob, Dank, Bitte und in den Sakramenten das auch in einem äußeren Zeichen, Wasser, Brot, Wein, Chrisam, Handauflegung zugesagte Heil.

Das Wort hat bereits die ins Griechische übersetzte Bibel der Juden, die Septuaginta, für den Tempeldienst verwendet. Als Liturgie konnte damals auch die Armenspeisung als von Gott aufgetragenes Werk verstanden werden.
