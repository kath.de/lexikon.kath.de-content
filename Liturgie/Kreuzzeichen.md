---
title: Kreuzzeichen
tags: Kreuzzeichen, Segen, Priester, kath
---

Das Kreuzzeichen wird vom Priester als Segensgestus eingesetzt, aber Priester und Gläubige bezeichnen sich selbst mit dem Kreuzzeichen, das auf den Dreieinigen Gott hin gedeutet wird: "Im Namen des Vaters und des Sohnes und des Heiligen Geistes." In den orthodoxen Kirchen legt der Betende die Spitzen von Daumen, Zeige- und Mittelfinger als Hinweis auf den dreifaltigen Gott zusammen und beginnt wie im Westen damit, seine Stirn zu berühren und die Brust zu berühren, dann berührt er aber zuerst die linke Schulter und dann erst die rechte.
