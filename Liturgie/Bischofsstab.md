---
title: Bischofsstab
tags: Bischofsstab, Hirtenstab, Investitur, Fürstenbistum, kath
---

Ein gekrümmter Stab, den der Bischof beim Ein- und Auszug aus der Kirche trägt, sowie bei der Verlesung des Evangeliums, bei der Predigt und zum abschließenden Segen. Der Bischofsstab leitet sich vom Hirtenstab her und ist Ausdruck der Amtsvollmacht. Im Mittelalter war mit der Übergabe des Stabes durch den König die Investitur, d.h. die Übergabe der weltlichen Macht über das Gebiet des Bistums, verbunden. Das Sprichwort "Unter dem Krummstab lässt sich gut leben" bezieht sich auf die meist besseren Lebensbedingungen in den ehemaligen Fürstbistümern, weil die Landesherren, die Bischöfe, etwas weniger Kriege führten.
