---
title: Schlagen, an die Brust schlagen
tags: Schlagen, an die Brust schlagen, Sündenbekenntnis, Agnus Dei, kath
---

Dieser Gestus wird in der Messe beim Sündenbekenntnis und beim Agnus Dei geübt. Es ist ein Verweis auf die eigene Person.
