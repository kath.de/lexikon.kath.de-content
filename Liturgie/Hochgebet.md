---
title: Hochgebet
tags: Hochgebet, Eucharestie, Einsetzungsworte, Wandlung, Sanctus, Epiklese, kath
---

Im Zentrum der Eucharistiefeier steht ein längeres Gebet, in das die Einsetzungsworte eingefügt sind. Diese Worte haben ihre Bezeichnung davon, dass Jesus mit ihnen das Mahl "eingesetzt" hat, das zu seinem Gedächtnis gefeiert wird. Mit den Einsetzungsworten bezeichnet Jesu das Brot als seinen Leib und den Wein als sein Blut. Damit hat er den Auftrag verbunden, das Mahl zu seinem Gedächtnis zu feiern. So bleibt Jesus durch seine Worte, die im Wortgottesdienst vorgelesen werden, sowie durch die Mahlfeier, in der der Priester seine Einsetzungsworte spricht, gegenwärtig. Das Mahl, das Jesus mit den Jüngern gefeiert hatte, war sein letztes. Jesus hatte sich anläßlich des Passahfestes in einer Jerusalemer Wohnung mit den 12 Aposteln zusammengefunden und innerhalb des jüdischen Mahles die Worte über das Brot und den Becher mit Wein gesprochen und diese dann herumgereicht. An die Mahlfeier erinnert der Gründonnerstag. Bereits das jüdische Mahl zum Passahfest war ein Gedächtnismahl, das an den Auszug der Juden aus Ägypten, an die Befreiung aus der Sklaverei erinnerte. Gott wurde für seine Rettungstat gedankt, es gab Lobgesänge und auch bereits das Halleluja. Im Johannesevangelium sind im Zusammenhang mit dem Bericht von dem Mahl zum Passahfest die sog. Abschiedsreden Jesu aufgezeichnet. Sie fügen sich auch in die jüdische Tradition ein, dass nicht nur gegessen, sondern über zentrale Fragen gesprochen wurde. Jesus hat nicht nur das Mahl gefeiert, sondern es auch erklärt und sich dabei an die jüdische Grundstruktur des Lobes und Dankes gehalten.
Im Markusevangelium heißt es: "Während des Mahls nahm er das Brot und sprach den <b>Lobpreis</b>  Dann nahm er den Kelch und sprach das <b>Dankgebet</b>. " Kap 14,22, 23 Die Texte zeigen auch, dass er dieses letzte Mahl mit seiner Sendung verbunden hat. So heißt es auch bei Markus: "Und er sagte zu ihnen: Das ist mein Blut, das Blut des Neues Bundes, das für viele vergossen wird. Amen ich sage euch: Ich werde nicht mehr von der Frucht des Weinstocks trinken bis zu dem Tag, an dem ich von Neuem davon trinken werde im Reich Gottes." Kap. 14, 24-25

Die Christen konnten an diese Gebetsstruktur anknüpfen. Sie haben nicht nur die in der jüdischen Mahlfeier entwickelten Elemente übernommen, sondern auch die Beziehungsstruktur des Gebetes. Der Priester richtet es an Gott Vater, ihm wird gedankt, ihm gegenüber wird das Lob zum Ausdruck gebracht. Diese Beziehungsstruktur findet sich in den Gebeten, die das Johannesevangelium von der Abendmahlsfeier überliefert. (s.u. bei Zitaten).
Der Priester dankt Gott für die Erlösung durch Christus, die Gemeinde lobt Gott, vor allem im Sanctus, sie erinnert sich an den Tod Jesu, an seine Auferstehung und Himmelfahrt und erfährt, dass sie mit dem Heiligen Geist begabt ist. Dank, Lob und Erinnerung münden bereits in der jüdischen Mahlfeier in Bitten für das Volk und andere Anliegen. Diese Elemente finden sich in den Liturgien des Ostens wie des Westens. Das Hochgebet der römischen Kirche hat in der Regel folgenden Aufbau:

<blockquote>1.   Präfation
2.   Sanctusgesang
3.   Herabrufung des Heiligen Geistes auf Brot und Wein (Epiklese)
4.   Einsetzungsworte
5.   Erinnerung an Jesu Tod und Auferstehung (Anamnese)
6.   Gebet für die Kirche, um die Verbindung mit dem Papst und dem Ortsbischof auszudrücken, werden der Papst und der Bischof der Diözese mit ihrem Vornamen genannt.
7.   Fürbitten für Lebende und Verstorbene (Interncessiones)
8.   Feierlicher Abschluß des Hochgebetes (Doxologie)</blockquote>

Diese Teile des Hochgebetes finden sich in einem liturgischen Text, der dem Priester Hippolyt zugeschrieben wird. Er hat die liturgischen Texte aufgeschrieben, die zu seiner Zeit, in der zweiten Hälfte des 3. Jahrhunderts, in der römischen Kirche in Gebrauch waren. Das heute in der katholischen Kirche benutzte II. Hochgebet geht auf diesen alten Text der römischen Kirche zurück. In seiner klaren Gliederung wird die oben beschriebene Grundstruktur erkennbar:

<i>Einladung zur Präfation:</i>
Priester:    Der Herr sei mit euch
Gemeinde:    Und mit deinem Geiste
Priester:    Erhebet die Herzen.
Gemeinde:    Wir haben sie beim Herrn
Priester:    Lasset uns danken dem Herrn, unserem Gott
Gemeinde:    Das ist würdig und recht

<i>Präfation</i>
In Wahrheit ist es würdig und recht,
dir, Herr, heiliger Vater, immer und überall zu danken,
durch deinen geliebten Sohn Jesus Christus.
Er ist dein Wort, durch ihn hast du alles erschaffen.
Ihn hast du gesandt als unseren Erlöser und Heiland.
Er ist Mensch geworden durch den Heiligen Geist,
geboren von der Jungfrau Maria.
Um deinen Ratschluß zu erfüllen
und dir ein heiliges Volk zu erwerben,
hat er sterbend die Arme ausgebreitet
am Holze des Kreuzes.
Er hat die Macht des Todes gebrochen
und die Auferstehung kundgetan.
Darum preisen wir dich mit allen Engeln
und Heiligen und singen vereint
mit ihnen das Lob deiner Herrlichkeit:

<i>Sanctus</i>
Heilig, heilig, heilig
Gott, Herr aller Mächte und Gewalten.
Erfüllt sind Himmel und Erde von deiner Herrlichkeit.
Hosanna in der Höhe.
Hochgelobt sei, der da kommt im Namen des Herrn.
Hosanna in der Höhe.

<i>Epiklese</i>
Ja, du bist heilig, großer Gott,
du bist der Quell aller Heiligkeit.
Darum bitten wir dich:
Sende deinen Geist auf diese Gaben herab und heilige sie,
damit sie uns werden Leib und Blut deines Sohnes,
unseres Herrn Jesus Christus.

<i>Einsetzungsworte</i>
Denn am Abend, an dem er ausgeliefert wurde und sich aus freiem Willen
dem Leiden unterwarf,
nahm er das Brot und sagte Dank, brach es,
reichte es seinen Jüngern und sprach:

NEHMET UND ESSET ALLE DAVON:
DAS IST MEIN LEIB,
DER FÜR EUCH HINGEGEBEN WIRD.

Ebenso nahm er nach dem Mahl den Kelch,
dankte wiederum, reichte ihn seinen Jüngern und sprach:

NEHMET UND TRINKET ALLE DARAUS:
DAS IST DER KELCH DES NEUEN UND EWIGEN BUNDES,
MEIN BLUT, DAS FÜR EUCH UND FÜR ALLE VERGOSSEN WIRD ZUR VERGEBUNG DER SÜNDEN.
TUT DIES ZU MEINEM GEDÄCHTNIS.

Geheimnis des Glaubens.
<i>Akklamation der Gemeinde:</i>
Deinen Tod, o Herr, verkünden wir,
und deine Auferstehung preisen wir,
bis du kommst in Herrlichkeit.

<i>Anamnese</i>
Darum, gütiger Vater, feiern wir das Gedächtnis des Todes und der Auferstehung deines Sohnes und bringen dir so das Brot des Lebens und den Kelch des Heiles dar.
Wir danken dir, daß du uns berufen hast,
vor dir zu stehen und dir zu dienen.
Wir bitten dich: Schenke uns Anteil an Christi Leib und Blut,
und laß uns eins werden durch den Heiligen Geist.

<i>Gebet für Papst, Bischof und die Kirche</i>
Gedenke deiner Kirche auf der ganzen Erde
und vollende dein Volk in der Liebe,
vereint mit unserem Papst N.,
unserem Bischof N. und allen Bischöfen,
unseren Priestern und Diakonen
und mit allen, die zum Dienst in der Kirche bestellt sind.

<i>Gebet für die Verstorbenen</i>
Gedenke (aller) unsere Brüder und Schwestern,
die entschlafen sind in der Hoffnung, dass sie auferstehen.
Nimm sie und alle, die in deiner Gnade aus dieser Welt geschieden sind, in dein Reich auf, wo sie dich schauen von Angesicht zu Angesicht.

<i>Gebet für die Lebenden</i>
Vater, erbarme dich über uns alle, damit uns das ewige Leben zuteil wird in der Gemeinschaft mit der seligen Jungfrau und Gottesmutter Maria, mit deinen Aposteln und mit allen, die bei dir Gnade gefunden haben von Anbeginn der Welt, dass wir dich loben und preisen durch deinen Sohn Jesus Christus.

<i>Schlußdoxologie</i>
Durch ihn und mit ihm und in ihm ist dir,
Gott, allmächtiger Vater,
in der Einheit des Heiligen Geistes
alle Herrlichkeit und Ehre jetzt und in Ewigkeit.
Amen.


<i><b>Zitate</b>
Von Johannes überliefertes Gebet Jesu beim Letzten Abendmahl:
Und Jesus erhob seine Augen zum Himmel und sprach:
Vater, die Stunde ist da. Verherrliche deinen Sohn, damit der Sohn dich verherrlicht. Denn du hast ihm Macht über alle Menschen gegeben, damit er allen, die du ihm gegeben hast, ewiges Leben schenkt. Das ist das ewige Leben: dich, den einzigen wahren Gott, zu erkennen und Jesus Christus, den du gesandt hast. Ich habe dich auf Erden verherrlicht und das Werk zu Ende geführt, das du mir aufgetragen hast. Vater, verherrliche mich jetzt bei dir mit der Herrlichkeit, die ich hatte, bevor die Welt war.
Kap. 17,1-5

Auf diesen Lobpreis folgt ein Gebet Jesu für die Jünger, das dem Gebet für die Kirche, die Gläubigen und die Verstorbenen in den verschiedenen Hochgebeten entspricht.</i>

<a href="http://www.kath.de/seiten/team.html#eb">Eckhard Bieger S.J.</a>

Hinweise zu den <a href="http://www.fernsehgottesdienst.de">Gottesdienstübertragungen im ZDF</a>

