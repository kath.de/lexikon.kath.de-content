---
title: Passion
tags: Passion, Christus, Leiden, Kreuzweg, kath
---

Das Wort kommt vom lateinischen Wort für "Leiden" und bezeichnet den Leidensweg <a href="http://www.kath.de/lexikon/liturgie/index.php?page=kreuzweg.php">Kreuzweg</a> und das Sterben Jesu. Passion werden die Teile der Evangelien genannt, die die Verurteilung, die Geißelung, den Weg nach Golgatha und den Tod Jesu beschreiben. Am Palmsonntag wird jeweils der Passionsbericht eines der Synoptiker, am Karfreitag der des Johannes gelesen. Diese Texte sind Grundblage der vertonten Passionen.
