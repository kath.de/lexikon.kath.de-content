---
title: Pallium
tags: Pallium, Erzbischof, kath
---

Ein Stoffband, das von Erzbischöfen um den Hals über dem Messgewand getragen wird. Die Wolle für die Pallien der neu ernannten Erzbischöfe wird am 21. Januar, am Gedenktag der hl. Agnes, vom Papst gesegnet und am Fest der heiligen Petrus und Paulus am 29.Juni verliehen. Das Pallium war im römischen Reich ein Rangabzeichen. Seine ursprüngliche Form ist ein Stoffsteifen, der um den Hals gelegt wird und dessen Ende an der linken Seite herabhängt. Diese Form ist in der Ostkirche in Gebrauch geblieben, im Mittelalter rückte der herabhängende Stoffstreifen in die Mitte, so dass das Pallium von vorne wie ein Ypsilon aussieht. Papst Benedikt XVI. trägt das Pallium wieder in der klassischen Form.
