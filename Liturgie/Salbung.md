---
title: Salbung
tags: Salbung, Sakrament, Weihe, Firmung, kath
---

Mit geweihten Ölen werden der Täufling, der Firmling, der für ein Amt zu Weihende wie auch der Altar und die Glocken gesalbt. Siehe auch <a href="http://www.kath.de/lexikon/liturgie/index.php?page=chrisam.php">Chrisam</a>
