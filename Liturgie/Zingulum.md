---
title: Zingulum
tags: Zingulum, Gürtel, Strick, kath
---

Das Wort bedeutet "Gürtel", heute eher einem Strick vergleichbar, mit dem die <a href="http://lexikon/liturgie/index.php?page=albe.php">Albe</a> zusammengehalten wird. Auch Messdiener und Messdienerinnen binden sich ein Zingulum um ihr Gewand. Der <--a href="/symbole_kirchenraum/index.php?page=guertel.php"-->Gürtel<--/a-->, den Mönche tragen, wird auch Zingulum genannt.
