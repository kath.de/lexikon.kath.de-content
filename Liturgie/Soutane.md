---
title: Soutane
tags: Soutane, Untergewand, Priestergewand, Kleriker, kath
---

Vom französischen "sous" bezeichnet es eigentlich ein Untergewand, es ist aber das typische Gewand, mit dem Priester in südlichen Ländern wie auch in Filmen sofort erkennbar sind. Die Soutane wird mit einem <a href="http://www.kath.de/lexikon/liturgie/index.php?page=zingulum.php">Zingulum</a> zusammengehalten. Die Farbe der Soutane zeigt den Rang eines Klerikers. Der Papst trägt eine weiße, Kardinäle rote, Bischöfe eine violette, Priester, aber auch Diakone und Priesteramtkandidaten eine schwarze Soutane. Für Priester werden Soutanen mit 33 Knöpfen angefertigt, die auf die 33 Jahre hinweisen, die Jesus gelebt hat. Die Ordensmitglieder, nicht nur die Priester, die nicht wie die Benediktiner oder Franziskaner ein eigenes Ordensgewand haben, tragen auch eine Soutane. In warmen Gebieten wird eine weiße Soutane getragen. Die Soutane wird, wenn sie bei liturgischen Anlässen getragen wird, <a href="http://www.kath.de/lexikon/liturgie/index.php?page=talar.php">Talar</a> genannt. Dann trägt der Kleriker kein Zingulum.
