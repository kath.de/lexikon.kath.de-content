---
title: Evangeliar
tags: Evangeliar, Buch, Evangelium, kath
bild:evangeliar.jpg

Meist besonders ausgestattetes und im Mittealter mit Buchmalerei gestaltete Vorlesebuch, das entsprechend seiner Wortbedeutung nur die Evangelien der Sonn- und Festtage enthält. Das Evangeliar wird vom Diakon beim Einzug in die Höhe gehalten. Es symbolisiert Christus in seinem Wort, denn in den vier Evangelien finden sich die Handlungen, die Worte und das Sterben Jesus wie auch die Botschaft von seiner Auferstehung aufgezeichnet.
