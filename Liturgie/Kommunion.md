---
title: Kommunion
tags: Kommunion, Eucharestie, Messe, Einsetzung, Jesus, Gemeinschaft, kath
---

Kommunion heißt, in eine Gemeinschaft eintreten. Das kann dadurch geschehen, dass man seine Gedanken austauscht und Übereinstimmung erkennt. Andere Möglichkeiten der Kommunion sind der gemeinsame Gesang oder das gemeinsame Gebet. Diesen Weg in eine größere Gemeinschaft ist die Gottesdienstgemeinde bereits im ersten Teil der Messe gegangen. Im zweiten, im eucharistischen Teil der Messe, wird die Gemeinschaft auf eine Person hin intensiviert. Das ermöglicht das Mahl, die Grundgestalt der Eucharistie. Die Gläubigen sind von Jesus zu einem Mahl eingeladen, das er zu seinem Gedächtnis eingesetzt hat. Wie bei anderen herausgehobenen Mählern, z.B. einer Geburtstagsfeier, soll das Mahl die Verbundenheit mit der zentralen Person, dem Geburtstagskind vertiefen. Deshalb muß man als Mitfeiernder nicht zahlen, sondern ist eingeladen. Der Einladende gibt von Seinem, um eine tiefere Gemeinschaft mit ihm zu ermöglichen. Anteil an sich selber geben, wollte Jesus mit dem letzten Mahl, dem Abendmahl, das er mit seinen Jüngern gefeiert hat. Das, was zu diesem Mahl aufgetragen wurde, hatte die Gruppe, die sich um den Prediger aus Nazareth gesammelt hatte, wohl aus Spenden erhalten. Deshalb ist das, was Jesus gibt, nicht das Essen selbst. Er knüpft an den Anlaß für das Mahl an, das Gedächtnis der Juden an den Auszug aus Ägypten. Pascha ist das Fest der Befreiung aus dem Frondienst des Pharao. Dieses Mahl hat bis heute als Hauptgang ein gebratenes Lamm. Jesus hat dieses Mahl neu akzentuiert. Er hat Brot und den Becher mit Wein zu seinem Zentrum gemacht. Deshalb stehen bis heute Brot und Wein im Zentrum der Eucharistie. An die Stelle des Lammes, eines der Opfertiere im jüdischen Kult, ist Jesus selbst getreten. Er ist das Lamm Gottes, das die Schuld hinweg nimmt. "Lamm" hatte der Vorläufer Jesu, Johannes der Täufer, den Messias genannt. (Johannesevangelium 1,29)
Weil Jesus sein Leben hingegeben hat, ist das eucharistische Mahl unerschöpflich. Wer die gewandelten Gaben von Brot und Wein zu sich nimmt, tritt in eine tiefere Gemeinschaft mit dem Spender der Gaben ein. Jesus hat im Abendmahlssaal von sich gesagt, daß das Brot zu seinem Leib und der Wein zu seinem Blut wird. Was der Einladende bei einem Geburtstagsessen versucht, eine tiefere Gemeinschaft mit ihm als Person zu erschließen, das gelingt in der Eucharistiefeier in einer noch anderen Dimension, die dadurch gegeben ist, daß Jesus den Menschen mit seinem Vater, dem Schöpfergott in eine tiefere Beziehung bringt.

Wie bei einem Geburtstags- oder Hochzeitsessen vertieft sich auch die Gemeinschaft der Feiernden untereinander - und so entsteht aus jeder Mahlfeier die kirchliche Gemeinschaft neu.

Die Bedeutung des Mahles hat sich bereits im <a href="index.php?page=hochgebet.php">Hochgebet</a> erschlossen, dem mittleren Teil der Eucharistiefeier, dem die Gabenbereitung vorausgegangen ist. Das meist vom Priester gesprochene Wort prägt die Gestalt des eucharistischen Mahles

Anders als bei einem Geburtstagsessen ist das eucharistische Mahl in hohem Maße stilisiert. Die Gläubigen empfangen das Brot in der Form einer kleinen Hostie, von dem Wein nur einen Schluck.

Aufbau des Kommunionteils der Messe
<blockquote>1.   Vater unser
2.   Embolismus
3.   Friedensgebet und Friedensgruß
4.   Brotbrechung mit Agnus-Dei-Gesang
5.   Präsentation der eucharistischen Gaben
6.   Kommuniongang
7.   Danklied
8.   Schlußgebet</blockquote>

Zu 1: Der Kommunionteil der Messfeier wird durch das "<b>Vater unser</b>" eingeleitet, das von allen gebetet wird. Es drückt die Bitte sowohl um das Kommen des Reiches Gottes aus wie um das tägliche Brot und die täglich neu notwendige Vergebung der persönlichen Schuld.

Zu 2: Die Bitten des "Vater unser" werden durch den sog. <b>Embolismus erweitert</b>.

Zu 3: Dann folgt ein Gebet um <b>Frieden</b>, das den Gedankengang, der der Messe insgesamt zugrunde liegt, weiterführt: Das Heil, das Jesus geschenkt hat, mündet in einen Frieden und wird einem Friedensgruß ausgedrückt, der durch den Austausch des Friedensgrußes mit den Banknachbarn fortgesetzt wird.

Zu 4: Das <b>Brotbrechen</b> ist im Neuen Testament ein Wort, das das Gedächtnismahl Jesu bezeichnet. Es leitet sich davon her, daß ein größeres Stück Brot geteilt wurde. Diese Bedeutung ist heute kaum erlebbar, weil das Brot beim Gabengang bereits als kleine Hostien zum Altar gebracht wird oder bei den orthodoxen Kirchen bereits in kleine Stücke geschnitten ist. Im Bericht über das erste Abendmahl heißt es: "Während des Mahls nahm Jesus das Brot und sprach den Lobpreis; dann brach er das Brot, reichte es den Jüngern und sagte: Nehmt und eßt, das ist mein Leib." (Matthäus 26,26) Im Brechen des Brotes wird die Hinrichtung Jesu gesehen, sein Leib wurde durch die Geißelung und die Kreuzigung "gebrochen". Zum Brotbrechen wird das Agnus die, Lamm Gottes gesungen und zwar dreimal.



Zu 5: <b>Präsentation der eucharistischen Gaben</b>
Die Gemeinde wird vom Priester mit folgenden Worten eingeladen:
"Seht das Lamm Gottes, das hinwegnimmt die Sünden der Welt". Der Satz ist dem Johannesevangelium entnommen (Kap. 1,29)
Zu 6: Die Gläubigen gehen nach vorne, um den Leib des Herrn, "die Kommunion" zu empfangen. Da es sich um eine Prozession handelt, gehört ein Wechselgesang zum Kommuniongang, der im Gregorianischen Choral "Communio" genannt wird.

Zu 7: <b>Danklied</b>
Wie es bei einer Geburtstagsfeier einen Dank an den Einladenden gibt, dankt auch die Gemeinde für die empfangende Gabe durch ein Lied.

Zu 8: <b>Schlussgebet</b>
Der Kommunionteil der Messe wird durch ein Gebet abgeschlossen, das im Lateinischen "Postcommunio" heißt. Das Gebet beinhaltet Dank und leitet auf die Entlassung aus der Feier über, nämlich dass die Gläubigen, das, was sie empfangen haben, im Alltag verwirklichen.

<i><b>Zitate:</b>

<b>Embolismus:</b>
Erlöse uns Herr, allmächtiger Vater, von allem Bösen
Und gib Frieden in unseren Tagen. Komm uns zu Hilfe mit deinem Erbarmen und bewahre uns vor Verwirrung und Sünde,
damit wir voll Zuversicht das Kommen unseres Erlösers Jesus Christus erwarten.
Denn dein ist das Reich und die Kraft und die Herrlichkeit in Ewigkeit. Amen

<b>Das Friedensgebet, wie es in der Osterzeit gebetet wird:</b>
Am Ostertag trat Jesus in die Mitte seiner Jünger und sprach den Friedensgruß.
Deshalb bitten wir:
Herr Jesu, du Sieger über Sünde und Tod,
schau nicht auf unsere Sünden,
sondern auf den Glauben deiner Kirche
und schenke ihr nach deinem Willen Einheit und Frieden.
Der Priester breitet die Hände aus und ruft:
Der Friede des Herrn sei allezeit mit euch.
Gemeinde: Und mit deinem Geiste.
Priester oder Diakon: Gebt einander ein Zeichen des Friedens und der Versöhnung.
Die Gläubigen tauschen den Friedensgruß aus.


<b>Während des Brotbrechens wird ein dreifacher Ruf gesprochen oder gesungen, der in eine Friedensbitte mündet:</b>
Lamm Gottes, du nimmst hinweg die Sünden der Welt,
erbarme dich unser.
Lamm Gottes, du nimmst hinweg die Sünden der Welt,
erbarme dich unser
Lamm Gottes, du nimmst hinweg die Sünden der Welt,
gib uns deinen Frieden.

<b>Lateinisch:</b>
Agnus Dei, qui tollis peccata mundi, miserere nobis
Agnus Dei, qui tollis peccata mundi, miserere nobis
Agnus Dei, qui tollis peccata mundi, dona nobis pacem

<b>Schlußgebet (Postcommunio) vom Pfingstsonntag</b>
Der Priester betet:
Lasset und beten:
Herr, unser Gott,
du hast deine Kirche mit himmlischen Gaben beschenkt.
Erhalte ihr deine Gnade,
damit die Kraft aus der Höhe, der Heilige Geist,
in ihr weiterwirkt und die geistliche Speise sie nährt bis zur Vollendung.
Darum bitten wir durch Christus, unseren Herrn.
Gemeinde: Amen

<b>Schlußgebet vom 8. Sonntag im Jahreskreis</b>
Barmherziger Gott,
du hast uns in diesem Mahl die Gabe des Heils geschenkt.
Dein Sakrament gebe uns Kraft in dieser Zeit
Und in der kommenden Welt das ewige Leben.
Darum bitten wir durch Christus, unseren Herrn.
Amen

<b>Schlußgebet vom 16. Sonntag</b>
Barmherziger Gott, höre unser Gebet.
Du hast uns im Sakrament das Brot des Himmels gegeben,
damit wir an Leib und Seele gesunden.
Gib, daß wir die Gewohnheiten des alten Menschen ablegen
Und als neue Menschen leben.
Darum bitten wir durch Christus, unsern Herrn.
Amen</i>

<a href="http://www.kath.de/seiten/team.html#eb">Eckhard Bieger S.J.</b>

Hinweise zu den <a href="http://www.fernsehgottesdienst.de">aktuellen Gottesdienstübertragungen im ZDF</a>
