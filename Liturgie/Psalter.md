---
title: Psalter
tags: Psalter, Stundengebet, Psalm, kath
---

Das Alte Testament als Sammlung mehrer Bücher, so der fünf Bücher Moses, der Bücher der Propheten. Es enthält auch das Buch der 150 Psalmen. Diese Gebete sind die Basis des <a href="http://www.kath.de/lexikon/liturgie/index.php?page=stundengebet.php">Stundengebets</a>. Der Psalter oder ein Psalterium, das in der Kirche gebraucht wird, enthält die Psalmen sowie Hymnen und andere Texte entsprechend der Ordnung des Stundengebets.
