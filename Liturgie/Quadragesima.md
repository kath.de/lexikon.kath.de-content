---
title: Quadragesima
tags: Quadragesima, vierzig, Fastenzeit, Ostern, kath
---

Der Name Quadragesima (für lateinisch vierzig) ist in Rom entstanden und bezeichnet die 40 Tage der Fastenzeit. Im griechischen Raum wird die Zeit auch als Tessarakoste bezeichnet. In den germanischen Sprachen wird das Moment des Fastens mehr überbetont. Die Zahl 40 leitet sich von den 40 Tagen her, die Jesus fastend in der Wüste verbrachte. 40 Jahre wanderte Israel in der Wüste, vierzig Tage war Moses auf dem Sinai.
Die Erwähnung dieser Zeit als Vorbereitungszeit für Ostern ist erstmals in einem Hirtenbrief des Athanasius von Alexandrien um 334 belegt. Ende des 4. Jahrhunderts war die Quadragesima allgemein eingeführt. Sie erhielt deshalb eine weitere Bedeutung, weil sie der Vorbereitung auf die Taufe gewidmet war. Die Taufe erfolgte in der Osternacht. Als sich die Kindertaufe durchsetzte, verlor die Fastenzeit ihren Charakter als Vorbereitung auf die Taufe.
Die Fastenzeit war 1. Jahrtausends dem Bussakrament gewidmet. Man konnte in diesen Jahrhunderten nur einmal im Leben das Sakrament empfangen. Die Büßer nahmen am Beginn der Fastenzeit vom Bischof das Bußgewand entgegen, waren von der Eucharistiefeier ausgeschlossen. Die Asche, die am Aschermittwoch an die Gläubigen ausgeteilt wird, wurde bis zum 11. Jahrhundert nur an die Büßer ausgeteilt. Am Gründonnerstag wurden die Büßer wieder in die Gottesdienstgemeinschaft aufgenommen
Die Fastenzeit beginnt deshalb am Aschermittwoch, weil die Sonntage als Gedenktage für Ostern nicht mitgezählt werden. Sie endet am Karsamstag um 18 Uhr, denn der Vorabend gehört bereits zum Feiertag.
Seit dem II. Vatikanum spricht man von der österlichen Bußzeit. In ihr werden zwei Themen wichtig: Die Tauferinnerung und die Buße und Umkehr. Im Laufe der drei Lesejahre spiegeln sich die drei Hauptthemen der Quadragesima: A Initiation (Taufe), B das Paschamysterium (Passion Jesu) und C Buße.
Die österliche Bußzeit ist neben dem Advent eine Zeit für Exerzitien.
Die Farbe der Quadragesima ist Violett, so wie im Advent. Am 4. Sonntag der Fastenzeit beginnt das Eingangslied mit Laetare, freue dich, deshalb wird an diesem Sonntag ein rosafarbiges und kein violettes Messgewand getragen.
Eine Zäsur bildet der 5. Fastensonntag als Passionssonntag, wo die Kreuze und Bilder verhüllt werden. Die Kreuzesverhüllung ist ein verehrendes Vorgehen, denn was man liebt, das verhüllt man. Am Karfreitag wir das Kreuz enthüllt und an den Altarstufen den Gläubigen zur Verehrung durch einen Kuss entgegen gehaltne.

Jürgen Pelzer
