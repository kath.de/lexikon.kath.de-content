---
title: Klapper
tags: Klapper, kath
---

Am Gründonnerstag verstummen die Glocken und auch die von den Messdienern benutzten Schellen oder Klingeln. Diese werden bis zur Osternacht durch Holzklappern ersetzt.
