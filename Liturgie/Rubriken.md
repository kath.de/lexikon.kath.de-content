---
title: Rubriken
tags: Rubriken, rot, Messbuch, kath
---

Das Wort kommt vom lateinischen Wort ruber "rot". In Rot sind die Anweisungen für den Zelebranten gedruckt, die im <a href="http://www.kath.de/lexikon/liturgie/index.php?page=messbuch.php">Messbuch</a> konkrete Hinweise für die jeweilige Handlung geben.
