---
title: Sitzen
tags: Sitzen, Stehen, Zuhören, kath
---

In der Liturgie entspricht das Sitzen dem Zuhören der Lesungen und der Predigt. Aus Ehrfurcht vor den Worten und Taten Jesu wird das Evangelium stehend gehört. In der frühen Kirche predigten die Bischöfe von ihrem Platz in der Apsis sitzend. Die der Liturgie entsprechende Haltung ist das <a href="http://www.kath.de/lexikon/liturgie/index.php?page=stehen.php">Stehen</a>, das gilt auch für das Chorgebet. Zum Ausruhen gibt es im Chorgestühl die <a href="http://www.kath.de/lexikon/liturgie/index.php?page=misericordien.php">Misericordien</a>
