---
title: Die Eröffnung der Messe
tags: Messe, Einzug, Verehrung des Altars, Eingangsgruß, Bußakt, Kyrie, Gloria, Tagesgebet, kath
---

<b>Einzug:</b>
Bis zum 5. Jahrhundert begann die Messe mit einem stillen Gebet der Kleriker, die sich dazu vor den Altar niederwarfen. Dies findet sich heute noch in der Karfreitagsliturgie. Der Fachausdruck dafür ist die Proskynese. Durch dieses stille Gebet wurde der Einzug abgeschlossen. Seit Gregor dem Großen werden dabei Psalmen bzw. Lieder gesungen. Die Gestalt des heute üblichen Einzugs entwickelte sich im Mittelalter: Wenn der Papst feierlich in eine Basilika einzog, wurden ihm sieben Leuchter und Weihrauch vorweggetragen.
Der Einzug muss dabei vom Altar aus gesehen werden: Das ganze Volk geht auf den Altar, also auf Gott zu. Die Gemeinde beteiligt sich durch den Gesang am Einzug der Altargruppe. Die Mitwirkenden am Gottesdienst sollen mit dem Priester einziehen, ihnen wird ein Kreuz vorangetragen.

<b>Verehrung des Altars:</b>
Der Priester küsst den Altar nach dem Einzug. Die in diesem Vorbereitungsritus gezeigte Verehrung gilt Christus, der durch den Altar symbolisiert wird. Noch im 13. Jahrhundert war es üblich, den Altar 21 mal zu küssen. Dabei können der Altar und das Kreuz auch mit Weihrauch inzensiert werden. Das meint, dass der Priester diese mit dem Weihrauchfaß beräuchert. Dies findet sich seit dem 11. Jahrhundert. Die Verwendung von Weihrauch im Gottesdienst ist fakultativ, so dass es mancherorts zur Abschaffung kam, anderenorts zu einer fast täglichen Verwendung von Weihrauch im Gottesdienst.

<b>Eingangsgruß</b>
Die Messe beginnt mit den Worten "Im Namen des Vaters, des Sohnes und des heiligen Geistes". Das dabei von dem Priester und den Gläubigen gemachte Kreuzzeichen ist seit dem 14. Jahrhundert belegt. Danach spricht der Priester "Der Herr Sei mit euch" (lat. Dominus vobiscum). Diese Formel ist eine Zitation aus Rut 2,4. Sie war bei den Juden eine gängige Grußformel. Wenn der Priester dabei die Arme ausbreitet, soll das eine Umarmung andeuten.
Die Gemeinde antwortet "Und mit deinem Geiste" (lat. Et cum spiritu tuo). Dies könnte den Sinn von "Und auch mit dir" haben. Wahrscheinlicher ist jedoch, dass mit dem Wort Geist hier die Gabe des Heiligen Geistes gemeint, die der Priester als Amtsträger in besonderer Weise braucht. Dementsprechend geht es nicht um die Kopie eines alltäglichen Begrüßungsrituals, sondern um eine erste Hinführung in das Mysterium, das in der Messe gefeiert werden soll.

<b>Bußakt - auch: Allgemeines Schuldbekenntnis</b>
Wer sich Gott nähert, wird sich bewusst, dass er dafür nicht vorbereitet ist, Fehlhaltungen, Nachlässigkeiten und bewußte Verletzung anderer Menschen stehen der Begegnung im Wege. Im Schuldbekenntnis stehen die Feiernden zu ihren Fehlern und lassen sich vom Priester die von Gott gewährte Versöhnung zusprechen. Der Bußakt kann bei Messen von besonderer Festlichkeit entfallen.

<b>Kyrie</b>
Im Kyrie-Ruf (griechisch für Herr) begrüßt die Gemeinde Christus als Retter.
Kyrie eleison / Christe eleison - Herr errette uns / Christus errette uns lautet der Kyrie Ruf. Diesen Ruf haben die Christen aus ihrer Umwelt übernommen. Er galt als Huldigung einem Gott oder einen Herrscher. Zugleich ist der Kyrios Ruf eine der kürzesten Glaubensformeln. Christus ist der auferstandene Herr (s. der Hymnus im Philipperbrief 2,6-11). Kyrios geht zurück auf den alttestamentlichen Gottestitel Adonai, mit dem der unausgesprochene Gottesname JHWH in den Texten des Alten Testamentes umschrieben wurde. Um 500 übernahm man in Rom die Kyrielitanei aus dem Osten. Dabei wurden die Anliegen der Gemeinde von dem Diakon vorgetragen und die Gemeinde antwortete mit dem Kyrie eleison. Unter Papst Gregor I. (+ 604) wurden die Anliegen dann weggelassen. Der nun übrig gebliebene Kyrie Ruf wurde im Laufe der Zeit noch um das Christe Eleison erweitert. Bis zur Liturgiereform des II. Vatikanischen Konzils gab es neun Kyrierufe.
Ursprünglich war die Zahl der Kyrierufe nicht begrenzt, es wurde nämlich so lange gesungen, bis der Papst an seinem Sitz angelangt war. Dann gab er den Sängern ein Zeichen. Der Neunzahl begegnen wir zum ersten Mal in Gallien. Im Mittelalter werden die Kyrierufe dann zunehmend trinitarisch gedeutet, d.h. das erste Kyrie gilt Gott Vater, das Christe eleison dem Sohn, das dritte Kyrie dem Heiligen Geist.

<b>Gloria</b>
Das Gloria ist einer der wenigen überlieferten Hymnen mit griechischem Ursprung. Durch das Konzil von Laodizäa (zwischen 341 und 380) wurden die meisten Hymnen verboten, weil sie man in ihnen häretisches Gedankengut sah. Das Gloria muss damals schon so angesehen gewesen sein, dass es nicht diesem Verbot zum Opfer fiel. Gloria meint übersetzt soviel wie Ruhm, Glanz, Herrlichkeit (hebräisch. kabod griechisch Doxa)
Es ist somit ein Lobgesang auf die Herrlichkeit Gottes. Anfänglich war es kein Bestandteil der Messe, wurde aber schon sehr früh in die Mitternachtsmesse von Weihnachten aufgenommen. Seit dem 11. Jahrhundert wird es dann an allen Festtagen und Sonntagen gebetet. Text s.u.

<b>Tagesgebet</b>
Auf lateinisch heißt Gebet collecta. Es schließt den Eröffnungsteil der Messe ab. Collectare meint sammeln, ein Gebet, das der Priester über die versammelte Gemeinde ausspricht. Zum anderen bedeutet es auch ein Gebet, in dem der Priester die Anliegen der Gemeinde sammelt und zusammenfaßt. Begonnen wird das Tagesgebet mit dem lateinischen Oremus (Beten wir bzw. lasset uns beten). Es folgt ein kurzer Moment des Schweigens, um sich zu sammeln.
Das Gebet selbst singt der Priester in der althergebrachten Gebetshaltung: Mit erhobenen Händen und aufrecht stehend. Dieses Gebet heißt auch Oration.
Das Gebet richtet der Priester an Gott Vater, in der Schlußformel werden immer die drei Personen der Trinität, Vater, Sohn und Heiliger Geist erwähnt.
Das Tagesgebet wechselt je nach Fest- oder Wochentag bzw. Heiligengedenktag, ebenso wie das Gebet am Ende der Gabenbereitung und das nach der Kommunionausteilung. Diese drei Gebete werden auch Gebete des Vorstehers genannt, weil der Priester als Vorsteher sie im Namen der Gemeinde an Gott richtet.
Jürgen Pelzer

<i><b>Das Tagesgebet vom 1. Adventssonntag</b>
Lasset uns beten:
Her, unser Gott,
alles steht in deiner Macht;
du schenkst das Wollen und das Vollbringen.
Hilf uns, daß wir auf dem Weg der Gerechtigkeit Christus entgegengehen
und uns durch Taten der Liebe auf seine Ankunft vorbereiten,
damit wir den Platz zu seiner Rechten erhalten,
wenn er wiederkommt in Herrlichkeit.
Er, der in der Einheit des Heiligen Geistes
Mit dir lebt und herrscht in Ewigkeit.
Amen

<b>Das Gloria
Lateinisch:</b>
Gloria in excelsis Deo et in terra pax hominibus bonae voluntatis.
Laudamus te, benedicimus te, adoramus te, glorificamus te,
gratias agimus tibi propter magnam gloriam tuam,
Domine Deus, Rex caelestis Deus Pater omnipotens,
Domine Fili unigenite, Iesu Christe, Domine Deus, Agnus Dei,
Filius Patris, qui tollis peccata mundi, miserere nobis;
qui tollis peccata mundi,
suscipe deprecationem nostram.
Qui sedes ad dexteram Patris, miserere nobis.
Quoniam tu solus Sanctus, tu solus Dominus, tu solus Altissimus,
Iesu Christe, cum Sancto Spiritu: in gloria Dei Patris. Amen.

<b>Deutsch:</b>
Ehre sei Gott in der Höhe und Friede auf Erden den Menschen seiner Gnade.
Wir loben dich, wir preisen dich, wir beten dich an, wir rühmen dich und danken dir,
denn groß ist deine Herrlichkeit:
Herr und Gott, König des Himmels, Gott und Vater, Herrscher über das All,
Herr, eingeborener Sohn, Jesus Christus.
Herr und Gott, Lamm Gottes, Sohn des Vaters, du nimmst hinweg die Sünde der Welt: erbarme dich unser;
du nimmst hinweg die Sünde der Welt: nimm an unser Gebet;
du sitzest zur Rechten des Vaters: erbarme dich unser.
Denn du allein bist der Heilige, du allein der Herr, du allein der Höchste, Jesus Christus, mit dem Heiligen Geist, zur Ehre Gottes des Vaters. Amen.</i>

Hinweise zu den <a href="http://www.fernsehgottesdienst.de">aktuellen Gottesdienstübertragungen im ZDF</a>
