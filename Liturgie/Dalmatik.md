---
title: Dalmatik
tags: Dalmatik, Messgewand, Diakon, kath
---

Das Messgewand, die <a href="http://www.kath.de/lexikon/liturgie/index.php?page=kasel.php">Kasel</a>, die der Diakon trägt, der Name leitet sich von Dalmatien her, weil die Wolle, aus der das Gewand gefertigt wurde, aus Dalmatien bezogen wurde. Das Messgewand des Diakons ist daran zu erkennen, dass seitlich zwei Streifen senkrecht von oben nach unten über die vordere wie die hintere Seite des Gewandes fallen.
