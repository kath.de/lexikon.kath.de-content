---
title: Eucharistie
tags: Eucharistie, Liturgie, Hochgebet, Abendmahl, kath
---

Dieses griechische Wort bezeichnet den zentralen christlichen Gottesdienst, das Abendmahl im evangelischen Sprachgebrauch, die Messe, wie die Eucharistiefeier auch in der katholischen Kirche genannt wird. Eucharistie kommt vom griechischen Verb "danken".
Es werden also nicht nur Dankgottesdienste gefeiert, wie z.B. zum Erntedank, nach der Wiedervereinigung Deutschlands oder wie früher nach dem Ende einer Pestepidemie. Jede Messe ist im Kern Dank. Eine Grundidee braucht jedes feierliche Mahl, das nicht nur der Sättigung dient. Eine Geburtstagsfeier wird meist als Essen arrangiert und jeder erwartet, dass einige Worte gesprochen werden. Diese haben oft auch den Dank als Thema, das Geburtstagskind dankt für die Jahre, die ihm geschenkt wurden, seinem Ehegatten und den Freunden, denn in der Regel sind zu der Feier die Menschen eingeladen, die dem Geburtstagskind nahe stehen.
In einem dänischen Film wird diese Grundstruktur des festlichen Mahles vor Augen geführt, das im Verlauf des Mahles ein deutendes Wort verlangt. Der Film heißt "Babettes Fest". Babette ist eine Französin, die es im 19. Jahrhundert an die Westküste Dänemarks verschlagen hat. Sie kommt bei zwei Pfarrerstöchtern unter. Eines Tages erhält sie einen Lottogewinn und veranstaltet mit dem Geld ein großes Fest. Eingeladen ist die kleine puritanische Gemeinschaft, die vom Vater der Schwestern aufgebaut worden war. Diese haben auf Grund ihrer asketischen Lebenseinstellung große Schwierigkeiten, sich den Genuß zu gönnen, den Babette ihnen bereitet. Ein ehemaliger General, der in Paris Militärattaché war, erkennt nicht nur, wer die Köchin sein muß. Sie hatte das bekannteste Restaurant geführt, er deutet das Fest in einer kleinen Tischrede als Erfahrung, die die Gemeinschaft durch das Festessen machen können: Jede Hoffnung nach Glück wird einmal in Erfüllung gehen. Der Film zeigt, wie sich die Herzen der Menschen, die etwas verbittert waren, sich öffnen und sie wieder von dem Glück überzeugt werden, das ihr Glaube ihnen verspricht.
In der Eucharistie danken die Christen Gott für seinen Sohn. Dass er ihn als den <a href="index.php?page=messias.php">Messias</a> gesandt hat, daß durch seinen Lebensweg Erlösung geschehen, das Böse überwunden ist und ein neues Leben begonnen hat, das in die ewige Anschauung Gottes, das Leben im Himmel, münden wird. Im Ablauf des Kirchenjahres konkretisiert sich der Dank in den einzelnen Festen, Dank für die Geburt Jesu, für sein Leiden, seine Auferstehung, seine Himmelfahrt, die Sendung des Geistes, für seine Gegenwart in den eucharistischen Gaben an Fronleichnam und für das Geschenk der Gnade, das sich an den Heiligenfesten auf einzelne Menschen bezieht, "in denen die Gnade Gottes besonders wirksam geworden ist".
Dieser Dank wird in der <a href="index.php?page=praefation.php">Präfation</a> zum Ausdruck gebracht, die in feierlichen Gottesdiensten gesungen wird, jedoch auch Bestandteil jeder Werktagsmesse ist.
Aus dem Danken folgt logisch der Grund, wofür gedankt wird, die Erinnerung, griechisch <a href="index.php?page=anamese.php">Anamnese</a>. An das Erinnern schließen sich wie bei einer Geburtstagsfeier Wünsche an. Man wünscht dem Geburtstagskind Gesundheit und "noch viele Jahre", im Gottesdienst wünscht die Gemeinde von Gott, dass sie z.B. den inneren Frieden bewahrt, sie betet für den Papst, den Bischof, die Regierenden, für Menschen in Not.
Mit der Präfation wird das zentrale Gebet der eucharistischen Mahlfeier eingeleitet, sie ist der erste Teil des <a href="index.php?page=hochgebet.php">Hochgebetes</a>.


<i><b>Zitate:
Weihnachtspräfation</b>
In Wahrheit ist es würdig und recht, dir, Herr, Heiliger Vater, allmächtiger, ewiger Gott, immer und überall zu danken. Den  Fleisch geworden ist das Wort, und in diesem Geheimnis erstrahlt dem Auge unseres Geistes das neue Licht seiner Herrlichkeit. In der sichtbaren Gestalt des Erlösers läßt du uns den unsichtbaren Gott erkenn, um in uns die Liebe zu entflammen zu dem Was kein Auge geschaut hat.

<b>Präfation vom Leiden Christi</b>
In Wahrheit ist es würdig und recht, die, allmächtiger Vater, zu danken und das Werk deiner Gnade zu rühmen. Denn das Leiden deines Sohnes wurde zum Heil für die Welt. Seine Erlösungstat bewegt uns, deine Größe zu preisen. Im Kreuz enthüllt sich dein Gericht, im Kreuz erstrahlt die Macht des Retters, der sich für uns dahingab, unser Herr Jesus Christus.

<b>Pfingstpräfation</b>
In Wahrheit ist es würdig und recht, dir, Herr, heiliger Vater, immer und überall zu danken und diesen Tag in festlicher Freude zu feiern. Denn heute hast du das österliche Heilswerk vollendet, heute hast du den Heiligen Geist gesandt über alle, die mit Christus auferweckt und zu deinen Kindern berufen hast. Am Pfingsttag erfüllst du diene Kirche mit Leben</i>

<b><a href="http://www.magnificat-das-stundenbuch.de/de/Die-Eucharistiefeier.html?utm_source=Onlinewerbung&utm_medium=Bannerwerbung&utm_campaign=kath_de_Eucharistie">Weitere Informationen zu diesem Thema finden Sie hier bei Magnificat.</a></b>

