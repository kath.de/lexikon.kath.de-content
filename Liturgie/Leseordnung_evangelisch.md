---
title: Leseordnung der ev. Kirche
tags: Leseordnung ev. Kirche, Trinitatis, Evangelium, kath
---

In den Kirchen gibt es alte Traditionen, welche Texte aus der Bibel gelesen werden. Das ist vor allem für die Sonntage wichtig, weil hier mehr Gläubige sich versammeln und die Chance haben, Texte aus der Bibel zu hören. Während die katholische Kirche ihre Leseordnung sowohl nach dem Konzil von Trient (1545 - 63) wie nach dem II. Vatikanischen Konzil geändert hat, sind die Evangelischen Landeskirchen in Deutschland bei der altkirchliche Ordnung geblieben.
Die evangelische Kirche hat auch die alte Zählweise für die Sonntage beibehalten, die nicht in die Festzeiten Advent-Weihnachten bzw. Fastenzeit-Osterzeit fallen. Sie zählt wie früher diese als "Sonntage nach Epiphanie" und "Sonntage nach Dreifaltigkeit" oder "Trinitatis" (das ist der Sonntag nach Pfingsten).
Im evangelischen Gottesdienst werden an den Sonn- und Feiertagen mindestens zwei Lesungen vorgetragen, auch drei sind möglich, immer wird ein Text aus einem der vier Evangelien gelesen. Die Leseordnung wird nicht wie in der katholischen Kirche in einem Turnus von drei Jahren gewechselt, sondern bleibt gleich. Die evangelische Liturgie kennt jedoch neben den Lesungen noch eigene Texte, die der Predigt zugrunde gelegt werden. Hier gibt es sechs Zyklen. Die Reihe 1 orientiert sich an den Evangelientexten der Sonntage, die Reihe 2 an den Lesungen, die an den Sonntagen aus den neutestamentlichen Briefen entnommen werden. Die Reihen 3 und 5 legen weitere Texte aus den Briefen zugrunde, die Reihen 4 und 6 wählen aus weiteren Evangelientexten aus. Es werden also der Predigt mehr Texte zugrunde gelegt als die Texte, die für die Lesungen vorgesehen sind. In den Reihen 3-6 findet sich jeden 4. Sonntag ein Predigttext aus dem Alten Testament.

<a href="/seiten/team.html#eb">Eckhard Bieger S.J.</a>
