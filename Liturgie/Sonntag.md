---
title: Sonntag/Sabbat
tags: Sonntag, Sabbat, Auferstehung, Samstag, achter Tag, kath
---

Der Sonntag ist nach dem jüdischen Kalender der erste Tag der Woche, an dem die Christen  die <a href="http://www.kath.de/lexikon/liturgie/index.php?page=auferstehung.php">Auferstehung</a> Jesu Christi feiern, indem sie sein Gedächtnismahl begehen, das Jesus mit dem Auftrag verbunden hat: "Tut dies zu meinen Gedächtnis" (Lukas 22,19).
In die christliche Sonntagspraxis ist die jüdische Sabbat-Tradition eingeflossen. Der Sabbat, der jeweils samstags von den Juden gefeiert wird, bezieht sich auf den siebten Tag der Schöpfung, an dem Gott ruhte. Im Buch Genesis heißt es: "Und Gott segnete den siebten Tag und erklärte ihn für heilig; denn an ihm ruhte Gott, nachdem er das ganze Werk der Schöpfung vollendet hatte" (Gen 2,3). Der Sonntag ist entsprechend diesem Wochenrhythmus der <a href="http://www.kath.de/lexikon/liturgie/index.php?page=achter_tag.php">achte Tag</a>, an dem die neue Schöpfung beginnt. Die Vollendung, die Freude an allem, was Gott geschaffen hat, und der Dank für die Taten seiner Erlösung gehören zum Sonntag.
