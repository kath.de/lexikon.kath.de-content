---
title: Regina caeli
tags: Regina caeli, Angelus, Stundengebet, Maria, kath
---

Regina, die Königin, des Himmels, wird in der Osterzeit mit einem eigenen Gebet geehrt, das an die Stelle des Angelus, des Engel des Herrn tritt. Maria wird an Ostern als Himmelskönigin angesprochen, weil sie wie ihr Sohn mit Lieb und Seele im Himmel bei Gott ist. Das Fest <--a href="/lexikon/kirchenjahr/index.php?maria_himmelfahrt.php"-->Maria Himmelfahrt<--/a--> am 15. August ist diesem Geheimnis gewidmet. Das Gebet, das auch gesungen werden kann, wir immer wieder von dem Freudenruf Halleluja unterbrochen.
<blockquote>Freu dich, du Himmelkönigin, Halleluja!
Den du zu tragen würdig warst, Halleluja!
Er ist auferstanden, wie er gesagt hat, Halleluja!
Bitt Gott für uns Halleluja!

Vorbeter:         Freu dich und frohlocke, Jungfrau Maria, Halleluja
Gemeinde:         denn der Herr ist wahrhaft auferstanden, Halleluja

Vorbeter: Lasset uns beten. - Allmächtiger Gott, durch die Auferstehung deines Sohnes unseres Herrn Jesus Christus, hast du die Welt mit Jubel erfüllt. Lass uns durch seine jungfräuliche Mutter Maria zur unvergänglichen Osterfreude gelangen. Darum bitten wir durch Christus, unseren Herrn.
Amen.</blockquote>
Der lateinische Ursprungstext, der wohl in Rom im 12. Jahrhundert gedichtet und komponiert wurde, lautet
<blockquote>Regina caeli, laetare Halleluja!
Qui, quem meruisti portare, Hallelluja!
Resurrexit sicut dixit Halleluja!
Ora pro nobis Maria, Halleluja!</blockquote>
