---
title: Weihwasser
tags: Weihwasser, Taufe, Kirche, Leben, kath
---

Das stehende, leicht verderbliche Wasser galt früher als der Sitz dämonischer Kräfte. Deshalb wurde es vor dem Gebrauch exorziert (durch wirkkräftige Spruchformeln von den Dämonen befreit) und gesegnet und gesalzen. Dies geschieht besonders in der Osternacht. Im Weihwasserbecken an den Türen jeder Kirche werden so die Eintretenden an ihre Taufe erinnert, indem sie sich mit dem Weihwasser bekreuzigen, so wie Wasser in Kreuzesform über  das Haupt des Täuflings gegossen wird. Ebenso wurden früher vor dem Hochamt am Sonntag die Gemeinde und der Altar mit geweihtem Wasser besprengt. Dieser Ritus wurde nach der dabei (außerhalb der österlichen Zeit) gesungenen Antiphon als <a href="http://www.kath.de/lexikon/liturgie/index.php?page=asperges.php>Asperges</a> bezeichnet. Weihwasser und Weihwasserbecken finden sich auch auf Gräbern und in Haushalten.
