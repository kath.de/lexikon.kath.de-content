---
title: Passahfest oder Pessach
tags: Passahfest, Pessach, Juden, Lamm, kath
---

Das wichtigste Fest der Juden, das nach dem ersten Frühlingsvollmond, am 14. Nisan, gefeiert wird und an die Befreiung aus Ägypten erinnert. Es wird durch das Passahmahl begangen, in dessen Mittelpunkt das <a href="http://www.kath.de/lexikon/liturgie/index.php">Lamm</a> steht. Jesus hat mit seinen Jüngern das Passahfest gefeiert und ergänzende Riten, das Herumreichen des Brotes und des letzten Weinkelches zur Formung des christlichen Gedächtnismahles herausgegriffen. Während der jüdische Festtermin auf verschiedene Wochentage fallen kann, haben die Christen das Osterfest jeweils auf den ersten Wochentag der jüdischen Zählung gelegt, weil im Todesjahr Jesu der Auferstehungstag auf den ersten Wochentag nach dem Sabbat der jüdischen Woche gefallen ist und sich daraus der Sonntag entwickelt hat. Da Jesus in der Zeit des Passahfestes hingerichtet wurde und nach dem Passahfest als Auferstandener seinen Jüngern erschien, bleibt das zentrale Fest der Christen mit dem jüdischen Hauptfest verbunden.
