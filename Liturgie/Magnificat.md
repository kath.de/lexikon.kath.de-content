---
title: Magnificat
tags: Magnifikat, Lukas, Maria, Elisabeth, kath
---

Dieses Lied wird von Lukas überliefert, Kap. 1,46-55. Maria hat es gesprochen, als sie ihre Kusine Elisabeth besuchte, die im 6. Monat schwanger war. Das Magnifikat wird in jeder Vesper nach dem kurzen Lesungstext gebetet. Magnifikat kommt von "magnus", groß und "preisen". Im Text heißt es: "Meine Seele preist die Größe des Herrn."
Da sagte Maria: Meine Seele preist die Größe des Herrn, und mein Geist jubelt über Gott, meinen Retter.
Denn auf die Niedrigkeit seiner Magd hat er geschaut. Siehe, von nun an preisen mich selig alle Geschlechter.
Denn der Mächtige hat Großes an mir getan und sein Name ist heilig.
Er erbarmt sich von Geschlecht zu Geschlecht über alle, die ihn fürchten.
Er vollbringt mit seinem Arm machtvolle Taten: Er zerstreut, die im Herzen voll Hochmut sind;
er stürzt die Mächtigen vom Thron / und erhöht die Niedrigen.
Die Hungernden beschenkt er mit seinen Gaben und lässt die Reichen leer ausgehen.
Er nimmt sich seines Knechtes Israel an und denkt an sein Erbarmen, das er unsern Vätern verheißen hat, Abraham und seinen Nachkommen auf ewig.
Lukas 1, 46-55

<b><a href="http://www.magnificat-das-stundenbuch.de/de/Inhalte.html?utm_source=Onlinewerbung&utm_medium=Bannerwerbung&utm_campaign=kath_de_Inhalte">Weitere Informationen zu diesem Thema finden Sie hier bei Magnificat.</a></b>
