---
title: Chormantel
tags: Chormantel, Prozession, Andacht, Bischof, Priester, kath
---

Während die <a href="http://www.kath.de/lexikon/liturgie/index.php?page=kasel.php">Kasel</a> bei der Messe getragen wird, ist der Chormantel, früher auch "Vespermantel" oder Rauchmantel, (lat. pluviale "Regengewand") für Prozessionen und den Gang zum Friedhof entwickelt. Bei feierlichen Andachten trägt der Priester oder Bischof ebenfalls einen Chormantel. Währen die Kasel so geschnitten ist, das die Arme frei beweglich sind, umschließt der Chormantel wie ein Überwurf die Arme und muss daher vorne offen sein.
