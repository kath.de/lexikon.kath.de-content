---
title: Prim
tags: Prim, Stundengebet, kath
---

Die Prim Auf dem zweiten vatikanischen Konzil wurde die Prim (lat. prima (hora) = erste Stunde), die zur ersten Stunde der antiken Zeiteinteilung (06.00 Uhr unserer Zeit) gefeiert wurde, abgeschafft. Sie war eigentlich eine Doppelung zur Laudes. Lediglich die Karthäuser haben Sie in ihrem Stundengebet erhalten. Sie gehört zu den <a href="index.php?page=kleine_hore.php">kleinen Horen</a>.

Jürgen Pelzer
