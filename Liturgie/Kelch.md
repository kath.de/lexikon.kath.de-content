---
title: Kelch
tags: Kelch, Becher, Eucharestie, kath
bild:kelch.jpg

Dieses Gefäß ist ein <a href="http://www.kath.de/lexikon/liturgie/index.php?page=becer.php">Becher</a> mit einem Stiel. Der Kelch nimmt den Wein und einen Tropfen Wasser auf, über die der Priester die Wandlungsworte spricht. Daher ist der Kelch meist mit eucharistischen Symbolen oder der Abendmahlszene verziert. Kelche können einfach aus Ton gebrannt sein, aus Messing, Silber oder Gold getrieben. Biblische Bezüge sprechen von dem Kelch des Heils. Wenn in einer Darstellung, die Jesus vor seiner Gefangennahme betend zeigt, ein Kelch zu sehen ist, spielt das auf das Wort Jesu an: "Vater, wenn du willst, lass diesen Kelch an mir vorüber gehen." Der Kelch kann sowohl auf Heil und Erlösung hindeuten wie auf das Schwere, das der Mensch zu tragen hat.
