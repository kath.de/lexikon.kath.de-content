---
title: Sext
tags: Sext, Stundengebet, kath
---

Die Sext von lateinisch sexta hora = sechste Stunde) wird zur sechsten Stunde der römischen Zeitrechnung, also um 12.00 Uhr unserer Zeit gefeiert. Die Sext erwähnt die Arbeit im Weinberg des Herrn. Sie gehört zu den ==>kleinen Horen.
Hippolyt:

" Bete ähnlich auch zur Zeit der 6. Stunde.
Als nämlich Christus am Kreuze angenagelt war,
zerfiel der Tag und große Finsternis trat ein.
So soll man zu dieser Stunde mit mächtiger Stimme beten
und die Stimme dessen nachahmen, der betete
und die Schöpfung zur Finsternis werden ließ
wegen der Ungläubigkeit der Juden."

Jürgen Pelzer
