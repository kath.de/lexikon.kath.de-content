---
title: Priester
tags: Priester, Gottesdienst, kath
---

Für sie ist der Kirchenraum der wichtigste Aktionsraum ihrer Beauftragung, nämlich die Gläubigen zum Gottesdienst zu begrüßen, sich im Namen der Versammelten im Gebet an Gott zu wenden, das Evangelium zu verkünden und auszulegen; das eucharistische Hochgebet zu sprechen und die gewandelten Gaben zu reichen. Als Leiter des Gottesdienstes steht der Priester der Versammlung vor.
Andere Orte priesterlichen Handelns sind das Taufbecken und der Beichtstuhl. Vor dem Priester bzw. Diakon als amtlichem Zeugen der Kirche spenden sich die Brautleute das Sakrament der Ehe.
Im Gottesdienst ist der Priester nicht der einzige Rollenträger: Lektoren tragen die Lesungen aus der Heiligen Schrift vor und Kommunionspender bringen das eucharistische Brot zu den Kranken nach Hause und teilen es in der Kirche aus.
<a href="http://www.kath.de/lexikon/liturgie/index.php?page=messdiener.php">Messdiener und -dienerinnen</a> sind die Nachfolger der <a href="http://www.kath.de/lexikon/liturgie/index.php?page=akolyth.php">Akolythen</a> der frühen Kirche, d.h. sie tragen die Kerzen, halten und schwenken das < a href="http://www.kath.de/lexikon/liturgie/index.php?page=weihrauchfass.php">Weihrauchfass</a> und bringen Brot, Wein und Wasser zur Bereitung des eucharistischen Mahles zum <a href="http://www.kath.de/lexikon/liturgie/index.php?page=altar.php">Altar</a>.
