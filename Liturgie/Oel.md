---
title: Oel, heilige Öle
tags: Öl, heilige Öle, König, Salbe, kath
---

Mit Öl hat Samuel zuerst Saul und dann David zu <a href="http://www.kath.de/lexikon/liturgie/index.php?page=koenig.php">Königen</a> gesalbt. David nennt Saul öfters den "Gesalbten des Herrn". Der Gesalbte ist der Messias, der Christos (von griechisch chrinein = salben). Weil die Getauften zu Brüdern und Schwestern Christi werden, sind sie mit <a href="http://www.kath.de/lexikon/index.php?page=chrisam.php">Chrisam</a> Gesalbte, also Christen. Ergänzt wird die Bedeutung des Öls durch seine heilende Wirkung, die durch die Krankensalbung an Menschen vollzogen wird.
Heilige Öle gehören zu den Urelementen christlicher Liturgie. Für den antiken Menschen waren die Öle Nahrung, Medizin, Kosmetik, Opfergabe und Lichtquelle. Der Jakobusbrief bezeugt die auch medizinisch zu verstehende Krankensalbung. Nach alttestamentlichen Vorbildern finden Salbungen beim Eintritt in die Glaubensgemeinschaft (Taufe und Firmung) und bei der Übertragung apostolischer Leitungsgewalt (Priester- und Bischofsweihe) statt, aber auch, um gottesdienstliche Geräte und Räume zu heiligen.
Christsein heißt vom Wortsinn her: Gesalbtsein (jüdisch: Messias: der Gesalbte, lateinisch Christus; vgl. daher Chrisam). Die heiligen Öle werden in der Chrisammesse am Gründonnerstag durch den Bischof geweiht. In jeder Pfarrkirche finden sich drei Öle: Katechumenenöl und Chrisam zur Salbung vor und nach der Taufe und das Gefäß mit dem Krankenöl.
