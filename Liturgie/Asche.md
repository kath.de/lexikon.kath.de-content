---
title: Asche
tags: Asche, Aschermittwoch, Aschekreuz, kath
---

Aus den Zweigen, die am Palmsonntag geweiht wurden, wird im darauf folgenden Jahr die Asche gewonnen, die den Gläubigen am Aschermittwoch nach dem Verlesen des Evangeliums auf das Haupt gestreut oder in Form eines Kreuzes auf die Stirn gezeichnet wird. "Bedenke Mensch, dass du Staub bist und wieder zum Staub zurückkehren wirst", hört der Gläubige. Im Sinne der Fastenzeit kann die Bezeichnung mit dem Aschenkreuz auch so lauten: "Bekehrt euch und glaubt an das Evangelium."
