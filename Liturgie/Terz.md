---
title: Terz
tags: Terz, Stundengebet, kath
---

Die Terz von lateinisch tertia (hora) = dritte Stunde, ist ein Teil des <a href="index.php?page=stundengebet.php">Stundengebets</a> und wird zur dritten Stunde der römischen Zeitrechnung, also um 09.00 Uhr unserer Zeit gefeiert. Im Hymnus begegnet täglich das Thema der Herabkunft des Heiligen Geistes.
Hippolyt von Rom: "Bete zur dritten Stunde und preise den Herrn. Denn zu dieser Stunde wurde Christus geschaut: angenagelt ans Kreuz. Deshalb auch hat im Alten Bund das Gesetz schon vorgeschrieben, daß zu jeder Zeit Schaubrote dargestellt würden als Vor-bild (Typos) des Leibes und Blutes Christi. Und die Hinopferung eines vernunftlosen Lammes ist Typos des vollkommenen Lammes. Dieses ist Christus, der zugleich Hirt ist, und das Brot, das vom Himmel herabstieg."

Jürgen Pelzer

