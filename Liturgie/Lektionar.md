---
title: Lektionar
tags: Lektionar, Lesung, Evangelium, kath
bild:lektionar.jpg

Vom lateinischen Wort für Lesen abgleitet ist es das Buch, aus dem die Lesungen und Evangelientexte während des Gottesdienstes vorgetragen erden. Das Lektionar wird vom Diakon beim Einzug zum Gottesdienst  hereingetragen, der das Buch mit beiden Händen hoch hält. Wenn in einem Lektionar nur die Evangelientexte enthalten sind, wird das Buch <a href="http://www.kath.de/lexikon/liturgie/index.php?page=evangeliar.php">Evangeliar</a> genannt.
