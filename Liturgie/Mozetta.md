---
title: Mozetta
tags: Mozetta, Umhang, Bishof, Kardinal, kath
---

Ein Umhang, der die Schultern bedeckt. Er wird von Kardinälen und Bischöfen über der <a href="index.php?page=soutane.php">Soutane</a> getragen, aber auch vom höheren Klerus, so den Mitgliedern des Domkapitels und von einigen Orden getragen. Die Mozetta der Kardinäle hat eine kleine Kapuze. Das Wort kommt vom italienischen "abgeschnitten".
