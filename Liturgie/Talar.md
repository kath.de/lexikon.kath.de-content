---
title: Talar
tags: Talar, Priestergewand, Rochett, Soutane, kath
---

Ein meist schwarzes Gewand, das bis zu den Knöcheln reicht. Es hat sich mit der Gründung der mittelalterlichen Universitäten entwickelt und gilt auch als Amtskleidung von Richtern. Der Talar der lutherischen Geistlichen leitet sich von dem Universitäten her. Der Talar wird ohne Zingulum getragen und hat meistens eine Rückenfalte.
Priester tragen bei liturgischen Feiern, die keine Messe sind, z.B. bei Beerdigungen oder andachten und Prozessionen, einen Talar, darüber ein weißes <a href="http://www.kath.de/lexikon/liturgie/index.php?page=rochett.php">Rochett</a> mit <a href="http://www.kath.de/lexikon/liturgie/index.php?page=stola.php">Stola</a>. Der Talar, den Priester in romanischen Ländern als Straßenkleidung tragen, wird <a href="http://www.kath.de/lexikon/index.php?page=soutane.php">Soutane</a> genannt. Jedoch ist der Talar nicht den Priestern vorbehalten, er soll eigentlich von allen getragen werden, die im Chorraum liturgische Dienste tun. So tragen die Messdiener einen Talar, auch viel Küster und in manchen Gemeinden Lektoren und Kommunionhelfer.
