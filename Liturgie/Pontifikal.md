---
title: Pontifikal, Pontifikale
tags: Pontifikal, Pontifikale, Bischof, Brücke, kath
---

Das Wort kommt von dem lateinischen Wort Pons-Brücke. Pontifex, Brückenbauer wird vor allem der Papst genannt. Im Sprachgebrauch der katholischen Kirche bezieht sich das Adjektiv "pontifikal" auf liturgische Handlungen, die der Bischof durchführt. Eine Pontifkal-Vesper ist eine mit dem Bischof gefeiert, ebenso eine Ponifikalamt eine Messe, die der Bischof feiert.
Das Pontifikale ist ein liturgisches Buch, das die <a href="http://www.kath.de/lexikon/liturgie/index.php?page=rubrik.php">Texte und Anleitungen</a> für liturgische Handlungen eines Bischofs enthält, so für die Weihe von <a href=http://www.kath.de/lexikon/liturgie/index.php?page=ostirarier.php">Ostiariern</a>, <a href="http://www.kath.de/lexikon/liturgie/index.php?page=akolyth.php">Akolythen</a>, <a href="http://www.kath.de/lexikon/liturgie/index.php?page=lektor.php">Lektoren</a>, <a href="http://www.kath.de/lexikon/liturgie/index.php?page=diakon.php">Diakonen</a>, <a href="http://www.kath.de/lexikon/liturgie/index.php?page=priester.php">Priestern</a>, Äbten und Äbtissinnen, Bischöfen, Kirchen- und Altarweihe sowie für <a href="http://www.kath.de/lexikon/liturgie/index.php?page=oel.php">liturgische Öle</a>
