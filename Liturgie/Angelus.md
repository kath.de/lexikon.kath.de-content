---
title: Angelusläuten - Engel des Herrn
tags: Angelusläuten, Engel des Herrn, Liturgie, Hore, Stundengebet, kath
---

Dreimal am Tag läutet die Glocke zum "Engel des Herrn", jeweils morgens, zur Mittagszeit und am Abend. Das Gebet beginnt mit dem Gruß des Engels an Maria, daher "Angelus", lateinisch Engel.

Der Engel des Herrn brachte Maria die Botschaft -
und sie empfing vom Heiligen Geist
Gegrüßet seist du Maria 
Maria sprach: Siehe ich bin die Magd des Herrn -
mir geschehe nach deinem Wort.
Gegrüßet seist du Maria 
Und das Wort ist Fleisch geworden -
und hat unter uns gewohnt
Gegrüßet seist du Maria .
Bitte für uns, heilige Gottesmutter;
dass wir würdig werden der Verheißungen Christi.
Lasset uns beten: Allmächtiger Gott, gieße deine Gnade in unsere Herzen ein. Durch die Botschaft des Engels haben wir die Menschwerdung Christi, deines Sohnes, erkannt. Lass uns durch sein Leiden und Kreuz zur Herrlichkeit der Auferstehung gelangen. Darum bitten wir durch Christus unsern Herrn. Amen

Das abschließende Gebet spannt den Bogen von der Menschwerdung des Sohnes Gottes bis zu seinem Kreuz und seiner Auferstehung. Darauf hinzuweisen ist, dass dieses Gebet sich nicht mehr wie das "Gegrüßet seist du" an Maria richtet, sondern an den Vater, zu dem die Christen "durch" Jesus Christus, den Mittler zwischen den Menschen und Gott, beten.

Der Engel des Herrn ist ein kleines Stundengebet, das man auf der Straße, auch im Büro oder vor dem Essen beten kann. Morgens, mittags und abends sind auch die drei Hauptzeiten des <a href="index.php?page=stundengebet.php">Stundengebets</a> Die <a href="index.php?page=laudes.php">Laudes</a>, das Morgenlob, die <a href="index.php?page=kleine_hore.php">Sext</a> Mittags zur sechsten Stunde um 12 Uhr und das Abendgebet, die <a href="index.php?page=vesper.php">Vesper</a>.
Von dem Stundegebet übernimmt der Engel des Herrn auch den Wechsel, nicht wie im Stundengebet zwischen zwei Chören, sondern zwischen Vorbeter und den Mitbetenden. Bei den Drei Sätzen antworten die Mitbetenden nach dem Bindestrich, beim Gegrüßet seist Du Maria sprechen sie die abschließende Bitte:
Heilige Maria, Mutter Gottes, bitte für uns Sünder jetzt und in der Stunde unseres Todes. Amen

<a href="http://www.kath.de/seiten/team.html#eb">Eckhard Bieger</a>

