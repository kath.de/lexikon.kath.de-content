---
title: Becher
tags: Becher, Kelch, Passah, kath
---

Beim jüdischen Passahfest, aus dem das Abendmahl hervorgegangen ist, wird Wein in einem Becher herumgereicht worden. Alle tranken aus dem gleichen Becher so wie heute aus dem Kelch. Der Becher wird nämlich im Gottesdienst meist mit einem Stil als <a href="index.php?page=kelch.php">Kelch</a> benutzt. Becher kann auch im übertragnen Sinne gebraucht werden, wenn z.B. ein "Becher des Zornes" ausgegossen wird.
