---
title: Asperges
tags: Asperges, besprengen, Taufe, Ysop, kath
---

Dieses lateinische Wort für "besprengen" bezeichnet einen Ritus, der vor Beginn des Sonntagsgottesdienstes seinen Platz hat. Der Priester geht durch die Reihen und besprengt die Gläubigen mit Weihwasser. Der Ritus ist eine Erinnerung an die Taufe. Der Vers, der dabei von Chor und Gemeinde gesungen wird, heißt übersetzt: "Besprenge mich Herr mit Ysop und ich werde rein. Wasche mich und ich werde weißer als Schnee." Dieser Vers steht in dem Bußpsalm 51,9
