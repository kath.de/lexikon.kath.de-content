---
title: Ciborium auch Ziborium
tags: Ciborium, Ziborium, Kelch, Hostie, kath
---

In Kelchform oder auch ohne Stil genutztes Gefäß für die Aufbewahrung konsekrierter, d.h. in der Messe übriggebliebener Hostien, die im Tabernakel aufgehoben werden. Das Ciborium ist meist mit einem kleinen Stoffstück umkleidet, auch Velum, Segel genannt. In der Baukunst nennt man Ciborium auch die Kuppel über der Vierung. Das Wort leitet sich vom lateinischen "Trinkbecher" her
