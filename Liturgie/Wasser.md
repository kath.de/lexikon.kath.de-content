---
title: Wasser
tags: Wasser, Reinigung, Sakrament, Taufe, Weihwasser, neues Leben, kath
---

Im Alltag dient das Wasser der Reinigung. Es ist mit der Kultform der rituellen Waschung verbunden. Beim Eintreten in die Kirche bekreuzigen sich die Gläubigen mit geweihtem Wasser. Das rituelle Händewaschen des Priesters vor Beginn des Hochgebetes soll seine Hände von den Vorbereitungen reinigen, wird aber durch das Gebet, das der Priester während der Händewaschung leise betet, ausdrücklich als Reinigung von Schuld verstanden.
Wasser ist wie Blut ein zentrales Symbol für das Leben und so auch für den Eintritt in die christliche Gemeinschaft. In der Taufe, früher durch Untertauchen, wie heute noch bei den Baptisten üblich, bewirken Wasser und Geist neues Leben. Zugleich geschieht die Reinigung von Sünde und Schuld. Im Akt der Taufe wird der Getaufte so in das Wasser, das Symbol der  Vergänglichkeit, hineingenommen, dass er gleichsam symbolisch stirbt, aber auch wieder in das Licht und das Leben aufgerichtet wird. Dieses Sakrament steht für den Glauben: Sind wir mit Christus gestorben, werden wir auch zum Leben auferstehen.
Durch das Hinzufügen des Wassers zum Wein, im Mittelmeerraum ein üblicher Umgang mit dem Getränk, wird auf das Einswerden von Gottheit und Menschheit in Jesus Christus
hingewiesen. Bei der Bereitung des eucharistischen Mahles betet der Priester: "Wie das Wasser sich mit dem Wein verbindet zum heiligen Zeichen, so lasse uns dieser Kelch teilhaben an der Gottheit Christi, der unsere Menschennatur angenommen hat."
Wasser ist jedoch, wie alle Symbole, ambivalent: Als aufgewühltes Meer oder als Unwetter ist es bedrohlich und kann den Menschen schwer schaden. Die Wellenlinien, die das Wasser kräuseln, sind Zeichen der Vergänglichkeit und so Ausdruck der menschlichen Existenz. Siehe auch <a href="http://www.kath.de/lexikon/liturgie/index.php?page=weihwasser.php">Weihwasser</a>.
