---
title: Exsultet
tags: Exsultet, Osternacht, Lobpreis, Osterlob, kath
---

<h2>das Osterlob der Osternachtfeier eröffnet</h2>

Die Feier der Osternacht beginnt erst, wenn es dunkel geworden ist. Dann zog sie sich in den frühen Jahrhunderten durch die ganze Nacht. Wenn morgens dann die Sonne aufging, wurde das Oster-Halleluja angestimmt und das Osterevangelium verkündet. Durch die ganze Nacht hindurch wachte die Kirche dieser Botschaft entgegen. Lesungen und Gesänge aus dem Alten Testament füllten die Nacht, dazu die Taufe der neuen Christen. Naturnotwendig gehört zu solchen nächtlichen Gottesdiensten die Symbolkraft des Lichtes. Auch andere nächtliche Gottesdienste während des Jahres wurden durch eine Segnung des Lichtes und ein Loblied auf Christus, der unser Licht ist, eröffnet, und in manchen Klöstern geschieht das heute noch. Doch nie war das Lob des Lichtes so festlich wie in der Osternacht. Feierlich wurde und wird heute noch die entzündete Osterkerze in die dunkle, menschengefüllte Kirchenhalle hineingetragen. Dreimal singt der Diakon, jedesmal in höherem Ton: "Lumen Christi" - "Das Licht des Christus." Alle antworten: "Deo gratias" - "Gott sei gedankt." Die Gläubigen entzünden ihre Kerzen am Licht der Osterkerze. Die Osterkerze wird auf einen großen Leuchter gestellt. Der Diakon tritt vor sie hin und singt nun den schönsten kantillierenden Gesang, den die lateinische Liturgie kennt, das "Exsultet". Es stammt aus dem Schülerkreis des heiligen Ambrosius von Mailand. Es hatte viele Vertonungen, aber eine hat sich am Ende durchgesetzt, sicher die schönste.

<h2>Theologie des Exsultet</h2>
Es ist ein Lobgesang auf das Licht. Doch zugleich ist es ein Lobgesang auf das ganze Ostergeheimnis, und im Kern ist es ein Lobgesang auf den erstandenen Christus, den die Osterkerze zeichenhaft darstellt. Zugleich aber ist es der deutende Schlüssel für all die Lesungen aus dem Alten Testament, die jetzt bald in der nächtlichen Feier gelesen werden. Was da, mit der Schöpfung beginnend, vorgelesen wird, vor allem der Auszug aus Ägypten, ist schon der Anfang, ja ein inneres Stück des Ostergeheimnisses. Dieses Festgeheimnis beschränkt sich nicht auf Jesu Tod und Auferstehung, sondern reicht zurück bis zu den Anfängen der Welt, und es reicht voraus bis zur Wiederkunft Christi, des jetzt im Himmel Erhöhten. Mit Erwartung an sein Kommen endet das Exsultet - die frühe Christenheit war überzeugt, Christus werde in einer Osternacht wiederkommen. Der Morgen, dem die nächtliche Feier entgegenschreitet, ist für die Feiernden der Morgen am Ende der Weltgeschichte.

<h2>Das Exsultet ist ein Loblied auf die Taten Gottes</h2>
Zuerst zieht der Diakon den Vorhang auf: Er besingt den Einzug des Erstandenen in den Raum des Himmels und das Licht, das sich dadurch in der ganzen Schöpfung ausbreitet, vor allem auch über die jetzt versammelte Gemeinde. Dann bittet er, für ihn selbst zu beten, damit aus seinem Gesang auch wirklich Lob Gottes entsteht. Das ist gewissermaßen die Ouvertüre. Dann folgen die Wechselrufe wie bei jeder Präfation, und im Stil von Präfationen, nur viel festlicher, geht es dann auch weiter. Der Preis geht auf Gott, den unsichtbaren Vater, gleitet über auf seinen Sohn Jesus Christus, auf dessen Tod als das "wahre Lamm", und gelangt so zur jetzigen Nacht. Immer wieder heißt es: "Dies ist die Nacht" - und dabei schieben sich die verschiedenen Nächte ineinander zu einer einzigen, die Nacht des Auszugs aus Ägypten, des Durchzugs durchs Meer, die vielen Nächte des Wüstenzugs Israels hinter der Feuersäule her, die Nacht der Auferstehung Jesu von den Toten, die jetzige Nacht, in der in der ganzen Welt Menschen durch die Taufe "den Heiligen zugesellt" werden. Mitten in diesen Preis der Verschmelzung der Nächte schieben sich, immer wieder mit "O" beginnend, Freudenrufe über das wunderbare Handeln Gottes ein. Dieses Loblied ist ein "Abendopfer". In der Gestalt der Kerze bringt der Diakon es Gott dar und bittet ihn, dieses Opfer anzunehmen. Nochmals klingt das Lob der Nacht auf, und dann folgt die abschließende Bitte: Das Licht der Osterkerze, in dem sich die Hingabe aller Versammelten verdichtet, möge mitten unter den himmlischen Lichtern weiterleuchten bis zum Morgen, wenn die Sonne aufgeht - die Sonne, die Christus ist, der am Ende der Zeiten wiederkehren wird.

<h2>Hinführung auf die Lesungen im Osternachgottesdienst</h2>
Dies ist hohe Dichtung, zugleich ganz durchsetzt mit Bildern und Wörtern der Heiligen Schrift, am Ende vor allem des Hohenliedes der Liebe. Mit diesem Osterlob im Ohr wird man die Lesungen aus dem Alten Testament, die jetzt aufeinander folgen, ganz neu hören können. Sie werden alle in das eine große Geheimnis der Ostern hineingenommen.
Die hier vorgelegte Übersetzung ist genauer und bibelnäher als die Übersetzung in den liturgischen Büchern. Ferner ist sie sprachlich so angelegt, daß sie sich in größerer Entsprechung zur Tonführung des lateinischen Exsultet singen läßt. Die aus diesem Anliegen stammende Vertonung (durch Erwin Bücken) finden Sie hier Link zu www.schimmelpfeng.org/kantill/exsult02.htm

<b>Norbert Lohfink S.J.</b>

<h2>Das Exsultet</h2>
<ol><li>Schon juble in den Himmeln die Menge der Engel,
es juble die Schar der göttlichen Dienste,
und zu solch eines Königs Einzug künde Sieg die Trompete.</li>
<li>Da freue sich auch der Erdkreis, erhellt von leuchtenden Blitzen,
und, angestrahlt von der Pracht des ewigen Königs,
erspüre er, daß er befreit ist vom Dunkel, das alles deckte.</li>
<li>Glückselig sei auch die Mutter Kirche, geschmückt mit solch blitzendem Lichte,
und vom lauten Jubel der Völker töne wider diese Halle.</li>
<li>So bitte ich euch, liebste Brüder und Schwestern,
die ihr steht beim herrlichen Glanz dieses heiligen Lichtes:
Ruft mit mir zu Gott, dem Allmächtigen,
er möge sich meiner erbarmen:</li>
<li>Daß er, der mich von sich aus in die Zahl der Leviten gerufen hat,
mich fülle mit dem Glanz seines Lichtes
und durch mich das Lob dieser Kerze wirke.</li></ol>

Der Herr sei mit euch.
Und mit deinem Geiste.

Erhebet die Herzen.
Wir haben sie beim Herrn.

Lasset uns danken dem Herrn, unserm Gott.
Das ist würdig und recht.

<ol><li>Wahrhaft würdig und recht ist es, den unsichtbaren Gott, den allmächtigen Vater,
und seinen eingeborenen Sohn, unsern Herrn Jesus Christus,
mit aller Inbrunst des Herzens und Geistes,
im Dienste des Wortes, mit lauter Stimme zu preisen -</li>
<li>ihn, der für uns beim ewigen Vater die Schulden Adams bezahlt hat
und ausgelöscht hat den uralten Schuldbrief
mit Blut des Erbarmens.</li>
<li>Dies ist ja das Fest der Ostern,
an dem jenes wahre Lamm getötet wird,
durch dessen Blut die Türen der Gläubigen gefeit sind.</li>
<li>Dies ist die Nacht, in der du am Anfang unsere Väter, die Nachkommen Israels,
nachdem sie herausgeführt waren aus Ägypten,
trockenen Fußes durch das Schilfmeer geleitet hast.</li>
<li>Dies also ist die Nacht, welche die Finsternis der Sünden
durch der Feuersäule Erleuchtung verscheucht hat.</li>
<li>Dies ist die Nacht, die heute auf der ganzen Erde Menschen, die zum Glauben an Christus
gekommen sind, losgelöst von den Lastern der Welt und vom Dunkel der Sünde,
heimführt zur Gnade und den Heiligen zugesellt.</li>
<li>Dies ist die Nacht, da Christus die Fesseln des Todes gesprengt hat
und aus denen, die unter der Erde sind, als Sieger emporstieg.</li>
<li>Denn umsonst wären wir geboren,
wäre keiner gekommen, uns zu erlösen.</li>
<li>O wie du dich über uns neigest
in staunenswertem Erbarmen!</li>
<li>O unerwartbare Zuwendung der Liebe:
Um den Knecht zu erlösen, gabst du den Sohn dahin!</li>
<li>O wahrhaft nötige Sünde Adams,
die getilgt ward vom Tode Christi!</li>
<li>O glückliche Schuld,
der solch ein großer Erlöser geziemte!</li>
<li>O wahrhaft selige Nacht, der einzig es ziemte, die Zeit und die Stunde zu kennen,
da Christus erstanden ist aus denen, die unter der Erde sind!</li>
<li>Dies ist die Nacht, von der geschrieben steht:
"und die Nacht - wie der Tag wird sie leuchten,"
und: "die Nacht ist meine Erleuchtung, sie wird mir zur Wonne."</li>
<li>Die Heiligung also, die in dieser Nacht sich ereignet,
jagt die Verbrechen fort,
spült weg jede Schuld,
gibt Gestrauchelten wieder die Unschuld
und Trauernden Freude.
Feindschaft jagt sie fort,
bereitet die Eintracht
und beugt die Gewalten.</li>
<li>In deiner Gnade also, die diese Nacht durchwaltet,
nimm an, heiliger Vater, das Abendopfer dieses Loblieds,
das dir in dieser Kerze festlicher Darbringung, durch die Hände deiner Diener, aus der Arbeit
der Bienen, entrichtet die hochheilige Kirche.</li>
<li>Doch schon wissen wir, wie sich der Heroldsruf dieser Säule verbreitet,
die das goldene Feuer zur Ehre Gottes entzündet hat:</li>
<li>Wenn es auch vielfach geteilt ist,
weiß es dennoch von keiner Schwächung des weitergereichten Lichtes.</li>
<li>Es nährt sich nämlich vom schmelzenden Wachse,
das als den Reichtum dieser kostbaren Fackel
die Mutter Biene bereitet hat.</li>
<li>O wahrhaft selige Nacht, da werden verbunden
Irdischem Himmlisches, Menschlichem Göttliches.</li>
<li>So bitten wir dich, o Herr:
Diese Kerze, geweiht zur Ehre deines Namens, brenne unermüdlich weiter,
um das Dunkel dieser Nacht zu vernichten.
Als lieblicher Opferduft entgegengenommen,
mische sie sich unter die Lichter am Himmel.</li>
<li>Lodernde Flamme - so soll sie finden der Morgenstern.
Jener Morgenstern nämlich, der keinen Untergang kennt:
Christus, dein Sohn, der, zurückgekehrt aus denen, die unter der Erde sind,
dem Menschengeschlechte heiter aufging
und der lebt und herrscht in alle Ewigkeit.</li></ol>
Amen.

