---
title: Missale
tags: Missale,Buch, Gebet, kath
bild:missale.jpg

Das Buch, aus dem die Gebete der Messe gesprochen oder gesungen werden. Im <a href="index.php?page=ritus_tridentinisch.php">tridentinischen Ritus</a> enthielt das Missale auch die Lesungen. Da nach dem II. Vatikanischen Konzil sehr viel mehr biblische Texte, über drei Jahre an den Sonntagen, über zwei Jahre an den Werktagen verteilt, gelesen werden, gibt es für den katholischen Gottesdienst mehre Bücher für die Lesungen, <a href="http://www.kath.de/lexikon/liturgie/index.php?page=lektionar.php">Lektionare</a>. In der evangelischen Kirche heißt das Gottesdienstbuch Agende, in den orthodoxen Kirchen Euchologion oder Leiturgikon.
