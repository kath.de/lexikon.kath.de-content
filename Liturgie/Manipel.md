---
title: Manipel
tags: Manipel, Schweißtuch, kath
---

Ursprünglich das Tuch, das in den Ärmel gesteckt wurde, um den Schweiß abzuwischen, wurde es später stilisiert und vom Priester über das linke Handgelenk gezogen. Nach der Liturgiereform 1969 wird es kaum noch getragen. Im tridentinischen Ritus gehört es zu den liturgischen Gewändern und ist wie die Stola farblich an das Messgewand angepasst. Auch der Diakon trug ein Manipel.
