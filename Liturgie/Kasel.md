---
title: Kasel
tags: Kasel, Messgewand, Zelt, kath
---

Messgewand, das über der <a href="http://www.kath.de/lexikon/liturgie/index.php?page=albe.php">Albe</a> getragen wird. Das Wort kommt wohl von Casa, Haus. Es wird symbolisch als das Zelt gedeutet, das den Israeliten bis zum Bau des Tempels als Aufbewahrungsort für die Gebotstafeln diente. Das Gewand bedeckt die Arme und wird auch heute wieder so getragen. Diese werden im Mittelalter ausgeschnitten, so dass die Kasel nach vorne wie eine Geige aussieht. Die Kasel der Diakone ist an zwei Streifen erkennbar, die jeweils seitlich vom Kopf senkrecht sowohl über die Vorder- wie über die Rückseite herablaufen. Diese Form der Kasel wird auch ==>Dalmatika genant. Die Kaseln wurden im Laufe des Mittelalters und des Barock immer reicher verziert. In den orthodoxen Kirchen heißt die Kasel "Phelonion"
