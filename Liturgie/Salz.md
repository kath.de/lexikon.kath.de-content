---
title: Salz
tags: Salz, Weihe des Taufwassers, Glaube, kath
---

Das Salz ist nach einem Wort Jesu ein Bild für die Glaubensüberzeugung, die seine Jünger haben. "Ihr seid das Salz der Erde. Wenn das Salz seinen Geschmack verliert, womit kann man es wieder salzig machen? Es taugt zu nichts mehr; es wird weggeworfen und von den Leuten zertreten." (Matthäus 5,13) Salz gehört daher zur Weihe des Taufwassers.
