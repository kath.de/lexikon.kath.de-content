---
title: Non
tags: Non, Stundengebet, kath
---

Die Non von lateinsich nona (hora) = neunte Stunde, wird zur neunten Stunde der römischen Zeitrechnung, also um 15.00 Uhr unserer Zeit gefeiert. Die Non erinnert an den Tod des Herrn. Sie gehört zu den <a href="index.php?page=kleine_hore.php">kleinen Horen</a>.
Die Non gedenkt dem Abstieg Jesu in das Reich des Todes. Durch dieses Hinabsteigen besiegt er das Reich des Todes. Im römischen Hochgebet kommt dies wieder: "Er hat die Macht des Todes gebrochen und die Auferstehung kundgetan."
Hippolyt:
" Man soll aber großes Gebet und Lobpreis
auch sprechen zur Zeit der 9. Stunde
zum Abbild des Lobes, mit dem die Seelen der Gerechten
Gott preisen.
Denn er war seiner Heiligen eingedenk
und sandte sein Wort, sie zu erleuchten.
In jener Stunde hat Christus auch Blut und Wasser
aus seiner durchbohrten Seite verströmt
und hat den Rest des Tages
erleuchtet zum Abend gelangen lassen.
Er hat so, da er im Tode entschlief,
den Beginn des kommenden Tages
unter das Zeichen der Auferstehung gestellt."
Die Deutung der 9. Stunde erschließt sich aus Psalm 16,10:
Du läßt meine Seele nicht in der Unterwelt;
wirst Deinen Heiligen nicht schauen lassen
die Verwesung.

Jürgen Pelzer

