---
title: Osterwasser
tags: Osterwasser, Osternacht, Taufe, kath
bild:osterwasser.jpg

In der frühen Kirche wurden die Katechumenen, die Menschen, die Christen werden wollten, in der Fastenzeit auf die Taufe vorbereitet und in der Osternacht getauft. Die Taufe und damit das Wasser prägen die österliche Liturgie. Das Osterwasser wird in der Ostnachtfeier durch Eintauchen der Osterkerze geweiht und für die Taufe verwendet. Von den Gläubigen wird es als Segenszeichen mit nach Hause genommen.
