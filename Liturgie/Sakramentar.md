---
title: Sakramentar
tags: Sakramentar, Sammlung, Gebetstext, kath
---

Die ersten Sammlungen von Gebetstexten für Gottesdienste und die Spendung der Sakramente wurden Sakramentare genannt. Sie sind wegen ihrer Bildgestaltung kunstgeschichtliche von hohem Wert.
