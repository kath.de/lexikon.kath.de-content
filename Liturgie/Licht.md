---
title: Licht
tags: Licht, Christus, kath
---

In der Liturgie spielt die Lichtsymbolik eine wichtige Rolle. Viele vorchristliche Religionen und Mythen bezogen sich auf die Sonne. So wurde die Sonnensymbolik übernommen, aber auf Christus umgedeutet. Christus ist das wahre Licht. In der Taufe hat der Gläubige eine existentielle Erleuchtung erfahren. In der Taufliturgie wird dieser Gedanke durch die vom Priester gesprochenen Worte "Empfange das Licht Christi" und durch das Anzünden der Taufkerze an der Osterkerze symbolisiert.
Wie jedes Symbol hat auch die Lichtsymbolik ein Moment der Unangemessenheit: Geht die Sonne am Ende des Tages unter, so geht die wahre Sonne, also Christus nie unter. Vielmehr wird der Abglanz der abendlichen Sonne zur Verheißung eines neuen Tages. Die abendliche Sonne ist also kein Zeichen des Unterganges, sondern ein Symbol für das unvergängliche Licht, Christus, welches den Menschen nicht der Finsternis überlässt.
Die Lichtsymbolik ist sowohl an Weihnachten wie in der Osternacht von hoher Bedeutung <a href="index.php?page=exultet.php">Exsultet</a>

<b>Aus der Tauffeier der orthodoxen Liturgie</b>
Reiche mir das Lichtgewand,
der Du Dich umkleidest mit Licht
wie mit einem Gewand,
erbarmungsvoller Christus, unser Gott

Jürgen Pelzer
