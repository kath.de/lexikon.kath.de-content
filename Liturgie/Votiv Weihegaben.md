---
title: Votiv- und Weihegaben
tags: Votivgabe, Weihegabe, Pilger, Heilige, Dank, kath
---

Dies sind zumeist von Pilgern an (Wallfahrts-) Kirchen aufgehängte Gaben zur Erflehung von Hilfe oder als Dank für eine Gebetserhörung. Diese Formen gab es schon in den ältesten Kulten, die vom Christentum übernommen wurden. Der Brauch blieb durch alle Jahrhunderte lebendig. Besondere Ausgestaltung erlangte er zur Barockzeit, wo oft kranke und geheilte Körperteile nachgebildet wurden. Berühmte Wallfahrtsorte besitzen ganze Sammlungen von Votivbildern. Es werden auch einzelne Körperteile in Wachs oder Metall aufbewahrt. In Saint Nicolas de Port in Lothringen wurde für die Befreiung von ungerecht Verurteilten gebetet. Bis zur französischen Revolution hingen dort viele Ketten als Votivgaben. Auch Krücken von Geheilten finden sich in Wallfahrtskirchen.
