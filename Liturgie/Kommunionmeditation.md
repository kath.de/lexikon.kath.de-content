---
title: Kommunionmeditation
tags: Kommunionmeditation, Eucharestie, Liturgie, kath
---

Wenn in einer Messe die Gläubigen zur Kommunion gehen, spielt die Orgel oder es wird ein Lied gesungen. Die Musik und das Lied bieten einen Rahmen, in dem die Gläubigen meditieren können, dass sie Christus in den Gestalten von Brot und Wein begegnet sind. In manchen Gottesdiensten wird statt eines Liedes eine Meditation gesprochen. Dieses Gestaltungselement ist bei <a href="http://www.kath.de/lexikon/medien_oeffentlichkeit/gottesdienste_fernsehen.php">Fernsehübertragungen</a> unentbehrlich. Denn für den Zuschauer würde sich keine meditative Anregung ergeben, wenn er über Minuten die Gläubigen in der Kirche beim Kommuniongang beobachten müsste. Das Fernsehen würde dann auch in einen intimen Raum eindringen, wenn Menschen in der Kirche zu nahe beim Empfang der Kommunion gezeigt würden.
Eine Meditation anstelle des abgefilmten Kommuniongangs der Gläubigen in der Kirche eröffnet dem Zuschauer einen meditativen Zugang zum Geheimnis der Eucharistie und der Person Jesu. In fast allen Kirchen finden sich Bildmotive, zumindest ein Kreuz, ein Kelch oder auch eine Darstellung des Abendmahles. Da der Zuschauer nicht zur Kommunion gehen kann, es sei denn, Kommunionhelfer seiner Heimatgemeinde bringen ihm die Eucharistie, bietet ihm die Kommunionmeditation die Möglichkeit einer geistlichen Kommunion. Geistliche Kommunion meint, daß man das gewandelte Brot nicht empfängt, aber sich innerlich für die Begegnung mit Christus öffnet. Der einzelne empfängt Christus nicht im gewandelten Brot nicht, sondern begegnet ihm im Gebet.

Hinweise zu den <a href="http://www.fernsehgottesdienst.de">Übertagungen im ZDF</a>
