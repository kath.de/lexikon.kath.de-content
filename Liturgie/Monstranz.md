---
title: Monstranz
tags: Monstranz, Eucharestie, kath
---

In der Form eines Sonnenkranzes oder eines gotischen Gehäuses wird in der Monstranz das eucharistische Brot in Form einer großen Hostie gefasst und auf dem Altar zur Verehrung aufgestellt. Monstranz kommt vom lateinischen "monstrare", zeigen. Das Sonnenrad weist auf Christus als die Sonne hin. Er ist mit der Auferstehung zum Licht geworden. Die Monstranz hält den Augenblick der Erhebung der Hostie fest, ein Brauch, der in der Zeit der <a href="http://www.kath.de/lexikon/symbole_kirchenraum/index.php?page=gotik.php" target="_BLANK">Gotik</a> entstand. Die Menschen wollten sehen und gingen weniger zum Abendmahl. Deshalb erhob der Priester, der mit dem Rücken zum Volk zelebrierte, die Hostie über seinen Kopf. Im Fronleichnamsfest wird die Hostie in der Monstranz zum Segen für den Ort und die Felder durch die Straßen getragen. Viermal wird entsprechend den vier Himmelsrichtungen mit der  Monstranz der Segen gespendet, indem die Monstranz in Form eines Kreuzzeichens bewegt wird.
