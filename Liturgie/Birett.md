---
title: Birett
tags: Birett, Barett, Kopfbedeckung, Prister, Bischof, Kardinal, kath
---

Kopfbedeckung von Priestern und Bischöfen wie auch von Studenten, die sich auf das Priesteramt vorbereiten. Das Wort ist mit dem Barett verwandt, das Kopfbedeckung heißt, die evangelische Pastoren und auch Universitätsprofessoren tragen. Das Birett ist quadratisch geformt, keine Mütze, sondern steif und hat oben vier Stege, das römische und damit auch das der Kardinäle nur drei. Das der Priester ist schwarz, das der Bischöfe violett, das der Kardinäle purpurrot. Es wird außerhalb des Gottesdienstes getragen. In der sog. <a href="index.php?page=ritus_tridentinisch.php">tridentinischen Messe</a> zog der Priester mit dem Birett ein, legte es dann ab. Bischöfe tragen für den Gottesdienst die <a href="index.php?page=mitra.php">Mitra</a> als Kopfbedeckung.
