---
title: Rochett
tags: Rochett, Rock, kath
---

Das Wort kommt eigentlich von Rock, bezeichnet aber ein, meist mit Stickereien, verziertes Hemd, das über dem <a href="http://www.kath.de/lexikon/liturgie/index.php?page=talar.php">Talar</a> getragen wird. Prälaten, Bischöfe und Kardinäle tragen auch außerhalb liturgischer Vollzüge ein Rochett über der <a href="http://www.kath.de/lexikon/liturgie/index.php?page=soutane.php">Soutane</a>.
