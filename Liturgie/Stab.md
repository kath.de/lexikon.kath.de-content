---
title: Stab
tags: Stab, Bischof, Abt, Hirte, Moses, kath
---

Bischöfe und Äbte tragen als Zeichen ihrer Amtsvollmacht einen dem Hirtenstab nachbildeten Bischofs- oder Abtsstab. Moses trug auch einen Stab, mit dem er das Rote Meer teilte, damit die Israeliten so dem nachrückenden Heer des Pharao entkommen konnten (Buch Exodus 4,1-5, 14, 16.) Mit seinem Stab schlug Moses Wasser aus dem Felsen (Buch Numeri, 20, 7-11) Für Jakobspilger und andere Wallfahrer gehört der Stab zur Ausrüstung.
