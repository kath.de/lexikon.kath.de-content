---
title: Benediktionale
tags: Benediktionale, kath
---

Segnen ist die Konkretisierung des Segens, den Gott den Menschen zuspricht. Was gesegnet wird, wird dem besonderen Schutz Gottes anvertraut. Es soll Segen auf den Personen, den Gebäuden, den Gegenständen liegen.
Gesegnet und geweiht werden erst einmal Räume und liturgische Gegenstände und Gewänder, um diese herauszuheben und für die Nutzung in Gottesdiensten vorzubehalten. Jedoch ist der Segen nicht nur für Personen, Räume und Gegenstände vorgesehen, die mit dem Gottesdienst zu tun haben. Der Segen umfasst die ganze Welt des Menschen und reicht bis in Fabriken und Feuerwehrwagen.

Für den Segen, benedicere, eigentlich gibt es ein kirchliches Buch, das Benediktionale, das für die verschiedenen Segnungen jeweils Vorlagen zur Verfügung stellt.

Eine Segnung ist wie ein Gottesdienst aufgebaut.
-	nach der Begrüßung folgt
-	ein Text aus der Schrift, der
-	ausgelegt wird.
-	Der eigentliche Segen wird durch Lobpreis und Wechselgebet eingeleitet.
-	Der Segen selbst wird als Bitte formuliert.
-	Den Abschluss bilden Fürbitten und ein
-	allgemeiner Segen

Gesegnet wird in der Form des Kreuzzeichens
Weihwasser und Weihrauch verdeutlichen zeichenhaft den Segen.  
Wenn ein Mensch gesegnet wird, legt der Priester oder ein anderer diesem die Hände auf.

Segnungen sind für folgende Bereiche vorgesehen

Segnungen zu Festen und herausgehobenen Zeiten
Segnung des Adventskranzes
Kindersegen an Weihnachten
Segnung des Johannisweins am 27.12.
Segnungen der Sternsinger 
Blasiussegen,
Speisesegnung an Ostern
Wettersegen
Kräutersegen an Mari Himmelfahrt
Segnung der Ernte
Segnung der Gräber an Allerheiligen, Allerseelen
Kinder- und Lichtersegnung am Martinsfest
Brotsegnung an einzelnen Heiligenfesten
Feuersegnung am Johannistag oder an anderen Festen

Segnungen für Personen
Muttersegen vor und nach der Geburt
Kindersegnung
Segnung der Schulanfänger
Krankensegnung
Segnung alter Menschen
Segnungen stellvertretend für den Papst
Primizsegen
Segen zur silbernen und goldenen Hochzeit
Pilgersegen
Reisesegen

Segnung von Räumen und Gegenständen für den Gottesdienst
Weihe einer Kapelle und eines Altares
Weihe eines Kreuzes
Segnung beim Glockenguss
Glockenweihe
Orgelweihe
Weihe einer Hostienschale oder eines Kelches
Weihe von Gewändern, die für den Gottesdienst bestimmt sind
Segnung eines Grundsteins für ein kirchliches Gebäude
Segnung eines kirchlichen Gemeindehauses
Friedhofsweihe
Segnung einer Friedhofshalle

Segnung religiöser Zeichen
Weihwasser
Kreuz oder Christusbild
Marienbild 
Heiligenbild
Christopherusplakette
Rosenkranz 
Religiöse Abzeichen und Medaillen
Fahne
Kerzen
Sterbekreuz und Sterbekerze
Andere religiöse Zeichen

Segnungen für Familien
Segnung einer Familie
eines Kindes
eines kranken Kindes
für Jugendliche bei neuen Lebensabschnitten
Verlobung
Segnung eines Kranken
Tischsegen
Brotsegnung
Segnung eines Hauses
Segnung einer Wohnung

Segnungen von öffentlichen Einrichtungen
Rathaus
Amtsgebäude 
Krankenhaus, Sanatorium
Altenheims
Feuerwehr
Wasseranlage
Kläranlage

Arbeitstätten
Segnung eines Industriebetriebes
eines landwirtschaftlichen Betriebs
eines handwerklichen Betriebs
Eines Kaufhauses oder Geschäfts
einer Buchhandlung
von Büroräumen
einer Arztpraxis, einer Apotheke
einer Bank
einer Gaststätte, eines Hotels
von Tieren
von Maschinen und Geräten
von Feldern, Weiden, Weingärten

Bildungseinrichtungen
Segnung eines Kindergartens
einer Schule
eines Jugendheimes
einer Bildungsstätte
einer Bücherei oder Bibliothek

Segnung von Verkehrsmitteln
von Fahrzeugen
von öffentlichen Verkehrsmittel
eines Schiffes
eines Flugzeuges
eines Sanitätsfahrzeuges
eines Einsatzfahrzeuges
einer Seilbahn, eines Liftes
einer Straße
einer Brücke

Musikinstrumente, Sportanlagen, Tourismus
Sengung von Musikinstrumenten
Sportanlage
Berg- oder Schützhütte
Bergsteigerausrüstung


Segensgebet für einen Feuerwehrwagen
Herr, unser Gott, du hast die Welt erschaffen und alles wunderbar geordnet. Die Elemente stehen in deinem Dienst und erhalten den Menschen am Leben. Das Feuer spendet Wärme und Licht, es schmilzt das Erz und läutert das Gold. Es kann auch zur Gefahr werden, für Mensch und Tier, für Hab und Gut.
Segne, wir bitten dich, dieses Feuerwehrfahrzeug, das wir heute in Dienst stellen. In der Gefahr trage es dazu bei, die zerstörerische Gewalt des Feuers zu brechen, Unglück und Naturkatastrophen abzuwehren. Schütze die Menschen, die sich seiner bedienen und als Feuerwehrleute ihren Dienst zum Wohl der Gemeinschaft verrichten. Bewahre uns vor Schaden und mache uns alle bereit zu aufrichtiger Zusammenarbeit, zu brüderlicher Hilfeleistung und zum Dienst am Nächsten

Dann wird noch der Patron der Feuerwehrleute, der hl. Florian, angerufen:

Last uns auch um den Schutz des heiligen Florian bitten:
Großer und Starker Gott, du hast den heiligen Florian und seinen Gefährten die Gnade geschenkt, den Glauben an Christus durch ihr Sterben zu bezeugen.
Gewähre uns auf ihre Fürsprache Schutz und Hilfe und gib auch uns den Mut, den Glauben unerschrocken zu bekennen. 

Fürbitten
Wir beten zu unserem Herrn und Gott und rufen zu ihm:
	Allmächtiger Gott, lohne allen Feuerwehrmännern ihren Einsatz für die Gemeinschaft.
Wir bitten dich, erhöre uns!
	Bewahre uns vor Feuersnot und Katastrophen.
Wir bitten dich, erhöre uns!
	Lass unter uns die gegenseitige Verständigung und Hilfsbereitschaft wachsen.
Wir bitten dich, erhöre uns!




