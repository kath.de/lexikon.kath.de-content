---
title: Amikt
tags: Amikt, Schultertuch, Priestergewand, kath
---

Das Tuch, das sich Priester und Diakon über die Schulter binden, heißt lateinisch Amikt, deutsch einfach Schultertuch. Es dient der Schonung der anderen Messgewänder.
