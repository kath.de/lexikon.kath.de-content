---
title: Stehen
tags: Stehen, Liturgie, Sitzen, kath
---

Die angemessene Körperhaltung bei der Feier der Liturgie ist das Stehen. Auch wenn in katholischen Gottesdiensten viel gekniet wird, ist doch das Stehen bei herausgehobenen Gebeten, vor allem wenn diese von der Gemeinde gemeinsam gebetet oder gesungen werden, die typische Haltung. Das gilt für die Gebete, mit denen der Priester die Eröffnung, die Gabenbereitung und die Kommunion abschließen. Stehend werden die meisten Hymnen und Lieder gesungen, so das Eingangsleid, Kyrie, Gloria, Sanctus, Agnus Dei. Ebenso wird das Glaubensbekenntnis stehend gesungen oder gesprochen. Früher stand die Gemeinde auch beim zentralen Hochgebet, heute wird dabei gekniet. Lesungen und Predigt werden sitzend verfolgt.
Das Sitzen während des Betens entwickelte sich früh im Zusammenhang mit dem Chorgebet. Das <a href="http://www.kath.de/lexikon/liturgie/index.php?page=chorgestühl.php">Chorgestühl</a> ist für einen Wechsel von Sitzen und Stehen ausgelegt. Für die Gläubigen im Kirchenschiff wurden erst seit dem 15. Jahrhundert Stühle bereitgestellt.
