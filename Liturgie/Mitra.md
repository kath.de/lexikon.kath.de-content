---
title: Mitra
tags: Mitra, Bischof, Abt, kath
---

Das Wort leitet sich von griechisch "Stirnbinde" her, ist aber eine hohe Mütze, die Bischöfe und Äbte seit dem 11. Jahrhundert in dieser Form tragen. In der anglikanischen und in einigen lutherischen Kirchen hat sich die Mitra des Bischofs erhalten. Sie wölbt sich mit  einem schildähnlichen Vorder- und Rückseite nach oben und hat nach hinten zwei Bänder.  Die Mützenform hat sich in den Kirchen des Ostens deutlicher erhalten. Der Papst trägt ebenfalls eine Mitra, nicht die bis zum II. Vatikanischen Konzil übliche <a href="http://www.kath.de/lexikon/liturgie/index.php?page=tiara.php">Tiara</a>
