---
title: Purpur
tags: Purpur, Kardinal, Meeresschnecke, kath
---

Das aus einer Meeresschnecke gewonnene Rot war von hohem Wert. Da die römischen Patrizier Purpurgewänder trugen, ist Purpur die Farbe der Kardinäle. Da Kardinäle zu ihrer Ernennung das neue Gewand erhalten, bedeutet "den Purpur erhalten", zum Kardinal ernannt werden.
