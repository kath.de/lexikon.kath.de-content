---
title: Sakramente
tags: Sakrament, Symbol, Taufe, Buße, Firmung, Eucharestie, Ehe, Weihe, Krankensalbung, Kirche, Altar, Taufbecken, Beichtstuhl, kath
---

Die Kirche ist Ort der Spendung der Sakramente. Der Altar weist auf die Eucharistie hin, das Taufbecken auf die Taufe, der Beichtstuhl auf die Lossprechung von den Sünden. Andere Sakramente wie Firmung, Diakon-, Priester- und Bischofsweihe werden vor dem Altar vollzogen.
Die Struktur des Symbols findet sich auch in den <b>christlichen Sakramenten</b>. Dieses lateinische Wort bezeichnet ursprünglich die Weihe zum Kriegsdienst, es kommt von "sacer", das für den abgegrenzten heiligen Bezirk steht. Die Sakramente der christlichen Kirche haben die Doppelheit des Symbols, ein sichtbares Zeichen wie z.B. Wasser, Öl, Brot, deuten auf einen religiösen Gehalt. Mittels des äußeren Zeichens handelt Gott, schenkt in der Taufe die himmlische Wiedergeburt, lässt im eucharistischen Brot den Menschen an dem himmlischen Mahl teilnehmen, salbt ihn mit Öl für eine besondere Aufgabe in der Kirche.
Sakramente sind heilige Handlungen, die von Amtsträgern der Kirche vollzogen werden und sich individuell auf das Heil des einzelnen beziehen. In dem Amtsträger handelt Gott und spricht dem einzelnen Erlösung und Heil zu. Nur beim Ehesakrament ist der Amtsträger nicht Spender des Sakramentes. Da spenden sich die Brautleute selbst das Sakrament, der Amtsträger ist Zeuge des Eheversprechens.
In der Taufe erhält der einzelne die Gnade der Erlösung, die Vergebung seiner Sünden und wird in die Kirche aufgenommen.
In der Firmung wird das Pfingstereignis, dass nämlich die Christen Geistträger sind, dem einzelnen zugesprochen.
Nur die Eucharistiefeier ist ein Sakrament der versammelten Gemeinde insgesamt. In der <a href="http://www.kath.de/lexikon/liturgie/index.php?page=messe.php">Messe</a> feiert die Gemeinde die Gegenwart Jesu Christi in seinem Tod und seiner Auferstehung. Jeder einzelne wird in das neue Leben verwandelt. Deshalb ist die Kommunion die Wegzehrung auch für die Sterbenden in ihrem Übergang in das neue, vollendete Leben.
In der Beichte erhält der einzelne die Lossprechung von Sünden und Verfehlungen, die er nach seiner Taufe begangen hat.
In der Diakonen-, Priester- und Bischofsweihe wird dem einzelnen seine Beauftragung und die Gnade für sein Amt zugesprochen.
