---
title: Brevier
tags: Brevier, kurz, Stundengebet, Liturgie, kath
---

Das Brevier ist ein kurzes Buch (von lat. brevis = kurz) und enthält das Stundengebet der römisch-katholischen Kirche. Es entstand aus dem Anliegen, die wichtigsten Texte in einer Textvorlage zu konzentrieren. Denn bis dahin war es üblich, dass man bis zu 12 Bücher brauchte um einen Gottesdienst zu feiern, was zur Folge hatte, dass nur große Kirchen sich diesen Aufwand leisten konnten. Hintergrund war, dass die Stundenliturgie sich im Mittelalter sehr ausgeweitet hatte. Das <a href="index.php?page=stundengebet.php">Stundengebet</a> besteht aus 7 über den Tag verteilten Gebetszeiten, für die das Brevier die Texte für den jeweiligen Tag enthält: Die Lesehore, Laudes, Terz, Sext, Non, Vesper und Komplet.
Die Ausgabe von 1970 unter Papst Paul VI. wurde im Zuge der liturgischen Erneuerung des II. Vatikanischen Konzils eingeführt. Zusammengesetzt ist dieses Brevier besteht ncith mehr wie das davor aus einem Band, sondern aus 16 Lektionaren. Diese Lektionare enthalten die Lesungen für die Lesehore. Jeweils 8 Lektionare werden pro Jahr verwendet, so dass sich für jedes Lektionar ein zweijähriger Rhythmus der Verwendung ergibt.
Der zweite Bestandteil des Breviers bilden die 3-bändigen Stundenbücher: Band 1 enthält die Texte für die Advents und Weihnachtszeit, Band 2 enthält die Texte für die Fasten- und Osterzeit und Band 3 enthält die Texte für die Zeit im Jahreskreis.

Ausgaben:
1568 Pius V
1970 Papst Paul VI
Das Wort Brevier wird auch synonym verwendet für das Stundengebet selbst, welches auch als Offizium oder liturgia horarum bezeichnet wird.

Jürgen Pelzer
