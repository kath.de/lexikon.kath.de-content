---
title: Akolyth
tags: Akolyth, Messdiener, Priesteramtskandidat, kath
---

Früher eine Weihestufe, heute als Beauftragung gestaltete Einweisung in das Amt des Messdieners. Diese feierliche Einführung wird allerdings nicht Messdienern zuteil, sondern Priesteramtskandidaten auf ihrem Weg zur Priesterweihe.
