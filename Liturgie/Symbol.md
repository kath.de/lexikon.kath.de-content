---
title: Symbol
tags: Symbol, Kirche, kath
---

Empirisch-faktische Wirklichkeiten, Gesetze oder Befehle lassen sich sprachlich einfach vermitteln. So z. B.: "Das Gras wächst. Es ist deshalb vierwöchentlich zu mähen. Hole den Rasenmäher!"
Schließlich handelt es sich dabei um evidente und eindeutige Aussagen.
Wenn ich aber jemand erklären will, wie sehr ich ihn liebe oder hasse, so stellt sich ein sprachliches Problem. Wie kann ich meine inneren Empfindungen mitteilen, obwohl sie nur von mir selbst so wahrgenommen und erlebt werden? Wie ermögliche ich meinem Gegenüber, eine Idee meiner Empfindungen zu bekommen?
Innere Empfindungen werden verständlich, indem ich beispielsweise einen Strauss Rosen schenke oder einem Geschäftsfreund Wein und Öl mitbringe.

<b>Darstellung von Werten</b>
Ein ähnliches Problem stellt sich, wenn die Wirkung abstrakter Werte verdeutlicht werden soll. So z. B. wie "Einigkeit und Recht und Freiheit" als volksverbindende Tugenden verdeutlicht werden sollen.
Deutlich wird dies an einer gegenständlichen Metapher, beispielsweise einer Nationalflagge. So wird zum Beispiel aus einem Stück Stoff, das in den Farben Schwarz, Rot, Gold gestreift ist, eine Deutschlandfahne.
Die Bedeutung der Fahne ist identisch mit den Werten, den Tugenden und der Geschichte des Deutschen Volkes selbst. Es gibt also eine feste Beziehung zwischen dem Symbol und der darin eingeschlossenen Bedeutung. Die Bedeutung aber, d.h. der Inhalt des Symbols, kann sich dynamisch fortentwickeln, ohne dass die Fahne ihr Aussehen ändert. Diese Fahne steht dann neben dem Bundespräsidenten bei seiner Weihnachtsansprache, erscheint bei Staatsempfängen, wird bei der Siegerehrung nach einem sportlichen Wettkampf aufgezogen und wird in vielen Ländern von den Menschen spontan am Haus aufgehängt.
Durch die Vergegenständlichung im Symbol können die abstrakten Größen wie deutsche Tugenden, deutsche Werte und deutsche Geschichte zu aktiven oder passiven Akteuren werden. Das ist die Voraussetzung dafür, daß man durch Verbrennen einer Deutschlandflagge das deutsche Volk beleidigen kann.
Noch schwieriger wird die sprachliche Herausforderung, wenn Glaubensinhalte vermittelt werden sollen, die ja ihrer Eigenart nach nicht als empirische Erkenntnisse gelten und auch nicht an empirischen Wirklichkeiten überprüft werden können. Hier legt es sich nahe, die Funktionsweise eines Symbols näher zu betrachten. Es geht im religiösen Symbol nämlich darum, die empirisch nicht faßbare Wirklichkeit mit einem sichtbaren Zeichen zu verbinden. Das ist die ursprüngliche Wortbedeutung von Symbol, das sich von griechisch sym-ballein, zusammenfallen herleitet und zwei Pole zusammenbindet. Die Beziehung wird zwischen dem sichtbaren und sinnenhaften Pol eines Symbols (Farbe, Zeichen, Material, etc.) und dem Bedeutungspol hergestellt. Diese zwei Pole oder Ebenen sind allen Symbolen gemein. Der eine, sichtbare Pol hat einen empirisch-faktischen Bezug, der andere einen wertbezogenen und abstrakten. Die Aussage, die nun auf der empirisch-faktischen Seite des Symbols entsteht (z. B. Brot. Es ernährt, der Nährwert ist empirisch zu überprüfen), gilt auch für den abstrakt-wertbezogenen Pol. Wie das Brot in natürlichem Sinn nährt, so nährt Brot, das im religiösen Kult verwendet wird, die Seele. Religiöse Symbole zeichnen sich dadurch aus, dass der empirische Pol meist der Natur oder dem Bereich der menschlich-natürlichen Bedürfnisse entnommen ist, der wertbezogene Pol sich auf Aussagen bezieht, die aus dem religiösen Bereich kommen.

<b>Über das Zeichen hinausgehend</b>
Wie aber wird ein Gegenstand zu einem Symbol? Wenn er über seine ursprüngliche Funktion (z. B. Kerze: Leuchten und Wärme abstrahlen) hinaus weist, um eine dynamische Bedeutungsfunktion zu übernehmen (die Kerze stellvertretend für das Gebet von Personen brennt).
Entscheidend für das Funktionieren eines Gegenstandes als Symbol ist, dass der Gegenstand Erinnerungen an Erlebnisse und damit an Inhalte, an Geschichten zu wecken vermag. In der christlichen <a href="index.php?page=eucharestie"> Eucharistiefeier</a> werden Brot und Wein zum Symbol, weil dem die Erzählung vom Letzten Abendmahl Jesu mit seinen Jüngeren zugrunde liegt.

Ein besonderes Problem entsteht im religiösen Bereich, wenn das Symbol die feiernde Gemeinde mit Gott in Beziehung setzen soll. Gott, der dem Begriff nach jedes Verstehen übersteigt, wird von keinem Symbol wirklich erfasst, sondern es werden einzelne Bedeutungsaspekte symbolisch dargestellt. Deshalb ist das Sprechen von Gott nur dann gerechtfertigt, wenn es im Bewusstsein geschieht, dass jeweils nur ein Fragment vermittelt werden kann.

<b>Engeführung von Symbolen</b>
Die der Symbolsprache innewohnende Bedeutungsunschärfe macht diese Sprache für manche zum Problem. Sie produziert keine präzisen, exakt nachmessbaren und auch keine juristisch durchsetzbare Aussagen.
Weltanschauungen, die vorgeben, jede Wirklichkeit empirisch-faktisch verstehen und beschreiben zu können, tun sich mit Symbolen schwer. Das rechtfertigt aber nicht, die Symbolsprache als überflüssig zu deklarieren. Entscheidende menschliche Dimensionen, wie die Emotionalität, die Qualität zwischenmenschlicher Beziehungen, die Übereinstimmung in bestimmten Werthaltungen, der Bereich des Religiösen und viele kulturelle Lernvorgänge blieben ohne diese Sprachebene den Menschen verschlossen.

In Herrschaftssystemen, die auf Eindeutigkeit bedacht sind, besteht die Gefahr, die Bedeutung der Symbole und die Art und Weise ihres Gebrauches "definieren" zu wollen. Ein Symbol ist aber seiner Natur nach nicht definierbar im Gegensatz zum Zeichen, das meist nur eine einzige und fast immer eine eindeutige Bedeutung besitzt.

Volksfrömmigkeit und religiöse Kulturen sind von Symbolen geprägt. Sobald jedoch Religionen sich in Institutionen etablieren, können Symbole statisch festgeschrieben werden, d.h. ein bestimmter Bedeutungsaspekt wird festgeschrieben. Es besteht dann die Gefahr, dass Symbole ihrer eigentlichen Funktionsweise entleert werden.

Bilderverbot und Symbole
Das Alte Testament verbot, dass Bilder von Gott entstehen. Im Islam und im Judentum wird dieses Verbot streng eingehalten.
Gleichzeitig wurde durch das IV. Laterankonzil definiert, dass Aussagen über Gott sind immer mehr unzutreffend als zutreffendes transportieren. Dass im Christentum kein Bilderverbot besteht, leitet sich von der Menschwerdung des Sohnes Gottes her. Jesus kann dargestellt werden. Das Tuch, das nach der Legende Jesus dargereicht wurde, erhielt Veronika mit dem Abdruck der Gesichtszüge Jesu zurück. Der Name Veronika wird als <a href="/lexikon/symbole_kirchenraum/index.php?page=christusbild.php">"vere ikon"</a>, wahres Bild gedeutet.

Lange vor der Sprachphilosophie entwickelte die Theologie eine Symboltheorie, um die Beziehung zwischen dem Gegenstand, der etwas bedeutet (Signum) und der Bedeutung selbst (res) zu verdeutlichen. Unterschieden wird dabei das Zeichen, d.h. die sinnenhaft wahrnehmbare Dimension (Akzidens) von der Wirklichkeit, die sich in dem Zeichen verbirgt (Substanz).
Wenn nun die Beziehung zwischen dem Zeichen und der darin sich bergenden theologischen Wirklichkeit als glaubensentscheidend und als geoffenbart und vom gemeinsamen Glauben getragen, begriffen wird, spricht die Theologie von einem Realsymbol, gleichbedeutend mit dem Sakrament.

<a href="/seiten/team.html#th">Theo Hipp</a>
