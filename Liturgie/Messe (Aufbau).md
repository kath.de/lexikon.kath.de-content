---
title: Aufbau der Messe
tags: Messe, Aufbau, Wortgottesdienst, Eucharestie, Kommunion, Hochgebet, Sanctus, Gloria, Kyrie, kath
---

Die Messe, wie sie Katholiken feiern, hat sich über Jahrhunderte entwickelt. Im Abendland hat die in Rom entwickelte Form der Messe nicht zuletzt deshalb durchgesetzt, weil Karl d. Gr. sie für das Frankenreich verbindlich gemacht hat. In der Struktur unterscheidet sich die Form nicht wesentlich von den Liturgien der orthodoxen Kirchen und auch nicht von der Mailänder oder der mozarabischen, in Südspanien gefeierten Liturgie. Bis in die sechziger Jahre des 20. Jahrhunderts wurde die Messe lateinisch gefeiert, bis das II. Vatikanische Konzil die Feier in der jeweiligen Volkssprache einführte. Der Vorteil für die Katholiken besteht darin, daß sie im Ausland dem Ablauf einer Messe folgen können, ohne alle Texte zu verstehen, denn die Messe hat immer den gleichen Aufbau.
Der ältere Teil der Messe ist der Mahlteil, der eucharistische Gottesdienst. Das Neue Testament berichtet, daß die Jüngergemeinde sich auch nach Jesu Tod im Abendmahlsaal getroffen hat. Aus dem 1. Korintherbrief des Paulus ist zu entnehmen, daß die Christen sich zu dem Gedächtnismahl trafen, das Jesus am <a href="http://kath.de/Kirchenjahr/gruendonnerstag.php">Gründonnerstag</a> eingesetzt hatte. Da die Christen sich anfangs noch als Teil des Judentums verstanden, besuchten sie die Synagogengottesdienste am Sabbat-Samstag und feierten das Gedächtnismahl am 1. Tag der Woche, der dann zum Wochenfeiertag, dem Sonntag wurde. Der Synagogengottesdienst bestand aus Lesungen und Gesängen und kannte auch die Auslegung der gelesenen Texte in einer Predigt. Mit dem Entstehen eigener christlicher Texte in der zweiten Hälfte des 1. Jahrhunderts und der wachsenden Distanz zwischen Synagoge und Kirche kamen zu dem Gedächtnismahl die Lesungen, Gesänge und Gebete des Wortgottesdienstes hinzu.
Die Messe in Rom wurde in den ersten Jahrhunderten auf Griechisch gefeiert. Papst Damasus (366-384) führte die lateinische Sprache ein. Immer wieder mußte das liturgische Leben und damit auch die Messe von Überwucherungen befreit und neu geordnet werden. Papst Gregor I. gab um 600 der Liturgie für die Stadt Rom eine so schlüssige Gestalt, daß die römische Liturgie für das Abendland maßgebend und von den Germanen übernommen wurde. Nach ihm ist auch der <a href="http://www.kath.de/lexikon/symbole_kirchenraum/index.php?page=choral_gregorianisch.php">gregorianische Gesang</a> benannt, der im Mittelalter im Frankenreich zu der Vollendung geführt wurde, die heute noch die Hörer fasziniert. Das Konzil von Trient (1551 wurde das Dekret über die Liturgie verabschiedet) regte eine Liturgiereform an, die von Pius V. umgesetzt wurde. Die Liturgiereform, die das II. Vatikanische Konzil in Grundzügen beschlossen hatte, wurde von Paul VI. in eine Form gegossen. An der Grundstruktur der römischen Messe haben diese Reformen nichts geändert.
<table border="1"><tr><th>Eröffnung</th><th>Wortgottesdienst an Sonntagen</th><th>Eucharestischer Gottesdienst</th><th>Entlassung</th></tr>
<tr><td>Einzug
Begrüßung
Schuld-
Bekenntnis
Kyrie
Gloria
Tagesgebet</td>
<td><b>Erste Lesung</b>
meist aus dem Alten Testament

<b>Psalm</b>

<b>Zweite Lesung</b>
meist aus neutesta-mentlichen Briefen

<b>Halleluja</b>

Evangelientext

Predigt
Glaubensbekenntnis
Fürbitten</td>
<td>Kollekte und <b>Gabenbereitung</b>

<b><a href="index.php?page=hochgebet.php">Hochgebet</a></b>:
Präfation
Sanctus
Epiklese
Einsetzungsworte
Anamnese
Intercessiones
Doxologie

<b><a href="index.php?page=kommunion.php">Kommunionteil</a></b>
Vater unser
Embolismus
Friedensgebet
Brotbrechen mit Agnus Dei-Gesang
Kommunionausteilung</td>
<td>Schlußgebet
Vermel-dungen
Segen
Entlassung :
Ite Missa est</td></tr></table>

An Werktagen und bei den meisten Heiligenfesten entfallen das Gloria, die Zweite Lesung, das Glaubensbekenntnis und meist auch die Predigt. Festliche Messen unterscheiden sich nur durch die musikalische Gestaltung.

Was erst einmal wie aufeinander folgende Einzelteile erscheint, hat eine innere Dramaturgie, die mit dem Thema der Feier zusammenhängt. Das Thema ist die Beziehung des Menschen zu Gott unter dem Vorzeichen, dass der Mensch nicht wie selbstverständlich in dieser Beziehung lebt. Der Mensch hat sich verloren, so daß Gott ihn heimholen musste - durch die Sendung seines Sohnes.

In der <b>Eröffnung</b> geht es darum, daß das Thema jeder Messe bereits anklingt. Der Vorsteher eröffnet nicht nur die Messe, sondern bringt die Gemeinde in Beziehung zu Gott. Die Gemeinde versteht sich im Sündenbekenntnis als erlösungsbedürftig. Sie begrüßt ihren Herrn (Kyrios ist das griechische Wort für Herr) und preist ihn im anschließenden Gloria.
Im Tagesgebet wird der Eröffnungsteil zusammengefaßt, deshalb hieß dieses Gebet früher "Collecta" von "Sammeln", das gleiche Wort wie Kollekte, in der bei der Gabenbereitung Geld gesammelt wird.

Der <b>Wortgottesdienst</b>  thematisiert die Zuwendung Gottes und holt die Gemeinde da ab, wo sie herkommt: Dass die einzelnen während der Woche viele Erfahrungen machen mussten, die sie mit einer nicht-erlösten Welt und der eigenen Fehlerhaftigkeit und Bosheit konfrontierten. Wie kann Gott mich erlösen, wo doch viele Erfahrungen mir das Gefühl geben mussten, dass ich alles andere als erlöst bin? Die Lesungen, Gesänge, der Abschnitt, der aus einem der vier Evangelien verlesen wird und die Predigt sollen den einzelnen überzeugen, dass Gott, sicher anders als die Menschen erwarten, sein Werk der Erlösung vollbringt. Im Glaubensbekenntnis (Credo - Ich glaube) antwortet die Gemeinde auf die Lesungen und die Predigt und betet dann für die Anliegen der Zeit und der Kirche in den Fürbitten.

Eröffnung und Wortgottesdienst haben die Gläubigen in die Feier der Erlösung eingestimmt. Sie sind wieder in der Lage, Gott für Jesus zu danken, der die Erlösung in seinem Tod und seiner Auferstehung vollbracht hat und durch die Sendung des Geistes  Anteil an dem neuen Leben gewährt, in das die Christen durch die Taufe bereits eingetreten sind. Die <a href="index.php?page=eucharestie.php">Eucharistie</a>, die Danksagung, hat von Jesus die Form eines Mahles erhalten

Der <b>eucharistische Teil</b> der Messe hat als Grundstruktur ein Mahl, das nicht der Sättigung dient, sondern im Gedächtnis an Jesus Christus gefeiert wird. Es  untergliedert sich in drei Teile:
<ol><li><b>Gabenbereitung</b>
Brot und Wein werden zum Altar gebracht. Währenddessen findet eine Geldsammlung (Kollekte) statt. Die Gabenbereitung schließt mit einem eigenen Gebet, dem Gabengebet.</li>
<li><b><a href="index.php?page=hochgebet.php">Hochgebet</a></b>
Der Priester dankt im Namen der Gemeinde Gott für die Erlösungstat Jesu (Präfation), ruft den Heiligen Geist über Brot und Wein (Epiklese), spricht die Worte, die Jesus im Abendmahlssaal über Brot und Wein gesprochen hat (Einsetzungsworte), erinnert an Tod, Auferstehung, Himmelfahrt und Geistsendung (Anamnese), und bittet in verschiedenen Anliegen (Intercessionen).
Das Hochgebet schließt mit einem feierlichen Lobpreis, der Doxologie.</li>
<li>Der <b><a href="index.php?page=kommunion.php">Kommunionteil</a></b>, in dem die Feiernden das gewandelte Brot und an einigen Tagen auch den gewandelten Wein empfangen, wird mit dem Vater Unser und einer Fortführung der Bitten des Vaterunsers, dem Embolismus eingeleitet. Es folgen ein Friedensgebet und der Friedensgruß. Während des Brotbrechens wird das Agnus Dei (Lamm Gottes, erbarme dich unser) gesungen. Dann empfangen die Feiernden Jesus Christus in den Gestalten von Brot und Wein. Auf die Kommunionausteilung folgt ein Danklied.</li></ol>

<b>Entlassung</b>
An das Schlußgebet, nach dem Ankündigungen für die Gemeinde ihren Platz haben, folgen Segen und Entlassung. Die Entlassung schließt nicht nur die Messe ab, sondern gibt ihr auch den Namen. Denn der Entlassungsruf "Gehet hin in Frieden" heißt lateinisch "Ite, missa est": Geht, es ist Sendung. Die Gläubigen sollen gestärkt in dem Glauben, dass Gott die Menschen liebt und sie in einem großen, die Zeiten überspannenden Erlösungsgeschehen heimholt, sich neu im Alttag bewähren und die für das reich Gottes gewinnen, mit denen sie während der Woche in Kontakt kommen.

<a href="/seiten/team.html#eb">Eckhard Bieger S.J.</a>

Hinweise zu den <a href="http://www.fernseharbeit.de" target="_BLANK">Gottesdienstübertragungen im ZDF</a>
