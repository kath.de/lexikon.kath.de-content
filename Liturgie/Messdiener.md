---
title: Messdiener, Ministrant
tags: Messdiener, Ministrant, Akolyth, Messe, kath
---

Schon in der römischen Kirche des Altertums gab es einen eigenen Dienst, den des <a href="http://www.kath.de/lexikon/liturgie/index.php?page=akolyth">Akolythen</a>, der Lichtträger, für den es heute in der Vorbereitung auf das Priestertum eine eigene Beauftragung gibt. Messdiener sind heute meist Kinder und Jugendliche, die bereits zur "Ersten heiligen Kommunion" gegangen sind. Aufgaben der Messdiener sind:
Beim Ein- und Auszug das Kreuz tragen, das Messbuch zu halten, wenn der Priester nicht am Altar oder Ambo steht, mit brennenden Kerzen neben dem Ambo die Lesung des Evangeliums herauszuheben, die Gaben zum Altar zu bringen, das Rauchfass zu tragen und nach der Gabenbereitung den Priester und die Gemeinde mit Weihrauch zu inzensieren sowie bei der Wandlung kniend das Rauchfass entgegen der Hostie und dem Kelch zu schwenken.
