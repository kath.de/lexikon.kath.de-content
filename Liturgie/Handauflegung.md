---
title: Hände aufgelegen
tags: Hände aufgelegen, Weihe, kath
---

Mit der Auflegung der Hände, meist auf den Kopf, wird ein Amt übertragen. "Hände auflegen" ist schon im Neuen Testament der Begriff für eine Weihehandlung. Im 1. Timotheusbrief findet sich folgende Mahnung "Lege keinem vorschnell die Hände auf und mach dich nicht mitschuldig an fremden Sünden; bewahre dich rein!"
