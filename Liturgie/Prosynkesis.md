---
title: Proskynesis
tags: Proskynesis, Kuss, Kniebeuge, kath
---

In diesem griechischen Wort ist das Wort Kuss mit dem die Richtung anzeigenden pros-hin verbunden. Es bezeichnet eine Geste der Ehrerbietung, mit einer <a href="http://www.kath.de/lexikon/liturgie/index.php?page=kniebeuge.php">Kniebeuge</a>. Im liturgischen Sprachgebrauch bezeichnet Proskynese das sich vor Gott auf den Boden legen. Mit einer Proskynesis der Zelebranten u.a. beginnt der Karfreitagsgottesdienst. Als Zeichen ihrer Hingabe machen die Kandidaten vor ihrer Weihe zum Priester eine Proskynese, während die Allerheiligenlitanei gesungen wird.
