titel:Unschuldige Kinder: 28. Dezember
Startseite:"Hier ensteht das neue Kirchenjahrslexikon"
stichworte:Unschuldige Kinder: 28. Dezember, Kindermord von Bethlehem, Herodes, Weihnachten, Knaben, kath
bild:bild.jpg

Anders als der Stephanstag am 26. 12. und der Gedenktag des heiligen Apostels Johannes am 27.12. steht dieses Gedächtnis in unmittelbarer Beziehung zu Weihnachten. Im Matthäusevangelium wird berichtet, dass Herodes die Knaben bis zum Alter von zwei Jahren in Bethlehem aus Angst vor einem Rivalen hat umbringen lassen. Der historische Hintergrund dürfte die Grausamkeit des Herodes gewesen sein, der drei seiner Söhne hinrichten ließ. Dass der Retter des Volkes Israel schon als Säugling bedroht war, wird auch von dem Kind Moses berichtet. Der Pharao ließ alle neugeborenen Knaben der Israeliten töten. Aus der Verbindung beider Motive, Jesus als der neue Moses und die Infragestellung der Herrschaft des Herodes durch den neuen Messias, ist wohl diese Überlieferung entstanden, die die Idylle des Weihnachtsfestes sehr schnell aufhebt.
Am Fest der Unschuldigen Kinder wurde in Klosterschulen der Jüngste für einen Tag auf den Stuhl des Abtes gesetzt, ein Brauch, der sich im Mittelalter auf den <a href="Nikolaus.php">Nikolaustag</a> verschob.
