titel:Taufe Jesu
Startseite:"Hier ensteht das neue Kirchenjahrslexikon"
stichworte:Taufe Jesu, Messias, Erscheinung, Kana, kath
bild:bild.jpg

Am Sonntag nach dem 6. Januar wird die Taufe Jesu gefeiert, im alten Weihnachtsfest der alexandrinischen Kirche war der <a href="Dreikönige.php">6. Januar</a> der Gedenktag der Taufe Jesu.
Die Liturgiereform der katholischen Kirche 1969 hat traditionelle Inhalte des Epiphaniefestes auf die folgenden Sonntage gelegt, denn am 6. Januar wurde die Erscheinung Jesu mit drei Ereignissen gefeiert. Jesus wird von den Heiden, nämlich von den Weisen aus dem Morgenland als Messias erkannt. Bei seinem ersten öffentlichen Auftritt wird er von Johannes d. Täufer als Messias bekannt. Sein erstes Wunder tut er Kana. Am Sonntag nach Erscheinung wird das Fest der Taufe Jesu gefeiert, das Evangelium der Hochzeit zu Kana wird am 2. Sonntag nach Erscheinung gelesen.

