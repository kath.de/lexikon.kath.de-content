titel:27. Dezember Apostel Johannes
Startseite:"Hier ensteht das neue Kirchenjahrslexikon"
stichworte:27. Dezember Apostel Johannes, Jesu, kath
bild:bild.jpg

Zwei Tage nach Weihnachten wird das Fest des Lieblingsjüngers Jesu, Johannes, gefeiert. Er war der jüngere Bruder des Jakobus'. Als einziger der Apostel folgt er Jesus bis unter das Kreuz. Jesus vertraut dem Jünger seine Mutter an. Am Johannestag wird der nach ihm benannte Wein, die "Johannesminne", geweiht; ihn reicht der Priester der Gemeinde, die mit ihm die Liebe Johannes' kosten darf und damit vor Krankheiten und Gefahren geschützt wird. Der Brauch geht auf eine Legende zurück, nach der Johanne, einen vergifteten Kelch getrunken habe, ohne Schaden zu erleiden. Diese Legende hat einen realen Hintergrund, denn nicht wenige Päpste, Bischöfe und Priester wurden dadurch umgebracht, daß die Täter den Meßwein vergifteten.
