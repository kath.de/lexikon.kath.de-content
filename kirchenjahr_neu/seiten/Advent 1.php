titel:Advent 1. Teil:  Löwen, Drachen, das Weltende und die Geburt des Messias
Startseite:"Hier ensteht das neue Kirchenjahrslexikon"
stichworte:Advent, Teil, Angst, Löwe, Drache, kath
bild:bild.jpg

Advent kommt von lateinisch Ankunft. Erwartet wird die Wiederkunft Christi zum Weltgericht am Ende der Zeiten. Zugleich dient der Advent als Vorbereitungszeit auf Weihnachten. Mit dem 1. Advent beginnt das neue Kirchenjahr. Die Adventszeit war wie die Fastenzeit auch Vorbereitung auf die Taufe und wird wie die Fastenzeit eigentlich mit Fasten und Gebet begangen.
Der erste Teil des Advents ist von Motiven bestimmt, die unsere Vorfahren an Portalen und Säulen romanischer Kirchen eingemeißelt haben: Löwen, Drachen und Stiere blicken uns bedrohlich an, Tiere verschlingen sich gegenseitig. Bedrohungsgefühle und Ängste der Menschen haben sich in Fratzen und Monstern Ausdruck verschafft. Ängste bestimmen auch uns. Wir fühlen uns mehr und mehr gefährdet, die Zahl der Sprengstoffanschläge und lokalen Kriege läßt sich nicht eindämmen, Gewalt treibt neue Gewalt hervor. Wir fühlen uns immer weniger als Beherrscher der Welt. Die Hoffnungen bestätigen sich nicht, dass Technik und Medizin die Gefährdungen verringern. Es wächst das Bedrohungspotential und damit unsere Angst. Die Tiere symbolisieren eine tiefere Bedrohung - die durch Dämonen, durch das Böse. Die aus dem Stein heraus gehauenen Gestalten haben die Künstler den apokalyptischen Büchern der Bibel entnommen.
Im 1. Petrusbrief heißt es "Euer Widersacher, der Teufel, geht wie ein brüllender Löwe umher und sucht, wen er verschlingen kann. Leistet ihm Widerstand in der Kraft des Glaubens."
Im Kirchenraum suchten die Menschen Schutz vor dem Bösen. Deshalb ist auch die romanische Kirche im Westen mit Türmen und starken Mauern bewehrt, denn das Böse drängt vom Abend, vom Dunkeln her. Der Blick dessen, der die Kirche betritt, wird nach Osten gelenkt, wo die aufgehende Sonne den Sieg Christi über den Tod ankündigt. In der Offenbarung des Johannes ist der Drache der Gegner Gottes wie der Gläubigen. Der Drache will die Jungfrau mit dem Kind fassen, diese kann in die Wüste entfliehen. Der Drache speit ihr einen Wasserstrahl nach. Daher haben die Wasserspeier mittelalterlicher Kirchen die Form eines Drachenkopfes.
Warum werden diese grausigen Geschichten am Ende des Kirchenjahres bis in den Advent hinein erzählt. Advent ist die Erwartung der Wiederkehr Christi. Er ist der Menschensohn, der auf der Wolke als Weltenrichter erscheint, wenn das Chaos auf der Erde Überhand nimmt. In diese Situation sind die apokalyptischen Bücher geschrieben. Als die Juden in der Zeit des persischen Reiches und die Christen im römischen Reich verfolgt wurden, haben sie die weltlichen Mächte mit Drachen und Löwen dargestellt. Das lag auch nahe, denn im römischen Zirkus wurden die, die vor dem Götzenbild nicht geopfert hatten, den Tieren vorgeworfen. Wie sollte man in diesen Zeiten die Hoffnung aufrecht halten:
Zum 1. Adventssonntag heißt es im Lukasevangelium:
         Es werden Zeichen sichtbar werden an Sonne, Mond und Sternen,
         und auf der Erde werden die Völker bestürzt und ratlos sein über
         das Toben und Donnern des Meeres. Die Menschen werden vor Angst
         vergehen in der Erwartung der Dinge, die über die Erde kommen werden.
         Dann wird man den Menschensohn mit großer Macht und Herrlichkeit auf
         einer Wolke kommen sehen. Wenn all das beginnt, dann richtet euch auf,
         erhebt eure Häupter; denn eure Erlösung naht.  Kap. 21 25-28

Das Evangelium gibt als Verhaltensregel:
Wacht und betet allezeit, damit ihr allem, was geschehen wird, entrinnen und vor den Menschensohn hintreten könnt. V. 36
