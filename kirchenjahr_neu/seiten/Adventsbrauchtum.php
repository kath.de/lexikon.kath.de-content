titel:Adventsbrauchtum
Startseite:"Hier ensteht das neue Kirchenjahrslexikon"
stichworte:Adventsbrauchtum, Frautragen, Herbergssuche, Totenbräuche, Adventsblasen, Adventssingen,
Barbara und Lucia,Weihnachtsgebäck, kath
bild:bild.jpg

Neben dem Adventskalender und dem Adventskranz gibt es noch Brauchtum, das den Advent gestaltet.

Frautragen
In Tirol und Oberbayern wurde ein alter Brauch wiederbelebt. Ein Marienbild wird an den neun letzten Abenden vor Weihnachten von einem Bauerngehöft zum anderen getragen und auf den Hausaltar gestellt. Die Nachbarschaft versammelt sich um das Bild und betet gemeinsam. In der Weihnachtsnacht wird das Bild dann in die Pfarrkirche gebracht.

Familienandachten
In den letzten Jahren bemühen sich einige Diözesen, den Brauch von Familienandachten, häusliche Gebetsstunden in der Adventszeit, einzuführen.

Herbergssuche
Ein alpenländischer Brauch ist die Herbergssuche. Jugendliche ziehen von Haus zu Haus, singen ein Herbergslied und erhalten Gaben. Teilweise werden dabei auch kleine Spiele aufgeführt. Diese Herbergssuche hängt wohl mit den sogenannten Klöpfel-Nächten zusammen. In den drei Donnerstagsnächten des Advents ziehen Kinder und Jugendliche umher, klopfen gegen Türen und Fenster und werfen Erbsen, singen oder sprechen Verse. Es handelt sich wohl ursprünglich um einen Orakelbrauch. Der Brauch wird in Süddeutschland geübt, heißt in Baden, im Elsaß und der Schweiz Bosseins oder Bochselns.

Totenbräuche
Während im Kirchenjahr das Totengedächtnis auf den November konzentriert ist, leben im Advent noch Totenbräuche fort. Es gibt wohl Vorstellungen, dass die Toten in den Winternächten unterwegs sind (Wilde Jagd). Dämonisches wirkt in den Gestalten des Knecht Ruprecht, Hans Muff und Krampus weiter <a href="s. Nikolaus.php">(s. Nikolaus)</a> .

Adventsblasen
In den verschiedenen Regionen hat sich das Adventsblasen erhalten, das auf einen Brauch der Hirten in der vorweihnachtlichen Zeit zurückgeht.

Adventssingen
Dass das Brauchtum auch in den audiovisuellen Medien wirksam ist, zeigen z.B. das Adventssingen an den Adventssonntagen sowie Reportagen über Adventsbräuche.

Barbara und Lucia
Neben dem Nikolaustag haben einzelne Heiligengedenktage im Advent haben ein besonderes Brauchtum ausgebildet, das in Verbindung mit Advent und Weihnachten zu sehen ist.
Am Barbara-Tag (4. Dezember) werden Zweige von Kirschbäumen oder anderen Obstbäumen abgeschnitten und in Wasser gestellt, damit sie Weihnachten blühen. Die blühenden Zweige sollen wohl ursprünglich versinnbildlichen, dass das Leben über den Winter hinübergerettet wird.
Der Lucia-Tag (13. Dezember) ist sowohl in Südeuropa wie auch in Nordeuropa mit vielfältigem Brauchtum verknüpft. Er war bis zur gregorianischen Kalenderreform der alte Mitt-Wintertag. Es gibt Orakelbräuche, Bräuche zur Dämonenabwehr. Lucia erscheint als Gabenbringerin und in Schweden als Lichtbringerin.

Weihnachtsgebäck
Zu den Gepflogenheiten des Advents gehört auch das Backen des Weihnachtsgebäcks. Illustrierte und Bücher bieten hier einen reichhaltigen Service.
