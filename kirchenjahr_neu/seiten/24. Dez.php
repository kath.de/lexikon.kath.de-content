titel:24. Dezember, Adam und Eva
Startseite:"Hier ensteht das neue Kirchenjahrslexikon"
stichworte:24. Dezember, Adam und Eva, Heiliger Abend, Sündenfall, Erlösung, kath
bild:bild.jpg

Der 24. Dezember war in katholischen Gegenden bis Mittag ein Fasttag. Erst am späten Nachmittag begann Weihnachten, wie in jüdischer Tradition ein Fest am Vortag beginnt. Heringssalat oder eine andere Fastenspeise gehörten noch bis in jüngste Zeit zum Ablauf des Heiligen Abends, der eben erst nach Sonnenuntergang beginnen kann.
Der Tag selbst zog eine Verbindungslinie von der Geburt Jesu zum Sündenfall, die auf die älteste Prophezeiung der Bibel Bezug nimmt. Gott bestimmt nach dem Sündenfall über die Schlange: "Weil du das getan hast, bist du verflucht unter allem Vieh und allen Tieren des Feldes. Auf dem Bauch sollst du kriechen und Staub fressen alle Tage deines Lebens. Feindschaft setze ich zwischen dich und die Frau, zwischen deinen Nachwuchs und ihrem Nachwuchs. Er trifft dich am Kopf und du triffst ihn an der Ferse." (Genesis 3, 14f) Jesus ist das Kind und Maria die neue Eva. Die Erlösung ist nur verständlich zu machen, wenn auch von dem Sündenfall gesprochen wird, der überhaupt erst die Erlösung notwendig machte.
Daß der 24. Dezember Geburtstag des ersten Menschenpaares wurde, begründet sich auch aus dem Stammbaum Jesu, den Lukas von Jesus zurück bis auf Adam zurück verfolgt (3, 38), während Matthäus den Stammbaum mit Abraham beginnen läßt (1,1)
