titel:Heilige Familie
Startseite:"Hier ensteht das neue Kirchenjahrslexikon"
stichworte:Heilige Familie, Matthäus- und Lukasevangelium, Jesus, kath
bild:bild.jpg

Sonntag nach Weihnachten: Fest der Heiligen Familie

Am Sonntag nach Weihnachten wird ein sehr junges Fest begangen, das erst im 19. Jahrhundert entstanden ist. Die heilige Familie wird als Vorbild gesehen und ihre Verehrung gefördert. In den sozialen Umbrüchen der beginnenden Industrialisierung betonte die katholische Kirche den Wert der Familie. Über die Zeit nach der Geburt finden sich Berichte im Matthäus- wie im Lukasevangelium, die über die Zeit handeln, als Jesus noch nicht öffentlich auftrat. Das Matthäusevangelium berichtet von der Flucht nach Ägypten, um das Kind vor dem Zugriff des Königs Herodes zu retten. Das Lukasevangelium berichtet davon, daß Jesus mit den Theologen im Tempel diskutierte. Er war 12 Jahre alt, das Alter, in dem ein jüdischer Junge volles Mitglied der Gottesdienstgemeinde wird.
