titel:Adventskalender
Startseite:"Hier ensteht das neue Kirchenjahrslexikon"
stichworte:Adventskalender, Kalender, Weihnachten, kath
bild:bild.jpg

Damit Kinder sich auf Weihnachten vorbereiten können, erhalten sie einen Adventskalender. Er zählt vom 1. bis 24. Dezember. Vorausgegangen waren im 19. Jahrhundert Abreißkalender, man konnte auch die Tage mit Kreidestrichen zählen oder durch Tageskerzen, auf denen die Zählung aufgebracht war. Es gab auch sog. Weihnachtsuhren oder Weihnachtsleitern mit jeweils einer Stufe für einen Tag. Wahrscheinlich druckte der Verleger Gerhard Lang 1908 den ersten Kalender mit den aufklappbaren Türchen. Die Nazis versuchten, die christlichen Hinweise durch Märchenmotive zu verdrängen.
