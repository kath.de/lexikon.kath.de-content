titel:Aventszeit
Startseite:"Hier ensteht das neue Kirchenjahrslexikon"
stichworte:Aventszeit, Advent, Weihnachten, kath
bild:bild.jpg

Ähnlich wie Ostern hat auch Weihnachten eine Vorbereitungszeit ausgebildet. Heute sind es die Wochen zwischen dem 1. Adventssonntag und Weihnachten. In Gallien gab es, ähnlich wie vor Ostern, eine sechswöchige Vorbereitungszeit mit einem Karnevals-Vorspann. Der 11.11. als Beginn der Karnevalszeit hat dort seinen Ursprung. Denn der Termin für den Beginn der Adventszeit im frühen Christentum in Frankreich ergibt sich aus einer Berechnung von 40 Wochentagen bis zum 6. Januar, der anfangs der Termin des Weihnachtsfestes in Gallien war. Die Wochentage, nicht die Samstage und Sonntage, waren Fasttage. Dann war der 11. November die "Fastnacht" für den Advent.

Der Advent ist heute keine Zeit der Erwartung mehr, es wird auf den Weihnachtsmärkten, in Konzerten bereits Weihnachten gefeiert, so dass die Zeit nach Weihnachten kaum noch durch Tannegrün, Krippen und Weihnachtsbrauchtum geprägt ist.
