titel:Krippenspiele
Startseite:"Hier ensteht das neue Kirchenjahrslexikon"
stichworte:Krippenspiele, Weihnacht, Spiel, drei Könige, kath
bild:bild.jpg

Mysterienspiele hat das Mittelalter in direktem Zusammenhang mit dem Gottesdienst entwickelt. So wurde auch das Weihnachtsgeschehen zu Beginn der Weihnachtsmesse szenisch dargestellt. Das Krippenspiel begann mit der Geschichte vom Sündenfall, zu dem die Menschwerdung des Erlösers in unmittelbarem Zusammenhang steht. Das Krippenspiel ist dann auch der Kirche ausgewandert und findet sich im Puppenspiel. Den Kaspar von den drei Königen Caspar, Melchior und Balthasar hat es mitgenommen. In Köln hat sich diese Beziehung noch in dem Ausdruck "Kreppche" für ein Puppentheater erhalten.
