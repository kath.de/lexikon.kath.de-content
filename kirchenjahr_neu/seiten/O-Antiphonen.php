titel:O-Antiphonen
Startseite:"Hier ensteht das neue Kirchenjahrslexikon"
stichworte:O-Antiphonen, Advent, Herr, kath
bild:bild.jpg

In den letzten sieben Tagen des <a href="Advent.php">Advent</a> , vom 17. bis 23. Dezember, konzentriert sich die Erwartung auf das Erscheinen des Messias. Bis heute warten die Juden auf den Messias, die Christen sehen in ihm den "Gesalbten". Christus ist das griechische Wort für den Gesalbten, Chrisam wird das Öl genannt, mit dem die Täuflinge gesalbt werden.
Antiphonen sind Gebetsverse. Bei der der Vesper im Stundengebet werden sie vor und nach dem Magnifikat gebetet.
Die O-Antiphonen wenden sich an den kommenden Messias. Sie beginnen jeweils mit "O" und haben daher ihren Namen.

17.12. O Weisheit, hervorgegangen aus Gottes Mund, mächtig wirkst du in aller Welt, und freundlich ordnest du alles. Komm, o Herr, und lehre uns den Weg der Einsicht.
18.12. O Herr und Fürst des Hauses Israel, du bist dem Mose erschienen in der Flamme des Dornbuschs und gabst ihm das Gesetz am Sinai. Komm, o Herr, und erlöse uns mit starkem Arm.
19.12. O Wurzel Jesse, gesetzt zum Zeichen für die Völker. Vor dir verstummen die Mächtigen, zu dir rufen die Völker. Komm, o Herr, und erlöse uns, zögere nicht länger.
20.12. O Schlüssel Davids und Zepter des Hauses Israel, du öffnest und niemand schließt, du schließest und niemand öffnet. Komm, o Herr, und befreie aus dem Kerker den Gefangenen, der da sitzt in Finsternis und im Schatten des Todes.
21.12. Anfang, Glanz des ewigen Lichtes, du Sonne der Gerechtigkeit, komm,  o Herr, und erleuchte uns, die wir sitzen in Finsternis und im Schatten des Todes.
22.12. O König der Völker, den sie alle ersehnen. Du Eckstein, der das Getrennte eint. Komm, o Herr, und befreie den Menschen, den du aus Erde erschaffen hast.
23.12. O Immanuel, Gott mit uns. Du König und Lehrer, du Sehnsucht der Völker und ihr Heiland. Komm, o Herr, und erlöse uns, Herr, unser Gott.
