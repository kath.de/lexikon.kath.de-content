titel:Adventskranz
Startseite:"Hier ensteht das neue Kirchenjahrslexikon"
stichworte:Adventskranz, Johann Hinrich Wichern, Licht, kath
bild:bild.jpg

Der Gründer des "Rauen Hauses" in Hamburg, Johann Hinrich Wichern, führte im 19. Jahrhundert für Adventsandachten in dieser von ihm gegründeten sozialen Einrichtung einen Kranz von Tannenzweigen ein. Er gebrauchte noch 24 Kerzen. Der Adventskranz mit vier Kerzen wurde nach dem 1. Weltkrieg zum allgemeinen Brauch. Lichter zu entzünden ist nicht nur durch den dunklen Winter motiviert, sondern das Licht steht für Christus. Als Maria und Joseph das Kind in den Tempel bringen, begrüßt der Prophet Simeon das Kind, "ein Licht, das die Heiden erleuchtet und Herrlichkeit für das Volk Israel" (Lukas 2,32).
