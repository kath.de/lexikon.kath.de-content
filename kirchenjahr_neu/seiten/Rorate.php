titel:Rorate-Messen
Startseite:"Hier ensteht das neue Kirchenjahrslexikon"
stichworte:Rorate-Messen, Rorate, Adevent, kath
bild:bild.jpg

Neben den Adventssonntagen werden Wochentage noch besonders durch die sogenannten Rorate-Messen herausgehoben. Rorate ist das Anfangswort des Eingangsverses (Rorate caeli desuper - Tauet Himmel... Jes 45,2).
Sie stellen Maria in den Mittelpunkt, die den Sohn Gottes empfangen hat. Rorate-Messen werden an den Samstagen oder an anderen Tagen bis zum 16. Dezember gefeiert. Denn an den letzten sieben Tagen vor Weihnachten, vom 17. bis 23., werden die <a href="o-antiphonen.php">O-Antiphonen</a> gebetet oder gesungen. Die Rorate-Messen geben dem Advent einen besonderen Charakter und wurden früher durch szenische Darstellungen wie die Begegnung Marias mit dem Erzengel Gabriel und anderen Begebenheiten der Kindheitsgeschichte aus dem Üblichen herausgehoben.
