<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<meta name="title" content="Liturgie">
<meta name="author" content="Redaktion kath.de">
<meta name="publisher" content="kath.de">
<meta name="copyright" content="kath.de">
<meta name="description" content="">
<meta name="abstract" content="Lexikon �ber die Liturgie der katholischen Kirche">
<meta http-equiv="content-language" content="de">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="date" content="2006-00-01">
<meta name="robots" content="index,follow">
<meta name="revisit-after" content="10 days">
<meta name="revisit" content="after 10 days">
<meta name="DC.Title" content="Liturgie">
<meta name="DC.Creator" content="Redaktion kath.de">
<meta name="DC.Contributor" content="J�rgen Pelzer">
<meta name="DC.Rights" content="kath.de">
<meta name="DC.Publisher" content="kath.de">
<meta name="DC.Date" content="2006-00-01">
<meta name="DC.Description" content="Lexikon �ber die Liturgie der katholischen Kirche">
<meta name="DC.Language" content="de">
<meta name="DC.Type" content="Text">
<meta name="DC.Format" content="text/html">
<meta name="DC.Identifier" content="http://www.kath.de/lexikon/liturgie/">
<meta name="keywords" lang="de" content="empirisch, Zeichen, religi�s, Werte, Beziehung">
<title>Symbol, seine Funktion</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="kaltefleiter.css" title="fonts">
</head>
<body bgcolor="#ffffff" leftmargin="6" topmargin="6" marginwidth="6" marginheight="6">
<table width="100%" border="0" cellspacing="0" cellpadding="6">
  <tr>
    <td width="100" align="left" valign="top" height="3"> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b><font face="Arial, Helvetica, sans-serif">Das 
            Liturgie Lexikon</font></b></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td> <?php include("logo.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><font face="Arial, Helvetica, sans-serif"><strong>Inhaltsverzeichnis 
            </strong></font></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10"> <?php include("az.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
    <td rowspan="2" valign="top">
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td background="boxtop.gif" colspan="2"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="9"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif" width="8"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2" colspan="2"> 
            <h1><font face="Arial, Helvetica, sans-serif">Symbol, seine Funktion</font></h1>
          </td>
          <td background="boxtopright.gif" width="9"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif" colspan="2"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td width="9"><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif" width="8"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="L12" width="516"> 
            <p><strong><font face="Arial, Helvetica, sans-serif">Zeichen, die
                  etwas ausdr&uuml;cken, das empirisch nicht verifzierbar ist<br>
              </font></strong></p>
            <p><font face="Arial, Helvetica, sans-serif">Empirisch-faktische
                Wirklichkeiten, Gesetze oder Befehle lassen sich sprachlich einfach
                vermitteln. So z. B.: &#8222;Das Gras w&auml;chst.
              Es ist deshalb vierw&ouml;chentlich zu m&auml;hen. Hole den Rasenm&auml;her!&#8220;<br>
              Schlie&szlig;lich handelt es sich dabei um evidente und eindeutige
              Aussagen. <br>
              Wenn ich aber jemand erkl&auml;ren will, wie sehr ich ihn liebe
              oder hasse, so stellt sich ein sprachliches Problem. Wie kann ich
              meine inneren Empfindungen mitteilen, obwohl sie nur von mir selbst
              so wahrgenommen und erlebt werden? Wie erm&ouml;gliche ich meinem
              Gegen&uuml;ber, eine Idee meiner Empfindungen zu bekommen? <br>
              Innere Empfindungen werden verst&auml;ndlich, indem ich beispielsweise
              einen Strauss Rosen schenke oder einem Gesch&auml;ftsfreund Wein
            und &Ouml;l mitbringe. </font></p>
            <p><font face="Arial, Helvetica, sans-serif">Ein &auml;hnliches Problem
                stellt sich, wenn die Wirkung abstrakter Werte verdeutlicht oder
                werden soll. <br>
              So z. B. wie &#8222;Einigkeit und Recht und Freiheit&#8220; als
              volksverbindende Tugenden verdeutlicht werden sollen. 
              Deutlich wird dies an einer gegenst&auml;ndlichen Metapher, beispielsweise
              einer Nationalflagge. So wird zum Beispiel aus einem St&uuml;ck
              Stoff, das in den Farben Schwarz, Rot, Gold gestreift ist, eine
              Deutschlandfahne.Erst einmal ist das St&uuml;ck Stoff nur ein <a href="zeichen_verstehen.php">Zeichen</a>.
               Die Bedeutung des Zeichens macht es zur eienr  Fahne, die identisch
              mit den Werten, den Tugenden und der Geschichte
              des
              Deutschen
              Volkes
              selbst ist. Es gibt also eine
              feste Beziehung zwischen dem Symbol und der darin eingeschlossenen
              Bedeutung. Die Bedeutung aber, d.h. der Inhalt des Symbols kann
              sich dynamisch fortentwickeln, ohne dass die Fahne ihr Aussehen &auml;ndert.
              Diese Fahne steht dann neben dem Bundespr&auml;sidenten bei seiner
              Weihnachtsansprache, erscheint bei Staatsempf&auml;ngen, wird bei
              der Siegerehrung nach einem sportlichen Wettkampf aufgezogen und
              wird in vielen L&auml;ndern von den Menschen spontan am Haus aufgeh&auml;ngt. <br>
              Durch die Vergegenst&auml;ndlichung im Symbol k&ouml;nnen die abstrakten
              Gr&ouml;&szlig;en wie deutsche Tugenden, deutsche Werte und deutsche
              Geschichte zu aktiven oder passiven Akteuren werden. Das ist die
              Voraussetzung daf&uuml;r, da&szlig; man durch Verbrennen einer
              Deutschlandflagge das deutsche Volk beleidigen kann. <br>
              Noch schwieriger wird die sprachliche Herausforderung, wenn Glaubensinhalte
              vermittelt werden sollen, die ja ihrer Eigenart nach nicht als
              empirische Erkenntnisse gelten und auch nicht an empirischen Wirklichkeiten &uuml;berpr&uuml;ft
              werden k&ouml;nnen. Hier legt es sich nahe, die Funktionsweise
              eines Symbols n&auml;her zu betrachten. Es geht im religi&ouml;sen
              Symbol n&auml;mlich darum, die empirisch nicht fa&szlig;bare Wirklichkeit
              mit einem sichtbaren Zeichen zu verbinden. Das ist die urspr&uuml;ngliche
              Wortbedeutung von<a href="http://www.kath.de/kurs/symbole/"> Symbol</a>, das sich von griechisch sym-ballein,
              zusammenfallen herleitet und zwei Pole zusammenbindet. Die Beziehung
              wird zwischen dem sichtbaren und sinnenhaften Pol eines Symbols
              (Farbe, Zeichen, Material, etc.) und dem Bedeutungspol hergestellt.
              Diese zwei Pole oder Ebenen sind allen Symbolen gemein. Der eine,
              sichtbare Pol hat einen empirisch-faktischen Bezug, der andere
              einen wertbezogenen und abstrakten. Die Aussage, die nun auf der
              empirisch-faktischen Seite des Symbols entsteht (z. B. Brot. Es
              ern&auml;hrt, der N&auml;hrwert ist empirisch zu &uuml;berpr&uuml;fen),
              gilt auch f&uuml;r den abstrakt-wertbezogenen Pol. Wie das Brot
              in nat&uuml;rlichem Sinn n&auml;hrt, so n&auml;hrt Brot, das im
              religi&ouml;sen Kult verwendet wird, auch die Seele. Religi&ouml;se
              Symbole zeichnen sich dadurch aus, dass der empirische Pol meist
              der Natur oder dem Bereich der menschlich-nat&uuml;rlichen Bed&uuml;rfnisse
              entnommen ist, der wertbezogene Pol sich auf Aussagen bezieht,
              die aus dem religi&ouml;sen Bereich kommen.</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Wie aber wird ein Gegenstand
                zu einem Symbol? Wenn er im Stand ist, &uuml;ber seine urspr&uuml;ngliche Funktion hinaus (z. B.
              Kerze: Leuchten und W&auml;rme abstrahlen) eine dynamische Bedeutungsfunktion
              zu &uuml;bernehmen (stellvertretend f&uuml;r das Gebet von Personen
              stehen).<br>
              Entscheidend f&uuml;r das Funktionieren eines Gegenstandes als
              Symbol ist, dass der Gegenstand Erinnerungen an Erlebnisse und
              damit an Inhalte, an Geschichten zu wecken vermag. In der christlichen
              <a href="eucharistie_messe_abendmahl.php">Eucharistiefeier</a> werden Brot und Wein zum Symbol, weil dem die
              Erz&auml;hlung vom Letzten <a href="http://www.kath.de/Kirchenjahr/gruendonnerstag.php">Abendmahl</a> Jesu mit seinen J&uuml;ngeren
            zugrunde liegt. </font></p>
            <p><font face="Arial, Helvetica, sans-serif">Ein
                besonderes Problem entsteht im religi&ouml;sen Bereich, wenn
                das Symbol die feiernde Gemeinde mit Gott in Beziehung setzen
                soll. Gott, der dem Begriff nach jedes
              Verstehen &uuml;bersteigt, wird von keinem Symbol wirklich erfasst,
              sondern es werden einzelne Bedeutungsaspekte symbolisch dargestellt.
              Deshalb ist das Sprechen von Gott nur dann gerechtfertigt, wenn
              es im Bewusstsein geschieht, dass jeweils nur ein Fragment vermittelt
            werden kann.<br>
            </font><font face="Arial, Helvetica, sans-serif">Die
            der Symbolsprache inh&auml;rente Bedeutungsunsch&auml;rfe
              macht diese Sprache f&uuml;r manche zum Problem. Sie produziert
              keine pr&auml;zisen, exakt nachmessbaren und auch keine juristisch
              durchsetzbare Aussagen.<br>
              Weltanschauungen, die vorgeben, jede Wirklichkeit empirisch-faktisch
              verstehen und beschreiben zu k&ouml;nnen, tun sich mit Symbolen
              schwer. Das rechtfertigt aber nicht, die Symbolsprache als &uuml;berfl&uuml;ssig
              zu deklarieren. Entscheidende menschliche Dimensionen, wie die
              Emotionalit&auml;t, die Qualit&auml;t zwischenmenschlicher Beziehungen,
              die &Uuml;bereinstimmung in bestimmten Werthaltungen, der Bereich
              des Religi&ouml;sen und viele kulturelle Lernvorg&auml;nge blieben
            ohne diese Sprachebene den Menschen verschlossen. </font></p>
            <p><font face="Arial, Helvetica, sans-serif">In Herrschaftssystemen,
                die auf Eindeutigkeit bedacht sind, besteht die Gefahr, die Bedeutung
                der Symbole und die Art und Weise ihres
              Gebrauches &#8222;definieren&#8220; zu wollen. Ein Symbol ist aber
              seiner Natur nach nicht definierbar im Gegensatz zum Zeichen, das
              meist nur eine einzige und fast immer eine eindeutige Bedeutung
              besitzt. <br>
              Volksfr&ouml;mmigkeit und religi&ouml;se Kulturen sind von Symbolen
              gepr&auml;gt. Sobald jedoch Religionen sich in Institutionen etablieren,
              k&ouml;nnen Symbole statisch festgeschrieben werden, d.h. ein bestimmter
              Bedeutungsaspekt wird festgeschrieben. Es besteht dann die Gefahr,
            dass Symbole ihrer eigentlichen Funktionsweise entleert werden.</font></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Bilderverbot und Symbole</strong><br>
              Das Alte Testament verbot, dass Bilder von Gott entstehen. Im Islam
                und im Judentum wird dies streng eingehalten.<br>
              Gleichzeitig wurde durch das IV. Laterankonzil definiert, dass
              Aussagen &uuml;ber Gott sind immer mehr unzutreffend als zutreffendes
            transportieren.</font></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Theologische Deutung
            der religi&ouml;sen Symbole</strong><br>
            </font><font face="Arial, Helvetica, sans-serif">Lange vor der Sprachphilosophie
                entwickelte die Theologie eine Symboltherorie, um die Beziehung
                zwischen dem
              Gegenstand, der etwas bedeutet (Signum) und der Bedeutung selbst
              (res) zu verdeutlichen. Unterschieden wird dabei das Zeichen, d.h.
              die sinnenhaft wahrnehmbare Dimension (akzidens) von der Wirklichkeit,
              die sich in dem Zeichen verbirgt (substans). <br>
              Wenn nun die Beziehung zwischen dem Zeichen und der darin sich bergenden
              theologischen Wirklichkeit als glaubensentscheidend und als geoffenbart,
              d.h. vom gemeinsamen Glauben getragen, begriffen wird, spricht die
              Theologie von einem Realsymbol, gleichbedeutend mit dem Sakrament.
            </font></p>            
            <p><font face="Arial, Helvetica, sans-serif">Theo Hipp</font> </p>
            <p></p>
            <p>&nbsp;            </p>
            <p><br>
            </p>
            <p><font size="2" face="Arial, Helvetica, sans-serif"><a href="http://www.kath.de/">&copy; www.kath.de</a></font></p>
          </td>
          <td class="L12" width="1">            <div align="right"><br>
              <br>
              </div>
          </td>
          <td background="boxright.gif" width="9"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif" colspan="2"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td width="9"><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
</table>
</body>
</html>
