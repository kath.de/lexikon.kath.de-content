<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="kaltefleiter.css" title="fonts">
</head>
<body bgcolor="#ffffff" leftmargin="6" topmargin="6" marginwidth="6" marginheight="6">
<table width="100%" border="0" cellspacing="0" cellpadding="6">
  <tr>
    <td width="100" align="left" valign="top" height="3"> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b><font face="Arial, Helvetica, sans-serif">Das 
            Liturgie Lexikon</font></b></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td> <?php include("logo.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><font face="Arial, Helvetica, sans-serif"><strong>Inhaltsverzeichnis 
            </strong></font></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10"> <?php include("az.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
    <td rowspan="2" valign="top">
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td background="boxtop.gif" colspan="2"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif" width="8"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2" colspan="2"> 
            <h1><font face="Arial, Helvetica, sans-serif">Stundengebet</font></h1>
          </td>
          <td background="boxtopright.gif" width="8"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif" colspan="2"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td width="8"><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif" width="8"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="L12" width="70%"> 
            <p>&nbsp;</p>
            <p><font face="Arial, Helvetica, sans-serif"><a href="http://www.tedeum-beten.de/" target="_blank">  </a>�ber den Tag 
              verteilt gibt es sieben Gebetszeiten, zu denen sich die Mitglieder
                einer Abtei zusammenfinden. Sie werden von Priestern und Diakonen
                alleine
              oder in kleinen Gemeisnchaften gebetet werden. Diese Gesamtheit
                an Gebeten, die an einem Tag immer zu bestimmten Stunden gebet
                werden,
              nennt man das Stundengebet.</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Dem Stundengebet 
              liegt die Siebenzahl zugrunde, denn im Psalm 119, 164 hei�t es: 
              "Siebenmal am Tag singe ich dein Lob wegen deiner gerechten Entscheide." 
              Paulus ruft in seinen Briefen immer wieder zu h�ufigem Gebet auf, 
              in 1 Thess 5,17 hei�t es "Betet ohne Unterlass!" Im M�nchtum des 
              3. Jahrhunderts wurden entsprechend dieses Apostelwortes jeden Tag 
              die 150 Psalmen gebetet. Die Psalmen wurden deshalb als Grundlage 
              f�r das Stundengebet genommen, weil sie das Gebetbuch Jesu selbst 
              waren und daher eine besonders hohe Bedeutung hatten. Eine andere 
              Bezeichnung f�r das Stundengebet ist "Offizium", was soviel meint 
              wie Dienst. Auch wenn Priester und Diakone zum Stundengebet verpflichtet 
              sind, ist das ganze Volk, also die gesamte Gemeinde wie auch die 
              gesamte Abtei Tr�ger der Stundenliturgie ist. Die lateinsiche Bezeichnung 
              Liturgia Horarum deutet darauf hin, da� das Stundengebet Liturgie 
              ist, weil sich die Beter an den Vater richten, durch Jesus Christus 
              im Heiligen Geist. Stundenliturgie heisst, dass zu den sieben klassischen 
              Horen (Stunden) gebetet wird. Die Stundenliturgie umrahmt die Messfeier 
              und entfaltet sie �ber die 24 Stunden des Tages. Das II. Vatikanum 
              sagt: Die Stundenliturgie ist die Fortf�hrung der Messfeier.Benedikt
              8-18 </font></p>
            <p><font face="Arial, Helvetica, sans-serif">Auch der Engel 
              des Herrn und der Rosenkranz stellen eigene Stundengebete dar.
                In der Regel des hl. Benedikt findet sich folgende Einteilung
                f�r die 
              Stundenliturgie. Die Zeiten entsprechen dem damaligen Tagesrhythmus, 
              als man sehr viel fr�her zu Bett ging und fr�ehr aufstand. </font></p>
            <p><font face="Arial, Helvetica, sans-serif">03:00 Uhr Laudes 
              (lat. Laudes = Loblieder) - Morgengebet - Morgenhore (Die erste
              gro�e Hore (lat. Hora = Stunde))</font></p>
            <p><font face="Arial, Helvetica, sans-serif"> 06:00 Uhr Prim 
              (lat. 1. Stunde) seit dem II. Vatikanum abgeschafft) - Die 3 mittleren 
              (andere Bezeichnung: kleine) Horen: </font></p>
            <p><font face="Arial, Helvetica, sans-serif">09:00 Uhr Terz 
              (lat. = 3. Stunde) </font></p>
            <p><font face="Arial, Helvetica, sans-serif">12:00 Uhr Sext 
              (lat. = 6. Stunde) </font></p>
            <p><font face="Arial, Helvetica, sans-serif">15:00 Uhr Non 
              (lat. = 9 Stunde) - Mittagsgebet </font></p>
            <p><font face="Arial, Helvetica, sans-serif">18:00 Uhr Vesper 
              - Abendgebet - Abendhore (Die zweite gro�e Hore)</font></p>
            <p><font face="Arial, Helvetica, sans-serif"> 21:00 Uhr Komplet 
              - Nachtgebet - Nachthore </font></p>
            <p><font face="Arial, Helvetica, sans-serif">00:00 Uhr Lesehore 
              (fr�her: Matutin) beziehungsweise Vigil </font></p>
            <p><font face="Arial, Helvetica, sans-serif" color="#666600">Stundenliturgie 
              als Exerzitienprogramm</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Erste Vesper 
              ist die Sch�pfung, Mitternacht ist die Wiederkunft, Laudes Auferstehung, 
              Terz, Sext, Non sind Passion, Zweite Vesper ist die Menschwerdung. 
              Von Sch�pfung bis Wiederkunft ergibt sich ein Zyklus. Die Einteilung 
              der mittleren (auch kleinen) Horen in 1., 3., 6. und 9. Stunde orientiert 
              sich an der alten antiken Zeitrechnung, nach der 06.00 Uhr morgens 
              die erste Stunde war. Bei Benedikt von Nursia vermehrt sich die 
              Zahl der Horen von f�nf auf acht. Allerdinsg z�hlte man damals die 
              Nachthore und die Laudes als eine Einheit, so dass es auch damals 
              nur 7 Horen gab. Nach der Abschaffung der Prim durch das II. Vatikanum 
              gibt es derzeit 7 Horen, da Komplet und Laudes als eigenst�ndige 
              Horen angesehen werden. Kleriker, die keinem Kloster angeh�ren, 
              sollen wenigstens Laudes und Vesper, die Lesehore, eine mittlere 
              Hore und die Komplet beten. "Eckpfeiler" des Stundengebets sind
              das Morgen- und Abendgebet. </font></p>
            <p><font face="Arial, Helvetica, sans-serif" color="#666600">Bedeutung 
              der Stundenliturgie </font></p>
            <p><font face="Arial, Helvetica, sans-serif">Es gibt zwei 
              verschiedene Formen der Stundenliturgie: Das, was heute als Stundengebet
                in der r�m.-katholischen Kirche verpflichtend ist, ist das monastische 
              Studengebet. Es orientiert sich an den sieben Horen und dem Aufruf 
              zum immerw�hrenden Gebet. Die zweite Form, die bei der Liturgiereform 
              des II. Vatikanums verloren ging, ist die der kathedralen Stundenliturgie. 
              Sie war besonders am Abend und am Morgen, also am Tagesablauf und 
              am Licht orientiert. Die Symbolik des Stundengebets bzw. der Stundenliturgie 
              liegt darin, dass das Mysterium des Sonntags in der Stundenliturgie 
              gefeiert wird, die eine Auferstehungsliturgie ist, auch die Zeiten 
              des Alltags durchdringt. Bereits die Bezeichnung der Laudes als 
              Loblied deutet auf die nahe Verwandtschaft der Texte zu den Psalmen 
              hin. Die Hymnen, die jede Hore einleiten, sind jeweils Lobges�nge. 
              die Psalmen - jedoch als neutestamentliche "Psalmen". Die Lesungen 
              des Stundengebets vollenden, was die Psalmen verhei�en. Die Stundenliturgie 
              greift die Themen des Heils in den einzelnen Stunden des Tages auf: 
              Von der Auferstehung �ber die Sch�pfung, der Wiederkunft des Herrn, 
              der Herabkunft des Heiligen Geistes, den S�hnetod des Herrn, die 
              Menschwerdung. Der Tageslauf der Stundenliturgie ist also eine Osterfeier 
              auf den Tag umgelegt. Das Stundengebet gibt es in der orthodoxen, 
              r�misch-katholischen und der anglikanischen Kirche. Seit dem 20.
              Jahrhundert wird es auch von einigen evangelischen Kirchen wiederentdeckt. </font></p>
            <p><font face="Arial, Helvetica, sans-serif" color="#666600">Theologischer 
              Sinn </font></p>
            <p><font face="Arial, Helvetica, sans-serif">Die Stundengebete 
              dienen nicht nur einem sich erinnern: Vielmehr erneuert Gott in
                den Feiern das einst gewirkte Heil, dessen man gedenkt, f�r das 
              Jetzt. Das Ged�chtnis schafft Gegenwart. In der Stundenliturgie
              begegnet der Beter dem Auferstandenen. Es ergeht an ihn der Ruf
              zur Nachfolge Christi. Die Stundenliturgie projiziert die Thematik
              des Kirchejahres auf einen Tag. Die Feier der Stundenliturgie ist
              daher ein Kirchenjahr im Kleinen. <strong><br>
              </strong></font></p>
            <p><font face="Arial, Helvetica, sans-serif">Feedback bitte an mit 
              dem Stichwort Liturgie-Lexikon an: redaktion@kath.de</font></p>
          </td>
          <td class="L12" width="30%"> 
            <div align="right"> 
              <p>                <font size="2" face="Arial, Helvetica, sans-serif"><br>
                <img src="stundengebet2.jpg" width="340" height="255" alt="Stundengebet Bild Uhr mit r&ouml;mischen Ziffern"> 
                <br>
                Das Stundengebet orientiert sich 
                an</font><font face="Arial, Helvetica, sans-serif"><br>
                <font size="2">der antikien r&ouml;mischen Zeitrechnung<br>
                <br>
              <a href="http://www.magnificat.de"><br>
              </a><a href="http://www.magnificat.de" target="_blank"><img src="banner-magnificat-01.jpg" width="286" height="70" border="0"></a> </font><br>
              <br>
              <a href="http://www.mein-tedeum.com/" target="_blank"><img SRC="TeDeum_Logo%20(2).gif" hspace="10" vspace="10" border="0" align="right" style="border: double 4px #009999"></a>              <br>
              </font></p>
            </div>
          </td>
          <td background="boxright.gif" width="8"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif" colspan="2"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
<td width="8"><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
 </tr> 
</table> 
</td>
<!-- neuer Code Anfang -->
<td style='vertical-align:top;'> 
     <script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
/* Symbollexikon neu */
google_ad_slot = "9464926645";
google_ad_width = 120;
google_ad_height = 600;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
     <br>
</td>
<!-- neuer Code Ende -->

  </tr>
  <tr>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
</table>
</body>
</html>
