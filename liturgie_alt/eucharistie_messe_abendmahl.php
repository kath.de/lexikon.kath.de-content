<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<meta name="title" content="Liturgie">
<meta name="author" content="Redaktion kath.de">
<meta name="publisher" content="kath.de">
<meta name="copyright" content="kath.de">
<meta name="description" content="">
<meta name="abstract" content="Lexikon �ber die Liturgie der katholischen Kirche">
<meta http-equiv="content-language" content="de">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="date" content="2006-00-01">
<meta name="robots" content="index,follow">
<meta name="revisit-after" content="10 days">
<meta name="revisit" content="after 10 days">
<meta name="DC.Title" content="Liturgie">
<meta name="DC.Creator" content="Redaktion kath.de">
<meta name="DC.Contributor" content="J�rgen Pelzer">
<meta name="DC.Rights" content="kath.de">
<meta name="DC.Publisher" content="kath.de">
<meta name="DC.Date" content="2006-00-01">
<meta name="DC.Description" content="Lexikon �ber die Liturgie der katholischen Kirche">
<meta name="DC.Language" content="de">
<meta name="DC.Type" content="Text">
<meta name="DC.Format" content="text/html">
<meta name="DC.Identifier" content="http://www.kath.de/lexikon/liturgie/">
<meta name="keywords" lang="de" content="Mahl, Danksagung, Erinnerung, Anamnese, Pr�fation">
<title>Eucharistie -  Messe - Abendmahl</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="kaltefleiter.css" title="fonts">
</head>
<body bgcolor="#ffffff" leftmargin="6" topmargin="6" marginwidth="6" marginheight="6">
<table width="100%" border="0" cellspacing="0" cellpadding="6">
  <tr>
    <td width="100" align="left" valign="top" height="3"> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b><font face="Arial, Helvetica, sans-serif">Das 
            Liturgie Lexikon</font></b></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td> <?php include("logo.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><font face="Arial, Helvetica, sans-serif"><strong>Inhaltsverzeichnis 
            </strong></font></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10"> <?php include("az.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
    <td rowspan="2" valign="top">
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td background="boxtop.gif" colspan="2"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="9"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif" width="8"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2" colspan="2"> 
            <h1><font face="Arial, Helvetica, sans-serif">Eucharistie</font></h1>
          </td>
          <td background="boxtopright.gif" width="9"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif" colspan="2"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td width="9"><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif" width="8"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="L12" width="516"> 
            <p><strong><font face="Arial, Helvetica, sans-serif">Gottesdienst
                  als Danksagung</font></strong></p>
            <p><font face="Arial, Helvetica, sans-serif">Dieses griechische Wort
                bezeichnet den zentralen christlichen Gottesdienst, das Abendmahl
                im evangelischen Sprachgebrauch, die
              Messe, wie die Eucharistiefeier auch in der katholischen Kirche
              genannt wird. Eucharistie kommt vom griechischen Verb &#8222;danken&#8220;.<br>
              Es werden also nicht nur Dankgottesdienste gefeiert, wie z.B. zum
              Erntedank, nach der Wiedervereinigung Deutschlands oder wie fr&uuml;her
              nach dem Ende einer Pestepidemie. Jede <a href="messe_aufbau.php">Messe</a> ist im Kern Dank.
              Eine Grundidee braucht jedes feierliche Mahl, das nicht nur der
              S&auml;ttigung dient. Eine Geburtstagsfeier wird meist als Essen
              arrangiert und jeder erwartet, da&szlig; einige Worte gesprochen
              werden. Diese haben oft auch den Dank als Thema, das Geburtstagskind
              dankt f&uuml;r die Jahre, die ihm geschenkt wurden, seinem Ehegatten
              und den Freunden, denn in der Regel sind zu der Feier die Menschen
              eingeladen, die dem Geburtstagskind nahe stehen.<br>
              In einem d&auml;nischen Film wird diese Grundstruktur des festlichen
              Mahles vor Augen gef&uuml;hrt, das im Verlauf des Mahles ein deutendes
              Wort verlangt. Der Film hei&szlig;t &#8222;Babettes Fest&#8220;.
              Babette ist eine Franz&ouml;sin, die es im 19. Jahrhundert an die
              Westk&uuml;ste D&auml;nemarks verschlagen hat. Sie kommt bei zwei
              Pfarrerst&ouml;chtern unter. Eines Tages erh&auml;lt sie einen
              Lottogewinn und veranstaltet mit dem Geld ein gro&szlig;es Fest.
              Eingeladen ist die kleine puritanische Gemeinschaft, die vom Vater
              der Schwestern aufgebaut worden war. Diese haben auf Grund ihrer
              asketischen Lebenseinstellung gro&szlig;e Schwierigkeiten, sich
              den Genu&szlig; zu g&ouml;nnen, den Babette ihnen bereitet. Ein
              ehemaliger General, der in Paris Milit&auml;rattach&eacute; war,
              erkennt nicht nur, wer die K&ouml;chin sein mu&szlig;. Sie hatte
              das bekannteste Restaurant gef&uuml;hrt, er deutet das Fest in
              einer kleinen Tischrede als Erfahrung, die die Gemeinschaft durch
              das Festessen machen k&ouml;nnen: Jede Hoffnung nach Gl&uuml;ck
              wird einmal in Erf&uuml;llung gehen. Der Film zeigt, wie sich die
              Herzen der Menschen, die etwas verbittert waren, sich &ouml;ffnen
              und sie wieder von dem Gl&uuml;ck &uuml;berzeugt werden, das ihr
              Glaube ihnen verspricht.<br>
              In der Eucharistie danken die Christen Gott f&uuml;r seinen Sohn.
              Da&szlig; er ihn als den Messias gesandt hat, da&szlig; durch seinen
              Lebensweg Erl&ouml;sung geschehen, das B&ouml;se &uuml;berwunden
              ist und ein neues Leben begonnen hat, das in die ewige Anschauung
              Gottes, das Leben im Himmel, m&uuml;nden wird. Im Ablauf des Kirchenjahres
              konkretisiert sich der Dank in den einzelnen Festen, Dank f&uuml;r
              die Geburt Jesu, f&uuml;r sein Leiden, seine Auferstehung, seine
              Himmelfahrt, die Sendung des Geistes, f&uuml;r seine Gegenwart
              in den eucharistischen Gaben an <a href="http://www.kath.de/Kirchenjahr/fronleichnam.php">Fronleichnam</a> und f&uuml;r das Geschenk
              der Gnade, das sich an den Heiligenfesten auf einzelne Menschen
              bezieht, &#8222;in denen die Gnade Gottes besonders wirksam geworden
              ist&#8220;.<br>
              Dieser Dank wird in der Pr&auml;fation zum Ausdruck gebracht, die
              in feierlichen Gottesdiensten gesungen wird, jedoch auch Bestandteil
              jeder Werktagsmesse ist.<br>
              Aus dem Danken folgt logisch der Grund, wof&uuml;r gedankt wird,
              die Erinnerung, griechisch Anamnese. An das Erinnern schlie&szlig;en
              sich wie bei einer Geburtstagsfeier W&uuml;nsche an. Man w&uuml;nscht
              dem Geburtstagskind Gesundheit und &#8222;noch viele Jahre&#8220;,
              im Gottesdienst w&uuml;nscht die Gemeinde von Gott, da&szlig; sie
              z.B. den inneren Frieden bewahrt, sie betet f&uuml;r den Papst,
              den Bischof, die Regierenden, f&uuml;r Menschen in Not. <br>
              Mit der Pr&auml;fation wird das zentrale Gebet der eucharistischen
            Mahlfeier eingeleitet, sie ist der erste Teil des Hochgebetes.</font></p>
            <p></p>
            <p></p>
            <p><font face="Arial, Helvetica, sans-serif">Zitate:<br>
  In Wahrheit ist es w&uuml;rdig und recht, dir, Herr, Heiliger Vater,
              allm&auml;chtiger, ewiger Gott, immer und &uuml;berall zu danken.
              Den Fleisch geworden ist das Wort, und in diesem Geheimnis erstrahlt
              dem Auge unseres Geistes das neue Licht seiner Herrlichkeit. In
              der sichtbaren Gestalt des Erl&ouml;sers l&auml;&szlig;t du uns
              den unsichtbaren Gott erkenn, um in uns die Liebe zu entflammen
              zu dem Was kein Auge geschaut hat. &#8230; Weihnachtspr&auml;fation</font></p>
            <p><font face="Arial, Helvetica, sans-serif">In Wahrheit ist es w&uuml;rdig und recht, die, allm&auml;chtiger
              Vater, zu danken und das Werk deiner Gnade zu r&uuml;hmen. Denn
              das Leiden deines Sohnes wurde zum Heil f&uuml;r die Welt. Seine
              Erl&ouml;sungstat bewegt uns, deine Gr&ouml;&szlig;e zu preisen.
              Im Kreuz enth&uuml;llt sich dein Gericht, im Kreuz erstrahlt die
              Macht des Retters, der sich f&uuml;r uns dahingab, unser Herr Jesus
              Christus. &#8230;. Pr&auml;fation vom Leiden Christi</font></p>
            <p><font face="Arial, Helvetica, sans-serif">In Wahrheit ist es w&uuml;rdig und recht, dir, Herr, heiliger
              Vater, immer und &uuml;berall zu danken und diesen Tag in festlicher
              Freude zu feiern. Denn heute hast du das &ouml;sterliche Heilswerk
              vollendet, heute hast du den Heiligen Geist gesandt &uuml;ber alle,
              die mit Christus auferweckt und zu deinen Kindern berufen hast.
              Am Pfingsttag erf&uuml;llst du diene Kirche mit Leben &#8230; Pfingstpr&auml;fation</font><br>
            </p>
            <p><font face="Arial, Helvetica, sans-serif"><a href="http://www.fernsehgottesdienst.de/">Hinweise
                  zu den aktuellen Gottesdienst&uuml;bertragungen im ZDF</a></font><a href="http://www.fernsehgottesdienst.de/"> </a></p>
            <p><font size="2" face="Arial, Helvetica, sans-serif"><a href="http://www.kath.de/">&copy; www.kath.de</a></font></p>
          </td>
          <td class="L12" width="1">            <div align="right"><br>
              <br>
              </div>
          </td>
          <td background="boxright.gif" width="9"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif" colspan="2"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td width="9"><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
</table>
</body>
</html>
