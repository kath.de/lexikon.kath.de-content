<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title>Christliches Zeitverst&auml;ndnis</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="kaltefleiter.css" title="fonts">
</head>
<body bgcolor="#ffffff" leftmargin="6" topmargin="6" marginwidth="6" marginheight="6">
<table width="100%" border="0" cellspacing="0" cellpadding="6">
  <tr>
    <td width="100" align="left" valign="top" height="3"> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b><font face="Arial, Helvetica, sans-serif">Das 
            Liturgie Lexikon</font></b></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td> <?php include("logo.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><font face="Arial, Helvetica, sans-serif"><strong>Inhaltsverzeichnis 
            </strong></font></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10"> <?php include("az.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
    <td rowspan="2" valign="top">
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td background="boxtop.gif" colspan="2"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8" height="37" background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2" colspan="2"> 
            <h1><font face="Arial, Helvetica, sans-serif">                Das
                Zeitverst&auml;ndnis</font></h1>
          </td>
          <td background="boxtopright.gif" width="8"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif" colspan="2"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td width="8"><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif" width="8"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="L12" width="70%"> 
            <p>&nbsp;</p>
            <p><font face="Arial, Helvetica, sans-serif">Mit dem Auftreten Jesu
                Christi kommt es zu einem Paradox: Da Jesus wahrer Mensch und
                wahrer Gott ist wird Ewigkeit Zeit. Das bedeutet aber die Aufhebung
                der Zeit. Das hat mit dem spezifisch christlichen Zeitverst&auml;ndnis
                zu tun. Es gibt nat&uuml;rlich auch nach Jesus Kommen noch Zeit. Die
                Atomuhr bei Paris misst ja die Zeit. Ein Zeitpnkt l&auml;sst sich
                einordnen: entweder vor oder nach dem Vergleichszeitpunkt. In
                dieser Vorstellung kommt dann die Ewigkeit nach unserer Zeit.
                Aber das wahre Finale ist nicht das Ende der Zeit, sondern die
                F&uuml;lle der Zeit. Ewigkeit ist nicht vo oder nach der Zeit. Im
                hier und heute fallen Zeit und Ewigkeit zusammen. Man kennt solche
                Momente, wenn man das Gef&uuml;hl hat, dass die Zeit stillsteht. Es
                sind meist gl&uuml;ckliche Momente. Also ereignet sich Gottes Zukunft
                nicht als das gro&szlig;e Abschlussfinale das in die Zeit hereinbricht,
                sondern vielmehr schon im Jetzt, immer wieder einmal. <br>
                Somit befindet sich der Christ schon in der Endzeit, in der letzten
                Zeit, der Zeit der Apokalypse, also der Aufdeckung. Dementsprechend
                wurde sinnrichtig in der alten Kirche nach Ostern nicht aus der
                Apostelgeschichte gelesen, sondern aus dem Buch der Apokalypse,
                dem letzten Buch. Also beschreibt dieses Buch, was jetzt geschieht.
                Und Liturgie ist dann n&auml;mlich die Hineinnahme der Sch&ouml;pfung in
                diese
                Endzeit. Und wenn in der Messe der Heilige Geist &uuml;ber die Gaben
                gerufen wird (Epiklese), dann wird Ewigkeit in der Zeit gegenw&auml;rtig.
                Am Ende der Zeit wird Gott dann vollst&auml;ndig in seiner Sch&ouml;pfung
                einwohnen.<br>
                Gegenentw&uuml;rfe zu diesem Zeitmodell w&auml;ren etwa: Die ewige Wiederkehr
                im Buddhismus, die zyklische Zeit, Zeit als Gott, Zeit als Illusion.
                Demgegen&uuml;ber steht das biblische christliche Zeitverst&auml;ndnis:
                Die absolute Einmaligkeit und Unwiederholbarkeit eines jeden
                Momentes (eph hapax - Struktur), die jeden Moment besonders macht.
                Es wird nichs vergessen oder belanglos, sondern alles ist f&uuml;r
                immer unver&auml;nderbar geschehen. </font></p>
            <p><br>
            </p>
            <p><br>
              <br>
            </p>
            <p><br>
            </p>
            <p><br>
            </p>
            <p><font face="Arial, Helvetica, sans-serif">
            </font>            </p>
            <p><font face="Arial, Helvetica, sans-serif"><br>
            </font>            </p>
            <p><br>
            </p>
            <p><font face="Arial, Helvetica, sans-serif">
            </font>            </p>
            <p><font face="Arial, Helvetica, sans-serif"><br>
            </font>            </p>
            <p><br>
            </p>
            <p>&nbsp;
            </p>
            <p><br>
            </p>
            <p><br>
            </p>
            <p><br>
            </p>
            <p><br>
            </p>
            <p><br>
            </p>
            <p><br>
            </p>
            <p><br>
            </p>
            <p><br>              
                <font face="Arial, Helvetica, sans-serif">              </font><font face="Arial, Helvetica, sans-serif">              <br>
                Feedback bitte an mit 
          dem Stichwort Liturgie-Lexikon an: redaktion@kath.de</font></p></td>
          <td class="L12" width="30%"> 
            <div align="right"> 
              <p><br>
                <br>
                <font size="2" face="Arial, Helvetica, sans-serif"><br>
                </font></p>
            </div>
          </td>
          <td background="boxright.gif" width="8"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif" colspan="2"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
<td width="8"><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
 </tr> 
</table> 
</td>
<!-- neuer Code Anfang -->
<td style='vertical-align:top;'> 
     <script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
/* Symbollexikon neu */
google_ad_slot = "9464926645";
google_ad_width = 120;
google_ad_height = 600;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
     <br>
</td>
<!-- neuer Code Ende -->

  </tr>
  <tr>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
</table>
</body>
</html>
