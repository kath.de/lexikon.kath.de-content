<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title>Rosenkranz</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="kaltefleiter.css" title="fonts">
</head>
<body bgcolor="#ffffff" leftmargin="6" topmargin="6" marginwidth="6" marginheight="6">
<table width="100%" border="0" cellspacing="0" cellpadding="6">
  <tr>
    <td width="100" align="left" valign="top" height="3"> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b><font face="Arial, Helvetica, sans-serif">Das 
            Liturgie Lexikon</font></b></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td> <?php include("logo.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><font face="Arial, Helvetica, sans-serif"><strong>Inhaltsverzeichnis 
            </strong></font></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10"> <?php include("az.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
    <td rowspan="2" valign="top">
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td background="boxtop.gif" colspan="2"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif" width="8"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2" colspan="2"> 
            <h1>Rosenkranz </h1>
          </td>
          <td background="boxtopright.gif" width="8"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif" colspan="2"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td width="8"><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif" width="8"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="L12" width="70%"> 
            <p><strong><font face="Arial, Helvetica, sans-serif"><br>
              <font color="#666600">auch: Pater-Nosterschnur</font></font></strong></p>
            <p><font color="#666600"><strong><font face="Arial, Helvetica, sans-serif">Inhalt: 
              Aufbau || Geschichte || Links</font></strong></font></p>
            <p><font face="Arial, Helvetica, sans-serif"><b>Als Rosenkranz wird 
              zun�chst die Gebetsschnur (siehe Bild rechts) bezeichnet, die aus 
              vielen Perlen in Kranzform und einem angeh�ngten Kreuz besteht. 
              Aber auch das damit verbundene Gebet wird Rosenkranz genannt. Es 
              ist ein eher meditatives Gebet, das in den sog. Geheimnissen die 
              wichtigsten Aussagen des Christentums einbezieht und entlang des 
              Lebensweges Jesu ausfaltet. Es wird aber auch als Bittgebet gesprochen, 
              in dem der Beter ein Anliegen mit dem Rosenkranz verbindet und auf 
              die F�rsprache Marias hofft.</b></font></p>
            <p><b><font face="Arial, Helvetica, sans-serif" color="#666600">Aufbau: 
              Eine "Kette" als Gebetsanleitung</font></b></p>
            <p><font face="Arial, Helvetica, sans-serif"><b>Dabei dient die Gebetsschnur 
              als Anleitung zum Beten: Begonnen wird bei dem Kreuz, indem der 
              Beter ein Kreuzzeichen macht mit den Worten: Ehre sei dem Vater 
              und dem Sohne und dem Heiligen Geist. Amen, gefolgt von einem Glaubensbekenntnis, 
              einem erneuten Kreuzzeichen und dem Vaterunser. Darauf folgen auf 
              der Gebetsschnurr drei kleine Perlen. F�r jede Perle wird ein "Gegr��et 
              seist du, Maria", das Maria-Gebet, gebetet. Die erste Perle steht 
              insbesondere f�r die Erinnerung an die erste der drei g�ttlichen 
              Tugenden: Glaube, w�hrend die zweite f�r Hoffnung und die dritte 
              f�r Liebe steht. Es schlie�t sich eine gr��ere Perle an, bei der 
              immer das Vaterunser gebetet wird. Damit ist die Einleitung beendet 
              und es beginnt der eigentliche (Rosen)Kranz, der aus f�nf Abschnitten 
              besteht. Jeweils 10 kleine Perlen werden von einer gro�en umrahmt. 
              Die zehn kleinen Perlen entsprechen je einem "Gegr��et seist du 
              Maria", am Ende m�ndet es in eine gro�e Perle, die einem Vaterunser 
              entspricht. Jede der f�nf Zehnergruppen ist unter ein besonderes 
              Thema gestellt, ein Glaubensgeheimnis, das in die Maria-Gebete eingef�gt 
              wird. Diese Einf�gung geschieht wie folgt: Jedes Gegr��et seist 
              du Maria besteht aus folgendem Text: </b></font></p>
            <p><font face="Arial, Helvetica, sans-serif"><b>Gegr��et seist du 
              Maria, voll der Gnaden, der Herr ist mit dir. Du bist gebenedeit 
              unter den Frauen und gebenedeit ist die Frucht deines Leibes Jesus. 
              - der (f�r uns Blut geschwitzt hat - oder ein anderer Einschub). 
              Es folgt dann der immer gleiche Schluss des Gebetes: Heilige Maria, 
              Mutter Gottes, bitte f�r uns S�nder, jetzt und in der Stunde unseres 
              Todes. Amen. Diese Einsch�be werden "Geheimnisse" oder auch "Ges�tze" 
              genannt und zu F�nfergruppen zusammengestellt. Diese f�nf Zehnergruppen 
              ergeben ein Rosenkranzgebet, sprich einen Rosenkranz (zus�tzlich 
              der Einleitung). Der Rosenkranz selber kann auch unter ein Thema 
              gestellt sein, so dass es mehrere Rosenkr�nze gibt: Es gibt den 
              schmerzhaften, den glorreichen, den freudenreichen, den barmherzigen 
              und den der Wunden und seit 2002 auch den lichtreichen. Dabei sind 
              die Themen sehr eng am Leben Jesu orientiert und greifen thematisch 
              Aspekte daraus hervor. Auch wenn es also verschiedene Rosenkr�nze 
              gibt, was die Gebetsform angeht, gibt es aber nur einen Rosenkranz 
              im engeren Sinne der (siehe Bild rechts) vorliegenden Gebetsschnurr. 
              Sogar die einzelnen Rosenkr�nze, wie der der Wunden, k�nnen sich 
              nochmals unterscheiden je nachdem, wer ihn betet: So gibt es beispielsweise 
              den der Salesianerinnen, den der Passionisten und den des hlg. Alfons 
              Maria von Liguori. </b></font></p>
            <p><font face="Arial, Helvetica, sans-serif"><b><font color="#666600">Die 
              einzelnen Rosenkr�nze: </font> </b></font></p>
            <p><font face="Arial, Helvetica, sans-serif"><b><font color="#666600">Die 
              freudenreichen Geheimnisse</font> (weihnachtliche Verk�ndigung): 
              - den du, o Jungfrau, vom Heiligen Geist empfangen hast; - den du, 
              o Jungfrau, zu Elisabeth getragen hast; - den du, o Jungfrau, (in 
              Betlehem) geboren hast: - den du, o Jungfrau, im Tempel aufgeopfert 
              hast; - den du, o Jungfrau, im Tempel wiedergefunden hast. </b></font></p>
            <p><font face="Arial, Helvetica, sans-serif"><b><font color="#666600">Die 
              f�nf schmerzhaften Geheimnisse</font> (Passion des Herrn): - der 
              f�r uns Blut geschwitzt hat; - der f�r uns gegei�elt worden ist; 
              - der f�r uns mit Dornen gekr�nt worden ist; - der f�r uns das schwere 
              Kreuz getragen hat; - der f�r uns gekreuzigt worden ist. </b></font></p>
            <p><font face="Arial, Helvetica, sans-serif"><b><font color="#666600">Die 
              f�nf glorreichen Geheimnisse </font>(�sterliche Geheimnisse): - 
              der von den Toten auferstanden ist; - der in den Himmel aufgefahren 
              ist; - der uns den Heiligen Geist gesandt hat; - der dich, o Jungfrau, 
              in den Himmel aufgenommen hat; - der dich, o Jungfrau, im Himmel 
              gekr�nt hat. </b></font></p>
            <p><font face="Arial, Helvetica, sans-serif"><b>Im Oktober 2002 hat 
              Papst Johannes Paul II den Rosenkranz um f�nf <font color="#666600">"lichtreiche 
              Geheimnisse"</font> erweitert, die Ereignisse aus dem Leben Jesu 
              meditieren: Jesus, - der von Johannes getauft worden ist, - der 
              sich bei der Hochzeit in Kana offenbart hat; - der uns das Reich 
              Gottes verk�ndet hat; - der auf dem Berg verkl�rt worden ist; - 
              der uns die Eucharistie geschenkt hat. </b></font></p>
            <p><font face="Arial, Helvetica, sans-serif"><b><font color="#666600">Geschichte: 
              Von 150 zu 50 Perlen: </font></b></font></p>
            <p><font face="Arial, Helvetica, sans-serif"><b>Im 15. Jahrhundert 
              erhielt der Rosenkranz seine heutige Form. Das widerholende Beten 
              mit Steinen oder einer Z�hlschnur war schon bei den Erimiten der 
              fr�hen Kirche �blich. Noch im Mittelalter wurden bis zum 12. Jahrhundert 
              keine "Gegr��et seist du Maria" gebetet, sondern das Vaterunser, 
              weshalb auch der Name Pater-Noster-Kette in Umlauf ist. </b></font></p>
            <p><font face="Arial, Helvetica, sans-serif"><b><font color="#666600">Links: 
              </font></b></font></p>
            <p><font face="Arial, Helvetica, sans-serif"><b>Den genauen Aufbau 
              und Inhalt der einzelnen Rosenkr�nze finden Sie hier: <a href="http://www.autobahnkirche.de/spirit-container/gebete/rosenkranz/rosenkranz-inhalt.html">http://www.autobahnkirche.de/spirit-container/gebete/rosenkranz/rosenkranz-inhalt.html</a><br>
              Es handelt sich bei dem obenstehenden Link um ein Angebot der <a href="http//www.kgi.org">Katholischen 
              Glaubensinformation (KGI)</a>, einer offiziellen Stelle der <a href="http://www.dbk.de">Deutschen 
              Bischofskonferenz </a></b></font></p>
            <p><font face="Arial, Helvetica, sans-serif"><b><font size="2"><br>
              </font></b><font size="2">&copy; J&uuml;rgen Pelzer<b><br>
              </b></font></font><font size="2">F<font face="Arial, Helvetica, sans-serif">eedback 
              bitte an mit dem Stichwort Liturgie-Lexikon an: redaktion@kath.de</font></font></p>
            </td>
          <td class="L12" width="30%"> 
            <div align="right"> 
              <p align="right"><br>
                <br>
                <font size="2" face="Arial, Helvetica, sans-serif"><br>
                <img src="photocase219413723.jpg" width="370" height="285"><br>
                Ein Rosenkranz</font></p>
              <p>&nbsp;</p>
              <p><img src="DSC05332.jpg" width="236" height="305"><br>
                Weltjugentag 2005 (K&ouml;ln)<br>
                Ein Pilger betet den Rosenkranz</p>
            </div>
          </td>
          <td background="boxright.gif" width="8"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif" colspan="2"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
<td width="8"><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
 </tr> 
</table> 
</td>
<!-- neuer Code Anfang -->
<td style='vertical-align:top;'> 
     <script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
/* Symbollexikon neu */
google_ad_slot = "9464926645";
google_ad_width = 120;
google_ad_height = 600;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
     <br>
</td>
<!-- neuer Code Ende -->

  </tr>
  <tr>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
</table>
</body>
</html>
