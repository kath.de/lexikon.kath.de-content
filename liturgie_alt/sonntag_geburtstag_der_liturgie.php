<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title>Sonntag - Geburtstag der Liturgie</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="kaltefleiter.css" title="fonts">
</head>
<body bgcolor="#ffffff" leftmargin="6" topmargin="6" marginwidth="6" marginheight="6">
<table width="100%" border="0" cellspacing="0" cellpadding="6">
  <tr>
    <td width="100" align="left" valign="top" height="3"> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b><font face="Arial, Helvetica, sans-serif">Das 
            Liturgie Lexikon</font></b></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td> <?php include("logo.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><font face="Arial, Helvetica, sans-serif"><strong>Inhaltsverzeichnis 
            </strong></font></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10"> <?php include("az.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
    <td rowspan="2" valign="top">
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td background="boxtop.gif" colspan="2"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif" width="8"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2" colspan="2"> 
            <h1><font face="Arial, Helvetica, sans-serif"><strong>Sonntag -
            Geburtstag der Liturgie</strong></font></h1>
          </td>
          <td background="boxtopright.gif" width="8"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif" colspan="2"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td width="8"><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif" width="8"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="L12" width="70%"> 
            <p><br>
            <font face="Arial, Helvetica, sans-serif"><strong>&Uuml;ber die tieferliegende Bedeutung des Sonntags</strong></font></p>
            <p><font face="Arial, Helvetica, sans-serif">&#8222;Am siebten Tag sollst du ruhen&#8220;. Diesem biblischen
              Gebot verdanken viele Berufsgruppen auch heute noch den freien
              Sonntag. Hintergrund ist die in Buch Genesis erz&auml;hlte Version
              der Sch&ouml;pfung. Gott arbeitet 6 Tage lang, erschuf die Welt,
              das Weltall, Tiere und Menschen, kurzum den gesamten Kosmos. Am
              siebten Tag jedoch ruhte er. Um sich daran zu erinnern lassen die
              Christen am Sonntag die Arbeit ruhen. Doch was tat Gott an seinem
              arbeitsfreien Tag? Etwa Nichts? Die Antwort mag erstaunen: Er feierte.
              Laut j&uuml;discher Auffassung ist erst mit diesem Feiern die Sch&ouml;pfung
              beendet, nicht schon am 6. Tag, nachdem die handwerkliche Arbeit
              erledigt ist. Denn Gott will den Menschen miteinbeziehen, indem
              er ihm die M&ouml;glichkeit gibt an dem g&ouml;ttlichen Feiern
              des 7. Tages teilzuhaben &#8211; durch die Liturgie. Eben dieses
              Feiern Gottes der Sch&ouml;pfung ist die Geburtsstunde des Kultes,
              also der Liturgie, wie die Christen sie feiern. Die Kulthandlungen
              der Liturgie erinnern an den siebten Sch&ouml;pfungstag, als Gott
              feierte. Mehr noch: Sie erm&ouml;glichen dem Menschen die Teilnahme
              an dem Sch&ouml;pfungswerk Gottes. Und die Sch&ouml;pfung ist erst
              dadurch vollendet, dass Gott sich den Kult, das Feiern in ritueller
              Form, ausdachte, um es den Menschen zu erm&ouml;glichen an der
              Feier und Freude Gottes teilzuhaben. <br>
              Dieses Verst&auml;ndnis des sonnt&auml;glichen Gottesdienstes findet
              sich wieder in der Rede vom Gottesdienst als Feier der Messe.<br>
            </font><font face="Arial, Helvetica, sans-serif"><br>
              Der Ort, an dem Gott den Kult stiftete war die W&uuml;ste Sinai.
                Das Volk der Israeliten unter der F&uuml;hrung des Mose war gerade
                aus &Auml;gypten aufgebrochen, um in einer jahrzehnte dauernden
                Wanderung in das Land zu ziehen, dass Gott Ihnen zeigen sollte.
                Gott offenbarte sich dem Moses an dem Berg Sinai und gab ihm
              die 10 Gebote und Anweisungen zum Kult</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Neugierig geworden?
                Sie m&ouml;chten an die Quelle?<br>
  Hier finden Sie die entsprechende Stelle in der Bibel (Einheits&uuml;bersetzung):<br>
              Ex, 20,9f. (Buch Exodus, Kapitel 20, Verse 9 und 10)</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Das Motiv wird auch an folgenden Stellen aufgenommen:<br>
              Ex, 24,16f. (Buch Exodus, Kapitel 24, Vers 16)<br>
              Ex, 31,16f. (Buch Exodus, Kapitel 31, Vers 16)<br>
              Ex, 35,1-3. (Buch Exodus, Kapitel 35, Vers 1,2,3,)<br>
              <br>
              <strong>Jede Woche Ostern feiern</strong><br>
              <br>
              Im neuen Testament begegnet der Sonntag mehrmals als der erste
              Tag der Woche, in der Apokalypse sogar als der Tag des Herrn. Die
              Juden hingegen feierten den Sabbat, den Samstag, als Ihren heiligen
              Tag. Dies machten wohl auch die ersten Christen noch so.<br>
              Papst Leo der Gro&szlig;e (440-461) erkl&auml;rt, dass der Sonntag
              bereits am Samstagabend begonnen werden soll, da bereits mit dem
              Abend des Sabbats der Tag, also Sonntag, der Tag der Auferstehung
              folgt. Dementsprechend fanden die &auml;ltesten Zusammenk&uuml;nfte
              der Christen in der Samstagnacht statt. Bereits im 3./4. Jahrhundert
              ist die Eucharistiefeier schon auf den Sonntagmorgen gelegt. Am
              Samstagabend gibt es eine Agape (Liebe) Feier. <br>
              Entscheiden f&uuml;r die Entwicklung ist das Jahr 321, als Kaiser
              Konstantin den Sonntag (dies solis) zum Staatsfeiertag erkl&auml;rt.
              Es ist ein allgemeiner Ruhetag mit der Verpflichtung an einer gottesdienstlichen
              Versammlung (der Heiden oder Christen) teilzunehmen. <br>
            Tertullian sagt: &#8222;Am Sonntag sind wir fr&ouml;hlich.&#8220; </font></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Die Sabbatruhe
                  und die Neusch&ouml;pfung</strong><br>
                  <br>
  Der Sabbat der Juden l&auml;sst einen zur&uuml;ckschauen auf das
              Sch&ouml;pfungswerk Gottes und die menschliche Arbeit der Woche.
              Ruhe kennzeichnet diesen Abend. Nach der Arbeit der Woche kommt
              man zur Ruhe, wie auch Gott am siebten Tag der Sch&ouml;pfung zur
              Ruhe kam (vgl. Gen 1,1-7). Man sp&uuml;rt an diesem Abend etwas
              von der Vollendung der Sch&ouml;pfung. Der christliche Sonntag
              betont besonders den Aspekt der neuen Sch&ouml;pfung, begonnen
              in der Auferstehung von den Toten und hat von daher eher dynamischen
              Charakter, es l&auml;sst teilhaben an der Kraft der Neusch&ouml;pfung.</font><br>
            </p>
            <p><font face="Arial, Helvetica, sans-serif">              <br>
              </font><font face="Arial, Helvetica, sans-serif">              <br>
              Feedback bitte an mit 
            dem Stichwort Liturgie-Lexikon an: redaktion@kath.de</font></p></td>
          <td class="L12" width="30%"> 
            <div align="right"> 
              <p><br>
                <br>
                <font size="2" face="Arial, Helvetica, sans-serif"><br>
                </font></p>
            </div>
          </td>
          <td background="boxright.gif" width="8"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif" colspan="2"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
<td width="8"><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
 </tr> 
</table> 
</td>
<!-- neuer Code Anfang -->
<td style='vertical-align:top;'> 
     <script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
/* Symbollexikon neu */
google_ad_slot = "9464926645";
google_ad_width = 120;
google_ad_height = 600;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
     <br>
</td>
<!-- neuer Code Ende -->

  </tr>
  <tr>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
</table>
</body>
</html>
