<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<meta name="title" content="Liturgie">
<meta name="author" content="Redaktion kath.de">
<meta name="publisher" content="kath.de">
<meta name="copyright" content="kath.de">
<meta name="description" content="">
<meta name="abstract" content="Lexikon �ber die Liturgie der katholischen Kirche">
<meta http-equiv="content-language" content="de">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="date" content="2006-00-01">
<meta name="robots" content="index,follow">
<meta name="revisit-after" content="10 days">
<meta name="revisit" content="after 10 days">
<meta name="DC.Title" content="Liturgie">
<meta name="DC.Creator" content="Redaktion kath.de">
<meta name="DC.Contributor" content="J�rgen Pelzer">
<meta name="DC.Rights" content="kath.de">
<meta name="DC.Publisher" content="kath.de">
<meta name="DC.Date" content="2006-00-01">
<meta name="DC.Description" content="Lexikon �ber die Liturgie der katholischen Kirche">
<meta name="DC.Language" content="de">
<meta name="DC.Type" content="Text">
<meta name="DC.Format" content="text/html">
<meta name="DC.Identifier" content="http://www.kath.de/lexikon/liturgie/">
<meta name="keywords" lang="de" content="altkirchliche Ordnung, Sonntage nach Epiphanie, Sonntage nach Trinitatis">
<title>Leseordnung der evangelischen Kirche</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="kaltefleiter.css" title="fonts">
</head>
<body bgcolor="#ffffff" leftmargin="6" topmargin="6" marginwidth="6" marginheight="6">
<table width="100%" border="0" cellspacing="0" cellpadding="6">
  <tr>
    <td width="100" align="left" valign="top" height="3"> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b><font face="Arial, Helvetica, sans-serif">Das 
            Liturgie Lexikon</font></b></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td> <?php include("logo.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><font face="Arial, Helvetica, sans-serif"><strong>Inhaltsverzeichnis 
            </strong></font></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10"> <?php include("az.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
    <td rowspan="2" valign="top">
      <table width="100%" height="189" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td background="boxtop.gif" colspan="2"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="9"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif" width="8"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2" colspan="2"> 
            <h1><font face="Arial, Helvetica, sans-serif">Leseordnung der evangelischen
            Kirche f&uuml;r die Sonntage</font></h1>
          </td>
          <td background="boxtopright.gif" width="9"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif" colspan="2"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td width="9"><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif" width="8"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="L12" width="506"> 
            <p><font face="Arial, Helvetica, sans-serif"><strong>Die altkirchliche
            Ordnung und  6 Zyklen f&uuml;r die Predigt</strong></font></p>
            <p><font face="Arial, Helvetica, sans-serif"><br>
              In den Kirchen gibt es alte Traditionen, welche Texte aus der
                      Bibel gelesen werden. Das ist vor allem f&uuml;r die Sonntage wichtig,
                  weil hier mehr Gl&auml;ubige sich versammeln und die Chance haben,
                  Texte aus der Bibel zu h&ouml;ren. W&auml;hrend die katholische
                  Kirche ihre <a href="leseordnung_katholische_kirche.php">Leseordnung </a>sowohl
                  nach dem Konzil von Trient (1545 &#8211; 63)
                  wie nach dem II. Vatikanischen Konzil ge&auml;ndert hat, sind
                  die Evangelischen Landeskirchen in Deutschland bei der altkirchliche
                  Ordnung geblieben. Sie hat auch die Sonntage der sog. Vorfastenzeit,
                  Septuaginta, Sexuaginta, Quinquaginta, beibhalten, die die
                  katholsiche Kirche abgeschafft hat.<br>
              Die evangelische Kirche hat weiter die alte Z&auml;hlweise f&uuml;r
                die Sonntage beibehalten, die nicht in die Festzeiten Advent-Weihnachten
                bzw. Fastenzeit-Osterzeit fallen. Sie z&auml;hlt wie fr&uuml;her
                diese als &#8222;Sonntage nach Epiphanie&#8220; und &#8222;Sonntage
                nach Dreifaltigkeit&#8220; oder &#8222;Trinitatis&#8220; (das
                ist der Sonntag nach Pfingsten). <br>
              Im evangelischen Gottesdienst werden an den Sonn- und Feiertagen
                mindestens zwei Lesungen vorgetragen, auch drei sind m&ouml;glich,
                immer wird ein Text aus einem der vier Evangelien gelesen. Die
                Leseordnung ist wird nicht wie in der katholischen Kirche in einem
                Turnus von drei Jahren gewechselt, sondern bleibt gleich. Die evangelische
                Liturgie kennt jedoch neben den Lesungen noch eigene Texte, die
                der Predigt zugrunde gelegt werden. Hier gibt es sechs Zyklen.
                Die Reihe 1 orientiert sich an den Evangelientexten der Sonntage,
                die Reihe 2 an den Lesungen, die an den Sonntagen aus den neutestamentlichen
                Briefen entnommen werden. Die Reihen 3 und 5 legen weitere Texte
                aus den Briefen zugrunde, die Reihen 4 und 6 w&auml;hlen aus weiteren
                Evangelientexten aus. Es werden also der Predigt mehr Texte zugrunde
                gelegt als die Texte, die f&uuml;r die Lesungen vorgesehen sind.
                In den Reihen 3-6 findet sich jeden 4. Sonntag ein Predigttext
                aus dem Alten Testament.<br>
              Eckhard Bieger</font> </p>            <p>&nbsp;</p>
            <p><font size="2" face="Arial, Helvetica, sans-serif">&copy; <a href="http://www.kath.de/">www.kath.de</a></font></p></td>
          <td class="L12" width="11">            <div align="right"><br>
            <table border="1">
              <tr>
                <td>&nbsp;</td>
              </tr>
            </table>
              </div>
          </td>
          <td background="boxright.gif" width="9"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif" colspan="2"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td width="9"><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
</table>
</body>
</html>
