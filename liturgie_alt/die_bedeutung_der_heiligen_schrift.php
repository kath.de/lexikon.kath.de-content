<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title>Die Bedeutung der Heiligen Schrift in der Liturgie</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="kaltefleiter.css" title="fonts">
</head>
<body bgcolor="#ffffff" leftmargin="6" topmargin="6" marginwidth="6" marginheight="6">
<table width="100%" border="0" cellspacing="0" cellpadding="6">
  <tr>
    <td width="100" align="left" valign="top" height="3"> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b><font face="Arial, Helvetica, sans-serif">Das 
            Liturgie Lexikon</font></b></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td> <?php include("logo.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><font face="Arial, Helvetica, sans-serif"><strong>Inhaltsverzeichnis 
            </strong></font></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10"> <?php include("az.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
    <td rowspan="2" valign="top">
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td background="boxtop.gif" colspan="2"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8" height="37" background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2" colspan="2"> 
            <h1><font face="Arial, Helvetica, sans-serif">                <font face="Arial, Helvetica, sans-serif">Die
                  Bedeutung der Heiligen Schrift in der Liturgie</font></font></h1>
          </td>
          <td background="boxtopright.gif" width="8"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif" colspan="2"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td width="8"><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif" width="8"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="L12" width="70%"> 
            <p>&nbsp;</p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Es war einmal ?</strong><br>
              <br>
  Das gro&szlig;e Sakrament selber ist die Heilsgeschichte. Die heilige
              Schrift als Offenbarung Gottes. Dieses Wort ist Gott selber. Die
              Bibel wird allzu schnell als Buch gesehen. Sie ist aber die Epiphanie
              des Wortes Gottes als Verweis auf Jesus. Deshalb kann man auch
              in der Liturgie Eucharistie und Wortgottesdienst nicht gegeneinander
              ausspielen. Das Wort wird als Leib und Blut Christi bei den Kirchenv&auml;tern
              dargestellt. (In der Liturgiekonstitution des II Vatiknaums &#8222;die
              verbum&#8220; wurde das gegen den Willen des damaligen Papstes
              Paul VI.. Dort hei&szlig;t es nun: Die Kirche verehrt das Wort
              wie den Leib Christi). Die heilige Schrift hat drei Dimensionen:
              Die prophetische: Das AT verweist auf das NT. Vergegenw&auml;rtigend:
              Das Evangelium selber ist die Erf&uuml;llung all dessen. In der
              Person Jesu ist alles vollendet. 3. Eschatologisch: Erst im Eschaton
              zeigt sich die letzte Bedeutung der Schrift. Der Schatten ist im
              Gesetz, der in Jesu die Wahrheit im Eschaton.</font></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Vergegenw&auml;rtigung
                als Grundvollzug</strong><br>
                <br>
  Wie ist das Vergegenw&auml;rtigen zu verstehen? Ambrosius: &#8222;Wenn
              wir das Evangelium h&ouml;ren, dann vergegenw&auml;rtigt es sich&#8220; Nicht
              weil es einen Blinden irgendwann einmal gab lesen wir das Evangelium,
              sondern wir sind blind und Jesus heilt uns heute. Es geht nicht
              darum zu sehen und h&ouml;ren, sondern: Damals hat Jesus den Blinden
              geheilt, heute heilt er uns. Es geht darum, dass er das heute an
              mir tut. Ein zweiter Aspekt, den Ambrosius hineinbringt: Das er
              es heute an mir tut ist der Antitypos: Die Erf&uuml;llung des Blinden.
              Antitypos ist immer der H&ouml;here Typos, nicht im Sinne von anti
              = &#8222;gegen&#8220;. <br>
              So ist auch der Thomassonntag das Antipascha. Die Erf&uuml;llung
              von Ostern. Der Mensch kommt zur Erf&uuml;llung, die Auferstehung
              kommt zur Erf&uuml;llung. Der Mensch wird mit in die Auferstehung
              hineingekommen. Genauso wird auch der H&ouml;rer der Schrift mit
              hineingenommen in Gottes Heilshandeln an ihm.</font><br>
            </p>
            <p><br>
            </p>
            <p><br>
            </p>
            <p><br>              
                <font face="Arial, Helvetica, sans-serif">              </font><font face="Arial, Helvetica, sans-serif">              <br>
                Feedback bitte an mit 
          dem Stichwort Liturgie-Lexikon an: redaktion@kath.de</font></p></td>
          <td class="L12" width="30%"> 
            <div align="right"> 
              <p><br>
                <br>
                <font size="2" face="Arial, Helvetica, sans-serif"><br>
                </font></p>
            </div>
          </td>
          <td background="boxright.gif" width="8"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif" colspan="2"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
<td width="8"><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
 </tr> 
</table> 
</td>
<!-- neuer Code Anfang -->
<td style='vertical-align:top;'> 
     <script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
/* Symbollexikon neu */
google_ad_slot = "9464926645";
google_ad_width = 120;
google_ad_height = 600;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
     <br>
</td>
<!-- neuer Code Ende -->

  </tr>
  <tr>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
</table>
</body>
</html>
