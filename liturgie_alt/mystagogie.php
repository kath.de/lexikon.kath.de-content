<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<meta name="title" content="Liturgie">
<meta name="author" content="Redaktion kath.de">
<meta name="publisher" content="kath.de">
<meta name="copyright" content="kath.de">
<meta name="description" content="">
<meta name="abstract" content="Lexikon �ber die Liturgie der katholischen Kirche">
<meta http-equiv="content-language" content="de">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="date" content="2006-00-01">
<meta name="robots" content="index,follow">
<meta name="revisit-after" content="10 days">
<meta name="revisit" content="after 10 days">
<meta name="DC.Title" content="Liturgie">
<meta name="DC.Creator" content="Redaktion kath.de">
<meta name="DC.Contributor" content="J�rgen Pelzer">
<meta name="DC.Rights" content="kath.de">
<meta name="DC.Publisher" content="kath.de">
<meta name="DC.Date" content="2006-00-01">
<meta name="DC.Description" content="Lexikon �ber die Liturgie der katholischen Kirche">
<meta name="DC.Language" content="de">
<meta name="DC.Type" content="Text">
<meta name="DC.Format" content="text/html">
<meta name="DC.Identifier" content="http://www.kath.de/lexikon/liturgie/">
<meta name="keywords" lang="de" content="Mysterienkulte, Sakramente, r�mischer Staatskult, Katechesen, Taufe">
<title>Mystagogie</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="kaltefleiter.css" title="fonts">
</head>
<body bgcolor="#ffffff" leftmargin="6" topmargin="6" marginwidth="6" marginheight="6">
<table width="100%" border="0" cellspacing="0" cellpadding="6">
  <tr>
    <td width="100" align="left" valign="top" height="3"> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b><font face="Arial, Helvetica, sans-serif">Das 
            Liturgie Lexikon</font></b></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td> <?php include("logo.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><font face="Arial, Helvetica, sans-serif"><strong>Inhaltsverzeichnis 
            </strong></font></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10"> <?php include("az.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
    <td rowspan="2" valign="top">
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td background="boxtop.gif" colspan="2"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="9"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif" width="8"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2" colspan="2"> 
            <h1><font face="Arial, Helvetica, sans-serif"> Mystagogie</font></h1>
          </td>
          <td background="boxtopright.gif" width="9"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif" colspan="2"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td width="9"><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif" width="8"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="L12" width="516"> 
            <p><strong><font face="Arial, Helvetica, sans-serif">              Mysterienkulte und Sakramente</font></strong></p>
            <p><font face="Arial, Helvetica, sans-serif">Als die Christen begannen,
                au&szlig;erhalb Pal&auml;stinas zu
              missionieren, wurden sie mit einer Vielzahl religi&ouml;ser Str&ouml;mungen
              konfrontiert. Der r&ouml;mische Staat hatte einen Staatskult, der
              sich in Opfern f&uuml;r die Siegesg&ouml;ttin Nike und sp&auml;ter
              f&uuml;r den r&ouml;mischen Kaiser ausdr&uuml;ckte. Mit diesem
              Kult war eine Art Staatsr&auml;son verbunden. Wer sich dem Opfern
              verweigerte, zeigte sich dem Staat gegen&uuml;ber illoyal. Da die
              Christen nur einen Gott verehrten, mu&szlig;ten sie mit diesem
              Staatskult in Konflikt geraten. Die Situation war nicht so verschieden
              von der in j&uuml;ngerer Vergangenheit wie im Nationalsozialismus
              oder Kommunismus. Den r&ouml;mischen Staatskult &uuml;berwanden
              die Christen, Kaiser Konstantin f&uuml;hrte das Christentum als
              Staatsreligion ein, der Sonntag wurde zum Wochenfeiertag, anstelle
              des Kaiserbildnisses wurde Christus als Weltenherrscher in die
              Apsis der Basiliken gemalt. <br>
              Da man im r&ouml;mischen Reich mehreren Gottheiten opfern konnte,
              ohne da&szlig; der Staat sich irritiert f&uuml;hlte, gab es noch
              andere Konkurrenten, mit denen das Christentum zurecht kommen mu&szlig;te.
              Den sogenannten Mysterienkulten konnte das Christentum die eigenen
              Geheimnisse entgegensetzen. Wie der Dionysos-, der Isis- und Mithras-
              Kult hatte das Christentum auch Riten f&uuml;r die Aufnahme, Initiationsriten.
              Da die Erwachsenen mit der Taufe gefirmt wurden und im selben Gottesdienst,
              meist in der Osternacht, die Eucharistie empfingen, mu&szlig;ten
              den T&auml;uflingen diese Sakramente erschlossen werden. Mystagogie
              als Einf&uuml;hrung in das Geheimnis bezieht sich darauf, die theologische
              Bedeutung der Zeichen zu erkennen, des Wassers, des &Ouml;ls, von
              Brot und Wein. Mystagogie beinhaltet weiter die Erkl&auml;rung
            der Aufnahmeriten.</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Vom Jerusalemer Bischof
                Cyrill sind die Katechesen f&uuml;r die
              Vorbereitung auf die Taufe wie auch die mystagogischen Katechesen
              erhalten. Cyrill starb 386 und war wohl schon 348 Bischof. Die
              Einf&uuml;hrung in den Glauben und die Vorbereitung auf die Taufe
              war damals Aufgabe des Bischofs. <br>
              Auf die Taufe wurden die Bewerber nicht durch Erkl&auml;rung des
              Ritus, sondern durch Vortr&auml;ge &uuml;ber die einzelnen Artikel
              des Glaubens-bekenntnisses vorbereitet.<br>
              Interessant ist, da&szlig; die mystagogischen Katechesen erst in
              der Osterwoche, das hei&szlig;t nach der Aufnahme in die Kirche,
              gehalten wurden. Die Erkl&auml;rung der Mysterien, der christlichen
              Sakramente, folgte erst, wenn die Taufbewerber den Ritus und die
              ganze Liturgie miterlebt hatten. Der Bischof deutet den Neugetauften
              die Riten. Er beginnt mit dem Abschw&ouml;ren gegen&uuml;ber dem
              Satan, der Hinwendung zu Christus, der Salbung. Die Taufe wurde
              damals nicht blo&szlig; durch &Uuml;bergie&szlig;en des Kopfes
              gespendet, sondern indem der T&auml;ufling im Taufbecken ganz untertauchte &#8211; Zeichen
              f&uuml;r das Mit-Sterben mit Christus &#8211; und wieder auftauchte,
              Zeichen f&uuml;r das mit Christus in ein neues Leben Auferstehen. <br>
              Die Weihe des Salb&ouml;ls vergleicht Cyrill mit der Herabrufung
              des Hl. Geistes &uuml;ber Brot und Wein in der Eucharistiefeier.
              Das geweihte &Ouml;l ist geheiligt und bewirkt die Gaben des Geistes.
              Schlie&szlig;lich findet sich eine ausf&uuml;hrliche Erkl&auml;rung
              der <a href="eucharistie_messe_abendmahl.php">Eucharistiefeier</a>.</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Die Grundidee der Mystagogie
                geht davon aus, da&szlig; bereits
              geschehen ist, was dann erkl&auml;rt wird. Dieser Gedanke wurde
              in den letzten Jahrzehnten neu aufgegriffen.<br>
              Karl Rahner spricht von einer mystagogischen Katechese. In seiner
              Theologie zeigt er, da&szlig; der Mensch schon l&auml;ngst von
              Gott ber&uuml;hrt ist, vielleicht schon in einem unbewu&szlig;ten
              Gespr&auml;ch mit Gott steht, wenn er nach dem Sinn seiner Existenz
              fragt. Diese Realit&auml;t dem Menschen aufzuzeigen, ist mystagogische
              Katechese.<br>
              Die Katholische Fernseharbeit beim ZDF hat f&uuml;r die Gottesdienst&uuml;bertragungen
              die &#8222;<a href="http://www.kath.de/lexikon/medien_oeffentlichkeit/mystagogische_Bildregie.php">mystagogische
              Bildregie</a>&#8220; entwickelt. Sie verzichtet
              auf eine Kommentierung, die vorher schon erkl&auml;rt, was die
              n&auml;chste liturgische Handlung bedeutet, sondern erschlie&szlig;t
              den Sinn einzelner Riten, Handlungen und Gegenst&auml;nde, indem
              sie diese in Beziehung zu Bauelementen, z.B. dem Gew&ouml;lbe,
              setzt, oder bildliche Darstellungen wie das Kreuz mit einem Gesang
              verbindet. Die Kamera und die Bildregie leisten die mystagogische
              Erschlie&szlig;ung einzelner Abschnitte des Gottesdienstes. Mystagogische
              Bildregie ist deshalb m&ouml;glich, weil die Kirchenr&auml;ume
              mit einer theologischen Konzeption gebaut sind und Symbole im Kirchenraum
              auf die Feier der Eucharistie und verschiedenen anderen Gottesdienstformen
              bezogen sind.</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Die mystagogische Erschlie&szlig;ung eines Sakramentes beruht
              nicht auf einem Geheimwissen des Bischofs, vielmehr erkl&auml;rt
              er einzelne Riten durch Verweis auf Stellen im Neuen wie im Alten
              Testament.<br>
              Die Erkl&auml;rung Cyrills &uuml;ber das in der Firmung verwendete &Ouml;l
              hat sechs Verweise auf biblische Texte:</font></p>
            <p><font face="Arial, Helvetica, sans-serif">&#8222;
              Zuerst wurdet ihr auf die Stirn gesalbt, um von der Schande, welche
              der erste s&uuml;ndige Mensch &uuml;berall hin trug, befreit zu
              werden und um die &#8222;Herrlichkeit des Herrn mit unverh&uuml;lltem
              Antlitz widerzuspiegeln&#8220; (2 Kor 3,18). Darauf wurdet ihr
              an den Ohren gesalbt, damit ihr Ohren erhieltet, welche die g&ouml;ttlichen
              Geheimnisse h&ouml;ren, Ohren, von denen Jesaja sagte: &#8222;Und
              der Herr gab mir ein Ohr zu h&ouml;ren&#8220; (Jes 50,4), und der
              Herr Jesus Christus in den Evangelien sprach: &#8222;Wer Ohren
              hat zu h&ouml;ren, der h&ouml;re&#8220; (Mt 11,15 u.a.) Sodann
              wurdet ihr an der Nase gesalbt, damit ihr nach dem Empfang der
              g&ouml;ttlichen Salbe sagt: &#8222;Christi Wohlgeruch sind wir
              f&uuml;r Gott unter den Geretteten.&#8220; (2 Kor 2,15). Hierauf
              wurdet ihr auf der Brust gesalbt, damit ihr &#8222;angetan mit
              dem Panzer der Gerechtigkeit&#8220;, &#8222;gegen die Schliche
              des Teufels fest steht.&#8220; (Eph 6, 11.14). Gleichwie Jesus
              nach der Taufe und nach der Herabkunft des Hl. Geistes hinausging
              in die W&uuml;ste und den Widersacher bek&auml;mpfte, so sollt
              ihr nach der heiligen Taufe und nach der geistigen Salbung, angetan
              mit der ganzen Waffenr&uuml;stung des Hl. Geistes, euch der feindlichen
              Macht entgegenstellen und sie bek&auml;mpfen und sagen: &#8222;Alles
              vermag ich in Christus, der mich st&auml;rkt.&#8220; (Phil 4,13).</font><br>
              <font size="2" face="Arial, Helvetica, sans-serif">Nr. 4 aus der Mystagogischen
              Katechese des Cyrill &uuml;ber die Firmung</font><br>
            </p>
            <p><font face="Arial, Helvetica, sans-serif">Hinweise zu den aktuellen <a href="http://www.fernsehgottesdienst.de/">Gottesdienst&uuml;bertragungen im ZDF</a></font><a href="http://www.fernsehgottesdienst.de/"> </a></p>
            <p><font size="2" face="Arial, Helvetica, sans-serif"><a href="http://www.kath.de/">&copy; www.kath.de</a></font></p>
          </td>
          <td class="L12" width="1">            <div align="right"><br>
              <br>
              </div>
          </td>
          <td background="boxright.gif" width="9"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif" colspan="2"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td width="9"><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
</table>
</body>
</html>
