<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<meta name="title" content="Liturgie">
<meta name="author" content="Redaktion kath.de">
<meta name="publisher" content="kath.de">
<meta name="copyright" content="kath.de">
<meta name="description" content="">
<meta name="abstract" content="Lexikon �ber die Liturgie der katholischen Kirche">
<meta http-equiv="content-language" content="de">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="date" content="2006-00-01">
<meta name="robots" content="index,follow">
<meta name="revisit-after" content="10 days">
<meta name="revisit" content="after 10 days">
<meta name="DC.Title" content="Liturgie">
<meta name="DC.Creator" content="Redaktion kath.de">
<meta name="DC.Contributor" content="J�rgen Pelzer">
<meta name="DC.Rights" content="kath.de">
<meta name="DC.Publisher" content="kath.de">
<meta name="DC.Date" content="2006-00-01">
<meta name="DC.Description" content="Lexikon �ber die Liturgie der katholischen Kirche">
<meta name="DC.Language" content="de">
<meta name="DC.Type" content="Text">
<meta name="DC.Format" content="text/html">
<meta name="DC.Identifier" content="http://www.kath.de/lexikon/liturgie/">
<meta name="keywords" lang="de" content="Osternacht, Osterkerze, Osterlob, Exodus, Taufe">
<title>Exsultet - Osterjubel</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="kaltefleiter.css" title="fonts">
</head>
<body bgcolor="#ffffff" leftmargin="6" topmargin="6" marginwidth="6" marginheight="6">
<table width="100%" border="0" cellspacing="0" cellpadding="6">
  <tr>
    <td width="100" align="left" valign="top" height="3"> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b><font face="Arial, Helvetica, sans-serif">Das 
            Liturgie Lexikon</font></b></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td> <?php include("logo.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><font face="Arial, Helvetica, sans-serif"><strong>Inhaltsverzeichnis 
            </strong></font></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10"> <?php include("az.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
    <td rowspan="2" valign="top">
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td background="boxtop.gif" colspan="2"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="9"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif" width="8"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2" colspan="2"> 
            <h1><font face="Arial, Helvetica, sans-serif">Exsultet - Der Osterjubel</font></h1>
          </td>
          <td background="boxtopright.gif" width="9"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif" colspan="2"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td width="9"><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif" width="8"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="L12" width="516"> 
            <p><strong><font face="Arial, Helvetica, sans-serif">Das Osterlob
              der Osternachtfeier &uuml;bersetzt von Norbert Lohfink</font></strong></p>
            <p><font face="Arial, Helvetica, sans-serif">Schon juble in den Himmeln die Menge der Engel,<br>
  es juble die Schar der g&ouml;ttlichen Dienste,<br>
  und zu solch eines K&ouml;nigs Einzug k&uuml;nde Sieg die Trompete.<br>
              Da freue sich auch der Erdkreis, erhellt von leuchtenden Blitzen,<br>
              und, angestrahlt von der Pracht des ewigen K&ouml;nigs,<br>
              ersp&uuml;re er, da&szlig; er befreit ist vom Dunkel, das alles
              deckte.<br>
              Gl&uuml;ckselig sei auch die Mutter Kirche, geschm&uuml;ckt mit
              solch blitzendem Lichte,<br>
              und vom lauten Jubel der V&ouml;lker t&ouml;ne wider diese Halle.<br>
              So bitte ich euch, liebste Br&uuml;der und Schwestern, <br>
              die ihr steht beim herrlichen Glanz dieses heiligen Lichtes: <br>
              Ruft mit mir zu Gott, dem Allm&auml;chtigen,<br>
              er m&ouml;ge sich meiner erbarmen:<br>
              Da&szlig; er, der mich von sich aus in die Zahl der Leviten gerufen
              hat, <br>
              mich f&uuml;lle mit dem Glanz seines Lichtes <br>
              und durch mich das Lob dieser Kerze wirke.<br>
              <br>
              Der Herr sei mit euch.<br>
            Und mit deinem Geiste.<br>
            Erhebet die Herzen.<br>
            Wir haben sie beim Herrn.<br>
            Lasset uns danken dem Herrn, unserm Gott.<br>
            Das ist w&uuml;rdig und recht.<br>
            </font></p>
            <p><font face="Arial, Helvetica, sans-serif">Wahrhaft w&uuml;rdig und recht ist es, den unsichtbaren Gott,
              den allm&auml;chtigen Vater, <br>
              und seinen eingeborenen Sohn, unsern Herrn Jesus Christus, <br>
              mit aller Inbrunst des Herzens und Geistes, <br>
              im Dienste des Wortes, mit lauter Stimme zu preisen &#8211; <br>
              ihn, der f&uuml;r uns beim ewigen Vater die Schulden Adams bezahlt
              hat <br>
              und ausgel&ouml;scht hat den uralten Schuldbrief <br>
              mit Blut des Erbarmens.<br>
              Dies ist ja das Fest der Ostern, <br>
              an dem jenes wahre Lamm get&ouml;tet wird, <br>
              durch dessen Blut die T&uuml;ren der Gl&auml;ubigen gefeit sind.<br>
              Dies ist die Nacht, in der du am Anfang unsere V&auml;ter, die
              Nachkommen Israels, <br>
              nachdem sie herausgef&uuml;hrt waren aus &Auml;gypten, <br>
              trockenen Fu&szlig;es durch das Schilfmeer geleitet hast.<br>
              Dies also ist die Nacht, welche die Finsternis der S&uuml;nden <br>
              durch der Feuers&auml;ule Erleuchtung verscheucht hat.<br>
              Dies ist die Nacht, die heute auf der ganzen Erde Menschen, die
              zum Glauben in Christus gekommen sind, <br>
              losgel&ouml;st von den Lastern der Welt und vom Dunkel der S&uuml;nde, <br>
              heimf&uuml;hrt zur Gnade und den Heiligen zugesellt.<br>
              Dies ist die Nacht, da Christus die Fesseln des Todes gesprengt
              hat <br>
              und aus denen, die unter der Erde sind, als Sieger emporstieg.<br>
              Denn umsonst w&auml;ren wir geboren, <br>
              w&auml;re keiner gekommen, uns zu erl&ouml;sen.<br>
              O wie du dich &uuml;ber uns neigest <br>
              in staunenswertem Erbarmen!<br>
              O unerwartbare Zuwendung der Liebe: <br>
              Um den Knecht zu erl&ouml;sen, gabst du den Sohn dahin!<br>
              O wahrhaft n&ouml;tige S&uuml;nde Adams, <br>
              die getilgt ward vom Tode Christi!<br>
              O gl&uuml;ckliche Schuld, <br>
              der solch ein gro&szlig;er Erl&ouml;ser geziemte!<br>
              O wahrhaft selige Nacht, der einzig es ziemte, die Zeit und die
              Stunde zu kennen, <br>
              da Christus erstanden ist aus denen, die unter der Erde sind!<br>
              Dies ist die Nacht, von der geschrieben steht: <br>
&#8222;
              und die Nacht &#8211; wie der Tag wird sie leuchten,&#8220; <br>
              und: &#8222;die Nacht ist meine Erleuchtung, sie wird mir zur Wonne.&#8220;<br>
              Die Heiligung also, die in dieser Nacht sich ereignet, <br>
              jagt die Verbrechen fort, <br>
              sp&uuml;lt weg jede Schuld, <br>
              gibt Gestrauchelten wieder die Unschuld <br>
              und Trauernden Freude. <br>
              Feindschaft jagt sie fort, <br>
              bereitet die Eintracht <br>
              und beugt die Gewalten.<br>
              In deiner Gnade also, die diese Nacht durchwaltet, <br>
              nimm an, heiliger Vater, das Abendopfer dieses Loblieds,<br>
              das dir in dieser Kerze festlicher Darbringung, durch die H&auml;nde
              deiner Diener, aus der Arbeit der Bienen, <br>
              entrichtet die hochheilige Kirche.<br>
              Doch schon wissen wir, wie sich der Heroldsruf dieser S&auml;ule
              verbreitet, <br>
              die das goldene Feuer zur Ehre Gottes entz&uuml;ndet hat:<br>
              Wenn es auch vielfach geteilt ist, <br>
              wei&szlig; es dennoch von keiner Schw&auml;chung des weitergereichten
              Lichtes.<br>
              Es n&auml;hrt sich n&auml;mlich vom schmelzenden Wachse, <br>
              das als den Reichtum dieser kostbaren Fackel <br>
              die Mutter Biene bereitet hat.<br>
              O wahrhaft selige Nacht, da werden verbunden <br>
              Irdischem Himmlisches, Menschlichem G&ouml;ttliches.<br>
              So bitten wir dich, o Herr: <br>
              Diese Kerze, geweiht zur Ehre deines Namens, brenne unerm&uuml;dlich
              weiter, <br>
              um das Dunkel dieser Nacht zu vernichten. <br>
              Als lieblicher Opferduft entgegengenommen, <br>
              mische sie sich unter die Lichter am Himmel.<br>
              Lodernde Flamme &#8211; so soll sie finden der Morgenstern. <br>
              Jener Morgenstern n&auml;mlich, der keinen Untergang kennt:<br>
              Christus, dein Sohn, der, zur&uuml;ckgekehrt aus denen, die unter
              der Erde sind, <br>
              dem Menschengeschlechte heiter aufging <br>
              und der lebt und herrscht in alle Ewigkeit.<br>
            Amen.</font></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Erl&auml;uterungen</strong><br>
  Die Feier der Osternacht beginnt erst, wenn es dunkel geworden
                ist. Dann zog sie sich in den fr&uuml;hen Jahrhunderten durch
                die ganze Nacht. Wenn morgens dann die Sonne aufging, wurde das
                Oster-Halleluja angestimmt und das Osterevangelium verk&uuml;ndet.
                Durch die ganze Nacht hindurch wachte die Kirche dieser Botschaft
                entgegen. Lesungen und Ges&auml;nge aus dem Alten Testament f&uuml;llten
                die Nacht, dazu die Taufe der neuen Christen.Naturnotwendig geh&ouml;rt
                zu solchen n&auml;chtlichen Gottesdiensten die Symbolkraft des
                Lichtes. Auch andere n&auml;chtliche Gottesdienste w&auml;hrend
                des Jahres wurden durch eine Segnung des Lichtes und ein Loblied
                auf Christus, der unser Licht ist, er&ouml;ffnet, und in manchen
                Kl&ouml;stern geschieht das heute noch. Doch nie war das Lob
                des Lichtes so festlich wie in der Osternacht. Feierlich wurde
                und wird heute noch die entz&uuml;ndete Osterkerze in die dunkle,
                menschengef&uuml;llte Kirchenhalle hineingetragen. Dreimal singt
                der Diakon, jedesmal in h&ouml;herem Ton: &#8222;Lumen Christi&#8220; &#8211; &#8222;Das
                Licht des Christus.&#8220; Alle antworten: &#8222;Deo gratias&#8220; &#8211; &#8222;Gott
                sei gedankt.&#8220; Die Gl&auml;ubigen entz&uuml;nden ihre Kerzen
                am Licht der Osterkerze. Die Osterkerze wird auf einen gro&szlig;en
                Leuchter gestellt. Der Diakon tritt vor sie hin und singt nun
                den sch&ouml;nsten kantillierenden Gesang, den die lateinische
                Liturgie kennt, das &#8222;Exsultet&#8220;. Es stammt aus dem
                Sch&uuml;lerkreis des heiligen Ambrosius von Mailand. Es hatte
                viele Vertonungen, aber eine hat sich am Ende allein durchgesetzt,
                sicher die sch&ouml;nste. <br>
                <br>
              <strong>Theologie des Exsultet</strong><br>
              Es ist ein Lobgesang auf das Licht. Doch zugleich ist es ein
                Lobgesang auf das ganze Ostergeheimnis, und im Kern ist es ein
                Lobgesang
              auf den erstandenen Christus, den die Osterkerze zeichenhaft darstellt.
              Zugleich aber ist es der deutende Schl&uuml;ssel f&uuml;r all die
              Lesungen aus dem Alten Testament, die jetzt bald in der n&auml;chtlichen
              Feier gelesen werden. Was da, mit der Sch&ouml;pfung beginnend,
              vorgelesen wird, vor allem der Auszug aus &Auml;gypten, ist schon
              der Anfang, ja ein inneres St&uuml;ck des Ostergeheimnisses. Dieses
              Festgeheimnis beschr&auml;nkt sich nicht auf Jesu Tod und Auferstehung,
              sondern reicht zur&uuml;ck bis zu den Anf&auml;ngen der Welt, und
              es reicht voraus bis zur Wiederkunft Christi, des jetzt im Himmel
              Erh&ouml;hten. Mit ihrer Erwartung endet das Exsultet &#8211; die
              fr&uuml;he Christenheit war &uuml;berzeugt, Christus werde in einer
              Osternacht wiederkommen. Der Morgen, dem die n&auml;chtliche Feier
              entgegenschreitet, ist f&uuml;r die Feiernden der Morgen am Ende
            der Weltgeschichte.<br>
            Zuerst zieht der Diakon den Vorhang
              auf: Er besingt den Einzug des Erstandenen in den Raum des Himmels
              und das Licht, das dadurch
                sich in der ganzen Sch&ouml;pfung ausbreitet, vor allem auch &uuml;ber
                die jetzt versammelte Gemeinde. Dann bittet er, f&uuml;r ihn
                selbst zu beten, damit aus seinem Gesang auch wirklich Lob Gottes
                entsteht. Das ist gewisserma&szlig;en die Ouvert&uuml;re. Dann
                folgen die Wechselrufe wie bei jeder Pr&auml;fation, und im Stil
                von Pr&auml;fationen, nur viel festlicher, geht es dann auch
                weiter. Der Preis geht auf Gott, den unsichtbaren Vater, gleitet &uuml;ber
                auf seinen Sohn Jesus Christus, auf dessen Tod als das &#8222;wahre
                Lamm&#8220;, und gelangt so zur jetzigen Nacht. Immer wieder
                hei&szlig;t es: &#8222;Dies ist die Nacht&#8220; &#8211; und
                dabei schieben sich die verschiedenen N&auml;chte ineinander
                zu einer einzigen, die Nacht des Auszugs aus &Auml;gypten, des
                Durchzugs durchs Meer, die vielen N&auml;chte des W&uuml;stenzugs
                Israels hinter der Feuers&auml;ule her, die Nacht der Auferstehung
                Jesu von den Toten, die jetzige Nacht, in der in der ganzen Welt
                Menschen durch die Taufe &#8222;den Heiligen zugesellt&#8220; werden.
                Mitten in diesen Preis der Verschmelzung der N&auml;chte schieben
                sich, immer wieder mit &#8222;O&#8220; beginnend, Freudenrufe &uuml;ber
                das wunderbare Handeln Gottes ein. Dieses Loblied ist ein &#8222;Abendopfer&#8220;.
                In der Gestalt der Kerze bringt der Diakon es Gott dar und bittet
                ihn, dieses Opfer anzunehmen. Nochmals klingt das Lob der Nacht
                auf, und dann folgt die abschlie&szlig;ende Bitte: Das Licht
                der Osterkerze, in dem sich die Hingabe aller Versammelten verdichtet,
                m&ouml;ge mitten unter den himmlischen Lichtern weiterleuchten
                bis zum Morgen, wenn die Sonne aufgeht &#8211; die Sonne, die
                Christus ist, der am Ende der Zeiten wiederkehren wird.<br>
                <br>
              <strong>Hinf&uuml;hrung auf die Lesungen im Osternachgottesdienst</strong><br>
              Dies ist hohe Dichtung, zugleich ganz durchsetzt mit Bildern
                und W&ouml;rtern der Heiligen Schrift, am Ende vor allem des Hohenliedes
              der Liebe. Mit diesem Osterlob im Ohr wird man die Lesungen aus
              dem Alten Testament, die jetzt aufeinander folgen, ganz neu h&ouml;ren
              k&ouml;nnen. Sie werden alle in das eine gro&szlig;e Geheimnis
              der Ostern hineingenommen. <br>
              Die hier vorgelegte &Uuml;bersetzung ist genauer und bibeln&auml;her
              als die &Uuml;bersetzung in den liturgischen B&uuml;chern. Ferner
              ist sie sprachlich so angelegt, da&szlig; sie sich in gr&ouml;&szlig;erer
              Entsprechung zur Tonf&uuml;hrung des lateinischen Exsultet singen
              l&auml;&szlig;t. Zu diesem Anliegen gibt es eine Vertonung von<a href="http://www.thehomepagefactory.de/kantill/vert_grupp_tage_des_jahres.htm" target="_blank">              Erwin
              B&uuml;cken. </a></font></p>
            <p><font face="Arial, Helvetica, sans-serif">Norbert Lohfink S.J.<br>
            </font></p>
            <p>&nbsp;</p>
            <p><font size="2" face="Arial, Helvetica, sans-serif"><a href="http://www.kath.de/">&copy; www.kath.de</a></font></p>
          </td>
          <td class="L12" width="1">            <div align="right"><br>
              <br>
              </div>
          </td>
          <td background="boxright.gif" width="9"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif" colspan="2"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td width="9"><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
</table>
</body>
</html>
