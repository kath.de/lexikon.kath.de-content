<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title>Te Deum</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="kaltefleiter.css" title="fonts">
</head>
<body bgcolor="#ffffff" leftmargin="6" topmargin="6" marginwidth="6" marginheight="6">
<table width="100%" border="0" cellspacing="0" cellpadding="6">
  <tr>
    <td width="100" align="left" valign="top" height="3"> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b><font face="Arial, Helvetica, sans-serif">Das 
            Liturgie Lexikon</font></b></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td> <?php include("logo.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><font face="Arial, Helvetica, sans-serif"><strong>Inhaltsverzeichnis 
            </strong></font></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10"> <?php include("az.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
    <td rowspan="2" valign="top">
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td background="boxtop.gif" colspan="2"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8" height="37" background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2" colspan="2"> 
            <h1><font face="Arial, Helvetica, sans-serif">                Te
                Deum</font></h1>
          </td>
          <td background="boxtopright.gif" width="8"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif" colspan="2"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td width="8"><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif" width="8"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="L12" width="70%"> 
            <p>&nbsp;</p>
            <p><font face="Arial, Helvetica, sans-serif"><a href="http://www.tedeum-beten.de/" target="_blank">  </a>Te Deum, lat. f&uuml;r: Dich, Gott, sind die Anfangsbuchstaben
              eines Textes, der als <strong>&#8222;ambrosianischer Lobgesang&#8220; </strong>bekannt
              ist. Er wird dem Ambrosius von Mailand zugeschrieben. Der tats&auml;chliche
              Autor ist aber unbekannt. <br>
              Die deutsche &Uuml;bersetzung stammt von <strong>Romano Guardini</strong>. <br>
              <br>
              Es wird
              besonders an kirchlichen Festtagen gesungen aber auch bei anderen
              festlichen Anl&auml;ssen, besonders wen sie Anlass zur Danksagung
              geben. Die Melodie des Te deums bildet auch die Grundlage f&uuml;r
              die Lieder &#8222;Herr Gott, Dich loben wir&#8220; und &#8222;Gro&szlig;er
              Gott, wir loben dich&#8220;. <br>
              <br>
              Mehrstimmige Fassungen des Te deums stammen aus dem 13. Jahrhundert.
              Sie wurden abwechselnd von Gemeinde und Chor gesungen. Ab dem 18
            Jahrhundert gibt es dann auch Variationen im konzertanten Stil. </font></p>
            <p><font face="Arial, Helvetica, sans-serif">Dich, Gott, loben wir, dich, Herr, preisen wir.<br>
            Dir, dem ewigen Vater, huldigt das Erdenrund.</font></p>
            <p><font face="Arial, Helvetica, sans-serif"><br>
              Dir rufen die Engel alle,<br>
              dir Himmel und M&auml;chte ingesamt,<br>
              die Kerubim dir und die Serafim<br>
              mit niemals endender Stimme zu:<br>
              Heilig, heilig, heilig<br>
              der Herr, der Gott der Scharen!<br>
              Voll sind Himmel und Erde<br>
            von deiner hohen Herrlichkeit.</font></p>
            <p><font face="Arial, Helvetica, sans-serif"><br>
              Dich preist der glorreiche Chor der Apostel;<br>
              dich der Propheten lobw&uuml;rdige Zahl;<br>
              dich der M&auml;rtyrer leuchtendes Heer;<br>
              dich preist &uuml;ber das Erdenrund<br>
              die heilige Kirche;<br>
              dich, den Vater unerme&szlig;barer Majest&auml;t;<br>
              deinen wahren und einzigen Sohn;<br>
            und den Heiligen F&uuml;rsprecher Geist.</font></p>
            <p><font face="Arial, Helvetica, sans-serif"><br>
              Du K&ouml;nig der Herrlichkeit, Christus.<br>
              Du bist des Vaters allewiger Sohn.<br>
              Du hast der Jungfrau Scho&szlig; nicht verschm&auml;ht,<br>
              bist Mensch geworden,<br>
              den Menschen zu befreien.<br>
              Du hast bezwungen des Todes Stachel<br>
              und denen, die glauben,<br>
              die Reiche der Himmel aufgetan.<br>
              Du sitzest zur Rechten Gottes<br>
              in deines Vaters Herrlichkeit.<br>
              Als Richter, so glauben wir,<br>
            kehrst du einst wieder.</font></p>
            <p><font face="Arial, Helvetica, sans-serif"><br>
              Dich bitten wir denn,<br>
              komm deinen Dienern zu Hilfe,<br>
              die du erl&ouml;st mit kostbarem Blut.<br>
              In der ewigen Herrlichkeit<br>
              z&auml;hle uns deinen Heiligen zu.<br>
              Rette dein Volk, o Herr, und segne dein Erbe;<br>
            und f&uuml;hre sie und erhebe sie bis in Ewigkeit.</font></p>
            <p><font face="Arial, Helvetica, sans-serif"><br>
              An jedem Tag benedeien wir dich<br>
              und loben in Ewigkeit deinen Namen,<br>
              ja in der ewigen Ewigkeit.<br>
              In Gnaden wollest du, Herr,<br>
              an diesem Tag uns ohne Schuld bewahren.<br>
              Erbarme dich unser, o Herr, erbarme dich unser.<br>
              La&szlig; &uuml;ber uns dein Erbarmen geschehn,<br>
            wie wir gehofft auf dich.</font></p>
            <p><font face="Arial, Helvetica, sans-serif"><br>
              Auf dich, o Herr,<br>
              habe ich meine Hoffnung gesetzt.<br>
              In Ewigkeit werde ich nicht zuschanden.</font><br>
            </p>
            <p><br>
            </p>
            <p><br>
            </p>
            <p><br>
            </p>
            <p><font face="Arial, Helvetica, sans-serif">
            </font>            </p>
            <p><font face="Arial, Helvetica, sans-serif"><br>
            </font>            </p>
            <p><br>
            </p>
            <p><font face="Arial, Helvetica, sans-serif">
            </font>            </p>
            <p><font face="Arial, Helvetica, sans-serif"><br>
            </font>            </p>
            <p><br>
            </p>
            <p>&nbsp;
            </p>
            <p><br>
            </p>
            <p><br>
            </p>
            <p><br>
            </p>
            <p><br>
            </p>
            <p><br>
            </p>
            <p><br>
            </p>
            <p><br>
            </p>
            <p><br>              
                <font face="Arial, Helvetica, sans-serif">              </font><font face="Arial, Helvetica, sans-serif">              <br>
                Feedback bitte an mit 
          dem Stichwort Liturgie-Lexikon an: redaktion@kath.de</font></p></td>
          <td class="L12" width="30%"> 
            <div align="right"> 
              <p><br>
                <img src="te-deum-lexikon-liturgie.jpg" width="255" height="340"><br>
                <font face="Arial, Helvetica, sans-serif">Das Te Deum ist ein gro&szlig;es Loblied,<br>
                ein Gebet. Wie alle Gebete sprechen<br>
                die Christen sie zum Vater durch Christus<br> 
                im Heiligen Geist</font><br>
                <br>
                <br>
                <a href="http://www.magnificat.de" target="_blank"><img src="banner-magnificat-01.jpg" width="286" height="70" border="0"><br></a>
                <br>
                <font face="Arial, Helvetica, sans-serif"><a href="http://www.mein-tedeum.com/" target="_blank"><img SRC="TeDeum_Logo%20(2).gif" hspace="10" vspace="10" border="0" align="right" style="border: double 4px #009999"></a></font></a><br>
                <br>
                <font size="2" face="Arial, Helvetica, sans-serif"><br>
                </font></p>
            </div>
          </td>
          <td background="boxright.gif" width="8"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif" colspan="2"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
<td width="8"><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
 </tr> 
</table> 
</td>
<!-- neuer Code Anfang -->
<td style='vertical-align:top;'> 
     <script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
/* Symbollexikon neu */
google_ad_slot = "9464926645";
google_ad_width = 120;
google_ad_height = 600;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
     <br>
</td>
<!-- neuer Code Ende -->

  </tr>
  <tr>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
</table>
</body>
</html>
