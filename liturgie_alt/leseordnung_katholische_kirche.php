<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<meta name="title" content="Liturgie">
<meta name="author" content="Redaktion kath.de">
<meta name="publisher" content="kath.de">
<meta name="copyright" content="kath.de">
<meta name="description" content="">
<meta name="abstract" content="Lexikon �ber die Liturgie der katholischen Kirche">
<meta http-equiv="content-language" content="de">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="date" content="2006-00-01">
<meta name="robots" content="index,follow">
<meta name="revisit-after" content="10 days">
<meta name="revisit" content="after 10 days">
<meta name="DC.Title" content="Liturgie">
<meta name="DC.Creator" content="Redaktion kath.de">
<meta name="DC.Contributor" content="J�rgen Pelzer">
<meta name="DC.Rights" content="kath.de">
<meta name="DC.Publisher" content="kath.de">
<meta name="DC.Date" content="2006-00-01">
<meta name="DC.Description" content="Lexikon �ber die Liturgie der katholischen Kirche">
<meta name="DC.Language" content="de">
<meta name="DC.Type" content="Text">
<meta name="DC.Format" content="text/html">
<meta name="DC.Identifier" content="http://www.kath.de/lexikon/liturgie/">
<meta name="keywords" lang="de" content="Lesejahr A, Lesejahr B, Lesejahr C, Heiligengedenktag, Kirchenjahr">
<title>Leseordnung der katholischen Kirche</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="kaltefleiter.css" title="fonts">
</head>
<body bgcolor="#ffffff" leftmargin="6" topmargin="6" marginwidth="6" marginheight="6">
<table width="100%" border="0" cellspacing="0" cellpadding="6">
  <tr>
    <td width="100" align="left" valign="top" height="3"> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b><font face="Arial, Helvetica, sans-serif">Das 
            Liturgie Lexikon</font></b></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td> <?php include("logo.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><font face="Arial, Helvetica, sans-serif"><strong>Inhaltsverzeichnis 
            </strong></font></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10"> <?php include("az.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
    <td rowspan="2" valign="top">
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td background="boxtop.gif" colspan="2"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="9"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif" width="8"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2" colspan="2"> 
            <h1><font face="Arial, Helvetica, sans-serif">Leseordnung der
            katholischen Kirche f&uuml;r die Sonn- und Wochentage</font></h1>
          </td>
          <td background="boxtopright.gif" width="9"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif" colspan="2"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td width="9"><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif" width="8"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="L12" width="516"> 
            <p><font face="Arial, Helvetica, sans-serif"><strong> <a href="http://www.mein-tedeum.com/" target="_blank"> <img SRC="TeDeum_Logo%20(2).gif" hspace="10" vspace="10" border="0" align="right" style="border: double 4px #009999"> </a>Z&auml;hlung der Sonntage und Aufbau der liturgischen B&uuml;cher</strong><br>
                <br>
              Die katholische Kirche hat nach dem II. Vatikanischen Konzil die
                Zahl der Lesungen, die die Gottesdienstbesucher h&ouml;ren k&ouml;nnen,
                dadurch vergr&ouml;&szlig;ert, da&szlig; f&uuml;r den Sonntag
                drei Zyklen zusammengestellt wurden, die Lesejahre A, B und C.
                diese Lesejahre gelten auch f&uuml;r die gro&szlig;en Feste.
                Man findet in einem Band also alle Lesungen f&uuml;r die Feiertage
                wie f&uuml;r die Sonntage.<br>
                <br>
              Um zu berechnen, welches Lesejahr aktuell ist, geht man so vor:
              Wenn eine Jahreszahl durch drei teilbar ist, werden die Texte aus
              dem Lesejahr C gelesen.<br>
              Das jeweilige Lesejahr beginnt nicht mit dem 1. Januar, sondern
              mit dem <a href="http://www.kath.de/Kirchenjahr/advent.php">1.
              Advent</a> des Vorjahres.<br>
              Grundlage f&uuml;r die Lesejahre sind die Evangelisten Matth&auml;us
              (Lesejahr 1), Markus, 
              (Lesejahr 2), Lukas (Lesejahr 3). An den Festtagen wird der Text
              aus dem jeweiligen Evangelium genommen, au&szlig;erhalb der Festkreise
              wird das Evangelium kontinuierlich gelesen. Das Johannesevangelium
              hat kein eigenes Lesejahr, es wird vor allem
  in der Osterzeit gelesen.<br>
  Die 1. Lesung wird an den meisten Sonntagen und vielen Festtagen aus dem Alten
  Testament entnommen. Der Text entspricht thematisch dem Text aus dem Evangelium.
  In der 2. Lesung wird kontinuierlich ein Brief aus dem Neuen Testament gelesen.<br>
  F&uuml;r die Zeiten zwischen dem 6.Januar und <a href="http://www.kath.de/Kirchenjahr/aschermittwoch.php">Aschermittwoch</a> sowie zwischen
  Pfingsten und dem 1. Advent sind die Sonntage durchnumeriert. Es bleiben in
  der Regel 34 Sonntage, in manchen Jahren weniger, n&auml;mlich wenn in die
  Weihnachtszeit viele Sonntage fallen. Der Sonntag nach Epiphanie wird als 1.
  Sonntag im Kirchenjahr gez&auml;hlt. Er hat immer das Evangelium von der Taufe
  Jesu, das sich bei jedem Evangelisten findet. Nach dem Aschermittwoch beginnen
  die Sonntage der Fastenzeit. Die Sonntage der sog. Vorfastenzeit sind mit der
  Liturgiereform abgeschafft worden, sie hei&szlig;en Septuaginta, Sexuaginta,
  Quinquaginta. Nach Pfingsten wird die Reihe der Sonntage im Jahreskreis fortgesetzt.
  Wenn keine 34 Sonntage &uuml;brig geblieben sind, wird vom letzten Sonntag
  des Kirchenjahres zur&uuml;ckgerechnet. Es entfallen dann die Sonntage, die
  nicht mehr ber&uuml;cksichtig k&ouml;nnen, so da&szlig; die Z&auml;hlung immer
  mit dem <a href="http://www.kath.de/Kirchenjahr/christkoenig.php">34. Sonntag</a> endet. <br>
  Die Lesungen f&uuml;r die Wochentage orientieren sich an der Nummer des jeweiligen
  Sonntags. Da der Sonntag der erste Tag der (j&uuml;dischen) Woche ist, folgt
  z.B. auf den 12.Sonntag im Jahreskreis die 12. Woche, die jeweils am Samstagabend
  um 18 Uhr endet. Daher kann man am Samstagabend bereits den Sonntagsgottesdienst
  feiern.<br>
  F&uuml;r die Z&auml;hlung der Sonntage und damit der Wochen noch ein Hinweis:
  Fr&uuml;her wurden diese als &#8222;<a href="http://www.kath.de/Kirchenjahr/dreikoenige.php">Sonntage
  nach Epiphanie</a>&#8220; und &#8222;Sonntage
  nach Dreifaltigkeit&#8220; oder nach Pfingsten (und war darum der evangelischen
  um einen Z&auml;hler voraus) gez&auml;hlt.
  Mit der Liturgiereform nach dem II. Vatikanischen Konzil, die 1969 in Kraft
  trat,
  wurden
  die oben
  beschriebenen
  Leseordnungen
  und die Z&auml;hlung der Sonntage eingef&uuml;hrt. Die <a href="leseordnung_evangelische_kirche.php">evangelischen
  Kirchen</a>  z&auml;hlen weiter &#8222;nach Epiphanie&#8220; und &#8222;nach
  Trinitatis&#8220;.<br>
  Um herauszufinden, welcher Sonntag gerade gefeiert wird und welche Lesungen
  f&uuml;r die Wochentage vorgesehen sind, gibt es in jeder Sakristei ein sog.
  Direktorium. In Kirchenzeitungen finden sich ebenfalls f&uuml;r die kommende
  Woche die Angaben. Im Direktorium ist f&uuml;r jeden Tag angegeben, was
  gefeiert wird. Das ist nicht nur wegen der Sonn- und Feiertage wichtig, sondern
  auch f&uuml;r die einzelnen Wochentage. Denn die Heiligenfeste sind nicht nach
  dem Wochenrhythmus festgelegt, sondern nach dem Monatsdatum. Wie Weihnachten
  auf verschiedene Wochentage fallen kann, so auch z.B. der Gedenktag des heiligen
  Franziskus am 4.Oktober. Wenn kein gr&ouml;&szlig;eres Fest und auch kein Heiligengedenktag
  auf den Wochentag fallen, wird die Messe &#8222;vom Tag&#8220; mit ihren Lesungen
  zugrunde gelegt. F&uuml;r die Wochentage gibt es nicht drei, sondern nur zwei
  Zyklen. Sie hei&szlig;en Lesejahr I und II. Es &auml;ndert sich, anders als
  bei den drei Zyklen f&uuml;r die Sonn- und Feiertage, nicht die Auswahl der
  Evangelientexte, sondern nur die sog. 1. Lesung, da hierf&uuml;r die Auswahl
  sehr viel gr&ouml;&szlig;er ist. W&auml;hrend f&uuml;r die Lesung aus den
  Evangelien nur vier Evangelien zur Verf&uuml;gung stehen, kann f&uuml;r die
  1. Lesung aus dem gesamten Alten Testament sowie aus der Apostelgeschichte,
  den Briefen und der Geheimen Offenbarung des Neuen Testaments ausgew&auml;hlt
  werden. <br>
  Eckhard Bieger</font>            </p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p><strong><font face="Arial, Helvetica, sans-serif">&copy;<font size="2"> <a href="http://www.kath.de/">www.kath.de</a></font><br>
          </font></strong></p></td>
          <td class="L12" width="1"> 
            <div align="right"></div>
          </td>
          <td background="boxright.gif" width="9"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif" colspan="2"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td width="9"><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
</table>
</body>
</html>
