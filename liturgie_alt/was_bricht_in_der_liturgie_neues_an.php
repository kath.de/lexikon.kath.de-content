<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title>Was bricht in der Liturgie Neues an ?</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="kaltefleiter.css" title="fonts">
</head>
<body bgcolor="#ffffff" leftmargin="6" topmargin="6" marginwidth="6" marginheight="6">
<table width="100%" border="0" cellspacing="0" cellpadding="6">
  <tr>
    <td width="100" align="left" valign="top" height="3"> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b><font face="Arial, Helvetica, sans-serif">Das 
            Liturgie Lexikon</font></b></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td> <?php include("logo.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><font face="Arial, Helvetica, sans-serif"><strong>Inhaltsverzeichnis 
            </strong></font></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10"> <?php include("az.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
    <td rowspan="2" valign="top">
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td background="boxtop.gif" colspan="2"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8" height="37" background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2" colspan="2"> 
            <h1><font face="Arial, Helvetica, sans-serif">                <font face="Arial, Helvetica, sans-serif">Was bricht in der Liturgie
                Neues an ?</font></font></h1>
          </td>
          <td background="boxtopright.gif" width="8"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif" colspan="2"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td width="8"><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif" width="8"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="L12" width="70%"> 
            <p><br>
              <font face="Arial, Helvetica, sans-serif"><strong>Der Beginn der
              Endzeit &#8211; im
                Hier und Heute</strong><br>
                <br>
              Apocalypse now ist ein Filmtitel. Es k&ouml;nnte aber auch die &Uuml;berschrift
              der Liturgie sein:<br>
              Es gibt nur noch das apokalyptische Zeitma&szlig;, die Endzeit.
              Christentum ist Letztzeit. Das Buch der Apokalypse ist also nicht
              das letzte Buch, sondern das Aktuellste. Liturgie und Gottesdienst
              sind somit Feier des Anbruches der Letztzeit im Hier und heute.
              Es gibt verschiedene Schichten der Zeit: historisch etc., aber
              die wichtigste ist die apokalyptische. 
              Alles liturgische Feiern ist Verweisen auf den oben beschriebenen
              Verwandlungsprozess der Welt in die Ewigkeit. Dieses Geschehen
              nennt Johannes Liturgie. So hat jeder Tag keine Eigenaussage mehr,
              sondern l&auml;sst sich nur aus der Begegnung mit Jesus erkl&auml;ren.
              Deswegen meditieren die Christen. Die Liturgie ist nicht die Sonderfunktion
              des Alltags, sondern deren Definition. Mit Christus ist die Endzeit
              angebrochen, das Buch der Apokalypse beschreibt das als Liturgie.
              Nirgendwo war die Liturgie so m&auml;chtig in die Theologie eingedrungen,
            wie im Zeitverst&auml;ndnis.</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Das Ziel: Irdisch werden<br>
                <br>
              Konrad Weiss: &#8222;Wir sollen nicht geistig oder fromm werden,
              sondern nicht irdisch.&#8220; Christentum ist nicht weggehen oder
              verleugnen der Erde, sondern Jesus ist Mensch geworden, um alles
              mit sich zu heiligen. Alles im Leben des Menschen bis auf die S&uuml;nde
              ist heilig. Was hei&szlig;t es dann aber irdisch zu sein? Das kann
              man umschreiben mit dem Begriff des &#8222;neuen Leben&#8220;.
              Das neue Leben, das Jesus uns geschenkt hat, ist das irdische Leben.
              Der Christ wird niemals ein Asket sein, ein Weltfl&uuml;chtling,
              der sich abt&ouml;ten will. Gott ist Mensch geworden und kein spiritueller
              Asket. Askese hei&szlig;t dementsprechend Ein&uuml;bung in das &#8222;irdisch
              werden&#8220;. Hildegard von Bingen dr&uuml;ckt dies aus, wenn
              sie sagt: &#8222;Nicht der Kopf ist Gottesf&auml;hig, sondern der
              Magen.&#8220;</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Das Antlitz Gottes im Anderen<br>
                <br>
              Als Papst Johannes Paul II beerdigt wurde, lag ein weisses Tuch
                auf seinem Gesicht. Er hatte es selbst so angeordnet. Wenn der
                Mensch nach dem Antlitz Gottes geschaffen ist, dann ist ein Abglanz
                auf dem Gesicht des Menschen. Schon unter Konstantin dem Gro&szlig;en
                wurde eingef&uuml;hrt, dass Sklavenzeichen nicht im Kopfbereich
                vorgenommen werden d&uuml;rfen. Am Ende der Zeiten werden wir
                Gott von Antlitz zu Antlitz zu schauen. Das meint, zu erkennen
                wie &auml;hnlich wir ihm sind. Die Z&uuml;ge werden sich gleichen.
                Gott ist nicht der ganz andere, sondern die gr&ouml;&szlig;te
                Innigkeit zu Gott hin, das meint die Entdeckung des von Antlitz
                zu Antlitz schauen.</font><br>              
                <font face="Arial, Helvetica, sans-serif">              </font><font face="Arial, Helvetica, sans-serif">              <br>
              Feedback bitte an mit 
          dem Stichwort Liturgie-Lexikon an: redaktion@kath.de</font></p>
          </td>
          <td class="L12" width="30%"> 
            <div align="right"> 
              <p><br>
                <br>
                <font size="2" face="Arial, Helvetica, sans-serif"><br>
                </font></p>
            </div>
          </td>
          <td background="boxright.gif" width="8"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif" colspan="2"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
<td width="8"><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
 </tr> 
</table> 
</td>
<!-- neuer Code Anfang -->
<td style='vertical-align:top;'> 
     <script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
/* Symbollexikon neu */
google_ad_slot = "9464926645";
google_ad_width = 120;
google_ad_height = 600;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
     <br>
</td>
<!-- neuer Code Ende -->

  </tr>
  <tr>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
</table>
</body>
</html>
