<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title>Jahreskranz der G&uuml;te Gottes</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="kaltefleiter.css" title="fonts">
</head>
<body bgcolor="#ffffff" leftmargin="6" topmargin="6" marginwidth="6" marginheight="6">
<table width="100%" border="0" cellspacing="0" cellpadding="6">
  <tr>
    <td width="100" align="left" valign="top" height="3"> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b><font face="Arial, Helvetica, sans-serif">Das 
            Liturgie Lexikon</font></b></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td> <?php include("logo.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><font face="Arial, Helvetica, sans-serif"><strong>Inhaltsverzeichnis 
            </strong></font></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10"> <?php include("az.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
    <td rowspan="2" valign="top">
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td background="boxtop.gif" colspan="2"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8" height="37" background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2" colspan="2"> 
            <h1><font face="Arial, Helvetica, sans-serif">                <font face="Arial, Helvetica, sans-serif">Jahreskranz
            der G&uuml;te Gottes</font></font></h1>
          </td>
          <td background="boxtopright.gif" width="8"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif" colspan="2"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td width="8"><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif" width="8"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="L12" width="70%"> 
            <p>&nbsp;</p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Von der angemessenen Bezeichnung des Kirchenjahres</strong><br>
              <br>
  Wenn Gott eine Auszeichnung hat, dann sind das die, die an ihn
                glauben, seine Kirche. Die Kirche ist gleichsam der Kranz in
                der Hand Gottes. Auszugehen ist von dem Terminus, wann das Herrenjahr &uuml;berhaupt
                beginnt. Wann beginnt das Kirchenjahr? In der Vorstellungswelt
                vieler Menschen beginnt das Kirchenjahr mit Weihnachten, aber
                es beginnt schon mit dem ersten Advent. Das b&uuml;rgerliche
                Jahr hingegen mit dem 1 Januar. Die Juden und die Ostkirche feiern
                den Beginn des Jahres nach unserem Kalender am 1. September.<br>
              Hinter dem 1.9 steht ein theologisches Verst&auml;ndnis: Lev 23,
              24 Num 29,1: Numbers 29:1 Am ersten Tag des siebten Monats sollt
              ihr eine heilige Versammlung abhalten; an diesem Tag d&uuml;rft
              ihr keine schwere Arbeit verrichten. Es soll f&uuml;r euch ein
              Tag sein, an dem mit gro&szlig;em L&auml;rm die Trompete geblasen
              wird.<br>
              F&uuml;r die christliche Vorstellung ist folgende Textstelle wichtig:<br>
              Isaiah 61:1 Der Geist Gottes, des Herrn, ruht auf mir; denn der
              Herr hat mich gesalbt. Er hat mich gesandt, damit ich den Armen
              eine frohe Botschaft bringe und alle heile, deren Herz zerbrochen
              ist, damit ich den Gefangenen die Entlassung verk&uuml;nde und
              den Gefesselten die Befreiung, 2 damit ich ein Gnadenjahr des Herrn
              ausrufe, einen Tag der Vergeltung unseres Gottes, damit ich alle
            Trauernden tr&ouml;ste,</font></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Die Erf&uuml;llung
                  des Schriftwortes in Jesus</strong><br>
                <br>
  Aufgegriffen wird dieses Schriftwort im Evangelium nach Lukas 4,
                17-21 (4:21 Da begann er, ihnen darzulegen: Heute hat sich das
                Schriftwort, das ihr eben geh&ouml;rt habt, erf&uuml;llt.) <br>
              Jesus ruft also ein Gnadenjahr aus. Nicht der gro&szlig;e Gandenerlass
              alle 50 Jahre (wie &uuml;blich im Alten Testament), am Ende der
              Zeiten, sondern dieser Gnadenerlass ist jetzt immer, mit jeder
              Feier des Kirchenjahrs. Erl&ouml;sung ist geschehen in Jesus und
              damit ist endzeitliche Erl&ouml;sung geschehen. Das Herrenjahr
              ist kein Datum wie das b&uuml;rgerliche Jahr, sondern es ist ein
              Ausdruck von Zeit und Erl&ouml;sung. <br>
              Jahreskranz der G&uuml;te Gottes meint: Sein Heil ist immer da,
              immer gegenw&auml;rtig und wird immer gefeiert. Diese Feier geschieht
              f&uuml;r uns. Wir selber sind der Kranz Gottes. Wir sind der Kranz
              der G&uuml;te. Es geht nicht um ein Jahr, wie der Begriff Kirchenjahr
              es nahe legen k&ouml;nnte, sondern um das hier und jetzt - wobei
              der Schuldenerlass f&uuml;r immer gegeben ist. </font></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Viele Namen </strong><br>
              <br>
  Andere Bezeichnungen f&uuml;r das Kirchenjahr sind: <br>
              - Herrenjahr wird erstmals von J. Pinsk verwendet.<br>
              - Johannes Pomarius (1589): Kirchenjahr, erstmals bei den Protestanten
              benutzt<br>
              - Dom Gueranger (19. Jhd.): Das liturgische Jahr<br>
              - Parsch: Jahr des heiles</font></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Das Herrenjahr</strong><br>
              <br>
  Zum Begriff Herrenjahr: In der Mitte des Jahres steht das Herrenmysterium,
                also Ostern. Man will mit dem Begriff Herrenjahr ausdr&uuml;cken,
                das das triduum paschale, also die Tage von Gr&uuml;ndonnerstag
                bis Ostersamstag, die Mitte des Jahres ist. Mit dem Begriff Herrenjahr
                will man eben nicht nur aussagen, dass ein eigenes Jahr neben
                dem b&uuml;rgerlichen besteht, sondern das unser ganzes Leben
                gewandelt ist. . Otto Casel hat die Theologie des Herrenjahres
                entworfen.</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Psalm 65:12 Du kr&ouml;nst das Jahr mit deiner G&uuml;te, deinen
              Spuren folgt &Uuml;berflu&szlig;.<br>
              Isaiah 62:3 Du wirst zu einer pr&auml;chtigen Krone in der Hand
              des Herrn, zu einem k&ouml;niglichen Diadem in der Rechten deines
              Gottes.</font><br>
            </p>
            <p><br>
            </p>
            <p><br>
            </p>
            <p><br>
            </p>
            <p><br>
            </p>
            <p><br>
            </p>
            <p><br>              
                <font face="Arial, Helvetica, sans-serif">              </font><font face="Arial, Helvetica, sans-serif">              <br>
                Feedback bitte an mit 
          dem Stichwort Liturgie-Lexikon an: redaktion@kath.de</font></p></td>
          <td class="L12" width="30%"> 
            <div align="right"> 
              <p><br>
                <br>
                <font size="2" face="Arial, Helvetica, sans-serif"><br>
                </font></p>
            </div>
          </td>
          <td background="boxright.gif" width="8"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif" colspan="2"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
<td width="8"><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
 </tr> 
</table> 
</td>
<!-- neuer Code Anfang -->
<td style='vertical-align:top;'> 
     <script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
/* Symbollexikon neu */
google_ad_slot = "9464926645";
google_ad_width = 120;
google_ad_height = 600;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
     <br>
</td>
<!-- neuer Code Ende -->

  </tr>
  <tr>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
</table>
</body>
</html>
