<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title>Quadragesima</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="kaltefleiter.css" title="fonts">
</head>
<body bgcolor="#ffffff" leftmargin="6" topmargin="6" marginwidth="6" marginheight="6">
<table width="100%" border="0" cellspacing="0" cellpadding="6">
  <tr>
    <td width="100" align="left" valign="top" height="3"> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b><font face="Arial, Helvetica, sans-serif">Das 
            Liturgie Lexikon</font></b></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td> <?php include("logo.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><font face="Arial, Helvetica, sans-serif"><strong>Inhaltsverzeichnis 
            </strong></font></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10"> <?php include("az.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
    <td rowspan="2" valign="top">
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td background="boxtop.gif" colspan="2"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif" width="8"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2" colspan="2"> 
            <h1><font face="Arial, Helvetica, sans-serif"><strong>Quadragesima</strong></font></h1>
          </td>
          <td background="boxtopright.gif" width="8"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif" colspan="2"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td width="8"><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif" width="8"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="L12" width="70%"> 
            <p><br>
              <font face="Arial, Helvetica, sans-serif">Der Name Quadragesima
              ist seit Hieronymus 384 in Rom bezeugt und bezieht sich auf die
              vierzig als heilige Zeit. Im griechischen Raum wird die Zeit auch
              als tessarakoste bezeichnet. In den germanischen Sprachen wird
              das Moment des Fastens zu sehr &uuml;berbetont. Die Erw&auml;hnung
              dieser Zeit als Vorbereitungszeit ist erstmals im Osterhirtenbrief
              des Athanasius von Alexandrien um 334 belegt. Ende des 4. Jahrhunderts
              war die Quadragesima, die man eher als vor&ouml;sterliche Bu&szlig;zeit
              bezeichnen sollte, allgemein bekannt. Sie diente der 40 t&auml;tigen
              (quadra) Vorbereitung der Katechumenen auf Ostern. Ab dem 5. Jahrhundert
              feierte man dann an Ostern auch die Rekonzilation der B&uuml;&szlig;er
            mit der Kirche an Ostern.</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Auf vierzig Tage kommt
                man, indem man 6 Wochen nimmt, die 6 Sonntage abzieht, 4 Tage
                Aschermittwoch / do / Fr / Sa dazurechnet und den
              Karfreitag und den Karsamstag. 40 hat hohe biblische Bedeutung:
              40 Tage wanderte Israel in der W&uuml;ste, vierzig Tage war Mose
              auf dem Sinai, vierzig Tage fastete Jesus in der W&uuml;ste. </font></p>
            <p><font face="Arial, Helvetica, sans-serif">Im 4 und 5 Jahrhundert
                werden der Mittwoch und der Freitag als Fasttage ausschlie&szlig;lich mit Wortgottesdiensten (eucharistisches
              Fasten) gefeiert. Im 6. Jahrhundert kommen Mo., Di. und Sa. Mit
              der Eucharistie dazu, sp&auml;ter noch der Do als Gottesdienstag. <br>
              Im 7. Jahrhundert gab es drei Vorfastenzeiten in der Kirche, die
              im Osten 2 Wochen und bei den R&ouml;mern 3 Wochen dauerte. Die
              Bauern hatten keine Vorfastenzeit.</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Die Asche, die am Aschermittwoch
                an die Gl&auml;ubigen ausgeteilt
              wird, wurde bis zum 11. Jahrhundert nur an die B&uuml;&szlig;er
              ausgeteilt.              Die Kreuzesverh&uuml;llung ist ein verehrendes Vorgehen (was man
              liebt, das verh&uuml;llt man) Eine Kreuzesenth&uuml;llung w&auml;hrend
              der Karfreitagsliturgie gibt es erst seit dem 17. Jahrhundert. <br>
              Seit dem 16. Jahrhundert gibt es das rosafarbene Messgewand am
              Sonntag &#8222;Laetare&#8220; (4. Sonntag). Sein Ursprung k&ouml;nnte
              mit dem Rosenbrauch zusammenh&auml;ngen.</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Seit dem II. Vatikanum
                spricht man von der &ouml;sterlichen Bu&szlig;zeit.
              In ihr werden zwei Themen wichtig: Die Tauferinnerung und die Bu&szlig;e
              und Umkehr. </font><font face="Arial, Helvetica, sans-serif"> Im
              Laufe der drei Lesejahre spiegeln sich die drei Hauptthemen der
              Quadragesima: A Initiation (Taufe), B das Paschamysterium
              (Passion Jesu) und C Bu&szlig;e. </font></p>
            <p><font face="Arial, Helvetica, sans-serif">Die &ouml;sterliche Bu&szlig;zeit war die Zeit f&uuml;r
                Exerzitien gewesen, woher sich auch die Tradition des quadragesimalen
                Exercitums
              kommt. </font></p>
            <p><font face="Arial, Helvetica, sans-serif">Eine Z&auml;sur bildet der 5. Fastensonntag als Passionssonntag,
            wo die Kreuze und Bilder verh&uuml;llt werden </font></p>
            <p><br>              
              <font face="Arial, Helvetica, sans-serif">              </font><font face="Arial, Helvetica, sans-serif">              <br>
              Feedback bitte an mit 
          dem Stichwort Liturgie-Lexikon an: redaktion@kath.de</font></p></td>
          <td class="L12" width="30%"> 
            <div align="right"> 
              <p><br>
                <br>
                <font size="2" face="Arial, Helvetica, sans-serif"><br>
                </font></p>
            </div>
          </td>
          <td background="boxright.gif" width="8"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif" colspan="2"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
<td width="8"><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
 </tr> 
</table> 
</td>
<!-- neuer Code Anfang -->
<td style='vertical-align:top;'> 
     <script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
/* Symbollexikon neu */
google_ad_slot = "9464926645";
google_ad_width = 120;
google_ad_height = 600;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
     <br>
</td>
<!-- neuer Code Ende -->

  </tr>
  <tr>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
</table>
</body>
</html>
