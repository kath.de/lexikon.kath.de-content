<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title>Was ist die Grundlage der Liturgie ?</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="kaltefleiter.css" title="fonts">
</head>
<body bgcolor="#ffffff" leftmargin="6" topmargin="6" marginwidth="6" marginheight="6">
<table width="100%" border="0" cellspacing="0" cellpadding="6">
  <tr>
    <td width="100" align="left" valign="top" height="3"> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b><font face="Arial, Helvetica, sans-serif">Das 
            Liturgie Lexikon</font></b></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td> <?php include("logo.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><font face="Arial, Helvetica, sans-serif"><strong>Inhaltsverzeichnis 
            </strong></font></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10"> <?php include("az.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
    <td rowspan="2" valign="top">
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td background="boxtop.gif" colspan="2"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif" width="8"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2" colspan="2"> 
            <h1><font face="Arial, Helvetica, sans-serif">Wo ist der Sitz im
                Leben?</font></h1>
          </td>
          <td background="boxtopright.gif" width="8"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif" colspan="2"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td width="8"><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif" width="8"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="L12" width="70%"> 
            <p><br>
              <strong><font face="Arial, Helvetica, sans-serif">Gott wird Sch&ouml;pfung &#8211; und
              durchdringt das gesamte Leben.</font></strong><font face="Arial, Helvetica, sans-serif"><br>
              <br>
              Gottesdienst gibt es nur als Leben. Das Leben der Christen ist
                Nachvollzug des Lebens Jesu. Es geht dabei nicht um ethische
                Nachfolge. Leben und Biografie hat f&uuml;r das Christentum theologische
                Relevanz. Prim&auml;r ist nicht der Gottesdienst, sondern das
                Leben der Gl&auml;ubigen.              Liturgie darf nichts anderes
                sein als Leben. Der Gottesdienst ist kein Sonderbereich neben
                dem Alltag. Der Gottesdienst muss selber
              biografisch sein. Gottesdienst muss gelebtes Leben sein. F&uuml;r
              den Christen gibt es keinen Unterschied mehr zwischen sakral und
              profan. Es kommt zur innigsten Verschr&auml;nkung. Geistliches
              Leben ist Leben im 24-Stunden Rhythmus. <br>
              Jesus hat uns das vorgelebt als der in die Sch&ouml;pfung Eingegangene.
              Deshalb gibt es kein Gottesdienst ohne Sch&ouml;pfung. Es bedarf
            im Gottesdienst der erdhaften Elemente.</font></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Die Aufhebung der Zeit</strong><br>
                <br>
                Gott wird Zeit, somit wird Ewigkeit Zeit. Die Zeit ist damit
                aufgehoben. Damit verliert die Zeit ihr Merkmal, die Verg&auml;nglichkeit.
                Gott hat Zeit f&uuml;r seine Gesch&ouml;pfe. Ewigkeit wird zum
                Inhalt der Zeit. Sobald die Ewigkeit in die Zeit eintritt ist
                die Zeit erf&uuml;llt. Augustinus sagt: &#8222;Jesus ist anders
                zum Vater gegangen als er gekommen ist.&#8220; Der Unterschied
                zwischen dem irdischen und dem heimgekehrten Jesu ist, dass er
                als irdischer Jesu nur an einer Stelle sein kann, nach der Himelfahrt
                aber entgrenzt ist. Das Abendmahl fand nur an einer Stelle statt,
                die Messe aber immer. Indem er aufersteht und zum Vater auff&auml;hrt,
                wird unsere Zeit in die g&ouml;ttliche Zeit aufgenommen. Wer
                in die Zeit Christi eingetreten ist, f&uuml;r den gibt es keinen
                Tod mehr. Es gibt keinen Gottesdienst, der nichts mit der Zeit
                zu tun h&auml;tte. Ausbuchstabiert ist das in dem Herrenjahr
                (=Kirchenjahr), um zu zeigen, dass sein Leben, das Ma&szlig; der
                Zeit (=Jahr) ist. Indem wir das Herrenjahr feiern, sagen wir,
                wir verstehen die Zeit nur noch im Hinblick auf Christus hin.
                Die Feste sind Ausdruck davon, dass wir an jedem Fest den Beginn
                der neuen Zeit feiern. <br>
                Doppelt verdeutlicht ist das nochmals in der Stundenliturgie,
                die dasselbe wie das Herrenjahr am Tag ausdr&uuml;ckt. <br>
              Hans Urs von Balthasar: &#8222;Alles im Leben ist ewigkeitsf&auml;hig,
              weil immer schon ewigkeitshaltig.&#8220;</font><br>              
              <font face="Arial, Helvetica, sans-serif">              </font><font face="Arial, Helvetica, sans-serif">              <br>
              Feedback bitte an mit 
          dem Stichwort Liturgie-Lexikon an: redaktion@kath.de</font></p>
          </td>
          <td class="L12" width="30%"> 
            <div align="right"> 
              <p><br>
                <br>
                <font size="2" face="Arial, Helvetica, sans-serif"><br>
                </font></p>
            </div>
          </td>
          <td background="boxright.gif" width="8"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif" colspan="2"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
<td width="8"><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
 </tr> 
</table> 
</td>
<!-- neuer Code Anfang -->
<td style='vertical-align:top;'> 
     <script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
/* Symbollexikon neu */
google_ad_slot = "9464926645";
google_ad_width = 120;
google_ad_height = 600;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
     <br>
</td>
<!-- neuer Code Ende -->

  </tr>
  <tr>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
</table>
</body>
</html>
