<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<meta name="title" content="Liturgie">
<meta name="author" content="Redaktion kath.de">
<meta name="publisher" content="kath.de">
<meta name="copyright" content="kath.de">
<meta name="description" content="">
<meta name="abstract" content="Lexikon �ber die Liturgie der katholischen Kirche">
<meta http-equiv="content-language" content="de">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="date" content="2006-00-01">
<meta name="robots" content="index,follow">
<meta name="revisit-after" content="10 days">
<meta name="revisit" content="after 10 days">
<meta name="DC.Title" content="Liturgie">
<meta name="DC.Creator" content="Redaktion kath.de">
<meta name="DC.Contributor" content="J�rgen Pelzer">
<meta name="DC.Rights" content="kath.de">
<meta name="DC.Publisher" content="kath.de">
<meta name="DC.Date" content="2006-00-01">
<meta name="DC.Description" content="Lexikon �ber die Liturgie der katholischen Kirche">
<meta name="DC.Language" content="de">
<meta name="DC.Type" content="Text">
<meta name="DC.Format" content="text/html">
<meta name="DC.Identifier" content="http://www.kath.de/lexikon/liturgie/">
<meta name="keywords" lang="de" content="N�chtliche Gebetswache, Mette, Ostervigil, Lesungen, Ges�nge">
<title>Vigil - n&auml;chtliche Gebetswache</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="kaltefleiter.css" title="fonts">
</head>
<body bgcolor="#ffffff" leftmargin="6" topmargin="6" marginwidth="6" marginheight="6">
<table width="100%" border="0" cellspacing="0" cellpadding="6">
  <tr>
    <td width="100" align="left" valign="top" height="3"> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b><font face="Arial, Helvetica, sans-serif">Das 
            Liturgie Lexikon</font></b></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td> <?php include("logo.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><font face="Arial, Helvetica, sans-serif"><strong>Inhaltsverzeichnis 
            </strong></font></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10"> <?php include("az.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
    <td rowspan="2" valign="top">
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td background="boxtop.gif" colspan="2"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="9"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif" width="8"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2" colspan="2"> 
            <h1><font face="Arial, Helvetica, sans-serif">Vigil - n&auml;chtliche
                Gebetswache</font></h1>
          </td>
          <td background="boxtopright.gif" width="9"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif" colspan="2"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td width="9"><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif" width="8"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="L12" width="516"> 
            <p><strong><font face="Arial, Helvetica, sans-serif">              Osternachtfeier, Mette</font></strong></p>
            <p class=MsoNormal><font face="Arial, Helvetica, sans-serif"> Der
                Begriff "Vigil" kommt aus dem Lateinischen
              und bedeutet W�chter. Darin steckt schon "wach bleiben". Die "Vigiliae" sind
              Nachtwachen, durchwachte N�chte. Die Vigil als n�chtliche Gebetswache
              geh�rt zur Stundenliturgie der Kirche: in den Kl�stern versammeln
              sich die M�nche oder Nonnen zum mittern�chtlichen Gebet mit Psalmengesang,
              Lesungen aus dem Alten und Neuen Testament, Meditationen der Kirchenv�ter.
              Die Vigilfeier schlie�t traditionell mit dem Te Deum "Gro�er Gott
              wir loben dich", ab. Der Ursprung dieser n�chtlichen Feier liegt
              in der Zeit, als Gott das Volk Israel aus der Sklaverei �gyptens
              befreit hat. Die rettende Befreiungstat Gottes, die Erschlagung
              der Erstgeburt (die 10. Sog. �gyptische Plage), die Befreiung Israels
              aus dem Sklavenhaus �gypten und der Durchzug durch das Rote Meer
              geschehen in einer Nacht, genau um Mitternacht (Ex 12,29). "Eine
              Nacht des Wachens war dies f�r den Herrn, damit er sie aus dem
              Land �gypten herausf�hren konnte, das ist diese dem Herrn geweihte
              Nacht, ein Wachen f�r alle S�hne Israels in all ihren Generationen." (Ex
              12,42) Verbunden mit dem Auszug aus �gypten ist das Paschamahl,
              das auch Jesus, der ja Jude war, mit seinen J�ngern feierte. Dabei
              gibt er diesem Mahl einen neuen Akzent: Es wird zur Danksagung,
              zur Eucharistie der Christen. Sie feiern dieses Mahl, bis Christus
              am Ende der Zeiten in Herrlichkeit wieder kommt. Auch im Urchristentum
              kennt man also diese n�chtliche Feier des Gedenkens, jetzt aber
              nicht mehr an den Auszug aus �gypten, sondern im Gedenken an Jesu
              Tod und Auferstehung. Jesus selbst ermahnt die Seinen zum Wachen
              und Beten. In seiner Todesfurcht in der Nacht vor seiner Hinrichtung
              ermahnt er die J�nger: "Wacht und betet, damit ihr nicht in Versuchung
              kommt!". Und auch, auf seine eigene Wiederkunft bezogen: "Euere
              Lenden sollen umg�rtet und euere Lampen brennend sein. ... Gl�ckselig
              jene Knechte, die der Herr, wenn er kommt, wachend finden wird!" (Lukas
              12. 35.37a) Die Christen sollen wachend und betend mit brennenden
              Lampen in ihren H�nden bereit sein f�r ihren wiederkommenden Herrn.
              Im Gleichnis von den f�nf t�richten und f�nf klugen Jungfrauen
              (Mt 25,1-13) ist anschaulich erz�hlt, wie wichtig dabei eine brennende
              Lampe, gemeint ist damit ein lebendiger Glaube, ist. Neben dem
              Wachen und beten bekommt nun die Lichtsymbolik eine tragende Rolle:
              Schon aus rein praktischen Gr�nden brauchen die Christen bei ihren
              n�chtlichen Versammlungen, z. B. in Roms Katakomben, Fackeln und
              Lampen. Christus selbst spricht von sich: "Ich bin das Licht der
              Welt!" (Joh 8,12) Nicht nur die h�usliche j�dische Feier kennt
              einen Lichtsegen, sondern auch bei christlichen Zusammenk�nfte
              hei�en jetzt die Christgl�ubigen in einer Lichtfeier das Licht,
              also gleichsam Christus, willkommen, und segnen es. Der n�chtliche
              Gottesdienst hat seinen Zeitpunkt zur Mitternacht: die Mitte der
              Nacht, die Mette, wenn das Dunkel am m�chtigsten ist, ist gleichzeitig
              die Stunde des Herrn. "Und das Licht scheint in der Finsternis,
              und die Finsternis hat es nicht ergriffen." (Joh 1,5) Zentrale
              Glaubensaussagen feiert die Kirche deshalb liturgisch am Abend
              und in der Nacht: Eucharistie: Abend-Mahl; Menschwerdung Gottes:
              Weih-Nacht , <a href="http://www.kath.de/Kirchenjahr/weihnachten.php">Christmette</a> (Mette: Mitte der Nacht), Auferstehung
              Jesu: <a href="exsultet_osterjubel.php">Osternacht</a>. </font></P>
            <p class=MsoNormal><font face="Arial, Helvetica, sans-serif">Peter
                M&uuml;nch </font></P>            <p>&nbsp;</p>
            <p><font size="2" face="Arial, Helvetica, sans-serif"><a href="http://www.kath.de/">&copy; www.kath.de</a></font></p>
          </td>
          <td class="L12" width="1">            <div align="right"><br>
              <br>
              </div>
          </td>
          <td background="boxright.gif" width="9"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif" colspan="2"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td width="9"><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
</table>
</body>
</html>
