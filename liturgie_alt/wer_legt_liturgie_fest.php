<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title>Wer legt Liturgie fest?</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="kaltefleiter.css" title="fonts">
</head>
<body bgcolor="#ffffff" leftmargin="6" topmargin="6" marginwidth="6" marginheight="6">
<table width="100%" border="0" cellspacing="0" cellpadding="6">
  <tr>
    <td width="100" align="left" valign="top" height="3"> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b><font face="Arial, Helvetica, sans-serif">Das 
            Liturgie Lexikon</font></b></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td> <?php include("logo.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><font face="Arial, Helvetica, sans-serif"><strong>Inhaltsverzeichnis 
            </strong></font></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10"> <?php include("az.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
    <td rowspan="2" valign="top">
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td background="boxtop.gif" colspan="2"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8" height="37" background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2" colspan="2"> 
            <h1><font face="Arial, Helvetica, sans-serif">                <font face="Arial, Helvetica, sans-serif">Wer
                  legt Liturgie fest?</font></font></h1>
          </td>
          <td background="boxtopright.gif" width="8"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif" colspan="2"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td width="8"><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif" width="8"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="L12" width="70%"> 
            <p>&nbsp;</p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Zwischen Rom und den Ortskirchen</strong><br>
              <br>
  Man kann unterscheiden zwischen Universalliturgie, Di&ouml;zesanliturgie
              (die Liturgie einer Di&ouml;zese) und der Ordensliturgie. Der Vatikan
              will es auf der obersten Ebene f&uuml;r die ganze Welt verbindlich
              festlegen. Ausdruck davon ist das r&ouml;mische Messbuch, das Missale
              Romanum, in seiner Ausgabe ab 1970. Es ist verbindlich f&uuml;r
              die Krichen. Modifiziert wird es f&uuml;r die einzelnen Sprachgruppen.
              Pl&ouml;tzlich werden Liturgiegebiete nach ihrer Sprache definiert.
              Die Urzelle, die Di&ouml;zesanliturgie, ist nicht mehr ausschlaggebend.
              Der Bischof als Urzelle aller Liturgie ist nicht mehr relevant.
              Fr&uuml;her wurde n&auml;mlich die Liturgie von dem Ortsbischof
              festgelegt. Das dr&uuml;ckt sich noch darin aus, dass der Ortsbischof
              das Direktorium herausgibt. Das ist der letzte Rest der alten Regelung.
              Mittlerweile m&uuml;ssen die B&uuml;cher von Rom approbiert werden,
              eine Di&ouml;zese kann kein eigenst&auml;ndiges mehr herausgeben.</font></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Die einen Liturgie und die vielen Liturgien</strong><br>
              <br>
              Liturgie ist immer das, was die Kirche als ihren Glauben anerkennt.
                Liturgie ist somit immer auch dogmatisch Es wird nicht gefeiert,
                was nicht Glauben der Kirche ist. <br>
                Liturgie hat mit Beauftragung zu tun. Der Grund der Beauftragung
              ist die apostolische Sukzession, alle Liturgie leitet sich von
              den Aposteln ab, weil sie Zeugen der Auferstehung sind. Es gibt
              somit nur Auferstehungsliturgien, weil die Aposteln Zeugen der
              Auferstehung sind. Liturgie gibt es nur weil die Auferstehung wahr
              ist. Hinzu kommt bei den Aposteln die Beauftragung durch Jesu in
              dem Evangelium nach Markus 16,15 &#8222;Und er sprach zu ihnen:
              Gehet hin in alle Welt und prediget das Evangelium der ganzen Sch&ouml;pfung!&#8220;.
              Die Ordnung der Liturgie ist festgelegt in den B&uuml;chern (erg&auml;nzen)</font><br>
            </p>
            <p><br>              
                <font face="Arial, Helvetica, sans-serif">              </font><font face="Arial, Helvetica, sans-serif">              <br>
                Feedback bitte an mit 
          dem Stichwort Liturgie-Lexikon an: redaktion@kath.de</font></p></td>
          <td class="L12" width="30%"> 
            <div align="right"> 
              <p><br>
                <br>
                <font size="2" face="Arial, Helvetica, sans-serif"><br>
                </font></p>
            </div>
          </td>
          <td background="boxright.gif" width="8"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif" colspan="2"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
<td width="8"><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
 </tr> 
</table> 
</td>
<!-- neuer Code Anfang -->
<td style='vertical-align:top;'> 
     <script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
/* Symbollexikon neu */
google_ad_slot = "9464926645";
google_ad_width = 120;
google_ad_height = 600;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
     <br>
</td>
<!-- neuer Code Ende -->

  </tr>
  <tr>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
</table>
</body>
</html>
