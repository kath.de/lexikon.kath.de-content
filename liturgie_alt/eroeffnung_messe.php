<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<meta name="title" content="Liturgie">
<meta name="author" content="Redaktion kath.de">
<meta name="publisher" content="kath.de">
<meta name="copyright" content="kath.de">
<meta name="description" content="">
<meta name="abstract" content="Lexikon �ber die Liturgie der katholischen Kirche">
<meta http-equiv="content-language" content="de">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="date" content="2006-00-01">
<meta name="robots" content="index,follow">
<meta name="revisit-after" content="10 days">
<meta name="revisit" content="after 10 days">
<meta name="DC.Title" content="Liturgie">
<meta name="DC.Creator" content="Redaktion kath.de">
<meta name="DC.Contributor" content="J�rgen Pelzer">
<meta name="DC.Rights" content="kath.de">
<meta name="DC.Publisher" content="kath.de">
<meta name="DC.Date" content="2006-00-01">
<meta name="DC.Description" content="Lexikon �ber die Liturgie der katholischen Kirche">
<meta name="DC.Language" content="de">
<meta name="DC.Type" content="Text">
<meta name="DC.Format" content="text/html">
<meta name="DC.Identifier" content="http://www.kath.de/lexikon/liturgie/">
<meta name="keywords" lang="de" content="<font face="Arial, Helvetica, sans-serif">Einzug, Verehrung des Altars, Eingangsgru&szlig;, Bu&szlig;akt,
            Kyrie, Gloria, Tagesgebet"</font>">
<title>Er&ouml;ffnung der Messe</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="kaltefleiter.css" title="fonts">
</head>
<body bgcolor="#ffffff" leftmargin="6" topmargin="6" marginwidth="6" marginheight="6">
<table width="100%" border="0" cellspacing="0" cellpadding="6">
  <tr>
    <td width="100" align="left" valign="top" height="3"> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b><font face="Arial, Helvetica, sans-serif">Das 
            Liturgie Lexikon</font></b></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td> <?php include("logo.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><font face="Arial, Helvetica, sans-serif"><strong>Inhaltsverzeichnis 
            </strong></font></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10"> <?php include("az.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
    <td rowspan="2" valign="top">
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td background="boxtop.gif" colspan="2"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="9"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif" width="8"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2" colspan="2"> 
            <h1><font face="Arial, Helvetica, sans-serif">Er&ouml;ffnung
            der Messe</font></h1>
          </td>
          <td background="boxtopright.gif" width="9"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif" colspan="2"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td width="9"><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif" width="8"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="L12" width="516"> 
            <p><strong><font face="Arial, Helvetica, sans-serif">              Einzug, Verehrung des Altars, Eingangsgru&szlig;, Bu&szlig;akt,
              Kyrie, Gloria, Tagesgebet</font></strong></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Einzug:</strong><br>
  Fr&uuml;her begann die Messe direkt mit den Lesungen. Bis zum 5
              Jahrhundert ging den Lesungen ein stilles Gebet
  der Kleriker voraus, die sich dazu vor den Altar niederwarfen. Dies findet
  sich heute noch in der Karfreitagsliturgie. Der Fachausdruck daf&uuml;r ist
              die Proskynese. Durch dieses stille Gebet wurde der Einzug abgeschlossen.
              Seit Gregor dem Gro&szlig;en (um 600) werden dabei Psalmen bzw.
              Lieder gesungen. Die Gestalt des heute &uuml;blichen Einzugs entwickelte
              sich im Mittelalter: Wenn der Papst feierlich in eine Basilika
              einzog,
              wurden ihm sieben Leuchter und Weihrauch vorweggetragen. <br>
              Der Einzug muss dabei vom Altar aus gesehen werden: Das ganze Volk
              geht auf den Altar, also auf Gott zu. Die Gemeinde beteiligt sich
              durch den Gesang. Die Mitwirkenden am Gottesdienst sollen mit dem
              Priester einziehen, ihnen wird ein Kreuz vorangetragen. </font></p>
            <p><font face="Arial, Helvetica, sans-serif">Eingangsprozession und
                Kyrie sind besondere Themen der <a href="http://www.kath.de/lexikon/medien_oeffentlichkeit/mystagogische_Bildregie.php">mystagogischen Bildregie</a></font></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Verehrung des Altars:</strong><br>
              Der Priester k&uuml;sst den Altar nach dem Einzug. Die in diesem
                  Vorbereitungsritus gezeigte Verehrung gilt Christus, der durch
                  den Altar symbolisiert wird. Noch im 13. Jahrhundert war es &uuml;blich,
                  den Altar 21 mal zu k&uuml;ssen. Dabei k&ouml;nnen der Altar und
                  das Kreuz auch mit Weihrauch inzensiert werden. Das meint, dass
                  der Priester diese mit dem Weihrauchfa&szlig; ber&auml;uchert.
                  Dies findet sich seit dem 11. Jahrhundert. Die Verwendung von Weihrauch
                  im Gottesdienst ist fakultativ, so dass es mancherorts zur Abschaffung
                  kam, anderenorts zu einer fast t&auml;glichen Verwendung von
                Weihrauch im Gottesdienst. </font></p>            <p><font face="Arial, Helvetica, sans-serif"><strong>Eingangsgru&szlig;</strong><br>
  Nach dem Einzug spricht der Vorsteher &#8222;Im
  Namen des Vaters, des Sohnes und des heiligen Geistes&#8220;. Das dabei von
  dem Priester und den Gl&auml;ubigen gemachte Kreuzzeichen ist seit dem 14.
  Jahrhundert belegt. Danach spricht der Priester &#8222;Der Herr Sei mit euch&#8220; (lat.
              Dominus vobiscum). Diese Formel ist eine Zitation aus Rut 2,4.
              Sie war bei den Juden eine g&auml;ngige Gru&szlig;formel. Wenn
              der Priester dabei die Arme ausbreitet, soll das eine Umarmung
              andeuten. <br>
              Die Gemeinde antwortet mit dem Spruch &#8222;Und mit deinem Geiste&#8220; (lat.
              Et cum spiritu tuo). Dies k&ouml;nnte den Sinn von &#8222;Und auch
              mit dir&#8220; haben. Wahrscheinlicher ist jedoch, da&szlig; mit
              dem Wort Geist hier die Gabe des Heiligen Geistes gemeint, die
              dem Priester als Amtstr&auml;ger gegeben ist. Dementsprechend
              geht es nicht um die Kopie eines b&uuml;rgerlichen Begr&uuml;&szlig;ungsrituals,
              sondern um eine erste Hinf&uuml;hrung in das Mysterium, das in
              der Messe gefeiert werden soll.</font></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Bu&szlig;akt &#8211; auch:
                Allgemeines Schuldbekenntnis</strong><br>
  Wer sich Gott n&auml;hert, wird sich bewu&szlig;t, da&szlig; er
              daf&uuml;r nicht vorbereitet ist, Fehlhaltungen, Nachl&auml;ssigkeiten
              und bewu&szlig;te Verletzung anderer Menschen stehen der Begegnung
              im Wege. Im Schuldbekenntnis stehen die Feiernden zu ihren Fehlern
              und lassen sich vom Priester die von Gott gew&auml;hrte Vers&ouml;hnung
              zusprechen. Der Bu&szlig;akt kann bei Messen von besonderer Festlichkeit
              entfallen. </font></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Kyrie</strong><br>
  Der Kyrie-Ruf (griechisch f&uuml;r Herr)  soll zeigen, dass Christus
              in der Gemeinde zugegen ist als Retter. Kyrie eleison / Christe
              eleison &#8211; Herr errette uns / Christus errette uns lautet
              der Kyrie Ruf. In vorchristlicher Zeit war der Kyrie Ruf eine Huldigung
              an einen Gott oder einen Herrscher. Zugleich ist der Kyrios Ruf
              eine der k&uuml;rzesten Glaubensformeln. Christus ist der auferstandene
              Herr (vgl. Philipperhymnus 2,6-11). Kyrios geht zur&uuml;ck auf
              den alttestamentlichen Gottestitel Adonai, mit dem der unausgesprochene
              Gottesname JHWH in den Texten des Alten Testamentes umschrieben
              wurde. Um 500 &uuml;bernahm man in Rom die Kyrielitanei aus dem
              Osten. Dabei wurden die Anliegen der Gemeinde von dem Diakon vorgetragen
              und die Gemeinde antwortete mit dem Kyrie eleison. Unter Papst
              Gregor I. (+ 604) wurden die Anliegen dann weggelassen. Der nun &uuml;brig
              gebliebene Kyrie Ruf wurde im Laufe der Zeit noch um das Christe
              Eleison erweitert. Bis zur Liturgiereform des II. Vatikanischen
              Konzils gab es neun Kyrierufe. <br>
              Urspr&uuml;nglich war die Zahl der Kyrierufe nicht begrenzt, es
              wurde n&auml;mlich so lange gesungen, bis der Papst an seinem Sitz
              angelangt war. Dann gab er den S&auml;ngern ein Zeichen. Der Neunzahl
              (3 Wiederholungen eines dreifachen Kyrierufes) begegnen wir zum
              ersten Mal in Gallien. Im Mittelalter werden die Kyrierufe dann
              zunehmend trinitarisch gedeutet, d.h. das erste Kyrie gilt Gott
              Vater, das Christe eleison dem Sohn, das dritte Kyrie dem Heiligen
              Geist.<br>
              <br>
              <strong>Gloria</strong><br>
              Das Gloria ist einer der wenigen &uuml;berlieferten Hymnen mit
              griechischem Ursprung. Durch das Konzil von Laodiz&auml;a (zwischen
              341 und 380 n.Chr.) wurden die meisten Hymnen verboten. Das Gloria
              muss damals schon so angesehen gewesen sein, dass es nicht diesem
              Verbot zum Opfer fiel. Gloria meint &uuml;bersetzt soviel wie Ruhm,
              Glanz, Herrlichkeit (hebr&auml;isch. kabod, griechisch Doxa) <br>
              Es ist somit ein Lobgesang auf die Herrlichkeit Gottes. Anf&auml;nglich
              war es kein Bestandteil der Messe, wurde aber schon sehr fr&uuml;h
              in die Mitternachtsmesse von Weihnachten aufgenommen. Noch im 9.Jahrhundetr
              beteten die Priester es nur in der Osternacht und bei der Besitzergreifung
              ihrer Kirche. Seit dem 11. Jahrhundert wird es dann an allen Festtagen
              und Sonntagen gebetet.</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Lateinischer Text des
                Gloria:<br>
              Gloria in excelsis Deo et in terra pax hominibus bonae voluntatis. <br>
              Laudamus te, benedicimus te, adoramus te, glorificamus te,<br>
              gratias agimus tibi propter magnam gloriam tuam, <br>
              Domine Deus, Rex caelestis Deus Pater omnipotens, <br>
              Domine Fili unigenite, Iesu Christe, Domine Deus, Agnus Dei, <br>
              Filius Patris, qui tollis peccata mundi, miserere nobis; qui tollis
              peccata mundi, <br>
              suscipe deprecationem nostram. <br>
              Qui sedes ad dexteram Patris, miserere nobis. <br>
              Quoniam tu solus Sanctus, tu solus Dominus, tu solus Altissimus, <br>
              Iesu Christe, cum Sancto Spiritu: in gloria Dei Patris. Amen.</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Deutsche Fassung:<br>
  Ehre sei Gott in der H&ouml;he und Friede auf Erden den Menschen
              seiner Gnade. <br>
              Wir loben dich, wir preisen dich, wir beten dich an, wir r&uuml;hmen
              dich und danken dir, <br>
              denn gro&szlig; ist deine Herrlichkeit: <br>
              Herr und Gott, K&ouml;nig des Himmels, Gott und Vater, Herrscher &uuml;ber
              das All, <br>
              Herr, eingeborener Sohn, Jesus Christus.<br>
              Herr und Gott, Lamm Gottes, Sohn des Vaters, du nimmst hinweg die
              S&uuml;nde der Welt: erbarme dich unser; <br>
              du nimmst hinweg die S&uuml;nde der Welt: nimm an unser Gebet; <br>
              du sitzest zur Rechten des Vaters: erbarme dich unser. <br>
              Denn du allein bist der Heilige, du allein der Herr, du allein
              der H&ouml;chste, Jesus Christus, mit dem Heiligen Geist, zur Ehre
              Gottes des Vaters. Amen.</font></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Tagesgebet</strong><br>
  Auf lateinisch hei&szlig;t Gebet collecta. Es schlie&szlig;t den
              Er&ouml;ffnungsteil der Messe ab. Collectare meint sammeln, ein
              Gebet, das der Priester &uuml;ber die versammelte Gemeinde ausspricht.
              Zum anderen bedeutet es auch ein Gebet, in dem der Priester die
              Anliegen der Gemeinde sammelt und zusammenfa&szlig;t. Begonnen
              wird das Tagesgebet mit dem lateinischen Oremus (Beten wir bzw.
              lasset uns beten). Es folgt ein kurzer Moment des Schweigens, um
              sich zu sammeln. (In den Bu&szlig;zeiten fordert der Diakon zum
              Niederknien auf: Flectamus genua - beuget die Knie) Das Gebet selbst
              singt der Priester in der althergebrachten Gebetshaltung: Mit erhobenen
              H&auml;nden und aufrecht stehend. Diese Zusammenfassung hei&szlig;t
              auch Oration. <br>
              Das Gebet richtet der Priester an Gott Vater, in der Schlu&szlig;formel
              werden immer die drei Personen der Trinit&auml;t, Vater, Sohn und
              Heiliger Geist erw&auml;hnt. <br>
              Das Tagesgebet wechselt je nach Fest- oder Wochentag bzw. Heiligengedenktag,
              ebenso wie das Gebet am Ende der Gabenbereitung und das nach der
              Kommunionausteilung. Diese drei Gebete werden auch Pr&auml;sidial-
              Vorstehergebete genannt, weil der Priester als Vorsteher sie im
              Namen der Gemeinde
              an Gott richtet. </font></p>
            <p><font face="Arial, Helvetica, sans-serif">Das Tagesgebet vom 1. Adventssonntag<br>
              Lasset uns beten:<br>
              Her, unser Gott,<br>
              alles steht in deiner Macht;<br>
              du schenkst das Wollen und das Vollbringen.<br>
              Hilf uns, da&szlig; wir auf dem Weg der Gerechtigkeit Christus
              entgegengehen<br>
              und uns durch Taten der Liebe auf seine Ankunft vorbereiten,<br>
              damit wir den Platz zu seiner Rechten erhalten, <br>
              wenn er wiederkommt in Herrlichkeit.<br>
              Er, der in der Einheit des Heiligen Geistes<br>
              Mit dir lebt und herrscht in Ewigkeit.<br>
              Amen</font></p>
            <p><font size="2" face="Arial, Helvetica, sans-serif">J&uuml;rgen Pelzer</font></p>
            <p><font size="3" face="Arial, Helvetica, sans-serif"><a href="messe_aufbau.php">Der
                  Gesamtaufbau der Messe</a> </font></p>
            <p><font size="3" face="Arial, Helvetica, sans-serif">Hinweise zu
                den aktuellen <a href="http://www.fernsehgottesdienst.de/">&Uuml;bertragungen im ZDF</a></font></p>
            <p><font size="2" face="Arial, Helvetica, sans-serif"><a href="http://www.kath.de/">&copy; www.kath.de</a></font></p>
          </td>
          <td class="L12" width="1">            <div align="right"><br>
              <br>
              </div>
          </td>
          <td background="boxright.gif" width="9"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif" colspan="2"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td width="9"><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
</table>
</body>
</html>
