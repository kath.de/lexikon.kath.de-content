<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title>Wie werden Bibelstellen abgek&uuml;rzt</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="kaltefleiter.css" title="fonts">
</head>
<body bgcolor="#ffffff" leftmargin="6" topmargin="6" marginwidth="6" marginheight="6">
<table width="100%" border="0" cellspacing="0" cellpadding="6">
  <tr>
    <td width="100" align="left" valign="top" height="3"> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b><font face="Arial, Helvetica, sans-serif">Das 
            Liturgie Lexikon</font></b></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td> <?php include("logo.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><font face="Arial, Helvetica, sans-serif"><strong>Inhaltsverzeichnis 
            </strong></font></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10"> <?php include("az.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
    <td rowspan="2" valign="top">
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td background="boxtop.gif" colspan="2"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif" width="8"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2" colspan="2"> 
            <h1><font face="Arial, Helvetica, sans-serif">Bibelstellen-Abk&uuml;rzung</font></h1>
          </td>
          <td background="boxtopright.gif" width="8"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif" colspan="2"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td width="8"><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif" width="8"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="L12" width="70%"> 
            <p><br>
              <font face="Arial, Helvetica, sans-serif"><strong>Wie
            Bibelstellen abgek&uuml;rzt werden:</strong></font></p>
            <p><font face="Arial, Helvetica, sans-serif">Wundern Sie sich auch
                manchmal &uuml;ber diese komischen Angaben:
              Ex 32,4 ?<br>
            </font><font face="Arial, Helvetica, sans-serif"><br>
              Anders als bei den meisten B&uuml;chern werden Bibelstellen nicht
                nach Seitenzahlen abgek&uuml;rzt, sondern nach einem eigenen
                System: Da die Bibel aus 39 B&uuml;chern im Alten Testament und
                27 B&uuml;cher im Neuen Testament, also insgesamt 66 B&uuml;chern
                besteht, wird zun&auml;chst das Buch genannt. Diese Nennung folgt
                einem weltweit festgelegten Abk&uuml;rzungsverzeichnis (<a href="http://www.die-bibel-lebt.de/stellen.htm" target="_blank">das
                sie hier aufrufen k&ouml;nnen</a>). So steht in obigem Beispiel die Abk&uuml;rzung
                Ex etwa f&uuml;r das Buch Exodus. <br>
                <br>
              Jedes Buch der Bibel ist wiederum
                in einzelnen Kapitel eingeteilt. Die Zahl des entsprechenden
                Kapitels findet sich hinter dem Buchk&uuml;rzel und vor dem Komma.
                In unserem Beispiel Ex 32,4 w&auml;re es also das 32. Kapitel
                des Buches Exodus.</font></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Kleines Komma &#8211; gro&szlig;e
                Wirkung</strong></font></p>
            <p><font face="Arial, Helvetica, sans-serif">Die einzelnen Kapitel
                der einzelnen B&uuml;cher der Bibel sind
              wiederum in sog. Verse eingeteilt. Dabei kann ein Vers aus einem
              Satz oder auch aus mehreren S&auml;tzen bestehen. Die L&auml;nge
              variiert. Das Komma in der Abk&uuml;rzung k&uuml;ndigt an, das
              die Nennung des Kapitels beendet ist und nun die Nennung der Verse
              beginnt: Ex 32,4 meint also Vers 4 im 32. Kapitel des Buches Exodus.
              Es k&ouml;nnen auch mehrere Verse genannt werden, z.B. Ex 32,4-6.
              Diese Bibelstelle w&uuml;rde also die Verse 4, 5 und 6 des 32.
              Kapitels des Buches Exodus umfassen. Manchmal findet sich noch
              ein kleines f. in der Angabe: Ex 32, 4f. Das f. steht f&uuml;r
              folgender und meint, dass der folgende Vers auch noch zur betreffenden
              Stelle
              geh&ouml;rt. Es handelt sich also um die beiden Verse 4 und 5 des
            32. Kapitels des Exodusbuches. </font>                         </p>
            <p><font face="Arial, Helvetica, sans-serif">Eine sehr empfehlenswerte
                inhaltliche Einf&uuml;hrung in die Bibel finden Sie bei J&ouml;rg Sieger: <a href="http://www.joerg-sieger.de/einleit.htm" target="_blank">http://www.joerg-sieger.de/einleit.htm</a><br>
              <br>
              Feedback bitte an mit 
              dem Stichwort Liturgie-Lexikon an: redaktion@kath.de</font></p>
          </td>
          <td class="L12" width="30%"> 
            <div align="right"> 
              <p><br>
                <br>
                <font size="2" face="Arial, Helvetica, sans-serif"><br>
                </font></p>
            </div>
          </td>
          <td background="boxright.gif" width="8"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif" colspan="2"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
<td width="8"><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
 </tr> 
</table> 
</td>
<!-- neuer Code Anfang -->
<td style='vertical-align:top;'> 
     <script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
/* Symbollexikon neu */
google_ad_slot = "9464926645";
google_ad_width = 120;
google_ad_height = 600;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
     <br>
</td>
<!-- neuer Code Ende -->

  </tr>
  <tr>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
</table>
</body>
</html>
