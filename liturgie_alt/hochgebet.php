<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<meta name="title" content="Liturgie">
<meta name="author" content="Redaktion kath.de">
<meta name="publisher" content="kath.de">
<meta name="copyright" content="kath.de">
<meta name="description" content="">
<meta name="abstract" content="Lexikon �ber die Liturgie der katholischen Kirche">
<meta http-equiv="content-language" content="de">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="date" content="2006-00-01">
<meta name="robots" content="index,follow">
<meta name="revisit-after" content="10 days">
<meta name="revisit" content="after 10 days">
<meta name="DC.Title" content="Liturgie">
<meta name="DC.Creator" content="Redaktion kath.de">
<meta name="DC.Contributor" content="J�rgen Pelzer">
<meta name="DC.Rights" content="kath.de">
<meta name="DC.Publisher" content="kath.de">
<meta name="DC.Date" content="2006-00-01">
<meta name="DC.Description" content="Lexikon �ber die Liturgie der katholischen Kirche">
<meta name="DC.Language" content="de">
<meta name="DC.Type" content="Text">
<meta name="DC.Format" content="text/html">
<meta name="DC.Identifier" content="http://www.kath.de/lexikon/liturgie/">
<meta name="keywords" lang="de" content="Abednmahl, Paschahfest, Geschichte, Anamnese, Epiklese, Hippolyt">
<title>Hochgebet </title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="kaltefleiter.css" title="fonts">
</head>
<body bgcolor="#ffffff" leftmargin="6" topmargin="6" marginwidth="6" marginheight="6">
<table width="100%" border="0" cellspacing="0" cellpadding="6">
  <tr>
    <td width="100" align="left" valign="top" height="3"> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b><font face="Arial, Helvetica, sans-serif">Das 
            Liturgie Lexikon</font></b></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td> <?php include("logo.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><font face="Arial, Helvetica, sans-serif"><strong>Inhaltsverzeichnis 
            </strong></font></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10"> <?php include("az.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
    <td rowspan="2" valign="top">
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td background="boxtop.gif" colspan="2"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="9"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif" width="8"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2" colspan="2"> 
            <h1><font face="Arial, Helvetica, sans-serif">Hochgebet </font></h1>
          </td>
          <td background="boxtopright.gif" width="9"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif" colspan="2"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td width="9"><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif" width="8"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="L12" width="516"> 
            <p><strong><font face="Arial, Helvetica, sans-serif">Das zentrale
                  Gebet der Messe</font></strong></p>
            <p><font face="Arial, Helvetica, sans-serif">Im Zentrum der<a href="eucharistie_messe_abendmahl.php"> Eucharistiefeier</a>              steht ein l&auml;ngeres Gebet,
              in das die Einsetzungsworte eingef&uuml;gt sind. Diese Worte haben
              ihre Bezeichnung davon, da&szlig; Jesus mit ihnen das Mahl &#8222;eingesetzt&#8220; hat,
              das zu seinem Ged&auml;chtnis gefeiert wird. Mit den Einsetzungsworten
              bezeichnet Jesu das Brot als seinen Leib und den Wein als sein
              Blut. Damit hat er den Auftrag verbunden, das Mahl zu seinem Ged&auml;chtnis
              zu feiern. So bleibt Jesu durch seine Worte, die im Wortgottesdienst
              vorgelesen werden, sowie durch die Mahlfeier, in der der Priester
              seine Einsetzungsworte spricht, gegenw&auml;rtig. Das Mahl, das
              Jesus mit den J&uuml;ngern gefeiert hatte, war sein letztes. Jesus
              hatte sich anl&auml;&szlig;lich des Passahfestes in einer Jerusalemer
              Wohnung mit den 12 Aposteln zusammengefunden und innerhalb des
              j&uuml;dischen Mahles die Worte &uuml;ber das Brot und den Becher
              mit Wein gesprochen und diese dann herumgereicht. An die Mahlfeier
              erinnert der Gr&uuml;ndonnerstag. Bereits das j&uuml;dische Mahl
              zum Passahfest war ein Ged&auml;chtnismahl, das an den Auszug der
              Juden aus &Auml;gypten, an die Befreiung aus der Sklaverei erinnerte.
              Gott wurde f&uuml;r seine Rettungstat gedankt, es gab Lobges&auml;nge
              und auch bereits das Halleluja. Im Johannesevangelium sind im Zusammenhang
              mit dem Bericht von dem Mahl zum Passahfest die sog. Abschiedsreden
              Jesu aufgezeichnet. Sie f&uuml;gen sich auch in die j&uuml;dische
              Tradition ein, da&szlig; nicht nur gegessen, sondern &uuml;ber
              zentrale Fragen gesprochen wurde. Jesus hat nicht nur das Mahl
              gefeiert, sondern es auch erkl&auml;rt und sich dabei an die j&uuml;dische
              Grundstruktur des Lobes und Dankes gehalten. Im Markusevangelium
              hei&szlig;t es: &#8222;W&auml;hrend des Mahls nahm er das Brot
              und sprach den Lobpreis &#8230; Dann nahm er den Kelch und sprach
              das Dankgebet. &#8230;&#8220; Kap 14,22, 23 Die Texte zeigen auch,
              da&szlig; er dieses letzte Mahl mit seiner Sendung verbunden hat.
              So hei&szlig;t es auch bei Markus: &#8222;Und er sagte zu ihnen:
              Das ist mein Blut, das Blut des Neues Bundes, das f&uuml;r viele
              vergossen wird. Amen ich sage euch: Ich werde nicht mehr von der
              Frucht des Weinstocks trinken bis zu dem Tag, an dem ich von neuem
              davon trinken werde im Reich Gottes.&#8220; Kap. 14, 24-25<br>
              Die Christen konnten an diese Gebetsstruktur ankn&uuml;pfen. Sie
              haben nicht nur die in der j&uuml;dischen Mahlfeier entwickelten
              Elemente &uuml;bernommen, sondern auch die Beziehungsstruktur des
              Gebetes. Der Priester richtet es an Gott Vater, ihm wird gedankt,
              ihm gegen&uuml;ber wird das Lob zum Ausdruck gebracht. Diese Beziehungsstruktur
              findet sich in den Gebeten, die das Johannesevangelium von der
              Abendmahlsfeier &uuml;berliefert. (s.u. bei Zitaten).<br>
              Der Priester dankt Gott f&uuml;r die Erl&ouml;sung durch Christus,
              die lobt Gott, vor allem im Sanctus, sie erinnert sich an den Tod
              Jesu, an seine Auferstehung und Himmelfahrt und erfahren sich mit
              dem Heiligen Geist begabt. Dank, Lob und Erinnerung m&uuml;nden
              bereits in der j&uuml;dischen Mahlfeier in Bitten f&uuml;r das
              Volk und andere Anliegen. Diese Elemente finden sich in den Liturgien
              des Ostens wie des Westens. Das Hochgebet der r&ouml;mischen Kirche
            hat in der Regel folgenden Aufbau:</font></p>
            <p><font face="Arial, Helvetica, sans-serif">1.	Pr&auml;fation<br>
              2.	Sanctusgesang<br>
              3.	Herabrufung des Heiligen Geistes auf Brot und Wein (Epiklese)<br>
              4.	Einsetzungsworte<br>
              5.	Erinnerung an Jesu Tod und Auferstehung (Anamnese)<br>
              6.	Gebet f&uuml;r die Kirche, um die Verbindung mit dem Papst und
              dem Ortsbischof auszudr&uuml;cken, Papst und Bisch&ouml;fe<br>
              7.	F&uuml;rbitten f&uuml;r Lebende und Verstorbene (Interncessiones)<br>
              8.	Feierlicher Abschlu&szlig; des Hochgebetes (Doxologie</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Diese Teile des Hochgebetes
                finden sich in einem liturgischen Text, der dem Priester Hippolyt
                zugeschrieben wird. Er hat die
              liturgischen Texte aufgeschrieben, die zu seiner Zeit, in der zweiten
              H&auml;lfte des 3. Jahrhunderts, in der r&ouml;mischen Kirche in
              Gebrauch waren. Das heute in der katholischen Kirche benutzte II.
              Hochgebet geht auf diesen alten Text der r&ouml;mischen Kirche
              zur&uuml;ck. In seiner klaren Gliederung wird die oben beschriebene
              Grundstruktur erkennbar:</font></p>
            <p><font size="2" face="Arial, Helvetica, sans-serif">Einladung zur
                Pr&auml;fation:</font><font face="Arial, Helvetica, sans-serif"><br>
              Priester:	Der Herr sei mit euch<br>
              Gemeinde:	Und mit deinem Geiste<br>
              Priester:	Erhebet die Herzen.<br>
              Gemeinde:	Wir haben sie beim Herrn<br>
              Priester:	Lasset uns danken dem Herrn, unserem Gott <br>
              Gemeinde:	Das ist w&uuml;rdig und recht</font></p>
            <p><font size="2" face="Arial, Helvetica, sans-serif">Pr&auml;fation</font><font face="Arial, Helvetica, sans-serif"><br>
  In Wahrheit ist es w&uuml;rdig und recht, <br>
  dir, Herr, heiliger Vater, immer und &uuml;berall zu danken,<br>
              durch deinen geliebten Sohn Jesus Christus.<br>
              Er ist dein Wort, durch ihn hast du alles erschaffen. <br>
              Ihn hast du gesandt als unseren Erl&ouml;ser und Heiland.<br>
              Er ist Mensch geworden durch den Heiligen Geist, <br>
              geboren von der Jungfrau Maria.<br>
              Um deinen Ratschlu&szlig; zu erf&uuml;llen <br>
              und dir ein heiliges Volk zu erwerben, <br>
              hat er sterbend die Arme ausgebreitet <br>
              am Holze des Kreuzes. <br>
              Er hat die Macht des Todes gebrochen <br>
              und die Auferstehung kundgetan.<br>
              Darum preisen wir dich mit allen Engeln <br>
              und Heiligen und singen vereint <br>
              mit ihnen das Lob deiner Herrlichkeit:</font></p>
            <p><font size="2" face="Arial, Helvetica, sans-serif">Sanctus</font><font face="Arial, Helvetica, sans-serif"><br>
              Heilig, heilig, heilig <br>
              Gott, Herr aller M&auml;chte und Gewalten.<br>
              Erf&uuml;llt sind Himmel und Erde von deiner Herrlichkeit.<br>
              Hosanna in der H&ouml;he.<br>
              Hochgelobt sei, der da kommt im Namen des Herrn.<br>
              Hosanna in der H&ouml;he.</font></p>
            <p><font size="2" face="Arial, Helvetica, sans-serif">Epiklese</font><font face="Arial, Helvetica, sans-serif"><br>
  Ja, du bist heilig, gro&szlig;er Gott, <br>
              du bist der Quell aller Heiligkeit.<br>
              Darum bitten wir dich:<br>
              Sende deinen Geist auf diese Gaben herab und heilige sie, <br>
              damit sie uns werden Leib und Blut deines Sohnes, <br>
              unseres Herrn Jesus Christus.</font></p>
            <p><font size="2" face="Arial, Helvetica, sans-serif">Einsetzungsworte</font><font face="Arial, Helvetica, sans-serif"><br>
              Denn am Abend, an dem er ausgeliefert wurde und sich aus freiem
                Willen <br>
              dem Leiden unterwarf, <br>
              nahm er das Brot und sagte Dank, brach es, <br>
              reichte es seinen J&uuml;ngern und sprach:</font></p>
            <p><font face="Arial, Helvetica, sans-serif">NEHMET UND ESSET ALLE DAVON: <br>
              DAS IST MEIN LEIB, <br>
              DER F&Uuml;R EUCH HINGEGEBEN WIRD.</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Ebenso nahm er nach dem Mahl den Kelch, <br>
  dankte wiederum, reichte ihn seinen J&uuml;ngern und sprach:</font></p>
            <p><font face="Arial, Helvetica, sans-serif">NEHMET UND TRINKET ALLE DARAUS: <br>
              DAS IST DER KELCH DES NEUEN UND EWIGEN BUNDES,<br>
              MEIN BLUT, DAS F&Uuml;R EUCH UND F&Uuml;R ALLE VERGOSSEN WIRD ZUR
              VERGEBUNG DER S&Uuml;NDEN.<br>
              TUT DIES ZU MEINEM GED&Auml;CHTNIS.</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Geheimnis des Glaubens.<br>
                <font size="2">Akklamation der Gemeinde:</font><br>
              Deinen Tod, o Herr, verk&uuml;nden wir, <br>
              und deine Auferstehung preisen wir, <br>
              bis du kommst in Herrlichkeit.</font></p>
            <p><font size="2" face="Arial, Helvetica, sans-serif">Anamnese</font><font face="Arial, Helvetica, sans-serif"><br>
  Darum, g&uuml;tiger Vater, feiern wir das Ged&auml;chtnis des Todes
              und der Auferstehung deines Sohnes und bringen dir so das Brot
              des Lebens und den Kelch des Heiles dar. <br>
              Wir danken dir, da&szlig; du uns berufen hast, <br>
              vor dir zu stehen und dir zu dienen.<br>
              Wir bitten dich: Schenke uns Anteil an Christi Leib und Blut, <br>
              und la&szlig; uns eins werden durch den Heiligen Geist.</font></p>
            <p><font size="2" face="Arial, Helvetica, sans-serif">Gebet f&uuml;r
                Papst, Bischof und die Kirche</font><font face="Arial, Helvetica, sans-serif"><br>
              Gedenke deiner Kirche auf der ganzen Erde <br>
              und vollende dein Volk in der Liebe, <br>
              vereint mit unserem Papst N., <br>
              unserem Bischof N. und allen Bisch&ouml;fen,<br>
              unseren Priestern und Diakonen <br>
              und mit allen, die zum Dienst in der Kirche bestellt sind.</font></p>
            <p><font size="2" face="Arial, Helvetica, sans-serif">Gebet f&uuml;r
                die Verstorbenen</font><font face="Arial, Helvetica, sans-serif"><br>
  Gedenke (aller) unsere Br&uuml;der und Schwestern, <br>
              die entschlafen sind in der Hoffnung, das sie auferstehen. <br>
              Nimm sie und alle, die in deiner Gnade aus dieser Welt geschieden
              sind, in dein Reich auf, wo sie dich schauen von Angesicht zu Angesicht.</font></p>
            <p><font size="2" face="Arial, Helvetica, sans-serif">Gebet f&uuml;r
                die Lebenden</font><font face="Arial, Helvetica, sans-serif"><br>
  Vater, erbarme dich &uuml;ber uns alle, damit uns das ewige Leben
              zuteil wird in der Gemeinschaft mit der seligen Jungfrau und Gottesmutter
              Maria, mit deinen Aposteln und mit allen, die bei dir Gnade gefunden
              haben von Anbeginn der Welt, dass wir dich loben und preisen durch
              deinen Sohn Jesus Christus.</font></p>
            <p><font size="2" face="Arial, Helvetica, sans-serif">Schlu&szlig;doxologie</font><font face="Arial, Helvetica, sans-serif"><br>
              Durch ihn und mit ihm und in ihm ist dir, <br>
              Gott, allm&auml;chtiger Vater,<br>
              in der Einheit des Heiligen Geistes <br>
              alle Herrlichkeit und Ehre jetzt und in Ewigkeit.<br>
              Amen.</font></p>
            <p><font face="Arial, Helvetica, sans-serif"><a href="messe_aufbau.php">Zum
            Gesamtaufbau der Messe</a> </font></p>
            <p><font face="Arial, Helvetica, sans-serif"><br>
                <strong>Zitate</strong><br>
              Von Johannes &uuml;berliefertes Gebet Jesu beim Letzten Abendmahl:<br>
              Und Jesus erhob seine Augen zum Himmel und sprach:<br>
              Vater, die Stunde ist da. Verherrliche deinen Sohn, damit der
                Sohn dich verherrlicht. Denn du hast ihm Macht &uuml;ber alle Menschen
                gegeben, damit er allen, die du ihm gegeben hast, ewiges Leben
                schenkt. Das ist das ewige Leben: dich, den einzigen wahren Gott,
                zu erkennen und Jesus Christus, den du gesandt hast. Ich habe dich
                auf Erden verherrlicht und das Werk zu Ende gef&uuml;hrt, das
                du mir aufgetragen hast. Vater, verherrliche mich jetzt bei dir
                mit
                der Herrlichkeit, die ich hatte, bevor die Welt war. <br>
              Kap. 17,1-5</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Auf diesen Lobpreis
                folgt ein Gebet Jesu f&uuml;r die J&uuml;nger,
              das dem Gebet f&uuml;r die Kirche, die Gl&auml;ubigen und die Verstorbenen
              in den verschiedenen Hochgebeten entspricht.</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Hinweise zu den <a href="http://www.fernsehgottesdienst.de/">Gottesdienst&uuml;bertragungen im ZDF</a></font></p>
            <p><font size="2" face="Arial, Helvetica, sans-serif"><a href="http://www.kath.de/">&copy; www.kath.de</a></font></p>
          </td>
          <td class="L12" width="1">            <div align="right"><br>
              <br>
              </div>
          </td>
          <td background="boxright.gif" width="9"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif" colspan="2"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td width="9"><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
</table>
</body>
</html>
