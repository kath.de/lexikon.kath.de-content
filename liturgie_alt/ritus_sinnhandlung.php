<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<meta name="title" content="Liturgie">
<meta name="author" content="Redaktion kath.de">
<meta name="publisher" content="kath.de">
<meta name="copyright" content="kath.de">
<meta name="description" content="">
<meta name="abstract" content="Lexikon �ber die Liturgie der katholischen Kirche">
<meta http-equiv="content-language" content="de">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="date" content="2006-00-01">
<meta name="robots" content="index,follow">
<meta name="revisit-after" content="10 days">
<meta name="revisit" content="after 10 days">
<meta name="DC.Title" content="Liturgie">
<meta name="DC.Creator" content="Redaktion kath.de">
<meta name="DC.Contributor" content="J�rgen Pelzer">
<meta name="DC.Rights" content="kath.de">
<meta name="DC.Publisher" content="kath.de">
<meta name="DC.Date" content="2006-00-01">
<meta name="DC.Description" content="Lexikon �ber die Liturgie der katholischen Kirche">
<meta name="DC.Language" content="de">
<meta name="DC.Type" content="Text">
<meta name="DC.Format" content="text/html">
<meta name="DC.Identifier" content="http://www.kath.de/lexikon/liturgie/">
<meta name="keywords" lang="de" content="Sinnhanldung, Lebensfragen, existetiell, Religion">
<title>Ritus als Sinnhandlung</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="kaltefleiter.css" title="fonts">
</head>
<body bgcolor="#ffffff" leftmargin="6" topmargin="6" marginwidth="6" marginheight="6">
<table width="100%" border="0" cellspacing="0" cellpadding="6">
  <tr>
    <td width="100" align="left" valign="top" height="3"> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b><font face="Arial, Helvetica, sans-serif">Das 
            Liturgie Lexikon</font></b></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td> <?php include("logo.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><font face="Arial, Helvetica, sans-serif"><strong>Inhaltsverzeichnis 
            </strong></font></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10"> <?php include("az.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
    <td rowspan="2" valign="top">
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top"> 
          <td width="8" align="left"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td colspan="2" align="left" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="9" align="left"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top"> 
          <td width="8" align="left" background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td colspan="2" align="left" bgcolor="#E2E2E2"> 
            <h1><font face="Arial, Helvetica, sans-serif">Ritus als Sinnhandlung</font></h1>
          </td>
          <td width="9" align="left" background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top"> 
          <td width="8" align="left"><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td colspan="2" align="left" background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td width="9" align="left"><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top"> 
          <td width="8" align="left" background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td width="516" align="left" class="L12"> 
            <p><strong><font face="Arial, Helvetica, sans-serif">Riten, antworten
                  auf existenzielle Lebensfragen</font></strong></p>
            <p><font face="Arial, Helvetica, sans-serif">Das Leben steckt voller
                M&ouml;glichkeiten, vor allem wenn es
              aus einer zwischenmenschlichen Perspektive gesehen wird. Jede Situation
              besitzt das Potential, Ausgangssituation und Anlass einer ganz
              neuen Entwicklung oder Geschichte zu sein. Deutlich wird dies beispielsweise,
              wenn man einen Menschen neu kennen lernt. Es entstehen Fragen:
              Wie ist er mir gesonnen? Wird er mein Feind oder mein Freund werden,
              erw&auml;chst Interesse oder Gleichg&uuml;ltigkeit, entsteht so
              etwas wie Liebe auf den ersten Blick oder Abneigung? Wie soll ich
              mein Verhalten ausrichten?<br>
              Solche Fragen bleiben meistens im Unterbewusstsein stecken. Nur
              selten werden sie aufgegriffen und bewusst bearbeitet. Die Suche
              nach einer Antwort auf solche Fragen ist aufw&auml;ndig. Die Antwort
              kann weder die Wissenschaft noch ein Fachmann geben.<br>
              In gewissen Lebenssituationen gewinnen solche existentiellen Fragen
              eine besondere Dringlichkeit. Dann z. B. wenn es darum geht, das
              Kennenlernen von Menschen schnell zu bew&auml;ltigen. Es fehlt
              in den meisten F&auml;llen die Zeit und die M&ouml;glichkeit, einen
              Menschen gr&uuml;ndlich, d.h. auf &#8222;nat&uuml;rlichem&#8220; Weg
              kennen zu lernen. Dazu w&uuml;rde geh&ouml;ren: die Sinneseindr&uuml;cke,
              die er verursacht, wahrzunehmen, zu ordnen und zu bewerten. Dazu
              w&uuml;rde auch geh&ouml;ren, gegenseitig so weit in die Lebensgeschichten
              und in die Charaktere einzudringen, dass schlie&szlig;lich je ein
              pers&ouml;nliches und ein gemeinsames Urteil zustande k&auml;me,
              welches die zuk&uuml;nftige Form der Kommunikation und der Zusammenarbeit
              regelt. Dies ist in den seltensten F&auml;llen m&ouml;glich. Doch
              die kulturelle Tradition h&auml;lt f&uuml;r solche F&auml;lle Verhaltensmuster
              bereit. <br>
              In einer festgelegten Zeichenhandlung werden die Werte vermittelt
              und in Szene gesetzt, die im jeweiligen Kulturkreis f&uuml;r das
              Gelingen eines guten Starts von Beziehung als notwenig erachtet
              werden. In unserem Kulturkreis sind dies die Bereitschaft, Aufmerksamkeit
              und Achtung zu schenken, sich an den H&auml;nden ber&uuml;hren
            zu lassen und zu ber&uuml;hren, miteinander wohlwollend zu reden.<br>
            </font><font face="Arial, Helvetica, sans-serif">An
            diesem Beispiel wird deutlich: Es gibt Handlungen, deren ausschlie&szlig;liches
              Ziel es ist, Antworten auf existentielle Lebensfragen herbei zu
              f&uuml;hren. Solche Handlungen verfolgen nicht unmittelbar funktionale
              Zwecke, Befriedigung biologischer Bed&uuml;rfnisse oder physische
              Ver&auml;nderungen. Auch geht es nicht unmittelbar um die Befriedigung
              psychischer, sozialer oder emotional-informativer Bed&uuml;rfnisse,
              z. B. ein freundschaftliches Gespr&auml;ch zu f&uuml;hren oder
              sich &uuml;ber einen Sachverhalt zu informieren. Es geht vielmehr
              darum, innere Haltungen zu bestimmen und Werturteile und Wertehierarchien
              aufzubauen, die Handlungssicherheit geben und die Handlungsmotivation
              bestimmen. Solche Handlungen mit dem Ziel, Antworten auf existentielle
              Fragen herbeizuf&uuml;hren, sind Riten. Die in der Sprache der
              Riten produzierte Kommunikation vermittelt sich &uuml;ber alle
              sinnenhaften Wahrnehmungsformen. Riten folgen einer bestimmten
              Syntax und sind stilisiert. Sie sind teilweise genetisch festgelegt,
              werden aber auch imitiert und kultisch und kulturell rezipiert
            und weiterentwickelt.</font></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Wurzeln der
            Riten<br>
            </strong></font><font face="Arial, Helvetica, sans-serif">Es
                  gibt sehr verschiedene Urspr&uuml;nge der Riten. Manche von
                ihnen weisen Elemente auf, die von der Verhaltensbiologie im Tierreich
                beobachtet werden. Andere beruhen auf Verhaltensmustern, die sich
                erst im menschlichen Zusammenleben entwickelt haben. Alle Riten
                gehen auf urspr&uuml;ngliche Handlungsmuster zur&uuml;ck. Sie bilden
                den Kern der Riten, der erweitert werden kann. Dieser Handlungskern
                entstammt Handlungen, die der Befriedigung ganz urspr&uuml;nglicher
                Bed&uuml;rfnisse dienen: Mahlhandlungen, Erntehandlungen, Jagd-
                und Schlachtungsszenen, Partnerwerbung und Heilungshandlungen
                wie Salbung und Handauflegung.<br>
              Verbunden mit dieser nat&uuml;rlichen Urspr&uuml;nglichkeit der
                rituellen Handlungen ist eine urspr&uuml;ngliche Religiosit&auml;t,
                die dem Ritus anhaftet. Die Urspr&uuml;nglichkeit der Handlungen,
                ihre nat&uuml;rliche Notwendigkeit und ihre Unver&auml;nderbarkeit
                verdeutlichen gleichzeitig ihre Heiligkeit, ggf. ihren g&ouml;ttlichen
                Ursprung. Rituale gelten als heilig, weil derartige Handlungen
                dem Menschen um des &Uuml;berlebens willen von einer unverf&uuml;gbaren
                Macht geschenkt wurden. Ein Ritus wir &uuml;berliefert, er entwickelt
                sich und kommt dann von au&szlig;en auf die Menschen zu. Riten
                verlieren einen gro&szlig;en Teil ihrer Bedeutung und ihrer Autorit&auml;t,
                wenn sie allgemein als Menschenwerk betrachtet werden, d.h. wenn
                sie zum Gegenstand willk&uuml;rlicher Gestaltung werden. Daher
                k&ouml;nnen Liturgien nicht einfach ver&auml;ndert werden, ohne
                ihre soziale Wirksamkeit zu verlieren.<br>
              Die nat&uuml;rliche Religiosit&auml;t des Ritus korrespondiert
                mit dem menschlichen Bed&uuml;rfnis, sich mit einer Schicksalsmacht
                oder Gottheit in Beziehung setzen zu k&ouml;nnen. Diese Beziehungserfahrung
                bildet die Grundlage der Theologie oder Theosophie und liegt diesen,
                als Erkl&auml;rungswissenschaften oder Erkl&auml;rungsmodellen,
                logisch und chronologisch voraus. Von daher wird auch klar, dass
                Ritus und rituelles Verhalten Erkenntnisquellen religi&ouml;s-theologischer,
            philosophischer und anthropologischer Reflexionen sind. </font></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Ein Ritus ist interpersonal<br>
            </strong></font><font face="Arial, Helvetica, sans-serif">Ein
                Ritus wird immer interpersonal, d.h. von mindestens zwei Personen
                vollzogen. Damit
                ist der Ritus als dialogisches Geschehen, als
              sinnproduktives Beziehungshandeln, als kommunikatives Handel zu
              bezeichnen. Es kann aber durchaus sein, dass das Gegen&uuml;ber,
              der Dialogpartner im Ritus, eine Gottheit ist, die als Person angesprochen
              wird. Diese kann sowohl von Gegenst&auml;nden (Fetisch, <a href="symbol_funktion.php">Symbol</a>)
              dargestellt werden wie auch von Menschen (Priestern, Propheten),
              durch diese handeln und Botschaften vermitteln. Auch kann die Natur
              oder die ganze Umwelt Dialogpartner in einem Ritus sein. Die Gesellschaft
              tritt mir in Paraden, Nationalfeiertagen oder &uuml;ber eine Zeitung
              oder Nachrichtensendung in Beziehung und ordnet so meine Beziehung
              zur Umwelt neu.<br>
              In jedem Ritus vermittelt sich ein Beziehungsgef&uuml;ge, das sowohl
              zwischenmenschlicher als auch kosmischer Art sein kann. Riten beschreiben
              und steuern gesellschaftliche Beziehungen, sie definieren was heilig
              und verwerflich ist, was positiv und was negativ ist, wo verl&auml;ssliche
              Werte zu finden sind. Grunds&auml;tzlich l&auml;sst sich feststellen,
              dass Wertebewusstsein und wertebezogenes Handeln bevorzug in Riten
            dargestellt und vermittelt werden. <br>
            </font><font face="Arial, Helvetica, sans-serif">Innerhalb
            dieser anthropologischen Grundkonstitution weisen Riten eine kaum
            fassbare Variabilit&auml;t auf. Sie k&ouml;nnen an bestimmte
              R&auml;umlichkeiten (Tempel, Kulturst&auml;tten) oder Personen
              (Priester, Schamanen, Medizinm&auml;nner) gebunden oder aber frei,
              allgemein und von jedermann vollziehbar sein, wie z.B. der Begr&uuml;&szlig;ungsritus
            bei einer Feier, der mit einem Glas Sekt gestaltet wird.</font></p>            
            <p><font face="Arial, Helvetica, sans-serif"><strong>Riten im Tages, Wochen- und Jahresablauf:</strong><br>
              Zeitlich gesehen treten Riten dann auf, wenn existentieller Handlungszwang
                besteht, bzw. wenn Antworten auf existentielle Lebensfragen gebraucht
                werden. <br>
              Dies ist, auf den Tag betrachtet, morgens der Fall: Was erwartet
              mich heute wohl, wie gehe ich es an? Deshalb wird der Tag gerne
              mit einem Morgenritual begonnen, das sowohl in Morgengebet als
              auch im Griff zur Zeitung oder Einschaltknopf des Radios oder Fernsehers
              besteht. Ebenso besteht dieser Bedarf am Abend. Was war heute?
              Mit welchem Eindruck und welchen Schlussfolgerungen res&uuml;miere
              ich diesen Tag? Deshalb wird ein Ritus des Tagesabschlusses gerne
              in &auml;hnlicher Weise wie der des Tagesbeginns vollzogen. <br>
              Im Blick auf die Woche ist der w&ouml;chentliche Ruhetag, in der
              westeurop&auml;ischen Kultur der Sonntag, f&uuml;r die Kl&auml;rung
              existentieller Lebensfragen pr&auml;destiniert und gesch&uuml;tzt.
              Deshalb empfehlen, bzw. verlangen die christlichen Kirchen den
              sonnt&auml;glichen Kirchgang.<br>
              Auf das Jahr hin gesehen stellt sich zu Beginn und zum Ende des
              Jahres das Bed&uuml;rfnis zur Reflexion ein, im Sonnenjahr ebenso
              zu den Sonnenwendtagen. Aus anderen Kulturen sind weitere &auml;hnliche
              Tage &uuml;berliefert, so z.B. die Walpurgisnacht und genau sechs
              Monate sp&auml;ter Halloween. Folglich werden zu diesen Tage Riten
              begangen, die Werte vergegenw&auml;rtigen, die zu der jeweiligen
              Jahreszeit f&uuml;r die Lebensgestaltung hilfreich sein k&ouml;nnen.</font></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Umbruchphasen im Leben eiens Menschen:</strong><br>
  Auch im Lauf des menschlichen Lebens ergeben sich Phasen, die naturgegeben
                sich als Umbruchphasen darstellen, weil sich dringliche existentielle
                Fragen ergeben. In der Kulturanthropologie werden solche Phasen
                als &Uuml;berg&auml;nge beschrieben. Diese werden in allen Kulturen
                mit besonders gepr&auml;gten Riten - Rites de passage - begangen.
                Solche &Uuml;berg&auml;nge finden sich beim Lebenseintritt und
                am Lebensende, beim &Uuml;bergang vom Kindes- zum Erwachsenenalter,
                beim Eintritt in eine Lebensgemeinschaft, beim Antritt eines
                besonderen Amtes oder einer Funktion. </font></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Riten in Krisenzeiten: </strong><br>
  Weiterhin lassen sich Regelm&auml;&szlig;igkeiten feststellen,
              in denen das Bed&uuml;rfnis nach Riten gro&szlig; ist. Ausgehend
              von ihrer Funktion, Antworten auf dringliche existentielle Fragen
              herbei zu f&uuml;hren, wird die auch leicht verst&auml;ndlich.
              So w&auml;chst das menschliche Bed&uuml;rfnis nach Riten zu Zeiten,
              die als Krise bezeichnet werden k&ouml;nnen. Als ganz einfaches
              Beispiel l&auml;sst sich hier die Anzahl der Besucher liturgischer
              und religi&ouml;s-spiritueller Handlungen zu Zeiten von Kriegen
              oder Naturkatastrophen anf&uuml;hren.<br>
              Riten werden dringend notwendig, wenn Konflikte entstehen, die
              ihrerseits L&ouml;sungen verlangen, die von Menschen aus innerem
              Antrieb realisiert werden. Denn Riten k&ouml;nnen auch gezielt
              eingesetzt werden zur Herstellung bestimmter psychisch-emotionaler
              Zust&auml;nde wie Agressivit&auml;t oder Friedfertigkeit, Zusammengeh&ouml;rigkeitsgef&uuml;hl
              und St&auml;rkung der Identit&auml;t und zur Heilung. <br>
              Auch l&auml;&szlig;t sich beobachten, dass die Anzahl der Riten
              in Gesellschaftsformen, die starken ideologischen Einfluss auf
              ihrer Mitglieder aus&uuml;ben (Sekten, fundamentalistische und
              radikale Parteien und Staatsformen, Glaubensgemeinschaften) gr&ouml;&szlig;er
              ist als in liberalen Gesellschaften. Dies ist deshalb so, weil
              die Bewahrung der eigenen Identit&auml;t in einem anders gearteten
              Umfeld oder der Aufbau einer Ideologie, die vielen Vernunftseinsichten
              widerspricht, viele existentielle Fragen verursacht. </font></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Charakteristika von Riten:</strong><br>
              Der Form und Gestaltung nach weisen Riten typische Merkmale aus,
                die Handlungen als Riten erkennen lassen. <br>
              Rituelle Handlungen sind stilisierte Handlungen, die sich alle
              als urspr&uuml;nglich ausweisen lassen. Dabei kann die Urspr&uuml;nglichkeit
              in Bereiche hinein reichen, wo die Verhaltensmuster nicht mehr
              von der Tradition &uuml;berliefert und lernbar sind, sondern als
              genetisch bedingte Verhaltensmuster bezeichnet werden. Dies schlie&szlig;t
              aber keineswegs aus, dass solche, urspr&uuml;nglich der genetischen
              Determination verdankten, Verhaltensmuster von kulturellen Einfl&uuml;ssen
              weiterentwickelt wurden, z. b. wenn, um die Brautwerbung zu regeln,
              bestimmte Trachten Aufschluss dr&uuml;ber geben, ob ein junger
              Mann oder eine junge Frau verheiratet sind oder nicht. Dies weist
              durchaus Parallelen auf und kann in urspr&uuml;nglicher Beziehung
              mit dem Balzverhalten von Tieren stehen, wo entsprechende Signale
            ebenfalls gegeben werden.</font></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Verk&uuml;rzte Erkl&auml;rungen f&uuml;r das Ph&auml;nomen &#8222;Ritus&#8220;</strong><br>
  Ritus ist ein &auml;u&szlig;erst vielgestaltiges Ph&auml;nomen,
              das nur schwer in seiner Komplexit&auml;t erfa&szlig;t wird und
              deshalb dazu neigt, auf Einzelaspekte reduziert zu werden. Dies
              geschieht beispielsweise dann, wenn Ritus ausschlie&szlig;lich
              als genetisch determiniertes, stereotypes Verhalten begriffen wird.
              Andrerseits entspricht es nicht dem urspr&uuml;nglichen Begriff
              des Ritus, wenn willk&uuml;rlich gestaltete Handlungsfolgen, z.
              B. Shows, als Riten bezeichnet werden. Riten werden immer, zumindest
              in ihrem Kern, &uuml;berliefert und sind nicht willk&uuml;rlich
              komponierbar. <br>
              Rituelles Verhalten ist inzwischen Forschungsgegenstand vieler
              Wissenschaften, so der Soziologie, der Psychologie, der Verhaltensbiologie,
              der Anthropologie. Wenn das partielle Interesse der Einzeldisziplinen
              mit einem universalen Erkl&auml;rungsanspruch verbunden wird, der
              aber gerechtfertigter Weise nur einem interdisziplin&auml;ren und
              zweckfreien Zugang zukommen kann, entstehen reduktive Begriffe
              von Riten: Solche reduktiven Zug&auml;nge sind festzustellen, wenn
              Ritus verstanden wird als:<br>
              -	willk&uuml;rliche, durch Willensentscheid festgelegte Zeichensysteme;<br>
              -	Verhaltensbiologisch determiniertes, stereotypes Verhalten;<br>
              -	Zu bestimmtem, p&auml;dagogischen Zwecken ausgedachte Handlungen
              zur Dramatisierung abstrakter Inhalte;<br>
              -	Durch g&ouml;ttliche Setzung einmalig und unver&auml;nderlich
            festgelegte Handlungen.</font></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Religi&ouml;se Riten</strong><br>
              Der
                christlichen Theologie war es von ihren antiken Anf&auml;ngen
                an bewusst, dass der Ritus sowohl ihre Erkenntnisquelle als auch
                ihr Forschungsgegenstand ist (lex celebrandi lex credendi). <br>
              Theologie wurde vielfach als <a href="mystagogie.php">Mystagogie</a> verstanden, d. h. als
                Deutung ritueller Vorg&auml;nge wie beispielsweise der Taufe. Diese Art
                der Theologie weist Parallelen auf zur allegorische Schriftauslegung.
                Diese begreift die Bibel, die heilige Schrift, als symbolische
                Sprache, die sich gleichzeitig auf mehreren Ebenen deuten l&auml;&szlig;t,
                d. h. die mit einer Aussage gleichzeitig mehrere Inhalte vermittelt. <br>
              Die Kulturanthropologie hat sich, veranlasst durch die Erfahrungen
                der Kolonialzeit, im 19. und 20. Jhd. dem Ritus zugewandt und ihn
                systematisch erforscht. <br>
              Die sozialwissenschaftliche Forschung kam Mitte des letzten Jahrhunderts
                hinzu, seit C. G. Jung wendet sich auch die Psychologie dem Ritus
            zu.</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Christliche Riten sind
                bereits biblisch bezeugt. Der zentrale und wichtigste Ritus ist
                das &#8222;Brotbrechen&#8220;, d.h. die
              dem j&uuml;dischen Pesachmahl entstammende Geste, die durch Jesus
              beim &#8222;<a href="mystagogie.php">Letzten Abendmahl</a>&#8220; eine neue Bedeutung erhielt.
              Sie wurde zum Ged&auml;chtnis und zur Aktualisierung der Erl&ouml;sungstat
              von Jesus Christus: Sein S&uuml;hnetod und seine Auferstehung werden
              bei der rituellen Feier des Ged&auml;chtnismahles in ihrer Wirkung
              gegenw&auml;rtig. <br>
              Von diesem und anderen Ereignissen aus dem Leben Jesu leitet die
              christliche Theologie den ihr von Jesus Christus zukommenden Auftrag
              ab, die Liebe Gottes zu den Menschen, wie sie in Jesus Christus
              deutlich wurde, in konkreten Lebenssituationen zu verdeutlichen.
              Aus diesem Bewu&szlig;tsein entstanden neue Riten, bzw. wurden
              bereits in anderen Religionen bestehende Riten mit neuen Bedeutungen
              versehen, z. B. die Taufe, Ehe, Salbung bei Kranken und als Zeichen
              g&ouml;ttlicher Begabung verbunden mit Handauflegung bei Weihe
              und Eintritt in das Erwachsenenalter.<br>
              Insofern diese Riten, zumindest dem Ansatz nach, biblisch bezeugt
              sind und allgemeinen Charakter haben, werden diese in der katholischen
              Kirche als Sakramente bezeichnet, davon gibt es sieben. <br>
              Dar&uuml;ber hinaus gibt es weitere, regional begrenzte Segnungsriten.
              Sie verdeutlichen, dass auch die allt&auml;glichen Gegenst&auml;nde
              Mittel sind, um dem Auftrag Jesu entsprechend Heil und Gutes bewirkend
              zu leben.<br>
              Der Funktion der Riten entsprechend, G&ouml;ttliches zu bewirken,
              hat die Theologie verschiedene Wirkungsmodelle entwickelt. So wirken
              die Riten, die allgemein sind und vom Glauben aller Katholiken
              einheitlich getragen sind, &#8222;ex opere operatum&#8220;, d.h.
              aus der Kraft des Ritus selbst. Andere, regionale und gestaltbare
              Riten hingegen wirken &#8222;ex opere operantis&#8220;, d.h. aus
              ihrer von Gestaltung und Gestaltern abh&auml;ngigen Wirkungskraft. <br>
              Theo Hipp</font><br>
              <br>
            <font size="2" face="Arial, Helvetica, sans-serif"><a href="http://www.kath.de/">&copy; www.kath.de</a></font></p>
          </td>
          <td width="1" align="left" class="L12">            <div align="right"><br>
              <br>
              </div>
          </td>
          <td width="9" align="left" background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top"> 
          <td width="8" align="left"><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td colspan="2" align="left" background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td width="9" align="left"><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
</table>
</body>
</html>
