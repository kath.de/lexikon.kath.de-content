<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="kaltefleiter.css" title="fonts">
</head>
<body bgcolor="#ffffff" leftmargin="6" topmargin="6" marginwidth="6" marginheight="6">
<table width="100%" border="0" cellspacing="0" cellpadding="6">
  <tr>
    <td width="100" align="left" valign="top" height="3"> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b><font face="Arial, Helvetica, sans-serif">Das 
            Liturgie Lexikon</font></b></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td> <?php include("logo.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><font face="Arial, Helvetica, sans-serif"><strong>Inhaltsverzeichnis 
            </strong></font></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10"> <?php include("az.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
    <td rowspan="2" valign="top">
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td background="boxtop.gif" colspan="2"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif" width="8"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2" colspan="2"> 
            <h1><font face="Arial, Helvetica, sans-serif">Die Laudes</font></h1>
          </td>
          <td background="boxtopright.gif" width="8"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif" colspan="2"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td width="8"><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif" width="8"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="L12" width="70%"> 
            <p><br>
              <font face="Arial, Helvetica, sans-serif">&quot;Die Laudes als
              Morgengebet und die Vesper als Abendgebet, nach der ehrw&uuml;rdigen &Uuml;berlieferung
              der Gesamtkirche die beiden Angelpunkte des t&auml;glichen Stundengebetes,
              sollen als die vornehmsten Gebetsstunden angesehen und als solche
              gefeiert werden.&quot; (Sacrosanctum Concilium, 89a) <br>
              <br>
              Der Begriff 
              stammt vom lateinischen laus und bedeutet Lob (Laudes ist der Plural
                von laus und bedeutet damit "Loblieder"). Die Laudes sind das Morgengebet 
              der Kirche. Da Christus in seiner Auferstehung mit der aufgehenden 
              Sonne (des Ostermorgen)in Beziehung gesetzt wird, ist dieses Morgengebet 
              zugleich eine Auferstehungsfeier dar. Die Laudes k�nnen gesungen
            werden. <br>
              </font><font face="Arial, Helvetica, sans-serif"><br>
            In der Regel des hl. Benedikt sind die Laudes um 03.00 Uhr angesetzt
              (<a href="http://www.benediktiner.de/regula/RB_deutsch01.htm#Kap_13" target="_blank">Nr.
              12 und 13 der Regel</a>). Sie geh�rt zu den gro�en
              Horen und wird auch Morgenhore genannt. Neben der Vesper ist dieses
              Morgengebet
              die wichtigste Stundegebetszeit. <br>
              <br>
              Laudes wie andere Teile des Stundengebetes haben eine charakteristische
              Er�ffnung: Der Vorbeter/ Priester beginnt: Vorbeter: O Gott, komm 
              mir zu Hilfe Gemeinde: Herr, eile mir zu helfen V: Ehre sei dem 
              Vater und dem Sohne und des Heiligen Geiste. G: Wie es war im Anfang, 
              so auch jetzt und alle Zeit und in Ewigkeit Amen. Die Hymnen werden 
              aus der christlichen Tradition ausgew�hlt und geben diesem Morgengebet 
              vor allem an Festtagen eine eigene F�rbung. Aus dem Buch der Psalmen 
              werden jeweils zwei Psalmen ausgew�hlt, aus den psalm�hnlichen Ges�ngen 
              der anderen B�chern des Alten Testaments ein Canticum, ein Lied.
              Die Lesung ist kurz, einige Verse aus der Briefliteratur des Neuen
              Testaments Sie wird mit dem Responsorium, einem antwortgesang beantwortet.
            Dieses hat eine Charakristishce Form </font></p>
            <p><font face="Arial, Helvetica, sans-serif">              V: Doppelvers G. Wiederholung des Doppelverses <br>
              V: Halbvers G: zweite H�lfte des Doppelverses vom Anfang <br>
              V: Ehre sei dem � G:Doppelvers vom Anfang <br>
              <br>
              Das Benedictus ist ein Charakteristikum der Laudes, es ist
                  der Gesang des Zacharias bei der Geburt seines Sohnes Johannes,
                  dem
                sp�teren 
                T�ufer, den Lukas in Kap.1, 46-55 �berliefert. Die F�rbitten haben 
                den gleichen Aufbau wie in der Eucharistiefeier. In ihnen kommen 
                die Anliegen von Kirche und Welt zum Ausdruck. Das Bittgebet folgt 
                der Aufforderung des Jakobusbriefes: " Betet f�reinander, damit 
                ihr geheiligt werdet. Viel vermag das inst�ndige Gebet eines Gerechten" 
              (Jak 5,16). Es handelt sich bei den Bitten bzw. F�rbitten um Anregungen, 
                die die Betenden durch eigene Anliegen erg�nzen k�nnen. Die Bitten 
                und F�rbitten m�nden in das Gebet des Herrn, das gro�e Schlussgebet 
                von Morgen- und Abendlob. Es ist das Gebet, das Jesus seine J�nger 
                gelehrt hat, in dem er die Betenden hineinnimmt in sein Beten zum 
                Vater. Das Vaterunser ist die Urgestalt jeglichen christlichen Betens, 
                das alle christlichen Konfessionen verbindet. Auf das Vater Unser 
                folgt die Oration, die an Sonntagen aus dem Me�buch �bernommen wird 
                und identisch mit dem Tagesgebet der Eucharistiefeier ist. Ein Segenswort 
                m�ndet in die Entlassung: Gehet in Frieden <br>
              <br>
              Wenn den Laudes nicht ein Priester und kein Diakon vorsteht,
                wird das Morgengebet mit folgenden Worten abgeschlossen: V: Der
                Herr
                segne uns, er bewahre uns vor Unheil und f�hre uns zum ewigen Leben.
                G: Amen Zitate AES 38 . <strong><br>
                <br>
                </strong></font><font face="Arial, Helvetica, sans-serif">Aufbau </font></p>
            <p><font face="Arial, Helvetica, sans-serif">Er�ffnung<br>
  Hymnus <br>
  Psalm <br>
  Gesang aus dem Alten Testament<br>
  Psalm <br>
  Lesung <br>
  Responsorium <br>
  Benedictus <br>
  F�rbitten <br>
  V ater unser <br>
  Oration <br>
  Segen <br>
  Entlassung <br>
  <br>
  Weitere Inforamtionen zum Thema: <br>
  <br>
  <a href="http://www.kathpedia.com/index.php?title=Laudes" target="_blank">http://www.kathpedia.com/index.php?title=Laudes</a> </font></p>
            <p><font face="Arial, Helvetica, sans-serif">Feedback bitte an mit 
              dem Stichwort Liturgie-Lexikon an: redaktion@kath.de</font></p>
          </td>
          <td class="L12" width="30%"> 
            <div align="right"> 
              <p><br>
                <br>
                <font size="2" face="Arial, Helvetica, sans-serif"><img src="laudes2.jpg" width="263" height="370" alt="Die Laudes: Bild Sonnenaufgang hinter Glockenturm"><br>
                Die Laudes sind das <br>
                Morgengebet der Kirche<br>
                <br>
                <br>
                <img src="laudes.jpg" width="340" height="255" alt="Die Laudes: Bild Schriftzug Lob ist"><br>
                <br>
                Die Laudes sind Lobgebete. Sie<br>
                sind kleine Auferstehungsfeiern</font></p>
            </div>
          </td>
          <td background="boxright.gif" width="8"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif" colspan="2"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td width="8"><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
		<!-- neuer Code Anfang -->
	<td style='vertical-align:top;'>

     <script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
/* Symbollexikon neu */
google_ad_slot = "9464926645";
google_ad_width = 120;
google_ad_height = 600;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
     <br>

     
    </td>
	<!-- neuer Code Ende -->
  </tr>
  <tr>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
</table>
</body>
</html>
