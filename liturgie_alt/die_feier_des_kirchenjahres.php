<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title>Die Feier des Kirchenjahres</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="kaltefleiter.css" title="fonts">
</head>
<body bgcolor="#ffffff" leftmargin="6" topmargin="6" marginwidth="6" marginheight="6">
<table width="100%" border="0" cellspacing="0" cellpadding="6">
  <tr>
    <td width="100" align="left" valign="top" height="3"> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b><font face="Arial, Helvetica, sans-serif">Das 
            Liturgie Lexikon</font></b></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td> <?php include("logo.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><font face="Arial, Helvetica, sans-serif"><strong>Inhaltsverzeichnis 
            </strong></font></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10"> <?php include("az.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
    <td rowspan="2" valign="top">
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td background="boxtop.gif" colspan="2"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8" height="37" background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2" colspan="2"> 
            <h1><font face="Arial, Helvetica, sans-serif">                <font face="Arial, Helvetica, sans-serif">Die
            Feier des Kirchenjahres &#8211; historisch</font></font></h1>
          </td>
          <td background="boxtopright.gif" width="8"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif" colspan="2"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td width="8"><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif" width="8"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="L12" width="70%"> 
            <p><br>
              <font face="Arial, Helvetica, sans-serif"><strong>Ein neuer Kalender und die Folgen</strong><br>
              <br>
  Jede Feier im Herrenjahr ist Gesamtausdruck der gesamten Zeit.
                Ein Problem gab es bei der Umstellung von dem julianischem auf
                den gregorianischem Kalender. Wir feiern heute nach gregorianischen
                Kalender 1582 tritt er in Kraft. Die Ostkirche aber bleibt dem
                julianischem Kalender treu. Das hatte einen theologischen Grund:
                Es wurden bei der Umstellung 13 Tage &uuml;bersprungen. Wenn
                man aber 13 Tage &uuml;berspringt, so die Ostkirche, hat man
                die Zeit nicht liturgisch gefeiert. Die &uuml;bersprungenen Tage
                h&auml;tte man liturgisch nicht begangen. Auch hier wird deutlich,
                dass Zeit im theologischen Sinne nicht ein Datumsverst&auml;ndnis
            ist.</font></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Die Vorherrschaft des kirchlichen Kalenders.</strong><br> 
              <br>
  Durch die offiziellen Ausgaben des Breviers (1568) und des Me&szlig;buchs
              (1570) unter Papst Pius V. gewann der r&ouml;mische Kalender, der
              diesen B&uuml;chern vorausgeschickt war, f&uuml;r die katholische
              Welt offiziellen Charakter, der ihm von da an nie mehr streitig
              gemacht wurde. Der Kalender unserer Zeit ist praktisch eine letzte
              Reminiszenz an die Kirche. F&uuml;r die Kirche stellt sich die
              Frage, ob sie auch gesellschaftliche, politische Aspekte etc. in
              ihren Kalender aufnehmen soll.<br>
              So ist der 1. Mai politisch und gesellschaftlich wichtig. Die Kirche
              begeht nicht zuf&auml;llig an diesem Tag das Fest Josef des Arbeiters. <br>
              Aber auch theologische Ausdr&uuml;cke finden sich im liturgischen
              Kalender: Urzelle f&uuml;r die Kirche ist die Familie, was am Pfarrfest
              gefeiert wird. In S&uuml;damerika etwa stellt sich die Frage nach
              politischen Festen.</font></p>
            <p><strong><font face="Arial, Helvetica, sans-serif">R&uuml;ckblick
                  in die Geschichte </font></strong><font face="Arial, Helvetica, sans-serif"><br>
                <br>
  Der Sabbat steht am Anfang. Dann kam die Revolution mit den Heidenchristen,
                die das Sabbatfest nicht mitmachen wollten. Sie wehren sich gegen
                den j&uuml;dischen Kalender. Christentum l&ouml;st sich zusehends
                vom j&uuml;dischen Kalender. Im vierten Jahrhundert kommt es
                zu einer entscheidenden Hinzuf&uuml;gung: Sol invictus, das r&ouml;mische
                Fest der unbesiegbaren Sonne wird ins Christentum und in den
                Kalender transformiert. Das Triduum sacrum, also die Zeit von
                Gr&uuml;ndonnerstag bis Ostern, wird theologisch entfaltet. Es
                kommt zu einer Quasi-Historisierung Auf die gro&szlig;en Feste
                gibt es eine Vorbereitungszeit. War anf&auml;nglich das Werk
                Jesu entscheidend, kommt im 4. Jahrhundert die Person Jesu in
                den Vordergrund. <br>
              Im 6. und 7. Jahrhundert nehmen die Marienfeste stark zu. Das
                Jesus-Geheimnis wird zusehends unter dem Marienaspekt gesehen.
                Im 10. Jahrhundert
              kommt ein neuer Typ Fest hinzu: Historisierende Feste kommen hinzu:
              Himmelfahrt, Verkl&auml;rung, Dreifaltigkeit, Herz Jesu, ... Es
              wird zunehmend dramatisiert. Die Woche bekommt auch Themen. Im
              14. Jahrhundert beginnt man zu werten: Wichtigester Gegenstand
              ist die Trinit&auml;t. Deswegen war jeder Sonntag ein gr&uuml;ner
              Sonntag, weil die Trinit&auml;t gefeiert wurde. <br>
              Die Liturgie kann nichts anfangen mit den Monaten. Sie geh&ouml;ren
              zum Heidentum. (Sternzeichen, Mond) Am Ende des Monats wurde deshalb
              ein Apostelfest gefeiert, nicht ein Sternzeichen. Es kommt zu einer
              Apostolisierung der Monate. Nicht der Kosmos, sondern die Apostel
              bilden den Mittelpunkt der Monate. Auch bekommen die Monate ein
              Thema: Mai: Mutter Gottes, Juni: Herz-Jesu Monat. Oktober: Rosenkranz-Monat.</font><br>
            </p>
            <p><br>
            </p>
            <p><br>
            </p>
            <p><br>
            </p>
            <p><br>
            </p>
            <p><br>
            </p>
            <p><br>
            </p>
            <p><br>              
                <font face="Arial, Helvetica, sans-serif">              </font><font face="Arial, Helvetica, sans-serif">              <br>
                Feedback bitte an mit 
          dem Stichwort Liturgie-Lexikon an: redaktion@kath.de</font></p></td>
          <td class="L12" width="30%"> 
            <div align="right"> 
              <p><br>
                <br>
                <font size="2" face="Arial, Helvetica, sans-serif"><br>
                </font></p>
            </div>
          </td>
          <td background="boxright.gif" width="8"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif" colspan="2"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
<td width="8"><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
 </tr> 
</table> 
</td>
<!-- neuer Code Anfang -->
<td style='vertical-align:top;'> 
     <script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
/* Symbollexikon neu */
google_ad_slot = "9464926645";
google_ad_width = 120;
google_ad_height = 600;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
     <br>
</td>
<!-- neuer Code Ende -->

  </tr>
  <tr>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
</table>
</body>
</html>
