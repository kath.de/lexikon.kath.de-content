<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<meta name="title" content="Liturgie">
<meta name="author" content="Redaktion kath.de">
<meta name="publisher" content="kath.de">
<meta name="copyright" content="kath.de">
<meta name="description" content="">
<meta name="abstract" content="Lexikon �ber die Liturgie der katholischen Kirche">
<meta http-equiv="content-language" content="de">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="date" content="2006-00-01">
<meta name="robots" content="index,follow">
<meta name="revisit-after" content="10 days">
<meta name="revisit" content="after 10 days">
<meta name="DC.Title" content="Liturgie">
<meta name="DC.Creator" content="Redaktion kath.de">
<meta name="DC.Contributor" content="J�rgen Pelzer">
<meta name="DC.Rights" content="kath.de">
<meta name="DC.Publisher" content="kath.de">
<meta name="DC.Date" content="2006-00-01">
<meta name="DC.Description" content="Lexikon �ber die Liturgie der katholischen Kirche">
<meta name="DC.Language" content="de">
<meta name="DC.Type" content="Text">
<meta name="DC.Format" content="text/html">
<meta name="DC.Identifier" content="http://www.kath.de/lexikon/liturgie/">
<meta name="keywords" lang="de" content="Wortgottesdienst, Hochgebet, Kommunionteil, Entlassung">
<title>Messe - Aufbau</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="kaltefleiter.css" title="fonts">
</head>
<body bgcolor="#ffffff" leftmargin="6" topmargin="6" marginwidth="6" marginheight="6">
<table width="100%" border="0" cellspacing="0" cellpadding="6">
  <tr>
    <td width="100" align="left" valign="top" height="3"> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b><font face="Arial, Helvetica, sans-serif">Das 
            Liturgie Lexikon</font></b></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td> <?php include("logo.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><font face="Arial, Helvetica, sans-serif"><strong>Inhaltsverzeichnis 
            </strong></font></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10"> <?php include("az.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
    <td rowspan="2" valign="top">
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td background="boxtop.gif" colspan="2"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="9"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif" width="8"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2" colspan="2"> 
            <h1><font face="Arial, Helvetica, sans-serif">Messe - Aufbau</font></h1>
          </td>
          <td background="boxtopright.gif" width="9"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif" colspan="2"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td width="9"><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif">&nbsp;</td>
          <td class="L12"><p><strong><font face="Arial, Helvetica, sans-serif">Der
                  Ablauf einer katholischen Messfeier</font></strong></p>
            <p> <font face="Arial, Helvetica, sans-serif">Die Messe, wie sie
                Katholiken feiern, hat sich &uuml;ber Jahrhunderte entwickelt. Durchgesetzt
                hat sich die r&ouml;mische Liturgie, nicht zuletzt deshalb, weil
                Karl d. Gr. sie f&uuml;r das Frankenreich verbindlich gemacht
                hat. In der Struktur unterschieden sie sich nicht wesentlich
                von den Liturgien der orthodoxen Kirchen und auch nicht von der
                Mail&auml;nder oder der mozarabischen, in S&uuml;dspanien gefeierten
                Liturgie. Bis in die sechziger Jahre des 20. Jahrhunderts wurde
                die Messe lateinisch gefeiert, bis das II. Vatikanische Konzil
                die Feier in der jeweiligen Volkssprache einf&uuml;hrte. Der
                Vorteil f&uuml;r die Katholiken besteht darin, da&szlig; sie
                im Ausland dem Ablauf einer Messe folgen k&ouml;nnen, ohne alle
                Texte zu verstehen, denn die Messe hat immer den gleichen Aufbau. <br>
              Der &auml;ltere Teil der Messe ist der Mahlteil, der <a href="eucharistie_messe_abendmahl.php">eucharistische
  Gottesdienst</a>. Das Neue Testament berichtet, da&szlig; die J&uuml;ngergemeinde
  sich auch nach Jesu Tod im Abendmahlsaal getroffen hat. Aus dem 1. Korintherbrief
  des Paulus ist zu entnehmen, da&szlig; die Christen sich zu dem Ged&auml;chtnismahl
  trafen, das Jesus am Gr&uuml;ndonnerstag eingesetzt hatte. Da die Christen
  sich anfangs noch als Teil des Judentums verstanden, besuchten sie die Synagogengottesdienste
  am Sabbat-Samstag und feierten das Ged&auml;chtnismahl am 1. Tag der Woche,
  der dann zum Wochenfeiertag, dem Sonntag wurde. Der Synagogengottesdienst bestand
  aus Lesungen und Ges&auml;ngen und kannte auch die Auslegung der gelesenen
  Texte in einer Predigt. Mit dem Entstehen eigener christlicher Texte in der
  zweiten H&auml;lfte des 1. Jahrhunderts und der wachsenden Distanz zwischen
  Synagoge und Kirche kamen zu dem Ged&auml;chtnismahl die Lesungen, Ges&auml;nge
  und Gebete des Wortgottesdienstes.<br>
  Die Messe in Rom wurde in den ersten Jahrhunderten auf Griechisch gefeiert.
  Papst Damasus (366-384) f&uuml;hrte die lateinische Sprache ein. Immer wieder
  mu&szlig;te das liturgische Leben und damit auch die Messe von &Uuml;berwucherungen
  befreit und neu geordnet werden. Papst Gregor I. gab um 600 der Liturgie f&uuml;r
  die Stadt Rom eine so schl&uuml;ssige Gestalt, da&szlig; die r&ouml;mische
  Liturgie f&uuml;r das Abendland ma&szlig;gebend und von den Germanen &uuml;bernommen
  wurde. Nach ihm ist auch der gregorianische Gesang benannt, der Im Mittelalter
  im Frankenreich zu der Vollendung gef&uuml;hrt wurde, die heute noch die H&ouml;rer
  fasziniert. Das Konzil von Trient (1551 wurde das Dekret &uuml;ber die Liturgie
  verabschiedet) regte eine Liturgiereform an, die von Pius V. umgesetzt wurde.
  Die Liturgiereform, die das II. Vatikanische Konzil in Grundz&uuml;gen beschlossen
  hatte, wurde von Paul VI. in eine Form gegossen. An der Grundstruktur der r&ouml;mischen
  Messe haben diese Reformen nichts ge&auml;ndert.</font></p>
            <p>&nbsp;</p></td>
          <td class="L12">&nbsp;</td>
          <td background="boxright.gif">&nbsp;</td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif">&nbsp;</td>
          <td class="L12"><table width="100%" border="1">
            <tr>
              <td width="19%"> <p><font face="Arial, Helvetica, sans-serif"><strong>Er&ouml;ffnung</strong></font></p>
                <p><font size="2" face="Arial, Helvetica, sans-serif">Einzug<br>
  Begr&uuml;&szlig;ung<br>
                  Schuld-<br>
                  Bekenntnis<br>
                  Kyrie<br>
                  Gloria<br>
                  Tagesgebet</font><br>
                </p></td>
              <td width="31%"><p><strong><font face="Arial, Helvetica, sans-serif">Wortgottesdienst an Sonntagen</font></strong></p>
                <p><font face="Arial, Helvetica, sans-serif"><strong><font size="2">Erste Lesung</font></strong><font size="2"> <br>
                  meist aus dem Alten Testament</font></font></p>
                <p><font size="2" face="Arial, Helvetica, sans-serif"><strong>Psalm</strong></font></p>
                <p><font size="2" face="Arial, Helvetica, sans-serif"><strong>Zweite Lesung </strong><br>
                  meist aus neutestamentlichen Briefen</font></p>
                <p><font size="2" face="Arial, Helvetica, sans-serif"><strong>Halleluja</strong></font></p>
                <p><font size="2" face="Arial, Helvetica, sans-serif"><strong>Evangelientext</strong></font></p>
                <p><font size="2" face="Arial, Helvetica, sans-serif">Predigt<br>
                  Glaubensbekenntnis<br>
                  F&uuml;rbitten</font><font face="Arial, Helvetica, sans-serif"><br>
                  </font> </p>
                </td>
              <td width="31%"><p><strong><font face="Arial, Helvetica, sans-serif">Eucharistischer Gottesdienst</font></strong></p>
                <p><font size="2" face="Arial, Helvetica, sans-serif">Kollekte und<br>
                    <strong>Gabenbereitung</strong> </font></p>
                <p><font size="2" face="Arial, Helvetica, sans-serif"><strong>Hochgebet:</strong><br>
  Pr&auml;fation<br>
                  Sanctus<br>
                  Epiklese<br>
                  Einsetzungsworte<br>
                  Anamnese<br>
                  Intercessiones<br>
                  Doxologie</font></p>
                <p><font size="2" face="Arial, Helvetica, sans-serif"><strong>Kommunionteil</strong><br>
                  Vater unser<br>
                  Embolismus<br>
                  Friedensgebet<br>
                  Brotbrechen mit Agnus Dei-Gesang<br>
                  Kommunionausteilung<br>
                </font><font face="Arial, Helvetica, sans-serif">                </font>                </p></td>
              <td width="19%"><p><strong><font face="Arial, Helvetica, sans-serif">Entlassung</font></strong></p>
                <p><font size="2" face="Arial, Helvetica, sans-serif">Schlu&szlig;gebet<br>
                  Vermeldungen<br>
                  Segen<br>
                  Entlassung :<br>
                  Ite Missa est</font><br>
                </p>
                </td>
            </tr>
          </table></td>
          <td class="L12">&nbsp;</td>
          <td background="boxright.gif">&nbsp;</td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif" width="8"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="L12" width="516"> 
            <p>&nbsp;</p>
            <p><font face="Arial, Helvetica, sans-serif">An Werktagen und bei
                  den meisten Heiligenfesten entfallen das Gloria, die Zweite Lesung,
                  das Glaubensbekenntnis und meist auch die
                  Predigt.<br>
              Ansonsten unterscheiden sich festliche Messen durch die musikalische
              Gestaltung.</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Was erst einmal wie
                aufeinander folgende Einzelteile erscheint, hat eine innere Dramaturgie,
                die mit dem Thema der Feier zusammenh&auml;ngt.
              Das Thema ist die Beziehung des Menschen zu Gott unter dem Vorzeichen,
              da&szlig; der Mensch nicht wie selbstverst&auml;ndlich in dieser
              Beziehung lebt. Der Mensch hat sich verloren, so da&szlig; Gott
              ihn heimholen mu&szlig;te - durch die Sendung seines Sohnes.</font></p>
            <p><font face="Arial, Helvetica, sans-serif">In der Er&ouml;ffnung geht es darum, da&szlig; das Thema jeder
              Messe bereits anklingt. Der Vorsteher er&ouml;ffnet nicht nur die
              Messe, sondern bringt die Gemeinde in Beziehung zu Gott. Die Gemeinde
              versteht sich im S&uuml;ndenbekenntnis als erl&ouml;sungsbed&uuml;rftig.
              Sie begr&uuml;&szlig;t ihren Herrn (Kyrios ist das griechische
              Wort f&uuml;r Herr) und preist ihn im anschlie&szlig;enden Gloria.<br>
              Im Tagesgebet wird der <a href="eroeffnung_messe.php">Er&ouml;ffnungsteil</a> zusammengefa&szlig;t,
              deshalb hie&szlig; dieses Gebet fr&uuml;her &#8222;Collecta&#8220; von &#8222;Sammeln&#8220;,
              das gleiche Wort wie Kollekte, in der bei der Gabenbereitung Geld
              gesammelt wird.</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Der Wortgottesdienst
                thematisiert die Zuwendung Gottes und holt die Gemeinde da ab,
                wo sie herkommt: Da&szlig; die einzelnen w&auml;hrend
              der Woche viele Erfahrungen machen mu&szlig;ten, die sie mit einer
              nicht-erl&ouml;sten Welt und der eigenen Fehlerhaftigkeit und Bosheit
              konfrontierten. Wie kann Gott mich erl&ouml;sen, wo doch viele
              Erfahrungen mir das Gef&uuml;hl geben mu&szlig;ten, da&szlig; ich
              alles andere als erl&ouml;st bin? Die Lesungen, Ges&auml;nge, der
              Abschnitt, der aus einem der vier Evangelien verlesen wird und
              die Predigt sollen den einzelnen &uuml;berzeugen, da&szlig; Gott,
              sicher anders als die Menschen erwarten, sein Werk der Erl&ouml;sung
              vollbringt. Im Glaubensbekenntnis (Credo &#8211; Ich glaube) antwortet
              die Gemeinde auf die Lesungen und die Predigt und betet dann f&uuml;r
              die Anliegen der Zeit der Kirche in den F&uuml;rbitten.</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Er&ouml;ffnung und Wortgottesdienst haben die Feiernden in die
              Feier der Erl&ouml;sung eingestimmt. Sie sind wieder in der Lage,
              Gott f&uuml;r Jesus zu danken, der die Erl&ouml;sung in seinem
              Tod und seiner Auferstehung vollbracht hat und durch die Sendung
              des Geistes bereits Anteil an dem neuen Leben gew&auml;hrt, in
              das die Christen durch die Taufe bereits eingetreten sind. Die
              Eucharistie, die Danksagung, hat von Jesus die Form eines Mahles
              erhalten</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Der eucharistische Teil
                der Messe hat als Grundstruktur ein Mahl, das nicht der S&auml;ttigung dient, sondern im Ged&auml;chtnis
              an Jesus Christus gefeiert wird. Es untergliedert sich in drei
              Teile:<br>
              1.	Gabenbereitung<br>
              Brot und Wein werden zum Altar gebracht. W&auml;hrenddessen findet
              eine Geldsammlung (Kollekte) statt. Die Gabenbereitung schlie&szlig;t
              mit einem eigenen Gebet, dem Gabengebet.<br>
              2<a href="hochgebet.php">.	Hochgebet</a><br>
              Der Priester dankt im Namen der Gemeinde Gott f&uuml;r die Erl&ouml;sungstat
              Jesu (Pr&auml;fation), ruft den Heiligen Geist &uuml;ber Brot und
              Wein (Epiklese), spricht die Worte, die Jesus im Abendmahlssaal &uuml;ber
              Brot und Wein gesprochen hat (Einsetzungsworte), erinnert an Tod,
              Auferstehung, Himmelfahrt und Geistsendung (Anamnese), und bittet
              in verschiedenen Anliegen (Intercessionen). <br>
              Das Hochgebet schlie&szlig;t mit einem feierlichen Lobpreis, der
              Doxologie.<br>
              3. Der <a href="kommunion.php">Kommunionteil</a>, in dem die Feiernden das gewandelte Brot
              und an eingien Tagen auch den gewandelten Wein empfangen, wird
              mit dem Vater Unser und einer Fortf&uuml;hrung der Bitten des Vaterunsers,
              dem Embolismus eingeleitet. Es folgen ein Friedensgebet und der
              Friedensgru&szlig;. W&auml;hrend des Brotbrechens wird das Agnus
              Dei (Lamm Gottes, erbarme dich unser) gesungen. Dann empfangen
              die Feiernden Jesus Christus in den Gestalten von Brot und Wein.
              Auf die Kommunionausteilung folgt ein Danklied.</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Entlassung<br>
  An das Schlu&szlig;gebet, nach dem Ank&uuml;ndigungen f&uuml;r
              die Gemeinde ihren Platz haben, folgen Segen und Entlassung. Die
              Entlassung schlie&szlig;t nicht nur die Messe ab, sondern gibt
              ihr auch den Namen. Denn der Entlassungsruf &#8222;Gehet hin in
              Frieden&#8220; hei&szlig;t lateinisch &#8222;Ite, missa est&#8220;:
              Geht, es ist Sendung. Die Gl&auml;ubigen sollen gest&auml;rkt in
              dem Glauben, da&szlig; Gott die Menschen liebt und sie in einem
              gro&szlig;en, die Zeiten &uuml;berspannenden Erl&ouml;sungsgeschehen
              heimholt, sich neu im Alttag bew&auml;hren und die &uuml;berzeugen,
              mit denen sie w&auml;hrend der Woche in Kontakt kommen.</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Eckhard Bieger</font></p>
            <p></p>
            <p><font face="Arial, Helvetica, sans-serif"><a href="http://www.fernsehgottesdienst.de/">Hinweise zu den aktuellen Gottesdienst&uuml;bertragungen im ZDF</a></font></p>
            <p><br>
            </p>
            <p><font size="2" face="Arial, Helvetica, sans-serif"><a href="http://www.kath.de/">&copy; www.kath.de</a></font></p>
          </td>
          <td class="L12" width="1">            <div align="right"><br>
              <br>
              </div>
          </td>
          <td background="boxright.gif" width="9"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif" colspan="2"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td width="9"><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
</table>
</body>
</html>
