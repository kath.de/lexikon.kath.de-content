<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<meta name="title" content="Liturgie">
<meta name="author" content="Redaktion kath.de">
<meta name="publisher" content="kath.de">
<meta name="copyright" content="kath.de">
<meta name="description" content="">
<meta name="abstract" content="Lexikon �ber die Liturgie der katholischen Kirche">
<meta http-equiv="content-language" content="de">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="date" content="2006-00-01">
<meta name="robots" content="index,follow">
<meta name="revisit-after" content="10 days">
<meta name="revisit" content="after 10 days">
<meta name="DC.Title" content="Liturgie">
<meta name="DC.Creator" content="Redaktion kath.de">
<meta name="DC.Contributor" content="J�rgen Pelzer">
<meta name="DC.Rights" content="kath.de">
<meta name="DC.Publisher" content="kath.de">
<meta name="DC.Date" content="2006-00-01">
<meta name="DC.Description" content="Lexikon �ber die Liturgie der katholischen Kirche">
<meta name="DC.Language" content="de">
<meta name="DC.Type" content="Text">
<meta name="DC.Format" content="text/html">
<meta name="DC.Identifier" content="http://www.kath.de/lexikon/liturgie/">
<meta name="keywords" lang="de" content="Gemeisnchaft, Abendmahl, Embolismus, Agnus Dei, Lamm Gottes">
<title>Kommunion</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="kaltefleiter.css" title="fonts">
</head>
<body bgcolor="#ffffff" leftmargin="6" topmargin="6" marginwidth="6" marginheight="6">
<table width="100%" border="0" cellspacing="0" cellpadding="6">
  <tr>
    <td width="100" align="left" valign="top" height="3"> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b><font face="Arial, Helvetica, sans-serif">Das 
            Liturgie Lexikon</font></b></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td> <?php include("logo.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><font face="Arial, Helvetica, sans-serif"><strong>Inhaltsverzeichnis 
            </strong></font></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10"> <?php include("az.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
    <td rowspan="2" valign="top">
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td background="boxtop.gif" colspan="2"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="9"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif" width="8"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2" colspan="2"> 
            <h1><font face="Arial, Helvetica, sans-serif">Kommunion </font></h1>
          </td>
          <td background="boxtopright.gif" width="9"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif" colspan="2"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td width="9"><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif" width="8"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="L12" width="516"> 
            <p><strong><font face="Arial, Helvetica, sans-serif"><br>
            Kommunion hei&szlig;t Gemeinschaft</font></strong></p>
            <p><font face="Arial, Helvetica, sans-serif"> Kommunion
                hei&szlig;t, in eine Gemeinschaft eintreten. Das kann
                dadurch geschehen, da&szlig; man seine Gedanken austauscht und &Uuml;bereinstimmung
                erkennt. Andere M&ouml;glichkeiten der Kommunion sind der gemeinsame
                Gesang oder das gemeinsame Gebet. Diesen Weg in eine gr&ouml;&szlig;ere
                Gemeinschaft ist die Gottesdienstgemeinde bereits im ersten Teil
                der Messe gegangen. Im zweiten, im eucharistischen Teil der Messe
                wird die Gemeinschaft auf eine Person hin intensiviert. Das erm&ouml;glicht
                bereits das Mahl, die Grundgestalt der Eucharistie. Die Gl&auml;ubigen
                sind von Jesus zu einem Mahl eingeladen, das er zu seinem Ged&auml;chtnis
                eingesetzt hat. Wie bei anderen herausgehobenen M&auml;hlern, z.B.
                einer Geburtstagsfeier, soll das Mahl die Verbundenheit mit der
                zentralen Person, dem Geburtstagskind vertiefen. Deshalb mu&szlig; man
                Mitfeiernder nicht zahlen, sondern ist eingeladen. Der Einladende
                gibt von Seinem, um eine tiefere Gemeinschaft mit ihm zu erm&ouml;glichen.
                Anteil an sich selber geben wollte Jesus mit dem letzten Mahl,
                dem <a href="http://www.kath.de/Kirchenjahr/gruendonnerstag.php">Abendmahl</a>, das er mit seinen J&uuml;ngern gefeiert hat. Das,
                was zu diesem Mahl aufgetragen wurde, hatte die Gruppe, die sich
                um den Prediger aus Nazareth gesammelt hatte, wohl aus Spenden
                erhalten. Deshalb ist das, was Jesus gibt, nicht das Essen selbst.
                Er kn&uuml;pft an den Anla&szlig; f&uuml;r das Mahl an, das Ged&auml;chtnis
                der Juden an den Auszugs aus &Auml;gypten. Pascha ist das Fest
                der Befreiung aus dem Frondienst des Pharao. Dieses Mahl hat bis
                heute als Hauptgang ein gebratenes Lamm. Jesus hat dieses Mahl
                neu akzentuiert. Er hat Brot und den Becher mit Wein zu seinem
                Zentrum gemacht, so wie bis heute Brot und Wein im Zentrum der
                Eucharistie stehen. An die Stelle des Lammes, eines der Opfertiere
                im j&uuml;dischen Kult, ist Jesus selbst getreten. Er ist das Lamm
                Gottes, das die Schuld hinweg nimmt. &#8222;Lamm&#8220; hatte der
                Vorl&auml;ufer Jesu, Johannes der T&auml;ufer, den Messias genannt.
                (Johannesevangelium 1,29) <br>
              Weil Jesus sein Leben hingegeben hat, ist das <a href="eucharistie_messe_abendmahl.php">eucharistische
              Mahl</a>              unersch&ouml;pflich. Wer die gewandelten Gaben von Brot und Wein
                zu sich nimmt, tritt in eine tiefere Gemeinschaft mit dem Spender
                der Gaben ein. Jesus hat im Abendmahlssaal von sich gesagt, da&szlig; das
                Brot zu seinem Leib und der Wein zu seinem Blut wird. Was der Einladende
                bei einem Geburtstagsessen versucht, eine tiefere Gemeinschaft
                mit ihm als Person zu erschlie&szlig;en, das gelingt in der Eucharistiefeier
                in einer noch anderen Dimension, die dadurch gegeben ist, da&szlig; Jesus
                den Menschen mit seinem Vater, dem Sch&ouml;pfergott in eine tiefere
              Beziehung bringt. </font></p>            
            <p><font face="Arial, Helvetica, sans-serif">Wie bei einem Geburtstags-
                oder Hochzeitsessen vertieft sich auch die Gemeinschaft der Feiernden
                untereinander &#8211; und so entsteht
              aus jeder Mahlfeier die kirchliche Gemeinschaft neu.</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Die Bedeutung des Mahles
                hat sich bereits im <a href="hochgebet.php">Hochgebet</a> erschlossen,
                dem mittleren Teil der Eucharistiefeier, dem die Gabenbereitung
              vorausgegangen ist. Das meist vom Priester gesprochene Wort pr&auml;gt
              die Gestalt des eucharistischen Mahles </font></p>
            <p><font face="Arial, Helvetica, sans-serif">Anders als bei einem
                Geburtstagsessen ist das eucharistische Mahl in hohem Ma&szlig;e stilisiert. Die Gl&auml;ubigen
                empfangen das Brot in der Form einer kleinen Hostie, von dem
              Wein nur einen Schluck. </font></p>
            <p><font face="Arial, Helvetica, sans-serif">Aufbau des Kommunionteils der Messe<br>
              1.	Vater unser<br>
              2.	Embolismus<br>
              3.	Friedensgebet und Friedensgru&szlig;<br>
              4.	Brotbrechung mit Agnus-Dei-Gesang<br>
              5.	Pr&auml;sentation der eucharistischen Gaben<br>
              6.	Kommuniongang<br>
              7.	Danklied<br>
              8.	Schlu&szlig;gebet</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Zu 1: Der Kommunionteil
                der Messfeier wird durch das &#8222;Vater
              unser&#8220; eingeleitet, das von allen gebetet wird. Es dr&uuml;ckt
              die Bitte sowohl um das Kommen des Reiches Gottes wie um das t&auml;gliche
              Brot und die t&auml;glich neu notwendige Vergebung der pers&ouml;nlichen
              Schuld.<br>
              Zu 2: Die Bitten des &#8222;Vater unser&#8220; werden durch den
              sog. Embolismus erweitert. <br>
              Zu 3: Dann folgt ein Gebet um Frieden, das den Gedankengang, der
              der Messe insgesamt zugrunde liegt, weiterf&uuml;hrt: Das Heil,
              das Jesus geschenkt hat, m&uuml;ndet in einen Frieden und wird
              einem Friedensgru&szlig; ausgedr&uuml;ckt, der durch den Austausch
              des Friedensgru&szlig;es mit den Banknachbarn fortgesetzt wird.<br>
              Zu 4: Das Brotbrechen ist im Neuen Testament ein Wort, das das
              Ged&auml;chtnismahl Jesu bezeichnet. Es leitet sich davon her,
              da&szlig; ein gr&ouml;&szlig;eres St&uuml;ck Brot geteilt wurde.
              Diese Bedeutung ist heute kaum erlebbar, weil das Brot in die Hostien
              oder bei den orthodoxen Kirchen bereits in kleine St&uuml;cke geschnitten
              ist. Im Bericht &uuml;ber das erste Abendmahl hei&szlig;t es: &#8222;W&auml;hrend
              des Mahls nahm Jesus das Brot und sprach den Lobpreis; dann brach
              er das Brot, reichte es den J&uuml;ngern und sagte: Nehmt und e&szlig;t,
              das ist mein Leib.&#8220; (Matth&auml;us 26,26) Im Brechen des
              Brotes wird die Hinrichtung Jesu gesehen, sein Leib wurde durch
              die Gei&szlig;elung und die Kreuzigung &#8222;gebrochen&#8220;. <br>
              Zu 5: Die Gemeinde wird vom Priester mit folgenden Worten eingeladen:<br>
&#8222;
              Seht das Lamm Gottes, das hinwegnimmt die S&uuml;nden der Welt&#8220;.
              Der Satz ist dem Johannesevangelium entnommen (Kap. 1,29<br>
              Zu 6: Die Gl&auml;ubigen gehen nach vorne, um den Leib des Herrn, &#8222;die
              Kommunion&#8220; zu empfangen. Da es sich um eine Prozession handelt,
              geh&ouml;rt ein Wechselgesang zum Kommuniongang, der im Gregorianischen
              Choral &#8222;Communio&#8220; genannt wird.<br>
              Zu 7: Wie es bei einer Geburtstagsfeier einen Tag an den Einladenden
              gibt, dankt auch die Gemeinde f&uuml;r die empfangende Gabe durch
              ein Lied.<br>
              Zu 8: Der Kommunionteil der Messe wird durch ein Gebet abgeschlossen,
              das im Lateinischen &#8222;Postcommunio&#8220; hei&szlig;t. Das
              Gebet beinhaltet Dank und leitet auf die Entlassung aus der Feier &uuml;ber,
              da&szlig; die Gl&auml;ubigen, das, was sie empfangen haben, im
              Alltag verwirklichen.</font></p>
            <p><font face="Arial, Helvetica, sans-serif">F&uuml;r Menschen, die nicht
                zur Kommunion gehen k&ouml;nnen, weil sie z.B. die Messe am Bildschirm
                verfolgen, sollte eine <a href="kommunionmeditation.php">Kommunionmeditation</a> angeboten werden.</font></p>
            <p><font face="Arial, Helvetica, sans-serif"><a href="messe_aufbau.php">Zum Gesamtaufbau der
                Messe</a></font></p>
            <p><font face="Arial, Helvetica, sans-serif">Zitate:</font></p>
            <p><font size="2" face="Arial, Helvetica, sans-serif">Embolismus:</font><font face="Arial, Helvetica, sans-serif"><br>
  Erl&ouml;se uns Herr, allm&auml;chtiger Vater, von allem B&ouml;sen<br>
  Und gib Frieden in unseren Tagen. Komm uns zu Hilfe mit deinem
              Erbarmen und bewahre uns vor Verwirrung und S&uuml;nde,<br>
              damit wir voll Zuversicht das Kommen unseres Erl&ouml;sers Jesus
              Christus erwarten.<br>
              Denn dein ist das Reich und die Kraft und die Herrlichkeit in Ewigkeit.
              Amen</font></p>
            <p><font size="2" face="Arial, Helvetica, sans-serif">Das Friedensgebet, wie es in der Osterzeit gebetet wird:</font><font face="Arial, Helvetica, sans-serif"><br>
  Am Ostertag trat Jesus in die Mitte seiner J&uuml;nger und sprach
              den Friedensgru&szlig;.<br>
              Deshalb bitten wir:<br>
              Herr Jesu, du Sieger &uuml;ber S&uuml;nde und Tod,<br>
              schau nicht auf unsere S&uuml;nden,<br>
              sondern auf den Glauben deiner Kirche<br>
              und schenke ihr nach deinem Willen Einheit und Frieden.<br>
              Der Priester breitet die H&auml;nde aus und ruft:<br>
              Der Friede des Herrn sei allezeit mit euch.<br>
              Gemeinde: Und mit deinem Geiste.<br>
              Priester oder Diakon: Gebt einander ein Zeichen des Friedens und
              der Vers&ouml;hnung.<br>
              Die Gl&auml;ubigen tauschen den Friedensgru&szlig; aus.</font></p>
            <p><font face="Arial, Helvetica, sans-serif"><br>
                <font size="2">W&auml;hrend des Brotbrechens wird ein dreifacher Ruf gesprochen
              oder gesungen, der in eine Friedensbitte m&uuml;ndet:</font><br>
              Lamm Gottes, du nimmst hinweg die S&uuml;nden der Welt,<br>
              erbarme dich unser.<br>
              Lamm Gottes, du nimmst hinweg die S&uuml;nden der Welt,<br>
              erbarme dich unser<br>
              Lamm Gottes, du nimmst hinweg die S&uuml;nden der Welt,<br>
              gib uns deinen Frieden.</font></p>
            <p><font size="2" face="Arial, Helvetica, sans-serif">Lateinisch:</font><font face="Arial, Helvetica, sans-serif"><br>
              Agnus Dei, qui tollis peccata mundi, miserere nobis<br>
              Agnus Dei, qui tollis peccata mundi, miserere nobis<br>
              Agnus Dei, qui tollis peccata mundi, dona nobis pacem</font></p>
            <p><font size="2" face="Arial, Helvetica, sans-serif">Schlu&szlig;gebet
                (Postcommunio) vom Pfingstsonntag</font><font face="Arial, Helvetica, sans-serif"><br>
              Der Priester betet: <br>
              Lasset und beten:<br>
              Herr, unser Gott,<br>
              du hast deine Kirche mit himmlischen Gaben beschenkt.<br>
              Erhalte ihr deine Gnade,<br>
              damit die Kraft aus der H&ouml;he, der Heilige Geist,<br>
              in ihr weiterwirkt und die geistliche Speise sie n&auml;hrt bis
              zur Vollendung.<br>
              Darum bitten wir durch Christus, unseren Herrn.<br>
              Gemeinde: Amen</font></p>
            <p><font size="2" face="Arial, Helvetica, sans-serif">Schlu&szlig;gebet
                vom 8. Sonntag im Jahreskreis</font><font face="Arial, Helvetica, sans-serif"><br>
              Barmherziger Gott,<br>
              du hast uns in diesem Mahl die Gabe des Heils geschenkt.<br>
              Dein Sakrament gebe uns Kraft in dieser Zeit<br>
              Und in der kommenden Welt das ewige Leben.<br>
              Darum bitten wir durch Christus, unseren Herrn.<br>
              Amen</font></p>
            <p><font size="2" face="Arial, Helvetica, sans-serif">Schlu&szlig;gebet
                vom 16. Sonntag</font><font face="Arial, Helvetica, sans-serif"><br>
  Barmherziger Gott, h&ouml;re unser Gebet.<br>
              Du hast uns im Sakrament das Brot des Himmels gegeben,<br>
              damit wir an Leib und Seele gesunden.<br>
              Gib, da&szlig; wir die Gewohnheiten des alten Menschen ablegen<br>
              Und als neue Menschen leben.<br>
              Darum bitten wir durch Christus, unsern Herrn.<br>
              Amen</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Hinweise zu den aktuellen <a href="http://www.fernsehgottesdienst.de/">Gottesdienst&uuml;bertragungen im ZDF</a></font></p>
            <p><font size="2" face="Arial, Helvetica, sans-serif"><a href="http://www.kath.de/">&copy; www.kath.de</a></font></p>
          </td>
          <td class="L12" width="1">            <div align="right"><br>
              <br>
              </div>
          </td>
          <td background="boxright.gif" width="9"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif" colspan="2"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td width="9"><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
</table>
</body>
</html>
