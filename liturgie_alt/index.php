<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title>Das Liturgie Lexikon</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="kaltefleiter.css" title="fonts">
</head>
<body bgcolor="#ffffff" leftmargin="6" topmargin="6" marginwidth="6" marginheight="6">
<table width="100%" border="0" cellspacing="0" cellpadding="6">
  <tr>
    <td width="100" align="left" valign="top" height="3"> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b><font face="Arial, Helvetica, sans-serif">Das 
            Liturgie Lexikon</font></b></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td> <?php include("logo.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><font face="Arial, Helvetica, sans-serif"><strong>Inhaltsverzeichnis 
            </strong></font></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10"> <?php include("az.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
    <td rowspan="2" valign="top">
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td background="boxtop.gif" colspan="2"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif" width="8"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2" colspan="2"> 
            <h1><font face="Arial, Helvetica, sans-serif">Das Liturgie Lexikon</font></h1>
          </td>
          <td background="boxtopright.gif" width="8"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif" colspan="2"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td width="8"><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif" width="8"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="L12" width="70%"> 
            <p><strong><font face="Arial, Helvetica, sans-serif"><br>
              Zur Einf&uuml;hrung: </font></strong></p>
            <p><font face="Arial, Helvetica, sans-serif">F&uuml;r viele Zeitgenossen
                 stellen die liturgischen Vollz&uuml;ge der kath. Kirche ein
                 Buch  mit sieben Siegeln dar. Das liegt nicht zuletzt daran,
                 dass von 
              Theologen allzuviel Grundwissen vorausgesetzt wird. Dadurch geht
                  der Zugang zu den Schatzkammern der Liturgie verloren. Dieses
                 Lexikon 
              will durch seine Einfachheit dazu beitragen, den Zugang freizulegen.
                  Begeben Sie sich auf eine Entdeckungsreise, die f&uuml;r Sie
                  so  manch &uuml;berraschende, erstaunende Erfahrung bereith&auml;lt.
              Lernen Sie die Handlungen der kath. Kirche in den Gottesdiensten
              in ihrer tieferen Bedeutung kennen und nicht zuletzt: Entdecken
                  Sie wie Theologie in Stein gegossen und in Handlungen und
                  Ritualen sichtbar wird. Lassen Sie sich &uuml;berraschen wie dahinter
                  das je gr&ouml;&szlig;ere Licht des Glaubens aufleuchtet...</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Ihr<br>
  J&uuml;rgen Pelzer</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Feedback bitte an mit 
              dem Stichwort Liturgie-Lexikon an: redaktion@kath.de</font></p>
          </td>
          <td class="L12" width="30%">
            <div align="right"><br>
              <br>
              <img src="photocase676176763.jpg" width="340" height="255"><font size="2" face="Arial, Helvetica, sans-serif"><br>
              Die Spitze des K&ouml;lner Domes</font></div>
          </td>
          <td background="boxright.gif" width="8"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif" colspan="2"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
<td width="8"><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
 </tr> 
</table> 
</td>
<!-- neuer Code Anfang -->
<td style='vertical-align:top;'> 
     <script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
/* Symbollexikon neu */
google_ad_slot = "9464926645";
google_ad_width = 120;
google_ad_height = 600;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
     <br>
</td>
<!-- neuer Code Ende -->

  </tr>
  <tr>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
</table>
</body>
</html>
