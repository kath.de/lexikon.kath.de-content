<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<meta name="title" content="Liturgie">
<meta name="author" content="Redaktion kath.de">
<meta name="publisher" content="kath.de">
<meta name="copyright" content="kath.de">
<meta name="description" content="">
<meta name="abstract" content="Lexikon �ber die Liturgie der katholischen Kirche">
<meta http-equiv="content-language" content="de">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="date" content="2006-00-01">
<meta name="robots" content="index,follow">
<meta name="revisit-after" content="10 days">
<meta name="revisit" content="after 10 days">
<meta name="DC.Title" content="Liturgie">
<meta name="DC.Creator" content="Redaktion kath.de">
<meta name="DC.Contributor" content="J�rgen Pelzer">
<meta name="DC.Rights" content="kath.de">
<meta name="DC.Publisher" content="kath.de">
<meta name="DC.Date" content="2006-00-01">
<meta name="DC.Description" content="Lexikon �ber die Liturgie der katholischen Kirche">
<meta name="DC.Language" content="de">
<meta name="DC.Type" content="Text">
<meta name="DC.Format" content="text/html">
<meta name="DC.Identifier" content="http://www.kath.de/lexikon/liturgie/">
<meta name="keywords" lang="de" content="Logik, Zeichensysteme, Grammatik, Symbol, sprachliche Zeichen">
<title>Zeichen verstehen</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="kaltefleiter.css" title="fonts">
</head>
<body bgcolor="#ffffff" leftmargin="6" topmargin="6" marginwidth="6" marginheight="6">
<table width="100%" border="0" cellspacing="0" cellpadding="6">
  <tr>
    <td width="100" align="left" valign="top" height="3"> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b><font face="Arial, Helvetica, sans-serif">Das 
            Liturgie Lexikon</font></b></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td> <?php include("logo.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><font face="Arial, Helvetica, sans-serif"><strong>Inhaltsverzeichnis 
            </strong></font></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10"> <?php include("az.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
    <td rowspan="2" valign="top">
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td background="boxtop.gif" colspan="2"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="9"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif" width="8"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2" colspan="2"> 
            <h1><font face="Arial, Helvetica, sans-serif">Zeichen verstehen</font></h1>
          </td>
          <td background="boxtopright.gif" width="9"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif" colspan="2"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td width="9"><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif" width="8"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="L12" width="516"> 
            <p><strong><font face="Arial, Helvetica, sans-serif">              Zeichen wirken nicht, sie werden verstanden</font></strong><font face="Arial, Helvetica, sans-serif"><br>
              <br>
              Ein Zeichen steht f&uuml;r etwas anderes. Es ist ein Zeichen &#8222;f&uuml;r&#8220;,
              wenn z.B. eine bestimmte Lautfolge M-E-N-SCH f&uuml;r Mensch steht.
              Ein Verkehrszeichen steht auch f&uuml;r etwas, z.B. ein Dreieck,
              das auf den Kopf gestellt ist, weist darauf hin, da&szlig; eine
              Querstra&szlig;e kommt, auf der die Fahrer f&uuml;r sich die Vorfahrt
              beanspruchen d&uuml;rfen. Das auf den Kopf gestellte Dreieck ist
              aber nicht nur eine Information &uuml;ber eine Querstra&szlig;e,
              die den eigenen Weg kreuzt, sondern beinhaltet auch eine Aufforderung,
              n&auml;mlich so lange zu warten, bis auf der Vorfahrtsstra&szlig;e
              keine Auto kommt und dann erst die Kreuzung zu passieren. <br>
              Es gibt weiter Zeichen von etwas, z.B. wenn etwas Spuren hinterlassen
              hat. So gibt es Fu&szlig;stapfen, die ein Zeichen davon sind, da&szlig; jemand
              den Weg gegangen ist. An einer Wasserpf&uuml;tze in einer Dusche
              kann ich erkennen, da&szlig; vor mir jemand die Dusche benutzt
              hat. G&auml;hnen ist ein Zeichen von M&uuml;digkeit oder wenn ich
              an meinem Gespr&auml;chspartner beobachte, da&szlig; ihm die Augen
              zufallen. <br>
              Zeichen k&ouml;nnen auch eine bestimmte Anordnung haben. So entspricht
              dem roten Dreieck an einer Stra&szlig;enkreuzung ein gelb umrandetes
              Viereck auf der Stra&szlig;e, die Vorfahrt hat. Dieser Zusammenhang
              liegt in der Logik der Sache, hier der Vorfahrtsregelung durch
              Verkehrszeichen. Eine Logik ist auch mit dem Gebrauch der Wortzeichen
              verbunden. So ist die Aussage &#8222;Mensch&#8220; in der Regel
              nicht vollst&auml;ndig. Entweder mu&szlig; ein Satz um das Wort
              gebaut werden, z.B. &#8222;Der Mensch ist eitel.&#8220; Oder es
              handelt sich um einen Ausruf, in dem eine Bedeutung mitschwingt,
              z.B. Mensch, Hannes&#8220; der im jeweiligen Kontext bedeuten kann: &#8222;Stell
              dich nicht so an!&#8220; Oder &#8222;Mach endlich voran!&#8220; Die
              Logik, wie Zeichen verbunden werden m&uuml;ssen, nennen wir in
              der Sprache &#8222;Grammatik&#8220;.<br>
&#8222;
              Zeichen von&#8220; sind in der Regel Spuren, die ein bestimmter
              Vorgang, ein anderer Mensch hinterlassen. Wer diese Zeichen sieht,
              h&ouml;rt, riecht, kann sie deuten, weil er sie in den Zusammenhang
              stellt, z.B. weil er wei&szlig;, da&szlig; nach dem Duschen noch
              etwas Wasser auf dem Boden der Dusche und des Badezimmers zur&uuml;ckbleibt.<br>
              Bei vom Menschen gesetzten Zeichen, der Sprache wie auch Verkehrszeichen,
              mu&szlig; man die Grammatik gelernt haben, z.B. da&szlig; das rot
              umrandete Dreieck als Pendant das gelb umrandete Viereck hat.<br>
              Die Religionen haben auch Zeichensysteme entwickelt, mit denen
              sie ihre Gottesdienste gestalten. Im christlichen Kontext bezeichnet
              das Wort &#8222;Liturgie&#8220; das Zeichensystem, aus dem ein
              <a href="http://www.kath.de/lexikon/philosophie_theologie/sohn_gottes.php"> </a>Gottesdienst
              gebaut wird. Zeichen im Gottesdienst sind z.B. eine Kniebeuge,
              zum Gebet
              erhobene Arme, bestimmte Melodien, eine Prozession.
              Innerhalb des Gottesdienstes gibt es auch viele sprachliche Zeichen,
              die sich verschiedenen Gattungen zuordnen lassen, z.B. Lobpreis,
              F&uuml;rbittgebet, Wechselges&auml;nge, Berichte und Erz&auml;hlungen,
              die vorgelesen werden. Aus den Geb&auml;rden, Prozessionen und
              Texten wird der Gottesdienst &#8222;gebaut&#8220;. Wie dem Aufbau
              (Aufbau der Messe) der Sprache liegt auch einem <a href="messe_aufbau.php">Gottesdienst</a> eine
              Grammatik zugrunde, die nicht verletzt werden darf, soll der Gottesdienst
              f&uuml;r die Feiernden mit vollziehbar werden. Innerhalb der Liturgie
              gibt es besonders herausgehobene Zeichen, die nicht nur, wie eine
              Gebetshaltung, auf etwas hinweisen, sondern eine gr&ouml;&szlig;ere
              Bedeutungstiefe haben. Im christlichen Gottesdienst sind das die
              heiligen &Ouml;le, die Handauflegung, der Segen, Brot und Wein.
              Diese Zeichen haben sakramentalen Charakter und sind daher <a href="symbol_funktion.php">Symbole</a>.</font></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Zeichen wirken nicht, sondern setzen Verstehen voraus:</strong><br>
  Zeichen &#8222;funktionieren&#8220; nicht physikalisch, sie haben
              also keine kausale Wirkung, sondern der Empf&auml;nger mu&szlig; Bedeutung
              des Zeichens verstehen. Das kann er z.B., wenn er die Bedeutung
              der Verkehrszeichen &#8222;gelernt&#8220; hat. Es gibt auch Ziechen,
              die wir ohne Lernen verstehen. So versteht ein Baby bereits das
              L&auml;cheln und antwortet darauf mit L&auml;cheln. M&ouml;wenk&uuml;ken
              reagieren auf den roten Punkt am Schnabel der Eltern. Sie &ouml;ffnen
              ihre Schn&auml;bel allerdings auch dann, wenn man ihnen eine Holzstange
              mit einem roten Punkt vor die Augen h&auml;lt. Es k&ouml;nnte sein,
              da&szlig; Tiere die Bedeutung der Zeichen nicht lernen m&uuml;ssen,
              sondern da&szlig; diese ihnen instinktm&auml;&szlig;ig mit auf
              den Weg gegeben ist. Tiere k&ouml;nnen aber auch die Bedeutung
              von Zeichen lernen, z.B. fliegen Spatzen schnell weg, wenn jemand
              mit einem Gewehr auftaucht, Hunde und Katzen verstehen, da&szlig; es
              Futter gibt, wenn eine Gabel an den Fre&szlig;napf schl&auml;gt.<br>
              Die Zeichen einer Sprache m&uuml;ssen wir lernen. Gleiches gilt
              f&uuml;r die vielen Zeichensystem einer Kultur, wie man mit Messer
              und Gabel i&szlig;t, wie man sich in einem Museum bewegt, Tanzen,
              ein Instrument spielen, wie die mathematischen Zeichen, vor allem
              wie die Zahlen funktionieren. Je mehr Zeichensystem eine Kultur
              entwickelt hat, desto l&auml;nger sind Schuld- und Studienzeiten.
              Auch das Zeichensystem einer Religion mu&szlig; man lernen, die
              <a href="ritus_sinnhandlung.php">Riten</a>, die Gebetsformen, wie man meditiert, und &uuml;berhaupt
              die Bedeutung vieler Worte wie &#8222;Prophet&#8220;, &#8222;<a href="http://www.kath.de/lexikon/philosophie_theologie/sohn_gottes.php">Sohn
              Gottes</a>&#8220;, &#8222;Gnade&#8220;, &#8222;Sakrament&#8220;. Dieses
              Begriffslexikon &uuml;ber Liturgie erkl&auml;rt die Zeichen und
              Zeichensysteme f&uuml;r die verschiedenen Gottesdienste.</font><br>
              <font size="2" face="Arial, Helvetica, sans-serif">Eckhard Bieger</font><br>
            </p>
            <p><font size="2" face="Arial, Helvetica, sans-serif"><a href="http://www.kath.de/">&copy; www.kath.de</a></font></p>
          </td>
          <td class="L12" width="1">            <div align="right"><br>
              <br>
              </div>
          </td>
          <td background="boxright.gif" width="9"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif" colspan="2"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td width="9"><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
</table>
</body>
</html>
