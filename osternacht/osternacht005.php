<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title>Osternacht und Altes Testament - W&uuml;ste und verhei&szlig;enes Land</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="osternacht.css" rel="stylesheet" type="text/css">
</head>
<body bgcolor="#ffffff" leftmargin="6" topmargin="6" marginwidth="6" marginheight="6">
<table width="100%" border="0" cellspacing="0" cellpadding="6">
  <tr>
    <td width="100" align="left" valign="top" height="3"> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="../lexikon/liturgie/boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="../lexikon/liturgie/boxtop.gif"><img src="../lexikon/liturgie/boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="../lexikon/liturgie/boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="../lexikon/liturgie/boxtopleft.gif"><img src="../lexikon/liturgie/boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b><font face="Arial, Helvetica, sans-serif">Lexikontitel</font></b></td>
          <td background="../lexikon/liturgie/boxtopright.gif"><img src="../lexikon/liturgie/boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="../lexikon/liturgie/boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="../lexikon/liturgie/boxdivider.gif"><img src="../lexikon/liturgie/boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="../lexikon/liturgie/boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="../lexikon/liturgie/boxleft.gif"><img src="../lexikon/liturgie/boxleft.gif" width="8" height="8" alt=""></td>
          <td> <?php include("logo.html"); ?> </td>
          <td background="../lexikon/liturgie/boxright.gif"><img src="../lexikon/liturgie/boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="../lexikon/liturgie/boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="../lexikon/liturgie/boxbottom.gif"><img src="../lexikon/liturgie/boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="../lexikon/liturgie/boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="../lexikon/liturgie/boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="../lexikon/liturgie/boxtop.gif"><img src="../lexikon/liturgie/boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="../lexikon/liturgie/boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="../lexikon/liturgie/boxtopleft.gif"><img src="../lexikon/liturgie/boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><font face="Arial, Helvetica, sans-serif"><strong>Inhaltsverzeichnis 
            </strong></font></td>
          <td background="../lexikon/liturgie/boxtopright.gif"><img src="../lexikon/liturgie/boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="../lexikon/liturgie/boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="../lexikon/liturgie/boxdivider.gif"><img src="../lexikon/liturgie/boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="../lexikon/liturgie/boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="../lexikon/liturgie/boxleft.gif"><img src="../lexikon/liturgie/boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10"> <?php include("az.html"); ?> </td>
          <td background="../lexikon/liturgie/boxright.gif"><img src="../lexikon/liturgie/boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="../lexikon/liturgie/boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="../lexikon/liturgie/boxbottom.gif"><img src="../lexikon/liturgie/boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="../lexikon/liturgie/boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
    <td rowspan="2" valign="top">
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="../lexikon/liturgie/boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td background="../lexikon/liturgie/boxtop.gif" colspan="2"><img src="../lexikon/liturgie/boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="../lexikon/liturgie/boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="../lexikon/liturgie/boxtopleft.gif" width="8"><img src="../lexikon/liturgie/boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2" colspan="2"> 
            <h1>W&uuml;ste und verhei&szlig;enes Land</h1>
          </td>
          <td background="../lexikon/liturgie/boxtopright.gif" width="8"><img src="../lexikon/liturgie/boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8"><img src="../lexikon/liturgie/boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="../lexikon/liturgie/boxdivider.gif" colspan="2"><img src="../lexikon/liturgie/boxdivider.gif" alt="" width="8" height="13"></td>
          <td width="8"><img src="../lexikon/liturgie/boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="../lexikon/liturgie/boxleft.gif" width="8"><img src="../lexikon/liturgie/boxleft.gif" width="8" height="8" alt=""></td>
          <td class="L12" width="70%">		  <p><strong><font face="Arial, Helvetica, sans-serif">Vom Alten Testament einen neuen
              Blick auf die Fastenzeit werfen</font></strong></p>
		    <p class="text">Die Osterfestzeit baut sich in konzentrischen Kreisen
		        auf. Es geht jetzt um den gr&ouml;&szlig;ten und umfangenden Kreis, der am Aschermittwoch
		      beginnt und an Pfingsten endet. Auch er ist, wie die Tage direkt um Ostern
		      herum, noch locker von der Ebene des Lebens Jesu mitbestimmt. An den
		      ersten Fastensonntagen werden Schl&uuml;sselevangelien aus dem &ouml;ffentlichen
		      Leben Jesu gelesen, nach Ostern steuert die Zeit auf das Fest der Himmelfahrt
		      Jesu zu, und schlie&szlig;lich auf das Pfingstfest, den Tag der Sendung
		      des Geistes. Aber in diesem umfassenden Kreis treten im ganzen
	      eher andere Dinge in den Vordergrund.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        </p>
		    <p class="text"><span class="&uuml;berschrift">Neuentstehen der Kirche</span><br>
  Da ist einmal in der Zeit vor Ostern das Thema &#8222;Taufvorbereitung&#8220; f&uuml;r
		      die neu zur Kirche Kommenden und das Thema &#8222;Bu&szlig;zeit&#8220; f&uuml;r
		      die schon Getauften. In der Zeit zwischen Ostern und Pfingsten gibt es
		      die erstaunliche Tatsache, da&szlig; das &ouml;sterliche Fest gegen alles,
		      was sonst gerade an Vor&uuml;bergehendem und nicht mehr Wiederholbarem
		      zu einem Fest geh&ouml;rt, sich wie ein Vorgeschmack des ewigen Lebens
		      im Reiche Gottes sieben mal sieben Tage lang durchh&auml;lt. <br>
		      In diesem &auml;u&szlig;eren Kreis der Osterzeit richtet sich der Blick
		      nicht mehr nur auf Jesus allein, oder auf mich, den Einzelnen, und meine
		      Beziehung zu Jesus (was nat&uuml;rlich auch in den inneren Kreisen niemals
		      wirklich der Fall war), sondern es geht um die Kirche &#8211; eine neue
		      Gesellschaft, der wir durch Jesu Tod und Auferstehung angeh&ouml;ren.
		      Sie erw&auml;chst wieder neu in den 40 vorangehenden Tagen des Hinzutretens
		      der Katechumenen und der Heimkehr der B&uuml;&szlig;er, sie zeigt sich
		      in ihrer Sch&ouml;nheit in den 50 Tagen bis zum Pfingstfest, in denen
		      immer wieder durch die Apostelgeschichte die fr&uuml;he Kirche vor Augen
		      steht. <br>
		      Das andere, was in dem umfassenden Kreis der Osterfestzeit in den
		      Vordergrund tritt, ist die typologische Sinndimension, die das &ouml;sterliche
		      Festgeheimnis vom Alten Testament her bekommt. Auch diese Dimension steht
		      kaum im Bewu&szlig;tsein des heutigen Christen. Den Kirchenv&auml;tern
		      und der alten Kirche war sie umso wichtiger. </p>
		    <p class="text"><span class="&uuml;berschrift">Die beiden Osterfeste Israels</span><br>
  Sobald das Osterfest erreicht ist, zeigt sich in der Liturgie ein
		        seltsamer Umsprung in der Perspektive. Wir k&ouml;nnen uns dem Ph&auml;nomen
		        durch folgende Frage n&auml;hern: Welches Osterfest, das die Bibel
		        schildert, entspricht eigentlich unserem Osterfest, das wir j&auml;hrlich
		        feiern &#8211; das Ostern des Auszugs aus &Auml;gypten (vgl. Exodus
		        12&#8211;15), oder das Ostern 40 Jahre sp&auml;ter, nach der Zeit in
		        der W&uuml;ste und nach der &Uuml;berquerung des Jordan, beim Beginn
		        des Einzugs in das verhei&szlig;ene Land (vgl. Josua 3&#8211;5)? <br>
	          Zwischen diesen beiden Osterfeiern Israels, die uns die Bibel
		        erz&auml;hlt,
		      liegen die 40 Jahre in der W&uuml;ste. Das ist keine unschuldige und
		      gewisserma&szlig;en naturnotwendig eingetretene Zeit. Israel sollte vielmehr
		      nach dem Auszug aus &Auml;gypten sofort vom Sinai aus in das den V&auml;tern
		      verhei&szlig;ene Land einziehen. Dann h&auml;tte es nur ein Ostern des
		      Auszugs aus &Auml;gypten gegeben, und nach ihm den Einzug ins Gl&uuml;ck.
		      Doch die S&uuml;nde schob sich dazwischen. Israel weigerte sich an der
		      Grenze des verhei&szlig;enen Landes, dort einzuziehen. Es verlor den
		      Glauben und wollte nach &Auml;gypten zur&uuml;ck. Nach &Auml;gypten schickte
		      Gott es nicht zur&uuml;ck, wohl aber in die W&uuml;ste. Es blieb in der
		      W&uuml;ste 40 Jahre lang, bis die ganze Generation der S&uuml;nder ausgestorben
		      war. Erst eine neue Generation konnte dann in das Land einziehen.
		      Ihr Einzug begann wieder mit einer Wasserdurchquerung, dem Zug
		      durch den
		      Jordan, dessen Wasser sich stauten. Dann folgte eine Osterfeier
		      am anderen Ufer. </p>
		    <p class="text"><span class="&uuml;berschrift">Der zweifache Sinn des christlichen Osterfestes</span><br>
  Nun wollen die 40 Tage unserer &ouml;sterlichen Bu&szlig;zeit zweifellos
		      den 40 Jahren Israels in der W&uuml;ste entsprechen. Unsere Liturgie
		      macht das dadurch deutlich, da&szlig; sie am ersten Fastensonntag das
		      Evangelium von den 40 Tagen Jesu in der W&uuml;ste und von seiner Versuchung
		      durch Satan lesen l&auml;&szlig;t. Das hei&szlig;t aber: Das Ostern,
		      auf das wir in den 40 Tagen der &ouml;sterlichen Bu&szlig;zeit zugehen,
		      ist das Ostern am Jordan, nicht eigentlich das der Nacht des Auszugs
		      aus &Auml;gypten. Doch in unserer Osternacht kommt dann die &Uuml;berraschung.
		      Da feiern wir nicht das Ostern am Jordan, sondern das des Auszugs. Alles
		      in den Texten der Nacht ist auf den Auszug aus &Auml;gypten abgestimmt.<br>
		      Das ist nun genau angemessen f&uuml;r die T&auml;uflinge. Sie verlassen
		      in der Osternacht das &Auml;gypten ihrer alten Existenz, durchqueren
		      in der Taufe das Wasser und ziehen ein in das neue Land. Die Gemeinde
		      der Gl&auml;ubigen mu&szlig; diesen Auszug eigentlich nicht mehr machen.
		      Sie begleitet nur die neu Hinzukommenden. Sie selbst hat ihr Ostern schon
		      hinter sich. Allerdings: Sie ist dem schon geschehenen Auszug aus der
		      alten Welt nicht gerecht geworden. Die S&uuml;nde ist auch hier dazwischen
		      gekommen. So ist den schon vor Zeiten Getauften ebenso wie damals dem
		      Israel des Anfangs die Zeit der W&uuml;ste gew&auml;hrt, die 40 Tage
		      der Bu&szlig;e und Umkehr. Sie k&ouml;nnen wie Israels W&uuml;stengeneration
		      am Ende mit einem neuen Ostern rechnen, dem am Jordan. Die Osternacht
		      wird auch f&uuml;r die, die ihren Auszug schon lange hinter sich haben,
		      wieder ein echtes Osterfest vor dem Einzug ins Land. Ostern am Schilfmeer
		      und Ostern am Jordan wachsen zusammen. Vom Ostertag an ist es ein einziges
		      Ostern f&uuml;r alle und er&ouml;ffnet allen wie neu das Leben im Land
		      der Verhei&szlig;ung. Es kann dann in der Zeit bis Pfingsten dargestellt
		      werden in den Lesungen aus der Apostelgeschichte.</p>
		    <p class="text"><span class="&uuml;berschrift">Wurzeln im Alten Testament</span><br>
  Diese Dialektik zwischen dem Ostern des Auszugs und dem Ostern
		        am Jordan ist nicht erst von unserer Liturgie konstruiert. Sie
      steckt schon in den alttestamentlichen Schriften selbst. Die Mehrzahl der
		        berichteten Osterbegehungen, die ganze Schilderung der W&uuml;stenzeit
		        Israels &#8211; all das ist nichts als die Aufarbeitung der Tatsache,
		        da&szlig; Israel in ein Land gef&uuml;hrt werden sollte und da&szlig; es
		        des Landes nicht w&uuml;rdig war. Die 40 Jahre der W&uuml;ste sind
		        Israels einzige Hoffnung. Deshalb sind die 5 B&uuml;cher Moses, vor
		        allem aber die B&uuml;cher von Exodus bis Numeri, auf andere Weise
		        dann noch das Buch Deuteronomium, auch bei uns die f&uuml;r diese Zeit
		        des Kirchenjahres angemessenste Lesung aus der Bibel. <br>
	          Das Judentum wei&szlig; sich seit dem babylonischen Exil, das ja den
		      Verlust des schon besessenen Landes bedeutete, immer noch vor dem Ufer
		      des Jordans und liest das Jahr hindurch als Hauptlesung in der Synagoge &uuml;berhaupt
		      nur die 5 B&uuml;cher Moses, niemals das Buch Josua mit der Erz&auml;hlung
		      vom Einzug. Es wartet immer noch darauf, da&szlig; ihm das zweite Ostern
		      gew&auml;hrt wird. <br>
		      Die Kirche ist &uuml;berzeugt, da&szlig; Jesus sie ins Land der Verhei&szlig;ung
		      gef&uuml;hrt hat. Doch zugleich ist sie bis zur Wiederkunft des Herrn
		      in jener eigent&uuml;mlichen Zwischenzeit, in der auch sie noch in der
		      W&uuml;ste wandert und immer wieder auf ein neues Ostern zugehen mu&szlig;,
		      weil das erste immer wieder verspielt wird. Ein neues Ostern jedoch,
		      das die ganze Kraft des alten in sich besitzt. Uralte Liturgien, die
		      noch in unsere Zeit hineinragen oder die wir noch aus alten Handschriften
		      kennen, lesen deshalb in der Fastenzeit aus den B&uuml;chern Moses, so
		      etwa die ambrosianische Liturgie, die noch in Mailand gefeiert wird,
		      und die alte Liturgie von Jerusalem, die wir noch aus einer armenischen
		      Handschrift kennen. In unserer heutigen r&ouml;mischen Liturgie leuchtet
		      es nur noch hin und wieder auf, da&szlig; wir in den 40 Tagen der &ouml;sterlichen
		      Bu&szlig;zeit die Existenz Israels in den 40 Jahren der W&uuml;ste liturgisch
		      wiederholen. <br>
		      Eines ist aber gegen&uuml;ber dem j&uuml;dischen Selbstverst&auml;ndnis
		      wichtig: In der Zeit von Ostern bis Pfingsten lesen wir, obwohl
		      wir glauben, da&szlig; wir mit Jesus den Jordan &uuml;berschritten haben,
		      nicht das Buch Josua. Auch die Juden lesen es, wie gesagt, nicht. Doch
		      sie tun
		      es nicht, weil sie sich noch vor dem neuen Einzug ins Land f&uuml;hlen.
		      Wir glauben, da&szlig; der Einzug begonnen hat. Aber nicht als der Einzug
		      des Buches Josua, dessen Landnahme keine Dauer hatte, sondern als
		      der Einzug in jenes Land, das sich in der Kirche verwirklicht. Deshalb
		      lesen
		      wir zwischen Ostern und Pfingsten die Apostelgeschichte, deren
		      Geschichten f&uuml;r unser Heute gelten.</p>
		    <p class="text">Texte: Georg Braulik, Norbert Lohfink</p>		    
		    <p class="text"><font face="Arial, Helvetica, sans-serif">	          <span class="text">Weitere Informationen:<br>
	          <a href="http://www.peterlang.com/index.cfm?vID=51819&vLang=D&vHR=1&vUR=3&vUUR=4" target="_blank">Georg
	          Braulik, Norbert Lohfink, Osternacht und Altes Testament</a>, Peter
	          Lang, Frankfurt/M </span></font><br>
		    </p>		    <p><font face="Arial, Helvetica, sans-serif"><b><font size="2"><br>
              </font></b><font size="2">&copy; kath.de<b><br>
              </b></font></font><font size="2">F<font face="Arial, Helvetica, sans-serif">eedback
              bitte an redaktion@kath.de</font></font></p>
          </td>
          <td class="L12" width="30%">            <div align="right"> 
              <p align="right">
			  
			  
			 <script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
google_ad_width = 336;
google_ad_height = 280;
google_ad_format = "336x280_as";
google_ad_type = "text_image";
//2006-10-23: Lexikon Symbole
google_ad_channel = "8495253357";
//--></script>
<script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
			  
			  
			  <br>
                <br>
                <font size="2" face="Arial, Helvetica, sans-serif"><br>
                <br>
                </font></p>
              <p>&nbsp;</p>
              <p><br>
              </p>
            </div>
          </td>
          <td background="../lexikon/liturgie/boxright.gif" width="8"><img src="../lexikon/liturgie/boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8"><img src="../lexikon/liturgie/boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="../lexikon/liturgie/boxbottom.gif" colspan="2"><img src="../lexikon/liturgie/boxbottom.gif" width="8" height="8" alt=""></td>
          <td width="8"><img src="../lexikon/liturgie/boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td width="100" align="left" valign="top">&nbsp;</td>
  </tr>
</table>
</body>
</html>
