<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title>Osternacht und Altes Testament - Die Osternacht als Zentrum des Osterfestkreises</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="osternacht.css" rel="stylesheet" type="text/css">
</head>
<body bgcolor="#ffffff" leftmargin="6" topmargin="6" marginwidth="6" marginheight="6">
<table width="100%" border="0" cellspacing="0" cellpadding="6">
  <tr>
    <td width="100" align="left" valign="top" height="3"> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="../lexikon/liturgie/boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="../lexikon/liturgie/boxtop.gif"><img src="../lexikon/liturgie/boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="../lexikon/liturgie/boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="../lexikon/liturgie/boxtopleft.gif"><img src="../lexikon/liturgie/boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b><font face="Arial, Helvetica, sans-serif">Lexikontitel</font></b></td>
          <td background="../lexikon/liturgie/boxtopright.gif"><img src="../lexikon/liturgie/boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="../lexikon/liturgie/boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="../lexikon/liturgie/boxdivider.gif"><img src="../lexikon/liturgie/boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="../lexikon/liturgie/boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="../lexikon/liturgie/boxleft.gif"><img src="../lexikon/liturgie/boxleft.gif" width="8" height="8" alt=""></td>
          <td> <?php include("logo.html"); ?> </td>
          <td background="../lexikon/liturgie/boxright.gif"><img src="../lexikon/liturgie/boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="../lexikon/liturgie/boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="../lexikon/liturgie/boxbottom.gif"><img src="../lexikon/liturgie/boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="../lexikon/liturgie/boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="../lexikon/liturgie/boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="../lexikon/liturgie/boxtop.gif"><img src="../lexikon/liturgie/boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="../lexikon/liturgie/boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="../lexikon/liturgie/boxtopleft.gif"><img src="../lexikon/liturgie/boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><font face="Arial, Helvetica, sans-serif"><strong>Inhaltsverzeichnis 
            </strong></font></td>
          <td background="../lexikon/liturgie/boxtopright.gif"><img src="../lexikon/liturgie/boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="../lexikon/liturgie/boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="../lexikon/liturgie/boxdivider.gif"><img src="../lexikon/liturgie/boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="../lexikon/liturgie/boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="../lexikon/liturgie/boxleft.gif"><img src="../lexikon/liturgie/boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10"> <?php include("az.html"); ?> </td>
          <td background="../lexikon/liturgie/boxright.gif"><img src="../lexikon/liturgie/boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="../lexikon/liturgie/boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="../lexikon/liturgie/boxbottom.gif"><img src="../lexikon/liturgie/boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="../lexikon/liturgie/boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
    <td rowspan="2" valign="top">
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="../lexikon/liturgie/boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td background="../lexikon/liturgie/boxtop.gif" colspan="2"><img src="../lexikon/liturgie/boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="../lexikon/liturgie/boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="../lexikon/liturgie/boxtopleft.gif" width="8"><img src="../lexikon/liturgie/boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2" colspan="2"> 
            <h1>Die Osternacht als Zentrum des Osterfestkreises</h1>
          </td>
          <td background="../lexikon/liturgie/boxtopright.gif" width="8"><img src="../lexikon/liturgie/boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8"><img src="../lexikon/liturgie/boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="../lexikon/liturgie/boxdivider.gif" colspan="2"><img src="../lexikon/liturgie/boxdivider.gif" alt="" width="8" height="13"></td>
          <td width="8"><img src="../lexikon/liturgie/boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="../lexikon/liturgie/boxleft.gif" width="8"><img src="../lexikon/liturgie/boxleft.gif" width="8" height="8" alt=""></td>
          <td class="L12" width="70%">		  <p><strong><font face="Arial, Helvetica, sans-serif">Fastenzeit und die Zeit nach Ostern
              liegen wie konzentrische Kreise um die Osternacht herum</font></strong></p>
		    <p class="text">Die Osternacht ist eingebettet in eine weitgespannte
		        Festzeit. Diese ganze Festzeit mu&szlig;, auch wenn man sich nur mit der zentralen heiligen
		      Nacht besch&auml;ftigt, stets auch als ganze vor Augen stehen. So ist
		      es gut, auch einen Blick auf den ganzen Osterfestkreis zu werfen, wie
		      er in unserer Kirche gefeiert wird. Wir d&uuml;rfen in diesem Zusammenhang
		      nicht in linear aufeinander folgenden Einzelfesten denken, die von der
		      christlichen Gemeinde in fester Reihenfolge nacheinander begangen werden
		      m&uuml;&szlig;ten. Vielmehr gibt es so etwas wie ein System konzentrischer
		      Kreise. </p>
		    <p class="text">In ihm ist die Osternacht das Zentrum. In ihr ist
		        alles zusammen. Alles wird hier gefeiert, nichts steht mehr als
		      das andere im Vordergrund.
		      Diese Nacht ist das Fest der Sch&ouml;pfung, der ersten und der neuen.
		      Sie ist das Fest des Auszugs aus &Auml;gypten und der Entstehung des
		      Gottesvolkes. Sie ist das Fest des Todes und der Auferstehung Jesu. Sie
		      ist die Stunde, da neue Menschen aus einer alten Welt durch die Taufe
		      in die neue Welt hin&uuml;bertreten. Diese Nacht ist unmittelbar hingeordnet
		      auf die Wiederkunft des Herrn am Ende der Zeiten. Alles zusammen. Und
		      deshalb ist diese Nacht so reich und mu&szlig; in ihrer Feier so weit
		      ausgreifen und so vieles zugleich zum Klingen bringen. </p>
		    <p class="text">Vielleicht ist es im vorigen Absatz beim Lesen aufgefallen,
		        da&szlig; gesagt
		      wurde, die Osternacht sei das Fest des Todes und der Auferstehung Jesu.
		      Wieso auch des Todes? Ist das nicht der Karfreitag? Der Karfreitag ist
		      in der Tat der &#8222;&ouml;sterliche Tag vom Leiden und Sterben des
		      Herrn&#8220;, ebenso wie der Karsamstag der &#8222;&ouml;sterliche Tag
		      von der Grabesruhe des Herrn&#8220; und der Ostersonntag der &#8222;&ouml;sterliche
		      Tag von der Auferstehung des Herrn&#8220; ist. Das sind die Namen, die
		      diese Tage seit der letzten Liturgiereform im Me&szlig;buch haben. Aber
		      mit ihnen sind wir schon beim ersten konzentrischen Kreis, der um die
		      Osternacht herumliegt und der die Sinngebung der einzelnen Tage von den
		      Ereignissen am Ende des Lebens Jesu her kalendarisch festh&auml;lt. Dieser
		      Kreis schlie&szlig;t jedoch nicht aus, da&szlig; in der Osternacht, die
		      den Mittelpunkt von allem bildet, alles auf einmal zum Festgeheimnis
		      geh&ouml;rt. </p>
		    <p><span class="text">Das ist allerdings dadurch verdunkelt, da&szlig; im Gegensatz zum Brauch
		      der alten Kirche in der Osternacht als Evangelium nur noch ein Auferstehungstext
		      gelesen wird, nicht mehr auch die Passion oder zumindest der letzte Teil
		      der Passion. Das war urspr&uuml;nglich &uuml;blich und geh&ouml;rt eigentlich
		      unbedingt hinein. Im Zentrum des Festes aller Feste d&uuml;rfen Leiden
		      und Tod Jesu auf keinen Fall fehlen. Es ist zu w&uuml;nschen, da&szlig; das
		      Evangelium der Osterfr&uuml;he bei einer kommenden Reform wieder in die
	      alte Gestalt gebracht wird.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       </span>            </p>
		    <p><font face="Arial, Helvetica, sans-serif">Text: Georg Braulik, Norbert
	        Lohfink</font></p>
		    <p><font face="Arial, Helvetica, sans-serif"><br>
              <span class="text">Weitere Informationen:<br>
              <a href="http://www.peterlang.com/index.cfm?vID=51819&vLang=D&vHR=1&vUR=3&vUUR=4" target="_blank">Georg
            Braulik, Norbert Lohfink, Osternacht und Altes Testament                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       </a>,
            Peter Lang, Frankfurt/M </span></font><br>
	        </p>
		    <p><font face="Arial, Helvetica, sans-serif"><b><font size="2"><br>
              </font></b><font size="2">&copy; kath.de<b><br>
              </b></font></font><font size="2">F<font face="Arial, Helvetica, sans-serif">eedback
              bitte an redaktion@kath.de</font></font></p>
          </td>
          <td class="L12" width="30%">            <div align="right"> 
              <p align="right">
			  
			  
			 <script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
google_ad_width = 336;
google_ad_height = 280;
google_ad_format = "336x280_as";
google_ad_type = "text_image";
//2006-10-23: Lexikon Symbole
google_ad_channel = "8495253357";
//--></script>
<script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
			  
			  
			  <br>
                <br>
                <font size="2" face="Arial, Helvetica, sans-serif"><br>
                <br>
                </font></p>
              <p>&nbsp;</p>
              <p><br>
              </p>
            </div>
          </td>
          <td background="../lexikon/liturgie/boxright.gif" width="8"><img src="../lexikon/liturgie/boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8"><img src="../lexikon/liturgie/boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="../lexikon/liturgie/boxbottom.gif" colspan="2"><img src="../lexikon/liturgie/boxbottom.gif" width="8" height="8" alt=""></td>
          <td width="8"><img src="../lexikon/liturgie/boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td width="100" align="left" valign="top">&nbsp;</td>
  </tr>
</table>
</body>
</html>
