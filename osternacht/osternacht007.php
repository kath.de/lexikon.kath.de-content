<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title>Osternacht und Altes Testament - Abraham - der Glaube</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="osternacht.css" rel="stylesheet" type="text/css">
</head>
<body bgcolor="#ffffff" leftmargin="6" topmargin="6" marginwidth="6" marginheight="6">
<table width="100%" border="0" cellspacing="0" cellpadding="6">
  <tr>
    <td width="100" align="left" valign="top" height="3"> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="../lexikon/liturgie/boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="../lexikon/liturgie/boxtop.gif"><img src="../lexikon/liturgie/boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="../lexikon/liturgie/boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="../lexikon/liturgie/boxtopleft.gif"><img src="../lexikon/liturgie/boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b><font face="Arial, Helvetica, sans-serif">Lexikontitel</font></b></td>
          <td background="../lexikon/liturgie/boxtopright.gif"><img src="../lexikon/liturgie/boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="../lexikon/liturgie/boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="../lexikon/liturgie/boxdivider.gif"><img src="../lexikon/liturgie/boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="../lexikon/liturgie/boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="../lexikon/liturgie/boxleft.gif"><img src="../lexikon/liturgie/boxleft.gif" width="8" height="8" alt=""></td>
          <td> <?php include("logo.html"); ?> </td>
          <td background="../lexikon/liturgie/boxright.gif"><img src="../lexikon/liturgie/boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="../lexikon/liturgie/boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="../lexikon/liturgie/boxbottom.gif"><img src="../lexikon/liturgie/boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="../lexikon/liturgie/boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="../lexikon/liturgie/boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="../lexikon/liturgie/boxtop.gif"><img src="../lexikon/liturgie/boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="../lexikon/liturgie/boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="../lexikon/liturgie/boxtopleft.gif"><img src="../lexikon/liturgie/boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><font face="Arial, Helvetica, sans-serif"><strong>Inhaltsverzeichnis 
            </strong></font></td>
          <td background="../lexikon/liturgie/boxtopright.gif"><img src="../lexikon/liturgie/boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="../lexikon/liturgie/boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="../lexikon/liturgie/boxdivider.gif"><img src="../lexikon/liturgie/boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="../lexikon/liturgie/boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="../lexikon/liturgie/boxleft.gif"><img src="../lexikon/liturgie/boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10"> <?php include("az.html"); ?> </td>
          <td background="../lexikon/liturgie/boxright.gif"><img src="../lexikon/liturgie/boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="../lexikon/liturgie/boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="../lexikon/liturgie/boxbottom.gif"><img src="../lexikon/liturgie/boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="../lexikon/liturgie/boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
    <td rowspan="2" valign="top">
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="../lexikon/liturgie/boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td background="../lexikon/liturgie/boxtop.gif" colspan="2"><img src="../lexikon/liturgie/boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="../lexikon/liturgie/boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="../lexikon/liturgie/boxtopleft.gif" width="8"><img src="../lexikon/liturgie/boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2" colspan="2"> 
            <h1>Abraham - der Glaube</h1>
          </td>
          <td background="../lexikon/liturgie/boxtopright.gif" width="8"><img src="../lexikon/liturgie/boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8"><img src="../lexikon/liturgie/boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="../lexikon/liturgie/boxdivider.gif" colspan="2"><img src="../lexikon/liturgie/boxdivider.gif" alt="" width="8" height="13"></td>
          <td width="8"><img src="../lexikon/liturgie/boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="../lexikon/liturgie/boxleft.gif" width="8"><img src="../lexikon/liturgie/boxleft.gif" width="8" height="8" alt=""></td>
          <td class="L12" width="70%">		  <p><strong><font face="Arial, Helvetica, sans-serif">Den
                  Glauben Abrahams meditieren:</font></strong></p>
		    <p class="text">Zu Genesis 22,1-18</p>
		    <p><span class="text">Im Vaterunser beten wir: &#8222;F&uuml;hre uns nicht in Versuchung.&#8220; In
		      welchem Ausma&szlig; unser Gott uns auf die Probe stellen kann, zeigt
		      die Geschichte von Abraham, dem er befiehlt, seinen eigenen Sohn als
		      Opfer darzubringen. Und Isaak ist nicht nur Abrahams geliebtes Kind.
		      Er ist der, an den Gott seine Verhei&szlig;ung f&uuml;r die ganze Menschheit
		      gekn&uuml;pft hat. Im wortlosen Vertrauen Abrahams auf den unbegreiflichen
		      Gott erfahren wir, wie ernst die Sache des Glaubens ist. Sie geht an
		      die tiefsten Wurzeln unserer eigenen Sicherheit. Im Ausgang dieser Geschichte
		      erfahren wir, wie sehr Gott das Leben, das Heil und die Zukunft will.
		      So liegt in dieser Osternacht in der Lesung am Ende aller Ton auf der
		      Gr&ouml;&szlig;e der Verhei&szlig;ung. Bei Christus, der wirklich durch
		      den Tod hindurchgehen mu&szlig;te, zeigt sich uns dann in seiner Auferstehung
		      das Ende aller Wege Gottes. </span> </p>
		    <p class="text">Texte: Georg Braulik, Norbert Lohfink</p>		    
		    <p class="text"><font face="Arial, Helvetica, sans-serif">	          <span class="text">Weitere Informationen:<br>
	          <a href="http://www.peterlang.com/index.cfm?vID=51819&vLang=D&vHR=1&vUR=3&vUUR=4" target="_blank">Georg
	          Braulik, Norbert Lohfink, Osternacht und Altes Testament</a>, Peter
	          Lang, Frankfurt/M </span></font><br>
		    </p>		    <p><font face="Arial, Helvetica, sans-serif"><b><font size="2"><br>
              </font></b><font size="2">&copy; kath.de<b><br>
              </b></font></font><font size="2">F<font face="Arial, Helvetica, sans-serif">eedback
              bitte an redaktion@kath.de</font></font></p>
          </td>
          <td class="L12" width="30%">            <div align="right"> 
              <p align="right">
			  
			  
			 <script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
google_ad_width = 336;
google_ad_height = 280;
google_ad_format = "336x280_as";
google_ad_type = "text_image";
//2006-10-23: Lexikon Symbole
google_ad_channel = "8495253357";
//--></script>
<script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
			  
			  
			  <br>
                <br>
                <font size="2" face="Arial, Helvetica, sans-serif"><br>
                <br>
                </font></p>
              <p>&nbsp;</p>
              <p><br>
              </p>
            </div>
          </td>
          <td background="../lexikon/liturgie/boxright.gif" width="8"><img src="../lexikon/liturgie/boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8"><img src="../lexikon/liturgie/boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="../lexikon/liturgie/boxbottom.gif" colspan="2"><img src="../lexikon/liturgie/boxbottom.gif" width="8" height="8" alt=""></td>
          <td width="8"><img src="../lexikon/liturgie/boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td width="100" align="left" valign="top">&nbsp;</td>
  </tr>
</table>
</body>
</html>
