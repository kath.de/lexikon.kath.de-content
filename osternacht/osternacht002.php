<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title>Osternacht und Altes Testament - Ostern - auch ein Neujahrsfest</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="osternacht.css" rel="stylesheet" type="text/css">
</head>
<body bgcolor="#ffffff" leftmargin="6" topmargin="6" marginwidth="6" marginheight="6">
<table width="100%" border="0" cellspacing="0" cellpadding="6">
  <tr>
    <td width="100" align="left" valign="top" height="3"> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="../lexikon/liturgie/boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="../lexikon/liturgie/boxtop.gif"><img src="../lexikon/liturgie/boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="../lexikon/liturgie/boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="../lexikon/liturgie/boxtopleft.gif"><img src="../lexikon/liturgie/boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b><font face="Arial, Helvetica, sans-serif">Lexikontitel</font></b></td>
          <td background="../lexikon/liturgie/boxtopright.gif"><img src="../lexikon/liturgie/boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="../lexikon/liturgie/boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="../lexikon/liturgie/boxdivider.gif"><img src="../lexikon/liturgie/boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="../lexikon/liturgie/boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="../lexikon/liturgie/boxleft.gif"><img src="../lexikon/liturgie/boxleft.gif" width="8" height="8" alt=""></td>
          <td> <?php include("logo.html"); ?> </td>
          <td background="../lexikon/liturgie/boxright.gif"><img src="../lexikon/liturgie/boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="../lexikon/liturgie/boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="../lexikon/liturgie/boxbottom.gif"><img src="../lexikon/liturgie/boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="../lexikon/liturgie/boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="../lexikon/liturgie/boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="../lexikon/liturgie/boxtop.gif"><img src="../lexikon/liturgie/boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="../lexikon/liturgie/boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="../lexikon/liturgie/boxtopleft.gif"><img src="../lexikon/liturgie/boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><font face="Arial, Helvetica, sans-serif"><strong>Inhaltsverzeichnis 
            </strong></font></td>
          <td background="../lexikon/liturgie/boxtopright.gif"><img src="../lexikon/liturgie/boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="../lexikon/liturgie/boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="../lexikon/liturgie/boxdivider.gif"><img src="../lexikon/liturgie/boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="../lexikon/liturgie/boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="../lexikon/liturgie/boxleft.gif"><img src="../lexikon/liturgie/boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10"> <?php include("az.html"); ?> </td>
          <td background="../lexikon/liturgie/boxright.gif"><img src="../lexikon/liturgie/boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="../lexikon/liturgie/boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="../lexikon/liturgie/boxbottom.gif"><img src="../lexikon/liturgie/boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="../lexikon/liturgie/boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
    <td rowspan="2" valign="top">
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="../lexikon/liturgie/boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td background="../lexikon/liturgie/boxtop.gif" colspan="2"><img src="../lexikon/liturgie/boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="../lexikon/liturgie/boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="../lexikon/liturgie/boxtopleft.gif" width="8"><img src="../lexikon/liturgie/boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2" colspan="2"> 
            <h1>Ostern
              - auch ein Neujahrsfest</h1>
          </td>
          <td background="../lexikon/liturgie/boxtopright.gif" width="8"><img src="../lexikon/liturgie/boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8"><img src="../lexikon/liturgie/boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="../lexikon/liturgie/boxdivider.gif" colspan="2"><img src="../lexikon/liturgie/boxdivider.gif" alt="" width="8" height="13"></td>
          <td width="8"><img src="../lexikon/liturgie/boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="../lexikon/liturgie/boxleft.gif" width="8"><img src="../lexikon/liturgie/boxleft.gif" width="8" height="8" alt=""></td>
          <td class="L12" width="70%">		  <p>&nbsp;		      </p>
		    <p class="text">Wir brauchen das Fest, damit wir die Zeit, sie unterbrechend,
		        nachher wieder bestehen k&ouml;nnen. Wir brauchen vor allem das Fest der Feste,
		      das hohe Fest der Ostern. Es ist das Fest aller Feste. Nicht nur, weil
		      es ein Fest ist, versetzt es uns ins Offene der Sch&ouml;pfung hinein.
		      Es ist, obwohl wir das nur noch gerade am Rande unsres Bewu&szlig;tseins
		      ahnen, auch unser eigentliches Sch&ouml;pfungsfest. Weil uns das so wenig
		      bewu&szlig;t ist, zun&auml;chst etwas dazu. ......</p>
		    <p class="text"><span class="&uuml;berschrift">Die Neujahrsfeste der Religionen</span><br>
  Abgesehen davon, da&szlig; jedes Fest, wie gesagt, ein Hineinsinken in
		      den Sch&ouml;pfungsmoment ist, geh&ouml;rt bei fast allen V&ouml;lkern
		      und Religionen die Sch&ouml;pfungsthematik vor allem zum Neujahrsfest.
		      In der Neujahrsnacht vergeht das Alte, und der gro&szlig;e Kreislauf
		      der Welt beginnt von neuem. Wie alle Religionsgeschichtler wissen, ist
		      es fast gleichg&uuml;ltig, zu welchem Datum das Neujahrsfest gefeiert
		      wird. Es gibt besonders geeignete Stellen im nat&uuml;rlichen Jahreslauf.
		      Aber an welcher von ihnen dann das Neujahrsfest seinen Platz bekommt,
		      das sind terminliche Konventionen. Jede Kultur hat die ihren. Es kommt
		      auch oft vor, da&szlig; im gleichen Kulturraum nebeneinander verschiedene
		      Neujahrsfeste gefeiert werden. Auch die gleichen Menschen feiern oft
		      mehrmals im Jahr Neujahr, wenn sie gleichzeitig verschiedenen Gruppen
		      zugeh&ouml;ren. Auf das Datum kommt es nicht an. Nur mu&szlig; einmal
		      im Jahr &#8211; f&uuml;r die Festerfahrung ist es dann stets ein einziges
		      Mal &#8211; der Sprung getan, das Alte beendet, die Schwelle ins Neue &uuml;berschritten
		      werden. Es mu&szlig; diesen Augenblick h&ouml;chster Gef&auml;hrdung,
		      aber auch zitterndster Hoffnung geben: Durchgang, &Uuml;bergang, neuer
		      Anfang. </p>
		    <p class="text">Bei uns im christlichen Kulturraum erscheint nur
		        eines als seltsam: In vielen anderen Religionen ist das Neujahrsfest
		        das Hauptfest, das
		      eigentliche Fest des Jahres. Wir begehen die Sylvesternacht. Sie
		        ist sicher anders als die anderen N&auml;chte &#8211; doch ein christliches
		      Fest ist sie nicht. Unser Neujahr ist nur ein b&uuml;rgerliches Datum.
		      Vom 1. Januar an tragen wir unsere Notizen in einen neuen Taschenkalender
		      ein und schreiben im Datum eine neue Jahreszahl. Bis zur Einf&uuml;hrung
		      der elektronischen Datenverarbeitung machten die Firmen &#8222;zwischen
		      den Jahren&#8220; ihre Inventur. Und das war es. Oder doch nicht ganz.
		      Sagen wir deshalb besser: Unser Neujahr ist ein heidnisches Fest. Die
		      Nacht wird durchwacht, so wie es zu jedem gro&szlig;en Fest geh&ouml;rt.
		      Der Sekt verwirrt das Zeitgef&uuml;hl. Die Nacht wird zum Tag durch Feuerwerk
		      und B&ouml;ller. Der numinose Schauer ber&uuml;hrt uns. Wir sp&uuml;ren
		      an Sylvester, wenn wir das Fenster &ouml;ffnen, wie die Geheimnisse von
		      Zeit und Ewigkeit k&uuml;hl an uns vor&uuml;berstreichen. Doch ins christliche
		      Mysterium werden wir durch diese Mitternacht nicht getaucht. Was ist
		      da los? Will der christliche Glaube kein Neujahrsfest, kein Fest der
		      Sch&ouml;pfung? </p>
		    <p class="text">Ist vielleicht das Weihnachtsfest das christliche
		        Neujahr? Es will in den Tagen der winterlichen Sonnenwende ja
		      offenbar den aus r&ouml;mischer
		      Zeit z&auml;h weiterlebenden Neujahrstag verdr&auml;ngen. Aber will es
		      Sylvester verdr&auml;ngen, indem es ein alternatives Neujahrsfest ist?
		      Wir glauben, nein. Es hat ganz neue Inhalte. </p>
		    <p class="text"><span class="&uuml;berschrift">Die christliche Neujahrstradition</span><br>
  Es ist auch in christlichen Zeiten und L&auml;ndern keineswegs immer
		      so gewesen, da&szlig; das neue Jahr vom 1. Januar an gerechnet wurde.
		      Im Mittelalter berechnete man das neue Jahr weithin vom 25. M&auml;rz
		      an. Das war nicht nur deshalb so, weil dies der Tag der Empf&auml;ngnis
		      des Messias ist. Es hing auch damit zusammen, da&szlig; um diese Zeit
		      die Natur wieder hervorkommt. Und die Wurzeln dieses Termins sind
		      uralt. </p>
		    <p class="text">Das j&uuml;dische Neujahrsfest liegt heute im Herbst, nach der letzten
		      Ernte von Obst und Wein, vor den ersten Winterregen, die die Samen in
		      den Feldern keimen lassen. Warum haben die Christen den um das Laubh&uuml;ttenfest
		      gewundenen j&uuml;dischen Neujahrsfestkranz nicht weitergef&uuml;hrt?
		      Warum konnten sp&auml;ter die geheimen, ihrer selbst kaum bewu&szlig;ten
		      Versuche, hier im Herbst das Neujahr wieder einzuf&uuml;hren, etwa der
		      Thanksgiving Day der amerikanischen Pilgrim Fathers oder unser
		      Erntedankfest, sich doch nicht zu echten Neujahrsfesten entwickeln? <br>
		      Die Antwort ist tief, und vielleicht &uuml;berraschend: Weil wir durchaus
		      ein christliches Neujahrsfest, ein Fest der Sch&ouml;pfung, haben. Und
		      irgendwo wissen wir das auch noch. Es ist das Osterfest. </p>
		    <p class="text"><span class="&uuml;berschrift">Ostern als Neubeginn und
		        Fest der Neuen Sch&ouml;pfung</span><br>
  Das Christentum war ein Neuheitserlebnis. Es hat zun&auml;chst einmal
		      alle Feste, die es gab, in seinem einzigen, neuen Fest aufgesaugt, dem
		      Fest des Sterbens und der Auferweckung des Messias. Um es an den j&uuml;dischen
		      Festen zu zeigen: Das alte Wochenfest wurde als Pfingstfest zum gro&szlig;en
		      Schlu&szlig;punkt der 50 Tage dauernden &ouml;sterlichen Festzeit. Das
		      alte Herbstfest verschwand spurlos. Sein Charakter als Neujahrsfest ging
		      an das eine christliche Fest &uuml;ber, das Osterfest. </p>
		    <p class="text">Die Fr&uuml;hlingszeit, in der das Osterfest gefeiert wird, ist durchaus
		      ein m&ouml;glicher Neujahrstermin. Wenn die Natur mit Gewalt hervorbricht,
		      wenn die Tiere der Herden ihr Jungen werfen und das Kleinvieh seine Eier
		      legt, dann ist das eine Zeit, in der das Geheimnis der Sch&ouml;pfung
		      die Menschen anr&uuml;hrt und wir begehen k&ouml;nnen, da&szlig; das
		      Alte vor&uuml;ber ist und Neues hervortritt &#8211; in der wir also Neujahr
		      feiern k&ouml;nnen. Nicht umsonst kommt der Osterhase und l&auml;&szlig;t
		      die Kinder seine Eier finden. </p>
		    <p class="text">Ja noch mehr. Das alte Israel hatte zwei Kalender
		        nebeneinander (die Mischna kennt sogar vier Jahresanf&auml;nge &#8211; aber das ist sekund&auml;re
		      Systematisierung). Es gab das Herbstjahr, an dem das Neujahrsfest schlie&szlig;lich
		      h&auml;ngenblieb. Aber es gab auch das Fr&uuml;hlingsjahr, und alle biblischen
		      Festverzeichnisse beginnen mit dem Fr&uuml;hlingsfest. Der Anfang des
		      Fr&uuml;hlingsjahrs war offenbar das geheime Neujahrsfest der Priester
		      und Eingeweihten. Denn nach der Bibel liegt das Osterfest im &#8222;ersten
		      Monat&#8220;. Nach Exodus 12,2 sagte der Herr zu Mose und Aaron in &Auml;gypten,
		      bevor die letzte der gro&szlig;en Plagen &uuml;ber das Land kam: &#8222;Dieser
		      Monat soll die Reihe eurer Monate er&ouml;ffnen. Er soll euch als der
		      erste unter den Monaten des Jahres gelten.&#8220; Und dann ordnete er
		      an, da&szlig; am 14. Tag dieses Monats das Osterlamm geschlachtet werden
		      sollte. Die Nacht des Lammessens war dann auch die Nacht des Auszugs
		      aus &Auml;gypten. </p>
		    <p class="text">Nicht genau am ersten Tag des ersten Monats lag also
		        das Osterfest, sondern erst zwei Wochen sp&auml;ter. Dennoch war es mit dem Jahresanfang
		      verbunden, es war das Neujahrsfest. Die wohl gewollte Verschiebung zeigte
		      jedoch, da&szlig; dieses neue Fest nicht nur das Fest der Sch&ouml;pfung
		      war, sondern zugleich das einer historischen, in der menschlichen Geschichte
		      geschehenen Neusch&ouml;pfung, das Fest der Befreiung des versklavten
		      Gottesvolkes, des Beginns einer neuen Gesellschaft. </p>
		    <p class="text">Da&szlig; der Auszug aus &Auml;gypten aber wirklich als Sch&ouml;pfungsgeschehen
		      zu sehen war, zeigt die Erz&auml;hlung vom Durchzug durch das Rote Meer.
		      Hier teilt Gott die t&ouml;dlichen Wasser und l&auml;&szlig;t das jetzt
		      in die Freiheit gelangende Volk hindurchziehen aufs trockene Land, w&auml;hrend
		      die M&auml;chte der t&ouml;dlichen Gewalt verschlungen werden vom nassen
		      Chaos. Das nimmt ein uraltes Sch&ouml;pfungsbild auf. Der Sch&ouml;pfergott
		      k&auml;mpft mit dem Meer, der Chaosschlange, zerteilt sie und f&uuml;hrt
		      das feste Land herauf. Die Auszugserz&auml;hlungen machen den Auszug
		      aus &Auml;gypten zu einem neuen, nun in der Geschichte selbst geschehenden
		      Sch&ouml;pfungsakt. Das Osterfest ist seit den Anf&auml;ngen Israels
		      das neue Fest der Sch&ouml;pfung und zugleich das Fest von Israels Neusch&ouml;pfung. </p>
		    <p class="text">Hier zeichnet sich dann bruchlos der christliche
		        Festinhalt der Ostern ein. Denn die Auferweckung des get&ouml;teten Messias ist neue Sch&ouml;pfung.
		      Das Neue Testament ist voll von dieser Aussage. Alle, die an den erh&ouml;hten
		      Christus glauben, werden hineingenommen in die neue Welt der Endzeit.
		      Und sie beginnt schon in dieser Welt, in Christi mystischem Leib, der
		      Kirche. Man tritt in diese neue Sch&ouml;pfung ein durch die Taufe. Da
		      stirbt man mit Christus und steht in ihm auf, in eine neue Wirklichkeit
		      hinein. Deshalb ist die Osternacht nicht nur die Nacht, wo Christus aus
		      den Toten erstand, sondern zugleich der eigentliche Tauftermin, die Nacht,
		      wo aus der Kraft seiner Auferstehung immer von neuem Volk Gottes entsteht.
		      In ihm kommt die Sch&ouml;pfung erst in ihr wahres Sein. </p>
		    <p><span class="text">Weil sich so im Osterfest alles, was Fest sein kann,
		        verbindet und umschlingt, ist Ostern das Fest der Feste. Und die Osternacht
		        ist die gro&szlig;e
		      Nacht aller N&auml;chte. Sie steht am Anfang des Jahres, und zugleich
		      ist sie der Zenit des Jahres. Es kann kein gr&ouml;&szlig;eres Fest f&uuml;r
		      uns geben als diese heilige Nacht.</span><br>
	          </p>
		    <p><span class="text">Texte: Georg Braulik, Norbert Lohfink	</span> </p>
		    <p><font face="Arial, Helvetica, sans-serif"><br>
	          <span class="text">Weitere Informationen:<br>
	          <a href="http://www.peterlang.com/index.cfm?vID=51819&vLang=D&vHR=1&vUR=3&vUUR=4" target="_blank">Georg
	          Braulik, Norbert Lohfink, Osternacht und Altes Testament</a>, Peter
	          Lang, Frankfurt/M </span></font><br>
		    </p>
		  <p><font face="Arial, Helvetica, sans-serif"><b><font size="2"><br>
              </font></b><font size="2">&copy; kath.de<b><br>
              </b></font></font><font size="2">F<font face="Arial, Helvetica, sans-serif">eedback
              bitte an redaktion@kath.de</font></font></p>
          </td>
          <td class="L12" width="30%">            <div align="right"> 
              <p align="right">
			  
			  
			 <script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
google_ad_width = 336;
google_ad_height = 280;
google_ad_format = "336x280_as";
google_ad_type = "text_image";
//2006-10-23: Lexikon Symbole
google_ad_channel = "8495253357";
//--></script>
<script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
			  
			  
			  <br>
                <br>
                <font size="2" face="Arial, Helvetica, sans-serif"><br>
                <br>
                </font></p>
              <p>&nbsp;</p>
              <p><br>
              </p>
            </div>
          </td>
          <td background="../lexikon/liturgie/boxright.gif" width="8"><img src="../lexikon/liturgie/boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8"><img src="../lexikon/liturgie/boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="../lexikon/liturgie/boxbottom.gif" colspan="2"><img src="../lexikon/liturgie/boxbottom.gif" width="8" height="8" alt=""></td>
          <td width="8"><img src="../lexikon/liturgie/boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td width="100" align="left" valign="top">&nbsp;</td>
  </tr>
</table>
</body>
</html>
