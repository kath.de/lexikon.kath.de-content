<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title>Osternacht und Altes Testament - Die Fastenzeit &ouml;sterlich begehen</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="osternacht.css" rel="stylesheet" type="text/css">
</head>
<body bgcolor="#ffffff" leftmargin="6" topmargin="6" marginwidth="6" marginheight="6">
<table width="100%" border="0" cellspacing="0" cellpadding="6">
  <tr>
    <td width="100" align="left" valign="top" height="3"> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="../lexikon/liturgie/boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="../lexikon/liturgie/boxtop.gif"><img src="../lexikon/liturgie/boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="../lexikon/liturgie/boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="../lexikon/liturgie/boxtopleft.gif"><img src="../lexikon/liturgie/boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b><font face="Arial, Helvetica, sans-serif">Lexikontitel</font></b></td>
          <td background="../lexikon/liturgie/boxtopright.gif"><img src="../lexikon/liturgie/boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="../lexikon/liturgie/boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="../lexikon/liturgie/boxdivider.gif"><img src="../lexikon/liturgie/boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="../lexikon/liturgie/boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="../lexikon/liturgie/boxleft.gif"><img src="../lexikon/liturgie/boxleft.gif" width="8" height="8" alt=""></td>
          <td> <?php include("logo.html"); ?> </td>
          <td background="../lexikon/liturgie/boxright.gif"><img src="../lexikon/liturgie/boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="../lexikon/liturgie/boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="../lexikon/liturgie/boxbottom.gif"><img src="../lexikon/liturgie/boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="../lexikon/liturgie/boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="../lexikon/liturgie/boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="../lexikon/liturgie/boxtop.gif"><img src="../lexikon/liturgie/boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="../lexikon/liturgie/boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="../lexikon/liturgie/boxtopleft.gif"><img src="../lexikon/liturgie/boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><font face="Arial, Helvetica, sans-serif"><strong>Inhaltsverzeichnis 
            </strong></font></td>
          <td background="../lexikon/liturgie/boxtopright.gif"><img src="../lexikon/liturgie/boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="../lexikon/liturgie/boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="../lexikon/liturgie/boxdivider.gif"><img src="../lexikon/liturgie/boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="../lexikon/liturgie/boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="../lexikon/liturgie/boxleft.gif"><img src="../lexikon/liturgie/boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10"> <?php include("az.html"); ?> </td>
          <td background="../lexikon/liturgie/boxright.gif"><img src="../lexikon/liturgie/boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="../lexikon/liturgie/boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="../lexikon/liturgie/boxbottom.gif"><img src="../lexikon/liturgie/boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="../lexikon/liturgie/boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
    <td rowspan="2" valign="top">
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="../lexikon/liturgie/boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td background="../lexikon/liturgie/boxtop.gif" colspan="2"><img src="../lexikon/liturgie/boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="../lexikon/liturgie/boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="../lexikon/liturgie/boxtopleft.gif" width="8"><img src="../lexikon/liturgie/boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2" colspan="2"> 
            <h1><font face="Arial, Helvetica, sans-serif">Die Fastenzeit &ouml;sterlich
            begehen</font> </h1>
          </td>
          <td background="../lexikon/liturgie/boxtopright.gif" width="8"><img src="../lexikon/liturgie/boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8"><img src="../lexikon/liturgie/boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="../lexikon/liturgie/boxdivider.gif" colspan="2"><img src="../lexikon/liturgie/boxdivider.gif" alt="" width="8" height="13"></td>
          <td width="8"><img src="../lexikon/liturgie/boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="../lexikon/liturgie/boxleft.gif" width="8"><img src="../lexikon/liturgie/boxleft.gif" width="8" height="8" alt=""></td>
          <td class="L12" width="70%">		  <p><br>
		      <span class="&uuml;berschrift"><font face="Arial, Helvetica, sans-serif"><font face="Arial, Helvetica, sans-serif">Fastenzeit
		      und Ostern</font></font></span><font face="Arial, Helvetica, sans-serif"><span class="text"><br>
		      <br>
		      sind eine Einheit. Es werden alle Seiten des Lebens angeschaut.
		      Nicht nur sind die Grundelemente Wasser und Licht im Spiel, es
		      geht um das Leben und sogar die ganze Sch&ouml;pfung. Alles soll neu
		      werden. Beten,  Almosen geben, Fasten sind bereits Zeichen des
		      neuen Lebens. Alles
		      kommt in der Osternacht zusammen. Deshalb ist es sinnvoll, schon
		      in der Fastenzeit auf das zu blicken, um was es auch an Ostern
		      geht. </span></font></p>
            <p><font face="Arial, Helvetica, sans-serif"><span class="text">Die
                  Osternacht steht im <a href="osternacht003.php">Zentrum des
                  ganzen Osterfestkreises. </a>Auf
                  sie l&auml;uft die Fastenzeit zu und sie wird in den Wochen nach
                  Ostern entfaltet.<br>
              <br>
	          Ostern ist nicht nur Tag der Auferstehung, sondern eigentlich
		          das <a href="osternacht006.php">christliche Sch&ouml;pfungsfest</a>.
		          Deshalb ist der Gedanke richtig, sich in der Fastenzeit auf die
	          Bewahrung der Sch&ouml;pfung zu besinnen. <br>
	          <br>
	          An Ostern beginnt das Neue Leben. Da Ostern in den Fr&uuml;hling f&auml;llt,
	          war schon das j&uuml;dsiche Pesachfest ein Neujahrstag. Die Fastenzeit
	          f&uuml;hrt uns zu diesme neun Anfang. Da das Neujahrsfest nach usnerer
	          Monatsrechnugn fr&uuml;her auf dem 1. M&auml;rz lag, z&auml;hlen wir von September
	          bis Dezember noch die Moante nach dem alten Neujahrsfest. Sepember
	          ist lateinsich der siebte, nciht der neunte Monat. Der Dezember
	          ist, wie auch im Dezimalsystem gebr&auml;uchlich, der 10. udn nciht
	          der 12. Monat. Zur Datierung des Neujahrsfestes s. <a href="osternacht002.php">Ostern  - auch
	          Neujahr</a></span></font><a href="osternacht002.php"><font size="3" face="Arial, Helvetica, sans-serif">sfest</font></a></p>
            <p><font face="Arial, Helvetica, sans-serif">Die Fastenzeit hat viele
                Motive des Alten Testaments aufgenommen, so den 40 Jahre dauernden
                Zug durch die W&uuml;ste. Es ist aber nicht
                einfach so, dass man immer im Gelobten Land leben k&ouml;nnte, man
                muss wieder in die W&uuml;ste ziehen, um
              neu ins Gelobte Land zu gelangen bzw. wieder Ostern feiern zu k&ouml;nnen.
                <br>
              s. <a href="osternacht005.php">W&uuml;ste und verhei&szlig;enes Land</a></font></p>
            <p><font face="Arial, Helvetica, sans-serif"><br>
              <span class="text">Weitere Informationen:<br>
              <a href="http://www.peterlang.com/index.cfm?vID=51819&vLang=D&vHR=1&vUR=3&vUUR=4" target="_blank">Georg
            Braulik, Norbert Lohfink, Osternacht und Altes Testament                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       </a>,
            Peter Lang, Frankfurt/M                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       </span></font><br>
	        </p>
		  <p><font face="Arial, Helvetica, sans-serif"><b><font size="2"><br>
              </font></b><font size="2">&copy; kath.de<b><br>
              </b></font></font><font size="2">F<font face="Arial, Helvetica, sans-serif">eedback
              bitte an redaktion@kath.de</font></font></p>
          </td>
          <td class="L12" width="30%">            <div align="right"> 
              <p align="right"><a href="http://www.einfachlebenbrief.de/bestellen.html?elwkz=LEWWW51" target="_blank"><img src="../religioeses_leben/einfachleben_268x70.jpg" border="0"></a>&nbsp;			 </p>
              <p align="right">
  <script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
google_ad_width = 336;
google_ad_height = 280;
google_ad_format = "336x280_as";
google_ad_type = "text_image";
//2006-10-23: Lexikon Symbole
google_ad_channel = "8495253357";
//--></script>
  <script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
			    
			  
			    <br>
                <br>
                <font size="2" face="Arial, Helvetica, sans-serif"><img src="fotos/Sonnenuntergang.JPG"><br>
                <br>
                </font></p>
              <p>&nbsp;</p>
              <p><br>
              </p>
            </div>
          </td>
          <td background="../lexikon/liturgie/boxright.gif" width="8"><img src="../lexikon/liturgie/boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8"><img src="../lexikon/liturgie/boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="../lexikon/liturgie/boxbottom.gif" colspan="2"><img src="../lexikon/liturgie/boxbottom.gif" width="8" height="8" alt=""></td>
          <td width="8"><img src="../lexikon/liturgie/boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td width="100" align="left" valign="top">&nbsp;</td>
  </tr>
</table>
</body>
</html>
