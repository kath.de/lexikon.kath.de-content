<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title>Osternacht und Altes Testament - Abschreiten des Weges Jesu</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="osternacht.css" rel="stylesheet" type="text/css">
</head>
<body bgcolor="#ffffff" leftmargin="6" topmargin="6" marginwidth="6" marginheight="6">
<table width="100%" border="0" cellspacing="0" cellpadding="6">
  <tr>
    <td width="100" align="left" valign="top" height="3"> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="../lexikon/liturgie/boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="../lexikon/liturgie/boxtop.gif"><img src="../lexikon/liturgie/boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="../lexikon/liturgie/boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="../lexikon/liturgie/boxtopleft.gif"><img src="../lexikon/liturgie/boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b><font face="Arial, Helvetica, sans-serif">Lexikontitel</font></b></td>
          <td background="../lexikon/liturgie/boxtopright.gif"><img src="../lexikon/liturgie/boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="../lexikon/liturgie/boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="../lexikon/liturgie/boxdivider.gif"><img src="../lexikon/liturgie/boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="../lexikon/liturgie/boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="../lexikon/liturgie/boxleft.gif"><img src="../lexikon/liturgie/boxleft.gif" width="8" height="8" alt=""></td>
          <td> <?php include("logo.html"); ?> </td>
          <td background="../lexikon/liturgie/boxright.gif"><img src="../lexikon/liturgie/boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="../lexikon/liturgie/boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="../lexikon/liturgie/boxbottom.gif"><img src="../lexikon/liturgie/boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="../lexikon/liturgie/boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="../lexikon/liturgie/boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="../lexikon/liturgie/boxtop.gif"><img src="../lexikon/liturgie/boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="../lexikon/liturgie/boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="../lexikon/liturgie/boxtopleft.gif"><img src="../lexikon/liturgie/boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><font face="Arial, Helvetica, sans-serif"><strong>Inhaltsverzeichnis 
            </strong></font></td>
          <td background="../lexikon/liturgie/boxtopright.gif"><img src="../lexikon/liturgie/boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="../lexikon/liturgie/boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="../lexikon/liturgie/boxdivider.gif"><img src="../lexikon/liturgie/boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="../lexikon/liturgie/boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="../lexikon/liturgie/boxleft.gif"><img src="../lexikon/liturgie/boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10"> <?php include("az.html"); ?> </td>
          <td background="../lexikon/liturgie/boxright.gif"><img src="../lexikon/liturgie/boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="../lexikon/liturgie/boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="../lexikon/liturgie/boxbottom.gif"><img src="../lexikon/liturgie/boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="../lexikon/liturgie/boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
    <td rowspan="2" valign="top">
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="../lexikon/liturgie/boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td background="../lexikon/liturgie/boxtop.gif" colspan="2"><img src="../lexikon/liturgie/boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="../lexikon/liturgie/boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="../lexikon/liturgie/boxtopleft.gif" width="8"><img src="../lexikon/liturgie/boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2" colspan="2"> 
            <h1>Abschreiten des Weges Jesu</h1>
          </td>
          <td background="../lexikon/liturgie/boxtopright.gif" width="8"><img src="../lexikon/liturgie/boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8"><img src="../lexikon/liturgie/boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="../lexikon/liturgie/boxdivider.gif" colspan="2"><img src="../lexikon/liturgie/boxdivider.gif" alt="" width="8" height="13"></td>
          <td width="8"><img src="../lexikon/liturgie/boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="../lexikon/liturgie/boxleft.gif" width="8"><img src="../lexikon/liturgie/boxleft.gif" width="8" height="8" alt=""></td>
          <td class="L12" width="70%">		  <p><font face="Arial, Helvetica, sans-serif"><strong>Am
                  Karfreitag werden schon die Auferstehung und die Erl&ouml;sung gefeiert</strong></font></p>
		    <p class="text">Die Osternacht enth&auml;lt das Ganze, nichts vom
		        Geheimnis des Osterfestes ist ihr fremd. Um sie herum jedoch
		      entfaltet sich das Festgeheimnis in
		      den Tagen vorher und nachher, vor allem auf der Ebene der einzelnen
	        Ereignisse des Lebens Jesu in Jerusalem. </p>
		    <p class="text">Das bahnt sich schon an beim 5. Fastensonntag, fr&uuml;her einmal &#8222;Passionssonntag&#8220; genannt.
		      Der Palmsonntag ist dem Einzug Jesu in Jerusalem zugeordnet. Fast jeder
		      Tag in der Karwoche hat ein Evangelium, das kalendarisch genau auf ihn
		      geh&ouml;rt. Und ganz genau wird alles in der Abendmesse des Gr&uuml;ndonnerstags,
		      die ja zur gleichen Stunde stattfindet wie Jesu Mahl mit seinen J&uuml;ngern
		      und nichts ist als die Vorabendmesse des Karfreitag, und schlie&szlig;lich
		      auch am Karfreitag selbst, wo die Gemeinde zu Jesu Todesstunde zusammenkommt.
		      Aber auch vom Ostertag an bis zum &#8222;Wei&szlig;en Sonntag&#8220; geht
		      die Linie weiter, denn an diesen Tagen werden immer wieder Erscheinungen
		      des Auferstandenen aus den vier Evangelien vorgelesen. Auf dieser Ebene,
		      die zumindest in der christlichen Neuzeit fast allein das gl&auml;ubige
		      Bewu&szlig;tsein pr&auml;gt, ist das Pascha Jesu also gewisserma&szlig;en
		      in Einzelszenen auseinandergenommen. </p>
		    <p class="text">Selbstverst&auml;ndlich sollte auch hier beim Einzelgeheimnis stets
		      das Ganze im Blick bleiben, so etwa am Karfreitag auf dem H&ouml;hepunkt
		      seiner Liturgie, bei der Kreuzverehrung. Da hei&szlig;t der Text des
		      ersten im Me&szlig;buch vorgesehenen Gesanges:</p>
		    <p class="text">Dein Kreuz, o Herr, verehren wir,<br>
  und deine heilige Auferstehung preisen und r&uuml;hmen wir:<br>
		      Denn siehe, durch das Holz des Kreuzes<br>
		      kam Freude in alle Welt.</p>
		    <p><span class="text">Leider erlebt man nur noch selten eine Karfreitagsliturgie,
		        wo dieser Text gesungen w&uuml;rde. Er wird h&auml;ufig ausgelassen. Wissen Pfarrer
		      und Liturgieaussch&uuml;sse nicht mehr, da&szlig; wir selbst am Karfreitag
		      keine reine Totenklage begehen, sondern das gesamte Ostergeheimnis
		      feiern?<br>
	        </span></p>
		    <p><span class="text">Texte: Georg Braulik, Norbert Lohfink	</span> </p>		    <p>&nbsp;</p>
		    <p class="text"><font face="Arial, Helvetica, sans-serif">	          <span class="text">Weitere Informationen:<br>
	          <a href="http://www.peterlang.com/index.cfm?vID=51819&vLang=D&vHR=1&vUR=3&vUUR=4" target="_blank">Georg
	          Braulik, Norbert Lohfink, Osternacht und Altes Testament</a>, Peter
	          Lang, Frankfurt/M </span></font><br>
		    </p>		    <p><font face="Arial, Helvetica, sans-serif"><b><font size="2"><br>
              </font></b><font size="2">&copy; kath.de<b><br>
              </b></font></font><font size="2">F<font face="Arial, Helvetica, sans-serif">eedback
              bitte an redaktion@kath.de</font></font></p>
          </td>
          <td class="L12" width="30%">            <div align="right"> 
              <p align="right">
			  
			  
			 <script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
google_ad_width = 336;
google_ad_height = 280;
google_ad_format = "336x280_as";
google_ad_type = "text_image";
//2006-10-23: Lexikon Symbole
google_ad_channel = "8495253357";
//--></script>
<script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
			  
			  
			  <br>
                <br>
                <font size="2" face="Arial, Helvetica, sans-serif"><br>
                <br>
                </font></p>
              <p>&nbsp;</p>
              <p><br>
              </p>
            </div>
          </td>
          <td background="../lexikon/liturgie/boxright.gif" width="8"><img src="../lexikon/liturgie/boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8"><img src="../lexikon/liturgie/boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="../lexikon/liturgie/boxbottom.gif" colspan="2"><img src="../lexikon/liturgie/boxbottom.gif" width="8" height="8" alt=""></td>
          <td width="8"><img src="../lexikon/liturgie/boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td width="100" align="left" valign="top">&nbsp;</td>
  </tr>
</table>
</body>
</html>
