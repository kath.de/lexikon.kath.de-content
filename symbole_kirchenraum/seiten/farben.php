titel:Farben -ihre symbolische Bedeutung
stichworte:Farben, Wei�, Schwarz, Gelb, Blau, Rot, Violett, Gr�n, Grau, kath
bild:bild.jpg

Wei�
Wei�e Gew�nder tragen die Heiligen im Himmel, das Gewand Jesu wird bei der Verkl�rung wei�. Das Wei� ist dem Papst vorbehalten. Wei� ist im Orient und fr�her auch am franz�sischen K�nigshof Zeichen der Trauer.

Schwarz
W�hrend Wei� die Farbe des Tages ist, steht Schwarz f�r Finsternis und ist damit Zeichen f�r Nacht, Vernichtung, Tod. Zugleich signalisiert Schwarz die Abkehr von allem Farbenfrohen und wird damit zur Farbe der Askese und Weltabgewandtheit.

Gelb
Die Farbe des Sonnenlichtes, des <a href="http://www.kath.de/lexikon/symbole_kirchenraum/index.php?page=gold.php">Goldes</a> und damit Sinnbild f�r den Himmel und die Ewigkeit. Gelb ist auch der Wein und damit dem Himmel zugeordnet. Das Gelb steht aber auch f�r Neid.

Blau
Diese Farbe wird dem Himmel zugeordnet, sie signalisiert Transparenz und Klarheit des Denkens. Auch das Wasser und der Diamant sind blau. Maria tr�gt einen blauen Mantel, weil sie in den Himmel aufgenommen wurde und als Himmelsk�nigin verehrt wird. Im Kampf gegen das B�se verbinden sich Wei� und Blau, meist oben im Bild gegen Gr�n und Rot unten im Bild, so im Kampf Michaels oder Georgs gegen den Drachen.

Rot
Diese Farbe steht f�r Blut und damit Opfer. Es ist die Farbe der M�rtyrer. Als Farbe des Blutes steht Rot f�r Leben. Da sie auch die Liebe symbolisiert, wird sie dem Apostel Johannes zugeordnet. Weil die Patrizier im alten Rom rote Kleidung trugen, ist diese Farbe heute den Kardin�len vorbehalten. Zugleich ist Rot die Farbe des Teufels und der im Hochmut gescheiterten Stadt Babylon.

Violett
Da diese Farbe aus Rot und Blau gemischt ist, steht sie f�r Besonnenheit und ma�volles Verhalten. Auf mittelalterlichen Gem�lden tr�gt Christus bei seiner Passion ein violettes Gewand. Daher werden im Advent und in der Fastenzeit violette Me�gew�nder getragen. Violett ist Farbe des Bischofs in der katholischen Kirche.

Gr�n
Diese Farbe steht zwischen dem Rot der H�lle und dem Blau des Himmels. Sie symbolisiert Ausgleich, Beschaulichkeit und als Farbe des Fr�hlings neues Leben. Gr�n steht auch f�r das Paradies und bezeichnet die Hoffnung. Weil die Rettung des Menschen vom Kreuz ausging, malten mittelalterliche K�nstler manchmal das Kreuz gr�n, um es als Baum des Lebens darzustellen. Gr�n kann aber auch der Teufel dargestellt werden.

Braun
Diese Farbe steht f�r das Erdhafte und den Herbst. Bei den R�mern wie auch in der christlichen Tradition steht Braun f�r Bescheidenheit und Demut. Die Franziskaner tragen daher eine braune Kutte.

Grau
Dass die Farbe aus Wei� und Schwarz gemischt ist, deutet auf die Auferstehung der Toten hin. Christus tr�gt beim Endgericht ein graues Gewand.
