titel:Hase, Osterhase
stichworte:Hase, Osterhase, kath
bild:bild.jpg

In der byzantinischen Kirche gilt der Hase als Symbol f�r die Auferstehung, weil er beim Schlafen die Augen nicht schlie�en soll, ein Bild daf�r, da� Jesus nicht im Tod geblieben ist. Gleiches wird in der Antike vom <a href="http://www.kath.de/lexikon/symbole_kirchenraum/index.php?page=loewe.php">L�wen</a> gesagt. Der Osterhase als durchg�ngiges Symboltier f�r Ostern kam erst im 19. Jahrhundert auf, als gen�gend Kakao und Zucker zur Verf�gung stand. Auch der eierlegende Osterhase ist eine neuere Bildgebung, die sich m�glicherweise von seiner Fortbewegung herleitet. Er hoppelt und bleibt dann sitzen, als ob er etwas ausbr�ten oder ein Ei legen w�rde.
Im Kreuzgang des Paderborner Doms dienen drei Hasen der Darstellung der Dreifaltigkeit. Die Hasen sind kreisf�rmig gemei�elt, so da� drei Ohren gen�gen, damit jeder Hase zwei Ohren hat.