titel:Bischofsstab
stichworte:Bischofsstab, Bischof, kath
bild:bischofsstab_mitra.jpg

Gekr�mmter Stab, den der Bischof beim Ein- und Auszug aus der Kirche tr�gt, bei der Verlesung des Evangeliums
und bei der Predigt, leitet sich vom Hirtenstab her und ist Ausdruck seiner Amtsvollmacht. Im Mittelalter war
mit der �bergabe des Stabes durch den K�nig die Investitur, d.h. die �bergabe der weltlichen Macht �ber das
Gebiet des Bistums verbunden. Das Sprichwort "Unter dem Kummstab l�sst sich gut leben" bezieht sich auf die
meist besseren Lebensbedingungen in den ehemaligen F�rstbist�mern, weil die Landesherren, die Bisch�fe,
etwas weniger Kriege f�hrten.