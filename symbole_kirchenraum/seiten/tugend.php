titel:Tugend
stichworte:Tugend, Glaube, Hoffnung, Liebe, Klugheit, Gerechtigkeit, Tapferkeit, M��igkeit, Demut, Sanftmut, Keuschheit, kath
bild:bild.jpg

Wie die <a href="http://www.kath.de/lexikon/symbole_kirchenraum/index.php?page=laster.php">Laster</a> gibt es auch Bildsymbole f�r die Tugenden und �hnlich wie bei den Lastern gibt es f�r fast jede Tugend mehrere Zeichen.
Die <a href="http://www.kath.de/lexikon/symbole_kirchenraum/index.php?page=leiter.php">Leiter</a>, die zum Himmel f�hrt, ist Symbol f�r tugendhaftes Leben.
F�r den <b>Glauben</b> stehen das Kreuz, der Kelch, der Leuchter, Buch und Buchrolle. Die <b>Hoffnung</b> kann mit einem <a href="http://www.kath.de/lexikon/symbole_kirchenraum/index.php?page=anker.php">Anker</a>, einer von einem Engel entgegengehalten Krone, einem Kompa� dargestellt werden. Die Farbe Gr�n geh�rt zur Hoffnung.
Die <b>Liebe</b> hat das flammende Herz, den ==>Pelikan. Wenn das Herz durch ein Schwert durchbohrt wird, deutet das auf in Liebe getragenes Leid. Die Farbe Rot geh�rt zur Liebe.
Die <b>Klugheit</b> ist schwer darzustellen. Wegen des biblischen Wortes, dass die Schlange klug ist, steht auch dieses, an sich negative gesehene Tier, f�r diese Tugend. Weiter der Spiegel.
<b>Gerechtigkeit</b> ist meist durch die Waage gekennzeichnet.
Die <b>Tapferkeit</b> wird durch den Ritter, das L�wenfell, ein Schild bezeichnet.
Die <b>M��igkeit</b> reitet auf einem L�wen, Kamel oder Elefanten. Wenn Wein mit Wasser gemischt wird, kann das auch auf diese Tugend hindeuten.
Das <a href="http://www.kath.de/lexikon/symbole_kirchenraum/index.php?page=lamm.php">Lamm</a> steht f�r <b>Demut</b> und <b>Sanftmut</b>. Die <b>Keuschheit</b> hat die Lilie und auch die Palme als Pflanze, weiter das <a href="http://www.kath.de/lexikon/symbole_kirchenraum/index.php?page=einhorn.php">Einhorn</a> als Tier.