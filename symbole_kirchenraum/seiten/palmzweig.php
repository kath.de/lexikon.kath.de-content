titel:Palmzweig
stichworte:Palmzweig, Palmsonntag, kath
bild:bild.jpg

Die Palme hat ihre Wurzeln im lebendigen Wasser. Dort ist auch der Gerechte verwurzelt: "Der Gerechte gedeiht wie die Palme, er w�chst wie die Zedern des Libanon." (Psalm 92,13).
Als Sinnbild der Freude und des Jubels dienen Palmwedel beim Laubh�ttenfest und auch beim Einzug des "Friedensk�nigs" Jesus in <a href="http://www.kath.de/lexikon/symbole_kirchenraum/index.php?page=jerusalem.php">Jerusalem</a>. Den Kirchenv�tern galt die Palme im Einklang mit der Antike als Zeichen der sieghaften Vollendung und des Triumphes, besonders der im Martyrium Vollendeten in Anlehnung an Offb 7,9: "Danach sah ich eine gro�e Schar aus allen Nationen ... Sie standen in wei�en Gew�ndern vor dem Thron und vor dem Lamm und trugen Palmzweige in den H�nden". Viele <a href="http://www.kath.de/lexikon/symbole_kirchenraum/index.php?page=maertyrer.php">M�rtyrer</a> werden mit einem Palmzweig in der Hand dargestellt.