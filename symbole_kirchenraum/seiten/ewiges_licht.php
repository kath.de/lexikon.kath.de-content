titel:Ewiges Licht
stichworte:Ewiges Licht, Tabernakel, kath
bild:bild.jpg

In der N�he des <a href="index.php?page=tabernakel.php">Tabernakels</a> brennt eine Kerze in einem roten Gef��. Damit wird der Beter darauf hingewiesen, dass sich im Tabernakel konsekrierte Hostien finden. Dieses Ewige Licht unterstreicht nicht nur die Kirche als Ort des Mysteriums, sondern ist das Symbol daf�r, dass Gottes Licht ewig auf Erden brennt. Jesus als das Licht der Erde, ein f�r allemal angez�ndet, erlischt nicht mehr. Er ist gegenw�rtig, so dass das Dunkel der Welt durch Gottes Gnadengegenwart immer gebrochen ist.