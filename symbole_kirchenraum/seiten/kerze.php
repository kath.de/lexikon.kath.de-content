titel:Kerze
stichworte:Kerze, kath
bild:bild.jpg

Sie steht mit ihrem brennenden Docht f�r das Gebet. Deshalb brennen sie vor Bildern der Mutter Gottes und werden an Wallfahrtsorten entz�ndet. Kerzen werden zum Gottesdienst angez�ndet. Die Taufkerze symbolisiert das neue Licht, das die Taufe im Menschen entz�ndet hat. Die Osterkerze wird in der Osternacht geweiht und steht im Chorraum w�hrend der Osterzeit und oft auch l�nger. Im Exsultet wird die Kerze besungen:
"Diese Kerze, geweiht zur Ehre deines Namens, brenne unerm�dlich weiter,
um das Dunkel dieser Nacht zu vernichten.
Als lieblicher Opferduft entgegengenommen,
mische sie sich unter die Lichter am Himmel.
Lodernde Flamme - so soll sie finden der Morgenstern.
Jener Morgenstern n�mlich, der keinen Untergang kennt:
Christus, dein Sohn, der, zur�ckgekehrt aus denen, die unter der Erde sind,
dem Menschengeschlechte heiter aufging
und der lebt und herrscht in alle Ewigkeit. Amen."
Es gibt einen eigenen Tag, an dem die Kerzen f�r den liturgischen Gebrauch geweiht werden, Maria Lichtmess. Dieser Tag, 40 Tage nach Weihnachten, erinnert an ein besonderes Reinigungsopfer f�r die Mutter Jesu. Am 2. Februar endete traditionell die Winterzeit.