titel:Narthex
stichworte:Narthex, Kirchenbau, kath
bild:bild.jpg

S�ulen-Vorhalle einer Kirche. Zuerst f�r fr�hchristliche byzantinische Kirchen verwendet. Der Begriff findet als Fachausdruck auch Anwendung auf mittelalterliche Kirchen. F�r die abendl�ndische Baukunst ist jedoch eher das <a href="http://www.kath.de/lexikon/symbole_kirchenraum/index.php?page=paradies.php">Paradies</a> kennzeichnend. Narthex bezeichnet im Griechischen eine Schilfart, die im Lateinischen <a href="http://www.kath.de/lexikon/symbole_kirchenraum/index.php?page=ferula.php">Ferula</a> genannt wird. Wenn dem Narthex ein weiterer Raum vorgelagert ist, spricht man von Innerem Narthex. Der vorgelagerte Raum wird �u�erer Narthex genannt.
Der Narthex hat f�r die Liturgie eine Funktion, weil sich hier der Zelebrant mit den anderen Funktionstr�gern versammelt, um dann einzuziehen. Der Narthex wurde zunehmend mit Darstellungen der Heiligen bzw. des Kirchenjahres ausgestaltet.