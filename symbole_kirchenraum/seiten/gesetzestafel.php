titel:Gesetzestafel
stichworte:Gesetzestafel, Gott, Israel,Moses, kath
bild:bild.jpg

Im 2. Buch der Bibel, Exodus ist der konstituierende Akt beschrieben, der den Bund, den Gott mit dem Volk Israel schlie�t, inhaltlich bestimmt. Es sind die Gebote, die dem Volk und den einzelnen den Weg zum Leben weisen. Diese Tafeln bringt Moses vom Berg aus seiner Begegnung mit Gott mit in das Lager und st��t auf ein Volk, das um das goldene Kalb tanzt. Er zerschl�gt aus Wut die Tafeln, fertigt sie aber dann neu an.