titel:Myrrhe
stichworte:Myrrhe, Drei K�nige, kath
bild:bild.jpg

Das Wort kommt vom semitischen Wortstamm f�r "bitter", es ist ein Harz. Neben Gold und Weihrauch war es eines der Geschenke der Weisen aus dem Morgenland, die das Kind in Bethlehem besucht haben. Im 2. Kapitel des Math�usevangeliums hei�t es in Vers 11: "Sie gingen in das Haus und sahen das Kind und Maria, seine Mutter; da fielen sie nieder und huldigten ihm. Dann holten sie ihre Sch�tze hervor und brachten ihm Gold, Weihrauch und Myrrhe als Gaben dar."