titel:Vogel
stichworte:Vogel, Paradies, Seele, kath
bild:bild.jpg

Dieses Motiv haben die Christen der Antike von g�ngigen Vorstellungen �ber die Seele �bernommen. Ein Vogel, der aus einem K�rper fliegt, stellt die Seele des Verstorbenen dar. V�gel geh�ren auch zu Paradiesesdarstellungen.
Der Vogel ist auch die gerettete Seele: "Unsere Seele ist wie ein Vogel, dem Netz des J�gers entkommen; das Netz ist zerrissen und wir sind frei," hei�t es in Psalm 124,7
V�gel haben jedoch nicht nur eine positive Bedeutung. Raubv�gel bewohnen Ruinen und fressen das Fleisch der erschlagenen Menschen:
"Danach sah ich einen anderen Engel aus dem Himmel herabsteigen; er hatte gro�e Macht und die Erde leuchtete auf von seiner Herrlichkeit. Und er rief mit gewaltiger Stimme: Gefallen, gefallen ist Babylon, die Gro�e! Zur Wohnung von D�monen ist sie geworden, zur Behausung aller unreinen Geister und zum Schlupfwinkel aller unreinen und abscheulichen V�gel." Apokalypse 18,1-2, s.u. Kap 19,17-21