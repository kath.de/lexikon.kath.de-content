titel:Kuss
stichworte:Kuss, Wertschätzung, Judas, kath
bild:bild.jpg

Der Kuss auf die Wange drückt Wertschätzung aus, vor allem von einem Höhergestellten. In der christlichen Tradition hat der Kuss eine negative Bedeutung. Die Tempelpolizei, die Jesus am Abend des Passahfestes festnahm, hatte mit seinem Verräter Judas den Kuss als ein Zeichen vereinbart, um Jesus in der Dunkelheit zu identifizieren.