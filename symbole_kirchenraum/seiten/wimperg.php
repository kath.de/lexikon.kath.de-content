titel:Wimperg
stichworte:Wimperg, gotik, Kirchenbau, kath
bild:bild.jpg

Als Wint-Berga werden die Mauerteile bezeichnet, die vor dem Wind sch�tzen. In der Gotik werden sie als Ziergiebel gebaut. Diese sind mit Ma�werk durchbrochen. Sie werden oft durch eine Kreuzblume gek�rnt, die aufsteigenden seitlichen Linien sind mit Krabben, Blumenmotiven verziert.
Auf Stadt und Burgmauern bezeichnen Wimperge die durchbrochenen Maust�cke, die die Verteidiger vor Geschossen sch�tzten.�