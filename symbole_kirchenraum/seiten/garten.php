titel:Garten
stichworte:Garten, Paradies, Gotik, Kirchenbau, kath
bild:bild.jpg

Der Garten steht f�r das Paradies. In der <a href="index.php?page=gotik.php">Sp�tgotik</a> und dann in der Renaissance werden Motive des Gartens in die Bildgestaltung sowohl der liturgischen B�cher wie der Kirchen aufgenommen. In der Sp�tgotik werden Pflanzenmotive in die Architektur integriert. S�ulen und Gew�lberippen werden als �ste geformt und die Gew�lbe mit Blumenmotiven ausgemalt, die den Kirchen den Charakter von "Himmelslauben" geben. In der symbolischen Ausgestaltung vieler Marienbilder wird das Gartenmotiv aufgenommen, als <a href=index.php?page=hortus_conclusus.php">hortus conclusus</a>, �bersetzt "verschlossener Garten".