titel:Gabriel
stichworte:Gabriel, Engel, Bote, kath
bild:bild.jpg

Dieser Engel wird selten f�r sich, sondern in seiner Funktion als Bote Gottes dargestellt, der Maria die Menschwerdung des Sohnes verk�ndet und ihr mitteilt, da� sie die Mutter des Messias sein wird. "Im sechsten Monat wurde der Engel Gabriel von Gott in eine Stadt in Galil�a namens Nazareth zu einer Jungfrau gesandt... Der Name der Jungfrau war Maria. Der Engel trat bei ihr ein und sagte: "Sei gegr��t, du Begnadete, der Herr ist mit dir" (Lukas 1 26-28).
Gabriel hat in seinem Namen das hebr. Wort "El", wie bei Micha-el und Rapha-el. Das bedeutet "Gott". Sein Name hei�t: "St�rke Gottes", weil dieser Engel das Wirken Gottes mitteilt.