titel:Kartusche
stichworte:Kartusche, kath
bild:bild.jpg

Hat sich aus einer Schnur entwickelt, die urspr�nglich um Inschriften, Wappen und Namen, z.B. unter Heiligenfiguren geschlungen wurde. Daher sind die meisten dieser Einrahmungen, die in der Renaissance und im Barock h�ufig gebraucht wurden, nicht rechteckig, sondern aus Kurven zusammengesetzt.