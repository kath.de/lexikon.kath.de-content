titel:Einhorn
stichworte:Einhorn, Marienbild, kath
bild:bild.jpg

Auf Marienbildern erscheint dieses Tier. Nach der griechischen Sage kann dieses Tier nur von einer Jungfrau gefangen werden. Dem Tier wird gro�e St�rke und Gef�hrlichkeit zugeschrieben. Sobald es eine Jungfrau erkennt, wird es zahm und legt seinen Kopf in ihren Scho�. Die Einhornjagd durch den Erzengel Gabriel findet sich auf vielen Malereien und Wandteppichen des Mittealters und symbolisiert die Empf�ngnis Jesu, die der Engel angek�ndigt hat.
Dem Einhorn wird in der Antike auch heilbringende Wirkung zuerkannt, deshalb wurde es zum Symbol f�r eine Apotheke