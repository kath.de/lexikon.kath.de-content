titel:Greif
stichworte:Greif, Tier, kath
bild:bild.jpg

Ein Tier mit einem Vogelkopf auf einen L�wenk�rper, der Fl�gel tr�gt, ist vorchristlich und bis nach Indien zu finden. Das Tier dr�ckt wie die Tiere der vier <a href="http://www.kath.de/lexikon/symbole_kirchenraum/index.php?page=evangelisten.php">Evangelisten</a> Kraft, Verstand. Bei den Griechen ist er das Reittier Apolls. Da diese Tierdarstellung h�ufig von den Persern benutzt wurde, gilt er bei den Juden als Symbol f�r die Perser. Im Mittelalter findet sich der Greif h�ufig in der Heraldik.