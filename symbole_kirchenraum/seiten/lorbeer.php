titel:Lorbeer
stichworte:Lorbeer, Kranz, Sieger, kath
bild:bild.jpg

Aus dem griechischen Kulturraum kommend, wird der Lorbeerkranz einem Sieger auf das Haupt gelegt. Da die Glaubenszeugen durch ihren Tod einen Sieg errungen haben, ist das Lorbeerblatt oder der Lorbeerkranz in der fr�hen Kirche ein Bild f�r das Glaubenszeugnis.