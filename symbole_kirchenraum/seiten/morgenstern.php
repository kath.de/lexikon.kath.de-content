titel:Morgenstern
stichworte:Morgenstern, Venus, Christus, kath
bild:bild.jpg

Mit dem Morgenstern ist der Planet Venus, zugleich auch der <a href="http://www.kath.de/lexikon/symbole_kirchenraum/index.php?page=abendstern.php">Abendstern</a> gemeint. Er steht f�r Christus, das aufgehende Licht. Ein bekanntes Kirchenlied von Angelus Silesius beginnt "Morgenstern der finstern Nacht, der die Welt voll Freude macht, Jesu mein �." Auch dieses Bild geht auf die Geheime Offenbarung zur�ck. Dort hei�t es in Kap. 25,16: "Ich, Jesus, habe meinen Engel gesandt als Zeugen f�r das, was die Gemeinden betrifft. Ich bin die Wurzel und der Stamm Davids, der strahlende Morgenstern."