titel:Nimbus
stichworte:Nimbus, Wolke, kath
bild:bild.jpg

Das Wort kommt von lateinisch "Wolke" und wurde erst im Sp�tlateinischen entwickelt. Gott erscheint in einer Wolke. Eigentlich stellt die runde Scheibe die Sonne dar und ist ein Attribut der griechischen G�tter. Deshalb steht der Nimbus eigentlich nur Christus zu. Sp�ter erhalten Maria, die Apostel und dann die Heiligen dieses W�rdezeichen. In den Nimbus Christi wird das Kreuz eingezeichnet. Da Christus auch durch das <a href="http://www.kath.de/lexikon/symbole_kirchenraum/index.php?page=lamm.php">Lamm</a> dargestellt wird, erh�lt dieses auch einen Nimbus.