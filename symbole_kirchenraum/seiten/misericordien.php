titel:Misericordien
stichworte:Misericordien, Sitzst�tze, kath
bild:bild.jpg

Damit werden Sitzst�tzen bezeichnet, die unter den <a href="http://www.kath.de/lexikon/symbole_kirchenraum/index.php?page=chor.php">Chorst�hlen</a> angebracht sind. Eigentlich kommt das Wort von dem lateinischen Begriff f�r Barmherzigkeit, es soll auch Barmherzigkeit f�r die Seite des M�nchs- oder Stiftschors ausdr�cken, die stehen mussten. Denn beim Psalmgebet steht die eine Seite des Chors, w�hrend die andere sitzt. Daf�r kann die Sitzfl�che der Chorbestuhlung hochgeklappt werden, unter die ein St�ck Holz angebracht ist, auf der man sich stehend etwas abst�tzen kann. Die Arme mit dem Psalmbuch kann man dann auf den Seitenlehnen abst�tzen.