titel:Lanze
stichworte:Lanze, Leidenswerkzeug, kath
bild:bild.jpg

Die Lanze geh�rt zu den Leidenswerkzeugen. Um den Tod Jesu festzustellen, stach einer der r�mischen Soldaten eine Lanze in das Herz des Gekreuzigten. Die Lanze gilt als Reliquie. Sie wird bei Schlachten gegen die Ungarn, so auch der auf dem Lechfeld 955 erw�hnt. Sie wurde schlie�lich im Kloster Corvey aufbewahrt. War in Frankfurt ein neuer Kaiser gew�hlt, wurde er erst gekr�nt, wenn die Lanze aus Corvey geholt worden war.