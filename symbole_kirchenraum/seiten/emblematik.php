titel:Emblematik
stichworte:Emblematik, Kirchenbau, kath
bild:bild.jpg

Das Wort leitet sich von dem Griechischen Begriff f�r "das Eingesetzte" ab. Es geht aber nicht um Intarsienarbeiten, sondern um die Verbindung von Symbolen mit kurzen Sinnspr�chen. Werke der Emblematik findet sich meist in Buchform, die Tradition beginnt in der Renaissance durch R�ckgriff auf die Antike.