titel:Hirte
stichworte:Hirte, Christus, kath
bild:bild.jpg

In Kirchen, die nach dem Ende der Verfolgung gebaut werden konnten, wird Christus in der Apsis h�ufig als Hirte dargestellt, die Schafe sind die Gl�ubigen. Das geht auf das Gleichnis des Guten Hirten zur�ck, bei Johannes im 10. Kapitel, bei Lukas im 15. Von diesem Gleichnis ausgehend wird Jesus als Hirte dargestellt, der ein Schaf auf den Schultern tr�gt. Der Gute Hirt war in der fr�hen Kirche das am meisten gebrauchte Bild f�r Christus, im Mittelalter wurde es das Kreuz.
Da die Bisch�fe in der Nachfolge der Apostel mit der Hirtenaufgabe dargestellt werden, tragen sie in der rechten Hand einen <a href="http://www.kath.de/lexikon/symbole_kirchenraum/index.php?page=bischofsstab.php">Hirtenstab</a>.