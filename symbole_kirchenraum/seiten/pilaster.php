titel:Pilaster
stichworte:Pilaster, Kirchenbau, Pfeiler, kath
bild:bild.jpg

S�ulenartiger Vorsprung, der eine Mauer gliedert. Im Unterschied zur <a href="http://www.kath.de/lexikon/symbole_kirchenraum/index.php?page=lisene.php">Lisene</a> hat ein Pilaster ein Kapitel. W�hrend die Lisene mehr ein Stilmittel der Romanik ist, werden Pilaster im Barock h�ufig eingesetzt. Sie geben der Mauer eine Dynamik nach oben, denn sie durchbrechen, auch bei Schl�ssern, die Waagrechte der Fensterreihen. Das Wort kommt vom lateinischen Wort f�r Pfeiler, pila