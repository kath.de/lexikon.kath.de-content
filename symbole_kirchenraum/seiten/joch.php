titel:Joch
stichworte:Joch, kath
bild:bild.jpg

Zwischen jeweils vier S�ulen l�sst sich ein Gew�lbe einsetzen. Manche Joche �berspannen auch den Raum zwischen sechs Pfeilern. Urspr�nglich bezeichnete Joch den Abstand zwischen zwei Pfeilern, gemessen jeweils vom Mittelpunkt. Meist sind die Joche durch sog. Gurt-B�gen gegliedert. Die <a href="http://www.kath.de/lexikon/symbole_kirchenraum/index.php?page=kreuzrippe.php">Kreuzrippen-Technik</a> erm�glicht eine besonders leichte Bauweise.