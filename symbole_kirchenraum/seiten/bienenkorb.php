titel:Bienenkorb
stichworte:Bienenkorb, Biene, kath
bild:bienenhaus.jpg

Die Biene gilt als Vorbild von Tugend und Gemeinschaftssinn. Der Bienenkorb ist daher Symbol f�r die Kirche. Biene bzw. Bienekorb werden gro�en Predigern als Symbol beigegeben, so Ambrosius von Mailand, Chrysostomus, (�bersetzt "Goldmund) und Bernhard von Clairvaux, weil ihr Mund von "honigs��er" Rede �berfloss.