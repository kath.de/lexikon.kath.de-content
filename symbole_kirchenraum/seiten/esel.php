titel:Esel
stichworte:Esel, Krippe, Jesaja, kath
bild:bild.jpg

Der Esel ist auf Grund eines Verses im Propheten Isaias der ==>Krippe zugewachsen. In Kap.1,3 hei�t "Der Ochse kennt seinen Besitzer und der Esel die Krippe seines Herrn." Jesus ist auf einem Esel in Jerusalem eingeritten, so dass der Esel in einigen Orten zum Palmsonntagsbrauchtum geh�rt. Der Esel steht f�r ein nicht-herrschaftliches Reittier.