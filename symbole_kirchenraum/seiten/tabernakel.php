titel:Tabernakel
stichworte:Tabernakel, kath
bild:bild.jpg

Ein durch zwei T�rchen verschlossener Kasten, der seit dem <a href="http://www.kath.de/lexikon/symbole_kirchenraum/index.php?page=barock.php">Barock</a> auf dem Altar zur Aufbewahrung konsekrierter Hostien steht. Das Wort kommt aus dem Lateinischen und hei�t Zelt, H�tte. Das spielt auf die <a href="http://www.kath.de/lexikon/symbole_kirchenraum/index.php?page=bundeslade.php">Bundeslade</a> an. Daher sind die Tabernakelt�rchen auch mit zwei <a href="http://www.kath.de/lexikon/symbole_kirchenraum/index.php?page=cherub.php">Cheruben</a> ausgestaltet ist, weil diese neben der Bundeslade angebracht waren.