titel:Gold
stichworte:Gold, kath
bild:bild.jpg

Symbolisiert das Ewige, die himmlische Herkunft, den himmlischen Glanz und h�chste Herrlichkeit. Gold ist elementar, es kann schmelzen und man kann es bearbeiten. Und trotzdem bleibt es Gold und damit Zeichen daf�r, da� das G�ttliche trotz aller Eingriffe und Verdunklung durch den Menschen das G�ttliche bleibt.
Gold korrespondiert auch mit der <a href="http://www.kath.de/lexikon/symbole_kirchenraum/index.php?page=fisch.php">Farbe</a> des in der Messe gebrauchten Weins.