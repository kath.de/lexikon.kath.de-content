titel:Fuchs
stichworte:Fuchs, Lust, Wollust, kath
bild:bild.jpg

In der antiken Fabelwelt steht der Fuchs f�r List und auch t�ckisches Verhalten. Da der Fuchs bei den Germanen dem Gott Loki zugeordnet war, konnte er auch den Teufel darstellen, denn heidnische Gottheiten wurden in der Missionspredigt als Gestalten des Teufels hingestellt. In diesem Zusammenhang �berlistet der Fuchs den Menschen zu falschem Glauben. In der Darstellung der Laster steht der Fuchs f�r die Wollust.