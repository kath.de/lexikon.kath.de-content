titel:Hoffnung
stichworte:Hoffnung, Tugend, Gr�n, kath
bild:bild.jpg

Eine der drei <a href="index.php?page=tugend.php">g�ttlichen Tugenden</a>, der die Farbe Gr�n" zugeordnet ist. Die Hoffnung ist anders zu verstehen als Optimismus, n�mlich als das im Glauben begr�ndete Vertrauen auf Gott, dass er sein Reich verwirklichen und dem Menschen ein Leben nach dem Tod schenken wird.