titel:Moses, mit H�rnern dargestellt
stichworte:Moses, Horn, Moses mit Horn, kath
bild:bild.jpg

Diese alttestamentliche Gestalt wird mit zwei H�rnern dargestellt, diese leiten sich von den beiden Augenflammen her, die Moses nach der Begegnung mit Gott auf dem Berg Horeb hatte. "Als Mose vom Sinai herunterstieg, hatte er die beiden Tafeln der Bundesurkunde in der Hand. W�hrend Mose vom Berg herunterstieg, wu�te er nicht, da� die Haut seines Gesichtes Licht ausstrahlte, weil er mit dem Herrn geredet hatte. Als Aaron und alle Israeliten Mose sahen, strahlte die Haut seines Gesichtes Licht aus und sie f�rchteten sich, in seine N�he zu kommen." Exodus 34,29 