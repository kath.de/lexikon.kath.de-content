titel:Kreuzweg
stichworte:Kreuzweg, kath
bild:bild.jpg

Er hat seinen Ursprung in den Umg�ngen bzw. im Nachgehen des Leidensweges Jesu in Jerusalem seit dem 4. Jahrhundert. Der Brauch wurde von Jerusalempilgern in den Westen gebracht. Zuerst wurden Kreuzwege an H�geln und Bergen angelegt. Etwa um 1700 begann man, auch im Kircheninneren Kreuzwege zu errichten, indem man die vierzehn Stationen durch Holzkreuze markierte und darunter h�ufig bildlich den Inhalt, z.B. "Jesus f�llt unter dem Kreuz", darstellte. Andere Formen der Verehrung des Leidens Jesu waren die "�lberge" wie auch die Darstellungen der Gei�elung oder der Dornenkr�nung Jesu.