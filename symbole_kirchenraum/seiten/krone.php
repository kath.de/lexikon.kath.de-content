titel:Krone
stichworte:Krone, Kranz, kath
bild:bild.jpg

Die Krone hat sich aus einem Kranz entwickelt, der als Zeichen des Sieges um den Kopf gelegt wurde. Sie ist daher oft nur ein Reif. Schon in der Antike symbolisierte die Krone k�nigliche oder kaiserliche Macht. Heiligendarstellungen mit einer Krone auf dem Kopf verweisen jeweils auf einen heiligen K�nig oder Kaiser. Wenn Christus eine Krone tr�gt, dann ist das Ausdruck seiner K�nigsw�rde, die er als Nachkomme des K�nigs David beanspruchen konnte. Die <a href="http://www.kath.de/lexikon/symbole_kirchenraum/index.php?page=tiara.php">Tiara</a> des Papstes ist eine Dreifachkrone, die die drei �mter Christi, das K�nigs- das Priester- und das Propheten- oder Lehramt widerspiegeln.