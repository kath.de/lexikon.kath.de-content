titel:Dom
stichworte:Dom, Haus, Bischof, Kirche, kath
bild:bild.jpg

Das Wort kommt vom lateinischen domus - Haus. Auch wenn heute mit dem Begriff "Dom" meist eine Bischofskirche bezeichnet wird, meint Dom nicht das Haus eines Bischofs, sondern das Haus Gottes.