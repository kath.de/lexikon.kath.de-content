titel:Pantokrator
stichworte:Pantokrator, Weltenherrscher, Christus, kath
bild:bild.jpg

Ein in der fr�hen Kirche, in der Romanik und im 19. Jahrhundert wieder aufgenommene Tradition des <a href="http://www.kath.de/lexikon/symbole_kirchenraum/index.php?page=christusbild.php">Christusbildes</a>, Jesus als den Weltenherrscher darzustellen. Sie geht auf das Bild des <a href="http://www.kath.de/lexikon/philosophie_theologie/menschensohn.php" target="_blank">Menschensohnes</a> zur�ck, das im Buch Daniel Kap. 7 f�r den Weltenrichter zu finden ist. Jesus wurde in den Evangelien mit dem Menschensohn identifiziert. <a href="http://www.kath.de/lexikon/symbole_kirchenraum/index.php?page=majestas_domini">Majestas Domini</a>