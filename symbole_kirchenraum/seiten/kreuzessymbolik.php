titel:
stichworte:Kreuzessymbolik,Himmelssymbolik, Kreuz, kath
bild:bild.jpg

Um den Kirchenraum auf ein theologisches Grundkonzept zur�ckzuf�hren, dient neben der Himmelssymbolik das Kreuz als Grundriss. Seit die Romanik das Querschiff eingef�hrt hat, liegt eine solche Ausdeutung des Kirchenraumes nahe. Das Langschiff entspricht den Beinen Jesu, der Chor symbolisiert das Haupt. Das erkl�rt, warum bei einigen gotischen Kirchen, so beim Stephansdom in Wien, der Chor ein wenige von der Mittellinie verschoben ist. Damit wird an das geneigte Haupt des sterbenden Jesus am Kreuz erinnert.