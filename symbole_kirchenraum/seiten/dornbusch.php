titel:Dornbusch
stichworte:Dornbusch, kath
bild:bild.jpg

Dieses Motiv ist dem Alten Testament entnommen. Als Abraham Isaak opfern will und ihn ein Engel davon abh�lt, hat sich ein Widder mit seinen H�rnern in einem Dornbusch verfangen und kann als Opfertier genommen werden.
Aus dem Dornbusch, der nicht verbrennt, h�rt Moses die Stimme Gottes, der ihn beauftragt, das Volk aus �gypten herauszuf�hren.
Der Dornbusch wird in Verbindung mit der Dornenkrone gesehen, die Jesus zum Spott aufgesetzt wurde.