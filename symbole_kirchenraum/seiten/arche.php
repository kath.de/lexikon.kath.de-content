titel:Arche
stichworte:Arche, Schiff, Sintflut, Noah, Kirchenbau, kath
bild:bild.jpg

In dem Holzschiff, das Noah zur Rettung vor der Sintflut gebaut hatte, sehen Theologen die Kirche vorgebildet. Noah steht f�r die Seele des Geretteten und ist daher �fters auf fr�hchristlichen Sarkophagen eingraviert.
Die Taube, die Noah aussendet und die mit einem �lzweig zur�ckkommt, ist Zeichen der Rettung und wird oft ohne die Arche dargestellt.
Da das Buch Genesis die Ma�e der Arche festh�lt, 300 Ellen in der L�nge, 50 Ellen breit und 30 Ellen hoch (Genesis 6,14) konnten die Gr��enverh�ltnisse f�r den Kirchbau �bernommen werden.
Weiter ist berichtet, dass die Arche drei Stockwerke hatte (Genesis 6,16), wie sie sich, wenn man die Krypta hinzunimmt, in den romanischen Kirchen findet.