titel:Kreis
stichworte:Kreis, kath
bild:bild.jpg

Die runde Linie, die in ihren Anfang zurückkommt, ist Symbol der Zeit und des Neuanfangs. Zugleich ist der Kreis Zeichen der Vollkommenheit und damit des Himmels. 