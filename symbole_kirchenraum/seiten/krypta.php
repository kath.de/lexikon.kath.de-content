titel:Krypta
stichworte:titel, H�hle, kath
bild:bild.jpg

Das Wort leitet sich von griechisch "H�hle" her. Im Kirchbau ist die Krypta ein meist unter dem Chor der Kirche liegendes Gew�lbe zur Aufbewahrung der Reliquien oder der sterblichen �berreste von Herrschern und den Stiftern der Kirche, die jedoch meist unter dem Westwerk begraben werden. Sp�ter wurde die Krypta auf das Querhaus ausgedehnt. Die symbolische Bedeutung liegt wie bei der <a href="http://www.kath.de/lexikon/symbole_kirchenraum/index.php?page=confessio.php">Confessio</a> darin, dass die feiernde Gemeinde auf dem Bekenntnis der Heiligen und M�rtyrer steht, die der jetzigen Generation vorausgegangen sind.
Als in der Gotik die <a href="http://www.kath.de/lexikon/symbole_kirchenraum/index.php?page=reliquien.php">Reliquien</a> in die Oberkirche geholt werden, finden sich keine Krypta mehr, es sei denn, diese sei von dem Vorg�ngerbau noch erhalten geblieben, so in Chartres.