titel:Gnadenstuhl
stichworte:Gnadenstuhl, Barock, Kirchenbau, kath
bild:bild.jpg

Gott Vater h�lt das Kreuz mit dem Gekreuzigten in beiden H�nden, �ber ihnen schwebt der Heilige Geist als Taube. Diese Darstellung entwickelt sich im Sp�ten Mittelalter und ist im Barock ein h�ufiges Motiv. Das Bild wird deshalb in den Zusammenhang mit der Gnade gebracht, weil der f�r die S�nder Gekreuzigte das Erbarmen des Vaters gegen�ber den Menschen darstellt. Das Wort wurde von Luther anstelle f�r das Wort  "Thron" genutzt, auf dem Gott sitzt. 