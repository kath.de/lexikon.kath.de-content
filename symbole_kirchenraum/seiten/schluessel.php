titel:Schl�ssel
stichworte:Schl�ssel, Petrus, kath
bild:bild.jpg

Jesus hat <a href="http://www.kath.de/lexikon/symbole_kirchenraum/index.php?page=petrus.php">Petrus</a> die Schl�ssel des Himmelreiches �bertragen. Diese werden oft als Doppelschl�ssel gezeigt, denn Jesus hat den Aposteln die Macht, zu binden und zu l�sen gegeben. Das findet in der altkirchlichen Bu�praxis ihren Ausruck. Der B��ende wird in seine S�nden "gebunden", indem er w�hrend der 40 Tage der Fastenzeit aus der Gemeinschaft der Glaubenden ausgeschlossen bleibt. Am Gr�ndonnerstag wird der B��ende aus seinen S�nden gel�st.