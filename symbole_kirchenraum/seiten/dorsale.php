titel:Dorsale
stichworte:Dorsale, Chorgest�hl, kath
bild:bild.jpg

R�ckw�nde des <a href="index.php?page=chorgestuehl.php">Chorgest�hls</a>. Da in vielen gotischen Kirchen ein Chorumgang vorhanden ist, steht das Chorgest�hl nicht direkt an der Wand, sondern hat zu dem Chorgest�hl eine R�ckwand, die oft bemalt oder verziert ist. Der Name leitet sich vom lateinischen Wort f�r R�cken: dos, ab.