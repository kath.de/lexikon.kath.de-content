titel:Nagel
stichworte:Nagel, Leidenswerkzeuge, kath
bild:bild.jpg

Die N�gel geh�ren zu den <a href="http://www.kath.de/lexikon/symbole_kirchenraum/index.php?page=leidenswerkzeuge.php">Leidenswerkzeugen</a>, da mit ihnen Christus ans Kreuz geschlagen wurde. in dem <a href="http://www.kath.de/lexikon/symbole_kirchenraum/index.php?page=ihs.php">IHS-Wappen</a> finden sich oft unten drei N�gelk�pfe. Sie stehen f�r die Gel�bde Armut, Ehelosigkeit, Gehorsam der Ordensleute.