titel:Hortus conclusus
stichworte:Hortus conclusus, Garten, Paradies, Gotik, kath
bild:bild.jpg

Dieser lateinische Ausdruck steht f�r einen "verschlossenen Garten". Das Motiv findet sich h�ufig in der sp�ten Gotik, Maria mit dem Kind in einem ummauerten Garten. Das Bild ist dem Hohen Lied des Alten Testaments entnommen. In Kap. 4, 12 hei�t es: " Ein verschlossener Garten ist meine Schwester Braut, ein verschlossener Garten, ein versiegelter Quell." Das Hohe Lied ist ein Liebesgedicht, das allegorisch (==>Kap. 1  "symbolisch sehen") auf die Beziehung der Seele zu Gott ausgelegt wird. Weiter wird in der Braut die Kirche gesehen, im Zentrum der Kirche steht Maria. Das Hohe Lied hat die deutsche Frauenmystik inspiriert.