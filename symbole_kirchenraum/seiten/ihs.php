titel:IHS
stichworte:IHS, Trigramm, kath
bild:bild.jpg

Das Trigramm, drei Buchstaben, leitet sich vom Namen Jesus her, der im Mittelalter Jhesus geschrieben wurde. Es wird auch auf das Zeichen gedeutet, unter dem Konstantin an der milvischen Br�cke die Schlacht gewann: In hoc signo vinces: In diesem Zeichen wirst du siegen. Es kann als "Jesus Hominum Salvator": Jesus, der Menschen Erl�ser" gelesen werden. Im sp�ten Mittelalter l�ste diese Zeichen das Chi/Rho ab. Von den Jesuiten wurde es �bernommen und findet sich an den von ihnen erbauten und sp�ter an den Barockkirchen.