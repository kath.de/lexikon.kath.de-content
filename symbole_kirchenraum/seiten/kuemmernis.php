titel:K�mmernis, heilige
stichworte:heilige K�mmernis, Heilige, kath
bild:bild.jpg

Eine mit einem Gewand bekleidete ans Kreuz geschlagene Heilige. Nach der Legende soll sie Tochter eines portugiesischen K�nigs gewesen sein. Als dieser sie einem nicht-getauften Prinzen verm�hlen wollte, bat sie darum, dass ihr ein Bart wachse, damit ihr Gesicht h�sslich wirke. M�glicherweise leitet sich die Kreuzesdarstellung daher, dass der Vater sie nicht nur verstie�, sondern auch kreuzigen lie�. In Westeuropa ist die Verehrung ab 1400 dokumentiert, im Barock lebt sie wieder auf, um jetzt vergessen zu sein.