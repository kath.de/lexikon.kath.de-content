titel:Delphin
stichworte:Delphin, kath
bild:bild.jpg

Dieser menschrettende Meeresbewohner findet sich auf fr�hchristlichen Sarkophagen und Grabstelen. Der Delphin stellt Christus dar, der die Menschen aus dem Meer der S�nde rettet. Der Delphin n�hert sich dem Symbol
<a href="http://www.kath.de/lexikon/symbole_kirchenraum/index.php?page=fisch.php">Fisch.</a>