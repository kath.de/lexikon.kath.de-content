titel:Bock
stichworte:Bock, Wollust,Aaron, kath
bild:steinbock.jpg

Dieses Tier stellt die Wollust dar, weil es das Reittier der Liebesg�ttin Aphrodite war. Im Mittelalter wird der Teufel oft in Bockform dargestellt. Der Bock ist ein Opfertier des j�dischen Kultes gewesen und gewinnt in der Funktion des S�ndenbocks eine reinigende Funktion: Er tr�gt die S�nden des Volkes in die W�ste hinaus.
Im Buch Levitikus findet sich folgende Anweisung f�r die j�hrliche Ents�hnung des Volkes: Der Priester Aaron "soll den lebenden Bock herbringen lassen. Aaron soll seine beiden H�nde auf den Kopf des lebenden Bockes legen und �ber ihm alle S�nden der Israeliten, alle ihre Frevel und alle ihre Fehler bekennen. Nachdem er sie so auf den Kopf des Bockes geladen hat, soll er ihn durch einen bereitstehenden Mann in die W�ste treiben lassen und der Bock soll alle ihre S�nden mit sich in die Ein�de tragen. Kap 16, 20-22