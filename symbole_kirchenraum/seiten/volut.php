titel:Voluten
stichworte:Voluten, Barock, Kirchenbau, kath
bild:bild.jpg

Schneckenförmige Verzierung, charakteristisch für die jonischen Säulenkapitelle. Der Barock verwendet diese Zierform wieder intensiv.