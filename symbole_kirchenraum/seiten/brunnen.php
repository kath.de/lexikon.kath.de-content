titel:Brunnen
stichworte:Brunnen, Erkenntnis, Reinigung, kath
bild:brunnen.jpg

Im Hebr�ischen werden Auge und Brunnen mit der gleichen Lautfolge bezeichnet. Daher symbolisiert der Brunnen Erkenntnis. Das Wasser steht f�r Segen und Reinigung. Der Brunnen ist wegen des Wasserritus der Taufe im Christentum ein Bild f�r die Aufnahme in die Kirche, die mit einer Reinigung von den S�nden verbunden ist. Im sog. <a href="http://www.kath.de/lexikon/symbole_kirchenraum/index.php?page=paradies.php"></a>Paradies</a> steht wie im Garten Eden ein Brunnen.