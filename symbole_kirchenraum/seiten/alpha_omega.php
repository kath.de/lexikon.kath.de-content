titel:Alpha und Omega, &#913; und &#937;
stichworte:Alpha, Omega, kath
bild:bild.jpg

sind der erste bzw. der letzte Buchstabe des griechischen Alphabets. Im Buch der Geheimen Offenbarung bezeichnet sich so Gott Vater  "Ich bin das Alpha und das Omega, spricht Gott der Herr, der ist und der war und der kommt, der Herrscher �ber die ganze Sch�pfung"; Offenbarung des Johannes 1,8: vgl. auch Offb. 21,6 Christus als der kommende Richter sagt von sich dasselbe aus: "Ich bin das Alpha und das Omega, der Erste und der Letzte, der Anfang und das Ende". Offb 22,13: Sp�ter wurde das Symbol zusammen mit dem Kreuz oder dem Christusmonogramm vornehmlich als Symbol Christi verwandt, so z.B. auf der Osterkerze.
Im Deutschen ist die Bedeutung von Alpha und Omega in der Aussage "das "A und O einer Sache" �bernommen worden.