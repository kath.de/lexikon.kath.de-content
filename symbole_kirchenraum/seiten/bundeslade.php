titel:Bundeslade
stichworte:Bundeslade, Tempel, Truhe, Jude, kath
bild:bild.jpg

Das eigentliche Heiligtum des j�dischen Volkes war eine Truhe aus Akazienholz, die mit Gold �berzogen war. Die Bauanleitung findet sich im 2. Buch der Bibel, Exodus Kap. 25, 10-40. Dort wird auch die Herstellung eines Tisches und der Leuchter genau beschrieben.
Die Bundeslade enthielt die zwei Steintafeln, auf denen die 10 Gebote eingemei�elt waren. Die Bundeslade wurde im Zelt und sp�ter in der inneren Zelle, dem Allerheiligsten des Tempels, aufbewahrt.
Die Bundeslade wurde von zwei Engel <a href="index.php?page=cherub.php">Cherubim</a> eingerahmt, die aus Gold getrieben sind (Exodus 37, 7-9) Die Bundeslade gilt sp�testens seit der Zerst�rung Jerusalems durch den neubabylonischen K�nig Nebukadnezar 586 v. Chr. als verschollen. Sie ist jedoch immer wieder Gegenstand von Spekulationen und auch Romanen, dass sie n�mlich immer noch irgendwo zu finden sie.
In Erinnerung an die Bundeslade werden in den Synagogen die Pergamentrollen der j�dischen Bibel in einer Lade aufbewahrt. Der <a href="index.php?page=tabernakel.php">Tabernakel</a> wird oft als Lade gestaltet, auf seinen beiden T�rchen sind die beiden Engel abgebildet.