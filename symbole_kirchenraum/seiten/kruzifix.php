titel:Kruzifix
stichworte:Kruzifix, Kreuz, kath
bild:bild.jpg

Das Wort verbindet das lateinischen figere - heften mit Crux, <a href="http://www.kath.de/lexikon/symbole_kirchenraum/index.php?page=kreuz_gekreuzigter.php">Kreuz</a> und bedeutet "Der ans Kreuz Geschlagene". Das Wort bezeichnet also Kreuze mit dem Corpus Jesu. Beim Kruzifix stehen Maria und Johannes, entsprechend dem Bericht des Johannesevangeliums 19,26