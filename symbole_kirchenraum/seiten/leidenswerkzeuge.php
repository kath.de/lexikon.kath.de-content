titel:Leidenswerkzeuge
stichworte:Leidenswerkzeuge, kath
bild:bild.jpg

Neben dem Kreuz geh�ren die Gei�el, die Dornenkrone, Hammer und N�gel und die <a href="http://www.kath.de/lexikon/symbole_kirchenraum/index.php?page=lanze.php">Lanze</a> zu den Leidenswerkzeugen. Sie werden oft um das Kreuz dargestellt, in Barockkirchen auch an den W�nden, um auszudr�cken, dass die Erl�sung durch das Leiden am Kreuz geschehen ist.