titel:Kapitell
stichworte:Kapitell, kath
bild:bild.jpg

Abschluss einer S�ule. Daher kommt auch der Name, n�mlich von dem lateinischen Wort f�r Kopf: caput. Bereits in der Antike wurde der Abschlu�stein gestaltet. Unterschieden werden die dorische (niedriger, zylindrsicher Stein), die jonische (mit <a href="http://www.kath.de/lexikon/symbole_kirchenraum/index.php?page=volut.php">Voluten</a>) und die korinthische S�ule, letztere gestaltet das Kapitell mit <a href="http://www.kath.de/lexikon/symbole_kirchenraum/index.php?page=arkanthus.php">Arkanthus-Laub</a>. Die Kapitelle wurden neben den Portalen in der Romanik erste Pl�tze f�r Skulpturen. Sie wurden nicht mehr mit Voluten oder Ornamente gestaltet, sondern mit Figurengruppen werden meist biblische Szenen dargestellt.