titel:Heilige, Heiligenschein (Aura)
stichworte:Heilige, Heiligenschein, Aura, kath
bild:bild.jpg

Heilige sind Vorbilder im Glauben und Helfer bei Gott. Um die Vielzahl der Heiligen unterscheidbar zu machen, haben sie Attribute, die z.B. ihre Todesart als M�rtyrer f�r den Glauben bezeugen. Daher tr�gt Petrus das Petruskreuz, Paulus das Schwert, mit dem er hingerichtet wurde, Andreas das nach ihm benannte Andreaskreuz, Bonifatius ein Schwert im Evangelienbuch, denn er wollte sich mit dem erhobenen Evangelium sch�tzen und wurde durch ein Schwert umgebracht. H�ufig verweisen die Attribute auch auf bekannte Taten und Wunder aus dem Leben der Heiligen, wie etwa im Falle der Mantelteilung des Martin von Tour.
Oberhalb der Augenh�he des Betrachters sind Menschen dargestellt, die oft mit dem Heiligenschein eine besondere Aura erhalten. Es sind die Menschen, die die Vollendung erlangt haben, die Gott im Himmel schauen. Wenn die Gotik �ber dem Heiligenstandbild ein kleines Dach anbringt, steht dieses f�r den Heiligenschein.
Die Ikonostase der orthodoxen Kirchen besteht aus Bildern mit Heiligen, die den Beter aus dem Bereich des Himmels anblicken.