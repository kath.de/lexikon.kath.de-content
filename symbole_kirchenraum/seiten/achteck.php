titel:Achteck
stichworte:Vierung, Achteck, Taufkirchen, Himmelsymbolik
bild:bild.jpg

Wenn �ber die Vierung eine Kuppel gebaut ist, hat sie in der Regel acht Ecken, genauso wie viele Taufbecken und Taufkapellen. Auch Begr�bniskirchen sind wie in Ottmarsheim im Elsa� achteckig gebaut. Der achte Tag ist der Tag der Auferstehung und damit Anfang der neuen Sch�pfung, der himmlischen Welt. Auf diese Weise unterst�tzt das Achteck die Himmelssymbolik des Kirchenraumes.