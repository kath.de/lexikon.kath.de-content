titel:Labyrinth
stichworte:Labyrinth, M�anderband, kath
bild:bild.jpg

In ein Quadrat oder einen Kreis ist ein M�anderband, meist aus verschiedenfarbigen Steinen, eingef�gt. Dieses Symbol f�r den menschlichen Lebensweg kannten schon die R�mer. Die Christen haben das Labyrinth bereits in der Antike �bernommen. Das Band f�hrt �ber die ganze Fl�che und endet in der Mitte, die das himmlische Jerusalem symbolisiert.