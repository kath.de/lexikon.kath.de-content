titel:Osten
stichworte:Osten, kath
bild:bild.jpg

Eigentlich ist die Erde nach Norden, auf den Polarstern hin, orientiert. Denn um die Achse  Erde-Polarstern dreht sich nicht nur die Erde, sondern der gesamte Sternhimmel. Auf den Polarstern verweist der <a href="http://www.kath.de/lexikon/symbole_kirchenraum/index.php?page=obelisk.php">Obelisk</a>. Da das Christentum sich in einer Kultur ausbreitete, die ganz von der Himmelssymbolik, dem Lauf der Sonne durch die Sternkreiszeichen, gepr�gt war, sind die Kirchen nach Osten ausgerichtet. Im Osten geht die Sonne auf, Symbol f�r die Auferstehung Jesu. Weiter liegt Jerusalem im Osten, dort soll Jesus zum letzten Gericht wieder erscheinen. Orgines, (185-254) schreibt:
"Die Vers�hnung kommt aus dem Osten. Denn von dort kommt der Mann, dessen Name "Anfang" ist, der Mittler zwischen Gott und den Menschen. Damit ergeht an dich die Aufforderung, immer nach Osten auszuschauen, wo die Sonne der Gerechtigkeit aufgeht, wo dir immer neu das Licht geboren wird, damit du niemals im Schatten zu gehen brauchst."
Die Bedeutung des Ostens ist bis heute in unserem Sprachgebrauch im Begriff "Orientierung" erhalten.