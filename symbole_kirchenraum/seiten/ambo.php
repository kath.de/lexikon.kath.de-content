titel:Ambo
stichworte:Ambo, Lesepult, Erh�hung, kath
bild:bild.jpg

Der griechische Name f�r Lesepult leitet sich von "Erh�hung" ab. Der Lektor musste zu sehen sein. Weil es erst seit wenigen Jahrzehnten eine Lautsprecheranlage gibt, musste der Lektor �ber Jahrhunderte allein durch seine Stimme die letzten Reihen in der Kirche erreichen. Deshalb wurde der Sprechgesang entwickelt, der die Vokale verl�ngert. <a href="http://www.kath.de/lexikon/symbole_kirchenraum/index.php?page=choral_gregorianisch.php">Gregorianischer Choral</a>. Das Pult des Ambos wurde im Mittelalter als Adler gestaltet.
Oft f�hrt eine Treppe zum Ambo hinauf. Auf dieser Treppe stand der Vors�nger f�r den Zwischengesang, weshalb dieser, meist den Psalmen entnommene Gesang, "Graduale" hei�t, von lateinisch "Stufe".
In manchen Kirchen sind zwei Lesepulte aufgestellt, eines f�r das Evangelium, das andere f�r die Lesungen aus dem Alten Testament oder den Apostelbriefen.