titel:Kommunion, Kommunionbank
stichworte:Kommunion, Kommunionbank, kath
bild:bild.jpg

Da fr�her die Kommunion kniend empfangen wurde, der Priester legte dem Gl�ubigen die Hostie in den Mund, gab es am �bergang aus dem Kirchenschiff zum Chorraum eine Stufe, auf die man knien konnte. die Kommunionbank, meist aus Stein oder Metall, erm�glichte es, sich abzust�tzen und die H�nde auf die Kommunionbank zu legen. Seitdem die Hostie in die Hand gelegt wird, haben die Kommunionb�nke ihre Funktion verloren und sind oft abgebaut worden. Wenn sie k�nstlerisch ausgestaltet waren, dann meist mit �hren und Weintrauben, die auf Brot und Wein als eucharistische Gaben hinweisen.