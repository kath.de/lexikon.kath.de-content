titel:Lettner
stichworte:Lettner, Trennwand, Chor, kath
bild:bild.jpg

Diese aus Holz bzw. Stein konstruierte Trennwand teilte den Chor, in dem Chorherren bzw. Ordensfrauen das Chorgebet verrichteten, vom Kirchenschiff. Vor dem Lettner ist ein Altar f�r die Gl�ubigen aufgebaut. Der Lettner beruhte wohl auf dem Bestreben, zwischen den Klerikern einerseits und den Laien eine deutliche Trennung herbeizuf�hren. Das Konzil von Trient hat den Abriss der Lettner angeordnet, so dass es heute nur noch wenige Exemplare gibt, meist in mittelalterlichen Kirchen, die von protestantischen Gemeinden genutzt werden, so im Dom von Naumburg. Der Name leitet sich vom lateinischen Lectorium, Lesepult, her. So wurde der Lettner auch f�r die Lesung, die Predigt und auch kleine Ch�re genutzt.