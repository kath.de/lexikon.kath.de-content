titel:Grab, Heiliges
stichworte:Grablegung, Jesus, Gotik, Kirchenbau, kath
bild:bild.jpg

Vor allem in der <a href="index.php?page=gotik.php">Gotik</a> wurde die Grablegung Jesu durch gro�e Skulpturen dargestellt. Um das Grab stehen die in den Evangelien erw�hnten Personen: "Gegen Abend kam ein reicher Mann aus Arimath�a namens Josef; auch er war ein J�nger Jesu. Er ging zu Pilatus und bat um den Leichnam Jesu. Da befahl Pilatus, ihm den Leichnam zu �berlassen. Josef nahm ihn und h�llte ihn in ein reines Leinentuch. Dann legte er ihn in ein neues Grab, das er f�r sich selbst in einen Felsen hatte hauen lassen. Er w�lzte einen gro�en Stein vor den Eingang des Grabes und ging weg. Auch Maria aus Magdala und die andere Maria waren dort; sie sa�en dem Grab gegen�ber.  Math�us 27, 57-61
H�ufig ist das Heilige Grab in einer Nische an der Au�enmauer einer Kirche aufgestellt, es findet sich jedoch auch an den Seitenw�nden im Innenraum.