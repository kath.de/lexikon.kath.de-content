titel:Netz
stichworte:Netz, Menschenfischer, kath
bild:bild.jpg

Die christliche Deutung bezieht sich auf Worte Jesu, dass die Apostel Menschenfischer werden sollen. Daher ist das Netz, �hnlich wie das Schiff, ein Bild f�r die Kirche. In der Antike sind Steine mit einem Netz umspannt, �gyptische Mumien sind netzartig eingebunden. Diese Netze stehen f�r den Lauf der Sonne und der Gestirne, die sich in der Sonnenuhr, vor allem wenn sie als Hohlkugel ausgeformt ist, netzartig abbilden.