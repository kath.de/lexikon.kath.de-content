titel:Fels
stichworte:Fels, Petrus, kath
bild:bild.jpg

Im alten Testament wird Gott "Fels Israels" genannt Ich will den Namen des Herrn verk�nden. "Preist die Gr��e unseres Gottes! Er hei�t: der Fels. Vollkommen ist, was er tut;  denn alle seine Wege sind recht. Er ist ein unbeirrbar treuer Gott, er ist gerecht und gerade." Deuteronomium 32,4. Aus einem Felsen schl�gt Moses mit seinem Stock Wasser (Numeri 17,6). Dieser Felsen wird mit Christus in Beziehung gesetzt 1 Kor 10,4 Auch <a href="http://www.kath.de/lexikon/symbole_kirchenraum/index.php?page=petrus.php">Petrus</a> wird Fels genannt, Jesus gibt ihm diesen Namen anstelle von Simon. Auf dem Felsen Petrus soll die Kirche aufgebaut werden