titel:Apotrop�isch
stichworte:Apotrop�isch, Bestiarium, Apokalypse, Abwehr, kath
bild:bild.jpg

Die Abwehr des B�sen durch die wilde Tiere und Bestien soll die <a href="index.php?page=bestiarium.php>Bestiarien</a> in romanischen Kirchen erkl�ren. Da die Tierdarstellungen in der Apokalypse f�r das B�se stehen, stellen die Tierplastiken sehr wohl das B�se dar, das aber �berwunden ist, so wie es die <a href="index.php?page=apokalypse.php">Apokalypse</a> beschreibt.