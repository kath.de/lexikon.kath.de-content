titel:Kirche/Ecclesia
stichworte:Kirche, Ecclesia, kath
bild:bild.jpg

Sie wird h�ufig durch eine allegorische Frauengestalt symbolisiert. Sie steht als Frau neben der Synagoge. Letztere ist an den verbundenen Augen erkennbar. Im Barock wird ihr Triumph in den Deckenfresken gezeigt. Auch durch das Symbol des <a href="http://www.kath.de/lexikon/symbole_kirchenraum/index.php?page=schiff.php">Schiffes</a> wird sie dargstellt.
Das Wort Ekklesia kommt aus dem Griechischen und bezeichnet die "Herausgerufenen", so ruft eine Kirche die Gl�ubigen mit ihren Glocken aus dem Alltag in den himmlischen Bereich. Kyriak�-Versammlung des Herrn ist die Wurzel des deutschen Wortes "Kirche". In dem Wort ist Kyrios, Herr enthalten. Kirche sind diejenigen, die sich der Herrschaft des auferstandenen Christus unterwerfen. Das Kyrie am Beginn der Messfeier ist der Begr��ungsruf, der Christus gilt.