titel: Startseite

<h2>Zur Einf�hrung:</h2>

Da Symbole wie auch Sakramente auf eine nicht-materielle Wirklichkeit hindeuten, mu� man sie zu lesen verstehen. Hier ist ein W�rterbuch zusammengestellt, das auch dazu dienen soll, die mittelalterlichen und barocken Kirchen mit ganz anderen Augen zu betrachten, weil diese Kirchen immer den Himmel darstellen.

<b>Symbol hei�t griechisch das "Zusammengeworfene"</b>

Zusammengeworfen wurden die Teile eines auseinandergebrochenen Tont�felchens. Es best�tigte den Freundschaftsbund, beide nahmen einen Teil mit, beim Wiedertreffen zeigte sich, ob die Tonscherben sich l�ckenlos zu einer Tafel verbanden. Auch ein auseinandergebrochener Ring konnte als Symbol dienen.

Der erweiterte Symbolbegriff hat die urspr�ngliche Bedeutung erweitert: Zwei Seiten, zwei Ebenen, zwei Welten werden im Symbol zusammengebracht. Die Christen haben aus der Antike viele Symbole �bernommen und eigene entwickelt, z.B. das Achteck f�r ein Taufbecken oder die Kuppel einer romanischen Kirche, das den achten Tag als Tag der Auferstehung und damit den Beginn des neuen Lebens bezeichnet. Das Ursymbol des Christentums ist der Mensch gewordene Gott, er vereint Erde und Himmel, er verbindet das Menschliche mit dem G�ttlichen.

Im sichtbaren Symbol ist eine andere Wirklichkeit anwesend. Das ist auch die christliche Umdeutung des Wortes Sakrament. Es hei�t urspr�nglich die Weihe zum Kriegsdienst, es kommt vom lateinischen "sacer", das f�r den abgegrenzten heiligen Bezirk steht. Die Sakramente der christlichen Kirche haben die gleiche symbolische Doppelheit, ein sichtbares Zeichen wie z.B. Wasser, �l, Brot deuten auf einen religi�sen Gehalt. Mittels des �u�eren Zeichens handelt Gott, schenkt die Taufe, das eucharistische Brot, eine Weihe.

Autor des Lexikons: Dr. Eckhard Bieger SJ
technische Umsetzung: B. Richter und J. Pelzer nach einer Vorlage von R. Jouaux

Feedback bitte mit dem Stichwort Symbollexikon an: redaktion@kath.de
<a href="http://www.kath.de/seiten/impressum.html">Impressum</a>