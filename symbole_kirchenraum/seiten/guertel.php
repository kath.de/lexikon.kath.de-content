titel:G�rtel
stichworte:G�rtel, kath
bild:bild.jpg

Einen G�rtel tragen die Benediktiner, einen Strick die Franziskaner, mit einem Zingulum umg�rten die Priester und Diakon die Albe, �ber die sie dann noch das Me�gewand ziehen.
In der Bibel steht der G�rtel f�r Gerechtigkeit und Kraft, mit der Gott den Berufenen umg�rtet.
F�r das Geschlechtsleben signalisiert der G�rtel Schutz des Genitalbereiches und Abgrenzung. Deshalb tragen bereits die Eremiten und heute noch M�nche und Nonnen den G�rtel als Zeichen geschlechtlicher Enthaltsamkeit.
