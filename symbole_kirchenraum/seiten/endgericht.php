titel:Endgericht
stichworte:Endgericht, Ende der Welt, kath
bild:weltgericht.jpg

Die erste christliche Generation erwartete das Ende der Welt noch zu ihren Lebzeiten. Der ruf "Marantha", Herr komme bald, war Teil des Gottesdienstes. Auch wenn das Ende der Welt noch nicht angebrochen ist, geht die Erwartung der Christen auf einen Abschluß der Weltgeschichte, die in dem letzten Gericht dargestellt wird. Die Vorlage dafür liefert das 25. Kapitel des Matthäusevangeliums mit seinem Gleichnis vom letzten Gericht, in dem die Böcke von den Schafen geschieden werden.
Das Endgericht wird meist mit dem auf dem <a href="http://www.kath.de/lexikon/symbole_kirchenraum/index.php?page=deesis.php">Richterstuhl thronenden Christus</a> dargestellt. Es kann aber auch der Thronsitz alleine dargestellt werden, in Verbindung mit dem Kreuz.
Ein anderes Motiv stellt einen Engel dar, der das Firmament aufrollt. Die Erde wird durch wilde Tiere repräsentiert. Der Erzengel Michael mit der Waage, auf der die Seelen der Verstorbenen gewogen werden, ist ein weiteres Motiv, mit dem das Endgericht dargestellt wird.