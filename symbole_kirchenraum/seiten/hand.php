titel:Hand
stichworte:Hand, kath
bild:bild.jpg

Sie ist in sich ein Symbol. Sie steht ge�ffnet f�r das Empfangen und ausgestreckt f�r Wegweisung und Handeln. Die Hand Gottes wird in der fr�hchristlichen Fresken und Mosaiken von oben, aus dem Himmel kommend, dargestellt. Meist deutet die Hand auf eine Berufung hin. In Bezug auf Gott stellt die rechte Hand Gottes Barmherzigkeit dar, die linke seine Gerechtigkeit. Deshalb segnet der Priester mit der rechten Hand, w�hrend der K�nig mit der linken Hand regiert.
Wenn beide Arme ausgebreitet und die H�nde nach oben gehalten werden, ist das der k�rperliche Ausdruck f�r Gebet. In der Liturgie segnen H�nde im Auftrag Gottes, sie salben den Menschen bei der Taufe, bei der Priesterweihe und auf dem Krankenbett mit geweihtem �l. Beim Friedensgru� im Gottesdienst dr�ckt die Hand Vers�hnung und Gemeinschaft aus.t