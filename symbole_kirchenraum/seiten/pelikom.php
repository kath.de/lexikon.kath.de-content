titel:Pelikan
stichworte:Pelikan, Vogel, Jesus, kath
bild:bild.jpg

Der Pelikan bringt in seinem Kehlsack Fische zu seinen Jungen. Diese presst er aus dem Mund heraus, so dass Fischblut seine wei�en Federn r�tet. Das hat zur Fabel gef�hrt, der Pelikan rei�e seine Brust auf, um seine Jungen zu f�ttern. Damit wurde er zum Symbol der sich selbst aufopfernden Elternliebe. Diese Symbolik wurde von den Christen auf Jesus �bertragen.