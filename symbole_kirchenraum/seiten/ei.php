titel:Ei
stichworte:Ei, Ostern, kath
bild:bild.jpg

Das Ei steht f�r neues Leben und symbolisiert zugleich den gesamten Kosmos. An Ostern gibt es auch deshalb viele Eier, weil in den Ostkirchen und fr�her auch im Westen w�hrend der vierzigt�tigen Fastenzeit keine Eier gegessen wurden.
Das Aufschlagen des Eis nach der Osternachtfeier zielt nicht darauf, m�glichst lange die Schale seines Eis zu erhalten, sondern umgekehrt. Wie das K�ken aus dem Ei schl�pft, ist Christus aus dem Grab erstanden. Da das Eigelb in der Ikonenmalerei verwandt wird, steht die gesamte Malerei in Beziehung zu Ostern.