titel:Lisene
stichworte:Lisene, inhalt, kath
bild:bild.jpg

Andeutung eines Pfeilers, der keine tragende Funktion hat, sondern eine Wand gliedert. Ein Stilmittel der Romanik, leitet es sich vom franz�sischen Wort f�r Kante, lisi�re, ab. Im Unterschied zu einem <a href="http://www.kath.de/lexikon/symbole_kirchenraum/index.php?page=pilaster.php">Pilaster</a> hat eine Lisene kein Kapitell.