titel:Apostelleuchter/Apostels�ulen
stichworte:Apostelleuchter, Apostels�ule, kath
bild:bild.jpg

Die Kirche ruht auf dem Fundament der Apostel und M�rtyrer; "Die Mauer der Stadt [das himmlischen Jerusalem] hat zw�lf Grundsteine; auf ihnen stehen die zw�lf Namen der zw�lf Apostel des Lammes" Geheime Offenbarung 21,14. Um dies zu vergegenw�rtigen, wurden das Kirchenschiff oder der Chor in fr�heren Zeiten tats�chlich von 12 S�ulen getragen (auch Anklang an die "S�ulen" im Galaterbrief). In vielen Kirchen finden sich bis heute 12 Apostelleuchter oder zw�lf Kreuze an den W�nden, die auf diesen Sachverhalt hinweisen.