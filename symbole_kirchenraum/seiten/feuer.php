titel:Feuer
stichworte:Feuer, Element, kath
bild:bild.jpg

Neben Erde, Wasser und Luft galt das Feuer als eines der vier Elemente, aus denen alles Gegenständliche zusammengesetzt ist.
Das Feuer ist das Element, das Gott am nächsten kommt. Deshalb erscheint Gott im Feuer Exodus 3,1-5, 24,17, 1 Könige 18,38, Ezechiel 1,2
Feuer kann verzehrend und vernichtend sein, jedoch auch den Menschen mit dem Geist Gottes begaben. Am Pfingsttag kam der Geist auf die Apostel in roten Zungen herab.