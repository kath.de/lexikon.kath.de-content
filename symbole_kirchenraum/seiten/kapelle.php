titel:Kapelle
stichworte:Kapelle, kath
bild:bild.jpg

Die kleineren R�ume f�r den Gottesdienst stehen einzeln, fr�her z.B. die Taufkapellen. Sie sind aber auch an gro�e Kirchen angegliedert. Auch Burgen, Schl�sser, Krankenh�user und kirchliche Einrichtungen haben eine Kapelle. Der Name kommt von der Capa, dem Mantel des hl. Martin, der in einem Seitenraum der Bischofskirche von Tours aufbewahrt wurde. F�r Reliquien wurden eigens Seitenr�ume angebaut, die dann auch Kapellen hie�en. Ein Kaplan ist der Priester, der nicht die gro�e Kirche, sondern eine Kapelle betreut.