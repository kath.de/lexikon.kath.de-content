titel:Evangelisten, Evangelistensymbole
stichworte:Evangelisten, Evangelistensymbole, kath
bild:bild.jpg

Der Ursprung der Evangelistensymbole reicht zur�ck bis in den babylonischen Mythos. Die vier Astralg�tter Nergal = Fl�gell�we, Marduk = Fl�gelstier, Nabu = Mensch und Mimurta = Adler, stellen Symbole g�ttlicher Macht dar. Meist sind die vier K�pfe in einem Wesen vereint, das als W�chter vor dem Heiligt�mern aufgestellt ist. In einer Vision schaut der alttestamentliche Prophet Ezechiel (vgl. Ez 1, 1-14) die Herrlichkeit Gottes in diesen vier Lebewesen, wie dies auch die Offenbarung des Johannes berichtet (Offb 4, 6-8). Die Kirchenv�ter Iren�us und Hippolyt bezogen erstmals die vier Wesen der Ezechiel-Vision und der Offenbarung auf die Evangelisten. Die jetzt gebr�uchliche Verteilung findet sich bei Hieronymus:
<b>Mensch</b> = Matth�us, sein Evangelium beginnt mit der Darlegung der menschlichen Abkunft Jesu,
<b>L�we</b> = Markus; das Evangelium beginnt mit dem T�ufer Johannes, dem "Rufer aus der W�ste"; Markus wird auch deshalb mit dem L�wen dargestellt, weil im Auftreten Jesu die messianische Zeit des Friedens beginnt, in der Kalb und L�we nebeneinander auf der Weide leben k�nnen, weil der L�we Gras fri�t,
<b>Stier</b> = Lukas; sein Evangelium beginnt mit dem Opfer des Zacharias; Lukas hat den Stier auch deshalb bei sich, weil Jesus am Kreuz geopfert wird und das Kalb bzw. der Stier als Opfertiere gelten.
<b>Adler</b> = Johannes; aus ihm spricht der von oben kommende Geist am m�chtigsten.

An Kanzeln und Kuppelzwickeln des Barock sind die Evangelisten-Symbole h�ufig mit den vier lateinischen Kirchenv�tern Augustinus, Ambrosius, Hieronymus und Gregor dem Gro�en dargestellt, um so im Sinne der "Gegenreformation" die Kontinuit�t der Tradition, auf die sich die katholische Kirche berief, zu unterstreichen.