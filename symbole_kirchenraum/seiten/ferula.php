titel:Ferula
stichworte:Ferula, Hirtenstab, Papst, kath
bild:bild.jpg

m Unterschied zu den Bisch�fen tr�gt der Papst keinen gekr�mmten Hirtenstab, sondern einen nach oben gerade auslaufenden Stab. Das Zepter als Zeichen der Autorit�t ist ein verk�rzter Stab. Johannes Paul II. trug einen Stab, der in ein Kruzifix auslief. Ferula leitet sich von dem Namen f�r ein Schilfrohr ab und ist auch die �bersetzung f�r Kirchen vorgelagerte S�ulenhalle, die auch <a href="http://www.kath.de/lexikon/symbole_kirchenraum/index.php?page=narthex.php">Narthex</a> genannt wird. Daher taucht der Begriff auch in Beschreibungen von Kirchen auf.