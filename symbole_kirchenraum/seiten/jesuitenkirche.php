titel:Jesuitenkirche
stichworte:Jesuitenkirche, kath
bild:bild.jpg

<h2>die Epoche zwischen Renaissance und Barock</h2>

Bereits im 16. Jahrhundert gr�ndeten die Jesuiten in ganz Europa Kollegien, H�here Schulen, die auch Fakult�ten beherbergten. Jedes Kolleg hatte eine Kirche, die sich an der Konzeption von Il Gesu in rom, der ersten vom Jesuitenorden gebauten Kirche orientiert. Der Stil l�st die strenge Form der Renaissance auf, findet aber noch nicht zu den Ellipsen und Hyperbeln des Barock. 