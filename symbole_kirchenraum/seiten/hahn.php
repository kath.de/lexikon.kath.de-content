titel:Hahn
stichworte:Hahn, kath
bild:bild.jpg

Der Hahn ist der Sonne zugeordnet, er ist ihr Bote. Deshalb ist er das Tier des jeweiligen Sonnengottes, im griechischen G�tterhimmel dem Apoll.
Im Neuen Testament wird mit dem Hahn die Voraussage Jesu verkn�pft "Da sagte Petrus zu ihm: Auch wenn alle (an dir) Ansto� nehmen - ich nicht! Jesus antwortete ihm: Amen, ich sage dir: Noch heute Nacht, ehe der Hahn zweimal kr�ht, wirst du mich dreimal verleugnen." Markus 14,29f