titel:Limbus - Totenreich
stichworte:Limbus, Totenreich, kath
bild:bild.jpg

Nach der Aussage des 1. Petrusbriefes  ist Christus "auch zu den Geistern gegangen, die im Gef�ngnis waren, und hat ihnen gepredigt." (Kap. 3,19) Nach dem Glaubensbekenntnis der r�mischen Kirche, Apostolisches Glaubensbekenntnis genannt, ist Christus nach seinem Tod am Kreuz "hinabgestiegen in das Reich des Todes", um die Verstorbenen aus dem Wartestand zu befreien und ihnen das Tor zum Himmel zu �ffnen. Dieses Motiv des Hinabstiegs wird h�ufig auf Ikonen dargestellt und geh�rt zum �sterlichen Bilderzyklus.