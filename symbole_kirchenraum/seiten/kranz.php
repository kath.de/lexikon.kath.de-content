titel:Kranz
stichworte:Kranz, kath
bild:bild.jpg

Er war das Ehrenzeichen des siegreichen Athleten, z.B. bei den Olympischen Spielen der Antike in Form eines Gewindes aus Laub, Blumen u.�. Das Christusmonogramm war in der fr�hen Kirche �fters von einem Kranz umgeben und bezeichnete so Christus als den Sieger �ber den Tod (daher h�ufig auf Sarkophagen) oder - da die siegreichen r�mischen Kaiser den Lorbeerkranz trugen - als Kyrios, den Herrn der Welt. Als Siegeszeichen geb�hrte der Kranz auch den M�rtyrern. 