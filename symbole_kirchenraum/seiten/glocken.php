titel:Glocken
stichworte:Glocke, kath
bild:bild.jpg

Die gro�en Glocken, die erst seit dem 14. Jahrhundert gegossen wurden, haben in Zimbeln, Gl�ckchen und den kleineren Glocken, mit denen in den Kl�stern zum Gebet gerufen wird, ihre Vorg�nger. Im Gottesdienst selbst gibt es die Schellen oder kleinen Gl�ckchen, die meist zu vier zusammengebunden, von den Me�diener zur Wandlung erklingen. Ebenfalls zur Wandlung in der Eucharistiefeier erklingen Glocken im Turm, um die, die nicht am Gottesdienst teilnehmen, auf diesen wichtigen Punkt im Ablauf der Messe aufmerksam zu machen.