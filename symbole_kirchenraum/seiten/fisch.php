titel:Fisch
stichworte:Fisch, Christus, kath
bild:bild.jpg

In der fr�hen Kirche findet sich das Fischsymbol besonders h�ufig. Auch heute wieder gilt der Fisch z.B. als Aufkleber auf Autos, als Zeichen der Zugeh�rigkeit zu einer christlichen Kirche. Das Symbol wurde wohl deshalb gew�hlt, weil der Getaufte im Netz der Kirche aufgefangen war. Das Auswerfen des Netzes wird von Jesus selbst als Symbol f�r die Gewinnung von Menschen f�r den Glauben gesehen. Im Matth�usevangelium beruft Jesus die Br�derpaare Petrus und Andreas, Jakobus und Johannes: "Kommt her, folgt mir nach! Ich werde euch zu Menschenfischern machen." Kap 4,19 Erst im 17. Jahrhundert begann man, die griechischen Buchstaben des Wortes Fisch auf Jesus zu deuten:
Die Anfangsbuchstaben des griechischen Wortes <b>&Iota;&Chi;&Theta;&Upsilon;&Sigma;</b> (Ichthys) hei�en �bersetzt:
<b>&Iota;</b>&eta;&sigma;&omicron;&upsilon;&sigmaf; - Jesus
<b>&Chi;</b>&rho;&iota;&sigma;&tau;&omicron;&sigmaf; - Christus
<b>&Theta;</b>&epsilon;&omicron;&upsilon; (Theou) - Gottes
<b>&Upsilon;</b>&iota;&omicron;&sigmaf; (Yios) - Sohn
<b>&Sigma;</b>&omicron;&tau;&eta;&rho; (Soter) - Erl�ser
