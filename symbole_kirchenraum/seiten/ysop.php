titel:Ysop
stichworte:Ysop, Essig, Schwamm, Jesus, kath
bild:bild.jpg

In der Bibel ist damit ein Stab gemeint, auf den die Soldaten einen Schwamm mit Essig steckten. Sie reichten ihn Jesus an den Mund, damit er etwas Entlastung von den Schmerzen haben sollte. Bei Johannes hei�t es: "Danach, als Jesus wusste, dass nun alles vollbracht war, sagte er, damit sich die Schrift erf�llte: Mich d�rstet. Ein Gef�� mit Essig stand da. Sie steckten einen Schwamm mit Essig auf einen Ysopzweig und hielten ihn an seinen Mund." Kap. 19, 28-29
Die Ysoppflanze ist ein Lippenbl�tler, der auf trockenen B�den w�chst und etwa 60 cm hoch wird.