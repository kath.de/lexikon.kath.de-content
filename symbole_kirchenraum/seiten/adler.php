titel:Adler
stichworte:Adler, Evangelist, Johannes, Logos, Kanzel
bild:


Der Adler ist das Tiersymbol des Evangelisten Johannes. Er findet sich an Kanzeln wie auch in vielen Bildern, die Christus darstellen. Der Adler steht f�r die geistige Kraft des Menschen und wird deshalb dem 4. Evangelisten zugeschrieben, weil dessen Evangelium mit dem Hymnus �ber den g�ttlichen Logos beginnt. Der Ambo, das Lesepult, ist in der Romanik oft als Adler gestaltet.