titel:Kirchenschiff
stichworte:Kirchenschiff, kath
bild:bild.jpg

Die Bezeichnung des Langhauses und der seitlichen G�nge als "Schiff" leitet sich davon her, dass die Kirchenv�ter die Gemeinschaft der Glaubenden als Schiff bezeichneten, das die Gl�ubigen aus dem Sturm der Zeit rettet. Daher wird auch die <a href="http://www.kath.de/lexikon/symbole_kirchenraum/index.php?page=arche.php">Arche</a> zum Sinnbild f�r die Kirche