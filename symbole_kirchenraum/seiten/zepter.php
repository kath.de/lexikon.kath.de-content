titel:Zepter
stichworte:Zepter, Stab, Herrscher, kath
bild:bild.jpg

Das Wort kommt von Skeptrum, dem griechischen Wort f�r Stab. Es ist als kurzer Stab zum Zeichen f�r weltliche Herrschaft geworden. Im kirchlichen Bereich entspricht es dem Hirtenstab des Bischofs.