titel:Chi, &#935;
stichworte:Chi, X, inhalt, kath
bild:bild.jpg

Der griechische Buchstabe &#935;, Chi ausgesprochen, ist der Anfangsbuchstabe von Christus. Es hat eine kosmische Bedeutung, den schon Platon sieht im &#935; die Struktur des Weltalls abgebildet, n�mlich den Winkel zwischen dem Sternkreis und dem Sonnen�quator, der durch die Ekliptik bestimmt ist. Das &#935; hat in der Mitte eine Senkrechte, das die Weltachse wiedergibt, die auf den Polarstern hin gerichtet ist. Um diese Achse dreht sich der Sternenhimmel. Die senkrechte Linie hat oben einen Halbkreis, der das griechische Rho, &#929;, das nicht unserem "P" sondern dem "R" entspricht. &#935;-&#929; (Ch-R) sind die beiden Anfangsbuchstaben von Christus.