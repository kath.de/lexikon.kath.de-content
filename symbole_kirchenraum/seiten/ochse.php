titel:Ochse
stichworte:Ochse, Stier, Lukas, kath
bild:bild.jpg

Dieses Wort begegnet uns wie L�we und Adler als Namen von Gastwirtschaften. Es verweist auf den Stier, der dem <a href="http://www.kath.de/lexikon/symbole_kirchenraum/index.php?page=evangelisten.php">Evangelisten</a> Lukas zugeordnet ist. Zur <a href="http://www.kath.de/lexikon/symbole_kirchenraum/index.php?page=krippe.php">Krippe</a> kam der Ochse, weil ein Zitat aus dem Propheten Jesaja den Ochsen mit dem Esel erw�hnt. In Kap.1, 3 hei�t "Der Ochse kennt seinen Besitzer und der Esel die Krippe seines Herrn."