titel:Filiale
stichworte:Filiale, T�rmchen, Gotik, Kirchenbau, kath
bild:bild.jpg

Kleine T�rmchen, charakteristisch f�r die <a href="index.php?page=gotik.php">Gotik</a>, die auf Strebepfeiler, �ber Portalen und an der Seite von <a href="index.php?page=wimperg">Wimpergen</a> zu finden sind. In den Filialen auf den Strebepfeilern stehen oft Engels- und Heiligeiguren, die von au�en her das Geb�ude sch�tzen sollen. Im Kirchenraum verzieren sie die <a href="index.php?page=sakramentshaeuschen.php">Sakramentsh�uschen</a>. Der Begriff leitet sich nicht vom lateinischen filia=Tochter her, von dem der Begriff "Werks-Filiale" stammt, sondern vom italienischen Wort Foglia-Nadel.