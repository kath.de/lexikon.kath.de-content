titel:Kuppel
stichworte:Kuppel,Gew�lbe, kath
bild:bild.jpg

Ein, meist �ber der <a href="http://www.kath.de/lexikon/symbole_kirchenraum/index.php?page=vierung.php">Vierung</a>, gebautes Gew�lbe. In der Romanik bildet die Kuppel meist ein Achteck, in der Renaissance kn�pft man an die r�mische Kuppelbautechnik an, die sich im Pantheon in Rom erhalten hatte, und baut Kuppeln als Halbkugeln. Im Barock werden Kuppeln �ber elliptischen Raumkompositionen abgeh�ngt. In Kirchen symbolisiert die Kuppel immer den Himmel. Deshalb findet sich im Scheitel der Kuppel oder in der <a href="http://www.kath.de/lexikon/symbole_kirchenraum/index.php?page=laterne_turmaufsatz.php">Laterne</a> meist ein Symbol, das, wie das Dreieck, auf die Dreifaltigkeit oder die Taube, die auf den Heiligen Geist hinweist.