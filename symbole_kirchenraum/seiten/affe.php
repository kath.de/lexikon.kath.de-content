titel:Affe
stichworte:heiliges Tier, besiegter Teufel
bild:bild.jpg

In �gypten und Indien gilt der Affe als heiliges Tier. In der christlichen Bildsprache hat er negative Vorzeichen. Er steht f�r L�sternheit und List. Wird er gefesselt dargestellt, ist damit der durch die Erl�sung besiegte Teufel gemeint.