titel:Kentauer
stichworte:Kentauer, Centaur, kath
bild:bild.jpg

Tier aus der Sagenwelt mit menschlichem Kopf. Symbolisiert das B�se, das mit Pfeilen auf den <a href="http://www.kath.de/lexikon/symbole_kirchenraum/index.php?page=hirsch.php">Hirsch</a>, Bild f�r den Gl�ubigen, oder auf <a href="http://www.kath.de/lexikon/symbole_kirchenraum/index.php?page=vogel.php">V�gel</a>, Bild f�r die Seele, zielt.