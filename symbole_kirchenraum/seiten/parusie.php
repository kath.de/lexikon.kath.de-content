titel:Parusie
stichworte:Parusie, Weltgericht, kath
bild:bild.jpg

Das Wort kommt vom griechischen "gegenw�rtig, "wirkm�chtig" und meint die zweite Ankunft Christi zum Weltgericht. Der endg�ltige Kampf zwischen Gut und B�se wird in der Apokalypse beschrieben. Dieser geht dem <a href="http://www.kath.de/lexikon/symbole_kirchenraum/index.php?page=weltgericht.php">Weltgericht</a> voraus