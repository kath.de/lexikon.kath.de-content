titel:Obergaden
stichworte:Obergaden, Kirchenbau, kath
bild:bild.jpg

Solange die Seitenschiffe niedriger gebaut sind als das Mittelschiff, können Fenster eingesetzt werden, die das Mittelschiff heller werden lassen.
In Süddeutschland und der Schweiz wird der Holzaufbau von Wohntürmen, der meist über das Mauerwerk hinausragt, auch Obergaden genannt. Das Wort Gaden bezeichnet ursprünglich ein Haus mit einem Raum, oft wurden Vorratshäuser so genannt.