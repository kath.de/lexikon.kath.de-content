titel:Ma�werk
stichworte:Ma�werk, Kirchenbau, Gotik, Fenster, kath
bild:masswerk.jpg

Geometrische Muster, mit denen vor allem in der <a href="http://www.kath.de/lexikon/symbole_kirchenraum/index.php?page=gotik.php">Gotik</a> Fenster, Balustraden, Fassaden und Turmhelme gegliedert werden.