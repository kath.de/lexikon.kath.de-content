titel:Blut
stichworte:Blut, Jesus, Becher, kath
bild:bild.jpg

Das Blut gilt als Tr�ger des Lebens. In den j�dischen Opferriten wurde es weder verbrannt noch, wie das Muskelfleisch der Opfertiere, f�r den Verzehr durch Menschen freigegeben, sondern am Fu� des Altars ausgesch�ttet. Jesus hat beim Abendmahl vor seinem Tod dem Blut eine neue symbolische Bedeutung gegeben, indem er es auf seinen nahen Tod bezogen hat. "Dieser Kelch ist der Neue Bund in meinem Blut, das f�r euch vergossen wird." (Lukas 22,22) Der Becher war mit <a href="index.php?page=brot_wein.php">Brot und Wein</a> gef�llt, der seitdem im <a href="index.php?page=mahl.php">eucharistischen Mahl</a> f�r das vergossene Blut Jesu steht. Blut ist in der christlichen Tradition mit dem Martyrertod verbunden.