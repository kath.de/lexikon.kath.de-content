titel:Granatapfel
stichworte:Granatapfel, Heilmittel, Blut, Tugend, kath
bild:bild.jpg

Diese Frucht kannten bereits die �gypter, sie gilt als Heilmittel, was auch die moderne Forschung best�tigt, so gegen Kreislaufbeschwerden und Blutdruck. Der Name leitet sich von "granae", dem lateinischen Wort f�r Kerne, K�rner her. Der Granatapfel hat viele Kerne, die ihn auch zum Symbol f�r Fruchtbarkeit machen. Im Hohen Lied steht der Granatapfel f�r die Sch�nheit der Braut (4,3 und 6,7) Die rote Farbe der Kerne und des Fleisches werden auch manchmal als Symbol f�r das Blut des Gekreuzigten gesehen. Wenn Maria mit einem Granstapel dargestellt wird, soll die gro�e Zahl ihrer Tugenden zum Ausdruck gebracht werden.