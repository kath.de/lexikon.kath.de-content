titel:Efeu
stichworte:Efeu,Treue, ewiges Leben, kath
bild:bild.jpg

Diese immergr�ne Pflanze steht f�r Treue und ewiges Leben. Die Pflanze ist in Griechenland dem Dionysis geweiht. Bei festlichen Gelagen wurde es als Kranz getragen. Die Pflanze findet sich auf fr�hchristlichen Sarkophagen. Efeu bedeutet, dass die Seele lebt, auch wenn der K�rper tot ist.