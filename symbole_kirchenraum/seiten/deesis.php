titel:Deesis
stichworte:Deesis, Gericht, Gebet, kath
bild:weltgericht.jpg

Darstellung von Jesus, der zu Gericht sitzt. Im zur Seite finden sich seine Mutter Maria und Johannes der T�ufer, die meist f�rbittend ihre H�nde erheben. Von dem griechischen Wort f�r Gebet, F�rbitte leitet sich der Name dieser Bildkomposition her, die in der Ikonenmalerei entwickelt und im Mittelalter vom Westen �bernommen wurde.