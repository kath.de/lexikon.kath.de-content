titel:Brot und Wein
stichworte:Brot und Wein, Jesus, Vereinigung, kath
bild:bild.jpg

Sie sind zentrale Zeichen des christlichen Gottesdienstes. Jesus selbst hat im Mahl, das er am Vorabend seines Todes mit seinen J�ngern gehalten hat, das Brot und den Wein als Zeichen seiner Gegenwart bestimmt. Wein ist insbesondere ein Zeichen des Himmels im Unterschied zum Wasser, das in seinen verflie�enden Wellen die Verg�nglichkeit symbolisiert.
Da Brot und Wein auf den Verzehr hin gereicht werden, kommt es zur Vereinigung zwischen diesen Gaben und dem Essenden und Trinkenden: "Ich bin das lebendige Brot, das vom Himmel herabgekommen ist. Wer von diesem Brot i�t, wird in Ewigkeit leben."
Dieses Brot ist durch Zermahlen der K�rner, der Wein aus dem Keltern der Trauben, hervorgegangen. Deshalb best�tigen Brot und Wein den Satz, der die Aussage fortsetzt (Joh 6,51): "Das Brot, das ich geben werde, ist mein Fleisch f�r das Leben der Welt."
Wie der Geber in seiner Gabe pr�sent ist, gibt es auch keine Trennung zwischen Jesus und den Gaben, die er nicht nur als Geschenke bezeichnet, sondern als seinen Leib und sein Blut.
Die Darstellung der "wunderbaren Brotvermehrung" findet sich in vielen Malereien und Glasfenstern.