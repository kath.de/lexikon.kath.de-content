<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<head profile="http://dublincore.org/documents/dcq-html/">

<link rel="schema.DC" href="http://purl.org/dc/elements/1.1/">
<link rel="schema.DCTERMS" href="http://purl.org/dc/terms/">
<link href="caritas_mannheim.css" rel="stylesheet" type="text/css">

<title>DHEAS Vorstufe der Geschlechtshormone :: Gesundheit &amp; Pr&auml;vention ::</title>
<meta name="robots" content="index,follow,all" />
<meta name="description" content="DHEAS">
<meta name="author" content="Neurolab.eu">
<meta name="keywords" content="DHEAS">


<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="DC.title" content="DHEAS">

<meta name="DC.creator" content="DHEAS">
<meta name="DC.subject" content="DHEAS">
<meta name="DC.description" content="DHEAS">
<meta name="DC.publisher" content="Neurolab.eu">
<meta name="DC.contributor" content="Neurolab.eu">
<meta name="DC.date" content="2007-05-15T08:49:37+02:00" scheme="DCTERMS.W3CDTF">
<meta name="DC.type" content="Text" scheme="DCTERMS.DCMIType">
<meta name="DC.format" content="text/html" scheme="DCTERMS.IMT">
<meta name="DC.identifier"
      content="http://www.kath.de/lexikon/praevention_gesundheit/dheas.php"
      scheme="DCTERMS.URI">
<meta name="DC.source"
      content="http://www.w3.org/TR/html401/struct/global.html#h-7.4.4"
      scheme="DCTERMS.URI">
<meta name="DC.language" content="de" scheme="DCTERMS.RFC3066">
<meta name="DC.relation" content="http://dublincore.org/" scheme="DCTERMS.URI">
<meta name="DC.coverage" content="Munich" scheme="DCTERMS.TGN">
<meta name="DC.rights" content="Alle Rechte liegen beim Autor">
</head>

<body background="images-gesundheit-praevention/neurolab-background-org.jpg" link="#ED5105" vlink="#666666" alink="#FF0000">
<table width="900" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="6">
      <tr>
        <td width="216" align="left" valign="top" height="3">
          <table width="216" border="0" cellpadding="0" cellspacing="0">
            <tr valign="top" align="left">
              <td width="8"><img src="boxtopleftcorner.jpg" width="8" height="8" alt=""></td>
              <td width="200" background="boxtop.jpg"><img src="boxtop.jpg" alt="" width="8" height="8"></td>
              <td width="8"><img src="boxtoprightcorner.jpg" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="boxtopleft.jpg"><img src="boxtopleft.jpg" width="8" height="8" alt=""></td>
              <td bgcolor="f7e39f"><p><b><font color="#333333" face="Arial, Helvetica, sans-serif">Gesundheit
                      &amp; Pr&auml;vention </font></b></p>
              </td>
              <td background="boxtopright.jpg"><img src="boxtopright.jpg" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="boxdividerleft.jpg" width="8" height="13" alt=""></td>
              <td background="boxdivider.jpg"><img src="boxdivider.jpg" alt="" width="8" height="13"></td>
              <td><img src="boxdividerright.jpg" width="8" height="13" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
              <td bgcolor="#FFFFFF">
                <?php include("logo.html"); ?>
              </td>
              <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
              <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
              <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
            </tr>
          </table>
          <br>
          <table width="216" border="0" cellpadding="0" cellspacing="0">
            <tr valign="top" align="left">
              <td width="8"><img src="boxtopleftcorner.jpg" width="8" height="8" alt=""></td>
              <td width="200" background="boxtop.jpg"><img src="boxtop.jpg" alt="" width="8" height="8"></td>
              <td width="8"><img src="boxtoprightcorner.jpg" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="boxtopleft.jpg"><img src="boxtopleft.jpg" width="8" height="8" alt=""></td>
              <td bgcolor="f7e39f"><font color="#333333" face="Arial, Helvetica, sans-serif"><strong>Inhaltsverzeichnis </strong></font></td>
              <td background="boxtopright.jpg"><img src="boxtopright.jpg" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="boxdividerleft.jpg" width="8" height="13" alt=""></td>
              <td background="boxdivider.jpg"><img src="boxdivider.jpg" alt="" width="8" height="13"></td>
              <td><img src="boxdividerright.jpg" width="8" height="13" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
              <td bgcolor="#FFFFFF" class="V10">
                <?php include("az.html"); ?>
              </td>
              <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
              <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
              <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
            </tr>
          </table>
        </td>
        <td width="530" rowspan="2" valign="top">          <table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr valign="top" align="left">
            <td width="8"><img src="boxtopleftcorner.jpg" width="8" height="8" alt=""></td>
            <td background="boxtop.jpg"><img src="boxtop.jpg" alt="" width="8" height="8"></td>
            <td width="8"><img src="boxtoprightcorner.jpg" width="8" height="8" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td background="boxtopleft.jpg" width="8"><img src="boxtopleft.jpg" width="8" height="8" alt=""></td>
            <td bgcolor="f7e39f">
              <h1><font color="#333333" face="Arial, Helvetica, sans-serif">DHEAS</font></h1>
            </td>
            <td background="boxtopright.jpg" width="8"><img src="boxtopright.jpg" width="8" height="8" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td width="8"><img src="boxdividerleft.jpg" width="8" height="13" alt=""></td>
            <td background="boxdivider.jpg"><img src="boxdivider.jpg" alt="" width="8" height="13"></td>
            <td width="8"><img src="boxdividerright.jpg" width="8" height="13" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td width="8" height="905" background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
            <td bgcolor="#FFFFFF" class="L12">
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="79%" class="&uuml;berschrift">Vorl&auml;ufer der
                    Geschlechtshormone</td>
                  <td valign=middle width="21%"><div align="right"> <span class="link"></span></div>
                  </td>
                </tr>
              </table>
              <p class="text">    <img SRC="images-gesundheit-praevention/dheas-neurolab.jpg" alt="DHEAS und Geschlechtshormone" name="DHEAS und Geschlechtshormone" hspace="10" vspace="10" align="right" style="border: double 4px #009999"><strong>DHEAS</strong>                ist ein bedeutender Vorl&auml;ufer
                der weiblichen und m&auml;nnlichen
                Geschlechtshormone. Auff&auml;llig ist die sinkende Produktion
                von DHEAS mit fortschreitendem Alter und ein <strong>Mangel </strong>an dieser
                Hormon-Vorstufe bei verschiedenen Erkrankungen. DHEA ist die
                Abk&uuml;rzung f&uuml;r Dehydroepiandrosteron.
                <br>
                <br>
                Diese Substanz wird zum gr&ouml;&szlig;ten Teil in der <strong>Nebennierenrinde</strong>                von Menschen und S&auml;ugetieren gebildet. Die Produktion wird
                wie bei dem Stress-Hormon Cortisol ma&szlig;geblich durch ACTH
                (Adrenocorticotropes Hormon) gesteuert, das wiederum aus dem
                vorderen Teil der Hirnanhangdr&uuml;se (Hypophyse) ausgesch&uuml;ttet
                wird. Nach einer Sulfatierung liegt DHEAS (Dehydroepiandrosteron
                - Sulfat) vor, das als Vorstufe der weiblichen <strong>(&Ouml;strogene)</strong>                und m&auml;nnlichen <strong>(Androgene)</strong> Geschlechtshormone angesehen
                werden kann, aber auch selbst die Wirkung eines Hormons aufweist.
                <br>
                <br>
                W&auml;hrend die Cortisol -Konzentration im Blut und im Speichel
                eine starke Abh&auml;ngigkeit von der <strong>Tageszeit</strong> besitzt, ist
                die Konzentration von DHEAS weniger von der Tageszeit als vielmehr
                von dem<strong> Alter </strong>der untersuchten Person abh&auml;ngig. Reihenuntersuchungen
                haben ergeben, dass die maximale DHEAS &#8211; Konzentration
                bei Erwachsenen bis etwa zum 30. Lebensjahr vorliegt, um dann
                stetig abzufallen. Mit etwa 70 Lebensjahren betr&auml;gt die<strong> DHEAS &#8211; Konzentration</strong> nur noch 90 Prozent der anf&auml;nglichen
                Maximalkonzentration. <br>
                <br>
                Diese Durchschnittswerte k&ouml;nnen individuell
                sehr unterschiedlich aussehen.DHEAS f&ouml;rdert den <strong>Muskelaufbau</strong>                und greift in den <strong>Fettstoffwechsel</strong> ein. Dar&uuml;ber hinaus wirkt
                DHEAS Entz&uuml;ndungen entgegen,
                im Gegensatz zu Cortisol ist es aber f&uuml;r das Immunsystem
                aktivierend. Weiterhin wirkt es auf den Menschen motivierend,
                hat antidepressive Eigenschaften und steigert die Wahrnehmungsf&auml;higkeit.
                Entsprechend der Wirkungen von DHEAS k&ouml;nnen Kr&auml;fteverfall,
                M&uuml;digkeit, Antriebsschw&auml;che, Muskelabbau, Gewichtszunahme
                (Fettanteil), Libidoverlust, Einbu&szlig;en in der geistigen
                Leistungsf&auml;higkeit, Schlafst&ouml;rungen und Depressionen
                mit einer abfallenden DHEAS &#8211; Konzentration in Verbindung
                gebracht werden.<br>
                <br>
                Text: Oliver Schonschek, Diplom-Physiker.<br>
                  <span class="text">                        </span><span class="text"><br>
                    </span><span class="&uuml;berschrift"><img src="images-gesundheit-praevention/caritas_mannheim_iconpunkt.gif" width="12" height="12"> WEr
                  ber&auml;t:</span><span class="text"> </span></p>
              <p><span class="text">&Auml;rzte im<a href="http://www.neurolab.eu/index.php?option=com_content&view=article&id=72&Itemid=148&#9001;=de" target="_blank"> Partnernetzwerk
                  von Neurolab.</a></span><br>
                  <font face="Arial, Helvetica, sans-serif">F&uuml;r weitere
                  Informationen <a href="http://www.neurolab.eu/index.php?option=com_content&view=article&id=106&Itemid=104&#9001;=de" target="_blank">bitte hier klicken </a></font> </p>              
                  <p align="right"><font size="2"><span class="text"><b><br>
                  </b></span><b><br>
                  </b><font size="4"><a href="http://www.neurolab.eu" target="_blank"><img src="neurolab_logo-1.jpg" width="120" height="77" border="0"></a><br>&copy;                  <a href="http://www.neurolab.eu" target="_blank">www.neurolab.eu</a></font></font></p>
              <p align="right"><font size="2" face="Arial, Helvetica, sans-serif">Bild: &copy; istockphoto.com</font><br>
              </p></td>
            <td background="boxright.gif" width="8"><img src="boxright.gif" width="8" height="8" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td width="8"><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
            <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
            <td width="8"><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
          </tr>
        </table></td>
        <td width="120" rowspan="2" valign="top">
		
	<script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
/* Gesundheitsprävention */
google_ad_slot = "0595101902";
google_ad_width = 120;
google_ad_height = 600;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>


</td>
      </tr>
      <tr>
        <td width="216" height="1374" align="left" valign="top">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>



</body>
</html>
