<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<head profile="http://dublincore.org/documents/dcq-html/">

<link rel="schema.DC" href="http://purl.org/dc/elements/1.1/">
<link rel="schema.DCTERMS" href="http://purl.org/dc/terms/">
<link href="caritas_mannheim.css" rel="stylesheet" type="text/css">

<title>PEA, Depressionen und Schokolade :: Gesundheit &amp; Pr&auml;vention ::</title>
<meta name="robots" content="index,follow,all" />
<meta name="description" content="PEA">
<meta name="author" content="Neurolab.eu">
<meta name="keywords" content="PEA">


<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="DC.title" content="PEA">

<meta name="DC.creator" content="PEA">
<meta name="DC.subject" content="PEA">
<meta name="DC.description" content="PEA">
<meta name="DC.publisher" content="Neurolab.eu">
<meta name="DC.contributor" content="Neurolab.eu">
<meta name="DC.date" content="2007-05-15T08:49:37+02:00" scheme="DCTERMS.W3CDTF">
<meta name="DC.type" content="Text" scheme="DCTERMS.DCMIType">
<meta name="DC.format" content="text/html" scheme="DCTERMS.IMT">
<meta name="DC.identifier"
      content="http://www.kath.de/lexikon/praevention_gesundheit/pea.php"
      scheme="DCTERMS.URI">
<meta name="DC.source"
      content="http://www.w3.org/TR/html401/struct/global.html#h-7.4.4"
      scheme="DCTERMS.URI">
<meta name="DC.language" content="de" scheme="DCTERMS.RFC3066">
<meta name="DC.relation" content="http://dublincore.org/" scheme="DCTERMS.URI">
<meta name="DC.coverage" content="Munich" scheme="DCTERMS.TGN">
<meta name="DC.rights" content="Alle Rechte liegen beim Autor">
</head>

<body background="images-gesundheit-praevention/neurolab-background-org.jpg" link="#ED5105" vlink="#666666" alink="#FF0000">
<table width="900" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="6">
      <tr>
        <td width="216" align="left" valign="top" height="3">
          <table width="216" border="0" cellpadding="0" cellspacing="0">
            <tr valign="top" align="left">
              <td width="8"><img src="boxtopleftcorner.jpg" width="8" height="8" alt=""></td>
              <td width="200" background="boxtop.jpg"><img src="boxtop.jpg" alt="" width="8" height="8"></td>
              <td width="8"><img src="boxtoprightcorner.jpg" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="boxtopleft.jpg"><img src="boxtopleft.jpg" width="8" height="8" alt=""></td>
              <td bgcolor="f7e39f"><p><b><font color="#333333" face="Arial, Helvetica, sans-serif">Gesundheit
                      &amp; Pr&auml;vention </font></b></p>
              </td>
              <td background="boxtopright.jpg"><img src="boxtopright.jpg" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="boxdividerleft.jpg" width="8" height="13" alt=""></td>
              <td background="boxdivider.jpg"><img src="boxdivider.jpg" alt="" width="8" height="13"></td>
              <td><img src="boxdividerright.jpg" width="8" height="13" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
              <td bgcolor="#FFFFFF">
                <?php include("logo.html"); ?>
              </td>
              <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
              <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
              <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
            </tr>
          </table>
          <br>
          <table width="216" border="0" cellpadding="0" cellspacing="0">
            <tr valign="top" align="left">
              <td width="8"><img src="boxtopleftcorner.jpg" width="8" height="8" alt=""></td>
              <td width="200" background="boxtop.jpg"><img src="boxtop.jpg" alt="" width="8" height="8"></td>
              <td width="8"><img src="boxtoprightcorner.jpg" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="boxtopleft.jpg"><img src="boxtopleft.jpg" width="8" height="8" alt=""></td>
              <td bgcolor="f7e39f"><font color="#333333" face="Arial, Helvetica, sans-serif"><strong>Inhaltsverzeichnis </strong></font></td>
              <td background="boxtopright.jpg"><img src="boxtopright.jpg" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="boxdividerleft.jpg" width="8" height="13" alt=""></td>
              <td background="boxdivider.jpg"><img src="boxdivider.jpg" alt="" width="8" height="13"></td>
              <td><img src="boxdividerright.jpg" width="8" height="13" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
              <td bgcolor="#FFFFFF" class="V10">
                <?php include("az.html"); ?>
              </td>
              <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
              <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
              <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
            </tr>
          </table>
        </td>
        <td width="530" rowspan="2" valign="top">          <table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr valign="top" align="left">
            <td width="8"><img src="boxtopleftcorner.jpg" width="8" height="8" alt=""></td>
            <td background="boxtop.jpg"><img src="boxtop.jpg" alt="" width="8" height="8"></td>
            <td width="8"><img src="boxtoprightcorner.jpg" width="8" height="8" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td background="boxtopleft.jpg" width="8"><img src="boxtopleft.jpg" width="8" height="8" alt=""></td>
            <td bgcolor="f7e39f">
              <h1><font color="#333333" face="Arial, Helvetica, sans-serif">PEA,
                  Despressionen und Schokolade</font></h1>
            </td>
            <td background="boxtopright.jpg" width="8"><img src="boxtopright.jpg" width="8" height="8" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td width="8"><img src="boxdividerleft.jpg" width="8" height="13" alt=""></td>
            <td background="boxdivider.jpg"><img src="boxdivider.jpg" alt="" width="8" height="13"></td>
            <td width="8"><img src="boxdividerright.jpg" width="8" height="13" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td width="8" height="905" background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
            <td bgcolor="#FFFFFF" class="L12">
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="79%" class="&uuml;berschrift">Warum Schokolade scheinbar
                    gl&uuml;cklich macht</td>
                  <td valign=middle width="21%"><div align="right"> <span class="link"></span></div>
                  </td>
                </tr>
              </table>
              <p class="text">    <img SRC="images-gesundheit-praevention/schokolade-neurolab.jpg" alt="L-Theanin und Gr�ntee" name="L-Theanin und Gr�ntee" hspace="10" vspace="10" align="right" style="border: double 4px #009999"><strong>Beta-Phenyletylamin,</strong>                  kurz PEA genannt, ist einer der Hauptbestandteile von Schokolade
                  und spielt bei der Aktivierung von bestimmten
                  neuronalen Botenstoffe (Neurotransmitter) wie Serotonin, Dopamin
                  und Noradrenalin eine wichtige Rolle. PEA wirkt dadurch <strong>motivierend
                  und stimmungsaufhellend</strong>. Wegen seiner chemischen Verwandtschaft
                  zu bestimmten Drogen wird PEA manchmal auch als k&ouml;rpereigene
                  Droge bezeichnet. Bei k&ouml;rperlicher Bet&auml;tigung kann
                  ein deutlicher Anstieg der PEA - Konzentration nachgewiesen werden.
                  PEA kann jedoch auch durch die Nahrung zugef&uuml;hrt werden.</p>
              <p class="text">Aus
                    dem Zusammenspiel mit den zuvor genannten Neurotransmittern
                  folgen unter anderem die Wirkungen von PEA als <strong>Antidepressivum,</strong>                  v.a. bei der sog. Major-Depression. Gleichzeitig ist PEA aktivit&auml;tssteigernd
                  und f&ouml;rdert die mentale Energie und die Aufmerksamkeit.
                  Neben dieser anregenden Wirkung auf das zentrale Nervensystem
                  kann auch ein Anstieg des <strong>Blutzuckerspiegels</strong> durch PEA nachgewiesen
                  werden.</p>
              <p class="text">Ein niedriger PEA - Spiegel kann mit Depressionen,
                Psychosen, aber auch mit <strong>chronischer M&uuml;digkeit</strong> (CFS)
                und dem Aufmerksamkeits-Defizit-Hyperaktivit&auml;ts-Syndrom
                  (ADS/ADHS) in Verbindung gebracht werden. Einen erh&ouml;hten
                  PEA - Wert kann man hingegen bei Stress, Migr&auml;ne, Angstsyndrom,
                  aber auch bei Schlaflosigkeit und Schizophrenie nachweisen.<br>
                    <span class="text">                        </span><span class="text"><br>
                    </span><span class="&uuml;berschrift"><img src="images-gesundheit-praevention/caritas_mannheim_iconpunkt.gif" width="12" height="12"> WEr
                  ber&auml;t:</span><span class="text"> </span></p>
              <p><span class="text">&Auml;rzte im<a href="http://www.neurolab.eu/index.php?option=com_content&view=article&id=72&Itemid=148&#9001;=de" target="_blank"> Partnernetzwerk
                  von Neurolab.</a></span><br>
                  <font face="Arial, Helvetica, sans-serif">F&uuml;r weitere
                  Informationen<a href="http://www.neurolab.eu/index.php?option=com_content&view=article&id=65&Itemid=99&#9001;=de" target="_blank"> bitte hier klicken </a></font> </p>              
                  <p align="right"><font size="2"><span class="text"><b><br>
                  </b></span><b><br>
                  </b><font size="4"><a href="http://www.neurolab.eu" target="_blank"><img src="neurolab_logo-1.jpg" width="120" height="77" border="0"></a><br>&copy;                  <a href="http://www.neurolab.eu" target="_blank">www.neurolab.eu</a></font></font></p>
              <p align="right"><font size="2" face="Arial, Helvetica, sans-serif">Bild: &copy; fotolia.com</font><br>
              </p></td>
            <td background="boxright.gif" width="8"><img src="boxright.gif" width="8" height="8" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td width="8"><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
            <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
            <td width="8"><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
          </tr>
        </table></td>
        <td width="120" rowspan="2" valign="top">
		
	<script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
/* Gesundheitspr�vention */
google_ad_slot = "0595101902";
google_ad_width = 120;
google_ad_height = 600;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>


</td>
      </tr>
      <tr>
        <td width="216" height="1374" align="left" valign="top">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>



</body>
</html>
