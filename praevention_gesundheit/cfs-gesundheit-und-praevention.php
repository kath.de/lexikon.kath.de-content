<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<head profile="http://dublincore.org/documents/dcq-html/">

<link rel="schema.DC" href="http://purl.org/dc/elements/1.1/">
<link rel="schema.DCTERMS" href="http://purl.org/dc/terms/">
<link href="caritas_mannheim.css" rel="stylesheet" type="text/css">

<title>CFS :: Gesundheit &amp; Pr&auml;vention ::</title>
<meta name="robots" content="index,follow,all" />
<meta name="description" content="CFS">
<meta name="author" content="Neurolab.eu">
<meta name="keywords" content="CFS">


<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="DC.title" content="CFS">

<meta name="DC.creator" content="CFS">
<meta name="DC.subject" content="CFS">
<meta name="DC.description" content="CFS">
<meta name="DC.publisher" content="Neurolab">
<meta name="DC.contributor" content="CFS">
<meta name="DC.date" content="2007-05-15T08:49:37+02:00" scheme="DCTERMS.W3CDTF">
<meta name="DC.type" content="Text" scheme="DCTERMS.DCMIType">
<meta name="DC.format" content="text/html" scheme="DCTERMS.IMT">
<meta name="DC.identifier"
      content="http://www.kath.de/lexikon/praevention_gesundheit/cfs-gesundheit-und-praevention.php"
      scheme="DCTERMS.URI">
<meta name="DC.source"
      content="http://www.w3.org/TR/html401/struct/global.html#h-7.4.4"
      scheme="DCTERMS.URI">
<meta name="DC.language" content="de" scheme="DCTERMS.RFC3066">
<meta name="DC.relation" content="http://dublincore.org/" scheme="DCTERMS.URI">
<meta name="DC.coverage" content="Munich" scheme="DCTERMS.TGN">
<meta name="DC.rights" content="Alle Rechte liegen beim Autor">
</head>

<body background="images-gesundheit-praevention/neurolab-background-org.jpg" link="#ED5105" vlink="#666666" alink="#FF0000">
<table width="900" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="6">
      <tr>
        <td width="216" align="left" valign="top" height="3">
          <table width="216" border="0" cellpadding="0" cellspacing="0">
            <tr valign="top" align="left">
              <td width="8"><img src="boxtopleftcorner.jpg" width="8" height="8" alt=""></td>
              <td width="200" background="boxtop.jpg"><img src="boxtop.jpg" alt="" width="8" height="8"></td>
              <td width="8"><img src="boxtoprightcorner.jpg" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="boxtopleft.jpg"><img src="boxtopleft.jpg" width="8" height="8" alt=""></td>
              <td bgcolor="f7e39f"><p><b><font color="#333333" face="Arial, Helvetica, sans-serif">Gesundheit
                      &amp; Pr&auml;vention </font></b></p>
              </td>
              <td background="boxtopright.jpg"><img src="boxtopright.jpg" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="boxdividerleft.jpg" width="8" height="13" alt=""></td>
              <td background="boxdivider.jpg"><img src="boxdivider.jpg" alt="" width="8" height="13"></td>
              <td><img src="boxdividerright.jpg" width="8" height="13" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
              <td bgcolor="#FFFFFF">
                <?php include("logo.html"); ?>
              </td>
              <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
              <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
              <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
            </tr>
          </table>
          <br>
          <table width="216" border="0" cellpadding="0" cellspacing="0">
            <tr valign="top" align="left">
              <td width="8"><img src="boxtopleftcorner.jpg" width="8" height="8" alt=""></td>
              <td width="200" background="boxtop.jpg"><img src="boxtop.jpg" alt="" width="8" height="8"></td>
              <td width="8"><img src="boxtoprightcorner.jpg" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="boxtopleft.jpg"><img src="boxtopleft.jpg" width="8" height="8" alt=""></td>
              <td bgcolor="f7e39f"><font color="#333333" face="Arial, Helvetica, sans-serif"><strong>Inhaltsverzeichnis </strong></font></td>
              <td background="boxtopright.jpg"><img src="boxtopright.jpg" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="boxdividerleft.jpg" width="8" height="13" alt=""></td>
              <td background="boxdivider.jpg"><img src="boxdivider.jpg" alt="" width="8" height="13"></td>
              <td><img src="boxdividerright.jpg" width="8" height="13" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
              <td bgcolor="#FFFFFF" class="V10">
                <?php include("az.html"); ?>
              </td>
              <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
              <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
              <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
            </tr>
          </table>
        </td>
        <td width="530" rowspan="2" valign="top">          <table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr valign="top" align="left">
            <td width="8"><img src="boxtopleftcorner.jpg" width="8" height="8" alt=""></td>
            <td background="boxtop.jpg"><img src="boxtop.jpg" alt="" width="8" height="8"></td>
            <td width="8"><img src="boxtoprightcorner.jpg" width="8" height="8" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td background="boxtopleft.jpg" width="8"><img src="boxtopleft.jpg" width="8" height="8" alt=""></td>
            <td bgcolor="f7e39f">
              <h1><font color="#333333" face="Arial, Helvetica, sans-serif">CFS</font></h1>
            </td>
            <td background="boxtopright.jpg" width="8"><img src="boxtopright.jpg" width="8" height="8" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td width="8"><img src="boxdividerleft.jpg" width="8" height="13" alt=""></td>
            <td background="boxdivider.jpg"><img src="boxdivider.jpg" alt="" width="8" height="13"></td>
            <td width="8"><img src="boxdividerright.jpg" width="8" height="13" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td width="8" height="905" background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
            <td bgcolor="#FFFFFF" class="L12">              <p><span class="text">    <img SRC="images-gesundheit-praevention/cfs.jpg" alt="CFS" name="CFS" hspace="10" vspace="10" align="right" style="border: double 4px #009999">    </span><span class="&uuml;berschrift">CFS &#8211; Mehr
                als nur eine langwierige Ersch&ouml;pfung</span><br>
                <span class="text"><br>
                Im Gegensatz zur Fatigue ist CFS (Chronic Fatigue Syndrome) eine
              eigenst&auml;ndige Erkrankung ohne erkennbare Ursache. Auch wenn
              sich der Name dieser Erkrankung mit <strong>Chronischem Ersch&ouml;pfungs-syndrom </strong>&uuml;bersetzen
              l&auml;sst, bedeutet sie f&uuml;r die Betroffenen weit mehr als
              eine langanhaltende Erm&uuml;dung. Die Symptome und Beschwerden
              sind nicht nur zahlreich, sondern auch sehr belastend, h&auml;ufig
              mit Folgen f&uuml;r das soziale und berufliche Umfeld der Erkrankten. </span></p>
              <p class="text"><span class="&uuml;berschrift">Die Symptome eines CFS</span><br>
                <br>
  W&auml;hrend die massiven Ersch&ouml;pfungszust&auml;nde (Fatigue)
                bei Krebspatienten und chronisch Kranken noch f&uuml;r die meisten
                Angeh&ouml;rigen und Kollegen der Betroffenen nachvollziehbar
                erscheinen, sto&szlig;en Menschen mit CFS oftmals auf Unverst&auml;ndnis,
                teilweise auch bei &Auml;rzten. Die enorme M&uuml;digkeit und
                Ersch&ouml;pfung, die auch nach einer ausreichenden Schlafphase
                nicht abklingen und oft &uuml;ber Monate anhalten, scheinen keine
                organische Ursache zu haben. So kommt es, dass manche CFS-Patienten
                die Empfehlung erhalten, sich eher rein psychotherapeutisch behandeln
                zu lassen. In verschiedenen F&auml;llen erfahren diese Patienten
                sogar Misstrauen im Berufsleben durch den Verdacht, dass sie
                sich diese Beschwerden bewusst oder unbewusst einbilden, ihre
                Krankheit also<strong> nur vorspielen</strong>.</p>
              <p class="text">Die Folge ist oft, dass in besonderen F&auml;llen CFS-Erkrankte
                versuchen, ihrem Umfeld gegen&uuml;ber die Beschwerden zu verbergen,
                da sie sich f&uuml;r ihren unerkl&auml;rlichen Zustand sch&auml;men                oder im Berufsleben nicht als <strong>&#8222;eingebildete Kranke&#8220;</strong> erscheinen
                wollen. Dabei sind die Beschwerden eines CFS nicht nur vielschichtig,
                sondern auch massiv. Dazu geh&ouml;ren neben der zentralen Ersch&ouml;pfung
                auch Muskelschmerzen, Halsschmerzen, Kopfschmerzen, Konzentrations-
                und Ged&auml;chtnisst&ouml;rungen, Depressionen, Ohrger&auml;usche,
                Schwindel, Fieber und ein allgemeiner Schw&auml;chezustand. Die
                N&auml;he zu verschiedenen Grippesymptomen scheint nicht zuf&auml;llig
                zu sein, denn es sind verschiedene F&auml;lle bekannt, in denen
                sich Patienten von einer schweren Infektion oder einer Grippe
                nicht mehr vollst&auml;ndig erholten, sondern im Gegenteil die
                Beschwerden eines CFS entwickelten.</p>
              <p class="text">Um ein CFS von anderen Ersch&ouml;pfungen zu unterscheiden,
                geht man von einer massiven Leistungsminderung aus, die mindestens &uuml;ber
                sechs Monate anhalten muss, bevor die Diagnose CFS getroffen
                werden kann. In den meisten F&auml;llen tritt das Chronische
                Ersch&ouml;pfungssyndrom <strong>unvermittelt und schlagartig</strong> auf und
                h&auml;lt f&uuml;r viele Jahre an. Das Abklingen der Beschwerden
                kann ebenso pl&ouml;tzlich auftreten wie der Beginn der Erkrankung.
                Auch wenn die Forschung von keiner lebensbedrohlichen Erkrankung
                im Fall von CFS ausgeht, obwohl ein entsprechender Todesfall
                aus Gro&szlig;britannien berichtet wird, ist ein CFS eine sehr
                ernst zu nehmende Gesundheitsst&ouml;rung, die leider immer noch,
                besonders im deutschsprachigen Raum, zu wenig Beachtung findet
                - zum Leidwesen der Patienten. Insbesondere in den Vereinigten
                Staaten wird CFS als eine der Krankheiten eingestuft, deren Erforschung
                oberste Priorit&auml;t hat. Entsprechend hohe Forschungsgelder
                f&uuml;r CFS-Programme stehen in den USA zur Verf&uuml;gung. &Uuml;bertr&auml;gt
                man die US-amerikanischen statistischen Werte &uuml;ber die H&auml;ufigkeit
                von CFS, so gibt es in Deutschland etwa 300.000 CFS-Erkrankungen,
                wahrscheinlich jedoch wesentlich mehr.<br>
                <span class="&uuml;berschrift"><br>
                Welche Ursachen kann ein CFS haben?</span><br>
                <br>
                Gerade das scheinbare Fehlen einer Ursache macht ein CFS f&uuml;r
                die Betroffenen noch schlimmer, die zum Teil zahlreiche &auml;rztliche
                Disziplinen konsultieren, ohne jedoch eine befriedigende Antwort
                f&uuml;r den Grund ihres Leidens zu bekommen. Die aktuelle Forschung
                kann noch keine eindeutige Antwort auf die Ursachenfrage geben. <br>
                <br>
                Aktuell geht man davon aus, dass bis zu 80% der F&auml;lle postinfekt&ouml;se
                Fatigueformen sind, f&uuml;r die in erster Linie Viren der Herpesgruppe
                als Ausl&ouml;ser in Frage kommen (Epstein-Barr-, Cytomegalie-
                Humanes Herpesvirus. Sie beginnen akut, oft mit grippaler Symptomatik,
                w&auml;hrend bei den restlichen 20% der Verlauf eher schleichend
                ist mit allm&auml;hlicher Zunahme der Symptomatik. Der Verlust
                mentaler, psychischer und physischer Leistungsf&auml;higkeit
                ist Ausdruck der zentralen M&uuml;digkeit, die Schlafst&ouml;rungen,
                Stimmungsschwankungen, Depressionen und unterschiedlichsten Befindlichkeitsst&ouml;rungen
                sind ihre Begleiterscheinungen. H&auml;ufig ist CFS auch mit
                Schmerzsyndromen (Fibromyalgie, Migr&auml;ne, Morgensteifigkeit),
                sensorischen &Uuml;berempfindlichkeitsreaktionen (MCS, ESM, Hyperosmie,
                Hyperakusis, Tinnitus, etc.) und funktionellen Magen-Darm-St&ouml;rungen
                (Nahrungsmittelunvertr&auml;glichkeiten, Diarrhoe, Reizdarmsyndrom)
                assoziiert.<br>
                Weitere Ausl&ouml;ser oder Cofaktoren f&uuml;r ein CFS sind Umweltgifte,
                Erkrankungen des Immunsystems, langanhaltende Stressbelastungen,
                Hormonst&ouml;rungen und starke psychische Belastungen. </p>
              <p class="text"><span class="&uuml;berschrift">CFS-Modell</span><br>
                <br>
                Die pathologische Entwicklung des CFS ist nach aktuellem Wissensstand
                  am ehesten im folgenden Modell zusammenzufassen: <br>
                  Bei Personen mit entsprechender genetischer Anlage innerhalb
                der Neuroregulation, dem Stresshormonsystem und Entz&uuml;ndungsmechanismen
                f&uuml;hrt &uuml;berm&auml;&szlig;ige Einwirkung von <strong>Stressoren </strong>unterschiedlichster Art (anhaltender physischer, psychischer,
                mentaler Stress, Schlafmangel, Infektionen, Chemikalien, Umweltstressoren,
                metabolischer Stress, Fehlern&auml;hrung, Rauchen, etc) zu anhaltenden
                Gesundheitsst&ouml;rungen, bei denen die zentrale M&uuml;digkeit
                an erster Stelle steht. Das komplexe Zusammenwirken von oxidativem
                Stress und Verlust der neurohormonellen Stabilit&auml;t bzw.
                Balance bewirkt vor allem eine anhaltende Schw&auml;che der hormonellen
                und neuralen (Neurotransmitter Serotonin und Noradrenalin) Stressachsen.
                Entz&uuml;ndungsfaktoren, Serotonindefizit und mangelhafte Energieproduktion
                sind die Ausl&ouml;ser des zentralen Fatiguesyndroms. </p>
              <p class="text">CRH (Corticotropin Releasing Hormone) wird
                  in Folge der dauerhaften Belastungen in vermehrter Form durch
                  den Hypothalamus ausgesch&uuml;ttet.
                M&ouml;glicherweise werden dadurch die Hypophyse und die Nebennierenrinde,
                die durch CRH beeinflusst werden, mit der Zeit f&uuml;r diesen
                Botenstoff unempfindlich. Als Folge findet sich bei CFS-Patienten
                ein erniedrigter Morgenwert bei Cortisol, ein gest&ouml;rter
                Cortisol-Tagesverlauf, ein erniedrigter Wert bei ACTH (Adrenocorticotropes
                Hormon) sowie ein Mangel an Noradrenalin, Dopamin, Serotonin
                und PEA. <br>
                  <br>
                  <span class="&uuml;berschrift">Empfohlene Testprofile von NeuroLab</span><br>
                  <br>
                  Im Rahmen seines umfangreichen Neurostress-Testprogramms empfiehlt
                  NeuroLab auch spezielle Testprofile zur Untersuchung von Patienten,
                  bei denen der Verdacht auf ein CFS besteht. Grundlegend ist
                  das Testprofil NEUROSTRESS BASIS, bei dem die wichtigsten Indikatoren
                  f&uuml;r eine auf Neurostress basierende Gesundheitsst&ouml;rung
                  untersucht werden. Dazu geh&ouml;ren der Morgenwert und der
                  Tagesverlauf von Cortisol sowie die Konzentrationen von Serotonin,
                  Noradrenalin, Dopamin und Adrenalin. </p>
              <p class="text"><span class="&uuml;berschrift">Therapieoptionen</span><br>
                <br>
  Die Behandlung des CFS muss der multifaktoriellen Entstehung
                  und Auspr&auml;gung Rechnung tragen. Meist ist die Therapie
                  jedoch nach allen Erfahrungen langwierig und muss h&auml;ufig
                  experimentell angepasst werden. <br>
                Am erfolgreichsten erscheint die Behandlung der akut beginnenden
                Fatigue-Formen nach Infektion durch ein Virostatikum ( z.B. Valganciclovir).<br>
                W&auml;hrend Antidepressiva in vielen F&auml;llen das Problem des Neurotransmittermangels
  und der Stressachsendysfunktion nicht beheben sondern l&auml;ngerfristig eher
  verst&auml;rken, bietet sich eine nat&uuml;rliche Alternative in der Wiederherstellung
  des Neurotransmitterpools durch Zufuhr der jeweiligen Neurotransmitter-Aminos&auml;urevorstufen
  an. Durch die neuronale Balancierung normalisiert sich auch die Funktion der
  Stresshormone Cortisol und DHEA. <br>
  Zur Behandlung der Tagesm&uuml;digkeit werden zunehmend hochdosiertes Coenzym
  Q10 (100 mg) , L-Carnitin oder Acetyl-Carnitin, B-Kompex-Pr&auml;parate und
  zur Verbesserung der antioxidativen , bzw antiinflammatorischen Kapazit&auml;t
  z. B. Vitamin B12 als Hydroxy-Cobalamin und N-Acetylcystein eingesetzt.</p>
              <p class="text"><br>
                Text: <br>
                Oliver Schonschek, Diplom-Physiker<br>
                Dr. Annemarie Neuner <br>
                (c) NeuroLab<br>
              </p>
              <p><span class="text"><br>
                    </span><span class="&uuml;berschrift"><img src="images-gesundheit-praevention/caritas_mannheim_iconpunkt.gif" width="12" height="12"> WEr
                    behandelt:</span><span class="text"> </span></p>
              <p><span class="text">&Auml;rzte im<a href="http://www.neurolab.eu/index.php?option=com_content&view=article&id=72&Itemid=148&#9001;=de" target="_blank"> Partnernetzwerk
                    von Neurolab.</a><br>
                  F&uuml;r weitere Informationen <a href="http://www.neurolab.eu/index.php?option=com_content&view=article&id=43&Itemid=87&#9001;=de" target="_blank">bitte hier klicken </a> </span></p>              
              <p align="right"><font size="2"><span class="text"><b><br>
                  </b></span><b><br>
                  </b><font size="4"><a href="http://www.neurolab.eu" target="_blank"><img src="neurolab_logo-1.jpg" width="120" height="77" border="0"></a><br>&copy;                  <a href="http://www.neurolab.eu" target="_blank">www.neurolab.eu</a></font></font></p>
              </td>
            <td background="boxright.gif" width="8"><img src="boxright.gif" width="8" height="8" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td width="8" height="8"><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
            <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
            <td width="8"><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
          </tr>
        </table></td>
        <td width="120" rowspan="2" valign="top">
		
	<script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
/* Gesundheitsprävention */
google_ad_slot = "0595101902";
google_ad_width = 120;
google_ad_height = 600;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>


</td>
      </tr>
      <tr>
        <td width="216" height="1374" align="left" valign="top">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>



</body>
</html>
