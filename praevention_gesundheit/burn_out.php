<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<head profile="http://dublincore.org/documents/dcq-html/">

<link rel="schema.DC" href="http://purl.org/dc/elements/1.1/">
<link rel="schema.DCTERMS" href="http://purl.org/dc/terms/">
<link href="caritas_mannheim.css" rel="stylesheet" type="text/css">

<title>BurnOut :: Gesundheit &amp; Pr&auml;vention ::</title>
<meta name="robots" content="index,follow,all" />
<meta name="description" content="Burn-Out">
<meta name="author" content="Neurolab.eu">
<meta name="keywords" content="Burn-Out">


<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="DC.title" content="Burn-Out">

<meta name="DC.creator" content="Burn-Out">
<meta name="DC.subject" content="Burn-Out">
<meta name="DC.description" content="Burn-Out">
<meta name="DC.publisher" content="Neurolab.eu">
<meta name="DC.contributor" content="Neurolab.eu">
<meta name="DC.date" content="2007-05-15T08:49:37+02:00" scheme="DCTERMS.W3CDTF">
<meta name="DC.type" content="Text" scheme="DCTERMS.DCMIType">
<meta name="DC.format" content="text/html" scheme="DCTERMS.IMT">
<meta name="DC.identifier"
      content="http://www.kath.de/lexikon/praevention_gesundheit/burn_out.php"
      scheme="DCTERMS.URI">
<meta name="DC.source"
      content="http://www.w3.org/TR/html401/struct/global.html#h-7.4.4"
      scheme="DCTERMS.URI">
<meta name="DC.language" content="de" scheme="DCTERMS.RFC3066">
<meta name="DC.relation" content="http://dublincore.org/" scheme="DCTERMS.URI">
<meta name="DC.coverage" content="Munich" scheme="DCTERMS.TGN">
<meta name="DC.rights" content="Alle Rechte liegen beim Autor">
</head>

<body background="images-gesundheit-praevention/neurolab-background-org.jpg" link="#ED5105" vlink="#666666" alink="#FF0000">
<table width="900" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="6">
      <tr>
        <td width="216" align="left" valign="top" height="3">
          <table width="216" border="0" cellpadding="0" cellspacing="0">
            <tr valign="top" align="left">
              <td width="8"><img src="boxtopleftcorner.jpg" width="8" height="8" alt=""></td>
              <td width="200" background="boxtop.jpg"><img src="boxtop.jpg" alt="" width="8" height="8"></td>
              <td width="8"><img src="boxtoprightcorner.jpg" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="boxtopleft.jpg"><img src="boxtopleft.jpg" width="8" height="8" alt=""></td>
              <td bgcolor="f7e39f"><p><b><font color="#333333" face="Arial, Helvetica, sans-serif">Gesundheit
                      &amp; Pr&auml;vention </font></b></p>
              </td>
              <td background="boxtopright.jpg"><img src="boxtopright.jpg" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="boxdividerleft.jpg" width="8" height="13" alt=""></td>
              <td background="boxdivider.jpg"><img src="boxdivider.jpg" alt="" width="8" height="13"></td>
              <td><img src="boxdividerright.jpg" width="8" height="13" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
              <td bgcolor="#FFFFFF">
                <?php include("logo.html"); ?>
              </td>
              <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
              <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
              <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
            </tr>
          </table>
          <br>
          <table width="216" border="0" cellpadding="0" cellspacing="0">
            <tr valign="top" align="left">
              <td width="8"><img src="boxtopleftcorner.jpg" width="8" height="8" alt=""></td>
              <td width="200" background="boxtop.jpg"><img src="boxtop.jpg" alt="" width="8" height="8"></td>
              <td width="8"><img src="boxtoprightcorner.jpg" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="boxtopleft.jpg"><img src="boxtopleft.jpg" width="8" height="8" alt=""></td>
              <td bgcolor="f7e39f"><font color="#333333" face="Arial, Helvetica, sans-serif"><strong>Inhaltsverzeichnis </strong></font></td>
              <td background="boxtopright.jpg"><img src="boxtopright.jpg" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="boxdividerleft.jpg" width="8" height="13" alt=""></td>
              <td background="boxdivider.jpg"><img src="boxdivider.jpg" alt="" width="8" height="13"></td>
              <td><img src="boxdividerright.jpg" width="8" height="13" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
              <td bgcolor="#FFFFFF" class="V10">
                <?php include("az.html"); ?>
              </td>
              <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
              <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
              <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
            </tr>
          </table>
        </td>
        <td width="530" rowspan="2" valign="top">          <table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr valign="top" align="left">
            <td width="8"><img src="boxtopleftcorner.jpg" width="8" height="8" alt=""></td>
            <td background="boxtop.jpg"><img src="boxtop.jpg" alt="" width="8" height="8"></td>
            <td width="8"><img src="boxtoprightcorner.jpg" width="8" height="8" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td background="boxtopleft.jpg" width="8"><img src="boxtopleft.jpg" width="8" height="8" alt=""></td>
            <td bgcolor="f7e39f">
              <h1><font color="#333333" face="Arial, Helvetica, sans-serif">Burn-Out</font></h1>
            </td>
            <td background="boxtopright.jpg" width="8"><img src="boxtopright.jpg" width="8" height="8" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td width="8"><img src="boxdividerleft.jpg" width="8" height="13" alt=""></td>
            <td background="boxdivider.jpg"><img src="boxdivider.jpg" alt="" width="8" height="13"></td>
            <td width="8"><img src="boxdividerright.jpg" width="8" height="13" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td width="8" height="905" background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
            <td bgcolor="#FFFFFF" class="L12">
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="79%" class="&uuml;berschrift">St&ouml;rung im Hormonhaushalt</td>
                  <td valign=middle width="21%"><div align="right"> <span class="link"></span></div>
                  </td>
                </tr>
              </table>
              <p><span class="text">    <img SRC="images-gesundheit-praevention/burnout-neurolab.jpg" alt="Burnout" name="Burnout" hspace="10" vspace="10" align="right" style="border: double 4px #009999">     <strong>Burn-Out</strong>                  ist ein modisch gebrauchter Begriff, der medizinisch einer
                  tiefgreifenden St&ouml;rung der Produktion von <strong>Stre&szlig;hormonen</strong>,
                  u.a. Cortisol, Adrenalin und von Neurotransmittern entspricht.
                  Burn-out ist die Folge lang andauernder und &uuml;berm&auml;&szlig;iger
                  Stressbelastung.<br>
                  <br>
              </span><span class="text"> Es gibt zwei Schwerpunkte
                  der Ver&auml;nderung. Zum einen bricht
                  die physiologische Regulation des <strong>Hormonhaushaltes</strong> zusammen.
                  Zum anderen entwickelt sich ein andauernder <strong>Entz&uuml;ndungszustand</strong>.
                  Der 24-Stunden-Rhythmus der Hormonproduktion ist au&szlig;er
                  Kraft gesetzt. Die n&auml;chtliche Cortisolproduktion ist blockiert.
                  Dieses Hormon wird in der Nacht gebildet, um f&uuml;r die Aktivit&auml;t
                  des Tages zur Verf&uuml;gung zu stehen. Weiter ist die Bildung
                  von <strong>Melatonin</strong> beeintr&auml;chtigt. Melatonin ist ein Hormon,
                  das den Schlaf reguliert und wein wichtiger Taktgeber f&uuml;r
                  die Hormonzyklen ist. Der Melatoninmangel ist Folge des Serotoninmangels.
                  Letzteres entsteht durch Blockade der Serotoninsynthese, die
                  u.a. durch Entz&uuml;ndungsmediatoren und oxidativen Stress
                  bedingt ist.<br>
                  <br>
                  Zur erh&ouml;hten <strong>Entz&uuml;ndungsneigung </strong>bei Burn-out tr&auml;gt
                  zuerst das erh&ouml;hte Noradrenalin, das unmittelbar Entz&uuml;ndungreaktionen
                  stimuliert, bei. H&auml;lt die Stressbelastung l&auml;nger an
                  und kommt es zu Ausf&auml;llen in der Hormonproduktion, beg&uuml;nstigt
                  der Mangel an Cortisol, das stark entz&uuml;ndungshemmend wirkt,
                  entz&uuml;ndliche Aktivit&auml;ten.<br>
                  <br>
              </span><span class="text"> Der
                  Mangel an Cortisol, an Adrenalin, Noradrenalin, an Neurtransmittern,
                    Melatonin erkl&auml;rt die <strong>Mattigkeit und Antriebslosigkeit</strong> bei
                    Burn-out. In Bezug auf die Bildung von Cortisol ist ungen&uuml;gender
                    und unregelm&auml;&szlig;iger Schlaf auch Ursache f&uuml;r
                  Mattigkeit und Antriebslosigkeit.<br>
                  <br>
              </span><span class="text"> Nicht
                  bei jedem f&uuml;hrt anhaltender Stress zu Burn-out. <strong>Genetische
                      Faktoren</strong>, die die Hormon- bzw. die Neurotransmittersynthese beeinflussen,
                      bzw. die die Wirkung von Hormonen bzw. Neurotransmittern bei
                      den Rezeptoren an den Zellen behindern und die &uuml;berschie&szlig;ende
                      Entz&uuml;ndungsreaktionen beg&uuml;nstigen, kommen hinzu.<br>
                      <br>
                    Diese Vielzahl hormoneller Ausf&auml;lle, fehlender Neurotransmitter
                      sowie gesteigerter Entz&uuml;ndungsreaktionen bewirken ein <strong>vielf&auml;ltiges
                      Spektrum</strong> von Reaktionen beim Burn-out-Syndrom: Antriebslosigkeit,
                      Ersch&ouml;pfungszust&auml;nde, Motivationsverlust, Schw&auml;chung
                      des Kurzzeitged&auml;chtnisses, Kopfschmerzen, Migr&auml;ne,
                      anhaltende Schmerzen in den Bindegeweben (Fibromyalgie), Aufmerksamkeitsverlust,
                  Sprach- und Koordinations-, Schlaf- und Ess-St&ouml;rungen.</span></p>
              <p class="text">Das <a href="www.neurolab.eu" target="_blank">NeuroLab-Labor</a> misst die hormonellen und immunologischen
                Ver&auml;nderungen bei Burn-out, so u.a. die Cortisolproduktion
                durch Speicheltests, die nach dem Aufwachen, mittags und abends
                leicht zu machen sind. Weiter k&ouml;nnen die Neurotransmitter
                in einer Urinprobe gemessen werden.<br>
                Zur &Uuml;berwindung von Burn-out hat NeuroLab mit amerikanischen
                Partnern eine <strong>wirksame Therapie</strong> entwickelt, die aus Aminos&auml;uren
                als Vorstufen f&uuml;r Neurotransmitter und Cofaktoren f&uuml;r
                die Bildung von Enzymen bestehen. Nahrungserg&auml;nzungsmittel
                dienen der Reduktion von Entz&uuml;ndungen.<span class="text"><br>
                        </span><span class="text"><br>
                        </span><span class="&uuml;berschrift"><img src="images-gesundheit-praevention/caritas_mannheim_iconpunkt.gif" width="12" height="12"> WEr
                  ber&auml;t:</span><span class="text"> </span></p>
          <p><span class="text">&Auml;rzte im<a href="http://www.neurolab.eu/index.php?option=com_content&view=article&id=72&Itemid=148&#9001;=de" target="_blank"> Partnernetzwerk
                von Neurolab.</a><br>
                <br>
                F&uuml;r weitere Informationen <a href="http://www.neurolab.eu/20080121143/burn-out.html" target="_blank">bitte hier klicken.
                </a><br>
                <br>
                </span></p>              
                  <p align="right"><font size="2"><span class="text"><b><br>
                  </b></span><b><br>
                  </b><font size="4"><a href="http://www.neurolab.eu" target="_blank"><img src="neurolab_logo-1.jpg" width="120" height="77" border="0"></a><br>&copy;                  <a href="http://www.neurolab.eu" target="_blank">www.neurolab.eu</a></font></font></p>
              <p align="right"><font size="2" face="Arial, Helvetica, sans-serif">Bild: &copy; istockphoto.com</font><br>
              </p></td>
            <td background="boxright.gif" width="8"><img src="boxright.gif" width="8" height="8" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td width="8"><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
            <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
            <td width="8"><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
          </tr>
        </table></td>
        <td width="120" rowspan="2" valign="top">
		
	<script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
/* Gesundheitsprävention */
google_ad_slot = "0595101902";
google_ad_width = 120;
google_ad_height = 600;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>


</td>
      </tr>
      <tr>
        <td width="216" height="1374" align="left" valign="top">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>



</body>
</html>
