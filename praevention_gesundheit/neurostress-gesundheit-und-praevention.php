<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<head profile="http://dublincore.org/documents/dcq-html/">

<link rel="schema.DC" href="http://purl.org/dc/elements/1.1/">
<link rel="schema.DCTERMS" href="http://purl.org/dc/terms/">
<link href="caritas_mannheim.css" rel="stylesheet" type="text/css">

<title>Neurostress :: Gesundheit &amp; Pr&auml;vention ::</title>
<meta name="robots" content="index,follow,all" />
<meta name="description" content="Neurostress">
<meta name="author" content="Neurolab.eu">
<meta name="keywords" content="Neurostress">


<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="DC.title" content="Neurostress">

<meta name="DC.creator" content="Neurostress">
<meta name="DC.subject" content="Neurostress">
<meta name="DC.description" content="Neurostress">
<meta name="DC.publisher" content="Neurolab">
<meta name="DC.contributor" content="Neurostress">
<meta name="DC.date" content="2007-05-15T08:49:37+02:00" scheme="DCTERMS.W3CDTF">
<meta name="DC.type" content="Text" scheme="DCTERMS.DCMIType">
<meta name="DC.format" content="text/html" scheme="DCTERMS.IMT">
<meta name="DC.identifier"
      content="http://www.kath.de/lexikon/praevention_gesundheit/neurostress-gesundheit-und-praevention.php"
      scheme="DCTERMS.URI">
<meta name="DC.source"
      content="http://www.w3.org/TR/html401/struct/global.html#h-7.4.4"
      scheme="DCTERMS.URI">
<meta name="DC.language" content="de" scheme="DCTERMS.RFC3066">
<meta name="DC.relation" content="http://dublincore.org/" scheme="DCTERMS.URI">
<meta name="DC.coverage" content="Munich" scheme="DCTERMS.TGN">
<meta name="DC.rights" content="Alle Rechte liegen beim Autor">
</head>

<body background="images-gesundheit-praevention/neurolab-background-org.jpg" link="#ED5105" vlink="#666666" alink="#FF0000">
<table width="900" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="6">
      <tr>
        <td width="216" align="left" valign="top" height="3">
          <table width="216" border="0" cellpadding="0" cellspacing="0">
            <tr valign="top" align="left">
              <td width="8"><img src="boxtopleftcorner.jpg" width="8" height="8" alt=""></td>
              <td width="200" background="boxtop.jpg"><img src="boxtop.jpg" alt="" width="8" height="8"></td>
              <td width="8"><img src="boxtoprightcorner.jpg" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="boxtopleft.jpg"><img src="boxtopleft.jpg" width="8" height="8" alt=""></td>
              <td bgcolor="f7e39f"><p><b><font color="#333333" face="Arial, Helvetica, sans-serif">Gesundheit
                      &amp; Pr&auml;vention </font></b></p>
              </td>
              <td background="boxtopright.jpg"><img src="boxtopright.jpg" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="boxdividerleft.jpg" width="8" height="13" alt=""></td>
              <td background="boxdivider.jpg"><img src="boxdivider.jpg" alt="" width="8" height="13"></td>
              <td><img src="boxdividerright.jpg" width="8" height="13" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
              <td bgcolor="#FFFFFF">
                <?php include("logo.html"); ?>
              </td>
              <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
              <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
              <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
            </tr>
          </table>
          <br>
          <table width="216" border="0" cellpadding="0" cellspacing="0">
            <tr valign="top" align="left">
              <td width="8"><img src="boxtopleftcorner.jpg" width="8" height="8" alt=""></td>
              <td width="200" background="boxtop.jpg"><img src="boxtop.jpg" alt="" width="8" height="8"></td>
              <td width="8"><img src="boxtoprightcorner.jpg" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="boxtopleft.jpg"><img src="boxtopleft.jpg" width="8" height="8" alt=""></td>
              <td bgcolor="f7e39f"><font color="#333333" face="Arial, Helvetica, sans-serif"><strong>Inhaltsverzeichnis </strong></font></td>
              <td background="boxtopright.jpg"><img src="boxtopright.jpg" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="boxdividerleft.jpg" width="8" height="13" alt=""></td>
              <td background="boxdivider.jpg"><img src="boxdivider.jpg" alt="" width="8" height="13"></td>
              <td><img src="boxdividerright.jpg" width="8" height="13" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
              <td bgcolor="#FFFFFF" class="V10">
                <?php include("az.html"); ?>
              </td>
              <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
              <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
              <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
            </tr>
          </table>
        </td>
        <td width="530" rowspan="2" valign="top">          <table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr valign="top" align="left">
            <td width="8"><img src="boxtopleftcorner.jpg" width="8" height="8" alt=""></td>
            <td background="boxtop.jpg"><img src="boxtop.jpg" alt="" width="8" height="8"></td>
            <td width="8"><img src="boxtoprightcorner.jpg" width="8" height="8" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td background="boxtopleft.jpg" width="8"><img src="boxtopleft.jpg" width="8" height="8" alt=""></td>
            <td bgcolor="f7e39f">
              <h1><font color="#333333" face="Arial, Helvetica, sans-serif">Neurostress</font></h1>
            </td>
            <td background="boxtopright.jpg" width="8"><img src="boxtopright.jpg" width="8" height="8" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td width="8"><img src="boxdividerleft.jpg" width="8" height="13" alt=""></td>
            <td background="boxdivider.jpg"><img src="boxdivider.jpg" alt="" width="8" height="13"></td>
            <td width="8"><img src="boxdividerright.jpg" width="8" height="13" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td width="8" height="1793" background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
            <td bgcolor="#FFFFFF" class="L12">              <p class="text">    <img SRC="images-gesundheit-praevention/neurostress.jpg" alt="Neurostress" name="Neurostress" hspace="10" vspace="10" align="right" style="border: double 4px #009999">    <span class="&uuml;berschrift">Was
                bedeutet Neurostress?</span></p>
              <p class="text">Stress wurde bereits um 1930 durch <strong>Hans
                  Seyle</strong>                  als Begriff f&uuml;r
                die Reaktion des Individuums auf den bereits damals zunehmenden
                Leistungsdruck in der freien Wirtschaft gepr&auml;gt. Seitdem
                hat sich Stress zum popul&auml;ren Sammelbegriff f&uuml;r alle
                Folgen von Belastung, Hektik und &Uuml;berforderung entwickelt.
                Nach aktuellen Erhebungen der WHO sieht sich fast die H&auml;lfte
                der Bev&ouml;lkerung westlicher L&auml;nder als &uuml;berlastet,
                als &#8222;gestresst&#8220; an. Belastungen jeglicher Art sind &#8222;Stressoren&#8220;,
                die im &Uuml;berma&szlig; tief greifende Gesundheitsst&ouml;rungen
                hervorrufen k&ouml;nnen. Man denkt dabei an psychische oder berufliche
                Belastungen, tats&auml;chlich setzen jedoch alle Arten von Stress
                im Organismus ein nahezu identisches Anpassungsprogramm in Gang.</p>
              <p class="text"><strong>Stressoren sind:</strong></p>
              <ul>
                <li class="text"><strong>metabolischer Stress:</strong> falsche und zu energiereiche
                    Ern&auml;hrung</li>
                <li class="text"> <strong>physischer Stress: </strong>schwere Arbeit ebenso wie Sport</li>
                <li class="text"><strong>chemischer/physikalischer
                    Stress:</strong> Umweltfaktoren, Schadstoffe, Schwermetalle, Strahlung,
                    auch Medikamente</li>
                <li class="text"><strong>sensorischer Stress:</strong> L&auml;rmbelastung,
                      Reiz&uuml;berflutung, &uuml;berm&auml;&szlig;iger
                          Fernseh-/EDV-Konsum, Schlafmangel</li>
                <li class="text"><strong>mentaler Stress: </strong>wachsende
                      schulische, berufliche Belastungen, hohe Arbeitsintensit&auml;t</li>
                <li><span class="text">vor
                      allem aber<strong> psychischer Stress </strong>wie famili&auml;re Schicksalsschl&auml;ge,
                          Partnerkonflikte, soziale Vereinzelung, beruflicher
                    Konkurrenzdruck, mangelnde Anerkennung, Mobbing und Zukunfts&auml;ngste <br>
                </span>
                </ul>
                    <span class="text">In
                      Verbindung mit Krankheiten wie Entz&uuml;ndungen oder Infektionen
                            oder genetischer Veranlagung kann die Wirkung von
                      Stressoren verst&auml;rkt werden.<br>
                      All diese Stressoren aktivieren ein k&ouml;rpereigenes  Stressbew&auml;ltigungsprogramm,
                            bestehend aus <strong>hormonellen Komponenten</strong> (Stresshormonachse,
                      CRH-ACTH-Cortisol) und Komponenten des zentralen und autonomen<strong> Nervensystems </strong>(Serotonin,
                            Noradrenalin, Dopamin, Adrenalin, GABA, Glutamat).
                      Der Mensch ist f&uuml;r die Bew&auml;ltigung akuter Belastungen
                      optimal ausgestattet. Anhaltender Stress bzw. das l&auml;ngere
                      Zusammenwirken unterschiedlicher Stressoren f&uuml;hren
                      jedoch bei vielen Menschen im Laufe der Zeit zu folgenden
                      wachsenden
                            Gesundheitlichen
                  Belastungen.
                    </li>
                    </ul>
                    </ul> 
                    </span>                  <p class="text"><strong>Gesundheitsst&ouml;rungen
                        durch Neurostress:</strong></p>
                    <ul>
                      <li class="text">Leistungsabfall</li>
                      <li class="text">Motivationsverlust</li>
                      <li class="text">Konzentrationsschw&auml;che</li>
                      <li class="text">kognitiven Blockaden</li>
                      <li class="text">Unruhe</li>
                      <li class="text">&Auml;ngste</li>
                      <li class="text">Depressionen (StressDepression oder reaktive
                      Depression) bis zu schweren Depressionen, manischer Depression</li>
                      <li class="text">Essst&ouml;rungen</li>
                      <li class="text">Schlafprobleme</li>
                      <li class="text">Tagesm&uuml;digkeit</li>
                      <li class="text">Sogar Herzrhythmusst&ouml;rungen</li>
                      <li class="text">Kreislaufst&ouml;rungen</li>
                      <li class="text">Hoher Blutdruck</li>
                      <li class="text">asthmatische Beschwerden</li>
                      <li class="text">Verdauungsst&ouml;rungen (Reizdarm)</li>
                      <li class="text">Suchtprobleme</li>
                      <li class="text">Kopfschmerzen</li>
                      <li class="text">Migr&auml;ne</li>
                      <li class="text">Fibromyalgie</li>
                      <li class="text">posttraumatische Stresserkrankung</li>
                      <li class="text">CFS (Chronic Fatigue
                      Syndrom)</li>
                      <li class="text">Burn-Out</li>
                    </ul>                  <p class="text">Individuell disponierende Faktoren, genetische
                    Besonderheiten, sind mitbestimmend f&uuml;r die Auswirkungen der chronischen
                  Stressbelastungen. Bei Frauen h&auml;ufen sich speziell Zyklusbeschwerden,
                  pr&auml;menstruelle Beschwerden oder Menopausebeschwerden, bei
                  Kindern werden immer h&auml;ufiger Aufmerksamkeitsst&ouml;rungen
                  festgestellt (ADS/ADHS).</p>              <p class="text">In den neunziger Jahren, als man die medizinischen
                  Erkenntnisse der Verbindung von Leib und Seele im medizinischen
                  Fachgebiet
                der <strong>&#8222;Psychoneuroendokrino-immunologie&#8220;</strong> (PNEI) integrierte,
                entstand auch der Begriff des Neurostress, der in der Praxis
                das Zusammenwirken psychischer, neurologischer, endokriner/hormoneller
                und auch immunologischer Ph&auml;nomene der Stressreaktion beschreibt.
                Die begriffliche N&auml;he des Neurstress zum <strong>&#8222;Oxidativen
                Stress&#8220;</strong> ist nicht ohne Grundlage, denn energie-abh&auml;ngige
                Prozesse auf zellul&auml;rer Ebene im Zentralnervensystem, die
                zu entz&uuml;ndlichen Belastungen und vermehrtem Auftreten oxidativer
                Stoffwechselprodukten f&uuml;hren, sind beim Neurostress des
                zentralen und autonomen Nervensystems urs&auml;chlich beteiligt.
                Es gibt nicht wenige Experten, die dem Neurostress (PNEI) f&uuml;r
                das 21. Jahrhundert eine &auml;hnlich gro&szlig;e Bedeutung f&uuml;r
                das Gesundheitswesen beimessen wie der Entschl&uuml;sselung des
                Erbguts und der Entwicklung der Gentechnik im zwanzigsten Jahrhundert. </p>
              <p class="text">Die Mitglieder des &#8222;NeuroLab&#8220;-Teams
                haben sich von Beginn an der Erforschung, Diagnostik und Behandlung
                des NeuroStress
                und seiner gesundheitlichen Komplikationen verschrieben. Als
                erste Gruppe in Deutschland waren wir in der Lage, die neurochemischen,
                hormonellen und immunologischen Ph&auml;nomene des Neurostress
                in der Praxis zu messen und daraus h&ouml;chst innovative, gezielte
                Behandlungsstrategien zu entwickeln, die auf nat&uuml;rlichen,
                k&ouml;rpereigenen Botenstoffen und orthomolekularen Wirkstoffen
                basieren.              </p>
              <p><span class="text"><br>
                    </span><span class="&uuml;berschrift"><img src="images-gesundheit-praevention/caritas_mannheim_iconpunkt.gif" width="12" height="12"> WEr
                    behandelt:</span><span class="text"> </span></p>
              <p class="text"><span class="text">&Auml;rzte im<a href="http://www.neurolab.eu/index.php?option=com_content&view=article&id=72&Itemid=148&#9001;=de" target="_blank"> Partnernetzwerk
                    von Neurolab.</a></span><a href="http://www.neurolab.eu/index.php?option=com_google_maps&Itemid=39" target="_blank"><br>
                    <br>
                    </a>F�r weitere Informationen<a href="http://www.neurolab.eu/Neurostress-Patienten/" target="_blank"> bitte
                    hier klicken </a></p>              
              <p align="right"><font size="2"><span class="text"><b><br>
                  </b></span><b><br>
                  </b><font size="4"><a href="http://www.neurolab.eu" target="_blank"><img src="neurolab_logo-1.jpg" width="120" height="77" border="0"></a><br>&copy;                  <a href="http://www.neurolab.eu" target="_blank">www.neurolab.eu</a></font></font></p>
              </td>
            <td background="boxright.gif" width="8"><img src="boxright.gif" width="8" height="8" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td width="8" height="8"><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
            <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
            <td width="8"><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
          </tr>
        </table></td>
        <td width="120" rowspan="2" valign="top">
		
	<script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
/* Gesundheitspr�vention */
google_ad_slot = "0595101902";
google_ad_width = 120;
google_ad_height = 600;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>


</td>
      </tr>
      <tr>
        <td width="216" height="1374" align="left" valign="top">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>



</body>
</html>
