<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<head profile="http://dublincore.org/documents/dcq-html/">

<link rel="schema.DC" href="http://purl.org/dc/elements/1.1/">
<link rel="schema.DCTERMS" href="http://purl.org/dc/terms/">
<link href="caritas_mannheim.css" rel="stylesheet" type="text/css">

<title>Adrenalin :: Gesundheit &amp; Pr&auml;vention ::</title>
<meta name="robots" content="index,follow,all" />
<meta name="description" content="Adrenalin und Stress">
<meta name="author" content="Neurolab.eu">
<meta name="keywords" content="Adrenalin und Stress">


<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="DC.title" content="Adrenalin und Stress">

<meta name="DC.creator" content="Adrenalin und Stress">
<meta name="DC.subject" content="Adrenalin und Stress">
<meta name="DC.description" content="Adrenalin und Stress">
<meta name="DC.publisher" content="Neurolab.eu">
<meta name="DC.contributor" content="Neurolab.eu">
<meta name="DC.date" content="2007-05-15T08:49:37+02:00" scheme="DCTERMS.W3CDTF">
<meta name="DC.type" content="Text" scheme="DCTERMS.DCMIType">
<meta name="DC.format" content="text/html" scheme="DCTERMS.IMT">
<meta name="DC.identifier"
      content="http://www.kath.de/lexikon/praevention_gesundheit/adrenalin_stress.php"
      scheme="DCTERMS.URI">
<meta name="DC.source"
      content="http://www.w3.org/TR/html401/struct/global.html#h-7.4.4"
      scheme="DCTERMS.URI">
<meta name="DC.language" content="de" scheme="DCTERMS.RFC3066">
<meta name="DC.relation" content="http://dublincore.org/" scheme="DCTERMS.URI">
<meta name="DC.coverage" content="Munich" scheme="DCTERMS.TGN">
<meta name="DC.rights" content="Alle Rechte liegen beim Autor">
</head>

<body background="images-gesundheit-praevention/neurolab-background-org.jpg" link="#ED5105" vlink="#666666" alink="#FF0000">
<table width="900" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="6">
      <tr>
        <td width="216" align="left" valign="top" height="3">
          <table width="216" border="0" cellpadding="0" cellspacing="0">
            <tr valign="top" align="left">
              <td width="8"><img src="boxtopleftcorner.jpg" width="8" height="8" alt=""></td>
              <td width="200" background="boxtop.jpg"><img src="boxtop.jpg" alt="" width="8" height="8"></td>
              <td width="8"><img src="boxtoprightcorner.jpg" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="boxtopleft.jpg"><img src="boxtopleft.jpg" width="8" height="8" alt=""></td>
              <td bgcolor="f7e39f"><p><b><font color="#333333" face="Arial, Helvetica, sans-serif">Gesundheit
                      &amp; Pr&auml;vention </font></b></p>
              </td>
              <td background="boxtopright.jpg"><img src="boxtopright.jpg" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="boxdividerleft.jpg" width="8" height="13" alt=""></td>
              <td background="boxdivider.jpg"><img src="boxdivider.jpg" alt="" width="8" height="13"></td>
              <td><img src="boxdividerright.jpg" width="8" height="13" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
              <td bgcolor="#FFFFFF">
                <?php include("logo.html"); ?>
              </td>
              <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
              <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
              <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
            </tr>
          </table>
          <br>
          <table width="216" border="0" cellpadding="0" cellspacing="0">
            <tr valign="top" align="left">
              <td width="8"><img src="boxtopleftcorner.jpg" width="8" height="8" alt=""></td>
              <td width="200" background="boxtop.jpg"><img src="boxtop.jpg" alt="" width="8" height="8"></td>
              <td width="8"><img src="boxtoprightcorner.jpg" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="boxtopleft.jpg"><img src="boxtopleft.jpg" width="8" height="8" alt=""></td>
              <td bgcolor="f7e39f"><font color="#333333" face="Arial, Helvetica, sans-serif"><strong>Inhaltsverzeichnis </strong></font></td>
              <td background="boxtopright.jpg"><img src="boxtopright.jpg" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="boxdividerleft.jpg" width="8" height="13" alt=""></td>
              <td background="boxdivider.jpg"><img src="boxdivider.jpg" alt="" width="8" height="13"></td>
              <td><img src="boxdividerright.jpg" width="8" height="13" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
              <td bgcolor="#FFFFFF" class="V10">
                <?php include("az.html"); ?>
              </td>
              <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
              <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
              <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
            </tr>
          </table>
        </td>
        <td width="530" rowspan="2" valign="top">          <table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr valign="top" align="left">
            <td width="8"><img src="boxtopleftcorner.jpg" width="8" height="8" alt=""></td>
            <td background="boxtop.jpg"><img src="boxtop.jpg" alt="" width="8" height="8"></td>
            <td width="8"><img src="boxtoprightcorner.jpg" width="8" height="8" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td background="boxtopleft.jpg" width="8"><img src="boxtopleft.jpg" width="8" height="8" alt=""></td>
            <td bgcolor="f7e39f">
              <h1><font color="#333333" face="Arial, Helvetica, sans-serif">Adrenalin</font></h1>
            </td>
            <td background="boxtopright.jpg" width="8"><img src="boxtopright.jpg" width="8" height="8" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td width="8"><img src="boxdividerleft.jpg" width="8" height="13" alt=""></td>
            <td background="boxdivider.jpg"><img src="boxdivider.jpg" alt="" width="8" height="13"></td>
            <td width="8"><img src="boxdividerright.jpg" width="8" height="13" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td width="8" height="905" background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
            <td bgcolor="#FFFFFF" class="L12">
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="79%" class="&uuml;berschrift">Hormon, das bei Stress ausgesch&uuml;ttet wird</td>
                  <td valign=middle width="21%"><div align="right"> <span class="link"></span></div>
                  </td>
                </tr>
              </table>
              <p><span class="text">    <img SRC="images-gesundheit-praevention/adrenalin-neurolab.jpg" alt="Adrenalin" name="Adrenalin" hspace="10" vspace="10" align="right" style="border: double 4px #009999">     Das
                  Stresshormon <strong>Adrenalin</strong> wird in der
                  Nebenniere gebildet und bei physischen/psychischen Belastungen
                  ausgesch&uuml;ttet.
                  Seine Hauptfunktion ist die Anpassung des Herzkreislaufsystems
                  und des Stoffwechsels an stressbedingte Belastungen. Adrenalin
                  steigert u.a. die Pulsfrequenz, das Herzminutenvolumen und
                  den Blutdruck. Durch die Aussch&uuml;ttung des Adrenalins werden
                  <strong>Zucker und Fette</strong> f&uuml;r den h&ouml;heren Energiebedarf bereitgestellt. &Uuml;ber
                  erh&ouml;hte Atemfrequenz wird der f&uuml;r die Energiegewinnung
                  in den Zellen ben&ouml;tigte Sauerstoff zur Verf&uuml;gung
                  gestellt.<br>
                  <br>
Das Adrenalin wird
              schnell abgebaut. Bei andauerndem Stre&szlig; wird durch die dauernd
              erh&ouml;hten
                  Adrenalin- und Noradrenalinaussch&uuml;ttung der Blutdruck
                  erh&ouml;ht
                  und der Blutzuckerspiegel steigt an. Dadurch wird das <strong>Herz </strong>&uuml;berbelastet
                  und die <strong>Immunfunktion</strong> geht zur&uuml;ck.<br>
                  <br>
                  Ein <strong>hoher</strong> <strong>  Adrenalinwert </strong>kann neben
                  Stress auch auf folgende Ursachen zur&uuml;ckgef&uuml;hrt werden:
                  Medikamente (L-Dopa, Tetrazykline), Alkohol, Kaffee, schwarzer
                  Tee oder Unterzucker
                  (zu viel Insulin, Hunger). Exzessiv hohe Adrenalinwerte kommen
                  beim Ph&auml;ochromozytom vor, einem sehr seltenen Tumor der
                  Nebenniere.<br>
                  <br>
              </span><span class="text">                    <strong>Verminderte
              Adrenalinwerte</strong> k&ouml;nnen
                    durch einen Funktionsausfall der Nebenniere oder durch bestimmte
                    Medikamente
                    ( z.B. Langzeittherapie mit Resorpin, Guanethidin, Clonidin)
                    entstehen. Niedrige Adrenalinwerte werden au&szlig;erdem
                    bei Personen mit dem chronischen M&uuml;digkeitssyndrom,
                    starker Einschr&auml;nkung der Konzentrationsf&auml;higkeit
                  und beim Burn-Out-Syndrom beobachtet. <br>
                  <br>
              </span><span class="text"> Gen&uuml;gender Schlaf, Entspannung
              und Sport helfen, den Stress bei Dauerbelastung abzubauen. Dabei
              ist zu beachten, da&szlig; eine
                      Mahlzeit zu sp&auml;ter Stunde den Stress erh&ouml;ht,
                      weil die K&ouml;rperfunktionen f&uuml;r die<strong> Verdauung</strong> in
                      Gang gesetzt werden. <strong>Sport </strong>steigert insgesamt
                      die Belastungsf&auml;higkeit,
                      so da&szlig; der Organismus &ouml;konomischer auf Stress
                      reagiert. Sport baut das Adrenalin schneller ab und vor
                      allem die durch
                      die Adrenalinaussch&uuml;ttung bereitgestellten Energien. 
                    Im Moment kann die Adrenalinaussch&uuml;ttung nur in Stressituationen
                      gemessen werden.</span><span class="text"><br>
                      </span><span class="text"><br>
                      </span><span class="&uuml;berschrift"><img src="images-gesundheit-praevention/caritas_mannheim_iconpunkt.gif" width="12" height="12"> WEr
                  ber&auml;t:</span><span class="text"> </span></p>
              <p><span class="text">&Auml;rzte
                  im<a href="http://www.neurolab.eu/index.php?option=com_content&view=article&id=72&Itemid=148&#9001;=de" target="_blank"> Partnernetzwerk
                  von Neurolab. </a><br>
                  F�r weitere Informationen<a href="http://www.neurolab.eu/index.php?option=com_content&view=article&id=32&catid=13&Itemid=92&#9001;=de" target="_blank"> bitte hier klicken</a> </span><span class="text"><a href="http://www.neurolab.eu/index.php?option=com_content&view=article&id=72&Itemid=148&#9001;=de" target="_blank"><br>
                    <br>
                  </a></span><font size="2"><span class="text"><b>                  </b></span><b><br>
                  </b><font size="4"><a href="http://www.neurolab.eu" target="_blank"><img src="neurolab_logo-1.jpg" width="120" height="77" border="0"></a><br>&copy;                  <a href="http://www.neurolab.eu" target="_blank">www.neurolab.eu</a></font></font></p>
              <p align="right"><font size="2" face="Arial, Helvetica, sans-serif">Bild: &copy; istockphoto.com</font><br>
              </p></td>
            <td background="boxright.gif" width="8"><img src="boxright.gif" width="8" height="8" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td width="8"><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
            <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
            <td width="8"><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
          </tr>
        </table></td>
        <td width="120" rowspan="2" valign="top">
		
	<script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
/* Gesundheitspr�vention */
google_ad_slot = "0595101902";
google_ad_width = 120;
google_ad_height = 600;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>


</td>
      </tr>
      <tr>
        <td width="216" height="1374" align="left" valign="top">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>



</body>
</html>
