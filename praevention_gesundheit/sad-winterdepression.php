<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<head profile="http://dublincore.org/documents/dcq-html/">

<link rel="schema.DC" href="http://purl.org/dc/elements/1.1/">
<link rel="schema.DCTERMS" href="http://purl.org/dc/terms/">
<link href="caritas_mannheim.css" rel="stylesheet" type="text/css">

<title>SAD Winterdepression :: Gesundheit &amp; Pr&auml;vention ::</title>
<meta name="robots" content="index,follow,all" />
<meta name="description" content="SAD Winterdepression">
<meta name="author" content="Neurolab.eu">
<meta name="keywords" content="SAD Winterdepression">


<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="DC.title" content="SAD Winterdepression">

<meta name="DC.creator" content="SAD Winterdepression">
<meta name="DC.subject" content="SAD Winterdepression">
<meta name="DC.description" content="SAD Winterdepression">
<meta name="DC.publisher" content="SAD Winterdepression">
<meta name="DC.contributor" content="SAD Winterdepression">
<meta name="DC.date" content="2007-05-15T08:49:37+02:00" scheme="DCTERMS.W3CDTF">
<meta name="DC.type" content="Text" scheme="DCTERMS.DCMIType">
<meta name="DC.format" content="text/html" scheme="DCTERMS.IMT">
<meta name="DC.identifier"
      content="http://www.kath.de/lexikon/praevention_gesundheit/sad-winterdepression.php"
      scheme="DCTERMS.URI">
<meta name="DC.source"
      content="http://www.w3.org/TR/html401/struct/global.html#h-7.4.4"
      scheme="DCTERMS.URI">
<meta name="DC.language" content="de" scheme="DCTERMS.RFC3066">
<meta name="DC.relation" content="http://dublincore.org/" scheme="DCTERMS.URI">
<meta name="DC.coverage" content="Munich" scheme="DCTERMS.TGN">
<meta name="DC.rights" content="Alle Rechte liegen beim Autor">
</head>

<body background="images-gesundheit-praevention/neurolab-background-org.jpg" link="#ED5105" vlink="#666666" alink="#FF0000">
<table width="900" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="6">
      <tr>
        <td width="216" align="left" valign="top" height="3">
          <table width="216" border="0" cellpadding="0" cellspacing="0">
            <tr valign="top" align="left">
              <td width="8"><img src="boxtopleftcorner.jpg" width="8" height="8" alt=""></td>
              <td width="200" background="boxtop.jpg"><img src="boxtop.jpg" alt="" width="8" height="8"></td>
              <td width="8"><img src="boxtoprightcorner.jpg" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="boxtopleft.jpg"><img src="boxtopleft.jpg" width="8" height="8" alt=""></td>
              <td bgcolor="f7e39f"><p><b><font color="#333333" face="Arial, Helvetica, sans-serif">Gesundheit
                      &amp; Pr&auml;vention </font></b></p>
              </td>
              <td background="boxtopright.jpg"><img src="boxtopright.jpg" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="boxdividerleft.jpg" width="8" height="13" alt=""></td>
              <td background="boxdivider.jpg"><img src="boxdivider.jpg" alt="" width="8" height="13"></td>
              <td><img src="boxdividerright.jpg" width="8" height="13" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
              <td bgcolor="#FFFFFF">
                <?php include("logo.html"); ?>
              </td>
              <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
              <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
              <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
            </tr>
          </table>
          <br>
          <table width="216" border="0" cellpadding="0" cellspacing="0">
            <tr valign="top" align="left">
              <td width="8"><img src="boxtopleftcorner.jpg" width="8" height="8" alt=""></td>
              <td width="200" background="boxtop.jpg"><img src="boxtop.jpg" alt="" width="8" height="8"></td>
              <td width="8"><img src="boxtoprightcorner.jpg" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="boxtopleft.jpg"><img src="boxtopleft.jpg" width="8" height="8" alt=""></td>
              <td bgcolor="f7e39f"><font color="#333333" face="Arial, Helvetica, sans-serif"><strong>Inhaltsverzeichnis </strong></font></td>
              <td background="boxtopright.jpg"><img src="boxtopright.jpg" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="boxdividerleft.jpg" width="8" height="13" alt=""></td>
              <td background="boxdivider.jpg"><img src="boxdivider.jpg" alt="" width="8" height="13"></td>
              <td><img src="boxdividerright.jpg" width="8" height="13" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
              <td bgcolor="#FFFFFF" class="V10">
                <?php include("az.html"); ?>
              </td>
              <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
              <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
              <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
            </tr>
          </table>
        </td>
        <td width="530" rowspan="2" valign="top">          <table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr valign="top" align="left">
            <td width="8"><img src="boxtopleftcorner.jpg" width="8" height="8" alt=""></td>
            <td background="boxtop.jpg"><img src="boxtop.jpg" alt="" width="8" height="8"></td>
            <td width="8"><img src="boxtoprightcorner.jpg" width="8" height="8" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td background="boxtopleft.jpg" width="8"><img src="boxtopleft.jpg" width="8" height="8" alt=""></td>
            <td bgcolor="f7e39f">
              <h1><font color="#333333" face="Arial, Helvetica, sans-serif">SAD
                  Winterdepression</font></h1>
            </td>
            <td background="boxtopright.jpg" width="8"><img src="boxtopright.jpg" width="8" height="8" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td width="8"><img src="boxdividerleft.jpg" width="8" height="13" alt=""></td>
            <td background="boxdivider.jpg"><img src="boxdivider.jpg" alt="" width="8" height="13"></td>
            <td width="8"><img src="boxdividerright.jpg" width="8" height="13" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td width="8" height="905" background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
            <td bgcolor="#FFFFFF" class="L12">
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="79%" class="&uuml;berschrift">Winterdepressionen:
                      Tr&uuml;bes Wetter, tr&uuml;be Stimmung?</td>
                  <td valign=middle width="21%"><div align="right"> <span class="link"></span></div>
                  </td>
                </tr>
              </table>
              <p class="text">    <img SRC="images-gesundheit-praevention/sad-winterdepression-neurolab.jpg" alt="sad-winterdepression" name="sad-winterdepression" hspace="10" vspace="10" align="right" style="border: double 4px #009999">                In
                den n&ouml;rdlichen L&auml;ndern, aber auch in Mitteleuropa
                bedeutet die Herbst- und Winterzeit f&uuml;r viele Menschen ein
                absolutes Stimmungstief gepaart mit Hei&szlig;hungerattacken.
                Viele Symptome erinnern an eine Depression, so dass man auch
                von Winterdepressionen oder saisonalen Depressionen (SAD, Saisonal
                Affective Disorder) spricht. Eine wichtige Rolle bei der Entstehung,
                aber auch bei der Therapie scheint Licht zu spielen.<br>
                Wie erkennt man eine Winterdepression?<br>
                Fast k&ouml;nnte man denken, es sei ganz normal, dass man sich
                in der dunklen Jahreszeit eher schlapp, m&uuml;de und antriebslos
                f&uuml;hlt, da die meisten Menschen in der warmen und hellen
                Zeit eine scheinbar bessere Stimmung haben. Im Fr&uuml;hling
                sp&uuml;rt man regelrecht die Erleichterung bei vielen Mitmenschen,
                die auch weniger gereizt und freundlicher erscheinen, als es
                im Winter der Fall war. In den meisten F&auml;llen handelt es
                sich jedoch um keine echte abklingende Winterdepression, die
                den Stimmungsumschwung verursacht. Eine saisonale Depression
                oder SAD (Saisonal Affective Disorder) geht oft weit &uuml;ber
                eine schlechte Stimmung hinaus, die fast jeder einmal bei weniger
                gutem Wetter versp&uuml;ren kann. SAD-Patienten leiden so sehr,
                dass in einzelnen F&auml;llen sogar Selbstmordgefahr besteht.
                Bekannt geworden sind solche Suizidf&auml;lle insbesondere in
                den n&ouml;rdlichen L&auml;ndern, die eine sehr lange dunkle
                Zeitperiode kennen. Doch auch in Deutschland spielt die Winterdepression
                keine untergeordnete Rolle. So gehen verschiedene Studien davon
                aus, dass immerhin ein Prozent der Bev&ouml;lkerung betroffen
                ist, vielleicht liegt aber die echte Anzahl noch wesentlich h&ouml;her.
                Zum Teil geht man sogar von zehn Millionen Betroffenen in Deutschland
                aus, in Alaska zum Beispiel leidet etwa jeder Dritte an SAD.</p>
              <p class="text">Anders als bei Depressionen, die von der
                  Jahreszeit unabh&auml;ngig
                zu sein scheinen, zeigen SAD-Betroffene ein erh&ouml;htes Schlafbed&uuml;rfnis
                und werden von Hei&szlig;hungerattacken auf Kohlenhydrathaltige
                Lebensmittel &uuml;berfallen. Die Traurigkeit, Verzweiflung,
                Tagesm&uuml;digkeit, Gereiztheit, die auftretende Isolation von
                der Umwelt und der Motivationsverlust sind jedoch den Symptomen
                einer Depression sehr &auml;hnlich. Die genannten Symptome beginnen
                im Herbst, meist zwischen September und November, und dauern
                bis M&auml;rz oder April an. Dabei treten diese depressiven Episoden
                regelm&auml;&szlig;ig in der dunklen Jahreszeit wieder auf, Jahr
                f&uuml;r Jahr.</p>
              <p class="text"><span class="&uuml;berschrift">Welche m&ouml;glichen
                  Ursachen sind bekannt?</span><br>
  Eine genaue wissenschaftliche Erkl&auml;rung f&uuml;r SAD gibt
                es noch nicht. Saisonale Depressionen sind jedoch in warmen,
                sonnigen L&auml;ndern eher unbekannt und treten eher in Regionen
                auf, in denen starke jahreszeitliche Schwankungen die Regel sind.
                Allein diese Tatsache dr&auml;ngt den Verdacht auf, dass das
                mangelnde Tageslicht eine wichtige Rolle als Ursache spielen
                k&ouml;nnte. Tats&auml;chlich hat das nat&uuml;rliche Licht einen
                Einfluss auf die sogenannte biologische oder innere Uhr des Menschen.
                Vor der Erfindung des k&uuml;nstlichen Licht war der Mensch in
                seinen Lebensgewohnheiten und in der Dauer des Schlafes dem verf&uuml;gbaren
                Sonnenlicht unterworfen. &Auml;hnlich wie die Natur im Herbst
                und Winter Ruhephasen durchl&auml;uft, galt dies auch f&uuml;r
                fr&uuml;here Generationen. Der moderne Mensch aber versucht auch
                in der dunklen Jahreszeit den gleichen Lebensrhythmus zu verfolgen
                wie im Fr&uuml;hling und Sommer. Man vermutet deshalb, dass der
                Organismus gewisse Anpassungsschwierigkeiten mit der neuen Lebensweise
                hat. Doch nicht alle Menschen sind davon gleicherma&szlig;en
                betroffen. Auch leiden etwa dreimal so viele Frauen wie M&auml;nner
                unter Winterdepressionen. Dies kann als Hinweis auf einen genetischen
                Einfluss gewertet werden. </p>
              <p class="text"><span class="&uuml;berschrift">Welche Rolle spielt das Hormonsystem?</span><br>
  Das fehlende Tageslicht und die gest&ouml;rte, innere Uhr stehen
                in direkter Verbindung mit dem Hormonsystem. So ist unter anderem
                bekannt, dass Melatonin in Abh&auml;ngigkeit von der St&auml;rke
                des Umgebungslichtes, genauer des Lichteinfalls auf der Netzhaut,
                gebildet wird. Die Melatonin-Produktion erfolgt in der Zirbeldr&uuml;se
                im Stammhirn aus der Aminos&auml;ure Tryptophan &uuml;ber die
                Zwischenstufe Serotonin. In der Dunkelheit steigt die Melatonin-Produktion
                an. Deshalb ist auch die verf&uuml;gbare Melatonin-Menge im Winter
                deutlich h&ouml;her und f&uuml;hrt zu einer verst&auml;rkten
                M&uuml;digkeit als im Sommer.</p>
              <p class="text">Parallel zu dem Anstieg der Melatonin-Konzentration
                  sinkt die Menge an Serotonin als Vorstufe des Melatonins. Deshalb
                  kann
                eine Winterdepression auch mit einem ausgepr&auml;gten Serotoninmangel
                in Verbindung gebracht werden. Betrachtet man die Wirkungen von
                Serotonin beziehungsweise die Auswirkung eines Mangels daran,
                kann man die Symptome von SAD besser verstehen. So kann ein Serotoninmangel
                unter anderem zu Konzentrationsproblemen, Essst&ouml;rungen,
                Gewichtszunahme, unspezifische Bindegewebsschmerzen (Fibromyalgie),
                Empfindungsst&ouml;rungen, chronische Ersch&ouml;pfung (CFS,
                Fatigue), Angstzust&auml;nden, Migr&auml;ne und Depressionen
                f&uuml;hren.</p>
              <p class="text"><span class="&uuml;berschrift">Welche Untersuchungen empfielt NeuroLab bei Verdacht auf SAD?
                Welche Therapien gibt es?</span><br>
                NeuroLab bietet verschiedene Untersuchungsprofile an, um Gesundheitsst&ouml;rungen
                im Zusammenhang mit Neurostress und Ungleichgewichten zwischen
                Neurohormonen und Neurotransmittern genauer zu untersuchen und
                nach M&ouml;glichkeit behandeln zu k&ouml;nnen. Bei Saisonal
                Affective Disorder (SAD) bietet sich eine Anwendung des NeuroStress-Profils
                an, bei dem insbesondere auch die Serotonin-Konzentration bei
                dem Patienten festgestellt wird.</p>
              <p><span class="text">Auf Basis des Untersuchungsergebnisses stehen
                  verschiedene Therapiem&ouml;glichkeiten
                f&uuml;r SAD-Patienten zur Verf&uuml;gung, die ausgesprochen
                gute Behandlungserfolge erlauben. Zu nennen ist zum einen die
                Lichttherapie mit speziellen Strahlern, die mindestens eine Lichtst&auml;rke
                von 2.500 Lux bieten sollten. Ein bis zwei Stunden pro Tag sollten
                die Patienten mit offenen Augen im ausgeleuchteten Bereich der
                Lampe sitzen, ein direktes Hineinsehen ist nicht erforderlich.
                Solche Lichttherapien werden in n&ouml;rdlichen L&auml;ndern
                zum Teil bereits in &ouml;ffentlichen Cafes oder Restaurants
                angeboten. Daneben kommt eine Behandlung mit der Aminos&auml;urevorstufe
                5-HTP (5-Hydroxytryptophan) oder mit SSRIs (Antidepressiva, die
                die Wiederaufnahme von Serotonin in die Nervenzellen verhindern)
                in Betracht, um den Serotoninmangel ausgleichen zu k&ouml;nnen.
                Als Vorbeugung gilt auch die h&auml;ufige Bewegung an frischer
                Luft, um m&ouml;glichst viel Tageslicht einfangen zu k&ouml;nnen.
              </span>              </p>
              <p><span class="text"><br>
                    <br>
                    </span><span class="&uuml;berschrift"><img src="images-gesundheit-praevention/caritas_mannheim_iconpunkt.gif" width="12" height="12"> WEr
                                            ber&auml;t:</span><span class="text"> </span></p>
              <p><span class="text">&Auml;rzte im<a href="http://www.neurolab.eu/index.php?option=com_content&view=article&id=72&Itemid=148&#9001;=de" target="_blank"> Partnernetzwerk
                  von Neurolab.</a><br>
                  F&uuml;r weitere Informationen <a href="http://www.neurolab.eu/index.php?option=com_content&view=article&id=48&Itemid=82&#9001;=de" target="_blank">bitte hier klicken </a> </span></p>
              <p align="right"><font size="2"><span class="text"><b><br>
                  </b></span><b><br>
                  </b><font size="4"><a href="http://www.neurolab.eu" target="_blank"><img src="neurolab_logo-1.jpg" width="120" height="77" border="0"></a><br>&copy;                  <a href="http://www.neurolab.eu" target="_blank">www.neurolab.eu</a></font></font></p>
              <p align="right">&nbsp;</p></td>
            <td background="boxright.gif" width="8"><img src="boxright.gif" width="8" height="8" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td width="8" height="8"><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
            <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
            <td width="8"><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
          </tr>
        </table></td>
        <td width="120" rowspan="2" valign="top">
		
	<script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
/* Gesundheitsprävention */
google_ad_slot = "0595101902";
google_ad_width = 120;
google_ad_height = 600;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>


</td>
      </tr>
      <tr>
        <td width="216" height="1374" align="left" valign="top">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>



</body>
</html>
