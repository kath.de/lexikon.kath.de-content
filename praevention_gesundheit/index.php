<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<head profile="http://dublincore.org/documents/dcq-html/">

<link rel="schema.DC" href="http://purl.org/dc/elements/1.1/">
<link rel="schema.DCTERMS" href="http://purl.org/dc/terms/">
<link href="caritas_mannheim.css" rel="stylesheet" type="text/css">

<title>Online-Lexikon :: Gesundheit &amp; Pr&auml;vention ::</title>
<meta name="robots" content="index,follow,all" />
<meta name="description" content="Migr�ne">
<meta name="author" content="Neurolab.eu">
<meta name="keywords" content="Migr�ne">


<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="DC.title" content="Migr�ne">

<meta name="DC.creator" content="Migr�ne">
<meta name="DC.subject" content="Migr�ne">
<meta name="DC.description" content="Migr�ne">
<meta name="DC.publisher" content="Neurolab.eu">
<meta name="DC.contributor" content="Neurolab.eu">
<meta name="DC.date" content="2007-05-15T08:49:37+02:00" scheme="DCTERMS.W3CDTF">
<meta name="DC.type" content="Text" scheme="DCTERMS.DCMIType">
<meta name="DC.format" content="text/html" scheme="DCTERMS.IMT">
<meta name="DC.identifier"
      content="http://www.kath.de/lexikon/praevention_gesundheit/migraene-gesundheit-und-praevention.php"
      scheme="DCTERMS.URI">
<meta name="DC.source"
      content="http://www.w3.org/TR/html401/struct/global.html#h-7.4.4"
      scheme="DCTERMS.URI">
<meta name="DC.language" content="de" scheme="DCTERMS.RFC3066">
<meta name="DC.relation" content="http://dublincore.org/" scheme="DCTERMS.URI">
<meta name="DC.coverage" content="Munich" scheme="DCTERMS.TGN">
<meta name="DC.rights" content="Alle Rechte liegen beim Autor">
</head>

<body background="images-gesundheit-praevention/neurolab-background-org.jpg" link="#ED5105" vlink="#666666" alink="#FF0000">
<table width="900" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="6">
      <tr>
        <td width="216" align="left" valign="top" height="3">
          <table width="216" border="0" cellpadding="0" cellspacing="0">
            <tr valign="top" align="left">
              <td width="8"><img src="boxtopleftcorner.jpg" width="8" height="8" alt=""></td>
              <td width="200" background="boxtop.jpg"><img src="boxtop.jpg" alt="" width="8" height="8"></td>
              <td width="8"><img src="boxtoprightcorner.jpg" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="boxtopleft.jpg"><img src="boxtopleft.jpg" width="8" height="8" alt=""></td>
              <td bgcolor="f7e39f"><p><b><font color="#333333" face="Arial, Helvetica, sans-serif">Gesundheit
                      &amp; Pr&auml;vention </font></b></p>
              </td>
              <td background="boxtopright.jpg"><img src="boxtopright.jpg" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="boxdividerleft.jpg" width="8" height="13" alt=""></td>
              <td background="boxdivider.jpg"><img src="boxdivider.jpg" alt="" width="8" height="13"></td>
              <td><img src="boxdividerright.jpg" width="8" height="13" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
              <td bgcolor="#FFFFFF">
                <?php include("logo.html"); ?>
              </td>
              <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
              <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
              <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
            </tr>
          </table>
          <br>
          <table width="216" border="0" cellpadding="0" cellspacing="0">
            <tr valign="top" align="left">
              <td width="8"><img src="boxtopleftcorner.jpg" width="8" height="8" alt=""></td>
              <td width="200" background="boxtop.jpg"><img src="boxtop.jpg" alt="" width="8" height="8"></td>
              <td width="8"><img src="boxtoprightcorner.jpg" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="boxtopleft.jpg"><img src="boxtopleft.jpg" width="8" height="8" alt=""></td>
              <td bgcolor="f7e39f"><font color="#333333" face="Arial, Helvetica, sans-serif"><strong>Inhaltsverzeichnis </strong></font></td>
              <td background="boxtopright.jpg"><img src="boxtopright.jpg" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="boxdividerleft.jpg" width="8" height="13" alt=""></td>
              <td background="boxdivider.jpg"><img src="boxdivider.jpg" alt="" width="8" height="13"></td>
              <td><img src="boxdividerright.jpg" width="8" height="13" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
              <td bgcolor="#FFFFFF" class="V10">
                <?php include("az.html"); ?>
              </td>
              <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
              <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
              <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
            </tr>
          </table>
        </td>
        <td width="530" rowspan="2" valign="top">          <table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr valign="top" align="left">
            <td width="8"><img src="boxtopleftcorner.jpg" width="8" height="8" alt=""></td>
            <td background="boxtop.jpg"><img src="boxtop.jpg" alt="" width="8" height="8"></td>
            <td width="8"><img src="boxtoprightcorner.jpg" width="8" height="8" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td background="boxtopleft.jpg" width="8"><img src="boxtopleft.jpg" width="8" height="8" alt=""></td>
            <td bgcolor="f7e39f">
              <h1><font color="#333333" face="Arial, Helvetica, sans-serif">::
                  Online-Lexikon
                  ::<br>
                  Gesundheit und Pr&auml;vention</font></h1>
            </td>
            <td background="boxtopright.jpg" width="8"><img src="boxtopright.jpg" width="8" height="8" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td width="8"><img src="boxdividerleft.jpg" width="8" height="13" alt=""></td>
            <td background="boxdivider.jpg"><img src="boxdivider.jpg" alt="" width="8" height="13"></td>
            <td width="8"><img src="boxdividerright.jpg" width="8" height="13" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td width="8" height="905" background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
            <td bgcolor="#FFFFFF" class="L12">
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td><span class="text"><img src="images-gesundheit-praevention/neurolab-lexikon-titelbild-apfel.jpg" alt="Migraene" name="Migraene" style="border: double 4px #009999"></span></td>
                  </tr>
              </table>
              <p>                <font size="2"><span class="text"><strong>Gesundheit</strong>                    sollte kein Buch mit sieben Siegeln sein. Dieses Lexikon
                    bietet Ihnen fundierte<strong> wissenschaftliche Informationen</strong>. In
                    unserer stressbelasteten schnellebigen Zeit nehmen die Funktionsst&ouml;rungen
                    und die daraus
                    resultierenden Krankheitsbilder zu. Dieses Lexikon zeigt
                    wie Sie auf <strong>nat&uuml;rliche Weise</strong> therapeutisch eingreifen
                    k&ouml;nnen um gesund zu werden.
                Informieren
                      Sie sich in diesem Online-Lexikon &uuml;ber die Ursachen veschiedener
                  Gesundheitsst&ouml;rungen wie <br>
                  <br>
              </span></font><font size="2"><span class="text">                    Burnout, <br>
                    Depression, <br>
                    Schlafst&ouml;rungen, <br>
                    Panikattacken, <br>
&Uuml;bergewicht, <br>
Hei&szlig;hunger, <br>
                    ADS/ADHS, <br>
                    CFS/MCS/Fibromyalgie, <br>
                    Migr&auml;ne, <br>
                    Colon irritabile, <br>
                    Pr&auml;menstruelles
                        Syndrom <b><br>
                        <br>
                        </b>und sehen Sie die M&ouml;glichkeiten zur Behandlung. Neurolab
                        bietet Ihnen ein &uuml;berzeugendes<a href="http://www.neurolab.eu/index.php?option=com_content&task=view&id=99&Itemid=53" target="_blank"> Therapiekonzept.</a><b><br>
                        </b></span><b><br>
                        </b><font size="4"><a href="http://www.neurolab.eu" target="_blank"><img src="neurolab_logo-1.jpg" width="120" height="77" border="0"></a><br>&copy;                  <a href="http://www.neurolab.eu" target="_blank">www.neurolab.eu</a></font></font></p>
              <p align="right"><font size="2" face="Arial, Helvetica, sans-serif">Bild: &copy; Liv
                  Friis-larsen -
                Fotolia.com</font><br>
              </p></td>
            <td background="boxright.gif" width="8"><img src="boxright.gif" width="8" height="8" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td width="8"><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
            <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
            <td width="8"><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
          </tr>
        </table></td>
        <td width="120" valign="top"align=Right>

                <p><a href="http://www.neufeld-verlag.de/" target="_blank"><img src="http://www.kath.de/lexikon/b_hinweise/neufeld.jpg" width="286" height="238" border="0"></a><br>

        <script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
/* Gesundheitspr�vention */
google_ad_slot = "0595101902";
google_ad_width = 120;
google_ad_height = 600;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>


</td>
      </tr>
      <tr>
        <td width="216" height="1374" align="left" valign="top">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>



</body>
</html>