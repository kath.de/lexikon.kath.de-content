<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<head profile="http://dublincore.org/documents/dcq-html/">

<link rel="schema.DC" href="http://purl.org/dc/elements/1.1/">
<link rel="schema.DCTERMS" href="http://purl.org/dc/terms/">
<link href="caritas_mannheim.css" rel="stylesheet" type="text/css">

<title>ADS - ADHS :: Gesundheit &amp; Pr&auml;vention ::</title>
<meta name="robots" content="index,follow,all" />
<meta name="description" content="ADS ADHS">
<meta name="author" content="Neurolab.eu">
<meta name="keywords" content="ADS ADHS">


<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="DC.title" content="ADS ADHS">

<meta name="DC.creator" content="ADS ADHS">
<meta name="DC.subject" content="ADS ADHS">
<meta name="DC.description" content="ADS ADHS">
<meta name="DC.publisher" content="Neurolab">
<meta name="DC.contributor" content="ADS ADHS">
<meta name="DC.date" content="2007-05-15T08:49:37+02:00" scheme="DCTERMS.W3CDTF">
<meta name="DC.type" content="Text" scheme="DCTERMS.DCMIType">
<meta name="DC.format" content="text/html" scheme="DCTERMS.IMT">
<meta name="DC.identifier"
      content="http://www.kath.de/lexikon/praevention_gesundheit/ads-adhs-gesundheit-und-praevention.php"
      scheme="DCTERMS.URI">
<meta name="DC.source"
      content="http://www.w3.org/TR/html401/struct/global.html#h-7.4.4"
      scheme="DCTERMS.URI">
<meta name="DC.language" content="de" scheme="DCTERMS.RFC3066">
<meta name="DC.relation" content="http://dublincore.org/" scheme="DCTERMS.URI">
<meta name="DC.coverage" content="Munich" scheme="DCTERMS.TGN">
<meta name="DC.rights" content="Alle Rechte liegen beim Autor">
</head>

<body background="images-gesundheit-praevention/neurolab-background-org.jpg" link="#ED5105" vlink="#666666" alink="#FF0000">
<table width="900" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="6">
      <tr>
        <td width="216" align="left" valign="top" height="3">
          <table width="216" border="0" cellpadding="0" cellspacing="0">
            <tr valign="top" align="left">
              <td width="8"><img src="boxtopleftcorner.jpg" width="8" height="8" alt=""></td>
              <td width="200" background="boxtop.jpg"><img src="boxtop.jpg" alt="" width="8" height="8"></td>
              <td width="8"><img src="boxtoprightcorner.jpg" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="boxtopleft.jpg"><img src="boxtopleft.jpg" width="8" height="8" alt=""></td>
              <td bgcolor="f7e39f"><p><b><font color="#333333" face="Arial, Helvetica, sans-serif">Gesundheit
                      &amp; Pr&auml;vention </font></b></p>
              </td>
              <td background="boxtopright.jpg"><img src="boxtopright.jpg" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="boxdividerleft.jpg" width="8" height="13" alt=""></td>
              <td background="boxdivider.jpg"><img src="boxdivider.jpg" alt="" width="8" height="13"></td>
              <td><img src="boxdividerright.jpg" width="8" height="13" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
              <td bgcolor="#FFFFFF">
                <?php include("logo.html"); ?>
              </td>
              <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
              <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
              <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
            </tr>
          </table>
          <br>
          <table width="216" border="0" cellpadding="0" cellspacing="0">
            <tr valign="top" align="left">
              <td width="8"><img src="boxtopleftcorner.jpg" width="8" height="8" alt=""></td>
              <td width="200" background="boxtop.jpg"><img src="boxtop.jpg" alt="" width="8" height="8"></td>
              <td width="8"><img src="boxtoprightcorner.jpg" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="boxtopleft.jpg"><img src="boxtopleft.jpg" width="8" height="8" alt=""></td>
              <td bgcolor="f7e39f"><font color="#333333" face="Arial, Helvetica, sans-serif"><strong>Inhaltsverzeichnis </strong></font></td>
              <td background="boxtopright.jpg"><img src="boxtopright.jpg" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="boxdividerleft.jpg" width="8" height="13" alt=""></td>
              <td background="boxdivider.jpg"><img src="boxdivider.jpg" alt="" width="8" height="13"></td>
              <td><img src="boxdividerright.jpg" width="8" height="13" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
              <td bgcolor="#FFFFFF" class="V10">
                <?php include("az.html"); ?>
              </td>
              <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
              <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
              <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
            </tr>
          </table>
        </td>
        <td width="530" rowspan="2" valign="top">          <table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr valign="top" align="left">
            <td width="8"><img src="boxtopleftcorner.jpg" width="8" height="8" alt=""></td>
            <td background="boxtop.jpg"><img src="boxtop.jpg" alt="" width="8" height="8"></td>
            <td width="8"><img src="boxtoprightcorner.jpg" width="8" height="8" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td background="boxtopleft.jpg" width="8"><img src="boxtopleft.jpg" width="8" height="8" alt=""></td>
            <td bgcolor="f7e39f">
              <h1><font color="#333333" face="Arial, Helvetica, sans-serif">ADS
                  - ADHS</font></h1>
            </td>
            <td background="boxtopright.jpg" width="8"><img src="boxtopright.jpg" width="8" height="8" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td width="8"><img src="boxdividerleft.jpg" width="8" height="13" alt=""></td>
            <td background="boxdivider.jpg"><img src="boxdivider.jpg" alt="" width="8" height="13"></td>
            <td width="8"><img src="boxdividerright.jpg" width="8" height="13" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td width="8" height="905" background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
            <td bgcolor="#FFFFFF" class="L12">              <p><span class="text">    <img SRC="images-gesundheit-praevention/ads-adhs.jpg" alt="ADS-ADHS" name="ADS-ADHS" hspace="10" vspace="10" align="right" style="border: double 4px #009999">    </span><span class="&uuml;berschrift"><strong>Einen
                    Zappelphilipp gibt es nicht nur im Kinderbuch</strong></span><span class="text"><strong> </strong><br>
                  <br>
Die Geschichten des Arztes Dr. Hoffmann rund um den Struwwelpeter, Suppenkasper,
Hans Guck-in-die-Luft und Zappelphilipp haben einen medizinischen Kern. Denn
Zappelphilipp ist nicht nur die ewig aktuelle Erfindung eines Kinderbuchautors.
Bis zu<strong> zehn Prozent </strong>aller Kinder leiden heute n&auml;mlich an
Konzentrationsschw&auml;che und Hyperaktivit&auml;t. Die im Kindesalter beginnende
Verhaltens- und Lernst&ouml;rung ist oft genetisch veranlagt. Zunehmend wird
klar, dass es sich hier um Entgleisungen des Hormonstoffwechsels im Gehirn handelt.
Darum ist eine Herstellung des hormonellen Gleichgewichts im Gehirn- und Nervenstoffwechsel
eine wichtige Rolle bei der Behandlung von AD(H)S darstellt.</span></p>
              <p class="text"><span class="&uuml;berschrift">Stillsitzen und
                  Aufpassen sind kaum m&ouml;glich</span><br>
                    <br>
  Konzentrationsschw&auml;che, Lernschwierigkeiten, Vergesslichkeit und teilweise
  eine &uuml;bersteigerte Aktivit&auml;t und Unruhe, diese Symptome zeigen viele
  Kinder, Jugendliche, aber auch Erwachsene in unserer Zeit, so m&ouml;chte man
  sagen. Doch das Aufmerksamkeits-Defizitsyndrom (ADS) sowie die gemeinsam mit
  einer Hyperaktivit&auml;t auftretende Variante ADHS ist kein reines <strong>Ph&auml;nomen
  unserer Zeit</strong>, die gepr&auml;gt ist durch Reiz&uuml;berflutung und
  massive Umwelteinfl&uuml;sse. Alleine schon die Literatur lehrt uns, dass die
  Symptome, nicht stillsitzen und nicht aufpassen zu k&ouml;nnen, auch schon
  zu fr&uuml;herer Zeit beobachtet werden konnte. So ist der Hans Guck-in-die-Luft
  aus dem Kinderbuch von Dr. Hoffmann ein typisches Beispiel f&uuml;r ein Kind
  mit ADS, w&auml;hrend der fast noch bekanntere Zappelphilipp eine sehr deutliche
  Erscheinung des ADS mit Hyperaktivit&auml;t ist. </p>
              <p class="text">Das Erscheinungsbild dieser Gesundheitsst&ouml;rung
                ist sehr vielschichtig und umfasst auch Symptome wie eine leichte
                Erregbarkeit, eine Impulsivit&auml;t, eine schnelle Frustration
                und eine eher ungeschickte Beweglichkeit. Schon an den Namen
                der Figuren aus Dr. Hoffmanns Kinderbuch kann man ablesen, dass
                Jungen h&auml;ufiger von einer Aufmerksamkeitsst&ouml;rung betroffen
                sind als M&auml;dchen. Insbesondere der hyperaktive und impulsive
                Erscheinungstyp ist bei <strong>Jungen</strong> deutlich h&auml;ufiger
                anzutreffen. Man geht hier von einem <strong>Verh&auml;ltnis
                von</strong> <strong>5:1</strong> aus. In 30 bis 50 Prozent der
                F&auml;lle endet die St&ouml;rung nicht mit der Kindheit, sondern
                betrifft auch Erwachsene. W&auml;hrend die Hyperaktivit&auml;t
                bei Erwachsenen eher abklingt, bleiben die Aufmerksamkeitsst&ouml;rung,
                die Impulsivit&auml;t und die Stimmungsschwankungen in diesen
                F&auml;llen erhalten. Die Beobachtung, dass ADS und ADHS bei
                eineiigen Zwillingen sehr h&auml;ufig bei beiden auftritt, legt
                heute den Schluss nahe, dass es eine genetische Veranlagung (Disposition)
                f&uuml;r diese Gesundheitsst&ouml;rung gibt. </p>
              <p class="text"><span class="&uuml;berschrift">Bekannte Ursachen
                  f&uuml;r ADS / ADHS</span><br>
                    <br>
  Die ersten Erkl&auml;rungsversuche sahen die Ursache f&uuml;r ADS / ADHS in
  einer Entwicklungsst&ouml;rung des Gehirns. Die fr&uuml;here Bezeichnung Minimale
  Celebrale Dysfunktion (MCD) ist inzwischen ebenfalls &uuml;berholt. Andere
  Untersuchungen sahen die Ursachen eher im psycho-sozialen Bereich und legten
  das Bild von &#8222;schlechten Eltern&#8220;, Erziehungsfehlern und einem viel
  zu kleinem Lebensraum mit Bewegungsarmut f&uuml;r das Kind nahe. Die Einfl&uuml;sse
  von bestimmten Nahrungsmitteln und Nahrungsmittelzus&auml;tzen, der Reiz&uuml;berflutung,
  dem Rauchen und dem Alkoholkonsum der Mutter w&auml;hrend der Schwangerschaft
  und andere Umwelteinfl&uuml;sse werden in der Forschung <strong>uneinheitlich
  beurteilt</strong>. M&ouml;glich erscheint es, dass eine genetische Veranlagung
  durch die genannten Faktoren in ihrer Auspr&auml;gung beg&uuml;nstigt wird.
  Die Bedeutung von Schwermetallvergiftungen (Blei, Quecksilber, Cadmium, Arsen)
  und die &Uuml;berempfindlichkeit gegen&uuml;ber Chemikalien (MCS) ist noch
  umstritten. Die aktuelle Forschung zeigt aber eine wesentliche Rolle des Hormonsystems
  an. Bedenkt man die Rolle der Neurohormone und Neurotransmitter f&uuml;r die
  Stimmungslage, Steuerung der Bewegungen, die Entwicklung der Lern- und Ged&auml;chtnisfunktionen,
  so liegt ein Zusammenhang mit AD(H)S nahe.</p>
              <p class="text"><span class="&uuml;berschrift">Ungleichgewicht
                  im Hormonsystem</span><br>
                  <br>
  Auffallend h&auml;ufig leiden Kinder mit ADS / ADHS an einer <strong>Unterfunktion</strong> der
  Schilddr&uuml;se. Schilddr&uuml;senhormone wirken regulierend auf den Stoffwechsel
  der neuronalen Botenstoffe, also auf das Netzwerk der Neurotransmitter. Grundlegend
  f&uuml;r AD(H)S scheint eine Stoffwechselst&ouml;rung im Gehirn und Nervensystem
  zu sein, die insbesondere zu einem Ungleichgewicht bei den Botenstoffen Dopamin
  und Noradrenalin f&uuml;hrt. Dadurch wird eine geordnete Informationsverarbeitung
  im Gehirn behindert. So erkl&auml;rt sich die meist positive Wirkung einer
  Medikamentation mit <strong>Ritalin (Methylphenidat)</strong>, <strong>Amphetaminsaft </strong>oder <strong>Captagon
  (Fenetyllin) </strong>unter anderem durch deren Einfluss auf den Dopamin-Haushalt.
  Bei der therapeutischen Entscheidungsfindung sollten auch die nicht unerheblichen
  Nebenwirkungen ber&uuml;cksichtigt werden. <br>
  Auch ein Ungleichgewicht bei dem Botenstoff PEA kann im Fall von ADS/ ADHS
  beobachtet werden. W&auml;hrend bei Patienten mit Aufmerksamkeitsdefiziten
  ein niedriger PEA-Spiegel festgestellt werden kann, normalisiert sich die Konzentration
  durch die Gabe von Ritalin. Bei Erwachsenen werden eher Antidepressiva (Nortriptylin,
  Desipramin, Imipramin), Noradrenalin-Wiederaufnahmehemmer wie Strattera (Atomoxetin)
  und Edronax (Reboxetin) oder auch das Antidepressivum Venlafaxin eingesetzt. </p>
              <p class="text"><span class="&uuml;berschrift">Empfohlene Untersuchungsm&ouml;glichkeiten
                  von NeuroLab und ein neues Therapiekonzept</span><br>
                    <br>
  Zur Kl&auml;rung eines Verdachts auf ADS/ADHS setzt NeuroLab spezielle Untersuchungsprofile
  unter dem Label NEUROSTRESS ein. Sinnvoll ist in diesem Fall das Profil ADS/ADHS
  plus die Konzentration von Adrenalin, Noradrenalin, Dopamin, Serotonin, GABA
  und zus&auml;tzlich noch PEA im so genannten zweiten Morgenurin zu bestimmen.
  Zus&auml;tzlich werden zum Beispiel die Konzentrationen von Zink, Magnesium
  und Vitamin B6 untersucht, da ein entsprechender Mangel ebenfalls mit dem Auftreten
  der Aufmerksamkeitsdefizit- beziehungsweise Hyperaktivit&auml;tsst&ouml;rungen
  in Verbindung zu stehen scheint. Als weitere Ursache eines chronischen Zink
  und Vitamin B6-Mangels kommt eine erh&ouml;hte Ausscheidung von Kryptopyrrol
  in Frage, was durch Messung von Krypropyrrol im ersten Morgenurin analysiert
  werden kann. Aktuelle AD(H)S-Studien zeigen ein geh&auml;uftes Vorkommen von
  Mikron&auml;hrstoffdefiziten, darunter auch ein Mangel an Niacin, Pyridoxin,
  Thiamin, Folat, Vitamin C und Omega-3-Fetts&auml;uren.<br>
  Nach Einsendung der Proben werden diese analysiert. Das Untersuchungsergebnis
  wird ausf&uuml;hrlich interpretiert und schlie&szlig;t mit einer Therapieempfehlung
  ab. Zentraler Inhalt ist dabei die Behandlung des Patienten mit speziellen
  Aminos&auml;uren, zur Wiederherstellung der Neurotransmitterbalance. <br>
  Eine zus&auml;tzliche Zufuhr (Supplementierung) mit Vitamin B6, Omega-3-Fetts&auml;uren,
  Flavonoiden und Phosphatidylserin kann zusammen mit einer Entgiftung, einer
  speziellen Di&auml;t und der Korrektur einer m&ouml;glichen Fehlbesiedlung
  des Darms deutliche Linderungen der Symptome erm&ouml;glichen. Weiterhin sieht
  ein Behandlungsplan die psychologische Beratung der Kinder und Eltern vor,
  auch die Ergotherapie, die Bewegungstherapie und Musiktherapie kann hierbei
  zum Einsatz kommen. Meist muss jedoch als erster Schritt eine medikament&ouml;se
  Therapie zur Wiederherstellung eines hormonellen Gleichgewichts erfolgen.</p>
              <p><span class="text">Text: Oliver Schonschek, Diplom-Physiker <br>
&copy; NeuroLab GmbH</span><span class="text"><br>
                    </span><span class="text"><br>
                    </span><span class="&uuml;berschrift"><img src="images-gesundheit-praevention/caritas_mannheim_iconpunkt.gif" width="12" height="12"> WEr
                  behandelt:</span><span class="text"> </span></p>
              <p><span class="text">&Auml;rzte im<a href="http://www.neurolab.eu/index.php?option=com_content&view=article&id=72&Itemid=148&#9001;=de" target="_blank"> Partnernetzwerk
                    von Neurolab.</a><a href="http://www.neurolab.eu/index.php?option=com_google_maps&Itemid=39" target="_blank"><br>
                     </a>F&uuml;r weitere Informationen <a href="http://www.neurolab.eu/index.php?option=com_content&view=article&id=40%3Aads&catid=16&Itemid=90&#9001;=de" target="_blank">bitte hier klicken</a></span></p>              
              <p align="right"><font size="2"><span class="text"><b><br>
                  </b></span><b><br>
                  </b><font size="4"><a href="http://www.neurolab.eu" target="_blank"><img src="neurolab_logo-1.jpg" width="120" height="77" border="0"></a><br>&copy;                  <a href="http://www.neurolab.eu" target="_blank">www.neurolab.eu</a></font></font></p>
              </td>
            <td background="boxright.gif" width="8"><img src="boxright.gif" width="8" height="8" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td width="8" height="8"><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
            <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
            <td width="8"><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
          </tr>
        </table></td>
        <td width="120" rowspan="2" valign="top">
		
	<script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
/* Gesundheitsprävention */
google_ad_slot = "0595101902";
google_ad_width = 120;
google_ad_height = 600;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>


</td>
      </tr>
      <tr>
        <td width="216" height="1374" align="left" valign="top">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>



</body>
</html>
