<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<head profile="http://dublincore.org/documents/dcq-html/">

<link rel="schema.DC" href="http://purl.org/dc/elements/1.1/">
<link rel="schema.DCTERMS" href="http://purl.org/dc/terms/">
<link href="caritas_mannheim.css" rel="stylesheet" type="text/css">

<title>Reizdarm :: Gesundheit & Prävention ::</title>
<meta name="robots" content="index,follow,all" />
<meta name="description" content="Der Reizdarm">
<meta name="author" content="Neurolab.eu">
<meta name="keywords" content="Reizdarm">


<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="DC.title" content="Der Reizdarm">

<meta name="DC.creator" content="Der Reizdarm">
<meta name="DC.subject" content="Der Reizdarm">
<meta name="DC.description" content="Der Reizdarm">
<meta name="DC.publisher" content="Der Reizdarm">
<meta name="DC.contributor" content="Der Reizdarm">
<meta name="DC.date" content="2007-05-15T08:49:37+02:00" scheme="DCTERMS.W3CDTF">
<meta name="DC.type" content="Text" scheme="DCTERMS.DCMIType">
<meta name="DC.format" content="text/html" scheme="DCTERMS.IMT">
<meta name="DC.identifier"
      content="http://www.kath.de/caritas_Mannheim/kurzzeitpflege_caritas_mannheim.php"
      scheme="DCTERMS.URI">
<meta name="DC.source"
      content="http://www.w3.org/TR/html401/struct/global.html#h-7.4.4"
      scheme="DCTERMS.URI">
<meta name="DC.language" content="de" scheme="DCTERMS.RFC3066">
<meta name="DC.relation" content="http://dublincore.org/" scheme="DCTERMS.URI">
<meta name="DC.coverage" content="Munich" scheme="DCTERMS.TGN">
<meta name="DC.rights" content="Alle Rechte liegen beim Autor">
</head>

<body background="images-gesundheit-praevention/thumbbild0010.jpg" link="#ED5105" vlink="#666666" alink="#FF0000">
<table width="800" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="6">
      <tr>
        <td width="100" align="left" valign="top" height="3">
          <table width="216" border="0" cellpadding="0" cellspacing="0">
            <tr valign="top" align="left">
              <td width="8"><img src="boxtopleftcorner.jpg" width="8" height="8" alt=""></td>
              <td width="200" background="boxtop.jpg"><img src="boxtop.jpg" alt="" width="8" height="8"></td>
              <td width="8"><img src="boxtoprightcorner.jpg" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="boxtopleft.jpg"><img src="boxtopleft.jpg" width="8" height="8" alt=""></td>
              <td bgcolor="f7e39f"><p><b><font color="#333333" face="Arial, Helvetica, sans-serif">Gesundheit
                      &amp; Pr&auml;vention </font></b></p>
              </td>
              <td background="boxtopright.jpg"><img src="boxtopright.jpg" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="boxdividerleft.jpg" width="8" height="13" alt=""></td>
              <td background="boxdivider.jpg"><img src="boxdivider.jpg" alt="" width="8" height="13"></td>
              <td><img src="boxdividerright.jpg" width="8" height="13" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
              <td bgcolor="#FFFFFF">
                <?php include("logo.html"); ?>
              </td>
              <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
              <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
              <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
            </tr>
          </table>
          <br>
          <table width="216" border="0" cellpadding="0" cellspacing="0">
            <tr valign="top" align="left">
              <td width="8"><img src="boxtopleftcorner.jpg" width="8" height="8" alt=""></td>
              <td width="200" background="boxtop.jpg"><img src="boxtop.jpg" alt="" width="8" height="8"></td>
              <td width="8"><img src="boxtoprightcorner.jpg" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="boxtopleft.jpg"><img src="boxtopleft.jpg" width="8" height="8" alt=""></td>
              <td bgcolor="f7e39f"><font color="#333333" face="Arial, Helvetica, sans-serif"><strong>Inhaltsverzeichnis </strong></font></td>
              <td background="boxtopright.jpg"><img src="boxtopright.jpg" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="boxdividerleft.jpg" width="8" height="13" alt=""></td>
              <td background="boxdivider.jpg"><img src="boxdivider.jpg" alt="" width="8" height="13"></td>
              <td><img src="boxdividerright.jpg" width="8" height="13" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
              <td bgcolor="#FFFFFF" class="V10">
                <?php include("az.html"); ?>
              </td>
              <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
              <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
              <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
            </tr>
          </table>
        </td>
        <td rowspan="2" valign="top">          <table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr valign="top" align="left">
            <td width="8"><img src="boxtopleftcorner.jpg" width="8" height="8" alt=""></td>
            <td background="boxtop.jpg"><img src="boxtop.jpg" alt="" width="8" height="8"></td>
            <td width="8"><img src="boxtoprightcorner.jpg" width="8" height="8" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td background="boxtopleft.jpg" width="8"><img src="boxtopleft.jpg" width="8" height="8" alt=""></td>
            <td bgcolor="f7e39f">
              <h1><font color="#333333" face="Arial, Helvetica, sans-serif">Reizdarm</font></h1>
            </td>
            <td background="boxtopright.jpg" width="8"><img src="boxtopright.jpg" width="8" height="8" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td width="8"><img src="boxdividerleft.jpg" width="8" height="13" alt=""></td>
            <td background="boxdivider.jpg"><img src="boxdivider.jpg" alt="" width="8" height="13"></td>
            <td width="8"><img src="boxdividerright.jpg" width="8" height="13" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td width="8" height="905" background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
            <td bgcolor="#FFFFFF" class="L12">
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="48%"><span class="&uuml;berschrift"><img src="images-gesundheit-praevention/caritas_mannheim_iconpunkt.gif" width="12" height="12"> Der
                      Begriff</span><span class="&uuml;berschrift"> Reizdarm:</span><span class="text"></span></td>
                  <td valign=middle width="52%"><div align="right"><img src="images-gesundheit-praevention/caritas_mannheim_image_pdf.jpg" width="30" height="30"> <span class="link"><font color="#000000">Als
                          PDF-Datei ausgeben</font></span></div>
                  </td>
                </tr>
              </table>
              <p><span class="text">    <img SRC="images-gesundheit-praevention/reizdarm_neurolab.jpg" alt="Der Reizdarm" name="Ehrenamt" width="250" height="367" hspace="10" vspace="10" align="right" style="border: double 4px #009999">    Der
                  Reizdarm, auch Colon irritable oder Reizdarm-Syndrom (RDS)
                  genannt, gilt als funktionelle St&ouml;rung des Darms oder
                  Motilit&auml;tsst&ouml;rung, also St&ouml;rung der Darm-Motorik,
                  und geht mit teils massiven Beschwerden im Magen-Darm-Bereich
                  einher. </span></p>
              <p class="text"><span class="&uuml;berschrift"><img src="images-gesundheit-praevention/caritas_mannheim_iconpunkt.gif" width="12" height="12"> Symptome
                  des Reizdarms:</span><br>
                            <br>
        Reizdarm-Patienten leiden meist unter Bl&auml;hungen, Verstopfungen im
        Wechsel mit Durchfall sowie Bauchschmerzen. K&ouml;rperliche Belastungen
        und Stress k&ouml;nnen die Schmerzen im Darm noch verst&auml;rken. <br>
        Zum Teil leiden die Betroffenen auch unter Sodbrennen, &Uuml;belkeit
        und V&ouml;llegef&uuml;hl. Man spricht dann auch von einem Reizmagen.
        Dar&uuml;ber hinaus treten teilweise Beschwerden im R&uuml;cken, im Kopf
        und in den Gliedern auf. Es kommt zu Schlafst&ouml;rungen und Nervosit&auml;t. </p>
              <p class="text"><span class="&uuml;berschrift"><img src="images-gesundheit-praevention/caritas_mannheim_iconpunkt.gif" width="12" height="12"> Ursachen
                  des Reizdarms:</span><br>
                            <br>
        Im Bauchraum gibt es ein autonomes Nervensystem, bestehend aus &uuml;ber
        100 Millionen Nervenzellen. Man spricht von einem &#8222;Bauchhirn&#8220;.
        Bauchhirn und Kopfhirn kommunizieren st&auml;ndig, um die Darmaktivit&auml;ten
        mit den &uuml;brigen K&ouml;rperfunktionen in Einklang zu bringen. Dazu
        z&auml;hlt auch die Schmerzwahrnehmung im Magen-Darm-Bereich, die bei
        Reizdarm-Patienten gest&ouml;rt zu sein scheint. Auch wenn im restlichen
        Organismus keine Schmerz&uuml;berempfindlichkeit besteht, kann es bei
        Reizdarm-Betroffenen dazu kommen, dass normale Darmaktivit&auml;ten als
        schmerzhaft empfunden werden. </p>
              <p><span class="&uuml;berschrift"><img src="images-gesundheit-praevention/caritas_mannheim_iconpunkt.gif" width="12" height="12"> Folgen
                  des Reizdarms, wenn nicht behandelt:<br>
                </span><span class="text"><br>
        Die Beschwerden durch Reizdarm und unter Umst&auml;nden Reizmagen halten
        an und stellen f&uuml;r den Betroffenen eine teils massive Belastung
        dar. Zus&auml;tzlich leiden viele Reizdarm-Patienten unter dem Unverst&auml;ndnis
        anderer und dem Gef&uuml;hl, an einer eingebildeten Krankheit zu leiden,
        da keine organischen Ursachen gefunden werden k&ouml;nnen. <br>
        Die Betroffenen sind zudem meist zwischen 20 und 50 Jahre alt und damit
        beruflichen und famili&auml;ren Belastungen ausgesetzt. Oftmals sind
        die Beschwerden so stark, dass die Betroffenen krank geschrieben werden
        m&uuml;ssen. So ist der Schaden, den auch die Volkswirtschaft durch den
        Colon irritable erleidet, nicht unerheblich. <br>
                                        <br>
                </span><span class="&uuml;berschrift"><img src="images-gesundheit-praevention/caritas_mannheim_iconpunkt.gif" width="12" height="12"> Wer
                ist vom Reizdarm betroffen: </span><span class="text"><br>
                <br>
                </span><span class="text">Das Reizdarm-Syndrom betrifft in Deutschland
                etwa f&uuml;nf Millionen Patienten. Frauen sind h&auml;ufiger
                betroffen als M&auml;nner. Die Dunkelziffer der Reizdarm-Erkrankung
                liegt h&ouml;her, da zum einen viele Betroffene keinen Arzt aufsuchen,
                und zum anderen diese Diagnose teilweise nicht richtig getroffen
                wird. Das Reizdarm-Syndrom betrifft in Deutschland etwa f&uuml;nf
                Millionen Patienten, die meist zwischen 20 und 50 Jahre alt sind.
                Frauen sind h&auml;ufiger betroffen als M&auml;nner. Die Dunkelziffer
                der Reizdarm-Erkrankung liegt jedoch h&ouml;her. <br> 
                </span><span class="&uuml;berschrift">
                  </span><span class="text">
                    <br>
                  </span><span class="&uuml;berschrift"><img src="images-gesundheit-praevention/caritas_mannheim_iconpunkt.gif" width="12" height="12"> Pr&auml;ventionsm&ouml;glichkeiten: </span><span class="text"><br>
                    <br>
                    </span><span class="text">Neben Stressabbau wird die Vermeidung
                    bestimmter Nahrungsmittelbestandteile und Medikamente empfohlen,
                    darunter Histamin, Laktose und Fructose. <br>
                    </span><span class="text">
                    <br>
                    </span><span class="&uuml;berschrift"><img src="images-gesundheit-praevention/caritas_mannheim_iconpunkt.gif" width="12" height="12"> Diagnoseformen
                    des Reizdarms: </span><span class="text"><br>
                    <br>
                    </span><span class="text">Da aufgrund der Symptome meist
                    zuerst an eine Entz&uuml;ndung oder einen Tumor im Magen-Darm-Bereich
                    gedacht wird, findet oftmals zum Ausschluss eine ganze Reihe
                    von Untersuchungen, beginnend mit Ultraschall &uuml;ber Magen-Darm-Spieglung
                    bis hin zu einer Computer-Tomografie, statt. <br>
                    Sinnvoll erscheint es deshalb, bei Reizdarm-Verdacht zuerst
                    die Serotonin-Konzentration im Vergleich zu anderen Botenstoffen
                    zu messen. Dazu setzt ANT&#8226;OX unter anderem das NeuroStress-Profil
                    ein. <br>
                    <br>
</span><span class="&uuml;berschrift"><img src="images-gesundheit-praevention/caritas_mannheim_iconpunkt.gif" width="12" height="12"> therapiem&ouml;glichkeiten: </span><span class="text"><br>
                <br>
                </span><span class="text">Zur Behandlung eines Reizdarms kommen
                insbesondere Substanzen in Betracht, die den Serotonin-Haushalt
                im K&ouml;rper beeinflussen k&ouml;nnen, darunter SSRIs (Selektive
                Serotonin Wiederaufnahmehemmer), um den Serotonin-Spiegel wieder
                zu normalisieren. <br>
                Die weitere Behandlung besteht in der Regel aus einer sinnvollen
                Kombination von Einzelma&szlig;nahmen. Man spricht von einem
                Vier-S&auml;ulen-Programm, bestehend aus einer Ern&auml;hrungsumstellung,
                psychologischer Hilfe, bestimmten Medikamenten sowie einem Fitness-Programm
                f&uuml;r K&ouml;rper und Geist. <br>
                Bei der Ern&auml;hrungsumstellung geht es um die Vermeidung der
                ausl&ouml;senden Substanzen und Nahrungsmittel, die zuvor eindeutig
                identifiziert werden m&uuml;ssen. <br>
                Ziel der Medikamentenbehandlung ist der Aufbau einer intakten
                Darmflora. Gegen die Darmbeschwerden haben sich als pflanzliches
                Mittel Flohsamenschalen bew&auml;hrt. <br>
                Ebenfalls positiv wirkt sich ein gesundes Gleichgewicht zwischen
                Bewegung und Entspannung aus, zumal durch sportliche Bet&auml;tigung
                eine Wirkung auf die Darmt&auml;tigkeit stattfindet. </span><span class="text"><br>
    </span><span class="text"><br>
                    </span><span class="&uuml;berschrift"><img src="images-gesundheit-praevention/caritas_mannheim_iconpunkt.gif" width="12" height="12"> WEr
                  behandelt:</span><span class="text"> </span></p>
              <p><span class="text">&Auml;rzte im<a href="http://www.neurolab.eu/index.php?option=com_google_maps&Itemid=39" target="_blank"> Partnernetzwerk von Neurolab.
                  Klicken Sie hier f&uuml;r weitere Informationen.</a>
                  </span></p>              
              <p align="right"><font size="2"><span class="text"><b><br>
                  </b></span><b><br>
                  </b><font size="4">&copy; <a href="http://www.neurolab.eu" target="_blank">www.neurolab.eu</a></font></font></p></td>
            <td background="boxright.gif" width="8"><img src="boxright.gif" width="8" height="8" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td width="8"><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
            <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
            <td width="8"><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td width="100" height="1374" align="left" valign="top">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
</body>
</html>
