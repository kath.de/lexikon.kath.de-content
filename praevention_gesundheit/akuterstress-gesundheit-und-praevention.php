<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<head profile="http://dublincore.org/documents/dcq-html/">

<link rel="schema.DC" href="http://purl.org/dc/elements/1.1/">
<link rel="schema.DCTERMS" href="http://purl.org/dc/terms/">
<link href="caritas_mannheim.css" rel="stylesheet" type="text/css">

<title>akuter Stress :: Gesundheit &amp; Pr&auml;vention ::</title>
<meta name="robots" content="index,follow,all" />
<meta name="description" content="akuter Stress">
<meta name="author" content="Neurolab.eu">
<meta name="keywords" content="akuter Stress">


<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="DC.title" content="akuter Stress">

<meta name="DC.creator" content="akuter Stress">
<meta name="DC.subject" content="akuter Stress">
<meta name="DC.description" content="akuter Stress">
<meta name="DC.publisher" content="Neurolab">
<meta name="DC.contributor" content="akuter Stress">
<meta name="DC.date" content="2007-05-15T08:49:37+02:00" scheme="DCTERMS.W3CDTF">
<meta name="DC.type" content="Text" scheme="DCTERMS.DCMIType">
<meta name="DC.format" content="text/html" scheme="DCTERMS.IMT">
<meta name="DC.identifier"
      content="http://www.kath.de/lexikon/praevention_gesundheit/akuterstress-gesundheit-und-praevention.php"
      scheme="DCTERMS.URI">
<meta name="DC.source"
      content="http://www.w3.org/TR/html401/struct/global.html#h-7.4.4"
      scheme="DCTERMS.URI">
<meta name="DC.language" content="de" scheme="DCTERMS.RFC3066">
<meta name="DC.relation" content="http://dublincore.org/" scheme="DCTERMS.URI">
<meta name="DC.coverage" content="Munich" scheme="DCTERMS.TGN">
<meta name="DC.rights" content="Alle Rechte liegen beim Autor">
</head>

<body background="images-gesundheit-praevention/neurolab-background-org.jpg" link="#ED5105" vlink="#666666" alink="#FF0000">
<table width="900" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="6">
      <tr>
        <td width="216" align="left" valign="top" height="3">
          <table width="216" border="0" cellpadding="0" cellspacing="0">
            <tr valign="top" align="left">
              <td width="8"><img src="boxtopleftcorner.jpg" width="8" height="8" alt=""></td>
              <td width="200" background="boxtop.jpg"><img src="boxtop.jpg" alt="" width="8" height="8"></td>
              <td width="8"><img src="boxtoprightcorner.jpg" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="boxtopleft.jpg"><img src="boxtopleft.jpg" width="8" height="8" alt=""></td>
              <td bgcolor="f7e39f"><p><b><font color="#333333" face="Arial, Helvetica, sans-serif">Gesundheit
                      &amp; Pr&auml;vention </font></b></p>
              </td>
              <td background="boxtopright.jpg"><img src="boxtopright.jpg" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="boxdividerleft.jpg" width="8" height="13" alt=""></td>
              <td background="boxdivider.jpg"><img src="boxdivider.jpg" alt="" width="8" height="13"></td>
              <td><img src="boxdividerright.jpg" width="8" height="13" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
              <td bgcolor="#FFFFFF">
                <?php include("logo.html"); ?>
              </td>
              <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
              <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
              <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
            </tr>
          </table>
          <br>
          <table width="216" border="0" cellpadding="0" cellspacing="0">
            <tr valign="top" align="left">
              <td width="8"><img src="boxtopleftcorner.jpg" width="8" height="8" alt=""></td>
              <td width="200" background="boxtop.jpg"><img src="boxtop.jpg" alt="" width="8" height="8"></td>
              <td width="8"><img src="boxtoprightcorner.jpg" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="boxtopleft.jpg"><img src="boxtopleft.jpg" width="8" height="8" alt=""></td>
              <td bgcolor="f7e39f"><font color="#333333" face="Arial, Helvetica, sans-serif"><strong>Inhaltsverzeichnis </strong></font></td>
              <td background="boxtopright.jpg"><img src="boxtopright.jpg" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="boxdividerleft.jpg" width="8" height="13" alt=""></td>
              <td background="boxdivider.jpg"><img src="boxdivider.jpg" alt="" width="8" height="13"></td>
              <td><img src="boxdividerright.jpg" width="8" height="13" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
              <td bgcolor="#FFFFFF" class="V10">
                <?php include("az.html"); ?>
              </td>
              <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
              <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
              <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
            </tr>
          </table>
        </td>
        <td width="530" rowspan="2" valign="top">          <table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr valign="top" align="left">
            <td width="8"><img src="boxtopleftcorner.jpg" width="8" height="8" alt=""></td>
            <td background="boxtop.jpg"><img src="boxtop.jpg" alt="" width="8" height="8"></td>
            <td width="8"><img src="boxtoprightcorner.jpg" width="8" height="8" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td background="boxtopleft.jpg" width="8"><img src="boxtopleft.jpg" width="8" height="8" alt=""></td>
            <td bgcolor="f7e39f">
              <h1><font color="#333333" face="Arial, Helvetica, sans-serif">akuter
                  Stress</font></h1>
            </td>
            <td background="boxtopright.jpg" width="8"><img src="boxtopright.jpg" width="8" height="8" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td width="8"><img src="boxdividerleft.jpg" width="8" height="13" alt=""></td>
            <td background="boxdivider.jpg"><img src="boxdivider.jpg" alt="" width="8" height="13"></td>
            <td width="8"><img src="boxdividerright.jpg" width="8" height="13" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td width="8" height="905" background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
            <td bgcolor="#FFFFFF" class="L12">
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="48%"><span class="&uuml;berschrift"><img src="images-gesundheit-praevention/caritas_mannheim_iconpunkt.gif" width="12" height="12"> Der
                      Begriff</span><span class="&uuml;berschrift"> aktuter stress:</span><span class="text"></span></td>
                  <td valign=middle width="52%"><div align="right"> <span class="link"></span></div>
                  </td>
                </tr>
              </table>
              <p><span class="text">    <img SRC="images-gesundheit-praevention/akuterstress_neurolab.jpg" alt="akuter Stress" name="akuter Stress" hspace="10" vspace="10" align="right" style="border: double 4px #009999">    Stress
                  ist in seiner akuten, kurzfristigen Form eine nat&uuml;rliche
                  k&ouml;rperliche und psychische Reaktion. K&ouml;rper und Psyche
                  reagieren damit auf &auml;u&szlig;ere Einfl&uuml;sse mit dem
                  Ziel einer Anpassung und m&ouml;glichen Abwehr. H&auml;ufige
                  und lang andauernde Stressbelastungen (<a href="chronischerstress-gesundheit-und-praevention.php" target="_blank">chronischer
                  Stress</a>) k&ouml;nnen
                  jedoch zu verschiedenen gesundheitlichen Problemen f&uuml;hren.
                  Stress hat im &uuml;blichen Wortgebrauch eine negative Bedeutung.
                  Das wird dieser K&ouml;rperreaktion nicht gerecht. Man spricht
                  n&auml;mlich von Eustress, wenn Stress durch positive Signale
                  ausgel&ouml;st wird. <strong>Eustress</strong> aktiviert und
                  macht leistungsbereiter. Stress, der durch negative Signale
                  entsteht, nennt sich dagegen
                  <strong>Distress</strong>.</span></p>
              <p class="text"><span class="&uuml;berschrift"><img src="images-gesundheit-praevention/caritas_mannheim_iconpunkt.gif" width="12" height="12"> Symptome
                  des aktuten stress:</span><br>
                            <br>
        Zu den Symptomen des akuten Stress geh&ouml;ren unter anderem              
              <ul><li> <span class="text">Aktivierung des Herz-Kreislaufsystems (beschleunigter Herzschlag)</span></li> 
<span class="text"><br>
</span>
<li class="text"> Schwei&szlig;ausbr&uuml;che </li> 
<span class="text"><br>
</span>
<li class="text">Muskelanpassung </li> 
<span class="text"><br>
</span>
<li class="text"> Hormonaussch&uuml;ttung</li>  
<span class="text"><br>
</span>
<li class="text"> Schnelle Atmung</li>  
<span class="text"><br>
</span>
<li class="text"> Erweiterte Pupillen </li> 
<span class="text"><br>
</span>
<li class="text"> Stoffwechselanpassung zur Energiebereitstellung</li>  
<span class="text"><br>
</span>
<li class="text"> Aktivierung des Immunsystems</li>  
<span class="text"><br>
</span>
<li class="text"> Erh&ouml;hte Konzentrationsf&auml;higkeit </li> 
<span class="text">
</p>
</span>        </ul>
              <p class="text"><span class="&uuml;berschrift"><img src="images-gesundheit-praevention/caritas_mannheim_iconpunkt.gif" width="12" height="12"> Ursachen
                  des aktuten stress:</span><br>
                            <br>
        Die Faktoren, die Stress verursachen k&ouml;nnen, werden Stressoren genannt.
        Als Stressoren kommen individuell unterschiedlich in Betracht:               
              <ul>
        <li class="text"> Zeitdruck </li>
        <span class="text"><br>
        </span>
        <li class="text"> Reiz&uuml;berflutung </li>
        <span class="text"><br>
        </span>
        <li class="text"> berufliche und oder private &Uuml;berforderung,
            Leistungsdruck </li>
        <span class="text"><br>
        </span>
        <li class="text"> die eigene oder famili&auml;re Krankengeschichte </li>
        <span class="text"><br>
        </span>
        <li class="text"> starke L&auml;rm- und Lichteinwirkung </li>
        <span class="text"><br>
        </span>
        <li class="text"> sowie weitere physikalische und chemische Reize</li>
		</ul>
              <p><span class="&uuml;berschrift"><img src="images-gesundheit-praevention/caritas_mannheim_iconpunkt.gif" width="12" height="12"> Folgen
                  des aktuten stress, wenn nicht behandelt:<br>
                </span><span class="text"><br>
        Lang andauernde Stressbelastungen k&ouml;nnen zu<a href="chronischerstress-gesundheit-und-praevention.php" target="_blank"> chronischem
        Stress</a> f&uuml;hren. <br>
                                        <br>
                </span><span class="&uuml;berschrift"><img src="images-gesundheit-praevention/caritas_mannheim_iconpunkt.gif" width="12" height="12"> Wer
                ist von aktutem stress betroffen: </span><span class="text"><br>
                <br>
                </span><span class="text">Akuter Stress tritt als Reaktion auf
                kurzfristige Belastungen bei jedem Menschen in unterschiedlichem
                Umfang auf. Was von einer Person als Stress wahrgenommen wird,
                ist individuell verschieden. Akuter Stress betrifft die gesamte
                Bev&ouml;lkerung, allerdings in individuell unterschiedlichem
                Ma&szlig;e. Nach aktuellen Erhebungen f&uuml;hlen sich &uuml;ber
                45 % der Bev&ouml;lkerung stark gestresst. <br> 
                </span><span class="&uuml;berschrift">
                  </span><span class="text">
                    <br>
                  </span><span class="&uuml;berschrift"><img src="images-gesundheit-praevention/caritas_mannheim_iconpunkt.gif" width="12" height="12"> Pr&auml;ventionsm&ouml;glichkeiten: </span>
              <span class="text">Akuter Stress geh&ouml;rt zu den
                    normalen Lebensvorg&auml;ngen. Es kommt auf den richtigen
                    Ausgleich an. Stress ist notwendig, auch um sich wohl zu
                    f&uuml;hlen. Zugleich muss der Stress wieder abgebaut werden.
                    Hierzu empfehlen sich verschiedene Entspannungstechniken
                    wie zum Beispiel 
                    <ul><li> Bewegung und Sport</li> <br>
                    <li> Meditation </li><br>
                    <li> Progressive Muskelentspannung nach Jacobsen </li><br>
                    <li> Autogenes Training</li> <br>
                    <li> Yoga </li></ul>
                    Auch die am besten geeignete Entspannungstechnik ist individuell
                    verschieden. <br>
</span><span class="text"> <br>
</span><span class="&uuml;berschrift"><img src="images-gesundheit-praevention/caritas_mannheim_iconpunkt.gif" width="12" height="12"> was
passiert im k&ouml;rper / in der zelle: </span></p>
              <p><span class="text">                    </span><span class="text">
                    Die Stressreaktion beginnt mit der Aussch&uuml;ttung der
                    Botenstoffe Noradrenalin und CRH (Corticotropin Releasing
                    Hormone). Durch diese beiden Substanzen werden die anderen
                    Stresshormone und Botenstoffen gesteuert. Die Feinkoordination
                    der Stressreaktion obliegt Dopamin, das Serotonin einbindet.
                    Zusammen mit Glutamat, PEA und Histamin steigern Noradrenalin
                    und Dopamin die Aktivit&auml;t des zentralen Nervensystems
                    (ZNS), w&auml;hrend Serotonin gemeinsam mit GABA f&uuml;r
                    eine D&auml;mpfung sorgt. Die gleichzeitige Aktivierung von
                    anregenden und d&auml;mpfenden Prozessen ist sehr wichtig
                    f&uuml;r das Gleichgewicht bei der Stressreaktion. So wird
                    im Normalfall verhindert, dass eine der Komponenten, die
                    anregende oder die d&auml;mpfende, langfristig &uuml;berwiegt,
                    sondern dass nach einer gewissen Zeit wieder der Normalzustand
                    eintreten kann. Die notwendige Aktivierung des Herz-Kreislaufsystems,
                    die Stoffwechselanpassung zur Energiebereitstellung sowie
                    die Einbeziehung des Immunsystems und auch anderer Hormonsysteme
                    regeln Adrenalin und Cortisol. <br>
                    <br>
                    </span><span class="&uuml;berschrift"><img src="images-gesundheit-praevention/caritas_mannheim_iconpunkt.gif" width="12" height="12"> Diagnoseformen
                      des aktuten stress: </span><span class="text"><br>
                      <br>
                      </span><span class="text">Spezialisierte Untersuchungsprogramme
                      messen die Auswirkungen von Stress bei dem jeweiligen Patienten
                      feststellen zu k&ouml;nnen. Grundlegend ist das <a href="www.neurostress.eu" target="_blank">NEUROSTRESS</a>
                      Profil und das Programm <a href="www.neurostress.eu" target="_blank">NEUROSTRESS</a> basis. Diese wurden
                      von PD Dr. med. WP Bieger (Neurolab, M&uuml;nchen) entwickelt. 
                      <br>
                      <br>
                      </span><span class="&uuml;berschrift"><img src="images-gesundheit-praevention/caritas_mannheim_iconpunkt.gif" width="12" height="12"> therapiem&ouml;glichkeiten: </span><span class="text"><br>
                      <br>
                      </span><span class="text">Stress, der nur kurzfristig
                      anh&auml;lt, bedarf keiner Therapie, da er zu den normalen
                      Lebensvorg&auml;ngen geh&ouml;rt. Es empfehlen sich jedoch
                      vorbeugende Ma&szlig;nahmen (Pr&auml;vention), um die Wirkungen
                      von wiederholten akuten Stressbelastungen auf K&ouml;rper
                      und Psyche zu begrenzen und die Ausbildung von chronischen
                      Stressreaktionen nach M&ouml;glichkeit zu vermeiden. Diese
                      vorbeugenden Ma&szlig;nahmen bestehen vor allem in sportlicher
                      Bet&auml;tigung. Denn ein trainierter K&ouml;rper verarbeitet
                      Stress-Belastungen sehr viel besser.</span><span class="text"><br>
                  </span><span class="text"><br>
                    </span><span class="&uuml;berschrift"><img src="images-gesundheit-praevention/caritas_mannheim_iconpunkt.gif" width="12" height="12"> WEr
                  behandelt:</span><span class="text"> </span></p>
              <p><span class="text">&Auml;rzte im<a href="http://www.neurolab.eu/index.php?option=com_content&view=article&id=72&Itemid=148&#9001;=de" target="_blank"> Partnernetzwerk
                    von Neurolab.</a><br>
                  F&uuml;r weitere Informationen <a href="http://www.neurolab.eu/index.php?option=com_content&view=article&id=41&Itemid=89&#9001;=de" target="_blank">bitte hier klicken </a> </span></p>              
              <p align="right"><font size="2"><span class="text"><b><br>
                  </b></span><b><br>
                  </b><font size="4"><a href="http://www.neurolab.eu" target="_blank"><img src="neurolab_logo-1.jpg" width="120" height="77" border="0"></a><br>&copy;                  <a href="http://www.neurolab.eu" target="_blank">www.neurolab.eu</a></font></font></p>
              <p align="right"><font size="2" face="Arial, Helvetica, sans-serif">Bild: &copy; Franz
                  Pfluegl - Fotolia.com</font><br>
              </p></td>
            <td background="boxright.gif" width="8"><img src="boxright.gif" width="8" height="8" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td width="8"><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
            <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
            <td width="8"><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
          </tr>
        </table></td>
        <td width="120" rowspan="2" valign="top">
		
	<script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
/* Gesundheitsprävention */
google_ad_slot = "0595101902";
google_ad_width = 120;
google_ad_height = 600;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>


</td>
      </tr>
      <tr>
        <td width="216" height="1374" align="left" valign="top">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>



</body>
</html>
