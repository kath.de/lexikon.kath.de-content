<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<head profile="http://dublincore.org/documents/dcq-html/">

<link rel="schema.DC" href="http://purl.org/dc/elements/1.1/">
<link rel="schema.DCTERMS" href="http://purl.org/dc/terms/">
<link href="caritas_mannheim.css" rel="stylesheet" type="text/css">

<title>Migr&auml;ne :: Gesundheit &amp; Pr&auml;vention ::</title>
<meta name="robots" content="index,follow,all" />
<meta name="description" content="Migr�ne">
<meta name="author" content="Neurolab.eu">
<meta name="keywords" content="Migr�ne">


<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="DC.title" content="Migr�ne">

<meta name="DC.creator" content="Migr�ne">
<meta name="DC.subject" content="Migr�ne">
<meta name="DC.description" content="Migr�ne">
<meta name="DC.publisher" content="Neurolab.eu">
<meta name="DC.contributor" content="Neurolab.eu">
<meta name="DC.date" content="2007-05-15T08:49:37+02:00" scheme="DCTERMS.W3CDTF">
<meta name="DC.type" content="Text" scheme="DCTERMS.DCMIType">
<meta name="DC.format" content="text/html" scheme="DCTERMS.IMT">
<meta name="DC.identifier"
      content="http://www.kath.de/lexikon/praevention_gesundheit/migraene-gesundheit-und-praevention.php"
      scheme="DCTERMS.URI">
<meta name="DC.source"
      content="http://www.w3.org/TR/html401/struct/global.html#h-7.4.4"
      scheme="DCTERMS.URI">
<meta name="DC.language" content="de" scheme="DCTERMS.RFC3066">
<meta name="DC.relation" content="http://dublincore.org/" scheme="DCTERMS.URI">
<meta name="DC.coverage" content="Munich" scheme="DCTERMS.TGN">
<meta name="DC.rights" content="Alle Rechte liegen beim Autor">
</head>

<body background="images-gesundheit-praevention/neurolab-background-org.jpg" link="#ED5105" vlink="#666666" alink="#FF0000">
<table width="900" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="6">
      <tr>
        <td width="216" align="left" valign="top" height="3">
          <table width="216" border="0" cellpadding="0" cellspacing="0">
            <tr valign="top" align="left">
              <td width="8"><img src="boxtopleftcorner.jpg" width="8" height="8" alt=""></td>
              <td width="200" background="boxtop.jpg"><img src="boxtop.jpg" alt="" width="8" height="8"></td>
              <td width="8"><img src="boxtoprightcorner.jpg" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="boxtopleft.jpg"><img src="boxtopleft.jpg" width="8" height="8" alt=""></td>
              <td bgcolor="f7e39f"><p><b><font color="#333333" face="Arial, Helvetica, sans-serif">Gesundheit
                      &amp; Pr&auml;vention </font></b></p>
              </td>
              <td background="boxtopright.jpg"><img src="boxtopright.jpg" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="boxdividerleft.jpg" width="8" height="13" alt=""></td>
              <td background="boxdivider.jpg"><img src="boxdivider.jpg" alt="" width="8" height="13"></td>
              <td><img src="boxdividerright.jpg" width="8" height="13" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
              <td bgcolor="#FFFFFF">
                <?php include("logo.html"); ?>
              </td>
              <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
              <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
              <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
            </tr>
          </table>
          <br>
          <table width="216" border="0" cellpadding="0" cellspacing="0">
            <tr valign="top" align="left">
              <td width="8"><img src="boxtopleftcorner.jpg" width="8" height="8" alt=""></td>
              <td width="200" background="boxtop.jpg"><img src="boxtop.jpg" alt="" width="8" height="8"></td>
              <td width="8"><img src="boxtoprightcorner.jpg" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="boxtopleft.jpg"><img src="boxtopleft.jpg" width="8" height="8" alt=""></td>
              <td bgcolor="f7e39f"><font color="#333333" face="Arial, Helvetica, sans-serif"><strong>Inhaltsverzeichnis </strong></font></td>
              <td background="boxtopright.jpg"><img src="boxtopright.jpg" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="boxdividerleft.jpg" width="8" height="13" alt=""></td>
              <td background="boxdivider.jpg"><img src="boxdivider.jpg" alt="" width="8" height="13"></td>
              <td><img src="boxdividerright.jpg" width="8" height="13" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
              <td bgcolor="#FFFFFF" class="V10">
                <?php include("az.html"); ?>
              </td>
              <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
              <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
              <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
            </tr>
          </table>
        </td>
        <td width="530" rowspan="2" valign="top">          <table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr valign="top" align="left">
            <td width="8"><img src="boxtopleftcorner.jpg" width="8" height="8" alt=""></td>
            <td background="boxtop.jpg"><img src="boxtop.jpg" alt="" width="8" height="8"></td>
            <td width="8"><img src="boxtoprightcorner.jpg" width="8" height="8" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td background="boxtopleft.jpg" width="8"><img src="boxtopleft.jpg" width="8" height="8" alt=""></td>
            <td bgcolor="f7e39f">
              <h1><font color="#333333" face="Arial, Helvetica, sans-serif">Migr&auml;ne</font></h1>
            </td>
            <td background="boxtopright.jpg" width="8"><img src="boxtopright.jpg" width="8" height="8" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td width="8"><img src="boxdividerleft.jpg" width="8" height="13" alt=""></td>
            <td background="boxdivider.jpg"><img src="boxdivider.jpg" alt="" width="8" height="13"></td>
            <td width="8"><img src="boxdividerright.jpg" width="8" height="13" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td width="8" height="905" background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
            <td bgcolor="#FFFFFF" class="L12">
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="48%"><span class="&uuml;berschrift"><img src="images-gesundheit-praevention/caritas_mannheim_iconpunkt.gif" width="12" height="12"> Der
                      Begriff</span><span class="&uuml;berschrift"> Migr&auml;ne:</span><span class="text"></span></td>
                  <td valign=middle width="52%"><div align="right"> <span class="link"></span></div>
                  </td>
                </tr>
              </table>
              <p><span class="text">    <img SRC="images-gesundheit-praevention/migraene-neurolab.jpg" alt="Migraene" name="Migraene" hspace="10" vspace="10" align="right" style="border: double 4px #009999">    Bei
                  Migr&auml;ne handelt es sich um eine neurologische Erkrankung,
                  die durch anfallsartige Kopfschmerzen in Erscheinung tritt,
                  die h&auml;ufig von &Uuml;belkeit, Lichtempfindlichkeit, Ger&auml;uschempfindlichkeit
                  sowie weiteren Symptomen begleitet werden. </span></p>
              <p class="text"><span class="&uuml;berschrift"><img src="images-gesundheit-praevention/caritas_mannheim_iconpunkt.gif" width="12" height="12"> Symptome
                  der Migr&auml;ne:</span><br>
                            <br>
        Migr&auml;ne-Patienten leiden unter pl&ouml;tzlich auftretenden, meist
        pulsierenden Kopfschmerzen und sind w&auml;hrend des Migr&auml;ne-Anfalls
        sehr empfindlich gegen&uuml;ber Ger&auml;uschen und Licht. Weitere Symptome
        des Anfalls sind &Uuml;belkeit und Erbrechen sowie Wahrnehmungsst&ouml;rungen. <br>
        Bei etwa zehn Prozent der Migr&auml;ne-Patienten kommt es zu einer besonderen
        Vorphase des Migr&auml;ne-Anfalls, der sogenannten Aura. Dabei treten
        verschiedene neurologische St&ouml;rungen auf, wie Sehst&ouml;rungen,
        Gleichgewichtsprobleme, Gef&uuml;hlsst&ouml;rungen und L&auml;hmungserscheinungen. <br>
        Bei einer Migr&auml;ne ohne Aura treten die stechenden Kopfschmerzen
        meist morgens auf und werden unter Belastung st&auml;rker. In der Regel
        sind Migr&auml;ne-Anf&auml;lle von kurzer Dauer, sie k&ouml;nnen aber
        auch &uuml;ber zwei bis drei Tage lang anhalten. <br>
        Viele Migr&auml;ne-Patienten leiden gleichzeitig unter einer Nahrungsmittelunvertr&auml;glichkeit
        oder selten auch unter einer echten Nahrungsmittelallergie. </p>
              <p class="text"><span class="&uuml;berschrift"><img src="images-gesundheit-praevention/caritas_mannheim_iconpunkt.gif" width="12" height="12"> Ursachen
                  der Migr&auml;ne:</span><br>
                            <br>
        Nach aktuellen Forschungsergebnissen liegen bei Migr&auml;ne-Attacken
        erweiterte und entz&uuml;ndete Blutgef&auml;&szlig;e in der Hirnhaut
        vor. Fasern eines bestimmten Hirnnervs (Trigeminussystem) scheinen intensiv
        gereizt zu sein und sch&uuml;tten Entz&uuml;ndungsbotenstoffe aus, die
        die Blutgef&auml;&szlig;e in der Hirnhaut anschwellen lassen. <br>
        Die Nervenzellen kommen in einen Zustand h&ouml;herer Erregbarkeit, die
        Kopfschmerzbereitschaft steigt deutlich an. Da die Gef&auml;&szlig;w&auml;nde
        durchl&auml;ssiger werden, tritt vermehrt Fl&uuml;ssigkeit aus, das umliegende
        Gewebe schwillt an, und die Nerven werden noch st&auml;rker gereizt.
        Dadurch schaukelt sich der Schmerz hoch, es kommt zu der Migr&auml;ne-Attacke.
        Warum es zu den beschriebenen Entz&uuml;ndungszust&auml;nden kommt, ist
        nicht vollst&auml;ndig gekl&auml;rt. Eine wichtige Rolle scheint aber
        der Botenstoff Serotonin zu spielen. <br>
        Bestimmte Faktoren k&ouml;nnen eine Attacke ausl&ouml;sen oder diese
        beg&uuml;nstigen. Diese Faktoren sind sehr vielschichtig und k&ouml;nnen
        in der Ern&auml;hrung, in Stressbelastungen, in Wetterverh&auml;ltnissen
        und anderen Umweltreizen gesehen werden. <br>
        Au&szlig;erdem gibt es disponierende, vererbte Faktoren, die das Auftreten
        der Migr&auml;ne beg&uuml;nstigt. Dies zeigt die famili&auml;re H&auml;ufung
        der Erkrankung. </p>
              <p><span class="&uuml;berschrift"><img src="images-gesundheit-praevention/caritas_mannheim_iconpunkt.gif" width="12" height="12"> Folgen
                  der Migr&auml;ne, wenn nicht behandelt:<br>
                </span><span class="text"><br>
        Migr&auml;ne kann nicht nur bei Eintreten eines Anfalls behandelt werden,
        sondern man kann vorbeugen, nicht zuletzt durch Bewegung. Eine genauere
        Untersuchung zur Bestimmung der ausl&ouml;senden Faktoren und der Verzicht
        auf bestimmte, individuell unterschiedlicher Reizstoffe k&ouml;nnen die
        Zahl der Anf&auml;lle deutlich reduzieren. Durch die fr&uuml;hzeitige
        Einnahme von Serotonin lassen die Anf&auml;lle vielfach mildern. <br>
                                        <br>
                </span><span class="&uuml;berschrift"><img src="images-gesundheit-praevention/caritas_mannheim_iconpunkt.gif" width="12" height="12"> Wer
                ist von Migr&auml;ne betroffen: </span><span class="text"><br>
                <br>
                </span><span class="text">Migr&auml;ne ist in der Bev&ouml;lkerung
                weit verbreitet. Bis zu 18 Prozent aller Frauen und sechs Prozent
                aller M&auml;nner, insbesondere im Alter zwischen 30 und 50 Jahren,
                sind davon betroffen. Dabei kann eine famili&auml;re H&auml;ufung
                von Migr&auml;ne-F&auml;llen beobachtet werden, was die Vermutung
                eines genetischen Einflusses nahe legt. Bis zu 18 Prozent aller
                Frauen und sechs Prozent aller M&auml;nner, insbesondere im Alter
                zwischen 30 und 50 Jahren, sind von Migr&auml;ne betroffen. <br> 
                </span><span class="&uuml;berschrift">
                  </span><span class="text">
                    <br>
                  </span><span class="&uuml;berschrift"><img src="images-gesundheit-praevention/caritas_mannheim_iconpunkt.gif" width="12" height="12"> Pr&auml;ventionsm&ouml;glichkeiten: </span><span class="text"><br>
                    <br>
                    </span><span class="text">Zur Vorbeugung gegen Migr&auml;ne-Anf&auml;lle
                    gilt es, die ausl&ouml;senden Faktoren festzustellen, um
                    diese nach M&ouml;glichkeit zu vermeiden. Daneben sind oft
                    das Erlernen und Aus&uuml;ben von Entspannungstechniken und
                    die Stressbew&auml;ltigung hilfreich. <br>
                    Bei akuten Anf&auml;llen haben sich ruhige und abgedunkelte
                    R&auml;ume als R&uuml;ckzugsm&ouml;glichkeit bew&auml;hrt. <br>
                    Bei verschiedenen Betroffenen kann es sinnvoll sein, den
                    Konsum von Kaffee, Rotwein oder Schokolade genau zu kontrollieren.
                    Andere Patienten erleiden Attacken, wenn sie zum Beispiel
                    eher wenig Kaffee trinken. Dies ist individuell unterschiedlich. <br>
</span><span class="text"> <br>
</span><span class="&uuml;berschrift"><img src="images-gesundheit-praevention/caritas_mannheim_iconpunkt.gif" width="12" height="12"> was
passiert im k&ouml;rper / in der zelle: </span></p>
              <p><span class="text">                    </span><span class="text">
                    Als eine wesentliche Ursache vermutet die aktuelle Forschung
                        eine St&ouml;rung des Serotonin-Gleichgewichtes im Gehirn.
                        Serotonin hemmt die Schmerzentwicklung. Wie die famili&auml;re
                        H&auml;ufung von Migr&auml;ne vermuten l&auml;sst, d&uuml;rfte
                        eine genetisch bedingte Ver&auml;nderung dazu f&uuml;hren,
                        dass Serotonin nicht in der gewohnten Weise die Schmerzausbreitung
                        hemmen kann. Zus&auml;tzlich kann man eine allergische
                        Reaktion des Immunsystems nachweisen. Weiterhin kann
                        man feststellen, dass die Konzentration von PEA bei Migr&auml;ne-Attacken
                        deutlich erh&ouml;ht ist. <br>
                    <br>
                    </span><span class="&uuml;berschrift"><img src="images-gesundheit-praevention/caritas_mannheim_iconpunkt.gif" width="12" height="12"> Diagnoseformen
                      der Migr&auml;ne: </span><span class="text"><br>
                      <br>
                      </span><span class="text">Bei einer Diagnose achtet der
                      Arzt genau auf die Unterschiede der Migr&auml;ne zu den
                      anderen Kopfschmerzarten wie dem Spannungskopfschmerz oder
                      Kopfschmerzen durch zu hohen Blutdruck, Blutarmut, Schilddr&uuml;senst&ouml;rungen
                      oder bestimmte Medikamente. Bei der abschlie&szlig;enden
                      Diagnose kommen in der Regel Verfahren wie EEG, Computer-Tomographie
                      oder Kernspin-Tomographie zum Einsatz, um andere neurologische
                      Erkrankungen auszuschlie&szlig;en. Insbesondere muss ein
                      Verdacht auf Entz&uuml;ndungen oder Tumore im Kopfbereich
                      gepr&uuml;ft werden. <br>
                      Neurolab setzt bei der Untersuchung von Migr&auml;ne-Patienten
                      das erweiterte NeuroStress-Profil ein, das insbesondere
                      durch die Messung der Konzentrationen von PEA, GABA, Histamin
                      und Glycin erg&auml;nzt wird. <br>
                      <br>
                      <br>
                      </span><span class="&uuml;berschrift"><img src="images-gesundheit-praevention/caritas_mannheim_iconpunkt.gif" width="12" height="12"> therapiem&ouml;glichkeiten: </span><span class="text"><br>
                      <br>
                      </span><span class="text">Migr&auml;ne muss nicht als
                      Schicksal hingenommen werden. Eine Vorstufe des Botenstoffs
                      Serotonin kann die Einnahme von Schmerztabletten &uuml;berfl&uuml;ssig
                      machen. Neurolab bietet dazu eine sogenannte Substitutionsbehandlung
                      mit Aminos&auml;ure-Vorstufen sowie eine Therapie-M&ouml;glichkeit
                      auf Basis bestimmter, aus der Depressionstherapie bekannter
                      Medikamente (SNRIs, Serotonin- und Noradrenalin-Reuptake-Inhibitoren).</span><span class="text"><br>
                  </span><span class="text"><br>
                    </span><span class="&uuml;berschrift"><img src="images-gesundheit-praevention/caritas_mannheim_iconpunkt.gif" width="12" height="12"> WEr
                  behandelt:</span><span class="text"> </span></p>
              <p><span class="text">&Auml;rzte im<a href="http://www.neurolab.eu/index.php?option=com_content&view=article&id=72&Itemid=148&#9001;=de" target="_blank"> Partnernetzwerk
                  von Neurolab.</a></span><br>
                  <font face="Arial, Helvetica, sans-serif">F&uuml;r weitere
                  Informationen <a href="http://www.neurolab.eu/index.php?option=com_content&view=article&id=104&Itemid=76&#9001;=de" target="_blank">bitte hier klicken </a></font> </p>              
              <p align="right"><font size="2"><span class="text"><b><br>
                  </b></span><b><br>
                  </b><font size="4"><a href="http://www.neurolab.eu" target="_blank"><img src="neurolab_logo-1.jpg" width="120" height="77" border="0"></a><br>&copy;                  <a href="http://www.neurolab.eu" target="_blank">www.neurolab.eu</a></font></font></p>
              <p align="right"><font size="2" face="Arial, Helvetica, sans-serif">Bild: &copy; Liv
                  Friis-larsen - 
                Fotolia.com</font><br>
              </p></td>
            <td background="boxright.gif" width="8"><img src="boxright.gif" width="8" height="8" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td width="8"><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
            <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
            <td width="8"><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
          </tr>
        </table></td>
        <td width="120" rowspan="2" valign="top">
		
	<script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
/* Gesundheitspr�vention */
google_ad_slot = "0595101902";
google_ad_width = 120;
google_ad_height = 600;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>


</td>
      </tr>
      <tr>
        <td width="216" height="1374" align="left" valign="top">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>



</body>
</html>
