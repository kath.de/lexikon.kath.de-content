<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<head profile="http://dublincore.org/documents/dcq-html/">

<link rel="schema.DC" href="http://purl.org/dc/elements/1.1/">
<link rel="schema.DCTERMS" href="http://purl.org/dc/terms/">
<link href="caritas_mannheim.css" rel="stylesheet" type="text/css">

<title>Aromahefe :: Das Weinlexikon :: Wein &amp; Co</title>
<meta name="robots" content="index,follow,all" />
<meta name="description" content="chronischer Stress">
<meta name="author" content="kath.de">
<meta name="keywords" content="chronischer Stress">


<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="DC.title" content="chronischer Stress">

<meta name="DC.creator" content="chronischer Stress">
<meta name="DC.subject" content="chronischer Stress">
<meta name="DC.description" content="chronischer Stress">
<meta name="DC.publisher" content="kath.de">
<meta name="DC.contributor" content="kath.de">
<meta name="DC.date" content="2007-05-15T08:49:37+02:00" scheme="DCTERMS.W3CDTF">
<meta name="DC.type" content="Text" scheme="DCTERMS.DCMIType">
<meta name="DC.format" content="text/html" scheme="DCTERMS.IMT">
<meta name="DC.identifier"
      content="http://www.kath.de/lexikon/weinlexikon/.php"
      scheme="DCTERMS.URI">
<meta name="DC.source"
      content="http://www.w3.org/TR/html401/struct/global.html#h-7.4.4"
      scheme="DCTERMS.URI">
<meta name="DC.language" content="de" scheme="DCTERMS.RFC3066">
<meta name="DC.relation" content="http://dublincore.org/" scheme="DCTERMS.URI">
<meta name="DC.coverage" content="Munich" scheme="DCTERMS.TGN">
<meta name="DC.rights" content="Alle Rechte liegen beim Autor">
</head>

<body background="images-weinlexikon-wein/weinlexikon-wein-background.jpg" link="#ED5105" vlink="#666666" alink="#FF0000">
<table width="900" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="6">
      <tr>
        <td width="216" align="left" valign="top" height="3">
          <table width="216" border="0" cellpadding="0" cellspacing="0">
            <tr valign="top" align="left">
              <td width="8"><img src="boxtopleftcorner.jpg" width="8" height="8" alt=""></td>
              <td width="200" background="boxtop.jpg"><img src="boxtop.jpg" alt="" width="8" height="8"></td>
              <td width="8"><img src="boxtoprightcorner.jpg" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="boxtopleft.jpg"><img src="boxtopleft.jpg" width="8" height="8" alt=""></td>
              <td bgcolor="b1cc1f"><p><b><font color="#FFFFFF" face="Arial, Helvetica, sans-serif">Das
                      Weinlexikon</font></b></p>
              </td>
              <td background="boxtopright.jpg"><img src="boxtopright.jpg" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="boxdividerleft.jpg" width="8" height="13" alt=""></td>
              <td background="boxdivider.jpg"><img src="boxdivider.jpg" alt="" width="8" height="13"></td>
              <td><img src="boxdividerright.jpg" width="8" height="13" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
              <td bgcolor="#ffffff">
                <?php include("logo.html"); ?>
              </td>
              <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
              <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
              <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
            </tr>
          </table>
          <br>
          <table width="216" border="0" cellpadding="0" cellspacing="0">
            <tr valign="top" align="left">
              <td width="8"><img src="boxtopleftcorner.jpg" width="8" height="8" alt=""></td>
              <td width="200" background="boxtop.jpg"><img src="boxtop.jpg" alt="" width="8" height="8"></td>
              <td width="8"><img src="boxtoprightcorner.jpg" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="boxtopleft.jpg"><img src="boxtopleft.jpg" width="8" height="8" alt=""></td>
              <td bgcolor="b1cc1f"><font color="#FFFFFF" face="Arial, Helvetica, sans-serif"><strong>Inhaltsverzeichnis </strong></font></td>
              <td background="boxtopright.jpg"><img src="boxtopright.jpg" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="boxdividerleft.jpg" width="8" height="13" alt=""></td>
              <td background="boxdivider.jpg"><img src="boxdivider.jpg" alt="" width="8" height="13"></td>
              <td><img src="boxdividerright.jpg" width="8" height="13" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
              <td bgcolor="#ffffff" class="V10">
                <?php include("az.html"); ?>
              </td>
              <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
              <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
              <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
            </tr>
          </table>
        </td>
        <td width="530" rowspan="2" valign="top">          <table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr valign="top" align="left">
            <td width="8"><img src="boxtopleftcorner.jpg" width="8" height="8" alt=""></td>
            <td background="boxtop.jpg"><img src="boxtop.jpg" alt="" width="8" height="8"></td>
            <td width="8"><img src="boxtoprightcorner.jpg" width="8" height="8" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td background="boxtopleft.jpg" width="8"><img src="boxtopleft.jpg" width="8" height="8" alt=""></td>
            <td bgcolor="b1cc1f">
              <h1><font color="#FFFFFF" face="Arial, Helvetica, sans-serif">Aromahefe</font></h1>
            </td>
            <td background="boxtopright.jpg" width="8"><img src="boxtopright.jpg" width="8" height="8" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td width="8"><img src="boxdividerleft.jpg" width="8" height="13" alt=""></td>
            <td background="boxdivider.jpg"><img src="boxdivider.jpg" alt="" width="8" height="13"></td>
            <td width="8"><img src="boxdividerright.jpg" width="8" height="13" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td width="8" height="905" background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
            <td bgcolor="#ffffff" class="L12">              <p class="text">    <img SRC="images-weinlexikon-wein/aromahefe-weinlexikon.jpg" style="border: double 4px #009999" align="right" hspace="10" vspace="10" name="Aromahefe Weinlexikon" alt="Aromahefe Weinlexikon" >Jede
                Traube, die nach der Lese aus dem Weinberg kommt, bringt nat&uuml;rlich
                vorkommende, <strong>wilde Hefen</strong> mit sich, die die alkoholische Verg&auml;rung
                des Traubenmostes zu Wein auf dem nat&uuml;rlichen Weg in Gang
                bringen.</p>
              <p class="text">Da der Winzer aber nie genau wei&szlig;, welche ungeliebten
                G&auml;ste sich auf diese Weise in den Keller schleichen, bewegen
                sich die meisten lieber auf der sicheren Seite: Sie setzen dem
                Most vor der G&auml;rung <strong>gez&uuml;chtete Hefest&auml;mme</strong> mit
                bekannten Eigenschaften zu, um reint&ouml;nige Weine und eine
                kontrollierte G&auml;rung zu erreichen. Die immer noch vorhandenen
                wilden Hefen geraten durch den Zusatz der gez&uuml;chteten Konkurrenz
                derart in Bedr&auml;ngnis, dass ihreEigenschaften geruchlich
                  und geschmacklich nicht mehr wahrnehmbar sind.</p>
              <p class="text">Seit man Hefen z&uuml;chtet, ist es Ziel, deren Eigenschaft
                zu optimieren und f&uuml;r die Bereitung unterschiedlicher <strong>Weinarten</strong>                zu differenzieren (Sekthefen, Rotweinhefen usw.). Das aktuellste
                Schlagwort in diesem Zusammenhang ist sicher das der <strong>&laquo;Aromahefe&raquo;</strong>.
                Gemeint sind Hefest&auml;mme, die w&auml;hrend der alkoholischen
                G&auml;rung ein besonders ausgepr&auml;gtes Aromenprofil im Wein
                erzeugen.</p>
              <p class="text">Da die Aromen im fertigen Wein zum gr&ouml;&szlig;ten Teil erst
                w&auml;hrend der alkoholischen G&auml;rung entstehen, weil sie
                zu einem guten Teil durch die Stoffwechselt&auml;tigkeit der
                Hefen produziert werden, scheint dieser Weg auf den ersten Blick
                interessant. Aber Wein ist ein<strong> komplexes System</strong>. Die bisher zum
                Thema Aromahefen durchgef&uuml;hrten Versuche konnten lediglich
                zeigen, dass es eine Hefe, die gezielt bestimmte Aromen in den
                Wein zaubert, nicht gibt. Zwar ist es m&ouml;glich, Hefest&auml;mme
                zu z&uuml;chten, die bestimmte Aromastoffe in gr&ouml;&szlig;erer
                Menge produzieren als andere. Wie sich das aber im <strong>Gesamtaromenprofil</strong>                eines Weines auswirkt, ist abh&auml;ngig von Jahrgang, Rebsorte,
                ja sogar von der Temperatur, unter der die jeweilige G&auml;rung
                stattfindet.</p>
              <p class="text">Der Ursprung hocharomatischer Weine mit besonders
                  ausgepr&auml;gtem
                Sortentyp ist wie immer die Traube im <strong>Weinberg</strong>. Gesunde Trauben,
                die ausreichend mit Wasser, Sonne (Jahrgang) und N&auml;hrstoffen
                (Boden und D&uuml;ngung) versorgt wurden, bringen ein optimales
                origin&auml;res Traubenaroma mit sich. Durch die Verarbeitung
                nach der Lese (Maischen, Pressen, Standzeiten der Trauben) entwickelt
                sich noch vor der G&auml;rung das so genannte sekund&auml;re
                <strong>Traubenbouquet</strong>. Die Grundlage des bei der G&auml;rung entstehenden
                G&auml;rbouquets wird also schon vor der eigentlichen Verarbeitung
                im Keller gelegt. Das schw&auml;chste Glied in der Verarbeitung
                wird die Qualit&auml;t des sp&auml;teren Weines bestimmen.</p>
              <p><span class="text">Selbst der hei&szlig; diskutierte Griff zu gentechnisch manipulierten
                Hefen kann an diesen Fakten wenig &auml;ndern. Denn ob Hefest&auml;mme
                mit definierten Eigenschaften konventionell gez&uuml;chtet oder
                gentechnisch gewonnen werden: Was bleibt, ist die <strong>Vielzahl
                an Komponenten</strong>, die f&uuml;r die Aromenstruktur eines Weines verantwortlich
                sind. Und so bleibt jeder Wein ein unkopierbares Individuum aus
                den nat&uuml;rlichen Gegebenheiten und dem Ehrgeiz des Winzers.</span><br>
              </p>              <p><a href="http://www.a-tavola.de/shop/index.php" target="_blank">Linktipp:
                  Bioweine von a-tavola!</a><span class="text"> </span></p>
              </td>
            <td background="boxright.gif" width="8"><img src="boxright.gif" width="8" height="8" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td width="8"><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
            <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
            <td width="8"><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
          </tr>
        </table></td>
        <td width="120" rowspan="2" valign="top">	<script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
/* 120x600, Erstellt 11.06.08 */
google_ad_slot = "6662271223";
google_ad_width = 120;
google_ad_height = 600;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
</td>
      </tr>
      <tr>
        <td width="216" height="1374" align="left" valign="top">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>



</body>
</html>
