<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<head profile="http://dublincore.org/documents/dcq-html/">

<link rel="schema.DC" href="http://purl.org/dc/elements/1.1/">
<link rel="schema.DCTERMS" href="http://purl.org/dc/terms/">
<link href="caritas_mannheim.css" rel="stylesheet" type="text/css">

<title>Ehrenamt in Mannheim - Caritasverband Mannheim e.V.</title>
<meta name="robots" content="index,follow,all" />
<meta name="description" content="Kurzzeitpflege in Mannheim - Caritas Mannheim">
<meta name="author" content="Caritasverband Mannheim e.V.">
<meta name="keywords" content="Caritas, Geschichte, Entstehtung, Kurzzeitpfelge">


<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="DC.title" content="Kurzzeitpflege in Mannheim - Caritas Mannheim">

<meta name="DC.creator" content="Die Geschichte der Caritas - Caritasverband Mannheim e.V.">
<meta name="DC.subject" content="Kurzzeitpflege in Mannheim - Caritas Mannheim">
<meta name="DC.description" content="Kurzzeitpflege in Mannheim - Caritas Mannheim">
<meta name="DC.publisher" content="Caritasverband Mannheim e.V.">
<meta name="DC.contributor" content="Caritasverband Mannheim e.V.">
<meta name="DC.date" content="2007-05-15T08:49:37+02:00" scheme="DCTERMS.W3CDTF">
<meta name="DC.type" content="Text" scheme="DCTERMS.DCMIType">
<meta name="DC.format" content="text/html" scheme="DCTERMS.IMT">
<meta name="DC.identifier"
      content="http://www.kath.de/caritas_Mannheim/kurzzeitpflege_caritas_mannheim.php"
      scheme="DCTERMS.URI">
<meta name="DC.source"
      content="http://www.w3.org/TR/html401/struct/global.html#h-7.4.4"
      scheme="DCTERMS.URI">
<meta name="DC.language" content="de" scheme="DCTERMS.RFC3066">
<meta name="DC.relation" content="http://dublincore.org/" scheme="DCTERMS.URI">
<meta name="DC.coverage" content="Munich" scheme="DCTERMS.TGN">
<meta name="DC.rights" content="Alle Rechte liegen beim Autor">
</head>

<body background="images-caritas-mannheim/caritas-mannheim-hintergrund.jpg" link="#ED5105" vlink="#666666" alink="#FF0000">
<table width="800" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="6">
      <tr>
        <td width="100" align="left" valign="top" height="3">
          <table width="216" border="0" cellpadding="0" cellspacing="0">
            <tr valign="top" align="left">
              <td width="8"><img src="../lexikon/liturgie/boxtopleftcorner.jpg" width="8" height="8" alt=""></td>
              <td width="200" background="../lexikon/liturgie/boxtop.jpg"><img src="../lexikon/liturgie/boxtop.jpg" alt="" width="8" height="8"></td>
              <td width="8"><img src="../lexikon/liturgie/boxtoprightcorner.jpg" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="../lexikon/liturgie/boxtopleft.jpg"><img src="../lexikon/liturgie/boxtopleft.jpg" width="8" height="8" alt=""></td>
              <td bgcolor="ed1c24"><p><b><font color="#FFFFFF" face="Arial, Helvetica, sans-serif">Caritas-Lexikon</font></b></p>
              </td>
              <td background="../lexikon/liturgie/boxtopright.jpg"><img src="../lexikon/liturgie/boxtopright.jpg" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="../lexikon/liturgie/boxdividerleft.jpg" width="8" height="13" alt=""></td>
              <td background="../lexikon/liturgie/boxdivider.jpg"><img src="../lexikon/liturgie/boxdivider.jpg" alt="" width="8" height="13"></td>
              <td><img src="../lexikon/liturgie/boxdividerright.jpg" width="8" height="13" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="../lexikon/liturgie/boxleft.gif"><img src="../lexikon/liturgie/boxleft.gif" width="8" height="8" alt=""></td>
              <td bgcolor="#FFFFFF">
                <?php include("logo.html"); ?>
              </td>
              <td background="../lexikon/liturgie/boxright.gif"><img src="../lexikon/liturgie/boxright.gif" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="../lexikon/liturgie/boxbottomleft.gif" width="8" height="8" alt=""></td>
              <td background="../lexikon/liturgie/boxbottom.gif"><img src="../lexikon/liturgie/boxbottom.gif" width="8" height="8" alt=""></td>
              <td><img src="../lexikon/liturgie/boxbottomright.gif" width="8" height="8" alt=""></td>
            </tr>
          </table>
          <br>
          <table width="216" border="0" cellpadding="0" cellspacing="0">
            <tr valign="top" align="left">
              <td width="8"><img src="../lexikon/liturgie/boxtopleftcorner.jpg" width="8" height="8" alt=""></td>
              <td width="200" background="../lexikon/liturgie/boxtop.jpg"><img src="../lexikon/liturgie/boxtop.jpg" alt="" width="8" height="8"></td>
              <td width="8"><img src="../lexikon/liturgie/boxtoprightcorner.jpg" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="../lexikon/liturgie/boxtopleft.jpg"><img src="../lexikon/liturgie/boxtopleft.jpg" width="8" height="8" alt=""></td>
              <td bgcolor="ed1c24"><font color="#FFFFFF" face="Arial, Helvetica, sans-serif"><strong>Inhaltsverzeichnis </strong></font></td>
              <td background="../lexikon/liturgie/boxtopright.jpg"><img src="../lexikon/liturgie/boxtopright.jpg" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="../lexikon/liturgie/boxdividerleft.jpg" width="8" height="13" alt=""></td>
              <td background="../lexikon/liturgie/boxdivider.jpg"><img src="../lexikon/liturgie/boxdivider.jpg" alt="" width="8" height="13"></td>
              <td><img src="../lexikon/liturgie/boxdividerright.jpg" width="8" height="13" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="../lexikon/liturgie/boxleft.gif"><img src="../lexikon/liturgie/boxleft.gif" width="8" height="8" alt=""></td>
              <td bgcolor="#FFFFFF" class="V10">
                <?php include("az.html"); ?>
              </td>
              <td background="../lexikon/liturgie/boxright.gif"><img src="../lexikon/liturgie/boxright.gif" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="../lexikon/liturgie/boxbottomleft.gif" width="8" height="8" alt=""></td>
              <td background="../lexikon/liturgie/boxbottom.gif"><img src="../lexikon/liturgie/boxbottom.gif" width="8" height="8" alt=""></td>
              <td><img src="../lexikon/liturgie/boxbottomright.gif" width="8" height="8" alt=""></td>
            </tr>
          </table>
        </td>
        <td rowspan="2" valign="top">          <table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr valign="top" align="left">
            <td width="8"><img src="../lexikon/liturgie/boxtopleftcorner.jpg" width="8" height="8" alt=""></td>
            <td background="../lexikon/liturgie/boxtop.jpg"><img src="../lexikon/liturgie/boxtop.jpg" alt="" width="8" height="8"></td>
            <td width="8"><img src="../lexikon/liturgie/boxtoprightcorner.jpg" width="8" height="8" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td background="../lexikon/liturgie/boxtopleft.jpg" width="8"><img src="../lexikon/liturgie/boxtopleft.jpg" width="8" height="8" alt=""></td>
            <td bgcolor="ed1c24">
              <h1><font color="#FFFFFF" face="Arial, Helvetica, sans-serif">Ehrenamt
                  in Mannheim</font></h1>
            </td>
            <td background="../lexikon/liturgie/boxtopright.jpg" width="8"><img src="../lexikon/liturgie/boxtopright.jpg" width="8" height="8" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td width="8"><img src="../lexikon/liturgie/boxdividerleft.jpg" width="8" height="13" alt=""></td>
            <td background="../lexikon/liturgie/boxdivider.jpg"><img src="../lexikon/liturgie/boxdivider.jpg" alt="" width="8" height="13"></td>
            <td width="8"><img src="../lexikon/liturgie/boxdividerright.jpg" width="8" height="13" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td width="8" height="905" background="../lexikon/liturgie/boxleft.gif"><img src="../lexikon/liturgie/boxleft.gif" width="8" height="8" alt=""></td>
            <td bgcolor="#FFFFFF" class="L12">
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="48%"><span class="&uuml;berschrift"><img src="images-caritas-mannheim/caritas_mannheim_iconpunkt.gif" width="12" height="12"> Soziales
                      Problem</span><span class="&uuml;berschrift">:</span><span class="text"></span></td>
                  <td valign=middle width="52%"><div align="right"><img src="images-caritas-mannheim/caritas_mannheim_image_pdf.jpg" width="30" height="30"> <span class="link"><font color="#000000">Als
                          PDF-Datei ausgeben</font></span></div>
                  </td>
                </tr>
              </table>
              <p><span class="text">    <img SRC="images-caritas-mannheim/ehrenamt_caritas_mannheim.jpg" alt="Kurzzeitpflege in Mannheim" name="Kurzzeitpflege" width="300" height="199" hspace="10" vspace="10" align="right" style="border: double 4px #009999">    Ehrenamtliches
                  Engagement aus christlicher Grundhaltung ist ein Wesensmerkmal
                  des christlichen Glaubens. Die Sorge um Mitmenschen geh&ouml;rt
                  zu den Diensten einer jeder Gemeinde. Daneben gibt es vielf&auml;ltiges
                  Engagement auch in Einrichtungen und Diensten des Caritasverbandes
                  sowie bei anderen Tr&auml;gern &#8211; geleistet von ehrenamtlich
                  t&auml;tigen Frauen und M&auml;nnern.</span></p>
              <p class="text"><span class="&uuml;berschrift"><img src="images-caritas-mannheim/caritas_mannheim_iconpunkt.gif" width="12" height="12"> Wer
                  ist davon betroffen:</span><br>
                            <br>
        Mithelfen d&uuml;rfen alle, die sich wertsch&auml;tzend und wohlwollend,
        unter Wahrung der Schweigepflicht, bestimmter Mitmenschen in Not annehmen.
        Vielfach gibt es vor &Uuml;bernahme einer Aufgabe eine inhaltliche Vorbereitung,
        ebenso wie regelm&auml;&szlig;ige Treffen zum Erfahrungsaustausch. </p>
              <p class="text"><span class="&uuml;berschrift"><img src="images-caritas-mannheim/caritas_mannheim_iconpunkt.gif" width="12" height="12"> Christliche
                  Motivation:</span><br>
                            <br>
        Einander Hilfe zu leisten ist ein urchristliches Anliegen. Jesus hat
        vor seiner Himmelfahrt die J&uuml;nger einander anvertraut. Er sendet
        sie, seine Liebe unter den Menschen zu bezeugen und gute Taten zu tun.
        Jeder einzelne Christ ist somit von Jesus direkt gesendet und hat den
        Auftrag Gutes zu tun. Ein Ehrenamt bei der Caritas bietet daf&uuml;r
        eine sinnvolle, spannende und gewinnbringende Aufgabe.</p>
              <p><span class="&uuml;berschrift"><img src="images-caritas-mannheim/caritas_mannheim_iconpunkt.gif" width="12" height="12"> Konkrete
                  Probleml&ouml;sung in Mannheim:<br>
                </span><span class="text"><br>
        So variationsreich die Arbeit der Caritas selbst ist, so weit sind auch
        die M&ouml;glichkeiten einer ehrenamtlichen Mitarbeit. M&ouml;glichkeiten
        bieten sich zum Beispiel in folgenden Bereichen: Als kaufm&auml;nnisch
        versierter Experte in der Schuldnerberatung; als M&uuml;nz- und Briefmarkenkenner
        in dem Second-Hand-Kaufhaus; als Besuchsdienst in der Altenpflege, im
        Krankenhaus oder der Gemeinde, als Patenomi in der Kinderbetreuung, in
        der &ouml;kumenischen Hospizhilfe. <br>
                                        <br>
                                        <br>
                </span><span class="&uuml;berschrift"><img src="images-caritas-mannheim/caritas_mannheim_iconpunkt.gif" width="12" height="12"> interessenvertretung: </span><span class="text"><br>
                <br>
                </span><span class="text">In Mannheim hat sich der Gro&szlig;teil
                der ehrenamtlichen Caritas-Gruppen dem Fachverband der Caritas-Konferenzen
                abgeschlossen. Die Gruppen haben gew&auml;hlte Leitungen: Es
                gibt einen gew&auml;hlten Dekanatsvorstand, der die Interessen
                sowohl der Ehrenamtlichen wie auch der Menschen, denen die Sorge
                der Ehrenamtliche gilt, in kirchlichen Gremien sowohl im Dekanat
                wie auch auf Di&ouml;zesanebene einbringen. <br> 
                <br>
                <br>
                </span><span class="&uuml;berschrift"><img src="images-caritas-mannheim/caritas_mannheim_iconpunkt.gif" width="12" height="12"> LINKS:</span><span class="text">                <br>
                <br>
                <a href="http://www.caritas-mannheim.de/39526.html" target="_blank">http://www.caritas-mannheim.de/39526.html</a></span></p>
              <p align="right"><font size="2"><span class="text"><b><img src="images-caritas-mannheim/logo-unten-caritasverband-mannheim-klein.jpg" width="560" height="107"><br>
                  </b></span><b><br>
                  </b>&copy; Caritasverband Mannheim e.V.<b><br>
                                                                                                                    <a href="http://www.caritas-mannheim.de/" target="_blank">http://www.caritas-mannheim.de</a>/ <br>
                                                                                                                    <br>
                </b></font></p></td>
            <td background="../lexikon/liturgie/boxright.gif" width="8"><img src="../lexikon/liturgie/boxright.gif" width="8" height="8" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td width="8"><img src="../lexikon/liturgie/boxbottomleft.gif" width="8" height="8" alt=""></td>
            <td background="../lexikon/liturgie/boxbottom.gif"><img src="../lexikon/liturgie/boxbottom.gif" width="8" height="8" alt=""></td>
            <td width="8"><img src="../lexikon/liturgie/boxbottomright.gif" width="8" height="8" alt=""></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td width="100" height="1374" align="left" valign="top">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
</body>
</html>
