<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>


<head profile="http://dublincore.org/documents/dcq-html/">

<link rel="schema.DC" href="http://purl.org/dc/elements/1.1/">
<link rel="schema.DCTERMS" href="http://purl.org/dc/terms/">
<link href="caritas_mannheim.css" rel="stylesheet" type="text/css">

<title>Geschichte der Caritas - Caritasverband Mannheim e.V.</title>
<meta name="robots" content="index,follow,all" />
<meta name="description" content="Die Geschichte der Caritas - Erfahren Sie hier mehr">
<meta name="author" content="Caritasverband Mannheim e.V.">
<meta name="keywords" content="Caritas, Geschichte, Entstehtung">


<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="DC.title" content="Geschichte der Caritas - Caritas Mannheim">

<meta name="DC.creator" content="Die Geschichte der Caritas - Caritasverband Mannheim e.V.">
<meta name="DC.subject" content="Geschichte der Caritas">
<meta name="DC.description" content="Die Geschichte der Caritas - Erfahren Sie hier mehr">
<meta name="DC.publisher" content="Caritasverband Mannheim e.V.">
<meta name="DC.contributor" content="Caritasverband Mannheim e.V.">
<meta name="DC.date" content="2007-05-15T08:49:37+02:00" scheme="DCTERMS.W3CDTF">
<meta name="DC.type" content="Text" scheme="DCTERMS.DCMIType">
<meta name="DC.format" content="text/html" scheme="DCTERMS.IMT">
<meta name="DC.identifier"
      content="http://www.kath.de/caritasverband_Mannheim/geschichte_der_caritas.php"
      scheme="DCTERMS.URI">
<meta name="DC.source"
      content="http://www.w3.org/TR/html401/struct/global.html#h-7.4.4"
      scheme="DCTERMS.URI">
<meta name="DC.language" content="de" scheme="DCTERMS.RFC3066">
<meta name="DC.relation" content="http://dublincore.org/" scheme="DCTERMS.URI">
<meta name="DC.coverage" content="Munich" scheme="DCTERMS.TGN">
<meta name="DC.rights" content="Alle Rechte liegen beim Autor">

</head>




<body bgcolor="#fffffd" link="#FF0000" vlink="#FF3300" alink="#CCCCFF" leftmargin="6" topmargin="6" marginwidth="6" marginheight="6">
<table width="100%" border="0" cellspacing="0" cellpadding="6">
  <tr>
    <td width="100" align="left" valign="top" height="3"> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="../lexikon/liturgie/boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="../lexikon/liturgie/boxtop.gif"><img src="../lexikon/liturgie/boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="../lexikon/liturgie/boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="../lexikon/liturgie/boxtopleft.gif"><img src="../lexikon/liturgie/boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><p><b><font face="Arial, Helvetica, sans-serif">Caritas-Lexikon</font></b></p></td>
          <td background="../lexikon/liturgie/boxtopright.gif"><img src="../lexikon/liturgie/boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="../lexikon/liturgie/boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="../lexikon/liturgie/boxdivider.gif"><img src="../lexikon/liturgie/boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="../lexikon/liturgie/boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="../lexikon/liturgie/boxleft.gif"><img src="../lexikon/liturgie/boxleft.gif" width="8" height="8" alt=""></td>
          <td> <?php include("logo.html"); ?> </td>
          <td background="../lexikon/liturgie/boxright.gif"><img src="../lexikon/liturgie/boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="../lexikon/liturgie/boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="../lexikon/liturgie/boxbottom.gif"><img src="../lexikon/liturgie/boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="../lexikon/liturgie/boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
	  <br>
	  <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="../lexikon/liturgie/boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="../lexikon/liturgie/boxtop.gif"><img src="../lexikon/liturgie/boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="../lexikon/liturgie/boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="../lexikon/liturgie/boxtopleft.gif"><img src="../lexikon/liturgie/boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><font face="Arial, Helvetica, sans-serif"><strong>Inhaltsverzeichnis
                <br>
                Caritas-Lexikon
          </strong></font></td>
          <td background="../lexikon/liturgie/boxtopright.gif"><img src="../lexikon/liturgie/boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="../lexikon/liturgie/boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="../lexikon/liturgie/boxdivider.gif"><img src="../lexikon/liturgie/boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="../lexikon/liturgie/boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="../lexikon/liturgie/boxleft.gif"><img src="../lexikon/liturgie/boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10"> <?php include("az.html"); ?> </td>
          <td background="../lexikon/liturgie/boxright.gif"><img src="../lexikon/liturgie/boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="../lexikon/liturgie/boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="../lexikon/liturgie/boxbottom.gif"><img src="../lexikon/liturgie/boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="../lexikon/liturgie/boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
    <td rowspan="2" valign="top">
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="../lexikon/liturgie/boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td background="../lexikon/liturgie/boxtop.gif" colspan="2"><img src="../lexikon/liturgie/boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="../lexikon/liturgie/boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="../lexikon/liturgie/boxtopleft.gif" width="8"><img src="../lexikon/liturgie/boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"> 
            <h1><font face="Arial, Helvetica, sans-serif">Das  Caritas-Lexikon
                f&uuml;r Mannheim</font> </h1>
          </td>
          <td bgcolor="#E2E2E2"><img src="images-caritas-mannheim/caritas_mannheim_image_pdf.jpg" width="30" height="30"> <span class="link"><font color="#000000">Als
          PDF-Datei ausgeben</font></span></td>
          <td background="../lexikon/liturgie/boxtopright.gif" width="8"><img src="../lexikon/liturgie/boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8"><img src="../lexikon/liturgie/boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="../lexikon/liturgie/boxdivider.gif" colspan="2"><img src="../lexikon/liturgie/boxdivider.gif" alt="" width="8" height="13"></td>
          <td width="8"><img src="../lexikon/liturgie/boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8" height="2187" background="../lexikon/liturgie/boxleft.gif">&nbsp;</td>
          <td class="L12" width="70%">		  <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="48%"><span class="&uuml;berschrift"><img src="images-caritas-mannheim/caritas_mannheim_iconpunkt.gif" width="12" height="12"> Herzlich
                  willkommen:</span><span class="text"></span> </td>
              <td valign=middle width="52%"><div align="right"></div></td>
            </tr>
          </table>            
            <p>		      <font size="2"><span class="text">Hier
                    finden Sie ein Nachschlagewerk f&uuml;r Ihre Themen rund um Mannheim.
                    Dieses Lexikon wurde erstellt vom Caritas-Verband Mannheim
                    e.V. und dient dazu, Sie &uuml;ber die wichtigsten Fakten kurz
                    und knapp zu informieren. Durch die Nutzung von googl earth
                    k&ouml;nnen Sie sich ein Bild &uuml;ber die Arbeit der Caritas anhand
            von 3-D Karten machen.</span></font></p>
            <p><font size="2"><span class="text">Im linken Men&uuml; finden
            Sie Links und Buchtipps zu den einzelnen Themen.
                  
            </span></font></p>
            <p><font size="2"><span class="text">Wir w&uuml;nschen Ihnen eine hilfreiche
            Lekt&uuml;re,</span></font></p>
            <p><font face="Arial, Helvetica, sans-serif">Ihr</font><font size="2"><span class="text"><b><br>
              </b></span><b><br>
              </b>&copy; Caritasverband
                                                          Mannheim e.V.<b><br>
                                              <a href="http://www.caritas-mannheim.de/" target="_blank">http://www.caritas-mannheim.de</a>/              </b></font></font></p>
          </td>
          <td class="L12" width="30%">            <div align="right"> 
              <p align="right">     <font size="2" face="Arial, Helvetica, sans-serif">              </font></p>
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="5%" height="20">&nbsp;</td>
                  <td width="90%"><font size="2" face="Arial, Helvetica, sans-serif"><img src="images-caritas-mannheim/caritas_mannheim_geschichte_der_caritas_1.jpg" alt="Die Geschichte der Caritas Mannheim " width="310" height="229"></font></td>
                  <td width="5%">&nbsp;</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td><font size="2" face="Arial, Helvetica, sans-serif"><span class="unterbild">Das
                        Motiv der N&auml;chstenliebe zieht sich wie ein roter<br>
Faden durch alle Kapitel der abwechslungsreichen <br>
Geschichte der Caritas</span></font></td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td height="236">&nbsp;</td>
                  <td><p><a href="http://maps.google.de/maps/ms?ie=UTF8&hl=de&msa=0&msid=112740146363501885729.000001128bd9e697fa096&ll=49.491378,8.464966&spn=0.028935,0.09407&t=h&z=14&om=1" target="_blank"><img src="images-caritas-mannheim/caritas_mannheim_google_maps_klein.jpg" alt="die caritas in Mannheim [ber google maps entdecken" width="325" height="224" border="0"></a></p>
                  </td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td><span class="unterbild">Erkunden Sie</span><span class="link"><a href="http://maps.google.de/maps/ms?ie=UTF8&hl=de&msa=0&msid=112740146363501885729.000001128bd9e697fa096&ll=49.491378,8.464966&spn=0.028935,0.09407&t=h&z=14&om=1" target="_blank"> hier</a> </span><span class="unterbild">den
                      Caritasverband Mannheim e.V.<br>
mittels interaktiver Satellitenka</span><span class="text">rten.Nutzen Sie auch<br>
unsere </span><span class="link">KML-Datei</span><span class="text"> f&uuml;r
google earth</span></td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td><p class="&uuml;berschrift"><span class="&uuml;berschrift"><img src="images-caritas-mannheim/caritas_mannheim_iconpunkt.gif" width="12" height="12"></span> Buchtipps
                      zum Thema</p>
                    <p class="text">Baldus, Manfred, Gr&uuml;ndung des Di&ouml;zesan-Caritasverbandes
                  f&uuml;r das Erzbistum K&ouml;ln </p></td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td><p><span class="&uuml;berschrift"><img src="images-caritas-mannheim/caritas_mannheim_iconpunkt.gif" width="12" height="12"> Linktipps</span></p>
                    <p><span class="text">Deutscher Caritasverband e.V. - Geschichte<br>
                      </span><span class="link"><a href="http://www.caritas.de/6135.html" target="_blank">http://www.caritas.de/6135.htm</a></span><span class="text"><a href="http://www.caritas.de/6135.html" target="_blank">l<br>
                    </a></span></p></td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td><span class="&uuml;berschrift"><img src="images-caritas-mannheim/caritas_mannheim_iconpunkt.gif" width="12" height="12"> Zitate<br>
                      <br>
                  </span><span class="text">&#8222;die vollst&auml;ndige Hingabe
                  f&uuml;r das Wohl anderer, ohne sich um Opfer und eigenen Nachteil
                  zu k&uuml;mmern&quot;<br>
Autor <br>
<br>
&#8222; Frieden, Lachen, S&auml;ttigung; Heil und Gl&uuml;ck&#8220;<br>
Autor </span><span class="&uuml;berschrift"><br>
</span></td>
                  <td>&nbsp;</td>
                </tr>
              </table>
              <p align="right"><font size="2" face="Arial, Helvetica, sans-serif"><br>
                  .</font></p>
              <p><br>
              </p>
              <p class="&uuml;berschrift">&nbsp;</p>
              <p><span class="text"><a href="http://www.caritas.de/6135.html" target="_blank"><br>
                </a></span><br>
              </p>
              </div>
          </td>
          <td background="../lexikon/liturgie/boxright.gif" width="8"><img src="../lexikon/liturgie/boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8"><img src="../lexikon/liturgie/boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="../lexikon/liturgie/boxbottom.gif" colspan="2"><img src="../lexikon/liturgie/boxbottom.gif" width="8" height="8" alt=""></td>
          <td width="8"><img src="../lexikon/liturgie/boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td width="100" align="left" valign="top">&nbsp;</td>
  </tr>
</table>
</body>
</html>
