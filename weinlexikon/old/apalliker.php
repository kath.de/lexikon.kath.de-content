<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>


<head profile="http://dublincore.org/documents/dcq-html/">

<link rel="schema.DC" href="http://purl.org/dc/elements/1.1/">
<link rel="schema.DCTERMS" href="http://purl.org/dc/terms/">
<link href="caritas_mannheim.css" rel="stylesheet" type="text/css">

<title>Apalliker Mannheim - Caritasverband Mannheim e.V.</title>
<meta name="robots" content="index,follow,all" />
<meta name="description" content="Die Geschichte der Caritas - Erfahren Sie hier mehr">
<meta name="author" content="Caritasverband Mannheim e.V.">
<meta name="keywords" content="Caritas, Geschichte, Entstehtung">


<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="DC.title" content="Geschichte der Caritas - Caritas Mannheim">

<meta name="DC.creator" content="Die Geschichte der Caritas - Caritasverband Mannheim e.V.">
<meta name="DC.subject" content="Geschichte der Caritas">
<meta name="DC.description" content="Die Geschichte der Caritas - Erfahren Sie hier mehr">
<meta name="DC.publisher" content="Caritasverband Mannheim e.V.">
<meta name="DC.contributor" content="Caritasverband Mannheim e.V.">
<meta name="DC.date" content="2007-05-15T08:49:37+02:00" scheme="DCTERMS.W3CDTF">
<meta name="DC.type" content="Text" scheme="DCTERMS.DCMIType">
<meta name="DC.format" content="text/html" scheme="DCTERMS.IMT">
<meta name="DC.identifier"
      content="http://www.kath.de/caritasverband_Mannheim/geschichte_der_caritas.php"
      scheme="DCTERMS.URI">
<meta name="DC.source"
      content="http://www.w3.org/TR/html401/struct/global.html#h-7.4.4"
      scheme="DCTERMS.URI">
<meta name="DC.language" content="de" scheme="DCTERMS.RFC3066">
<meta name="DC.relation" content="http://dublincore.org/" scheme="DCTERMS.URI">
<meta name="DC.coverage" content="Munich" scheme="DCTERMS.TGN">
<meta name="DC.rights" content="Alle Rechte liegen beim Autor">

</head>




<body bgcolor="#fffffd" link="#FF0000" vlink="#FF3300" alink="#CCCCFF" leftmargin="6" topmargin="6" marginwidth="6" marginheight="6">
<table width="100%" border="0" cellspacing="0" cellpadding="6">
  <tr>
    <td width="100" align="left" valign="top" height="3"> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="../lexikon/liturgie/boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="../lexikon/liturgie/boxtop.gif"><img src="../lexikon/liturgie/boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="../lexikon/liturgie/boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="../lexikon/liturgie/boxtopleft.gif"><img src="../lexikon/liturgie/boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><p><b><font face="Arial, Helvetica, sans-serif">Caritas-Lexikon</font></b></p></td>
          <td background="../lexikon/liturgie/boxtopright.gif"><img src="../lexikon/liturgie/boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="../lexikon/liturgie/boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="../lexikon/liturgie/boxdivider.gif"><img src="../lexikon/liturgie/boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="../lexikon/liturgie/boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="../lexikon/liturgie/boxleft.gif"><img src="../lexikon/liturgie/boxleft.gif" width="8" height="8" alt=""></td>
          <td> <?php include("logo.html"); ?> </td>
          <td background="../lexikon/liturgie/boxright.gif"><img src="../lexikon/liturgie/boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="../lexikon/liturgie/boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="../lexikon/liturgie/boxbottom.gif"><img src="../lexikon/liturgie/boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="../lexikon/liturgie/boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
	  <br>
	  <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="../lexikon/liturgie/boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="../lexikon/liturgie/boxtop.gif"><img src="../lexikon/liturgie/boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="../lexikon/liturgie/boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="../lexikon/liturgie/boxtopleft.gif"><img src="../lexikon/liturgie/boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><font face="Arial, Helvetica, sans-serif"><strong>Inhaltsverzeichnis
                <br>
                Caritas-Lexikon
          </strong></font></td>
          <td background="../lexikon/liturgie/boxtopright.gif"><img src="../lexikon/liturgie/boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="../lexikon/liturgie/boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="../lexikon/liturgie/boxdivider.gif"><img src="../lexikon/liturgie/boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="../lexikon/liturgie/boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="../lexikon/liturgie/boxleft.gif"><img src="../lexikon/liturgie/boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10"> <?php include("az.html"); ?> </td>
          <td background="../lexikon/liturgie/boxright.gif"><img src="../lexikon/liturgie/boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="../lexikon/liturgie/boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="../lexikon/liturgie/boxbottom.gif"><img src="../lexikon/liturgie/boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="../lexikon/liturgie/boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
    <td rowspan="2" valign="top">
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="../lexikon/liturgie/boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td background="../lexikon/liturgie/boxtop.gif" colspan="2"><img src="../lexikon/liturgie/boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="../lexikon/liturgie/boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="../lexikon/liturgie/boxtopleft.gif" width="8"><img src="../lexikon/liturgie/boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"> 
            <h1>Apalliker Mannheim</h1>
          </td>
          <td bgcolor="#E2E2E2"><img src="images-caritas-mannheim/caritas_mannheim_image_pdf.jpg" width="30" height="30"> <span class="link"><font color="#000000">Als
          PDF-Datei ausgeben</font></span></td>
          <td background="../lexikon/liturgie/boxtopright.gif" width="8"><img src="../lexikon/liturgie/boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8"><img src="../lexikon/liturgie/boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="../lexikon/liturgie/boxdivider.gif" colspan="2"><img src="../lexikon/liturgie/boxdivider.gif" alt="" width="8" height="13"></td>
          <td width="8"><img src="../lexikon/liturgie/boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8" height="2187" background="../lexikon/liturgie/boxleft.gif"><img src="../lexikon/liturgie/boxleft.gif" width="8" height="8" alt=""></td>
          <td class="L12" width="70%">		  <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="48%"><span class="&uuml;berschrift"><img src="images-caritas-mannheim/caritas_mannheim_iconpunkt.gif" width="12" height="12"> Begriffsbestimmung:</span><span class="text"></span> </td>
              <td valign=middle width="52%"><div align="right"></div></td>
            </tr>
          </table>            
            <p class="text">		      Bekannt ist die Problematik des
                Wachkomas vom Fall der Amerikanerin Terri Schiavo aus dem Jahre
                2005. Sie lag 15 Jahre im Wachkom.<br>
              Der Begriff leitet sich vom lat. ab und bedeutet soviel wie &#8222;ohne
              (Hirn-) Mantel&#8220;. Im amerikanischen wird der Zustand treffender
              als im deutschen (apallisch) als &#8222;persistent vegetative state&#8220; umschrieben,
              was &uuml;bersetzt soviel wie dahinvegetieren bedeutet. Ein anderer
              Ausdruck ist der des Wachkomas.</p>
            <p><span class="text">Ernst Kretschmer, der diesen Begriff 1940 in
                die Medizin einf&uuml;hrte
              liefert solgende Beschreibung: &quot;Der Patient liegt wach da
              mit offenen Augen. Der Blick starrt geradeaus und gleitet ohne
              Fixationspunkt verst&auml;ndnislos hin und her. Auch der Versuch,
              die Aufmerksamkeit hinzulenken, gelingt nicht oder h&ouml;chstens
              spurenweise ... Es fehlt manchmal auch das reflektorische R&uuml;ckgehen
              in die ... optimale Ruhestellung, mit dem der Gesunde zuf&auml;llige,
              nicht mehr gebrauchte und unbequeme K&ouml;rperstellungen zu beenden
              pflegt ... Trotz Wachsein ist der Patient unf&auml;hig zu sprechen,
              zu erkennen, sinnvolle Handlungen erlernter Art durchzuf&uuml;hren.
              Dagegen sind bestimmte vegetative Elementarfunktionen wie etwa
              das Schlucken erhalten. Daneben treten die bekannten, fr&uuml;hen
            Tiefenreflexe, wie Saugreflex, Greifreflex hervor.&quot;<br>
            </span><span class="text">              <br>
              </span><span class="&uuml;berschrift"><img src="images-caritas-mannheim/caritas_mannheim_iconpunkt.gif" width="12" height="12"> Soziales
                Problem</span><span class="&uuml;berschrift">:</span><span class="text"><br>
                <br>
                Wird ein Unfallopfer mit starken Hirnverletzungen eingeliefert
                oder wenn ein Herz-Kreislaufstillstand &uuml;ber l&auml;ngere
                Zeit der Fall ist, dann sind die &Auml;rzte verpflichtet, intensivmedizinische
                Ma&szlig;nahmen wie Wiederbelebung und den Einsatz von Beatmungsmaschinen
                einzuleiten. Viele Menschen, auch einige die aus dem Koma wiederaufgewacht
                sind, verdanken diesen Ma&szlig;nahmen ihr Leben. Andererseits
                sind viele Menschen nach solch einer intensivmedizinischen Behandlung
                Apalliker geworden.<br>
                Die Angeh&ouml;rigen von Apallikern leiden sehr unter dem irreversiblen
                Zustand des Betroffenen und machen sich sehr lange Hoffnung auf
                eine Besserung. Vor allem der Blick der Apalliker l&auml;sst
                Angeh&ouml;rige hoffen oft auf eine Heilungschance, die aber
                nicht gegeben ist. Das Verh&auml;ltnis der Angeh&ouml;rigen zu
                den &Auml;rzten wird oft sehr schwierig. Hier ist Begleitung
                und Beratung der Angeh&ouml;rigen und der &Auml;rzte n&ouml;tig.
                Gerade die Angeh&ouml;rigen leiden sehr unter der Situation und
                bilden oftmals psychosamatische St&ouml;rungen wie Migr&auml;ne
                und Depressionen aus und bed&uuml;rfen deshalb seelsorglicher
                und psychologischer Betreuung. Oftmals kommen finazielle Probleme
                aufgrund der hohen Pflegkosten hinzu. Die Krankenkassen m&uuml;ssen
                jedoch die vollen Pflegekosten &uuml;bernehmen. Die Pflege kann
                dabei auch in einer Einrichtung durchgef&uuml;hrt werden, die
                den Namen Pflegeheim tr&auml;gt.
                </span></p>
            <p class="text"><span class="&uuml;berschrift"><img src="images-caritas-mannheim/caritas_mannheim_iconpunkt.gif" width="12" height="12"> Ursachen:</span><br>
                <br>
                Bei Apallikern funktionert die Gro&szlig;hirnrinde mit ihren
                ca. 14 Milliarden Nervenzellen nicht mehr korrekt. Bereits wenige
                Minuten ohne Sauerstoff k&ouml;nnen diese Region dauerhaft sch&auml;digen.
                Forscher vermuten hier den Sitz der Pers&ouml;nlichkeit. Apalliker
                k&ouml;nnen im Schnitt 10 Jahre und l&auml;nger &uuml;berleben.
                In Japan wurde 110 Apalliker f&uuml;nf Jahre lang beobachtet.
                Ergebnis: Ca. 75% starben, bei 13% blieb der Zustand stabil,
                10% zeigten leichte Verbesserungen. </p>
            <p class="text"><span class="&uuml;berschrift"><img src="images-caritas-mannheim/caritas_mannheim_iconpunkt.gif" width="12" height="12"> Wer
                ist davon betroffen / Wieviel Prozent der Bev&ouml;lkerung:</span><br>
                <br>
            In Deutschland wird von ca. 10.000 Betroffenen ausgegangen.</p>
            <p class="text"><span class="&uuml;berschrift"><img src="images-caritas-mannheim/caritas_mannheim_iconpunkt.gif" width="12" height="12"> Christliche
                Motivation:</span><br>
                <br>
                Grunds&auml;tzlich ist aus christlicher Sicht jedes Leben, auch
                das schwer zu pflegende, sch&uuml;tzenswert. </p>
            <p><span class="&uuml;berschrift"><img src="images-caritas-mannheim/caritas_mannheim_iconpunkt.gif" width="12" height="12"> Konkrete
            Probleml&ouml;sung in Mannheim:</span></p>
            <p><span class="text">Der Caritasverband Mannheim e.V. bietet die
                M&ouml;glichkeit, Apalliker station&auml;r zu betreuen.<br>
              <br>
              <br>
              <br>
              </span><span class="&uuml;berschrift"><img src="images-caritas-mannheim/caritas_mannheim_iconpunkt.gif" width="12" height="12"> Interessenvertretung:              </span><span class="text"><br>
              <br>
              </span><span class="text"><br>
              <br>
              </span><font size="2"><span class="text"><b><br>
              </b></span><b><br>
                      </b>&copy; Caritasverband Mannheim e.V.<b><br>
              <a href="http://www.caritas-mannheim.de/" target="_blank">http://www.caritas-mannheim.de</a>/              </b></font></font></p></td>
          <td class="L12" width="30%">            <div align="right"> 
              <p align="right">     <font size="2" face="Arial, Helvetica, sans-serif">              </font></p>
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="5%" height="20">&nbsp;</td>
                  <td width="90%">&nbsp;</td>
                  <td width="5%">&nbsp;</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td><font size="2" face="Arial, Helvetica, sans-serif">&nbsp;</font></td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td height="236">&nbsp;</td>
                  <td><p><a href="http://maps.google.de/maps/ms?ie=UTF8&hl=de&msa=0&msid=112740146363501885729.000001128bd9e697fa096&ll=49.491378,8.464966&spn=0.028935,0.09407&t=h&z=14&om=1" target="_blank"><img src="images-caritas-mannheim/caritas_mannheim_google_maps_klein.jpg" alt="die caritas in Mannheim [ber google maps entdecken" width="325" height="224" border="0"></a></p>
                  </td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td valign="top" class="text">Erkunden Sie<a href="http://maps.google.de/maps/ms?ie=UTF8&hl=de&msa=0&msid=112740146363501885729.000001128bd9e697fa096&ll=49.491378,8.464966&spn=0.028935,0.09407&t=h&z=14&om=1" target="_blank"> hier</a> den
                      Caritasverband Mannheim e.V.                    mittels
                      interaktiver Satellitenkarten.Nutzen Sie auch                    unsere KML-Datei f&uuml;r
google earth</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td valign="top"><p class="&uuml;berschrift"><span class="&uuml;berschrift"><img src="images-caritas-mannheim/caritas_mannheim_iconpunkt.gif" width="12" height="12"></span> Buchtipps
                      zum Thema</p>
                    <p class="text">&nbsp;</p></td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td valign="top"><p><span class="&uuml;berschrift"><img src="images-caritas-mannheim/caritas_mannheim_iconpunkt.gif" width="12" height="12"> Linktipps</span></p>
                    <p><span class="text"><a href="http://www.caritas.de/6135.html" target="_blank"><br>
                    </a></span></p></td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td valign="top"><p><span class="&uuml;berschrift"><img src="images-caritas-mannheim/caritas_mannheim_iconpunkt.gif" width="12" height="12"> Zitate<br>
                        <br>
                  </span></p></td>
                  <td>&nbsp;</td>
                </tr>
              </table>
              <p align="right"><font size="2" face="Arial, Helvetica, sans-serif"><br>
                  .</font></p>
              <p><br>
              </p>
              <p class="&uuml;berschrift">&nbsp;</p>
              <p><span class="text"><a href="http://www.caritas.de/6135.html" target="_blank"><br>
                </a></span><br>
              </p>
              </div>
          </td>
          <td background="../lexikon/liturgie/boxright.gif" width="8"><img src="../lexikon/liturgie/boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8"><img src="../lexikon/liturgie/boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="../lexikon/liturgie/boxbottom.gif" colspan="2"><img src="../lexikon/liturgie/boxbottom.gif" width="8" height="8" alt=""></td>
          <td width="8"><img src="../lexikon/liturgie/boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td width="100" align="left" valign="top">&nbsp;</td>
  </tr>
</table>
</body>
</html>
