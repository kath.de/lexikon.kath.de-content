<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>


<head profile="http://dublincore.org/documents/dcq-html/">

<link rel="schema.DC" href="http://purl.org/dc/elements/1.1/">
<link rel="schema.DCTERMS" href="http://purl.org/dc/terms/">
<link href="caritas_mannheim.css" rel="stylesheet" type="text/css">

<title>Ambulante Hospizhilfe Mannheim - Caritasverband Mannheim e.V.</title>
<meta name="robots" content="index,follow,all" />
<meta name="description" content="Die Geschichte der Caritas - Erfahren Sie hier mehr">
<meta name="author" content="Caritasverband Mannheim e.V.">
<meta name="keywords" content="Caritas, Geschichte, Entstehtung">


<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="DC.title" content="Geschichte der Caritas - Caritas Mannheim">

<meta name="DC.creator" content="Die Geschichte der Caritas - Caritasverband Mannheim e.V.">
<meta name="DC.subject" content="Geschichte der Caritas">
<meta name="DC.description" content="Die Geschichte der Caritas - Erfahren Sie hier mehr">
<meta name="DC.publisher" content="Caritasverband Mannheim e.V.">
<meta name="DC.contributor" content="Caritasverband Mannheim e.V.">
<meta name="DC.date" content="2007-05-15T08:49:37+02:00" scheme="DCTERMS.W3CDTF">
<meta name="DC.type" content="Text" scheme="DCTERMS.DCMIType">
<meta name="DC.format" content="text/html" scheme="DCTERMS.IMT">
<meta name="DC.identifier"
      content="http://www.kath.de/caritasverband_Mannheim/geschichte_der_caritas.php"
      scheme="DCTERMS.URI">
<meta name="DC.source"
      content="http://www.w3.org/TR/html401/struct/global.html#h-7.4.4"
      scheme="DCTERMS.URI">
<meta name="DC.language" content="de" scheme="DCTERMS.RFC3066">
<meta name="DC.relation" content="http://dublincore.org/" scheme="DCTERMS.URI">
<meta name="DC.coverage" content="Munich" scheme="DCTERMS.TGN">
<meta name="DC.rights" content="Alle Rechte liegen beim Autor">

</head>




<body bgcolor="#fffffd" link="#FF0000" vlink="#FF3300" alink="#CCCCFF" leftmargin="6" topmargin="6" marginwidth="6" marginheight="6">
<table width="100%" border="0" cellspacing="0" cellpadding="6">
  <tr>
    <td width="100" align="left" valign="top" height="3"> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="../lexikon/liturgie/boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="../lexikon/liturgie/boxtop.gif"><img src="../lexikon/liturgie/boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="../lexikon/liturgie/boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="../lexikon/liturgie/boxtopleft.gif"><img src="../lexikon/liturgie/boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><p><b><font face="Arial, Helvetica, sans-serif">Caritas-Lexikon</font></b></p></td>
          <td background="../lexikon/liturgie/boxtopright.gif"><img src="../lexikon/liturgie/boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="../lexikon/liturgie/boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="../lexikon/liturgie/boxdivider.gif"><img src="../lexikon/liturgie/boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="../lexikon/liturgie/boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="../lexikon/liturgie/boxleft.gif"><img src="../lexikon/liturgie/boxleft.gif" width="8" height="8" alt=""></td>
          <td> <?php include("logo.html"); ?> </td>
          <td background="../lexikon/liturgie/boxright.gif"><img src="../lexikon/liturgie/boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="../lexikon/liturgie/boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="../lexikon/liturgie/boxbottom.gif"><img src="../lexikon/liturgie/boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="../lexikon/liturgie/boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
	  <br>
	  <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="../lexikon/liturgie/boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="../lexikon/liturgie/boxtop.gif"><img src="../lexikon/liturgie/boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="../lexikon/liturgie/boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="../lexikon/liturgie/boxtopleft.gif"><img src="../lexikon/liturgie/boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><font face="Arial, Helvetica, sans-serif"><strong>Inhaltsverzeichnis
                <br>
                Caritas-Lexikon
          </strong></font></td>
          <td background="../lexikon/liturgie/boxtopright.gif"><img src="../lexikon/liturgie/boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="../lexikon/liturgie/boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="../lexikon/liturgie/boxdivider.gif"><img src="../lexikon/liturgie/boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="../lexikon/liturgie/boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="../lexikon/liturgie/boxleft.gif"><img src="../lexikon/liturgie/boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10"> <?php include("az.html"); ?> </td>
          <td background="../lexikon/liturgie/boxright.gif"><img src="../lexikon/liturgie/boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="../lexikon/liturgie/boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="../lexikon/liturgie/boxbottom.gif"><img src="../lexikon/liturgie/boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="../lexikon/liturgie/boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
    <td rowspan="2" valign="top">
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="../lexikon/liturgie/boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td background="../lexikon/liturgie/boxtop.gif" colspan="2"><img src="../lexikon/liturgie/boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="../lexikon/liturgie/boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="../lexikon/liturgie/boxtopleft.gif" width="8"><img src="../lexikon/liturgie/boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"> 
            <h1>Ambulante Hospizhilfe Mannheim</h1>
          </td>
          <td bgcolor="#E2E2E2"><img src="images-caritas-mannheim/caritas_mannheim_image_pdf.jpg" width="30" height="30"> <span class="link"><font color="#000000">Als
          PDF-Datei ausgeben</font></span></td>
          <td background="../lexikon/liturgie/boxtopright.gif" width="8"><img src="../lexikon/liturgie/boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8"><img src="../lexikon/liturgie/boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="../lexikon/liturgie/boxdivider.gif" colspan="2"><img src="../lexikon/liturgie/boxdivider.gif" alt="" width="8" height="13"></td>
          <td width="8"><img src="../lexikon/liturgie/boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8" height="2187" background="../lexikon/liturgie/boxleft.gif"><img src="../lexikon/liturgie/boxleft.gif" width="8" height="8" alt=""></td>
          <td class="L12" width="70%">		  <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="48%"><span class="&uuml;berschrift"><img src="images-caritas-mannheim/caritas_mannheim_iconpunkt.gif" width="12" height="12"> Begriffsbestimmung:</span><span class="text"></span> </td>
              <td valign=middle width="52%"><div align="right"></div></td>
            </tr>
          </table>            
            <p class="text">		      Ambulant meint im Gegensatz zu station&auml;r,
              dass der Patient zu Hause gepflegt wird und nicht in einem Krankenhaus.
              Ambulant ist Lateinisch und bedeutet &quot;umher gehend&quot;, &quot;spazieren
              gehend&quot;. Hospiz (vom lateinische hospes Gastfreund/Fremdling
              ) meint eigentlich eine (kirchliche) Pilgerherberge wird aber in
              der Gegenwart f&uuml;r eine Pflege(einrichtung) benutzt, die sich
              speziell auf Sterbende Menschen spezialisiert hat, benutzt. <span class="text"><br>
            </span><span class="text">              <br>
              </span><span class="&uuml;berschrift"><img src="images-caritas-mannheim/caritas_mannheim_iconpunkt.gif" width="12" height="12"> Soziales
                Problem</span><span class="&uuml;berschrift">:</span><span class="text"><br>
                <br>
                Umfragen zufolge m&ouml;chten 90% aller Menschen gerne zuhause
                sterben. Jedoch sterben 50% in Krankenh&auml;usern und ca. 20%
                in Pflegeheimen und geraten somit auf Ihrem letzten Weg in eine
                fremde Umgebung und in soziale Isolation.</span></p>
            <p class="text"><span class="&uuml;berschrift"><img src="images-caritas-mannheim/caritas_mannheim_iconpunkt.gif" width="12" height="12"> Ursachen:</span><br>
                <br>
                In christlicher Sicht ist es unbedingt notwenig auch dem sterbenden
                Menschen W&uuml;rde zu erm&ouml;glichen, da er immer ein Abbild
                Gottes bleibt. Dazu geh&ouml;rt es auch, ihn in seinen Bed&uuml;rfnissen
                ernst zu nehmen. Ein starkes Bed&uuml;rfnis vieler alter Menschen
                ist es, in den eigenen, bekannten vier W&auml;nden zu sterben.
                Die Zeit des Abschiednehmens ist sowohl f&uuml;r den Sterbenden
                selbst als auch f&uuml;r die Verwandten und Freunde eine schwierige
                Zeit voller &Auml;ngste, Krisen und Unsicherheiten. Deshalb werden
                neben den Sterbenden auch die nahestehenden Personen in die professionelle
                Sterbebegleitung miteinbezogen.<br>
            </p>
            <p class="text"><span class="&uuml;berschrift"><img src="images-caritas-mannheim/caritas_mannheim_iconpunkt.gif" width="12" height="12"> Wer
                ist davon betroffen / Wieviel Prozent der Bev&ouml;lkerung:</span><br>
                <br>
            In Deutschland wird von ca. 10.000 Betroffenen ausgegangen.</p>
            <p class="text"><span class="&uuml;berschrift"><img src="images-caritas-mannheim/caritas_mannheim_iconpunkt.gif" width="12" height="12"> Christliche
                Motivation:</span><br>
                <br>
                In christlicher Sicht ist es unbedingt notwenig auch dem sterbenden
                Menschen W&uuml;rde zu erm&ouml;glichen, da er immer ein Abbild
                Gottes bleibt. Dazu geh&ouml;rt es auch, ihn in seinen Bed&uuml;rfnissen
                ernst zu nehmen. Ein starkes Bed&uuml;rfnis vieler alter Menschen
                ist es, in den eigenen, bekannten vier W&auml;nden zu sterben.
                Die Zeit des Abschiednehmens ist sowohl f&uuml;r den Sterbenden
                selbst als auch f&uuml;r die Verwandten und Freunde eine schwierige
                Zeit voller &Auml;ngste, Krisen und Unsicherheiten. Deshalb werden
                neben den Sterbenden auch die nahestehenden Personen in die professionelle
                Sterbebegleitung miteinbezogen.</p>
            <p><span class="&uuml;berschrift"><img src="images-caritas-mannheim/caritas_mannheim_iconpunkt.gif" width="12" height="12"> Konkrete
            Probleml&ouml;sung in Mannheim:</span></p>
            <p><span class="text">Die ambulante Hospizhilfe unterst&uuml;zt Angeh&ouml;rige
                bzw. die Sterbenden selbst bei Ihrem Wunsch, zu Hause den letzten
                Weg zu gehen.<br>
              In Mannheim stehen f&uuml;r die ambulante Hospizpflege die &Ouml;kumenische
              Hospizhilfe (LINK) und die Kath. Sozialstation Mannheim S&uuml;d-Ost
              e.V. zur Verf&uuml;gung. <br>
              Die Mitarbeiter leisten eine Vielzahl von Diensten bei den Alten
              zu Hause:<br>
              &#8226; 
              Medizinische Versorgung (Spritzen geben, Verband wechseln, Blutdruck
              und Blutzucker messen, Medikamentenvergabe, Wundpflege, k&uuml;nstliche
              Ern&auml;hrung,etc)<br>
              &#8226; 
              Betreuung der Alten Menschen (K&ouml;rperpflege, Hilfe beim An-
              und Auskleiden, Hilfe beim Essen und trinken, Lagern, Betten und
              Umbetten)<br>
              &#8226; 
              Hilfe im Haushalt (Einkaufen, Betreuung bei einem Krankenhausaufenthalt)<br>
              &#8226; 
              Fragen der Angeh&ouml;rigen bzgl. der Pflege<br>
              &#8226; 
              Verleih von Pflegemitteln<br>
              &#8226; 
              Kurse f&uuml;r Angeh&ouml;rige zur Pflege Ihrer Verwandten<br>
              &#8226; 
              Betreuung der Verwandten mit ihren ganz eigenen &Auml;ngsten und
              Hilflosigkeiten bei der Begleitung von Sterbenden<br>
              &#8226; 
              Seelsorgerische Begleitung der Alten und deren Angeh&ouml;rige<br>
              &#8226; 
              Psychosoziale Begleitung der Alten und deren Angeh&ouml;rige<br>
              &#8226; 
              Zuh&ouml;ren, Vorlesen, Spazieren gehen, beten<br>
              &#8226; 
              Auf die individuellen W&uuml;nsche der Alten eingehen<br>
              &#8226; 
              Zusammenarbeit und Vernetzung mit den Pflegern, &Auml;rzten, Seelsorgern,
              der Schmerzambulanz, der Palliativstation, den Krankenh&auml;usern,
              den Pflegeeinrichtungen etc. Vor Ort<br>
              &#8226; 
              Eintreten f&uuml;r eine &ouml;ffentliche Bewusstseinsbildung f&uuml;r
              das Thema Ambulante Hospizhilfe, Sterben zu Hause.<br>
              <br>
              <br>
              <br>
              <br>
              </span><span class="&uuml;berschrift"><img src="images-caritas-mannheim/caritas_mannheim_iconpunkt.gif" width="12" height="12"> Interessenvertretung:              </span><span class="text"><br>
              <br>
              </span><span class="text"><br>
              <br>
              </span><font size="2"><span class="text"><b><br>
              </b></span><b><br>
                      </b>&copy; Caritasverband Mannheim e.V.<b><br>
              <a href="http://www.caritas-mannheim.de/" target="_blank">http://www.caritas-mannheim.de</a>/              </b></font></font></p></td>
          <td class="L12" width="30%">            <div align="right"> 
              <p align="right">     <font size="2" face="Arial, Helvetica, sans-serif">              </font></p>
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="5%" height="20">&nbsp;</td>
                  <td width="90%">&nbsp;</td>
                  <td width="5%">&nbsp;</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td><font size="2" face="Arial, Helvetica, sans-serif">&nbsp;</font></td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td height="236">&nbsp;</td>
                  <td><p><a href="http://maps.google.de/maps/ms?ie=UTF8&hl=de&msa=0&msid=112740146363501885729.000001128bd9e697fa096&ll=49.491378,8.464966&spn=0.028935,0.09407&t=h&z=14&om=1" target="_blank"><img src="images-caritas-mannheim/caritas_mannheim_google_maps_klein.jpg" alt="die caritas in Mannheim [ber google maps entdecken" width="325" height="224" border="0"></a></p>
                  </td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td valign="top" class="text">Erkunden Sie<a href="http://maps.google.de/maps/ms?ie=UTF8&hl=de&msa=0&msid=112740146363501885729.000001128bd9e697fa096&ll=49.491378,8.464966&spn=0.028935,0.09407&t=h&z=14&om=1" target="_blank"> hier</a> den
                      Caritasverband Mannheim e.V.                    mittels
                      interaktiver Satellitenkarten.Nutzen Sie auch                    unsere KML-Datei f&uuml;r
google earth</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td valign="top"><p class="&uuml;berschrift"><span class="&uuml;berschrift"><img src="images-caritas-mannheim/caritas_mannheim_iconpunkt.gif" width="12" height="12"></span> Buchtipps
                      zum Thema</p>
                    <p class="text">&nbsp;</p></td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td valign="top"><p><span class="&uuml;berschrift"><img src="images-caritas-mannheim/caritas_mannheim_iconpunkt.gif" width="12" height="12"> Linktipps</span></p>
                    <p><span class="text"><a href="http://www.caritas.de/6135.html" target="_blank"><br>
                    </a></span></p></td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td valign="top"><p><span class="&uuml;berschrift"><img src="images-caritas-mannheim/caritas_mannheim_iconpunkt.gif" width="12" height="12"> Zitate<br>
                        <br>
                  </span></p></td>
                  <td>&nbsp;</td>
                </tr>
              </table>
              <p align="right"><font size="2" face="Arial, Helvetica, sans-serif"><br>
                  .</font></p>
              <p><br>
              </p>
              <p class="&uuml;berschrift">&nbsp;</p>
              <p><span class="text"><a href="http://www.caritas.de/6135.html" target="_blank"><br>
                </a></span><br>
              </p>
              </div>
          </td>
          <td background="../lexikon/liturgie/boxright.gif" width="8"><img src="../lexikon/liturgie/boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8"><img src="../lexikon/liturgie/boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="../lexikon/liturgie/boxbottom.gif" colspan="2"><img src="../lexikon/liturgie/boxbottom.gif" width="8" height="8" alt=""></td>
          <td width="8"><img src="../lexikon/liturgie/boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td width="100" align="left" valign="top">&nbsp;</td>
  </tr>
</table>
</body>
</html>
