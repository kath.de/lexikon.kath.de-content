<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>


<head profile="http://dublincore.org/documents/dcq-html/">

<link rel="schema.DC" href="http://purl.org/dc/elements/1.1/">
<link rel="schema.DCTERMS" href="http://purl.org/dc/terms/">
<link href="caritas_mannheim.css" rel="stylesheet" type="text/css">

<title>Psychisch Kranke Mannheim - Caritasverband Mannheim e.V.</title>
<meta name="robots" content="index,follow,all" />
<meta name="description" content="Die Geschichte der Caritas - Erfahren Sie hier mehr">
<meta name="author" content="Caritasverband Mannheim e.V.">
<meta name="keywords" content="Caritas, Geschichte, Entstehtung">


<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="DC.title" content="Geschichte der Caritas - Caritas Mannheim">

<meta name="DC.creator" content="Die Geschichte der Caritas - Caritasverband Mannheim e.V.">
<meta name="DC.subject" content="Geschichte der Caritas">
<meta name="DC.description" content="Die Geschichte der Caritas - Erfahren Sie hier mehr">
<meta name="DC.publisher" content="Caritasverband Mannheim e.V.">
<meta name="DC.contributor" content="Caritasverband Mannheim e.V.">
<meta name="DC.date" content="2007-05-15T08:49:37+02:00" scheme="DCTERMS.W3CDTF">
<meta name="DC.type" content="Text" scheme="DCTERMS.DCMIType">
<meta name="DC.format" content="text/html" scheme="DCTERMS.IMT">
<meta name="DC.identifier"
      content="http://www.kath.de/caritasverband_Mannheim/geschichte_der_caritas.php"
      scheme="DCTERMS.URI">
<meta name="DC.source"
      content="http://www.w3.org/TR/html401/struct/global.html#h-7.4.4"
      scheme="DCTERMS.URI">
<meta name="DC.language" content="de" scheme="DCTERMS.RFC3066">
<meta name="DC.relation" content="http://dublincore.org/" scheme="DCTERMS.URI">
<meta name="DC.coverage" content="Munich" scheme="DCTERMS.TGN">
<meta name="DC.rights" content="Alle Rechte liegen beim Autor">

</head>




<body bgcolor="#fffffd" link="#FF0000" vlink="#FF3300" alink="#CCCCFF" leftmargin="6" topmargin="6" marginwidth="6" marginheight="6">
<table width="100%" border="0" cellspacing="0" cellpadding="6">
  <tr>
    <td width="100" align="left" valign="top" height="3"> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="../lexikon/liturgie/boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="../lexikon/liturgie/boxtop.gif"><img src="../lexikon/liturgie/boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="../lexikon/liturgie/boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="../lexikon/liturgie/boxtopleft.gif"><img src="../lexikon/liturgie/boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><p><b><font face="Arial, Helvetica, sans-serif">Caritas-Lexikon</font></b></p></td>
          <td background="../lexikon/liturgie/boxtopright.gif"><img src="../lexikon/liturgie/boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="../lexikon/liturgie/boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="../lexikon/liturgie/boxdivider.gif"><img src="../lexikon/liturgie/boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="../lexikon/liturgie/boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="../lexikon/liturgie/boxleft.gif"><img src="../lexikon/liturgie/boxleft.gif" width="8" height="8" alt=""></td>
          <td> <?php include("logo.html"); ?> </td>
          <td background="../lexikon/liturgie/boxright.gif"><img src="../lexikon/liturgie/boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="../lexikon/liturgie/boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="../lexikon/liturgie/boxbottom.gif"><img src="../lexikon/liturgie/boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="../lexikon/liturgie/boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
	  <br>
	  <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="../lexikon/liturgie/boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="../lexikon/liturgie/boxtop.gif"><img src="../lexikon/liturgie/boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="../lexikon/liturgie/boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="../lexikon/liturgie/boxtopleft.gif"><img src="../lexikon/liturgie/boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><font face="Arial, Helvetica, sans-serif"><strong>Inhaltsverzeichnis
                <br>
                Caritas-Lexikon
          </strong></font></td>
          <td background="../lexikon/liturgie/boxtopright.gif"><img src="../lexikon/liturgie/boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="../lexikon/liturgie/boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="../lexikon/liturgie/boxdivider.gif"><img src="../lexikon/liturgie/boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="../lexikon/liturgie/boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="../lexikon/liturgie/boxleft.gif"><img src="../lexikon/liturgie/boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10"> <?php include("az.html"); ?> </td>
          <td background="../lexikon/liturgie/boxright.gif"><img src="../lexikon/liturgie/boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="../lexikon/liturgie/boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="../lexikon/liturgie/boxbottom.gif"><img src="../lexikon/liturgie/boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="../lexikon/liturgie/boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
    <td rowspan="2" valign="top">
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="../lexikon/liturgie/boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td background="../lexikon/liturgie/boxtop.gif" colspan="2"><img src="../lexikon/liturgie/boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="../lexikon/liturgie/boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="../lexikon/liturgie/boxtopleft.gif" width="8"><img src="../lexikon/liturgie/boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"> 
            <h1><font face="Arial, Helvetica, sans-serif">Psychisch Kranke Mannheim</font> </h1>
          </td>
          <td bgcolor="#E2E2E2"><img src="images-caritas-mannheim/caritas_mannheim_image_pdf.jpg" width="30" height="30"> <span class="link"><font color="#000000">Als
          PDF-Datei ausgeben</font></span></td>
          <td background="../lexikon/liturgie/boxtopright.gif" width="8"><img src="../lexikon/liturgie/boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8"><img src="../lexikon/liturgie/boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="../lexikon/liturgie/boxdivider.gif" colspan="2"><img src="../lexikon/liturgie/boxdivider.gif" alt="" width="8" height="13"></td>
          <td width="8"><img src="../lexikon/liturgie/boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8" height="2187" background="../lexikon/liturgie/boxleft.gif"><img src="../lexikon/liturgie/boxleft.gif" width="8" height="8" alt=""></td>
          <td class="L12" width="70%">		  <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="48%"><span class="&uuml;berschrift"><img src="images-caritas-mannheim/caritas_mannheim_iconpunkt.gif" width="12" height="12"> Begriffsbestimmung:</span><span class="text"></span> </td>
              <td valign=middle width="52%"><div align="right"></div></td>
            </tr>
          </table>            
            <p><span class="text">		      Psychisch Kranke werden oftmals
                auch als Psychisch gest&ouml;rte bezeichnet. Es handelt sich
                dabei um Erkrankungen der Seele, die sich nicht auf eine direkte
                k&ouml;rperliche Ursache zur&uuml;ckf&uuml;hren lassen. <br>
            <br>
            </span><span class="&uuml;berschrift"><img src="images-caritas-mannheim/caritas_mannheim_iconpunkt.gif" width="12" height="12"> Soziales
            Problem</span><span class="&uuml;berschrift">:</span><span class="text"><br>
              <br>
              Viele psychische Krankheiten sind heute gut behandelbar. Jedoch
              ist eine genaue Abw&auml;gung bei der Behandlung geboten. Viele
              psychische Erkrankungen entwickeln sich als Schutzfunktion und &uuml;bernehmen
              diese Rolle auch weiterhin. Deshalb ist eine Behandlung unter Umst&auml;nden
              nur mit einer eingehenden Verhaltens&auml;nderung m&ouml;glich.
              Einige psaychische Krankheiten stehen auch in einem direktem Zusammenhang
              mit den Lebensumst&auml;nden und dem sozialen Umfeld, so dass bei
              der Behanldung das Umfeld miteinbezogen werden muss. <br>
              Psychisch Kranke werden durch die Medien in der &Ouml;ffentlichkeit
              h&auml;ufig stigmatisiert. Darunter haben die Erkrankten dann doppelt
              zu leiden, so dass eine Reintegration erschwert wird. Die Signale,
              die die Umwelt an psaychisch Kranke sendet sind h&auml;ufig in
              der Art &#8222;Wir wollen dich so nicht.&#8220; Dies ist schon
              f&uuml;r einen gesunden Menschen eine schwere emotionale B&uuml;rde,
              um so mehr noch f&uuml;r den psychisch Erkrankten. Deswegen ist
              eine gezielte Information der &Ouml;ffentlichkeit wichtig.</span></p>
            <p class="text"><span class="&uuml;berschrift"><img src="images-caritas-mannheim/caritas_mannheim_iconpunkt.gif" width="12" height="12"> Ursachen:</span><br>
                <br>
                Die Ursachen psychischer Erkrankungen sind vielf&auml;ltig. Oftmals
                spielt die Umwelt des Erkrankten eine Rolle. Dementsprechend
              kann sie auch eine Rolle beim Heilungsprozess spielen.              </p>
            <p class="text"><span class="&uuml;berschrift"><img src="images-caritas-mannheim/caritas_mannheim_iconpunkt.gif" width="12" height="12"> Wer
                ist davon betroffen / Wieviel Prozent der Bev&ouml;lkerung:</span><br>
                <br>
                Sch&auml;tzungen der WHO gehen davon aus, dass fast jeder vierte
                Arztbesucher unter behandlungsbed&uuml;rftigen psychischen St&ouml;rungen
                leidet. Jeder Vierte leidet also irgendwann im Leben an seelischen
                Krankheiten. Und wiederrum rund ein Viertel der Betroffenen leidet
                unter chronischen psychischen St&ouml;rungen.<br>
                In Deutschland geht man von ca. 8 Millionen Betroffenen aus.
                Manche Mediziner sprechen sogar davon, dass psychische Erkrankungen
                zu den h&auml;ufigsten Anl&auml;ssen z&auml;hlen, um einen Mediziner
                aufzusuchen. Psyschiche St&ouml;rungen sind die 4. H&auml;ufigste
                Ursache f&uuml;r Arbeitsunf&auml;higkeit im Rahmen der Gesetzlichen
                Krankenversicherung. Die Zahl der Psychischen Erkrankungen steigt.
                1986 waren von 1000 station&auml;r zu behandelnden Patienten
                3,8 F&auml;lle psyhischer Erkrankung darunter. 2005 waren es
                9,5 F&auml;lle.            </p>
            <p class="text"><span class="&uuml;berschrift"><img src="images-caritas-mannheim/caritas_mannheim_iconpunkt.gif" width="12" height="12"> Christliche
                Motivation:</span><br>
                <br>
                Bereits Jesus selbst zeigt durch seine Wunder, dass der Mensch
              an K&ouml;rper und Seele gesund sein soll. Jesus selbst benutzt
              schon ein ganzheitliches Heilungskonzept, welches den Menschen
              an K&ouml;rper, Geist und Seele gesunden l&auml;sst.</p>
            <p><span class="&uuml;berschrift"><img src="images-caritas-mannheim/caritas_mannheim_iconpunkt.gif" width="12" height="12"> Konkrete
                Probleml&ouml;sung in Mannheim:</span><span class="text"><br>
                <br>
                Der  Caritasverband Mannheim bietet eine F&uuml;lle an M&ouml;glichkeiten
                f&uuml;r psychisch erkrankte Menschen an. In speziellen Wohnheimen
                werden psychisch erkrankte Menschen intensiv betreut und gepflegt.
                Dazu geh&ouml;ren alle medizinischen Aufgaben bis hin zu einer
                sinnvollen Strukturierung des Tagesablaufes. In anderen Einrichtungen
                in Mannheim, wie in der Bruchsaler Strasse (LINK google earth)
                leben die psychisch Erkrankten schon selbstst&auml;ndiger. Der
                Gedanke der Wiedereingliederung und der Erlangung von Selbstst&auml;ndigkeit
                in der Bew&auml;ltigung des Alltags stehen dabei im Vordergrund.
                Mitarbeiter stehen dennoch zur Versorgung und Hilfe bereit. <br>
                Betreuungspl&auml;tze f&uuml;r alleinlebende Menschen mit einer
                psychischen Erkrankung sind ebenfalls in Mannheim m&ouml;glich.
                Hierbei werden sowohl die Angeh&ouml;rigen als auch die Betroffenen
                in Einzel- und Familiengespr&auml;chen beraten. <br>
                <br>
              <br>
              </span><span class="&uuml;berschrift"><img src="images-caritas-mannheim/caritas_mannheim_iconpunkt.gif" width="12" height="12"> Interessenvertretung:              </span><span class="text"><br>
              <br>
              </span><span class="text"><br>
              <br>
              </span><font size="2"><span class="text"><b><br>
              </b></span><b><br>
                      </b>&copy; Caritasverband Mannheim e.V.<b><br>
            <a href="http://www.caritas-mannheim.de/" target="_blank">http://www.caritas-mannheim.de</a>/              </b></font></font></p>
            </td>
          <td class="L12" width="30%">            <div align="right"> 
              <p align="right">     <font size="2" face="Arial, Helvetica, sans-serif">              </font></p>
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="5%" height="20">&nbsp;</td>
                  <td width="90%"><font size="2" face="Arial, Helvetica, sans-serif"><img src="images-caritas-mannheim/caritas_mannheim_geschichte_der_caritas_1.jpg" alt="Die Geschichte der Caritas Mannheim " width="310" height="229"></font></td>
                  <td width="5%">&nbsp;</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td valign="top"><font size="2" face="Arial, Helvetica, sans-serif">&nbsp;</font></td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td height="236">&nbsp;</td>
                  <td><p><a href="http://maps.google.de/maps/ms?ie=UTF8&hl=de&msa=0&msid=112740146363501885729.000001128bd9e697fa096&ll=49.491378,8.464966&spn=0.028935,0.09407&t=h&z=14&om=1" target="_blank"><img src="images-caritas-mannheim/caritas_mannheim_google_maps_klein.jpg" alt="die caritas in Mannheim [ber google maps entdecken" width="325" height="224" border="0"></a></p>
                  </td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td valign="top" class="text">Erkunden Sie<a href="http://maps.google.de/maps/ms?ie=UTF8&hl=de&msa=0&msid=112740146363501885729.000001128bd9e697fa096&ll=49.491378,8.464966&spn=0.028935,0.09407&t=h&z=14&om=1" target="_blank"> hier</a> den
                      Caritasverband Mannheim e.V.                    mittels
                      interaktiver Satellitenkarten.Nutzen Sie auch                    unsere KML-Datei f&uuml;r
google earth</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td valign="top"><p class="&uuml;berschrift"><span class="&uuml;berschrift"><img src="images-caritas-mannheim/caritas_mannheim_iconpunkt.gif" width="12" height="12"></span> Buchtipps
                      zum Thema</p>
                    <p class="text">&nbsp;</p></td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td valign="top"><p><span class="&uuml;berschrift"><img src="images-caritas-mannheim/caritas_mannheim_iconpunkt.gif" width="12" height="12"> Linktipps</span></p>
                    <p><span class="text"><a href="http://www.caritas.de/6135.html" target="_blank"><br>
                    </a></span></p></td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td valign="top"><p><span class="&uuml;berschrift"><img src="images-caritas-mannheim/caritas_mannheim_iconpunkt.gif" width="12" height="12"> Zitate<br>
                        <br>
                  </span></p></td>
                  <td>&nbsp;</td>
                </tr>
              </table>
              <p align="right"><font size="2" face="Arial, Helvetica, sans-serif"><br>
                  .</font></p>
              <p><br>
              </p>
              <p class="&uuml;berschrift">&nbsp;</p>
              <p><span class="text"><a href="http://www.caritas.de/6135.html" target="_blank"><br>
                </a></span><br>
              </p>
              </div>
          </td>
          <td background="../lexikon/liturgie/boxright.gif" width="8"><img src="../lexikon/liturgie/boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8"><img src="../lexikon/liturgie/boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="../lexikon/liturgie/boxbottom.gif" colspan="2"><img src="../lexikon/liturgie/boxbottom.gif" width="8" height="8" alt=""></td>
          <td width="8"><img src="../lexikon/liturgie/boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td width="100" align="left" valign="top">&nbsp;</td>
  </tr>
</table>
</body>
</html>
