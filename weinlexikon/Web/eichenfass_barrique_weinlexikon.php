<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<head profile="http://dublincore.org/documents/dcq-html/">

<link rel="schema.DC" href="http://purl.org/dc/elements/1.1/">
<link rel="schema.DCTERMS" href="http://purl.org/dc/terms/">
<link href="weinlexikon.css" rel="stylesheet" type="text/css">

<title>Eichenfass Barrique Pi&egrave;ce:: Weinlexikon :: Weine ::</title>
<meta name="robots" content="index,follow,all" />
<meta name="description" content="Das Weinlexikon mit wichtigen und interessanten Hintergrundinformationen zu Weinen">
<meta name="author" content="Weinlexikon">
<meta name="keywords" content="Weinlexikon, Wein">


<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="DC.title" content="Weinlexikon">

<meta name="DC.creator" content="Weinlexikon">
<meta name="DC.subject" content="Kurzzeitpflege in Mannheim - Caritas Mannheim">
<meta name="DC.description" content="Weinlexikon">
<meta name="DC.publisher" content="Weinlexikon">
<meta name="DC.contributor" content="Weinlexikon">
<meta name="DC.date" content="2007-05-15T08:49:37+02:00" scheme="DCTERMS.W3CDTF">
<meta name="DC.type" content="Text" scheme="DCTERMS.DCMIType">
<meta name="DC.format" content="text/html" scheme="DCTERMS.IMT">
<meta name="DC.identifier"
      content="http://www.kath.de/weinlexikon"
      scheme="DCTERMS.URI">
<meta name="DC.source"
      content="http://www.w3.org/TR/html401/struct/global.html#h-7.4.4"
      scheme="DCTERMS.URI">
<meta name="DC.language" content="de" scheme="DCTERMS.RFC3066">
<meta name="DC.relation" content="http://dublincore.org/" scheme="DCTERMS.URI">
<meta name="DC.coverage" content="Munich" scheme="DCTERMS.TGN">
<meta name="DC.rights" content="Alle Rechte liegen beim Autor">
</head>

<body background="images-weinlexikon-wein/dstone1.gif" link="#ED5105" vlink="#666666" alink="#FF0000">
<table width="800" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="6">
      <tr>
        <td width="100" align="left" valign="top">
          <table width="216" border="0" cellpadding="0" cellspacing="0">
            <tr valign="top" align="left">
              <td width="8"><img src="boxtopleftcorner.jpg" width="8" height="8" alt=""></td>
              <td width="200" background="boxtop.jpg"><img src="boxtop.jpg" alt="" width="8" height="8"></td>
              <td width="8"><img src="boxtoprightcorner.jpg" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="boxtopleft.jpg"><img src="boxtopleft.jpg" width="8" height="8" alt=""></td>
              <td bgcolor="c9dc11"><p><b><font color="#333333" face="Arial, Helvetica, sans-serif">Weinlexikon
                      :: Weine</font></b></p>
              </td>
              <td background="boxtopright.jpg"><img src="boxtopright.jpg" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="boxdividerleft.jpg" width="8" height="13" alt=""></td>
              <td background="boxdivider.jpg"><img src="boxdivider.jpg" alt="" width="8" height="13"></td>
              <td><img src="boxdividerright.jpg" width="8" height="13" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
              <td bgcolor="#FFFFFF">
                <?php include("logo.html"); ?>
              </td>
              <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
              <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
              <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
            </tr>
          </table>
          <br>
          <table width="216" border="0" cellpadding="0" cellspacing="0">
            <tr valign="top" align="left">
              <td width="8"><img src="boxtopleftcorner.jpg" width="8" height="8" alt=""></td>
              <td width="200" background="boxtop.jpg"><img src="boxtop.jpg" alt="" width="8" height="8"></td>
              <td width="8"><img src="boxtoprightcorner.jpg" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="boxtopleft.jpg"><img src="boxtopleft.jpg" width="8" height="8" alt=""></td>
              <td bgcolor="c9dc11"><font color="#333333" face="Arial, Helvetica, sans-serif"><strong>Inhaltsverzeichnis </strong></font></td>
              <td background="boxtopright.jpg"><img src="boxtopright.jpg" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="boxdividerleft.jpg" width="8" height="13" alt=""></td>
              <td background="boxdivider.jpg"><img src="boxdivider.jpg" alt="" width="8" height="13"></td>
              <td><img src="boxdividerright.jpg" width="8" height="13" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
              <td bgcolor="#FFFFFF" class="V10">
                <?php include("az.html"); ?>
              </td>
              <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
              <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
              <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
            </tr>
          </table>
        </td>
        <td rowspan="2" valign="top">          <table width="100%" height="1369" border="0" cellpadding="0" cellspacing="0">
          <tr valign="top" align="left">
            <td width="8"><img src="boxtopleftcorner.jpg" width="8" height="8" alt=""></td>
            <td background="boxtop.jpg"><img src="boxtop.jpg" alt="" width="8" height="8"></td>
            <td width="8"><img src="boxtoprightcorner.jpg" width="8" height="8" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td background="boxtopleft.jpg" width="8"><img src="boxtopleft.jpg" width="8" height="8" alt=""></td>
            <td bgcolor="c9dc11">
              <h1><font color="#333333" face="Arial, Helvetica, sans-serif">Eichenfass,
                  <strong>Barrique, Pi&egrave;ce</strong></font></h1>
            </td>
            <td background="boxtopright.jpg" width="8"><img src="boxtopright.jpg" width="8" height="8" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td width="8"><img src="boxdividerleft.jpg" width="8" height="13" alt=""></td>
            <td background="boxdivider.jpg"><img src="boxdivider.jpg" alt="" width="8" height="13"></td>
            <td width="8"><img src="boxdividerright.jpg" width="8" height="13" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td width="8" height="905" background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
            <td bgcolor="#FFFFFF" class="L12">
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="48%">&nbsp;</td>
                  <td valign=middle width="52%"><div align="right"></div>
                  </td>
                </tr>
              </table>
              <p class="text">    <img SRC="images-weinlexikon-wein/barrique_eichenfass_weinlexikon.jpg" alt="Eichenfass Barrique" name="Migrationsdienst" width="250" height="188" hspace="10" vspace="10" align="right" style="border: double 4px #009999">    <strong><em>Diogenes sass in der Tonne<br>
                und sprach: So geht mir aus der Sonne!<br>
                Wars ihm zu tun um Sonnenschein,<br>
                was kroch er in die Tonne rein?<br>
                Er ist hineingekrochen,<br>
                weil sie nach Wein gerochen.</em></strong><br>
                (Volkslied) </p>
              <p class="text"><strong>Eichenfass, Barrique, Pi&egrave;ce </strong>&#8211; kaum ein Ausbauthema
                l&ouml;st in der Weinwelt mehr Diskussionen und Kontroversen
                aus. Eiche liegt zweifellos im Trend: Die weniger sensiblen Produzenten
                setzen das begehrte Aroma mit Essenzen und Sp&auml;nen zu. Andere
                st&uuml;rzen sich allj&auml;hrlich in Unkosten und geben jedem
                Jahrgang mit neuen Eichenf&auml;sschen die gefragte, feine Buketnote
                mit auf den Weg.</p>
              <p class="text">Ein Wein, der im Eichenfass gelagert wird,
                  nimmt den Duft und Geschmack des Holzes mehr oder weniger stark
                  an. Ein typisches
                Beispiel daf&uuml;r ist die Chardonnay-Traube, die sich geradezu
                mit <strong>Eichenholzduft</strong> voll saugen kann. Kleine Eichenf&auml;sschen &#8211; die
                ber&uuml;hmten Barriques mit nur 225 Litern Fassverm&ouml;gen &#8211; pr&auml;gen
                den Wein besonders stark. Je gr&ouml;sser und &auml;lter die
                F&auml;sser werden, desto schw&auml;cher kommt in der Regel die
                Eiche zum Ausdruck.</p>
              <p class="text"><span class="&uuml;berschrift">Eiche ist nicht gleich Eiche<br>
              </span><br>
  F&uuml;r die <strong>Fassproduktion</strong> stehen haupts&auml;chlich zwei Anbaugebiete
                  in Konkurrenz: Amerika und Frankreich. Eiche aus Amerika ist
                  f&uuml;r den Ausbau des Rioja begehrt, w&auml;hrend die edlen
                  franz&ouml;sischen H&ouml;lzer aus Limousin oder Allier f&uuml;r
                  die anderen Weine bevorzugt werden. Die feinste Eiche w&auml;chst
                  in den W&auml;ldern von Troncais (Allier). Holz f&uuml;r qualitativ
                  gute F&auml;sser &#8211; es stammt von 80-j&auml;hrigen Eichen &#8211; muss
                  der K&uuml;fer vor der Verarbeitung nochmals einige Monate lagern.
                  Um die Dauben in die richtige Fassform zu bringen, werden sie
                  innen gefl&auml;mmt oder getoastet. Die Art der St&auml;rke dieser
                  R&ouml;stung spiegelt in der Duftnote wider.</p>
              <p class="text"><span class="&uuml;berschrift">Eichenf&auml;sschen:
                  eine bedeutende Kostenfrage</span><br>
                  <br>
  Ein <strong>neues Barriquefass</strong> kostet 850 Franken. Der Eichenduft wird
                  aber nur bei den ersten Lagerungen abgegeben. Je neuer das
                  Beh&auml;ltnis ist, desto st&auml;rker duftet also das Holz.
                  Wird ein Fass mehr als zweimal eingesetzt, verschwindet der
                  begehrte Effekt. Eichenholzessenzen, Eichenst&auml;be oder
                  Eichensp&auml;ne sind wesentlich g&uuml;nstiger als ein neues
                  Fass. Expertinnen und Experten behaupten jedoch, das Resultat
                  sei nicht das gleiche. Bei edlen Weinen lohnt sich deshalb
                  die Nachfrage nach L&auml;nge der Fassreife, Alter des Fasses
                  und vielleicht sogar Art der Eiche. </p>
              <p><span class="&uuml;berschrift">Vom Holz zum Bukett</span><span class="text"><br>
                  <br>
                  Der seit einigen Jahren anhaltende <strong>Trend </strong>zu kr&auml;ftigen, manchmal
                sogar herben Eichenholznoten f&uuml;hrt wie bei allen &Uuml;bertreibungen
                zu zweifelhaften Ausbautricks und Qualit&auml;ten: Starker Eichenduft
                wird oft mit G&uuml;te gleichgesetzt. Grosse Weine aber schaffen
                sich ihren Namen seit Generationen durch das Gegenteil: Der vorerst
                dominante Holzduft tritt mit zunehmendem Alter zur&uuml;ck und
                verbindet sich mit den anderen Substanzen zu einem harmonischen,
                komplexen Buket. <br>
              </span> </p>
              <p><span class="text">                  </span><span class="&uuml;berschrift"><img src="images-caritas-mannheim/caritas_mannheim_iconpunkt.gif" width="12" height="12"> LINKS:</span><span class="text">                  </span></p>
              <p align="right"><font size="2"><span class="text"><b><br>
                  </b></span><b><br>
                  </b>&copy; <b><br>
                                                                                                                    <br>
                </b></font></p></td>
            <td background="boxright.gif" width="8"><img src="boxright.gif" width="8" height="8" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td width="8"><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
            <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
            <td width="8"><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td width="100" height="1374" align="left" valign="top">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
<script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
google_ad_width = 336;
google_ad_height = 280;
google_ad_format = "336x280_as";
google_ad_type = "text_image";
//2006-10-23: Lexikon Symbole
google_ad_channel = "8495253357";
//--></script>
<script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
<br>
</body>
</html>
