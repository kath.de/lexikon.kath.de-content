<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<head profile="http://dublincore.org/documents/dcq-html/">

<link rel="schema.DC" href="http://purl.org/dc/elements/1.1/">
<link rel="schema.DCTERMS" href="http://purl.org/dc/terms/">
<link href="caritas_mannheim.css" rel="stylesheet" type="text/css">

<title>chronischer Stress :: Gesundheit &amp; Pr&auml;vention ::</title>
<meta name="robots" content="index,follow,all" />
<meta name="description" content="chronischer Stress">
<meta name="author" content="Neurolab.eu">
<meta name="keywords" content="chronischer Stress">


<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="DC.title" content="chronischer Stress">

<meta name="DC.creator" content="chronischer Stress">
<meta name="DC.subject" content="chronischer Stress">
<meta name="DC.description" content="chronischer Stress">
<meta name="DC.publisher" content="Neurolab.eu">
<meta name="DC.contributor" content="Neurolab.eu">
<meta name="DC.date" content="2007-05-15T08:49:37+02:00" scheme="DCTERMS.W3CDTF">
<meta name="DC.type" content="Text" scheme="DCTERMS.DCMIType">
<meta name="DC.format" content="text/html" scheme="DCTERMS.IMT">
<meta name="DC.identifier"
      content="http://www.kath.de/lexikon/praevention_gesundheit/chronischerstress-gesundheit-und-praevention.php"
      scheme="DCTERMS.URI">
<meta name="DC.source"
      content="http://www.w3.org/TR/html401/struct/global.html#h-7.4.4"
      scheme="DCTERMS.URI">
<meta name="DC.language" content="de" scheme="DCTERMS.RFC3066">
<meta name="DC.relation" content="http://dublincore.org/" scheme="DCTERMS.URI">
<meta name="DC.coverage" content="Munich" scheme="DCTERMS.TGN">
<meta name="DC.rights" content="Alle Rechte liegen beim Autor">
</head>

<body background="images-gesundheit-praevention/neurolab-background-org.jpg" link="#ED5105" vlink="#666666" alink="#FF0000">
<table width="900" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="6">
      <tr>
        <td width="216" align="left" valign="top" height="3">
          <table width="216" border="0" cellpadding="0" cellspacing="0">
            <tr valign="top" align="left">
              <td width="8"><img src="boxtopleftcorner.jpg" width="8" height="8" alt=""></td>
              <td width="200" background="boxtop.jpg"><img src="boxtop.jpg" alt="" width="8" height="8"></td>
              <td width="8"><img src="boxtoprightcorner.jpg" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="boxtopleft.jpg"><img src="boxtopleft.jpg" width="8" height="8" alt=""></td>
              <td bgcolor="f7e39f"><p><b><font color="#333333" face="Arial, Helvetica, sans-serif">Gesundheit
                      &amp; Pr&auml;vention </font></b></p>
              </td>
              <td background="boxtopright.jpg"><img src="boxtopright.jpg" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="boxdividerleft.jpg" width="8" height="13" alt=""></td>
              <td background="boxdivider.jpg"><img src="boxdivider.jpg" alt="" width="8" height="13"></td>
              <td><img src="boxdividerright.jpg" width="8" height="13" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
              <td bgcolor="#FFFFFF">
                <?php include("logo.html"); ?>
              </td>
              <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
              <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
              <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
            </tr>
          </table>
          <br>
          <table width="216" border="0" cellpadding="0" cellspacing="0">
            <tr valign="top" align="left">
              <td width="8"><img src="boxtopleftcorner.jpg" width="8" height="8" alt=""></td>
              <td width="200" background="boxtop.jpg"><img src="boxtop.jpg" alt="" width="8" height="8"></td>
              <td width="8"><img src="boxtoprightcorner.jpg" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="boxtopleft.jpg"><img src="boxtopleft.jpg" width="8" height="8" alt=""></td>
              <td bgcolor="f7e39f"><font color="#333333" face="Arial, Helvetica, sans-serif"><strong>Inhaltsverzeichnis </strong></font></td>
              <td background="boxtopright.jpg"><img src="boxtopright.jpg" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="boxdividerleft.jpg" width="8" height="13" alt=""></td>
              <td background="boxdivider.jpg"><img src="boxdivider.jpg" alt="" width="8" height="13"></td>
              <td><img src="boxdividerright.jpg" width="8" height="13" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
              <td bgcolor="#FFFFFF" class="V10">
                <?php include("az.html"); ?>
              </td>
              <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
            </tr>
            <tr valign="top" align="left">
              <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
              <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
              <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
            </tr>
          </table>
        </td>
        <td width="530" rowspan="2" valign="top">          <table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr valign="top" align="left">
            <td width="8"><img src="boxtopleftcorner.jpg" width="8" height="8" alt=""></td>
            <td background="boxtop.jpg"><img src="boxtop.jpg" alt="" width="8" height="8"></td>
            <td width="8"><img src="boxtoprightcorner.jpg" width="8" height="8" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td background="boxtopleft.jpg" width="8"><img src="boxtopleft.jpg" width="8" height="8" alt=""></td>
            <td bgcolor="f7e39f">
              <h1><font color="#333333" face="Arial, Helvetica, sans-serif">chronischer
                  Stress</font></h1>
            </td>
            <td background="boxtopright.jpg" width="8"><img src="boxtopright.jpg" width="8" height="8" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td width="8"><img src="boxdividerleft.jpg" width="8" height="13" alt=""></td>
            <td background="boxdivider.jpg"><img src="boxdivider.jpg" alt="" width="8" height="13"></td>
            <td width="8"><img src="boxdividerright.jpg" width="8" height="13" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td width="8" height="905" background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
            <td bgcolor="#FFFFFF" class="L12">
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="58%"><span class="&uuml;berschrift"><img src="images-gesundheit-praevention/caritas_mannheim_iconpunkt.gif" width="12" height="12"> Der
                      Begriff chronischer Stress</span><span class="&uuml;berschrift">:</span><span class="text"></span></td>
                  <td valign=middle width="42%"><div align="right"> <span class="link"></span></div>
                  </td>
                </tr>
              </table>
              <p><span class="text">    <img SRC="images-gesundheit-praevention/chronischer-stress-neurolab.jpg" style="border: double 4px #009999" align="right" hspace="10" vspace="10" name="chronischer Stress" alt="chronischer Stress" >    Stress
                  ist eine Reaktion des Organismus auf k&ouml;rperliche
                  und geistige Belastungen. W&auml;hrend der sogenannte akute
                  Stress nur einige Minuten bis zu wenigen Stunden anh&auml;lt,
                  kann Stress durch langfristige, dauerhafte Belastungen chronisch
                  werden.                  </span></p>
              <p class="text"><span class="&uuml;berschrift"><img src="images-gesundheit-praevention/caritas_mannheim_iconpunkt.gif" width="12" height="12"> Symptome
                  des chronischer Stress:</span><br>
                            <br>
        Durch die langanhaltenden Belastungen befindet sich der Organismus
        im dauerhaften Alarmzustand. Chronischer Stress &auml;u&szlig;ert sich
        h&auml;ufig in Unruhe, Ersch&ouml;pfung und depressiven Zust&auml;nden.
        Kopfschmerzen, Magenschmerzen, R&uuml;ckenschmerzen, Schlafprobleme,
        Verdauungsprobleme, Gereiztheit, die Anzeichen f&uuml;r chronischen Stress
        sind vielf&auml;ltig und beim einzelnen Betroffenen sehr unterschiedlich. </p>
              <p class="text"><span class="&uuml;berschrift"><img src="images-gesundheit-praevention/caritas_mannheim_iconpunkt.gif" width="12" height="12"> Ursachen
                  des chronischer Stress:</span><br>
                            <br>
        Die Vielfalt der Anforderungen an den modernen Menschen nimmt st&auml;ndig
        zu. Zu den Belastungen (Stressoren), die chronischen Stress verursachen
        k&ouml;nnen, z&auml;hlen unter anderem eine falsche Ern&auml;hrung, zu
        wenig Bewegung, Reiz&uuml;berflutung, Schlafmangel, ein hoher Anspruch
        im Berufsleben, in der Familie oder in der Schule.  </p>
              <p><span class="&uuml;berschrift"><img src="images-gesundheit-praevention/caritas_mannheim_iconpunkt.gif" width="12" height="12"> Folgen
                  des chronischer Stress, wenn nicht behandelt:<br>
                </span><span class="text"><br>
        Chronischer Stress schw&auml;cht die Abwehrlage im Organismus. Man wird
        anf&auml;lliger f&uuml;r Erkrankungen, Wunden verheilen langsamer. Der
        Blutdruck steigt dauerhaft an. Betroffene klagen zudem &uuml;ber eine
        verst&auml;rkte M&uuml;digkeit, Konzentrationsschw&auml;che, Depressionen,
        Magen- und Darmprobleme, Libidoverlust und Muskelschw&auml;che. Das Risiko
        f&uuml;r Herz-Kreislauferkrankungen nimmt nachweislich zu. Genetische
        Untersuchungen zeigen einen Einfluss des chronischen Stresses auf eine
        vorzeitige Zellalterung. <br>
                                        <br>
                </span><span class="&uuml;berschrift"><img src="images-gesundheit-praevention/caritas_mannheim_iconpunkt.gif" width="12" height="12"> Wer
                ist vom chronischer Stress betroffen: </span><span class="text"><br>
                <br>
                </span><span class="text">Generell k&ouml;nnen Personen mit mehrfachen
                Belastungen, ungesunder Lebensweise und fehlendem Ausgleich zum
                Stressabbau als besonders gef&auml;hrdet angesehen werden. Da
                nicht jeder, der dauerhaften k&ouml;rperlichen und oder psychischen
                Belastungen ausgesetzt ist, die Anzeichen von chronischem Stress
                aufweist, kann man von einer genetischen Veranlagung (Disposition)
                ausgehen. <br> 
                </span><span class="&uuml;berschrift">
                  </span><span class="text">
                    <br>
                  </span><span class="&uuml;berschrift"><img src="images-gesundheit-praevention/caritas_mannheim_iconpunkt.gif" width="12" height="12"> Pr&auml;ventionsm&ouml;glichkeiten: </span><span class="text"><br>
                    <br>
                    </span><span class="text">Damit der f&uuml;r Antrieb und
                    Entwicklung notwendige Stress nicht &uuml;berm&auml;chtig
                    wird, m&uuml;ssen den Phasen der Anspannung regelm&auml;&szlig;ig
                    Phasen der Entspannung folgen. Ruhe, Erholung, kurze Pausen
                    und Aktivit&auml;ten anderer Art, als sonst im Beruf, in
                    der Schule oder im Haushalt &uuml;blich, k&ouml;nnen dem
                    chronischen Stress vorbeugen. Besonders empfehlenswert ist
                    die Aus&uuml;bung von Sport. <br>
                    </span><span class="text">
                    <br>
                    </span><span class="&uuml;berschrift"><img src="images-gesundheit-praevention/caritas_mannheim_iconpunkt.gif" width="12" height="12"> was
                    im k&ouml;rper passiert: </span><span class="text"><br>
                    <br>
                    </span><span class="text">Dauerhafte Belastungen f&uuml;hren
                    zu einer ver&auml;nderten Produktion und Aussch&uuml;ttung
                    des Stresshormons Cortisol. Die Cortisol-Produktion in der
                    Nacht ist bei chronischem Stress deutlich gesteigert. Dadurch
                    liegt in der Zeit nach dem Aufstehen der Cortisol-Spiegel
                    h&ouml;her als im unbelasteten Zustand. Der Cortisol-Wert
                    bleibt meist &uuml;ber den Tag hinweg erh&ouml;ht. <br>
                    Das Verh&auml;ltnis von Noradrenalin und Adrenalin wird durch
                    chronischen Stress gest&ouml;rt. Zun&auml;chst steigt der
                    Noradrenalin-Spiegel deutlich an, bei gleichzeitigem Abfall
                    von Adrenalin. Bei weiter anhaltendem Stress sinkt Noradrenalin
                    zusammen mit Dopamin ab. Es entsteht ein Mangel an den genannten
                    Botenstoffen. <br>
                    Serotonin wird st&auml;rker verbraucht, die Produktionsmenge
                    sinkt ab, so dass es zu einem Serotoninmangel kommen kann.
                    Das ist mit Ursache f&uuml;r die M&uuml;digkeit und Antriebslosigkeit. <br>
                    Die fein aufeinander abgestimmten Konzentrationen an Hormonen
                    und anderen beteiligten Botenstoffen befinden sich nicht
                    mehr im Gleichgewicht. Durch die vielf&auml;ltigen Wirkungen
                    der einzelnen beteiligten Botenstoffe kommt es in der Folge
                    zu den beobachtbaren Gesundheitsst&ouml;rungen wie M&uuml;digkeit,
                    Antriebslosigkeit, Migr&auml;ne, Fibromyalgie, Schlafst&ouml;rungen
                    oder Angstzust&auml;nden, Depressionen, h&auml;ufig auch
                    Essst&ouml;rungen.. <br>
                    systeme regeln Adrenalin und Cortisol.                    <br>
                    <br>
</span><span class="&uuml;berschrift"><img src="images-gesundheit-praevention/caritas_mannheim_iconpunkt.gif" width="12" height="12"> diagnoseformen: </span><span class="text"></span></p>
              <p><span class="text"> Um chronischen Stress feststellen zu k&ouml;nnen,
                  bietet sich die Untersuchung von Speichelproben an. Im Speichel
                  l&auml;sst sich unter anderem der Tagesverlauf der Cortisol-Konzentration
                  nachweisen. <br>
                F&uuml;r entsprechende Tests k&ouml;nnen die sogenannten Neurotransmitter-Profile &quot;Neurotransmitter
                I basis&quot; mit Untersuchung von Serotonin, Adrenalin, Noradrenalin
                und Dopamin sowie &quot;Neurotransmitter II plus&quot; zum Einsatz
                kommen. <br>
                    <br>
                </span><span class="&uuml;berschrift"><img src="images-gesundheit-praevention/caritas_mannheim_iconpunkt.gif" width="12" height="12"> therapiem&ouml;glichkeiten: </span><span class="text"><br>
                  <br>
                  </span><span class="text">Auf der Grundlage der Messung
                  von Hormonen und anderen Botenstoffen kann eine nebenwirkungsfreie,
                  individuelle Behandlung mit Aminos&auml;ure-Vorstufen erfolgen.
                  Lassen sich die Stressbelastungen auf bestimmte Lebensbereiche
                  oder Faktoren zur&uuml;ckf&uuml;hren, wird eine Verhaltens&auml;nderung
                  oder eine Vermeidung der Stress-Faktoren Teil der Behandlung
                  sein.</span><span class="text"><br>
                  </span><span class="text"><br>
                    </span><span class="&uuml;berschrift"><img src="images-gesundheit-praevention/caritas_mannheim_iconpunkt.gif" width="12" height="12"> WEr
                    behandelt:</span><span class="text"> </span></p>
              <p><span class="text">&Auml;rzte im<a href="http://www.neurolab.eu/index.php?option=com_google_maps&Itemid=39" target="_blank"> <strong>Partnernetzwerk
                      von Neurolab</strong>.</a>
                    <br>
                    <br>
                    Links zum Thema:</a><a href="Klicken%20Sie%20hier%20f%FCr%20weitere%20Informationen.%20" target="_blank"> Klicken
              Sie hier f&uuml;r weitere Informationen.</a> </span></p>              
              <p align="right"><font size="2"><span class="text"><b><br>
                  </b></span><b><br>
                  </b><font size="4">&copy; <a href="http://www.neurolab.eu" target="_blank">www.neurolab.eu</a><br>
                  <br>
                  <font size="2" face="Arial, Helvetica, sans-serif">Bild: &copy; Gernot Krautberger - Fotolia.com </font></font></font></p></td>
            <td background="boxright.gif" width="8"><img src="boxright.gif" width="8" height="8" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td width="8"><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
            <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
            <td width="8"><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
          </tr>
        </table></td>
        <td width="120" rowspan="2" valign="top">
		
	Google<br>
	<script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
/* 120x600, Erstellt 11.06.08 */
google_ad_slot = "6662271223";
google_ad_width = 120;
google_ad_height = 600;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
</td>
      </tr>
      <tr>
        <td width="216" height="1374" align="left" valign="top">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>



</body>
</html>
