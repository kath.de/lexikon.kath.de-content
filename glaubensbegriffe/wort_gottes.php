<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head profile="http://dublincore.org/documents/dcq-html/">
  <title>Wort Gottes Theologie</title>

  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <link title="fonts" href="kaltefleiter.css" type="text/css" rel="stylesheet">


  <link rel="schema.DC" href="http://purl.org/dc/elements/1.1/">

  <link rel="schema.DCTERMS" href="http://purl.org/dc/terms/">


  <meta name="DC.creator" content="J&uuml;rgen Pelzer">

  <meta name="DC.subject" content="Theologie, Gott, Religion, Glaube, Kirche, Katholisch, Katholiken">

  <meta name="DC.description" content="Ein Lexikon zu wichtigen Fragen und Begriffen des Glaubens auf der H&ouml;he der neueren Theologie des 20. Jahrhunderts">

  <meta name="DC.publisher" content="J&uuml;rgen Pelzer">

  <meta name="DC.contributor" content="J&uuml;rgen Pelzer">

  <meta name="DC.date" content="2007-09-15T08:49:37+02:00" scheme="DCTERMS.W3CDTF">

  <meta name="DC.type" content="Text" scheme="DCTERMS.DCMIType">

  <meta name="DC.format" content="text/html" scheme="DCTERMS.IMT">

  <meta name="DC.language" content="de" scheme="DCTERMS.RFC3066">

  <meta name="DC.relation" content="http://dublincore.org/" scheme="DCTERMS.URI">

  <meta name="DC.coverage" content="Frankfurt" scheme="DCTERMS.TGN">

  <meta name="DC.rights" content="Alle Rechte liegen beim Autor">



  <link rel="stylesheet" type="text/css" href="css-lexikon-glaubensbegriffe.css" title="fonts">
</head>
<body leftmargin="6" topmargin="6" background="dstone.gif" bgcolor="#ffffff" marginheight="6" marginwidth="6">

<table align="center" border="0" cellpadding="6" cellspacing="0" width="1160">

  <tbody>

  <tr>

    <td align="left" valign="top" width="100"> 
      
      <table border="0" cellpadding="0" cellspacing="0" width="216">

        <tbody>
          <tr align="left" valign="top">
 
          <td width="8"><img src="boxtopleftcorner.gif" alt="" height="8" width="8"></td>

          <td background="boxtop.gif" width="200"><img src="boxtop.gif" alt="" height="8" width="8"></td>

          <td width="8"><img src="boxtoprightcorner.gif" alt="" height="8" width="8"></td>

        </tr>

        <tr align="left" valign="top">
 
          <td background="boxtopleft.gif" height="14"><img src="boxtopleft.gif" alt="" height="8" width="8"></td>

          <td bgcolor="#e2e2e2" height="14"><b>Glaubensbegriffe erkl&auml;rt</b></td>

          <td background="boxtopright.gif" height="14"><img src="boxtopright.gif" alt="" height="8" width="8"></td>

        </tr>

        <tr align="left" valign="top">
 
          <td><img src="boxdividerleft.gif" alt="" height="13" width="8"></td>

          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" height="13" width="8"></td>

          <td><img src="boxdividerright.gif" alt="" height="13" width="8"></td>

        </tr>

        <tr align="left" valign="top">
 
          <td background="boxleft.gif"><img src="boxleft.gif" alt="" height="8" width="8"></td>

          <td bgcolor="#ffffff"> <?php include("logo.html"); ?> </td>

          <td background="boxright.gif"><img src="boxright.gif" alt="" height="8" width="8"></td>

        </tr>

        <tr align="left" valign="top">
 
          <td><img src="boxbottomleft.gif" alt="" height="8" width="8"></td>

          <td background="boxbottom.gif"><img src="boxbottom.gif" alt="" height="8" width="8"></td>

          <td><img src="boxbottomright.gif" alt="" height="8" width="8"></td>

        </tr>

      
        </tbody>
      </table>

      <br>

      
      <table border="0" cellpadding="0" cellspacing="0" width="216">

        <tbody>
          <tr align="left" valign="top">
 
          <td width="8"><img src="boxtopleftcorner.gif" alt="" height="8" width="8"></td>

          <td background="boxtop.gif" width="200"><img src="boxtop.gif" alt="" height="8" width="8"></td>

          <td width="8"><img src="boxtoprightcorner.gif" alt="" height="8" width="8"></td>

        </tr>

        <tr align="left" valign="top">
 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" alt="" height="8" width="8"></td>

          <td bgcolor="#e2e2e2"><strong>Begriff anklicken</strong></td>

          <td background="boxtopright.gif"><img src="boxtopright.gif" alt="" height="8" width="8"></td>

        </tr>

        <tr align="left" valign="top">
 
          <td><img src="boxdividerleft.gif" alt="" height="13" width="8"></td>

          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" height="13" width="8"></td>

          <td><img src="boxdividerright.gif" alt="" height="13" width="8"></td>

        </tr>

        <tr align="left" valign="top">
 
          <td background="boxleft.gif"><img src="boxleft.gif" alt="" height="8" width="8"></td>

          <td class="V10" bgcolor="#ffffff"> <?php include("az.html"); ?> </td>

          <td background="boxright.gif"><img src="boxright.gif" alt="" height="8" width="8"></td>

        </tr>

        <tr align="left" valign="top">
 
          <td><img src="boxbottomleft.gif" alt="" height="8" width="8"></td>

          <td background="boxbottom.gif"><img src="boxbottom.gif" alt="" height="8" width="8"></td>

          <td><img src="boxbottomright.gif" alt="" height="8" width="8"></td>

        </tr>

      
        </tbody>
      </table>

    </td>

    <td rowspan="2" valign="top">
      
      <table border="0" cellpadding="0" cellspacing="0" width="100%">

        <tbody>

        <tr align="left" valign="top">

          <td width="8"><img alt="" src="boxtopleftcorner.gif" height="8" width="8"></td>

          <td background="boxtop.gif"><img alt="" src="boxtop.gif" height="8" width="8"></td>

          <td width="8"><img alt="" src="boxtoprightcorner.gif" height="8" width="8"></td>
          </tr>

        <tr align="left" valign="top">

          <td background="boxtopleft.gif"><img alt="" src="boxtopleft.gif" height="8" width="8"></td>

          <td bgcolor="#e2e2e2"> 
            
            <h1>Wort-Gottes-Theologie</h1>

          </td>

          <td background="boxtopright.gif"><img alt="" src="boxtopright.gif" height="8" width="8"></td>
          </tr>

        <tr align="left" valign="top">

          <td><img alt="" src="boxdividerleft.gif" height="13" width="8"></td>

          <td background="boxdivider.gif"><img alt="" src="boxdivider.gif" height="13" width="8"></td>

          <td><img alt="" src="boxdividerright.gif" height="13" width="8"></td>
          </tr>

        <tr align="left" valign="top">

          <td background="boxleft.gif" height="124"><img alt="" src="boxleft.gif" height="8" width="8"></td>

          <td class="L12" bgcolor="#ffffff" height="124"> 
            
            <p><strong><font face="Arial, Helvetica, sans-serif" size="3"><img src="seitenbilder/wort-gottes.jpg" alt="Stress h&auml;ngt von vielen Faktoren ab" name="Stressbild" style="border: 4px double rgb(0, 153, 153);" align="right" height="451" hspace="10" vspace="10" width="300">Lange
                  Zeit vergessen, erlebt sie im 20. Jhd. auf katholischer Seite
                  zu recht eine Renaissance: Die Wort-Gottes-Theologie.</font></strong></p>

            
            <p><font face="Arial, Helvetica, sans-serif" size="3">"Im Anfang
                war das Wort, / und das Wort war bei Gott, / und das Wort war
                Gott."
                So beginnt in der Bibel das Evangelium nach Johannes. Scheinbar
                spielt das Wort Gottes eine zentrale Rolle f&uuml;r den Glauben.<br>

                <br>

                Bereits in der fr&uuml;hen Kirche gab es eine ausgepr&auml;gte
                Wort-Gottes-Theologie, so etwa bei Origenes und Bonaventura.
                Doch erst das Zweite Vatikanum (1962-65)
                hat
                die Wort
               -Gottes-Theologie
                wieder in den Fokus des theologischen Interesses unserer Zeit
                ger&uuml;ckt.
                Als es im Mittelalter zur Abspaltung der Protestanten von der
                katholischen
                Kirche kam, wurden in der katholischen Theologie in bewusster
                Abgrenzung die Sakramente betont und in den
                Blick genommen.
                Der Protestantismus zeichnete sich ja gerade dadurch aus, dass
                das Wort Gottes die Mitte der Theologie ausmacht. In einer Gegenreaktion
                wurde dann auf dem Konzil von Trient von Seiten der katholischen
                Theologen die Sakramententheologie st&auml;rker betont. Diese Ungleichgewichtung
                in der Theologie der katholischen Kirche zog sich bis ins 20.
                Jahrhundert durch.</font></p>

            
            <p class="L14"><font face="Arial, Helvetica, sans-serif" size="3">:::Ein
                Schl&uuml;sselerlebnis: Gott ist ein Sprechender:::</font></p>
            
            
            <p><font face="Arial, Helvetica, sans-serif" size="3">Hans Urs von
                Balthasar machte auf diesen Missstand aufmerksam. Er studierte
                ausgebieg die Kirchenv&auml;ter sowie die Theologiegeschichte
                der Kirche, und stie&szlig; dabei
                auch auf die Wort-Gottes-Theologie des Origenes und des Bonaventura.
                Balthasars
                Erkenntnis
                dabei: Gott ist ein sprechender Gott. Bereits bei Origenes findet
                sich als Zentrum seiner Theologie der Logos. Logos ist nichts
                anderes als die griechische Umschreibung f&uuml;r das Wort Gottes.
                Und dieses Wort Gottes ist in Jesus Christus Mensch geworden.
                Verbum caro factum est, wie Balthasar es lateinisch ausdr&uuml;ckt.
                Bonaventura hat beim Durchdenken dieses Vorgangs der Fleischwerdung
                des Wortes ein Schl&uuml;sselerlebnis: Das g&ouml;ttliche Wort
                hat eine ganz eigene Dynamik. Er nennt es expressio, also &uuml;bersetzt
                so etwas wie Ausdruckswillen. Das g&ouml;ttliche Wort hat den
                inneren Drang sich mitzuteilen, sich auszudr&uuml;cken.</font></p>

            
            <p><font face="Arial, Helvetica, sans-serif" size="3">Dieser Ausdruckswille
                schreckt auch vor erstaunlichen Wegen nicht zur&uuml;ck: Das Wort
                Gottes ist in Jesus Christus Fleisch geworden, mehr noch, es
                wurde ein konkreter Mensch, mit allem, was zum Menschsein dazugeh&ouml;rt:
                &Auml;ngste, Hunger, Durst, Hoffnung, Freude - freilich ohne seine
                G&ouml;ttlichkeit zu verlieren.</font></p>

            
            <p class="L14"><font face="Arial, Helvetica, sans-serif" size="3">::: Ein Wort
                ist mehr als ein Wort :::</font></p>
            
            
            <p><font face="Arial, Helvetica, sans-serif" size="3">Fasst man diese
                Erkenntnisse der Theologiegeschichte zusammen, sieht man bereits:
                Das Wort Gottes ist hier nicht mehr nur blo&szlig;er Tr&auml;ger
                einer Information, sondern es ist ein Ereignis; es ist gelebte,
                in Raum und Zeit konkret gewordene Geschichte. Somit
                hat Jesus den Vater ausgelegt (Joh 1,18), nicht nur mit seinen
                Worten, sondern in seinem ganzen Leben. Leben und Wort zusammen.
                Das dr&uuml;ckt Balthasar aus, indem er vom Tat-Wort spricht.</font></p>

            
            <p><font face="Arial, Helvetica, sans-serif" size="3">Es dauerte
                Jahrhunderte, ehe dieses Verst&auml;ndnis vom Wort Gottes schlie&szlig;lich
                in der Theologie des 20. Jahrhunderts wiederentdeckt wurde. Drei
                Stufen der Mitteilung (theol. Offenbarung) des Wortes Gottes
                an die Menschen lassen sich so unterscheiden: Die erste Stufe
                ist die Sch&ouml;pfung. Gott spricht zu dem Menschen in seiner
                Sch&ouml;pfung.
                Deshalb ist es durchaus nicht verwunderlich, dass viele mit intensiven
                Naturerlebnissen eine Art religi&ouml;se Erfahrung verbinden
                - sei es bei einem Waldsparziergang oder einem Sonnenuntergang.
                Die zweite Stufe ist die Geschichte Israels. Das ist der Bestand,
                von dem das  Alte Testament spricht und aus dem sich die Liturgie
                der Katholischen Kirche speist. Die letzte Stufe ist Jesus Christus,
                das menschgewordenen Wort Gottes. </font></p>

            
            <p class="L14"><font face="Arial, Helvetica, sans-serif" size="3">:::
                Die Aussage des Wort Gottes :::</font></p>

            
            <p class="L12"><font face="Arial, Helvetica, sans-serif" size="3"> Besonders
                in zwei Situationen des Lebens Jesu wird deutlich, was das Wort
                Gottes den Menschen eigentlich mitteilen will: Dem Kreuzestod
                und dem H&ouml;llenabstieg. In beiden Situationen wird deutlich:
                Gott liebt die Menschen so sehr, dass er seinen eigenen Sohn,
                Jesus
                Christus, f&uuml;r sie hingibt. Sogar noch in die H&ouml;lle
                geht Jesus hinab, um auch dort den verlorenen Seelen zu zeigen:
                "Ich bin
                f&uuml;r euch da". Somit ist das Wort Gottes dieses: "Ich liebe
                euch und ich bin mit euch und f&uuml;r euch da". Nicht von ungef&auml;hr
                lautet die &Uuml;bersetzung des Namens Gottes, Jahwe im hebr&auml;ischen, "Ich
                bin" im Deutschen. </font></p>

            
            <p class="L12"><font face="Arial, Helvetica, sans-serif" size="3">Und
                was ist mit dem Gericht? Ist Gott nicht auch ein Richter, ein
                strenger Herr, der die Spreu vom Weizen trennt? Balthasar wusste
                nat&uuml;rlich auch um diesen Aspekt des biblischen Gottesbildes
                und musste irgendwie das Gericht mit in das bisher vorgestellte
                Konzept einer Wort Gottes Theologie integrieren. Doch es ist
                vor dem Hintergrund des bisher Gesagten nicht einsehbar, warum
                Gott einem einzigen Menschen etwas B&ouml;ses will. Deshalb
                spricht Balthasar von der richtenden Liebe Gottes, die in seinen
                Worten deutlich wird: Sie richtet die S&uuml;nde, den S&uuml;nder aber
                richtet sie auf. Am Kreuz wurde die S&uuml;nde in Ihrer ganzen
                Grausamkeit blo&szlig;gestellt -
                im H&ouml;llenabstieg hingegen Gottes Heilswillen f&uuml;r jeden
                Menschen. Sogar bis in den
                letzten Ort der Verdamnis, der H&ouml;lle, reicht die Pr&auml;senz
                von Gottes Wort. </font></p>

            
            <p class="L14"><font face="Arial, Helvetica, sans-serif" size="3">:::
                Kirche und Heilige Schrift :::</font></p>

            
            <p class="L12"><font face="Arial, Helvetica, sans-serif" size="3">Die
                Heilige Schrift ist das Zeugnis der Offenbarung des Wortes Gottes,
                in der Form des Lebens Jesu Christi oder eben vorher schon in der Geschichte
                mit Israel. Sie ist wohlgemerkt nicht die Offenbarung, sondern
                das Zeugnis. Dies ist ein fundamentaler Unterschied, der bei
                Nichtbeachtung schnell zum Creationismus oder anderen abstrusen
                Irrwegen unserer Zeit f&uuml;hren kann. Die Kirche
                als Glaubensgemeinschaft wiederum ist der genuine Ort der Auslegung
                dieses Wortes Gottes. </font></p>

            
            <p class="L14"><font face="Arial, Helvetica, sans-serif" size="3">:::
                Das Echo des Wortes :::</font></p>

            
            <p class="L12"><font face="Arial, Helvetica, sans-serif" size="3">Das
                Wort Gottes zieht ein Echo durch die Geschichte. Das st&auml;rkste
                Echo ist das "Fiat" Marias. Maria, die Mutter Jesu, sagte "Ja" zu
                dem Plan, den Gott mit ihr ersonnen hatte. Maria wurde
                zur Mutter Jesu und damit zur Gottesmutter. Fiat ist
                dabei
                das&nbsp;lateinische
                Wort der Antwort, die Maria gab und die soviel bedeutet wie "Mir geschehe
                nach deinem Willen", oder kurz: "Ja". Und diese M&ouml;glichkeit
                ist jedem Menschen gegeben: Ja zu sagen, zu Gottes Plan; dazu,
                dass
                er sein Wort an die Menschen spricht, was immer eine frohe Botschaft,
                also ein Evangelium (frohe Botschaft) ist, keine Drohbotschaft.
                Dadurch kann der Mensch zum Mitt&auml;ter des Wortes Gottes werden.</font></p>

            
            <p class="L14"><font face="Arial, Helvetica, sans-serif" size="3">:::
                Zukunft der Wort-Gottes-Theologie :::</font></p>
            
            
            <p class="L12"><font face="Arial, Helvetica, sans-serif" size="3">Balthasar
spricht davon, dass die Geschichte der Menschheit mit Gott einem
Theodrama gleicht. Das Wort Gottes ordnet dem Menschen seinen Platz
innerhalb dieses Theodramas zu. In der Besch&auml;ftigung mit dem Thema
der Wort-Gottes-Theologie wird deutlich: Es handelt sich hierbei nicht
um einen Teilaspekt der Theologie, es ist kein Einzeltraktat, wie die
Theologen es nennen, sondern es ist eine Grundsatzfrage, die sich durch
alle Bereiche der Theologie zieht. Dass die Wort-Gottes-Theologie
wieder zu ihrer einstigen Bedeutung zur&uuml;ckgefunden hat, ist vor
allem dem Werk Hans Urs von Balthsars zu verdanken. Eine gro&szlig;e
Zukunft ist der Thematik aber auch noch im Hinblick auf das
&ouml;kumenischen Gespr&auml;ch mit den Schwestern und Br&uuml;dern der
Katholiken, den Protestanten, beschieden. Hier stellt die neuere
Betonung der Wichtigkeit des Wortes Gottes seitens der katholischen
Theologen einen wichtigen Impuls f&uuml;r das &ouml;kumenische
Gespr&auml;ch dar, das eine der vornehmlichsten Aufgaben beider
gro&szlig;en christlichen Kirchen in Deutschland ist.</font></p>

            
            <p><font face="Arial, Helvetica, sans-serif" size="3"><br>

              &copy; <a href="http://www.juergenpelzer.de">J&uuml;rgen Pelzer</a> 2007 Bild: &copy; Dimitar Atanasov
              - Fotolia.com</font></p>

            </td>

          <td background="boxright.gif" height="124"><img alt="" src="boxright.gif" height="8" width="8"></td>

        </tr>

        <tr align="left" valign="top">

          <td><img alt="" src="boxbottomleft.gif" height="8" width="8"></td>

          <td background="boxbottom.gif"><img alt="" src="boxbottom.gif" height="8" width="8"></td>

          <td><img alt="" src="boxbottomright.gif" height="8" width="8"></td>
          </tr>
        </tbody>
      </table>

      </td>

    <td rowspan="2" valign="top">
      <script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
google_ad_width = 160;
google_ad_height = 600;
google_ad_format = "160x600_as";
google_ad_type = "text_image";
//2007-09-06: Wort Gottes Lexikon
google_ad_channel = "2271631061";
google_ui_features = "rc:10";
//-->
      </script>
      
      <script type="text/javascript" src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
      </script>
      </td>

  </tr>

  <tr>

    <td align="left" height="2" valign="top">&nbsp; </td>

    </tr>
  </tbody>
</table>

</body>
</html>
