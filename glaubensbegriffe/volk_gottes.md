---
title: Volk-Gottes-Theologie
author: Jürgen Pelzer
tags: Theologie, Gott, Religion, Glaube, Kirche, Katholisch, Katholiken
created_at: 2007-09-15T08:49:37+02:00
images: ["seitenbilder/volk-gottes-gross.jpg"]
---


# *****Gott erwählt sich ein Heiliges Volk - Israel. Doch warum erwählt er sich ein singuläres kleines Volk? Welche Rolle spielen die vielen anderen Völker der Welt - und warum kam mit Jesus - aus katholischer Sicht - die Wende?*
# **Christologisch gesehen: Jesus sammelt das Gottesvolk*
# **Jesus war gekommen, um die Gottesherrschaft auszurufen und auf das nahende Gottesreich hinzuweisen. Ganz ursprünglich bedeutet Gottesherrschaft die Wiederherstellung Israels als Volk in der ursprünglichen Gestalt, also als ein einiges Volk, das nicht in 12 Stämme geteilt ist und als ein freies Volk, das nicht unter der Fremdherrschaft der Römer steht.**
 ***Jesus definiert die Erneuerung Israels allerdings noch weiter: Sie beinhaltet, dass die vielen Völker zusammenkommen. Dies zeigt sich schon in der Berufung der 12 Jünger, die ein Symbol für das 12-Stämmevolk Israel sind. In der Verkündigung Jesu kommt neu der Gedanke hinzu, dass die anderen Völker an der Gottesherrschaft teilhaben. 

# **Spielt Gott Lotto? Oder: Warum "nur" ein Volk?*
***Israel ist in biblischer Sichtweise das auserwählte Volk Gottes, sein heiliges Volk. Schon früh war aber klar, dass es auch andere Völker gibt. Daher drängte sich die Frage auf, wie es zu verstehen ist, das gerade Israel das auserwählte Volk ist, und wie damit umzugehen ist. Eine Lösung besteht in der Stellvertreter-These. Israel steht stellvertretend für alle Völker. Dieses Motiv findet sich unter anderem im sog. Gottesknechtlied im Buch des Propheten Jesaja. Dort erleidet der Gottesknecht stellvertretend für das Volk das Schicksal. 

***Ein anderes Motiv ist das der Völkerwallfahrt zum Zion. In dieser Vorstellung treten die anderen Völker zu Israel am Ende der Zeiten hinzu. Alle Völker gemeinsam pilgern dann zum Heiligen Berg Gottes, dem Zion. Die Frage ist allerdings, wann diese Völkerwallfahrt beginnt? Sie beginnt mit dem Tod Jesu, der von außen betrachtet eine Hinrichtung ist, innerlich aber geschieht eine Wandlung:             

# **Der Beginn der Auseinandersetzung: Die Tempelreinigung*
***Bei der Tempelreinigung wurde das Anliegen Jesu besonders deutlich: Es ging nicht so sehr darum, dass im Tempel Handel getrieben wurde. Vielmehr hängt Jesus seine Aktion ( in der er alle Händler aus dem Vorhof hinaustrieb) mit der Sammlung des Gottesvolkes zusammen.  Der Vorhof ist im Tempel der Ort, der für die Heiden reserviert ist. Dies ist bereits ein erstes Zeichen für die Ausrufung der Völkerwallfahrt, für die Sammlung der Völker. Hiermit beginnt auch der eigentliche Konflikt mit den Inhabern der Macht: Jesus erfährt Widerstand, der letztlich in der Kreuzigung gipfelt. Jesus seinerseits trägt dieses Schicksal - damit ist seine Handlungsweise paralell zu der des Gottesknechtes aus dem Buch des Propheten Jesaja. Er trägt sein Schicksal stellvertretend für die vielen (In der Messe wird bei der Wandlung deswegen auch gesprochen: "Das ist mein Blut - das für viele vergossen wird zur Vergebung der Sünden"). Im Tragen dessen, was Jesus widerfährt, geschieht von innen eine Wandlung. Versöhnung wird möglich. Die Spirale von Gewalt und Gegengewalt wird unterbrochen - Versöhnung wird möglich. 

# **Versöhnung als Grundpfeiler des Gottesvolkes*
***Im Tragen dessen, was dort geschieht - Versöhnung- konstituiert sich das Gottesvolk als wahrhaft messianisches, d.h. zugunster aller. Damit ist das Schicksal des Gottesknechtes überhöht und die Völkerwallfahrt eingeleitet - die anderen Völker können hinzutreten, die Völkerwallfahrt zum Zion hat begonnen. Deshalb ist es im katholischenZeitverständnis momentan die Zeit der Apokalypse - wir leben also in der Endzeit. Deshalb wurde  nach Ostern in der frühen Kirche nicht die Apostelgeschichte gelesen, sondern die Apokalypse. Das Hinzutreten der Völker findet seinen ersten geschichtlich greifbaren Ausdruck in den Heidenmissionen des Apostel Paulus.  

# **Das II Vatikanum und der Begriff*
***Der Begriff des Volkes Gottes wurde vor allem auf dem II Vatikanischen Konzil wiederentdeckt. Im Zuge des Konzils musste der Begriff vielfach für kirchenpolitische Argumentationen herhalten, die etwas an die Parolen "Wir sind das Volk" aus den Zeiten des Mauerfalls erinnern. Dabei meint der eigentliche Begriff eine zutiefst Christologische Wahrheit und ist weniger eine Aussage über Demokratisierungsbestrebungen innerhalb der Kirche. 

***Katholischerseits gibt es - und das ist die Sinnspitze des Begriffes Volk Gottes - keine Einzelchristen. Gott beruft sich ein Volk. Glaube ist nie ein Sache der individuellen Auslegung sondern geschieht immer innerhalb eines größeren Bezugrahmens - des Volkes Gottes. 

# ****
 © ***[Jürgen Pelzer](http://www.juergenpelzer.de) 2007 Bild: © drx - Fotolia.com
