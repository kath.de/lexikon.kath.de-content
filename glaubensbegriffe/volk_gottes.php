<HTML><HEAD><TITLE>Volk-Gottes-Theologie</TITLE>
<META http-equiv=Content-Type content="text/html; charset=iso-8859-1"><LINK 
title=fonts href="kaltefleiter.css" type=text/css 
rel=stylesheet>
<head profile="http://dublincore.org/documents/dcq-html/">
<link rel="schema.DC" href="http://purl.org/dc/elements/1.1/">
<link rel="schema.DCTERMS" href="http://purl.org/dc/terms/">

<meta name="DC.creator" content="J�rgen Pelzer">
<meta name="DC.subject" content="Theologie, Gott, Religion, Glaube, Kirche, Katholisch, Katholiken">
<meta name="DC.description" content="Ein Lexikon zu wichtigen Fragen und Begriffen des Glaubens auf der H�he der neueren Theologie des 20. Jahrhunderts">
<meta name="DC.publisher" content="J�rgen Pelzer">
<meta name="DC.contributor" content="J�rgen Pelzer">
<meta name="DC.date" content="2007-09-15T08:49:37+02:00" scheme="DCTERMS.W3CDTF">
<meta name="DC.type" content="Text" scheme="DCTERMS.DCMIType">
<meta name="DC.format" content="text/html" scheme="DCTERMS.IMT">
<meta name="DC.language" content="de" scheme="DCTERMS.RFC3066">
<meta name="DC.relation" content="http://dublincore.org/" scheme="DCTERMS.URI">
<meta name="DC.coverage" content="Frankfurt" scheme="DCTERMS.TGN">
<meta name="DC.rights" content="Alle Rechte liegen beim Autor">


<link rel="stylesheet" type="text/css" href="css-lexikon-glaubensbegriffe.css" title="fonts">
</HEAD>
<BODY bgColor=#ffffff background="dstone.gif" leftMargin=6 topMargin=6 marginwidth="6" marginheight="6">
<TABLE width="1160" border=0 align="center" cellPadding=6 cellSpacing=0>
  <TBODY>
  <TR>
    <TD vAlign=top align=left width=100> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif" height="14"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2" height="14"><b>Glaubensbegriffe erkl&auml;rt</b></td>
          <td background="boxtopright.gif" height="14"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#FFFFFF"> <?php include("logo.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#FFFFFF" class="V10"> <?php include("az.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </TD>
    <TD vAlign=top rowSpan=2>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
        <TBODY>
        <TR vAlign=top align=left>
          <TD width=8><IMG height=8 alt="" 
            src="boxtopleftcorner.gif" width=8></TD>
          <TD background=boxtop.gif><IMG height=8 alt="" 
            src="boxtop.gif" width=8></TD>
          <TD width=8><IMG height=8 alt="" 
            src="boxtoprightcorner.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxtopleft.gif><IMG height=8 alt="" 
            src="boxtopleft.gif" width=8></TD>
          <TD bgColor=#e2e2e2> 
            <H1>Volk-Gottes-Theologie</H1>
          </TD>
          <TD background=boxtopright.gif><IMG height=8 
            alt="" src="boxtopright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=13 alt="" src="boxdividerleft.gif" 
            width=8></TD>
          <TD background=boxdivider.gif><IMG height=13 
            alt="" src="boxdivider.gif" width=8></TD>
          <TD><IMG height=13 alt="" 
            src="boxdividerright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxleft.gif height="124"><IMG height=8 alt="" 
            src="boxleft.gif" width=8></TD>
          <TD height="124" bgcolor="#FFFFFF" class=L12> 
            <P><STRONG><font face="Arial, Helvetica, sans-serif" size="3"><img SRC="seitenbilder/volk-gottes-gross.jpg" alt="Bild-Volk-Gottes-Theologie" name="Stressbild" width="300" hspace="10" vspace="10" align="right" style="border: double 4px #009999">Gott
                  erw&auml;hlt sich ein Heiliges Volk - Israel. Doch warum erw&auml;hlt
                  er sich ein singul&auml;res kleines Volk? Welche Rolle spielen
                  die vielen anderen V&ouml;lker der Welt - und warum kam mit Jesus
                  - aus katholischer Sicht - die Wende?</font></STRONG></P>
            <P><font size="3" face="Arial, Helvetica, sans-serif"><strong>Christologisch
                gesehen: Jesus sammelt das Gottesvolk</strong></font></P>
            <P><font size="3" face="Arial, Helvetica, sans-serif">Jesus war gekommen,
                um die Gottesherrschaft auszurufen und auf das nahende Gottesreich
                hinzuweisen. Ganz urspr&uuml;nglich bedeutet
                Gottesherrschaft die Wiederherstellung Israels als Volk in der
                urspr&uuml;nglichen Gestalt, also als ein einiges Volk, das nicht
                in 12 St&auml;mme geteilt ist und als ein freies Volk, das nicht unter
                der Fremdherrschaft der R&ouml;mer steht.<br>
            </font><font size="3" face="Arial, Helvetica, sans-serif">Jesus definiert
            die Erneuerung Israels allerdings noch weiter: Sie beinhaltet, dass
            die vielen V&ouml;lker
                  zusammenkommen. Dies zeigt sich schon in der Berufung der 12
                  J&uuml;nger, die
                  ein Symbol f&uuml;r das 12-St&auml;mmevolk Israel sind. In
                  der Verk&uuml;ndigung Jesu kommt neu der Gedanke hinzu, dass
                  die anderen
                  V&ouml;lker an
                  der
                Gottesherrschaft teilhaben.</font></P>
            <P><font size="3" face="Arial, Helvetica, sans-serif"><strong>Spielt
                Gott Lotto? Oder: Warum &quot;nur&quot; ein Volk?</strong></font></P>            
            <P><font size="3" face="Arial, Helvetica, sans-serif">Israel
                    ist in biblischer Sichtweise das auserw&auml;hlte Volk Gottes,
                    sein heiliges
                Volk. Schon fr&uuml;h war aber klar, dass es auch andere V&ouml;lker gibt.
                Daher dr&auml;ngte sich die Frage auf, wie es zu verstehen ist, das
                gerade Israel das auserw&auml;hlte Volk ist, und wie damit umzugehen
                ist. Eine L&ouml;sung besteht in der Stellvertreter-These. Israel
                steht stellvertretend f&uuml;r alle V&ouml;lker. Dieses Motiv findet
                sich unter anderem im sog. Gottesknechtlied im Buch des Propheten
                Jesaja. Dort erleidet der Gottesknecht stellvertretend f&uuml;r das
                Volk das Schicksal.</font></P>
            <P><font size="3" face="Arial, Helvetica, sans-serif">Ein anderes
                Motiv ist das der V&ouml;lkerwallfahrt zum Zion. In dieser Vorstellung
                treten die anderen V&ouml;lker zu Israel am Ende der Zeiten hinzu.
                Alle V&ouml;lker gemeinsam pilgern dann zum Heiligen Berg Gottes,
                dem Zion. Die Frage ist allerdings, wann diese V&ouml;lkerwallfahrt
                beginnt? Sie beginnt mit dem Tod Jesu, der von au&szlig;en betrachtet
                eine Hinrichtung ist, innerlich aber geschieht eine Wandlung:</font></P>            
            <P><font size="3" face="Arial, Helvetica, sans-serif"><strong>Der Beginn
                der Auseinandersetzung:
                Die Tempelreinigung</strong></font></P>
            <P><font size="3" face="Arial, Helvetica, sans-serif">Bei der Tempelreinigung
                wurde das Anliegen Jesu besonders deutlich: Es ging nicht so
                sehr darum, dass im Tempel Handel getrieben wurde. Vielmehr h&auml;ngt
                Jesus seine Aktion ( in der er alle H&auml;ndler
                aus dem Vorhof hinaustrieb) mit der Sammlung des Gottesvolkes
                zusammen.  Der Vorhof ist im Tempel der Ort, der f&uuml;r
                die Heiden reserviert ist. Dies ist bereits ein erstes Zeichen
                f&uuml;r die Ausrufung der V&ouml;lkerwallfahrt, f&uuml;r die Sammlung der V&ouml;lker.
                Hiermit beginnt auch der eigentliche Konflikt mit den
                Inhabern der Macht: Jesus erf&auml;hrt Widerstand,
                der letztlich in der Kreuzigung gipfelt. Jesus seinerseits tr&auml;gt
                dieses Schicksal
                - damit ist seine Handlungsweise paralell zu der des Gottesknechtes
                aus dem Buch des Propheten Jesaja. Er tr&auml;gt sein Schicksal stellvertretend
                f&uuml;r die vielen (In der Messe wird bei der Wandlung deswegen auch
                gesprochen: &quot;Das ist mein Blut - das f&uuml;r viele vergossen wird
                zur Vergebung der S&uuml;nden&quot;). Im Tragen dessen, was Jesus widerf&auml;hrt,
                geschieht von innen eine
                Wandlung.
                Vers&ouml;hnung
                wird m&ouml;glich. Die Spirale von Gewalt und Gegengewalt wird unterbrochen
                - Vers&ouml;hnung wird m&ouml;glich.</font></P>
            <P><font size="3" face="Arial, Helvetica, sans-serif"><strong>Vers&ouml;hnung
                als Grundpfeiler des Gottesvolkes</strong></font></P>
            <P><font size="3" face="Arial, Helvetica, sans-serif">Im Tragen
                    dessen, was dort geschieht - Vers&ouml;hnung- konstituiert sich
                  das Gottesvolk als wahrhaft messianisches, d.h. zugunster aller.
                Damit ist das Schicksal des Gottesknechtes &uuml;berh&ouml;ht und die V&ouml;lkerwallfahrt
                eingeleitet - die anderen V&ouml;lker k&ouml;nnen hinzutreten, die V&ouml;lkerwallfahrt
                zum Zion hat begonnen. Deshalb ist es im katholischenZeitverst&auml;ndnis
                momentan die Zeit der Apokalypse
                - wir leben also in der Endzeit.
                Deshalb wurde  nach Ostern in der fr&uuml;hen
                Kirche nicht die Apostelgeschichte gelesen, sondern die Apokalypse.
                Das Hinzutreten der V&ouml;lker findet seinen ersten geschichtlich
                greifbaren Ausdruck in den Heidenmissionen des Apostel Paulus. </font></P>
            <P><font size="3" face="Arial, Helvetica, sans-serif"><strong>Das II Vatikanum
                und der Begriff</strong></font></P>
            <P><font size="3" face="Arial, Helvetica, sans-serif">Der Begriff
                des Volkes Gottes wurde vor allem auf dem II Vatikanischen Konzil
                wiederentdeckt. Im Zuge des Konzils musste der Begriff vielfach
                f&uuml;r kirchenpolitische Argumentationen herhalten, die etwas
                an die Parolen &quot;Wir sind das Volk&quot; aus den Zeiten des Mauerfalls
                erinnern. Dabei meint der eigentliche Begriff eine zutiefst Christologische
                Wahrheit und ist weniger eine Aussage &uuml;ber Demokratisierungsbestrebungen
                innerhalb der Kirche.</font></P>
            <P><font size="3" face="Arial, Helvetica, sans-serif">Katholischerseits
                gibt es - und das ist die Sinnspitze des Begriffes Volk Gottes
                - keine Einzelchristen. Gott beruft sich ein Volk. Glaube ist
                nie ein Sache der individuellen Auslegung sondern geschieht immer
                innerhalb eines gr&ouml;&szlig;eren Bezugrahmens - des Volkes Gottes.</font></P>
            <P><font face="Arial, Helvetica, sans-serif" size="3"><br>
              &copy; <a href="http://www.juergenpelzer.de">J&uuml;rgen Pelzer</a> 2007
              Bild: &copy; drx - Fotolia.com</font></P></TD>
          <TD background=boxright.gif height="124"><IMG height=8 alt="" 
            src="boxright.gif" width=8></TD>
        </TR>
        <TR vAlign=top align=left>
          <TD><IMG height=8 alt="" src="boxbottomleft.gif" 
            width=8></TD>
          <TD background=boxbottom.gif><IMG height=8 alt="" 
            src="boxbottom.gif" width=8></TD>
          <TD><IMG height=8 alt="" src="boxbottomright.gif" 
            width=8></TD></TR></TBODY></TABLE>
      </TD>
    <TD vAlign=top rowSpan=2><script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
google_ad_width = 160;
google_ad_height = 600;
google_ad_format = "160x600_as";
google_ad_type = "text_image";
//2007-09-06: Wort Gottes Lexikon
google_ad_channel = "2271631061";
google_ui_features = "rc:10";
//-->
      </script>
      <script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
      </script>
</TD>
  </TR>
  <TR>
    <TD vAlign=top align=left height="2">&nbsp; </TD>
</TR></TBODY></TABLE>
</BODY></HTML>
