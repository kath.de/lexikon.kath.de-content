<HTML><HEAD><TITLE>Bistum Konstanz</TITLE>
<meta name="title" content="Kirche in Deutschland">
<meta name="author" content="Redaktion kath.de">
<meta name="publisher" content="kath.de">
<meta name="copyright" content="kath.de">
<meta name="description" content="Lexikon �ber die Kirche in Deutschland">
<meta name="abstract" content="Lexikon �ber die Kirche in Deutschland">
<meta http-equiv="content-language" content="de">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="date" content="2006-00-01">
<meta name="robots" content="index,follow">
<meta name="revisit-after" content="10 days">
<meta name="revisit" content="after 10 days">
<meta name="DC.Title" content="Kirche in Deutschland">
<meta name="DC.Creator" content="Redaktion kath.de">
<meta name="DC.Contributor" content="u.a.">
<meta name="DC.Rights" content="kath.de">
<meta name="DC.Publisher" content="kath.de">
<meta name="DC.Date" content="2006-00-01">
<meta name="DC.Description" content="Lexikon �ber die Kirche in Deutschland">
<meta name="DC.Language" content="de">
<meta name="DC.Type" content="Text">
<meta name="DC.Format" content="text/html">
<meta name="DC.Identifier" content="http://www.kath.de/lexikon/kirche_in_deutschland/">
<meta name="keywords" lang="de" content="Geschichte, Gotthardpa�, Freiburg, Rottenburg-Stuttgart, Chur, St. Gallen, Theodor von Dalberg">
</HEAD>
<BODY bgColor=#ffffff leftMargin=6 topMargin=6 marginheight="6" marginwidth="6">
<TABLE cellSpacing=0 cellPadding=6 width="100%" border=0>
  <TBODY>
  <TR>
    <TD vAlign=top align=left width=100> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="../philosophie_theologie/boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="../philosophie_theologie/boxtop.gif"><img src="../philosophie_theologie/boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="../philosophie_theologie/boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="../philosophie_theologie/boxtopleft.gif"><img src="../philosophie_theologie/boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b>Kirche in Deutschland</b></td>
          <td background="../philosophie_theologie/boxtopright.gif"><img src="../philosophie_theologie/boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="../philosophie_theologie/boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="../philosophie_theologie/boxdivider.gif"><img src="../philosophie_theologie/boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="../philosophie_theologie/boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="../philosophie_theologie/boxleft.gif"><img src="../philosophie_theologie/boxleft.gif" width="8" height="8" alt=""></td>
          <td> <?php include("logo.html"); ?> </td>
          <td background="../philosophie_theologie/boxright.gif"><img src="../philosophie_theologie/boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="../philosophie_theologie/boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="../philosophie_theologie/boxbottom.gif"><img src="../philosophie_theologie/boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="../philosophie_theologie/boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="../philosophie_theologie/boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="../philosophie_theologie/boxtop.gif"><img src="../philosophie_theologie/boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="../philosophie_theologie/boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="../philosophie_theologie/boxtopleft.gif"><img src="../philosophie_theologie/boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="../philosophie_theologie/boxtopright.gif"><img src="../philosophie_theologie/boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="../philosophie_theologie/boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="../philosophie_theologie/boxdivider.gif"><img src="../philosophie_theologie/boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="../philosophie_theologie/boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="../philosophie_theologie/boxleft.gif"><img src="../philosophie_theologie/boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10"> <?php include("az.html"); ?> </td>
          <td background="../philosophie_theologie/boxright.gif"><img src="../philosophie_theologie/boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="../philosophie_theologie/boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="../philosophie_theologie/boxbottom.gif"><img src="../philosophie_theologie/boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="../philosophie_theologie/boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </TD>
    <TD vAlign=top rowSpan=2>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
        <TBODY>
        <TR vAlign=top align=left>
          <TD width=8><IMG height=8 alt="" 
            src="../philosophie_theologie/boxtopleftcorner.gif" width=8></TD>
          <TD background=../philosophie_theologie/boxtop.gif><IMG height=8 alt="" 
            src="../philosophie_theologie/boxtop.gif" width=8></TD>
          <TD width=8><IMG height=8 alt="" 
            src="../philosophie_theologie/boxtoprightcorner.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=../philosophie_theologie/boxtopleft.gif><IMG height=8 alt="" 
            src="../philosophie_theologie/boxtopleft.gif" width=8></TD>
          <TD bgColor=#e2e2e2> 
            <H1><font face="Arial, Helvetica, sans-serif">Bistum Konstanz</font></H1>
          </TD>
          <TD background=../philosophie_theologie/boxtopright.gif><IMG height=8 
            alt="" src="../philosophie_theologie/boxtopright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=13 alt="" src="../philosophie_theologie/boxdividerleft.gif" 
            width=8></TD>
          <TD background=../philosophie_theologie/boxdivider.gif><IMG height=13 
            alt="" src="../philosophie_theologie/boxdivider.gif" width=8></TD>
          <TD><IMG height=13 alt="" 
            src="../philosophie_theologie/boxdividerright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=../philosophie_theologie/boxleft.gif><IMG height=8 alt="" 
            src="../philosophie_theologie/boxleft.gif" width=8></TD>
          <TD class=L12>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td valign="top" class="L12"><P><STRONG><font face="Arial, Helvetica, sans-serif">Gro&szlig;es
                        Bistum im S&uuml;dwesten</font></STRONG></P>
                  <P><br>
                      <font face="Arial, Helvetica, sans-serif">Die Universit&auml;tsstadt
                      und &#8222;Bodenseemetropole&#8220; Konstanz war vom 6.
                      Jahrhundert bis 1821/27 Sitz eines der fl&auml;chenm&auml;&szlig;ig
                      gr&ouml;&szlig;ten Bist&uuml;mer im deutschen Sprachraum.
                      Das Bistumsgebiet erstreckte sich im ausgehenden Mittelalter
                      vom Gotthardpa&szlig; im S&uuml;den bis in den Raum Stuttgart/Ludwigsburg
                      im Norden, vom Rhein im Westen bis zur Iller im Osten und
                      umfa&szlig;te erhebliche Teile der heutigen (Erz)Bist&uuml;mer
                      Augsburg, <a href="http://www.erzbistum-freiburg.de/">Freiburg</a>, <a href="http://www.drs.de">Rottenburg-Stuttgart</a> (Deutschland),
                      Feldkirch (&Ouml;sterreich),<a href="http://www.bistum-basel.ch/"> Basel-Solothurn</a>, <a href="http://www.bistum-chur.ch">Chur</a> und <a href="http://www.bistum-stgallen.ch/%20">St.
                      Gallen</a> (Schweiz) . Entstanden war es urspr&uuml;nglich
                      als alemannisches Stammesbistum. Seine gr&ouml;&szlig;te
                      politische und kirchliche Bedeutung erreichte es zur Zeit
                      des Konstanzer Konzils (1414-18), als sich in Konstanz
                      f&uuml;r knapp vier Jahre faktisch das Zentrum der r&ouml;misch-katholischen
                      Kirche befand. Insgesamt aber erlangte es nie die kirchenpolitische
                      Bedeutung, die ihm angesichts seiner Ausdehnung angestanden
                      h&auml;tte. Grund daf&uuml;r waren seine Unterordnung als
                      Suffraganbistum unter den Erzbischof von Mainz, die Zugeh&ouml;rigkeit
                      zu einer Unzahl von kleinen und kleinsten Staatsgebilden,
                      das geringe politische Gewicht seines nur kleinen Hochstiftes
                      und die damit verbundene, fast immer prek&auml;re wirtschaftliche
                      Situation. <br>
  Als die Stadt Konstanz nach der Reformation protestantisch wurde, verlegte
  der Bischof seine Residenz dauerhaft nach Meersburg. Die grundlegende territoriale
  Neuordnung Mitteleuropas im Gefolge der Franz&ouml;sischen Revolution, der
  S&auml;kularisation und Mediatisierung sowie der Napoleonischen Kriege, aber
  auch die r&ouml;mische Gegenreaktion gegen nationalkirchliche Bestrebungen
  und die in Konstanz deutlich wirksame katholische Aufkl&auml;rung f&uuml;hrte
  zur Aufhebung des Bistums durch die p&auml;pstliche Bulle &#8222;Provida solersque&#8220; vom
  16. August 1821. Zuvor waren seit 1815 schon die schweizerischen, w&uuml;rttembergischen,
  bayerischen und &ouml;sterreichischen Teile abgetrennt worden, und nach dem
  Tod des letzten Bischofs Karl Theodor von Dalberg am 10. Februar 1817 war und
  blieb der Bischofsstuhl vakant. Bis zum faktischen Ende des Bistums durch die
  Inthronisation des ersten Freiburger Erzbischofs Bernhard Boll am 21. Oktober
  1827 verwaltete Ignaz Heinrich von Wessenberg als &#8211; vom Heiligen Stuhl
  nicht anerkannter &#8211; Bistumsverweser das nunmehr auf die badischen Anteile
  geschrumpfte, lediglich durch die rechtsrheinischen Teile des Bistums Stra&szlig;burg
  etwas vermehrte Restbistum. </font></P>
                  <p><font face="Arial, Helvetica, sans-serif">Dr. Christoph
                      Schmider, Leiter des Erzbisch&ouml;flichen Archivs Freiburg <br>
                    </font><br>
                  </p>
                  <P>&copy;<a href="http://www.ordinariat-freiburg.de/14.0.html"> <font face="Arial, Helvetica, sans-serif">Erzbistum
                    Freiburg</font></a></P></td>
                <td valign="top"><script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
google_ad_width = 160;
google_ad_height = 600;
google_ad_format = "160x600_as";
google_ad_type = "text_image";
//2007-09-05: Bildung und Erziehung
google_ad_channel = "3955521420";
google_ui_features = "rc:6";
//-->
            </script>
                    <script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
              </script>
                </td>
              </tr>
            </table>
            <P>&nbsp;</P>
            </TD>
          <TD background=../philosophie_theologie/boxright.gif><IMG height=8 alt="" 
            src="../philosophie_theologie/boxright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=8 alt="" src="../philosophie_theologie/boxbottomleft.gif" 
            width=8></TD>
          <TD background=../philosophie_theologie/boxbottom.gif><IMG height=8 alt="" 
            src="../philosophie_theologie/boxbottom.gif" width=8></TD>
          <TD><IMG height=8 alt="" src="../philosophie_theologie/boxbottomright.gif" 
            width=8></TD></TR></TBODY></TABLE></TD></TR>
  <TR>
    <TD vAlign=top align=left>&nbsp; </TD>
  </TR></TBODY></TABLE></BODY></HTML>
