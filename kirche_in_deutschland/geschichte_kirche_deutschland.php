<HTML><HEAD><TITLE>Geschichte der Kirche in Deutschland</TITLE>
<META http-equiv=Content-Type content="text/html; charset=iso-8859-1"><LINK 
title=fonts href="../philosophie_theologie/kaltefleiter.css" type=text/css 
rel=stylesheet>
<META content="MSHTML 5.50.4134.600" name=GENERATOR></HEAD>
<BODY bgColor=#ffffff leftMargin=6 topMargin=6 marginheight="6" marginwidth="6">
<TABLE cellSpacing=0 cellPadding=6 width="100%" border=0>
  <TBODY>
  <TR>
    <TD vAlign=top align=left width=100> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="../philosophie_theologie/boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="../philosophie_theologie/boxtop.gif"><img src="../philosophie_theologie/boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="../philosophie_theologie/boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="../philosophie_theologie/boxtopleft.gif"><img src="../philosophie_theologie/boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b>Kirche in Deutschland</b></td>
          <td background="../philosophie_theologie/boxtopright.gif"><img src="../philosophie_theologie/boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="../philosophie_theologie/boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="../philosophie_theologie/boxdivider.gif"><img src="../philosophie_theologie/boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="../philosophie_theologie/boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="../philosophie_theologie/boxleft.gif"><img src="../philosophie_theologie/boxleft.gif" width="8" height="8" alt=""></td>
          <td> <?php include("logo.html"); ?> </td>
          <td background="../philosophie_theologie/boxright.gif"><img src="../philosophie_theologie/boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="../philosophie_theologie/boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="../philosophie_theologie/boxbottom.gif"><img src="../philosophie_theologie/boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="../philosophie_theologie/boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="../philosophie_theologie/boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="../philosophie_theologie/boxtop.gif"><img src="../philosophie_theologie/boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="../philosophie_theologie/boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="../philosophie_theologie/boxtopleft.gif"><img src="../philosophie_theologie/boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="../philosophie_theologie/boxtopright.gif"><img src="../philosophie_theologie/boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="../philosophie_theologie/boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="../philosophie_theologie/boxdivider.gif"><img src="../philosophie_theologie/boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="../philosophie_theologie/boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="../philosophie_theologie/boxleft.gif"><img src="../philosophie_theologie/boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10"> <?php include("az.html"); ?> </td>
          <td background="../philosophie_theologie/boxright.gif"><img src="../philosophie_theologie/boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="../philosophie_theologie/boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="../philosophie_theologie/boxbottom.gif"><img src="../philosophie_theologie/boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="../philosophie_theologie/boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </TD>
    <TD vAlign=top rowSpan=2>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
        <TBODY>
        <TR vAlign=top align=left>
          <TD width=8><IMG height=8 alt="" 
            src="../philosophie_theologie/boxtopleftcorner.gif" width=8></TD>
          <TD background=../philosophie_theologie/boxtop.gif><IMG height=8 alt="" 
            src="../philosophie_theologie/boxtop.gif" width=8></TD>
          <TD width=8><IMG height=8 alt="" 
            src="../philosophie_theologie/boxtoprightcorner.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=../philosophie_theologie/boxtopleft.gif><IMG height=8 alt="" 
            src="../philosophie_theologie/boxtopleft.gif" width=8></TD>
          <TD bgColor=#e2e2e2> 
            <H1><font face="Arial, Helvetica, sans-serif">Geschichte der Kirche
                in Deutschland</font></H1>
          </TD>
          <TD background=../philosophie_theologie/boxtopright.gif><IMG height=8 
            alt="" src="../philosophie_theologie/boxtopright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=13 alt="" src="../philosophie_theologie/boxdividerleft.gif" 
            width=8></TD>
          <TD background=../philosophie_theologie/boxdivider.gif><IMG height=13 
            alt="" src="../philosophie_theologie/boxdivider.gif" width=8></TD>
          <TD><IMG height=13 alt="" 
            src="../philosophie_theologie/boxdividerright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=../philosophie_theologie/boxleft.gif><IMG height=8 alt="" 
            src="../philosophie_theologie/boxleft.gif" width=8></TD>
          <TD class=L12>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td valign="top" class="L12"><P><STRONG><font face="Arial, Helvetica, sans-serif">Ein
                        kurzer &Uuml;berblick </font></STRONG></P>
                  <P><font face="Arial, Helvetica, sans-serif"><a href="http://www.kath.de/kurs/kg/">mehr
                        Informationen </a></font></P>
                  <p><font face="Arial, Helvetica, sans-serif">Die ersten Christen
                      kamen als Kaufleute und Soldaten &uuml;ber das r&ouml;mische
                      Stra&szlig;ensystem in das linksrheinische Deutschland
                      und die Gebiete westlich des Limes. In den St&auml;dten
                      wie K&ouml;ln, Mainz, Speyer und auch Augsburg gab es bereits
                      Bisch&ouml;fe.<br>
  Als sich mit der V&ouml;llerwanderung im 4. und 5. Jahrhundert die r&ouml;mische
  staatliche Ordnung aufl&ouml;ste und die Bev&ouml;lkerung nach S&uuml;deuropa
  auswich, wurde Deutschland wieder zum Missionsland. Ein wichtiges Datum war
  die Taufe des Frankenk&ouml;nigs Chlodwig am Weihnachtsfest des Jahres 498.
  Die Missionierung der germanischen St&auml;mme, die die r&ouml;mischen H&ouml;fe &uuml;bernahmen
  und rechts des Rheins eigene Siedlungen gr&uuml;ndeten, erfolgte jedoch nicht
  vom S&uuml;den her, sondern von Irland und Schottland und sp&auml;ter von England
  her. Auf dem Kontinent bestanden heidnische Kulte fort, so da&szlig; eine Vertiefung
  des Glaubens notwendig war. Missionsmittel war vor allem die Gr&uuml;ndung
  von Kl&ouml;stern. Der gro&szlig;e Reformer wurde Bonifatius (672-754), ein
  englischer Benediktiner, der die Kirche neue ordnete, Kl&ouml;ster gr&uuml;ndete
  und bei den Hessen, in Th&uuml;ringen und im Friesland missionierte. Bonifatius
  hatte die Voraussetzungen geschaffen, da&szlig; sich unter Karl d. Gr. durch
  eine systematische Bildungspolitik, die Reorganisation und die Gr&uuml;ndung
  neuer Kl&ouml;ster das Abendland entwickeln konnte. <br>
  Im Verlauf des Mittelalsters wuchs die Bev&ouml;lkerung, die Landwirtschaft
  konnte mehr Menschen ern&auml;hren, so da&szlig; St&auml;dte entstehen konnten.
  W&auml;hrend die Abteien der Benediktiner und Zisterzienser landwirtschaftliche
  Zentren waren und damit nicht zum Kern f&uuml;r Stadtgr&uuml;ndungen wurden,
  kamen im 13. Jahrhundert die Franziskaner und Dominikaner, wie auch die Augustiner
  und andere Orden in die St&auml;dte. Die Gotik bestimmte die Architektur und
  die anderen K&uuml;nste.<br>
  Das Mittelalter war alles andere als eine harmonische Zeit, das 14. Jahrhundert
  war von Pestepedemien und gro&szlig;en innerkirchlichen Konflikten gepr&auml;gt.
  In dieser Zeit entstanden Laienbewegungen wie die Beginen und Begarden. Das
  Mittelalter m&uuml;ndete in eine Zeit gro&szlig;er religi&ouml;ser Unsicherheit.
  Anders als im 13. Jahrhundert waren die Menschen sich nicht mehr der Gnade
  Gottes sicher. Nach den Pestepedemien, den innerkirchlichen Auseinandersetzungen
  und den Kriegen waren die Menschen auf der Suche nach dem gn&auml;digen Gott.
  Diese Befindlichkeit verdichtet sich in der Person Luthers, der die Theologie
  des Heidenapostels Paulus neu formuliert, indem er die Rechtfertigung zum zentralen
  theologischen Thema macht und damit der religi&ouml;sen Sehnsucht des Zeitalters
  Ausdruck verleiht. Die mittelalterliche Ordnung, gegr&uuml;ndet auf dem labilen
  Gleichgewicht zwischen Papst und Kaiser, zerbrach. Die Neuzeit war auch durch
  den Buchdruck mit bewegten Lettern und einer neuen Bildungsinitiative angebrochen.
  Auf evangelischer Seite war es Melanchton, auf katholischer Petrus Canisius,
  die &uuml;ber die Gr&uuml;ndung von Bildungseinrichtungen den Menschen eine
  neue geistige und religi&ouml;se Basis gaben.<br>
  Da der Kaiser auf Seiten der katholischen Konfession geblieben war, brauchte
  Luther die Unterst&uuml;tzung der F&uuml;rsten. Diese nutzen die neue religi&ouml;se
  Bewegung, um sich vom Kaiser unanh&auml;ngig zu machen. Diese aus dem Mittelalter
  bekannte Verkn&uuml;pfung von Religion und Politik m&uuml;ndete in einen Konfessionskrieg,
  der als Drei&szlig;igj&auml;hriger Krieg in die Geschichte eingegangen ist.
  Dieser Krieg zerr&uuml;ttete nicht nur die Lebensgrundlagen der Bev&ouml;lkerung,
  sondern verband das Thema Religion mit traumatischen Erfahrungen von Gewalt.
  Am Ende dieses Krieges mit unz&auml;hlig vielen Schlachten gab es &uuml;ber
  300 deutsche Staaten und reichsfreie St&auml;dte. Die Religion der Untertanen
  war durch den F&uuml;rsten vorgegebne. Da die meisten der Staaten protestantisch
  geworden waren, lebten die Katholiken meist unter dem Krummstab, d.h. geh&ouml;rten
  zu einem Bistum oder einer Abtei, deren Bischof bzw. Abt zugleich Landesf&uuml;rst
  waren.<br>
  Nach dem langen B&uuml;rgerkrieg folgte im Barock eine Zeit des Aufbaus. Inspiriert
  war der Barock von Rom her und daher eher in den katholischen Regionen pr&auml;gend.
  Der Barock war universell, Naturwissenschaften wurden gepflegt. Seit Leibniz
  und Newton die Berechnung von Kurven entwickelt hatten, konnte sich in der
  Architektur ein gro&szlig;er Formenreichtum entwickeln. Musik, Theater und
  Theologie fanden zu einer Einheit, wie sie vorher nur das hohe Mittelalter
  gekannt hatte.<br>
  Die umgreifende Idee des Barock hatte sich Ende des 18. Jahrhunderts ersch&ouml;pft.
  Er wurde durch die kritische Rationalit&auml;t der Aufkl&auml;rung abgel&ouml;st,
  die in der franz&ouml;sischen Revolution sich politisch Bahn brach. Ziel waren
  die Freiheitsrechte des einzelnen B&uuml;rgers, Bildungschancen f&uuml;r jeden
  und die Einf&uuml;hrung demokratischer Strukturen. Auch diese geistige Bewegung
  f&uuml;hrte zu einem europweiten Krieg, der Kriegsbringer war Napoleon. Als
  dieser besiegt war, wurden beim Wiener Kongre&szlig; die duetsche Kleisntaaterei &uuml;berwunden.
  Es entstanden sehr viele gr&ouml;&szlig;ere K&ouml;nigreiche und Gro&szlig;herzogt&uuml;mer,
  Bayern erhielt z.B. die heutigen fr&auml;nkischen Regierungsbezirke. Die katholischen
  Bisch&ouml;fe und &Auml;bte waren 1803 in der S&auml;kularisierung bereits
  durch Napoleon abgesetzt und deren L&auml;ndereien den deutschen Staaten &uuml;bergeben
  worden. Nach dem Wiener Kongre&szlig; fanden sich die meisten Katholiken, au&szlig;er
  in Bayern, unter protestantischen F&uuml;rstenh&auml;usern wieder. Es wurden
  neue Bist&uuml;mer wie Limburg, Rottenburg, Freiburg gegr&uuml;ndet. Insgesamt
  wurden die Bistumsgrenzen auf die L&auml;ndergrenzen abgestimmt, so da&szlig; die
  Bist&uuml;mer Mainz, Passau und Speyer heute sehr viel kleiner sind als vor
  der napoleonischen Zeit.<br>
  Mit dem Parlament in der Paulskirche von 1848 gab es eine neue Demokratiebewegung,
  von der vor allem die Katholiken profitierten, weil sie Religionsfreiheit erhielten.
  Die Theologie stand in Auseinandersetzung mit der Infragestellung aller Glaubensartikel
  durch die aufkl&auml;rerische Philosophie. Wunder und Auferstehung Jesu wurden
  angezweifelt, die Gottessohnschaft Jesu abgelehnt. Trotz dieses eisigen Windes,
  der der Religion ins Gesicht blies, kam es ab Mitte des Jahrhunderts zu einem
  gro&szlig;en religi&ouml;sen Aufschwung. vor allem unter den Katholiken. Orden
  und Diakonissenstifte f&uuml;r die Krankenpflege wurden gegr&uuml;ndet, die
  katholische Kirche entwickelte eine eigene Soziallehre, die durch das Zentrum
  im Parlament vertreten wurde und organisierte viele Menschen in ihren Verb&auml;nden.
  Da die katholische Kirche st&auml;rker an der bereits im Mittelalter erk&auml;mpften
  Trennung von Staat und Kirche festgehalten hatte und die Katholiken in den
  meist von protestantischen F&uuml;rsten regierten Staaten weniger Chancen hatten,
  entwickelten sie in ihrer Kirche starke Strukturen. Die internationale Ausrichtung
  der katholischen Kirche entsprach nicht den starken nationalen Tendenzen des
  19. Jahrhunderts, so da&szlig; Bismarck das Erstarken des katholischen Lebens
  mit dem Kulturkampf einzud&auml;mmen versuchte, dadurch aber den Katholizismus
  nur st&auml;rker machte.<br>
  Bereits im 18. Jahrhundert nutzten die Theologen die Methoden der Literatur-
  und Geschichtswissenschaften, um die Bibel besser zu verstehen und die Theologen
  der ersten christlichen Jahrhunderte zu edieren. Die Romantik, die die katholische
  Welt mehr pr&auml;gte, f&uuml;hrte zu einer Renaissance des Mittelalters. Dome
  und Kirchen wurden zu Ende gebaut, so der K&ouml;lner Dom und das Ulmer M&uuml;nster.
  In den durch die Industrialisierung neu entstehenden Stadtvierteln wurden neugotische
  und neoromanische Kirchen gebaut. Der Karneval lebte neu auf.<br>
  Nach dem ersten Weltkrieg mu&szlig;ten Kaiser und F&uuml;rsten abdanken, in
  den deutschen L&auml;ndern wurde die Demokratie endg&uuml;ltig eingef&uuml;hrt.
  F&uuml;r das religi&ouml;se Leben wurden dadurch neue Freir&auml;ume geschaffen.
  Die Theologie entwickelte sich stark. <br>
  Der Nationalsozialismus entwickelte sich immer kirchenfeindlicher und konnte
  in den nur 12 Jahren seines diktatorischen Regimes den Kirchen gro&szlig;en
  geistigen und materiellen Schaden zuf&uuml;gen.<br>
  Die Bundesrepublik besann sich nach den Erfahrungen mit Nationalsozialismus
  und auch Kommunismus auf das christliche Wertesystem. Die katholische Soziallehre
  wie Denkschriften der evangelischen Kirche beeinflu&szlig;ten die Gesetzgebung
  in hohem Ma&szlig;.<br>
  Die katholische Kirche wurde durch das II. Vatikanische Konzil (1962-65) gepr&auml;gt.
  Die bisherigen Seelsorgsstrukturen der Pfarrei, der Ordensh&auml;user und Sozialen
  Einrichtungen wurden beibehalten, jedoch kam es zu dem Aufbau von Laiengremien,
  die in gewisser Weise die Organisationsform der katholischen Verb&auml;nde
  abl&ouml;sten. <br>
  Geistig waren die Kirchen seit Ende der sechziger Jahre mit einer zweiten Aufkl&auml;rung
  konfrontiert. Die Achtundsechziger-Bewegung war kirchenkritisch und suchte
  ihre Fundamente beim fr&uuml;hen Marx, der im Anschlu&szlig; an Ludwig Feuerbach
  die Existenz Gottes ablehnte, weil diese die freie Entfaltung des Menschen
  und damit den Fortschritt behindert. In der DDR sah sich besonders die evangelische
  Kirche einer Zerm&uuml;rbungsstrategie der kommunistischen Partei ausgesetzt,
  die wie schon die Nazis die christliche Symbolik durch sozialistische ersetzte.
  Hauptstreitpunkt war die Konfirmation, gegen die die DDR die Jugendweihe setzte.<br>
  Seit durch die &ouml;kologischen Probleme und die wachsende Arbeitslosigkeit
  der die Moderne tragende Glaube an den Fortschritt seine Kraft eingeb&uuml;&szlig;t
  hat, kommt es seit Mitte der achtziger Jahre zu einem neuen Interesse am Religi&ouml;sen,
  das sich vor allem am Erfolg von religi&ouml;sen Fernsehprogrammen und Zugriffszahlen
  auf Internetseiten mit religi&ouml;sen Inhalten zeigt. Spiritualit&auml;t ist
  die Ausdruckform dieses neuen religi&ouml;sen Interesses.<br>
                  </font></p>
                  <p><font face="Arial, Helvetica, sans-serif">Eckhard Bieger
                      S.J.</font></p>
                  <p></p>
                  <P>&copy;<a href="http://www.kath.de"> <font face="Arial, Helvetica, sans-serif">www.kath.de</font></a></P></td>
                <td valign="top"><script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
google_ad_width = 160;
google_ad_height = 600;
google_ad_format = "160x600_as";
google_ad_type = "text_image";
//2007-09-05: Bildung und Erziehung
google_ad_channel = "3955521420";
google_ui_features = "rc:6";
//-->
            </script>
                    <script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
              </script>
                </td>
              </tr>
            </table>
            <P>&nbsp;</P>
            </TD>
          <TD background=../philosophie_theologie/boxright.gif><IMG height=8 alt="" 
            src="../philosophie_theologie/boxright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=8 alt="" src="../philosophie_theologie/boxbottomleft.gif" 
            width=8></TD>
          <TD background=../philosophie_theologie/boxbottom.gif><IMG height=8 alt="" 
            src="../philosophie_theologie/boxbottom.gif" width=8></TD>
          <TD><IMG height=8 alt="" src="../philosophie_theologie/boxbottomright.gif" 
            width=8></TD></TR></TBODY></TABLE></TD></TR>
  <TR>
    <TD vAlign=top align=left>&nbsp; </TD>
  </TR></TBODY></TABLE></BODY></HTML>
