<HTML><HEAD><TITLE>Bistum Hildesheim</TITLE>
<meta name="title" content="Kirche in Deutschland">
<meta name="author" content="Redaktion kath.de">
<meta name="publisher" content="kath.de">
<meta name="copyright" content="kath.de">
<meta name="description" content="Lexikon �ber die Kirche in Deutschland">
<meta name="abstract" content="Lexikon �ber die Kirche in Deutschland">
<meta http-equiv="content-language" content="de">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="date" content="2006-00-01">
<meta name="robots" content="index,follow">
<meta name="revisit-after" content="10 days">
<meta name="revisit" content="after 10 days">
<meta name="DC.Title" content="Kirche in Deutschland">
<meta name="DC.Creator" content="Redaktion kath.de">
<meta name="DC.Contributor" content="u.a.">
<meta name="DC.Rights" content="kath.de">
<meta name="DC.Publisher" content="kath.de">
<meta name="DC.Date" content="2006-00-01">
<meta name="DC.Description" content="Lexikon �ber die Kirche in Deutschland">
<meta name="DC.Language" content="de">
<meta name="DC.Type" content="Text">
<meta name="DC.Format" content="text/html">
<meta name="DC.Identifier" content="http://www.kath.de/lexikon/kirche_in_deutschland/">
<meta name="keywords" lang="de" content="Geschichte, Rosenstock, Kaiser Ludwig I. Adolf Betram, Bernward, Gedehard, Bischof Homeyer">
</HEAD>
<BODY bgColor=#ffffff leftMargin=6 topMargin=6 marginheight="6" marginwidth="6">
<TABLE cellSpacing=0 cellPadding=6 width="100%" border=0>
  <TBODY>
  <TR>
    <TD vAlign=top align=left width=100> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="../kirche_deutschland/boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="../kirche_deutschland/boxtop.gif"><img src="../philosophie_theologie/boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="../kirche_deutschland/boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="../kirche_deutschland/boxtopleft.gif"><img src="../kirche_deutschland/boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b>Kirche in Deutschland</b></td>
          <td background="../kirche_deutschland/boxtopright.gif"><img src="../kirche_deutschland/boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="../kirche_deutschland/boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="../kirche_deutschland/boxdivider.gif"><img src="../kirche_deutschland/boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="../kirche_deutschland/boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="../kirche_deutschland/boxleft.gif"><img src="../philosophie_theologie/boxleft.gif" width="8" height="8" alt=""></td>
          <td> <?php include("logo.html"); ?> </td>
          <td background="../kirche_deutschland/boxright.gif"><img src="../kirche_deutschland/boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="../kirche_deutschland/boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="../kirche_deutschland/boxbottom.gif"><img src="../kirche_deutschland/boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="../kirche_deutschland/boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="../kirche_deutschland/boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="../kirche_deutschland/boxtop.gif"><img src="../philosophie_theologie/boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="../kirche_deutschland/boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="../philosophie_theologie/boxtopleft.gif"><img src="../philosophie_theologie/boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="../philosophie_theologie/boxtopright.gif"><img src="../philosophie_theologie/boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="../kirche_deutschland/boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="../kirche_deutschland/boxdivider.gif"><img src="../philosophie_theologie/boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="../kirche_deutschland/boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="../philosophie_theologie/boxleft.gif"><img src="../kirche_deutschland/boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10"> <?php include("az.html"); ?> </td>
          <td background="../philosophie_theologie/boxright.gif"><img src="../kirche_deutschland/boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">  
          <td><img src="../kirche_deutschland/boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="../kirche_deutschland/boxbottom.gif"><img src="../kirche_deutschland/boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="../kirche_deutschland/boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </TD>
    <TD vAlign=top rowSpan=2>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
        <TBODY>
        <TR vAlign=top align=left>
          <TD width=8><IMG height=8 alt="" 
            src="../kirche_deutschland/boxtopleftcorner.gif" width=8></TD>
          <TD background=../philosophie_theologie/boxtop.gif><IMG height=8 alt="" 
            src="../kirche_deutschland/boxtop.gif" width=8></TD>
          <TD width=8><IMG height=8 alt="" 
            src="../kirche_deutschland/boxtoprightcorner.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=../kirche_deutschland/boxtopleft.gif><IMG height=8 alt="" 
            src="../kirche_deutschland/boxtopleft.gif" width=8></TD>
          <TD bgColor=#e2e2e2> 
            <H1><font face="Arial, Helvetica, sans-serif">Bistum Hildesheim</font></H1>
          </TD>
          <TD background=../kirche_deutschland/boxtopright.gif><IMG height=8 
            alt="" src="../kirche_deutschland/boxtopright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=13 alt="" src="../kirche_deutschland/boxdividerleft.gif" 
            width=8></TD>
          <TD background=../kirche_deutschland/boxdivider.gif><IMG height=13 
            alt="" src="../kirche_deutschland/boxdivider.gif" width=8></TD>
          <TD><IMG height=13 alt="" 
            src="../kirche_deutschland/boxdividerright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=../kirche_deutschland/boxleft.gif><IMG height=8 alt="" 
            src="../philosophie_theologie/boxleft.gif" width=8></TD>
          <TD class=L12>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td valign="top" class="L12"><P><STRONG><font face="Arial, Helvetica, sans-serif">Geistiges
                        Zentrum seit dem 9. Jahrhudnert</font></STRONG></P>
                  <P><font face="Arial, Helvetica, sans-serif"> Das Bistum Hildesheim
                      ist eines der landschaftlich sch&ouml;nsten und abwechslungsreichsten.
                      Ein Bistum mit Gegens&auml;tzen: Es hat gro&szlig;e Anteile
                      am Harz und reicht bis zur K&uuml;ste zwischen Hamburg
                      und Cuxhaven. Unter seelsorglichen Gesichtpunkten bilden
                      das katholisch gepr&auml;gte Eichsfeld im S&uuml;den und
                      die bev&ouml;lkerungsarme und &uuml;berwiegend evangelische
                      K&uuml;stenregion deutliche Gegenpole. Zwar ist das Bistum
                      mit fast 1.200 Jahren eines der &auml;ltesten Bist&uuml;mer
                      Deutschlands, geh&ouml;rt aber zur Diaspora, denn nur 11,9
                      Prozent der Gesamtbev&ouml;lkerung sind katholisch, rund
                      657.600 Seelen. Das Bistum Hildesheim ist mit einer Fl&auml;che
                      von rund 30.000 Quadratkilometern das drittgr&ouml;&szlig;te
                      Fl&auml;chenbistum Deutschlands, nach den Erzbist&uuml;mern
                      Hamburg (32.600 Quadratkilometer) und Berlin (31.200 Quadratkilometer).</font></P>
                  <p><font face="Arial, Helvetica, sans-serif">Bischof ist Norbert
                      Trelle, er ist der 70. in der Reihe der Hildesheimer Bisch&ouml;fe.
                      Weihbisch&ouml;fe sind Hans-Georg Koitz und Dr. Nikolaus
                      Schwerdtfeger. 463 Priester, davon rund 300 in der Seelsorge
                      aktiv, begleiten die Katholiken, die in 314 Pfarrgemeinden
                      und 26 Dekanaten zusammengefasst sind. Den Priestern stehen
                      unter anderem 92 Diakone, 83 Pastoralreferenten und 124
                      Gemeindereferenten zur Seite.</font></p>
                  <p><font face="Arial, Helvetica, sans-serif">Das Haushaltsvolumen
                      des Bistums Hildesheim lag im Jahre 2004 bei 131,33 Millionen
                      Euro. 89,0 Millionen Euro stammten aus Kirchensteuern.
                      Im Haushaltsjahr 2004 musste ein Defizit von 8,2 Millionen
                      Euro aus der R&uuml;cklage ausgeglichen werden.</font></p>
                  <p><font face="Arial, Helvetica, sans-serif">Am Beginn der
                      Geschichte von Bistum und Stadt Hildesheim steht eine Rose.
                      Die Gr&uuml;ndungslegende erz&auml;hlt, Kaiser Ludwig I.,
                      genannt Ludwig der Fromme (778 bis 840) sei in die Gegend
                      des sp&auml;teren Hildesheim gekommen. An jener Stelle,
                      wo sich heute der Dom erhebt, soll Ludwig eine Messe in
                      freier Natur gefeiert und dabei ein mitgef&uuml;hrtes Marienreliquiar
                      an dem Ast eines Rosenstocks aufgeh&auml;ngt haben. Der
                      Hofstaat zog weiter und der verantwortliche Kaplan verga&szlig; die
                      Reliquie. Als er einen Tag sp&auml;ter seine Unaufmerksamkeit
                      bemerkte und wieder an die Stelle kam, um das Reliquiar
                      abzunehmen, konnte er es nicht vom Ast l&ouml;sen. Der
                      Kaiser sah darin angeblich einen Fingerzeig Gottes, hier
                      einen Bischofssitz zu errichten. Auf Ludwigs Gehei&szlig; sei
                      dann an dieser Stelle eine Kapelle zu Ehren der Gottesmutter
                      errichtet worden. Sp&auml;ter entstand am selben Platz
                      ein Dom &#8211; Keimzelle der Stadt und des Bistums Hildesheim.</font></p>
                  <p><font face="Arial, Helvetica, sans-serif">Unstrittig ist,
                      dass die Geschichte des Bistums und der Stadt Hildesheim
                      um 815 als karolingische Stiftung Ludwigs des Frommen beginnt.
                      Schon damals stand das Bistum unter dem Schutz Mariens.
                      In der Fr&uuml;hzeit kommt es zu einer ersten historischen
                      Bl&uuml;te durch drei Bisch&ouml;fe, die sp&auml;ter heilig
                      gesprochen wurden: Bischof Altfried, der den ersten gro&szlig;en
                      Dom erbaute und wichtige Kl&ouml;ster und Stifte gr&uuml;ndete,
                      um die Jahrtausendwende dann Bischof Bernward, dessen &#8222;Bernwardinische
                      Kunst&#8220; in der &#8222;Bernwardst&uuml;r&#8220; des
                      Doms und in der Abteikirche St. Michael ihren H&ouml;hepunkt
                      fand. Dom und St. Michael sind seit 1985 Weltkulturerbe.
                      Bischof Godehard schlie&szlig;lich wurde als Abt von Niederaltaich
                      und hervorragender Klosterreformator Nachfolger Bernwards
                      auf dem Bischofsstuhl in Hildesheim.<br>
                    </font><font face="Arial, Helvetica, sans-serif">Unter den
                    s&auml;chsischen und salischen Kaisern hatten Domkapitel
                    und Domschulen in Hildesheim und Goslar einen hervorragenden
                    Ruf. Zeitweilig kam jeder vierte neu ernannte Bischof in
                    Deutschland aus Hildesheim, darunter auch der ber&uuml;hmte
                    Reichskanzler und Erzbischof von K&ouml;ln, Rainald von Dassel.<br>
                    </font><font face="Arial, Helvetica, sans-serif">Die Neuzeit
                    des Bistums beginnt mit dem Jahr 1824, als die Grenzen der
                    Di&ouml;zese neu festgelegt wurden. Von da an umfasst sie
                    den gr&ouml;&szlig;ten Teil des heutigen Niedersachsens,
                    n&auml;mlich das Gebiet zwischen Weser und Unterelbe. Zu
                    den herausragenden Gestalten der neueren Zeit des Bistums
                    geh&ouml;rt Adolf Bertram (1859-1945), zun&auml;chst Domherr
                    und Bischof von Hildesheim, sp&auml;ter Erzbischof von Breslau
                    und Kardinal. Er machte sich auch als Historiker und Schriftsteller
                    sowie als Vorsitzender der Fuldaer Bischofskonferenz einen
                    Namen.<br>
                    </font><font face="Arial, Helvetica, sans-serif">Die Zeit
                    nach dem Zweiten Weltkrieg ist gepr&auml;gt durch die Folgen
                    der Vertreibung aus den deutschen Ostgebieten und das Einstr&ouml;men
                    von einer halben Million Katholiken in das Bistum Hildesheim.
                    Kennzeichnend f&uuml;r diese Zeit ist der Bau von 300 Kirchen.
                    Allein unter Bischof Heinrich Maria Janssen (Bischof von
                    1957 bis 1982) entstanden 245 Gottesh&auml;user.<br>
                    </font><font face="Arial, Helvetica, sans-serif">Dr. Josef
                    Homeyer, ehemaliger Sekret&auml;r der Deutschen Bischofskonferenz
                    (DBK), wurde am 13. November 1983 zum 69. Bischof von Hildesheim
                    geweiht. Wie vom Kirchenrecht vorgeschrieben bot Bischof
                    Dr. Josef Homeyer dem Papst zu seinem 75. Geburtstag am 1.
                    August 2004 den R&uuml;cktritt an. Johannes Paul II. hat
                    dieses R&uuml;cktrittsgesuch zum 20. August 2004 angenommen.</font></p>
                  <p></p>
                  <p></p>
                  <P><a href="http://www.bistum-hildesheim.de/"><font size="2" face="Arial, Helvetica, sans-serif">&copy; Bistum
                    Hildesheim</font></a></P></td>
                <td valign="top"><script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
google_ad_width = 160;
google_ad_height = 600;
google_ad_format = "160x600_as";
google_ad_type = "text_image";
//2007-09-05: Bildung und Erziehung
google_ad_channel = "3955521420";
google_ui_features = "rc:6";
//-->
            </script>
                    <script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
              </script>
                </td>
              </tr>
            </table>
            <P>&nbsp;</P>
            </TD>
          <TD background=../philosophie_theologie/boxright.gif><IMG height=8 alt="" 
            src="../philosophie_theologie/boxright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=8 alt="" src="../philosophie_theologie/boxbottomleft.gif" 
            width=8></TD>
          <TD background=../philosophie_theologie/boxbottom.gif><IMG height=8 alt="" 
            src="../philosophie_theologie/boxbottom.gif" width=8></TD>
          <TD><IMG height=8 alt="" src="../philosophie_theologie/boxbottomright.gif" 
            width=8></TD></TR></TBODY></TABLE></TD></TR>
  <TR>
    <TD vAlign=top align=left>&nbsp; </TD>
  </TR></TBODY></TABLE></BODY></HTML>
