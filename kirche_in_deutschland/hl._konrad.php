<HTML><HEAD><TITLE>Hl. Konrad</TITLE>
<meta name="title" content="Kirche in Deutschland">
<meta name="author" content="Redaktion kath.de">
<meta name="publisher" content="kath.de">
<meta name="copyright" content="kath.de">
<meta name="description" content="Lexikon �ber die Kirche in Deutschland">
<meta name="abstract" content="Lexikon �ber die Kirche in Deutschland">
<meta http-equiv="content-language" content="de">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="date" content="2006-00-01">
<meta name="robots" content="index,follow">
<meta name="revisit-after" content="10 days">
<meta name="revisit" content="after 10 days">
<meta name="DC.Title" content="Kirche in Deutschland">
<meta name="DC.Creator" content="Redaktion kath.de">
<meta name="DC.Contributor" content="u.a.">
<meta name="DC.Rights" content="kath.de">
<meta name="DC.Publisher" content="kath.de">
<meta name="DC.Date" content="2006-00-01">
<meta name="DC.Description" content="Lexikon �ber die Kirche in Deutschland">
<meta name="DC.Language" content="de">
<meta name="DC.Type" content="Text">
<meta name="DC.Format" content="text/html">
<meta name="DC.Identifier" content="http://www.kath.de/lexikon/kirche_in_deutschland/">
<meta name="keywords" lang="de" content=Konrad, Bischof von Konstanz, Spinne">
</HEAD>
<BODY bgColor=#ffffff leftMargin=6 topMargin=6 marginheight="6" marginwidth="6">
<TABLE cellSpacing=0 cellPadding=6 width="100%" border=0>
  <TBODY>
  <TR>
    <TD vAlign=top align=left width=100> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="../philosophie_theologie/boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="../philosophie_theologie/boxtop.gif"><img src="../philosophie_theologie/boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="../philosophie_theologie/boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="../philosophie_theologie/boxtopleft.gif"><img src="../philosophie_theologie/boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b>Kirche in Deutschland</b></td>
          <td background="../philosophie_theologie/boxtopright.gif"><img src="../philosophie_theologie/boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="../philosophie_theologie/boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="../philosophie_theologie/boxdivider.gif"><img src="../philosophie_theologie/boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="../philosophie_theologie/boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="../philosophie_theologie/boxleft.gif"><img src="../philosophie_theologie/boxleft.gif" width="8" height="8" alt=""></td>
          <td> <?php include("logo.html"); ?> </td>
          <td background="../philosophie_theologie/boxright.gif"><img src="../philosophie_theologie/boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="../philosophie_theologie/boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="../philosophie_theologie/boxbottom.gif"><img src="../philosophie_theologie/boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="../philosophie_theologie/boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="../philosophie_theologie/boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="../philosophie_theologie/boxtop.gif"><img src="../philosophie_theologie/boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="../philosophie_theologie/boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="../philosophie_theologie/boxtopleft.gif"><img src="../philosophie_theologie/boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="../philosophie_theologie/boxtopright.gif"><img src="../philosophie_theologie/boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="../philosophie_theologie/boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="../philosophie_theologie/boxdivider.gif"><img src="../philosophie_theologie/boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="../philosophie_theologie/boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="../philosophie_theologie/boxleft.gif"><img src="../philosophie_theologie/boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10"> <?php include("az.html"); ?> </td>
          <td background="../philosophie_theologie/boxright.gif"><img src="../philosophie_theologie/boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="../philosophie_theologie/boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="../philosophie_theologie/boxbottom.gif"><img src="../philosophie_theologie/boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="../philosophie_theologie/boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </TD>
    <TD vAlign=top rowSpan=2>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
        <TBODY>
        <TR vAlign=top align=left>
          <TD width=8><IMG height=8 alt="" 
            src="../philosophie_theologie/boxtopleftcorner.gif" width=8></TD>
          <TD background=../philosophie_theologie/boxtop.gif><IMG height=8 alt="" 
            src="../philosophie_theologie/boxtop.gif" width=8></TD>
          <TD width=8><IMG height=8 alt="" 
            src="../philosophie_theologie/boxtoprightcorner.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=../philosophie_theologie/boxtopleft.gif><IMG height=8 alt="" 
            src="../philosophie_theologie/boxtopleft.gif" width=8></TD>
          <TD bgColor=#e2e2e2> 
            <H1><font face="Arial, Helvetica, sans-serif">Hl. Konrad</font></H1>
          </TD>
          <TD background=../philosophie_theologie/boxtopright.gif><IMG height=8 
            alt="" src="../philosophie_theologie/boxtopright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=13 alt="" src="../philosophie_theologie/boxdividerleft.gif" 
            width=8></TD>
          <TD background=../philosophie_theologie/boxdivider.gif><IMG height=13 
            alt="" src="../philosophie_theologie/boxdivider.gif" width=8></TD>
          <TD><IMG height=13 alt="" 
            src="../philosophie_theologie/boxdividerright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=../philosophie_theologie/boxleft.gif><IMG height=8 alt="" 
            src="../philosophie_theologie/boxleft.gif" width=8></TD>
          <TD class=L12><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td valign="top" class="L12"><font face="Arial, Helvetica, sans-serif"><strong>Bischof
                    von Konstanz</strong> </font>
                <P><font face="Arial, Helvetica, sans-serif">Im Erzbistum Freiburg,
                    insbesondere aber in der Gegend um Konstanz, finden sich
                    mitunter Bilder eines als Bischof dargestellten Heiligen,
                    der einen Kelch in der Hand h&auml;lt, &uuml;ber dem sich
                    eine gro&szlig;e Spinne an ihrem Faden herabl&auml;&szlig;t.
                    Dies ist der Konstanzer Bischof Konrad, dem der Legende zufolge
                    w&auml;hrend eines &ouml;sterlichen Hochamtes direkt nach
                    der Wandlung eine Spinne in den Kelch fiel. Er trank den
                    Kelch aus, ohne sich von der Spinne st&ouml;ren zu lassen.
                    Auch die Spinne &uuml;berstand das Abenteuer und krabbelte
                    einige Zeit sp&auml;ter unversehrt wieder aus seinem Mund. <br>
  Der aus dem Welfengeschlecht stammende Konrad wurde um 900 als Sohn des Grafen
  Heinrich von Altdorf unweit von Weingarten (Oberschwaben) geboren. Er wurde
  in der Konstanzer Domschule und im Kloster St. Gallen umfassend gebildet und
  begann seine kirchliche Karriere als Kanonikus des Konstanzer Domstifts. 934
  wurde er zum Bischof von <a href="bistum_konstanz.php">Konstanz</a> geweiht.
  In der Reichspolitik trat er, anders als etwa der mit ihm befreundete heilige
  Augsburger Bischof Ulrich oder andere zeitgen&ouml;ssische Bisch&ouml;fe, kaum
  in Erscheinung, sondern k&uuml;mmerte sich vorrangig um die Belange seines
  Bistums, dem er als weitsichtiger und energischer Verwalter gro&szlig;e Dienste
  leistete. Er zeichnete sich durch tiefe Fr&ouml;mmigkeit und gro&szlig;e Mildt&auml;tigkeit
  aus, unternahm drei Wallfahrten ins Heilige Land und verwendete einen gro&szlig;en
  Teil seines Verm&ouml;gens f&uuml;r den Bau von Kirchen und Hospit&auml;lern.
  Konrad starb am 26. November 975 und wurde in der Konstanzer Mauritiuskirche
  beigesetzt. Sp&auml;ter, wahrscheinlich im Jahr 1089, wurden seine Reliquien
  ins Konstanzer M&uuml;nster &uuml;berf&uuml;hrt, in der Reformationszeit aber
  in den Bodensee geworfen. Lediglich das Haupt wurde gerettet und wird heute
  im Konstanzer M&uuml;nsterschatz verwahrt. Schon bald wurde Konrad als Heiliger
  verehrt, die definitive Heiligsprechung erfolgte im Jahr 1123. <br>
  Er ist Patron des <a href="http://www.erzbistum-freiburg.de">Erzbistums Freiburg</a> zusammen
  mit Maria und dem <a href="hl._fidelis.php"> Hl. Fidelis</a>), der Gemeinde
  Gurtweil, sowie zahlreicher Kirchen. Das &#8222;Konradsblatt&#8220;, die Freiburger
  Bistumszeitung, ist ebenso nach ihm benannt wie die h&ouml;chste vom Freiburger
  Erzbischof zu vergebende Auszeichnung, die &#8222;Konradsplaktette&#8220;.
  Dargestellt wird er als Bischof mit Kelch und Spinne.</font></P>
                <p><font face="Arial, Helvetica, sans-serif">Dr. Christoph Schmider,
                    Leiter des Erzbisch&ouml;flichen Archivs Freiburg <br>
                </font></p>
                <p></p>
                <p></p>
                <P>&copy;<a href="http://www.ordinariat-freiburg.de/14.0.html"><font face="Arial, Helvetica, sans-serif"> Erzbistum
                    Freiburg</font></a></P></td>
              <td valign="top"><script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
google_ad_width = 160;
google_ad_height = 600;
google_ad_format = "160x600_as";
google_ad_type = "text_image";
//2007-09-05: Bildung und Erziehung
google_ad_channel = "3955521420";
google_ui_features = "rc:6";
//-->
          </script>
                  <script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
            </script>
              </td>
            </tr>
          </table>            <P>&nbsp;</P>
            </TD>
          <TD background=../philosophie_theologie/boxright.gif><IMG height=8 alt="" 
            src="../philosophie_theologie/boxright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=8 alt="" src="../philosophie_theologie/boxbottomleft.gif" 
            width=8></TD>
          <TD background=../philosophie_theologie/boxbottom.gif><IMG height=8 alt="" 
            src="../philosophie_theologie/boxbottom.gif" width=8></TD>
          <TD><IMG height=8 alt="" src="../philosophie_theologie/boxbottomright.gif" 
            width=8></TD></TR></TBODY></TABLE></TD></TR>
  <TR>
    <TD vAlign=top align=left>&nbsp; </TD>
  </TR></TBODY></TABLE></BODY></HTML>
