<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <title>Das B&ouml;se - der Brudermord</title>


  <meta name="title" content="Philosophie&amp;Theologie">

  <meta name="author" content="Redaktion kath.de">

  <meta name="publisher" content="kath.de">

  <meta name="copyright" content="kath.de">

  <meta name="description" content="">

  <meta name="abstract" content="Lexikon &uuml;ber Philosophie&amp;Theologie">

  <meta http-equiv="content-language" content="de">

  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

  <meta name="date" content="2006-00-01">

  <meta name="robots" content="index,follow">

  <meta name="revisit-after" content="10 days">

  <meta name="revisit" content="after 10 days">

  <meta name="DC.Title" content="Philosophie&amp;Theologie">

  <meta name="DC.Creator" content="Redaktion kath.de">

  <meta name="DC.Contributor" content="J&uuml;rgen Pelzer">

  <meta name="DC.Rights" content="kath.de">

  <meta name="DC.Publisher" content="kath.de">

  <meta name="DC.Date" content="2006-00-01">

  <meta name="DC.Description" content="Lexikon &uuml;ber Philosophie&amp;Theologie">

  <meta name="DC.Language" content="de">

  <meta name="DC.Type" content="Text">

  <meta name="DC.Format" content="text/html">

  <meta name="DC.Identifier" content="http://www.kath.de/lexikon/philosophie_theologie/">

  <meta name="keywords" content="Freiheit, Stimme Gottes, Gewissen, Tiefenpsychologie, Ueber-Ich, Newman, Schelling, Johann Gottlieb Fichte" lang="de">

</head>
<body leftmargin="6" topmargin="6" style="background-color: rgb(255, 255, 255);" marginheight="6" marginwidth="6">

<table border="0" cellpadding="6" cellspacing="0" width="100%">

  <tbody>

    <tr>

      <td align="left" valign="top" width="100">
      <table border="0" cellpadding="0" cellspacing="0" width="216">

        <tbody>

          <tr align="left" valign="top">

            <td width="8"><img src="boxtopleftcorner.gif" alt="" height="8" width="8"></td>

            <td background="boxtop.gif" width="200"><img src="boxtop.gif" alt="" height="8" width="8"></td>

            <td width="8"><img src="boxtoprightcorner.gif" alt="" height="8" width="8"></td>

          </tr>

          <tr align="left" valign="top">

            <td background="boxtopleft.gif"><img src="boxtopleft.gif" alt="" height="8" width="8"></td>

            <td bgcolor="#e2e2e2"><b>Philosophie&amp;Theologie</b></td>

            <td background="boxtopright.gif"><img src="boxtopright.gif" alt="" height="8" width="8"></td>

          </tr>

          <tr align="left" valign="top">

            <td><img src="boxdividerleft.gif" alt="" height="13" width="8"></td>

            <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" height="13" width="8"></td>

            <td><img src="boxdividerright.gif" alt="" height="13" width="8"></td>

          </tr>

          <tr align="left" valign="top">

            <td background="boxleft.gif"><img src="boxleft.gif" alt="" height="8" width="8"></td>

            <td><?php include("logo.html"); ?> </td>

            <td background="boxright.gif"><img src="boxright.gif" alt="" height="8" width="8"></td>

          </tr>

          <tr align="left" valign="top">

            <td><img src="boxbottomleft.gif" alt="" height="8" width="8"></td>

            <td background="boxbottom.gif"><img src="boxbottom.gif" alt="" height="8" width="8"></td>

            <td><img src="boxbottomright.gif" alt="" height="8" width="8"></td>

          </tr>

        </tbody>
      </table>

      <br>

      <table border="0" cellpadding="0" cellspacing="0" width="216">

        <tbody>

          <tr align="left" valign="top">

            <td width="8"><img src="boxtopleftcorner.gif" alt="" height="8" width="8"></td>

            <td background="boxtop.gif" width="200"><img src="boxtop.gif" alt="" height="8" width="8"></td>

            <td width="8"><img src="boxtoprightcorner.gif" alt="" height="8" width="8"></td>

          </tr>

          <tr align="left" valign="top">

            <td background="boxtopleft.gif"><img src="boxtopleft.gif" alt="" height="8" width="8"></td>

            <td bgcolor="#e2e2e2"><strong>Begriff
anklicken</strong></td>

            <td background="boxtopright.gif"><img src="boxtopright.gif" alt="" height="8" width="8"></td>

          </tr>

          <tr align="left" valign="top">

            <td><img src="boxdividerleft.gif" alt="" height="13" width="8"></td>

            <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" height="13" width="8"></td>

            <td><img src="boxdividerright.gif" alt="" height="13" width="8"></td>

          </tr>

          <tr align="left" valign="top">

            <td background="boxleft.gif"><img src="boxleft.gif" alt="" height="8" width="8"></td>

            <td class="V10"><?php include("az.html"); ?>
            </td>

            <td background="boxright.gif"><img src="boxright.gif" alt="" height="8" width="8"></td>

          </tr>

          <tr align="left" valign="top">

            <td><img src="boxbottomleft.gif" alt="" height="8" width="8"></td>

            <td background="boxbottom.gif"><img src="boxbottom.gif" alt="" height="8" width="8"></td>

            <td><img src="boxbottomright.gif" alt="" height="8" width="8"></td>

          </tr>

        </tbody>
      </table>

      </td>

      <td rowspan="2" valign="top">
      <table border="0" cellpadding="0" cellspacing="0" width="100%">

        <tbody>

          <tr align="left" valign="top">

            <td width="8"><img alt="" src="boxtopleftcorner.gif" height="8" width="8"></td>

            <td background="boxtop.gif"><img alt="" src="boxtop.gif" height="8" width="8"></td>

            <td width="8"><img alt="" src="boxtoprightcorner.gif" height="8" width="8"></td>

          </tr>

          <tr align="left" valign="top">

            <td background="boxtopleft.gif"><img alt="" src="boxtopleft.gif" height="8" width="8"></td>

            <td bgcolor="#e2e2e2">
            <h1><font face="Arial, Helvetica, sans-serif">Das
B&ouml;se - der Brudermord<br>

            </font></h1>

            </td>

            <td background="boxtopright.gif"><img alt="" src="boxtopright.gif" height="8" width="8"></td>

          </tr>

          <tr align="left" valign="top">

            <td><img alt="" src="boxdividerleft.gif" height="13" width="8"></td>

            <td background="boxdivider.gif"><img alt="" src="boxdivider.gif" height="13" width="8"></td>

            <td><img alt="" src="boxdividerright.gif" height="13" width="8"></td>

          </tr>

          <tr align="left" valign="top">

            <td background="boxleft.gif"><img alt="" src="boxleft.gif" height="8" width="8"></td>

            <td style="font-family: Helvetica,Arial,sans-serif;" class="L12">
            <p class="MsoNormal"><span style="font-weight: bold;">Brudermord als
Erkl&auml;rung des B&ouml;sen</span><br>

Jede Woche stellt das Fernsehen in st&auml;ndig neuen Variationen
den Mord dar. Der Krimi ist das Fernsehformat, das am sichersten
Zuschauer vor
dem Bildschirm versammelt. Der Krimi stellt die entscheidende Frage
nach dem
B&ouml;sen, n&auml;mlich warum der Mensch nicht nur
entsprechend den nat&uuml;rlichen Abl&auml;ufen
sterben muss, sondern andere aktiv umbringt. Der Tod als Akt nicht der
Natur,
sondern des Menschen, geh&ouml;rt zur menschlichen Situation
au&szlig;erhalb des
Paradieses, &bdquo;jenseits von Eden&ldquo;. Genau das steht
bereits auf den ersten Seiten
der Bibel. Auf die Verf&uuml;hrung durch die Schlange folgt direkt
der Brudermord,
der &uuml;brigens auch von den Stadtgr&uuml;ndern Roms
berichtet wird, Romulus bringt
seinen Bruder Remulus um. </p>

            <p class="MsoNormal">Es ist die Situation,
bevor eine erste Rechtsordnung
etabliert ist. Ohne ein kulturelles Ger&uuml;st ist der Mensch
nicht so sehr der
Gefahr ausgesetzt, von wilden Tieren zerrissen, sondern von seinem
Bruder
umgebracht zu werden. In den mythischen Erz&auml;hlungen, ob von
Kain und Abel oder
von Romulus und Remus, wird eine Ursprungserfahrung weitergegeben, die
sich
t&auml;glich neu reproduziert. Die Bibel erz&auml;hlt lakonisch:<br>

            <cite><a name="a1"></a>&bdquo;Adam
erkannte Eva, seine Frau; sie
wurde schwanger und gebar Kain. Da sagte sie: Ich habe einen Mann vom
Herrn
erworben.
            <a name="a2">Sie gebar ein zweites Mal,
n&auml;mlich Abel, seinen Bruder. Abel wurde
Schafhirt und Kain Ackerbauer. </a><a name="a3">Nach
einiger Zeit brachte Kain
dem Herrn ein Opfer von den Fr&uuml;chten des Feldes dar;</a>
            <a name="a4">auch Abel
brachte eines dar von den Erstlingen seiner Herde und von ihrem Fett.
Der Herr
schaute auf Abel und sein Opfer,</a> <a name="a5">aber
auf Kain und sein Opfer
schaute er nicht. Da &uuml;berlief es Kain ganz hei&szlig; und
sein Blick senkte sich.</a>
            </cite><a name="a6"><cite>Der Herr
sprach zu Kain: Warum &uuml;berl&auml;uft es dich
hei&szlig; und warum
senkt sich dein Blick?"</cite></a><br>

Genesis, Kap. 4,1-6</p>

            <p class="MsoNormal">Es geht nicht nur um den
Gegensatz von Hirten und Bauern,
wie er sich auch in vielen amerikanischen Western zwischen
Viehz&uuml;chtern und
Ackerbauern wiederholt, sondern noch tiefer darum, wer das
gr&ouml;&szlig;ere Lebensgl&uuml;ck
erringt. Es geht Kain n&auml;mlich darum, ob sein Lebensopfer
angenommen wird, d.h.
f&uuml;r jeden Menschen heute, ob er sich im Vergleich zu anderen
&bdquo;gl&uuml;cklich
sch&auml;tzen&ldquo; kann. Da die meisten ihr
Lebensgl&uuml;ck nicht nur im beruflichen Erfolg,
sondern auch von menschlichen Beziehungen erwarten, kommt es zu Morden
gerade
aus verschm&auml;hter Liebe.&nbsp; </p>

            <p class="MsoNormal">Kain und Abel sind nur
Gestalten, die wiedergeben, was
t&auml;glich neu erlebt wird. Das Gl&uuml;ck, der Erfolg des
anderen f&auml;hrt wie ein Blitz
in das innerste Empfinden des anderen. In der Bibel hei&szlig;t es:
&bdquo;Der Herr sprach
zu Kain: Warum &uuml;berl&auml;uft es dich hei&szlig; und
warum senkt sich dein Blick?&ldquo; Weil es
uns im Letzten um das Gelingen unseres Lebens geht, kommt aus der
Rivalit&auml;t
nicht nur schlechtes Gef&uuml;hl, sondern Mord. Der andere wird zum
Rivalen meines
Lebensgl&uuml;cks. Das Gl&uuml;ck des anderen scheint mir mein
Lebensgl&uuml;ck zu nehmen.
Weil der andere gl&uuml;cklich ist, scheint mir das Gl&uuml;ck
verwehrt. Damit mir das
Gl&uuml;ck zugeteilt wird, muss ich den Rivalen aus dem Weg
r&auml;umen. Wenn er nicht
mehr da ist, finde ich Platz f&uuml;r mein Gl&uuml;ck. Der
Ausweg besteht nur im Verzicht
auf Nachahmung, dass ich nicht das Gl&uuml;ck des anderen, sondern
meines will,
indem ich dem anderen sein Gl&uuml;ck lasse. Siehe auch: <a href="boese_begehren.php">das b&ouml;se Begehren</a>.</p>

            <p>Die T&ouml;tung des Bruders
wird bei Romulus und Remus auch durch Rivalit&auml;t
ausgel&ouml;st und wird mit etwas
Heiligem in Verbindung gebracht. Beide wurden von ihrem
Gro&szlig;vater Numitor
beauftragt, an der Stelle, an der sie ausgesetzt und von einer
W&ouml;lfin gefunden
worden waren, eine Stadt zu bauen. Romulus und Remus gerieten
dar&uuml;ber in
Streit, wer Bauherr der Stadt sein sollte. Sie einigten sich darauf,
dass es
der sein sollte, der die meisten Adler beobachten w&uuml;rde. Jeder
von beiden
stellte sich auf einen H&uuml;gel. Remus z&auml;hlte 6 Adler
und Romulus 12. Daraufhin
zog Romulus eine Furche, die die Grenze der Stadt bestimmte und begann
mit dem
Bau der Stadtmauer. Remus verspottete seinen Bruder dadurch, dass er
&uuml;ber die noch
niedrige Mauer sprang. Da eine Stadtmauer als heilig galt, war das
&Uuml;berspringen
aus kultischen Gr&uuml;nden verboten. Romulus geriet in Zorn und
erschlug seinen
Bruder mit der Drohung. &bdquo;Jedem wird es so ergehen, der
&uuml;ber meine Mauern
springt!&ldquo;</p>

            <p>In dieser
Erz&auml;hlung klingt auch das Motiv an, dass der eine Hirte ist,
der andere sesshaft
wird, denn die Zwillinge werden als Hirten beschrieben, die mit anderen
im
Kampf um Weidepl&auml;tze stehen. Romulus baut eine Mauer,
f&uuml;r den Hirten Remulus ist
eine Mauer, die Romulus baut, hinderlich.</p>

            <p>Eckhard
Bieger S.J.</p>

            <p><br>

&copy; <a href="http://www.kath.de">www.kath.de</a></p>

            </td>

            <td background="boxright.gif"><img alt="" src="boxright.gif" height="8" width="8"></td>

          </tr>

          <tr align="left" valign="top">

            <td><img alt="" src="boxbottomleft.gif" height="8" width="8"></td>

            <td style="font-family: Helvetica,Arial,sans-serif;" background="boxbottom.gif"><img alt="" src="boxbottom.gif" height="8" width="8"></td>

            <td><img alt="" src="boxbottomright.gif" height="8" width="8"></td>

          </tr>

        </tbody>
      </table>

      </td>

      <td style="vertical-align: top;">
      <script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
google_ad_width = 336;
google_ad_height = 280;
google_ad_format = "336x280_as";
google_ad_type = "text_image";
//2006-10-23: Lexikon Symbole
google_ad_channel = "8495253357";
//--></script>
      <script type="text/javascript" src="show_ads.js">
      </script></td>

    </tr>

    <tr>

      <td align="left" valign="top">&nbsp; </td>

    </tr>

  </tbody>
</table>

</body>
</html>
