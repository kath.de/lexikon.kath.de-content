<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>

  


  
  <title>Das B&ouml;se - und Gottes Sch&ouml;pfung</title>
  <meta name="title" content="Philosophie&amp;Theologie">


  
  <meta name="author" content="Redaktion kath.de">


  
  <meta name="publisher" content="kath.de">


  
  <meta name="copyright" content="kath.de">


  
  <meta name="description" content="">


  
  <meta name="abstract" content="Lexikon &uuml;ber Philosophie&amp;Theologie">


  
  <meta http-equiv="content-language" content="de">


  
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">


  
  <meta name="date" content="2006-00-01">


  
  <meta name="robots" content="index,follow">


  
  <meta name="revisit-after" content="10 days">


  
  <meta name="revisit" content="after 10 days">


  
  <meta name="DC.Title" content="Philosophie&amp;Theologie">


  
  <meta name="DC.Creator" content="Redaktion kath.de">


  
  <meta name="DC.Contributor" content="J&uuml;rgen Pelzer">


  
  <meta name="DC.Rights" content="kath.de">


  
  <meta name="DC.Publisher" content="kath.de">


  
  <meta name="DC.Date" content="2006-00-01">


  
  <meta name="DC.Description" content="Lexikon &uuml;ber Philosophie&amp;Theologie">


  
  <meta name="DC.Language" content="de">


  
  <meta name="DC.Type" content="Text">


  
  <meta name="DC.Format" content="text/html">


  
  <meta name="DC.Identifier" content="http://www.kath.de/lexikon/philosophie_theologie/">


  
  <meta name="keywords" content="Anspruch, Gott, Entscheidung, Freiheit, Trotzalter, Sarte" lang="de">
</head>


<body leftmargin="6" topmargin="6" style="background-color: rgb(255, 255, 255);" marginheight="6" marginwidth="6">


<table border="0" cellpadding="6" cellspacing="0" width="100%">


  <tbody>


    <tr>


      <td align="left" valign="top" width="100">
      
      <table border="0" cellpadding="0" cellspacing="0" width="216">


        <tbody>


          <tr align="left" valign="top">


            <td width="8"><img src="boxtopleftcorner.gif" alt="" height="8" width="8"></td>


            <td background="boxtop.gif" width="200"><img src="boxtop.gif" alt="" height="8" width="8"></td>


            <td width="8"><img src="boxtoprightcorner.gif" alt="" height="8" width="8"></td>


          </tr>


          <tr align="left" valign="top">


            <td background="boxtopleft.gif"><img src="boxtopleft.gif" alt="" height="8" width="8"></td>


            <td bgcolor="#e2e2e2"><b>Philosophie&amp;Theologie</b></td>


            <td background="boxtopright.gif"><img src="boxtopright.gif" alt="" height="8" width="8"></td>


          </tr>


          <tr align="left" valign="top">


            <td><img src="boxdividerleft.gif" alt="" height="13" width="8"></td>


            <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" height="13" width="8"></td>


            <td><img src="boxdividerright.gif" alt="" height="13" width="8"></td>


          </tr>


          <tr align="left" valign="top">


            <td background="boxleft.gif"><img src="boxleft.gif" alt="" height="8" width="8"></td>


            <td><?php include("logo.html"); ?> </td>


            <td background="boxright.gif"><img src="boxright.gif" alt="" height="8" width="8"></td>


          </tr>


          <tr align="left" valign="top">


            <td><img src="boxbottomleft.gif" alt="" height="8" width="8"></td>


            <td background="boxbottom.gif"><img src="boxbottom.gif" alt="" height="8" width="8"></td>


            <td><img src="boxbottomright.gif" alt="" height="8" width="8"></td>


          </tr>


        
        </tbody>
      
      </table>


      <br>


      
      <table border="0" cellpadding="0" cellspacing="0" width="216">


        <tbody>


          <tr align="left" valign="top">


            <td width="8"><img src="boxtopleftcorner.gif" alt="" height="8" width="8"></td>


            <td background="boxtop.gif" width="200"><img src="boxtop.gif" alt="" height="8" width="8"></td>


            <td width="8"><img src="boxtoprightcorner.gif" alt="" height="8" width="8"></td>


          </tr>


          <tr align="left" valign="top">


            <td background="boxtopleft.gif"><img src="boxtopleft.gif" alt="" height="8" width="8"></td>


            <td bgcolor="#e2e2e2"><strong>Begriff
anklicken</strong></td>


            <td background="boxtopright.gif"><img src="boxtopright.gif" alt="" height="8" width="8"></td>


          </tr>


          <tr align="left" valign="top">


            <td><img src="boxdividerleft.gif" alt="" height="13" width="8"></td>


            <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" height="13" width="8"></td>


            <td><img src="boxdividerright.gif" alt="" height="13" width="8"></td>


          </tr>


          <tr align="left" valign="top">


            <td background="boxleft.gif"><img src="boxleft.gif" alt="" height="8" width="8"></td>


            <td class="V10"><?php include("az.html"); ?> </td>


            <td background="boxright.gif"><img src="boxright.gif" alt="" height="8" width="8"></td>


          </tr>


          <tr align="left" valign="top">


            <td><img src="boxbottomleft.gif" alt="" height="8" width="8"></td>


            <td background="boxbottom.gif"><img src="boxbottom.gif" alt="" height="8" width="8"></td>


            <td><img src="boxbottomright.gif" alt="" height="8" width="8"></td>


          </tr>


        
        </tbody>
      
      </table>


      </td>


      <td rowspan="2" valign="top">
      
      <table border="0" cellpadding="0" cellspacing="0" width="100%">


        <tbody>


          <tr align="left" valign="top">


            <td width="8"><img alt="" src="boxtopleftcorner.gif" height="8" width="8"></td>


            <td background="boxtop.gif"><img alt="" src="boxtop.gif" height="8" width="8"></td>


            <td width="8"><img alt="" src="boxtoprightcorner.gif" height="8" width="8"></td>


          </tr>


          <tr align="left" valign="top">


            <td background="boxtopleft.gif"><img alt="" src="boxtopleft.gif" height="8" width="8"></td>


            <td bgcolor="#e2e2e2">
            
            <h1><font face="Arial, Helvetica, sans-serif">
Das B&ouml;se - und Gottes Sch&ouml;pfung</font></h1>


            </td>


            <td background="boxtopright.gif"><img alt="" src="boxtopright.gif" height="8" width="8"></td>


          </tr>


          <tr align="left" valign="top">


            <td><img alt="" src="boxdividerleft.gif" height="13" width="8"></td>


            <td background="boxdivider.gif"><img alt="" src="boxdivider.gif" height="13" width="8"></td>


            <td><img alt="" src="boxdividerright.gif" height="13" width="8"></td>


          </tr>


          <tr align="left" valign="top">


            <td background="boxleft.gif"><img alt="" src="boxleft.gif" height="8" width="8"></td>


            <td style="font-family: Helvetica,Arial,sans-serif;" class="L12">
            
            <p class="MsoNormal"><o:p></o:p>Das
B&ouml;se wird als Macht erfahren, es scheint sogar
m&auml;chtiger
als das Gute zu sein. Das k&ouml;nnte bedeuten, dass es zwei
M&auml;chte gibt, eine, die
das Gute betreibt und eine andere, die gegen das Gute k&auml;mpft.
So &auml;hnlich war
die Vorstellung der Germanen von der Weltordnung. In ihrer Mythologie
stehen
auf der einen Seite die guten G&ouml;tter, ihnen gegen&uuml;ber
die Riesen, die am Ende
sogar die G&ouml;tter besiegen. Der Manich&auml;isms, eine
persische Weltvorstellung,
kennt sogar zwei Gottheiten, eine gute und eine b&ouml;se. Diese
Vorstellung hat
auch in die fr&uuml;he christliche Kirche hinein gewirkt. Markion
erkl&auml;rte den Gott des
Alten Testaments als den B&ouml;sen, den des Neuen Testaments als
den Guten. Wenn
aber sowohl das Alte wie das Neue Testament nur einen Gott
verk&uuml;ndet, der
Sch&ouml;pfer alles Existierenden ist, dann w&auml;re er auch
irgendwie Ursprung des
B&ouml;sen. Zumindest hat er eine Welt geschaffen, in der das
B&ouml;se bestimmend ist.
Das f&uuml;hrt zu einem Dilemma. Entweder hat Gott auch das
B&ouml;se gewollt oder er ist
nicht m&auml;chtig genug, es zu verhindern. Da Gott als das
h&ouml;chste Gut bestimmt
wird, folgt daraus, dass der das Gute wirklich wollen muss. Dann
m&uuml;sste er das
B&ouml;se verhindern. Wenn er jedoch ganz gut ist, dann kapituliert
er vor dem
B&ouml;sen. Daraus folgt aber, dass er nicht allm&auml;chtig
sein kann. Die
&bdquo;Rechtfertigung Gottes angesichts des
B&ouml;sen&ldquo;, ist daher eine Frage, die seit
Generationen diskutiert wird. Der Philosoph Leibniz hat daf&uuml;r
den Begriff
Theodizee eingef&uuml;hrt.</p>


            
            <p class="MsoNormal"><o:p>&nbsp;</o:p>Die
Theodizee-Frage, die durch den Holocaust und den Gulag
noch zugespitzt wurde, legt das Problem in das Handeln bzw.
Nicht-Handeln
Gottes. Tats&auml;chlich gehen die j&uuml;dische und die
christliche Religion davon aus,
dass Gott etwas gegen die &Uuml;berwindung des B&ouml;sen tut.
Erst einmal im einzelnen,
indem er dem S&uuml;nder die Vergebung des pers&ouml;nlich
angerichteten &Uuml;bels verspricht
und am Ende der Zeiten das Gelingen der menschlichen Geschichte. Jesus
wird als
Retter der ganzen Menschheit geglaubt. Aber h&auml;tte Gott nicht
von Anfang an eine
Welt schaffen k&ouml;nnen, in der das B&ouml;se gar nicht
vorkommt? Dazu drei
&Uuml;berlegungen. </p>


            
            <ol style="margin-top: 0cm;" start="1" type="1">


              <li class="MsoNormal" style="">Was
ist das B&ouml;se, das als L&uuml;ge, Gier, Gewalt bis zum
V&ouml;lkermord auftritt, in seinem Kern?</li>


              <li class="MsoNormal" style="">Warum
ist es nicht aus der Sch&ouml;pfung prinzipiell
auszuschlie&szlig;en?</li>


              <li class="MsoNormal" style="">Warum
ist das Tier nicht b&ouml;se, sondern erst der Mensch?</li>


            
            </ol>


            
            <p class="MsoNormal"><o:p></o:p><b style="">Das B&ouml;se mindert und
vernichtet Leben<o:p></o:p></b><br>


Das B&ouml;se tritt in vielerlei Gestalten auf. Es kann Betrug
sein, Irref&uuml;hrung des anderen, ihm Schaden zuf&uuml;gen,
ihn in seiner Existenz
nicht nur mindern, sondern ihn ausl&ouml;schen. Das B&ouml;se
f&uuml;hrt am Ende ins <a href="boese_gute.php"><u>Nichts</u></a>,&nbsp;<span style="color: red;"></span>erst einmal des
anderen, dem der Mensch B&ouml;ses zuf&uuml;gt, dann aber auch
f&uuml;r den B&ouml;sen selbst. Er
verliert den Sinn seines Lebens. Das ist in vielen Romanen dargestellt,
die
Biographie von Hitler zeigt es schl&uuml;ssig. Am Ende seines
Mordens hat er keine
andere Idee als Selbstmord. In der Bibel wird die gleiche Logik von
Judas, dem
Verr&auml;ter, berichtet.</p>


            
            <p class="MsoNormal">Das B&ouml;se ist
also keine Alternative zum Guten, auch wenn das
immer wieder gesagt wird, der Mensch m&uuml;sse zwischen Gut und
B&ouml;se w&auml;hlen. Das
stimmt zwar, dass er sich entscheiden muss. Aber was w&auml;hlt er
eigentlich, wenn
er sich f&uuml;r das B&ouml;se entscheidet? Er w&auml;hlt
nicht ein anderes Leben, sondern
weniger Leben. Das Weniger betrifft erst einmal den anderen, dem etwas
weggenommen, der beeintr&auml;chtigt, gesch&auml;digt,
umgebracht wird. B&ouml;se ist unter
dieser R&uuml;cksicht aber nicht nur aktive Sch&auml;digung,
sondern das
Nicht-Erm&ouml;glichen. Wenn Kinder nicht angemessen
gef&ouml;rdert werden,
beeintr&auml;chtigt das ihre Lebenschancen und w&auml;re damit
bereits b&ouml;se. Ein
aktuelles Beispiel ist das Doping. Es nimmt dem Konkurrenten die
Chancen auf
einen Sieg und sch&auml;digt die Gesundheit desjenigen, der durch
Doping sportlichen
Erfolg erzielt. Das zeigt: Der einzelne kann auch gegen sich
b&ouml;se handeln, wenn
er die eigene Gesundheit sch&auml;digt, sich nicht fortbildet,
seine Arbeit nicht
gut organisiert. </p>


            
            <p class="MsoNormal"><b style=""><o:p></o:p>Die
Laster sind das
Einfallstor des B&ouml;sen<o:p></o:p></b><br>


Wenn das B&ouml;se im Kern Leben mindert, dann wird der Auftrag,
das Gute zu tun, in seiner Notwendigkeit deutlich. Wenn der einzelne
sich nicht
um das Gute bem&uuml;ht, &ouml;ffnet er bereits dem
B&ouml;sen die T&uuml;r. Das ist deshalb so,
weil der Mensch wie auch die menschliche Geschichte auf Entwicklung
angelegt
ist. Aus jedem einzelnen wie aus dem Gemeinsamen soll etwas werden. Wer
sich
selbst nicht entwickelt und anderen keine
Entwicklungsm&ouml;glichkeiten einr&auml;umt,
bereitet dem B&ouml;sen den Weg. Er schadet dem anderen nicht
direkt, er f&ouml;rdert
aber auch nicht den anderen. Das wird am Gegenbild deutlich. Wer
anderen M&ouml;glichkeiten
erschlie&szlig;t, wer sich f&uuml;r andere einsetzt, gewinnt
auch f&uuml;r sich an Gutem. Die
Lasterkataloge beschreiben die Einfallstore des B&ouml;sen:
Faulheit, Tr&auml;gheit,
V&ouml;llerei, Neid, Missgunst. </p>


            
            <p class="MsoNormal">F&uuml;r die
Gemeinschaft ist es schwierig, die Laster direkt zu
bek&auml;mpfen. Der Rechtsstaat kann allenfalls den Besitz von
Drogen und die
Weitergabe von Dopingmitteln bestrafen, aber nur, weil daraus aktive
Sch&auml;digung
anderer folgen kann. Der Kern des B&ouml;sen als lebensmindernde
Tendenz wird durch
die Laster an dem Ursprungsort gefasst. Die Bergpredigt und
vergleichbare
Forderungskataloge in anderen Religionen zeigen, dass das Gute eine
Aufgabe
darstellt, der sich der Mensch im eigenen Interesse ganz verschreiben
sollte. Wer
das Gute will, muss die Tendenz zu den Lastern
zur&uuml;ckdr&auml;ngen und nach
M&ouml;glichkeit ganz &uuml;berwinden. Diese Grundaussagen der
meisten Religionen sehen
das Betreiben des Guten als ureignen religi&ouml;sen Auftrag, der
von der
Gottesverehrung nicht zu trennen ist. Man kann Gott nicht verehren und
gleichzeitig dem B&ouml;sen freien Lauf lassen. Die Religionen
setzen voraus, dass
Gott das Gute will. Aber warum ist das so, wenn es in seiner
Sch&ouml;pfung zugleich
das B&ouml;se gibt?</p>


            
            <p class="MsoNormal"><o:p></o:p><b style="">Das Endliche erst
erm&ouml;glicht das B&ouml;se<o:p></o:p></b><br>


Wenn das B&ouml;se auf Minderung, Nichtigkeit, Vernichtung zielt,
dann muss es etwas geben, das gemindert und vernichtet werden kann. In
der
philosophischen Tradition sind das typische Kennzeichen des
Nicht-G&ouml;ttlichen.
Denn Gott wird seit Aristoteles als der aus sich heraus Notwendige
gedacht, der
nicht &bdquo;nicht&ldquo; sein kann. Gott ist absolut, das
Geschaffene ist endlich,
kontingent. Im Wort &bdquo;sein Kontingent zugeteilt
bekommen&ldquo; findet sich die
Bedeutung, dass es etwas Begrenztes ist. Das Endliche unterscheidet
sich von
Gott, weil es auch nicht sein k&ouml;nnte. Weil Endliches auch
nicht sein kann, kann
es gemindert und sogar vernichtet werden. Deshalb kann man davon
ausgehen, dass
in Gott das B&ouml;se keinen Platz hat, sondern erst wenn Gott
Nicht-Notwendiges,
Kontingentes, Endliches erschafft. Da das Weltall, das Leben, der
einzelne
Mensch wie auch die Menschheit auf Entwicklung angelegt sind, gibt es
das Gute
zuerst nur einmal als Disposition, die erst verwirklicht werden soll.
Der Wille
des Sch&ouml;pfers zielt auf das Gute. Im Zusammenleben der
Menschen ist die
Grundlage daf&uuml;r die Gerechtigkeit. Das B&ouml;se kann in
seiner Wurzel dann so
verstanden werden, dass es die Entwicklung des Guten nicht betreibt,
daf&uuml;r
stehen die Laster, und dass es seinen Einfluss soweit treibt, das
Menschen
direkt gesch&auml;digt werden. Die Sch&auml;digung kann bewusst
angezielt werden, was bis
zu <a href="boese_brudermord.php"><u>Mobbing</u></a>
und
V&ouml;lkermord gehen kann. Sch&auml;digung auf Grund der
Endlichkeit, des Nicht-Sein-K&ouml;nnens
des Kosmos kann auch durch Naturkatastrophen geschehen. Diese
w&auml;ren aber erst dann
b&ouml;se, wenn dahinter eine Intention ausgemacht werden
k&ouml;nnte.</p>


            
            <p class="MsoNormal"><b style=""><o:p></o:p>Tsunamis
und
Vulkanausbr&uuml;che sch&auml;digen, sind aber nicht
b&ouml;se<o:p></o:p></b><br>


Wir unterscheiden willentlich herbeigef&uuml;hrtes B&ouml;se
und
Sch&auml;digungen durch Naturvorg&auml;nge. In einem Universum,
das auf Entwicklung und
wohl auch auf ein physikalisches Ende angelegt ist, bleibt selbst der
Sternenhimmel
nicht so, wie er ist. Auch wenn das Erkalten der Sonne erst in
Milliarden von
Jahren geschehen wird, ist doch die Sonne in st&auml;ndiger
Bewegung. Auch die Erde
ist kein statisches Gebilde, sie ist eigentlich ein Feuerklumpen, der
nur an
der Oberfl&auml;che erkaltet ist. Aus dieser Entwicklung heraus
speien Vulkane Lava
und Asche aus und verursachen Verschiebungen der Erdplatte Tsunmais.
Der Mensch
erf&auml;hrt diese Naturereignisse als sch&auml;digend. Um sich
die lebensmindernde Kraft
der Naturvorg&auml;nge zu erkl&auml;ren, vermutet er hinter den
Katastrophen das Wirken
d&auml;monischerM&auml;chte. Mit der Vorstellung einer
b&ouml;sen Macht, z.B. des <u><a href="boese_drache.php">Drachen</a></u>&nbsp;<span style="color: red;"></span>legt der Mensch in die
Naturvorg&auml;nge eine Intention, die auf Sch&auml;digung des
Menschen zielt. Das ist
verst&auml;ndlich, verwischt aber den wichtigen Unterschied zu von
einem Handelnden
gezielt herbeigef&uuml;hrten Sch&auml;digungen anderer bzw. zur
Unterlassung, z.B. dem
anderen Entwicklung zu erm&ouml;glichen. Es kommt die Freiheit ins
Spiel.</p>


            
            <p class="MsoNormal"><o:p></o:p><b style="">Die Freiheit und das
B&ouml;se</b><br>


Auch wenn Hunde Passanten anfallen und sogar Kinder zu Tode
bei&szlig;en, folgen sie nur ihrem Instinkt, z.B. das eigene Revier
zu verteidigen. Oder
sie sind nicht richtig erzogen, wenn sie Mitglieder des Rudels,
n&auml;mlich ein
Kind, bei&szlig;en. Sie k&ouml;nnen f&uuml;r ihr Verhalten
nicht angeklagt werden, auch wenn es
vom 15. bis zum 18. Jahrhundert in Europa Gerichtsprozesse gegen Tiere
gab, die
sogar Todesurteile verh&auml;ngten. Tiere k&ouml;nnen nicht
zwischen Gut und B&ouml;se
unterscheiden, auch wenn sie lernen k&ouml;nnen, Anweisungen z.B.
des Hundehalters
zu befolgen oder sich dem Befehl des &ldquo;Herrchens&ldquo;
auch zu entziehen. B&ouml;se kann
nur jemand handeln, der auch das Gute erkennen kann. Tiere
k&ouml;nnen aber nur das
f&uuml;r sie N&uuml;tzliche unterscheiden. Denn den Unterschied
von Gut und B&ouml;se kann nur
ein Lebewesen entdecken, das sich von seinen Antrieben und auch von den
Zw&auml;ngen
der Umgebung distanzieren kann. Das tun wir im Alltag in der Regel
nicht, die
meisten Handlungsmuster sind eingeschliffen und wir tun das, was uns
angemessen
erscheint. Aber es gibt in Entscheidungssituationen die
F&auml;higkeit, die Alternativen
vor sich hin zu stellen und abzuw&auml;gen. Oft geschieht das auch
erst nachher,
z.B. wenn ich knapp einem Unfall entgangen bin. Obwohl ich zu
risikoreich
gefahren bin, ist es noch einmal gut gegangen, weil der andere
fr&uuml;h genug
gebremst hat. Das kann mich f&uuml;r die n&auml;chsten Wochen
dazu bringen, weniger
risikoreich zu fahren. </p>


            
            <p class="MsoNormal">Das Gute zu
w&auml;hlen oder sich oder dem anderen Schaden
zuzuf&uuml;gen, das setzt Freiheit voraus. Dadurch kommt der
Gedanke auf, die Freiheit
sei die Ursache des B&ouml;sen. Dass das ein denkerischer
Kurzschluss ist, l&auml;sst
sich allein daran aufzeigen, dass ohne die Freiheit der Mensch nicht
f&auml;hig zum
Guten w&auml;re. Freiheit ist gerade die Bef&auml;higung, das
Gute f&uuml;r andere und f&uuml;r
sich zu verwirklichen, sie ist nicht Ursache des B&ouml;sen,
sondern dazu da, das Zusammenleben,
Geschichte zu gestalten. Wer b&ouml;se handelt, ist meist Opfer
seiner Laster bzw.
seiner <a href="boese_brudermord.php"><u>Gef&uuml;hle</u></a><span style="color: red;"></span>. </p>


            
            <p class="MsoNormal">Wenn das B&ouml;se
dadurch m&ouml;glich wird, dass es etwas gibt, das
auch nicht sein k&ouml;nnte, dann zeigt sich das auch an der
Freiheit. Sie kann
etwas w&auml;hlen, das lebensmindernd, vernichtend wirkt. Sie
selbst ist aber nicht
die Ursache des B&ouml;sen, sondern unterliegt auch der
Gef&auml;hrdung alles Endlichen. Die
Wurzel des B&ouml;sen ist das Begehren, aus dem neid und <a href="boese_begehren.php"><u>Missgunst</u></a>&nbsp;<span style="color: red;"></span>folgen.</p>


            
            <p class="MsoNormal"><o:p></o:p><b style="">Zusammenfassung<o:p></o:p></b><br>


Das B&ouml;se wird erst m&ouml;glich, wenn etwas, das ist und
sich
entwickeln soll, an seiner Entwicklung gehemmt und sogar in seiner
Existenz
ausgel&ouml;scht werden kann. Das B&ouml;se liegt als
M&ouml;glichkeit in der Sch&ouml;pfung, denn
wenn etwas nicht sein kann, jedoch zu Wachstum und Vollendung hin
angelegt ist,
kann das Gute verhindert und ein Existierendes sogar vernichtet werden.
Das
B&ouml;se hat deshalb sogar noch mehr Spielraum, weil die Welt und
vor allem der Mensch
auf Entwicklung hin angelegt ist. Es kann daher nicht nur seine
Existenz
bedroht und ausgel&ouml;scht, sondern auch seine Entwicklung in
b&ouml;ser Absicht oder
durch eigene oder fremde Laster behindert werden. Mit der
Sch&ouml;pfung schafft
Gott die M&ouml;glichkeit des B&ouml;sen. Da er selbst nicht
durch Nicht-Sein-K&ouml;nnen
bedroht ist, bleibt das B&ouml;se au&szlig;erhalb von Gott. </p>


            
            <p class="MsoNormal">Deshalb ist es kein
logischer Widerspruch, dass Gott nur das
Gute wollen und trotzdem das B&ouml;se zulassen kann. Weil er aus
sich heraus
notwendig ist, da er sich vom Endlichen gerade dadurch unterscheidet,
dass er
nicht &bdquo;nicht&ldquo; sein kann, hat er mit der
Sch&ouml;pfung die M&ouml;glichkeit f&uuml;r das
B&ouml;se
mit geschaffen, aber er will es nicht, sondern das Gute. Deshalb
besagen die
Religionen, vor allem Judentum, Christentum und Islam, dass Gott seine
Sch&ouml;pfung zu einem guten Ende f&uuml;hren wird. Das
Weltgericht ist der letzte
Punkt, an dem alles von Gott auf die Vollendung hin gelenkt wird, denn
die
ganze Sch&ouml;pfung seufzt diesem Ziel entgegen. S. Zitat unten</p>


            
            <p class="MsoNormal">Welchen Sinn das
B&ouml;se im Gesamt der Weltgeschichte macht,
ist uns verborgen. Es gibt allerdings in den Religionen angelegte
Strategien,
mit dem B&ouml;sen zurecht zu kommen und es auch f&uuml;r den
eigenen
Verantwortungsbereich zur&uuml;ckzudr&auml;ngen.</p>


            
            <p class="MsoNormal"><o:p></o:p><span style=""></span><cite> &bdquo;Die ganze Sch&ouml;pfung
wartet sehns&uuml;chtig auf das
Offenbarwerden der S&ouml;hne Gottes. Die Sch&ouml;pfung ist
der Verg&auml;nglichkeit
unterworfen, nicht aus eigenem Willen, sondern durch den, der sie unterworfen
hat; aber zugleich gab er ihr Hoffnung: Auch die Sch&ouml;pfung
soll von der
Sklaverei und Verlorenheit befreit werden zur Freiheit und Herrlichkeit
der
Kinder Gottes. Denn wir wissen, dass die
gesamte Sch&ouml;pfung bis zum heutigen Tag
seufzt und in Geburtswehen liegt. Aber auch wir, obwohl wir als
Erstlingsgabe
den Geist haben, seufzen in unserem Herzen und warten darauf, dass wir mit der
Erl&ouml;sung unseres Leibes als S&ouml;hne offenbar werden.
Denn wir sind gerettet, doch
in der Hoffnung. Hoffnung aber, die man schon erf&uuml;llt sieht,
ist keine
Hoffnung.
Wie kann man auf etwas hoffen, das man sieht? Hoffen wir aber auf
das, was wir nicht sehen, dann harren wir aus in Geduld&ldquo;</cite><br>


            Paulus im Brief an die r&ouml;mische
Christengemeinde
8,19-25<span style="color: windowtext;"><o:p></o:p></span></p>


            
            <p class="MsoNormal"><span style="color: windowtext;"><o:p>&nbsp;</o:p></span></p>


            
            <p class="MsoNormal">Eckhard Bieger S.J.</p>


            <br>


            
            <p><br>


            <br>


            </p>


            
            <p>&copy;<a href="http://www.kath.de">
www.kath.de</a></p>


            </td>


            <td background="boxright.gif"><img alt="" src="boxright.gif" height="8" width="8"></td>


          </tr>


          <tr align="left" valign="top">


            <td><img alt="" src="boxbottomleft.gif" height="8" width="8"></td>


            <td background="boxbottom.gif"><img alt="" src="boxbottom.gif" height="8" width="8"></td>


            <td><img alt="" src="boxbottomright.gif" height="8" width="8"></td>


          </tr>


        
        </tbody>
      
      </table>


      </td>


      <td style="vertical-align: top;">
      
      <script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
google_ad_width = 336;
google_ad_height = 280;
google_ad_format = "336x280_as";
google_ad_type = "text_image";
//2006-10-23: Lexikon Symbole
google_ad_channel = "8495253357";
//--></script>
      
      <script type="text/javascript" src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
      </script></td>


    </tr>


    <tr>


      <td align="left" valign="top">&nbsp; </td>


    </tr>


  
  </tbody>
</table>


</body>
</html>
