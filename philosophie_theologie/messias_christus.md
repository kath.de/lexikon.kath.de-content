---
title: Messias - Christus
author: 
tags: 
created_at: 
images: []
---


# **Christus, Christos, Massiah, Gesalbter*
# **Auf den Messias richten sich die Erwartungen der Juden. Wenn er kommt, wird er ein Reich des Friedens für das Volk Israel bringen. Die Christen behaupten, daß Jesus der Messias ist. Welchen Messias erwarten die Juden und warum sind die Christen überzeugt, daß Jesus der Messias ist, die Juden aber nicht?**
 Daß Jesus gegenüber der jüdischen Öffentlichkeit mit einem Anspruch aufgetreten ist, geht aus den Berichten der Bibel deutlich hervor. Warum wäre es auch sonst zu einem Prozeß vor dem jüdischen Religionsgericht gekommen. Jesus wurde vom jüdischen Hohen Rat unter Vorsitz des Hohenpriesters Kaiphas wegen Gotteslästerung verurteilt. Die Verurteilung durch Pilatus hatte wohl eher einen politischen Hintergrund. Die Kreuzesinschrift, die Pilatus anbringen läßt, bezeichnet Jesus als den König der Juden.***

 Das griechische Christos ist die wörtliche Übersetzung des hebräischen Messias, wörtlich als „Gesalbter“ übersetzt. Sowohl bei der Geburt wie beim Besuch der Sterndeuter wird das neugeborene Kind als der Messias verkündet. 

# **Der von den Juden erwartete messias hätte nicht sterben dürfen****
# **Die Evangelien von Matthäus und Lukas haben in einem Stammbaum die Vorfahren Jesu aufgelistet, um ihn als Nachkomme des Königs Davids zu erweisen. In mittelalterlichen Kirchen wird das durch die Wurzel Jesse dargestellt. Jesse oder Isai war der Vater Davids. Aber es hat viele Nachkommen Davids gegeben. In den Büchern der Könige wird ihre Regentschaft beschrieben. Die Eroberung Jerusalems durch Nebukadnezar hat dem Königtum ein Ende gesetzt. Herodes, zur Zeit Jesu König von Roms Gnaden, war ein Emporkömmling und konnte sich nicht auf die Abstammung aus dem jüdischen Königsgeschlecht berufen.**
 Aber selbst wenn Jesus ein Nachkomme Davids war, er hätte nach jüdischer Vorstellung das Reich seines Vaters Davids wieder aufrichten müssen, um sich für die Juden als der Messias zu erweisen. Statt dessen ist er elendig am Kreuz verendet. Wäre er der von Gott erwählte Messias, dann hätte Gott ihn vor diesem Schicksal bewahrt. Das dachten nicht nur die Gegner Jesu, sondern auch seine Jünger. Wenn Jesus wirklich der Gesandte Gottes gewesen sein soll, dann hätte Gott verhindern müssen, daß er so umgebracht wird und das noch von der römischen Besatzungsmacht. Gott, da war man sich sicher, würde seinen Erwählten nicht einem solchen Schicksal überantworten.***

 Ein gekreuzigter Messias, das ist unvorstellbar. So schreibt es auch Paulus: „Die Juden fordern Zeichen, die Griechen suchen Weisheit, Wir dagegen verkündigen Christus als den Gekreuzigten: für Juden ein empörendes Ärgernis, für Heiden eine Torheit.“ 1. Korintherbrief 1, 22-23***

 Die Christen sind dann nach Ostern mit dem Anspruch aufgetreten, Jesus von Nazareth sei der den Juden verheißene Messias. „Mit Gewißheit erkenne also das ganze Haus Israel: Gott hat ihn zum Herrn und Messias gemacht, diesen Jesus, den ihr gekreuzigt habt.“ Apostelgeschichte 2,36. Sie müssen erkannt haben, daß Gott die Kreuzigung seines Gesalbten zugelassen hat – wegen der Erlösung der Menschen. Das Gottesknechtslied (Link zu Gottesknecht) im Prophetenbuch des Jesaja ist dafür der Schlüssel. Bereits während des öffentlichen Auftretens Jesu wird die Messiasfrage diskutiert.***

# **Jesus vollbringt die Taten eines Messias****
# **Johannes der Täufer hatte den Juden gesagt: „Ich taufe euch nur mit Wasser. Der aber, der nach mir kommt, ist stärker als ich, und ich bin es nicht wert, ihm die Schuhe auszuziehen. Er wird euch mit dem Heiligen Geist und mit Feuer taufen.“ Matthäus 3,11**
 Lukas berichtet „Das Volk war voll Erwartung und alle überlegten im stillen, ob Johannes nicht vielleicht selbst der Messias sei.“ Kap. 3,15***

 Als Johannes von Herodes ins Gefängnis geworfen worden war, schickt er zwei seiner Jünger zu Jesus, um sich noch einmal zu vergewissern, daß Jesus wirklich der Messias ist. „Als die beiden Männer zu Jesus kamen, sagten sie: Johannes der Täufer hat uns zu dir geschickt und läßt dich fragen: bist du der, der kommen soll, oder müssen wir auf einen anderen warten? Damals heilte Jesus viele Menschen von ihren Krankheiten und Leiden, befreite sie von bösen Geistern und schenkte vielen Blinden das Augenlicht. Er antwortete den beiden: Geht und berichtet Johannes, was ihr gesehen habt: Blinde sehen wieder, Lahme gehen, und Aussätzige werden rein; Taube hören, Tote stehen auf, und den Armen wird das Evangelium verkündet. Selig, wer keinen Anstoß nimmt.“ Lukas 7, 20-23 Jesus sagt nicht direkt, daß er der Messias ist, aber er zitiert mit dem Satz „Blinde sehen wieder ...“ den Propheten Jesaja, der besonders auf das Kommen des Messias, des Gesalbten hingewiesen hat. Wenn seine Prophetenworte jetzt eingetreten sind, dann muß Jesus der Messias sein. 

# **Die***[ Auferstehung](auferstehung_jesu.php)                  erweist Jesus als Messias ****
 Die Männer und Frauen, die Jesus gefolgt waren, hatten in ihm den verheißenen Messias erkannt. Sonst hätten sie sich ihm nicht angeschlossen. Durch die Hinrichtung Jesu am Kreuz war dieser Glaube, daß Jesu der Gesalbte Gottes ist, zerbrochen, zumindest bei den Männern unter seinen Anhängern. Erst die ***[Auferweckung von den Toten](http://www.kath.de/Kirchenjahr/advent.php) hat sie erkennen lassen, daß Jesus wirklich der Messias ist. So predigt Petrus am Pfingsttag:***


„ Jesus, den Nazoräer, den Gott vor euch beglaubigt hat durch machtvolle Taten, Wunder und Zeichen, die er durch ihn in eurer Mitte getan hat, wie ihr selbst wißt – ihn, der nach Gottes beschlossenem Willen und Vorauswissen hingegeben wurde, habt ihr durch die Hand von Gesetzlosen ans Kreuz geschlagen und umgebracht. Gott aber hat ihn von den Wehen des Todes befreit und auferweckt .... Gott hat ihn zum Herrn und Messias gemacht.“ Apostelgeschichte 2, 22-24, 36***

 Auch wenn die Christen Jesus als den von den Propheten angekündigten Messias erkennen, können die Juden mit Recht fragen, wo das angekündigte Reich des Friedens und der Gerechtigkeit bleibt. Und sie können die Christen mit Recht fragen, warum gerade den Juden von Christen soviel Unrecht angetan wurde. Für die Christen erfüllt sich die Erwartung an das Reich Gottes erst, wenn Jesus ein zweites Mal wiederkommt, zum letzten Gericht und damit die Geschichte zu ihrem Ende bringt. Link Zu Wiederkunft Christi und zu ***[Menschensohn](menschensohn.php)***

 Der Titel Messias ist faktisch zum Eigennamen Jesu geworden, denn Christos ist der Gesalbte, die Bedeutung des Wortes Messias im Hebräischen.***

# **Zitate**
 Seht, der Tag wird kommen, Spruch des Herrn, ***

 da werde ich für David einen gerechten Sproß erwecken.***

 Er wird als König herrschen und weise handeln,***

 für Recht und Gerechtigkeit wird er sorgen im Land. ***

 Jeremia, 23,5 

# **Hört her, ihr vom Haus David! ..**
 Seht, die Jungfrau wird ein Kind empfangen,***

 sie wird einen Sohn gebären,***

 und sie wird ihm den Namen Immanuel – Gott mit uns – geben.***

 Jesaja 7,14 

# **An jenem Tag wächst aus dem Baumstumpf Isais (Geschlecht Davids) ein Reis hervor, ein junger Trieb aus seinen Wurzeln bringt Frucht. Der Geist des Herrn läßt sich nieder auf ihm ...**
 Er richtet nicht nach Augenschein und nicht nach dem Hörensagen entscheidet er,***

 sondern er richtet die Hilflosen gerecht und***

 entscheidet für die Armen, was recht ist....***

 Gerechtigkeit ist der Gürtel um seine Hüften,***

 Treue der Gürtel um seinen Leib.***

 Dann wohnt der Wolf beim Lamm,***

 der Panther liegt beim Böcklein....***

 Der Löwe frißt Stroh wie das Rind.***

 Der Säugling spielt am Schlupfloch der Natter.***

 Das Kind streckt seine Hand in die Höhle der Schlange.***

 Man tut nichts Böses mehr und begeht keine Verbrechen.....***

 Denn das Land ist erfüllt von der Erkenntnis des Herrn, so wie das Meer mit Wasser gefüllt ist.***

 An jenem Tag wird der Sproß aus der Wurzel sein,***

 der dasteht als Zeichen für die Nationen.***

 Aus Kap 11 im Jesajabuch 

# **Eine Stimme ruft:**
 Bahnt für den Herrn einen Weg durch die Wüste!***

 Baut in der Steppe eine ebene Straße für unseren Gott!***

 Jedes Tal soll sich heben, jeder Berg sich senken.***

 Was krumm ist, soll gerade werden und was hügelig ist, werde eben ....***

 Seht, Gott der Herr, kommt mit Macht, er herrscht mit starkem Arm.....***

 Wie ein Hirt führt er seine Herde zur Weide, er sammelt sie mit starker Hand.***

 Die Lämmer trägt er auf dem Arm, die Mutterschafe führt er behutsam.***

 Aus Kap. 40 im Jesajabuch 

# **Spruch Bileams, des Sohnes Beors, Spruch des Mannes mit geschlossenem Auge, Spruch dessen, der Gottesworte hört, der die Gedanken des Höchsten kennt, der eine Vision des Allmächtigen sieht, der daliegt mit entschleierten Augen:**
 Ich sehe, aber nicht jetzt,***

 ich erblicke ihn, aber nicht in der Nähe:***

 Ein Stern geht in Jakob auf, ein Zepter erhebt sich in Israel.***

 Buch Numeri, Kap 24, 15-17 ***

 Bileam ist kein Jude, er soll eigentlich Israel verfluchen, erkennt aber, daß es das von Gott auserwählte Volk ist und verheißt für die Zukunft einen Heilbringer, einen Stern, der über Jakob aufgeht. 

# **So spricht der Herr: du Bethlehem-Efrata,**
 so klein unter den Gauen Judas,***

 aus dir wird einer hervorgehen,***

 der über Israel herrschen soll....***

 Er wird auftreten und ihr Hirt sein in der Kraft des Herrn,***

 im hohen Namen Jahwes, seines Gottes.***

 Sie werden in Sicherheit leben,***

 denn nun reicht seine Macht bis an die Grenzen der Erde.***

 Und er wird ihr Friede sein.***

 Prophet Micha, im Kap.5 

# **Der Geist Gottes, des Herrn ruht auf mir;**
 Denn der Herr hat mich gesalbt. (Der Gesalbte ist der Messias)***

 Er hat mich gesandt, damit ich den Armen die frohe Botschaft bringe***

 Und alle heile, deren Herz zerbrochen ist,***

 damit ich den Gefangenen Entlassung verkünde***

 und den Gefesselten Befreiung.***

 Jesaja 61,1 

***Weihnachten ist nach Matthäus und Lukas der Tag der Geburt des Messias. König Herodes empfängt die Sterndeuter, die den neu geborenen König der Juden suchen. Herodes „ließ alle Hohenpriester und Schriftgelehrten des Volkes zusammenkommen und erkundigte sich bei ihnen, wo der Messias geboren werde solle. Sie antworteten ihm: In Bethlehem in Judäa.“ Matthäus 2,4-5 

# **Die Hirten erhalten von den Engel die Nachricht:**
 Heute ist euch in der Stadt Davids der Retter geboren; er ist der Messias, der Herr. Und das soll euch als Zeichen dienen: Ihr werdet ein Kind finden, das in Windeln gewickelt, in einer Krippe liegt.“ Lukas, 2,11-12 

# **Karl Rahner zeigt auf, wie sich der Mensch bereits unbewußt auf einen Heilbringer, auf den Gesalbten ausrichtet.              Ausgehend von dem Wort Jesu, daß man in dem Armen ihm selbst begegnet (s. Matthäus 25) folgert Rahner:**
„ Wenn man aus dem Wort Jesu, er selbst werde wahrhaft in jedem Nächsten geliebt, nicht ein „als ob“ oder nur die Theorie einer juristischen Anrechnung macht, dann sagt dieses von der Erfahrung der Liebe selbst her gelesene Wort, daß eine absolute Liebe, die sich radikal und vorbehaltlos auf einen Menschen einläßt, implizit Christus glaubend und immer liebend bejaht. Und das ist richtig. Denn der bloß endliche und immer unzuverlässige Mensch kann für sich allein die ihm entgegengebrachte, absolute Liebe, in der eine Person sich schlechthin „engagiert“ und an den anderen wagt, nicht als sinnvoll rechtfertigen; für sich allein könnte er nur unter Vorbehalt geliebt werden. …. Aber die Liebe …. will mehr als nur eine ihr transzendent bleibende göttliche „Garantie“: sie will eine Einheit von Gottes- und Nächstenliebe, in der Nächstenliebe und – wenn evtl. auch bloß unthematisch – Gottesliebe so erst ganz absolut ist. Damit aber sucht sie den Gottmenschen, d.h. jenen, der als Mensch mit der Absolutheit der Liebe zu Gott geliebt werden kann.“***

 Ein weiteres Indiz, daß der Mensch auf den göttlichen Messias wartet, leitet Rahner aus der Tatsache des Todes ab:***


„ Der Tod ist die eine, das ganze Leben durchwaltende Tat, in der der Mensch als Wesen der Freiheit über sich als ganzes verfügt, und zwar so, daß diese Verfügung die Annahme der absoluten Verfügtheit in der radikalen Ohnmacht ist (bzw. sein soll), die im Tod erscheint und erlitten wird. Soll aber die freie, bereite Annahme der radikalen Ohnmacht durch das über sich selbst verfügende und verfügen-wollende Freiheitswesen nicht die Annahme des Absurden sein, die dann mit ebenso gutem „Recht“ unter Protest abgelehnt werden könnte, dann impliziert diese Annahme bei dem Menschen, der zutiefst nicht abstrakte Ideen und Normen, sondern in seiner Geschichtlichkeit (schon gegebene oder zukünftige) Wirklichkeit als Grund seines Daseins bejaht, die ahnende Erwartung oder Bejahung eines (schon gegebenen oder künftig erhofften) Todes, in dem die – bei uns empirisch bleibende – Dialektik von Tat und ohnmächtigem Leiden im Tod versöhnt ist. Das ist aber nur dann der Fall, wenn diese reale Dialektik dadurch „aufgehoben“ ist, daß sie die  Wirklichkeit dessen selbst ist, der der letzte Grund dieser Zweiheit ist.“***

 Die mit der Zukunft verbundene Hoffnung sucht einen Grund:***


„ Sein (des Menschen) Gang in die Zukunft ist das beständige Bemühen, seine inneren und äußeren Selbstentfremdungen und den Abstand zu verringern zwischen dem, was er ist, und dem, was er sein will und soll. Ist die absolute Versöhnung (individuell und kollektiv) nur das ewig ferne, immer nur asymptotisch angezielte, nur in Distanz bewegende Ziel oder als absolute Zukunft das erreichbare Ziel, ohne daß es, als erreichtes, das Endliche abschaffen und in der Absolutheit Gottes verschlingen müßte? … Der Christ hat von dieser Hoffnung her ein Verständnis für das, was der Glaube in der Inkarnation und Auferstehung Jesu Christi bekennt als den irreversiblen Anfang des Kommens Gottes als der absoluten Zukunft der Welt und der Geschichte.“***

 Jesus Christus, in Sacramentum Mundi, Theologisches Lexikon für die Praxis, Bd. 2, 1968, S. 61ff 

# **Eckhard Bieger**
 ©***[ ***www.kath.de](http://www.kath.de)
