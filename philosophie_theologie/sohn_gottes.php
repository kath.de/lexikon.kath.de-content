<HTML><HEAD><TITLE>Sohn Gottes</TITLE>
<META http-equiv=Content-Type content="text/html; charset=iso-8859-1"><LINK 
title=fonts href="kaltefleiter.css" type=text/css 
rel=stylesheet>
<META content="MSHTML 5.50.4134.600" name=GENERATOR></HEAD>
<BODY bgColor=#ffffff leftMargin=6 topMargin=6 marginheight="6" marginwidth="6">
<TABLE cellSpacing=0 cellPadding=6 width="100%" border=0>
  <TBODY>
  <TR>
    <TD vAlign=top align=left width=100> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b>Philosophie&amp;Theologie</b></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td> <?php include("logo.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10"> <?php include("az.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </TD>
    <TD vAlign=top rowSpan=2>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
        <TBODY>
        <TR vAlign=top align=left>
          <TD width=8><IMG height=8 alt="" 
            src="boxtopleftcorner.gif" width=8></TD>
          <TD background=boxtop.gif><IMG height=8 alt="" 
            src="boxtop.gif" width=8></TD>
          <TD width=8><IMG height=8 alt="" 
            src="boxtoprightcorner.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxtopleft.gif><IMG height=8 alt="" 
            src="boxtopleft.gif" width=8></TD>
          <TD bgColor=#e2e2e2> 
            <H1><font face="Arial, Helvetica, sans-serif">Sohn Gottes</font></H1>
          </TD>
          <TD background=boxtopright.gif><IMG height=8 
            alt="" src="boxtopright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=13 alt="" src="boxdividerleft.gif" 
            width=8></TD>
          <TD background=boxdivider.gif><IMG height=13 
            alt="" src="boxdivider.gif" width=8></TD>
          <TD><IMG height=13 alt="" 
            src="boxdividerright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxleft.gif><IMG height=8 alt="" 
            src="boxleft.gif" width=8></TD>
          <TD class=L12>
            <P><STRONG><font face="Arial, Helvetica, sans-serif">Filius Dei,
                  Hyios Theou</font></STRONG></P>
            <P><font face="Arial, Helvetica, sans-serif">Gott hat viele S&ouml;hne und T&ouml;chter, denn seit Jesus seinen
              J&uuml;ngern das &#8222;Vater unser&#8220; vorgebetet hat, verstehen
              sich die Christen als Kinder Gottes. Ist Jesus aber in gleicher
              Weise Sohn wie die in der Taufe von Gott als Kinder Angenommenen? </font></P>
            <P><font face="Arial, Helvetica, sans-serif"><strong>Kann der Sohn Gottes geschaffen sein?<br>
            </strong>Es gibt
                eine Begebenheit am Beginn des &ouml;ffentlichen Wirkens
                  Jesu, die schon fr&uuml;h so interpretiert wurde, da&szlig; Jesus
                  von Gott in eine besondere Sohnschaft hinein genommen wurde,
                  sozusagen adoptiert worden ist. Nach den vier Evangelien begibt sich der
                  erwachsen gewordene Jesus zu dem Bu&szlig;prediger Johannes, der
                  unten im Jordantal den Menschen ins Gewissen redet, sie zur Umkehr
                  auffordert und eine Bu&szlig;taufe spendet. Johannes erkennt in
                  Jesus den Messias und will ihn nicht taufen. Doch Jesus unterzieht
                  sich der Bu&szlig;taufe und h&ouml;rt danach eine Stimme. Markus
                  berichtet: &#8222;Und als er aus dem Wasser steigt, sah er, da&szlig; der
                  Himmel sich &ouml;ffnete und der Geist wie eine Taube auf ihn herabkam.
                  Und eine Stimme aus dem Himmel sprach: Du bist mein geliebter Sohn,
                  an dir habe ich Gefallen.&#8220; Kap 1,10-11<br>
                  Diese Szene wurde schon fr&uuml;h so interpretiert, da&szlig; Gott
                  Jesus wie einen Propheten beruft und ihn <a href="christologische_streitigkeiten.php">als
                  Sohn annimmt</a>. Hintergrund
                  sind auch Inthronisationsriten j&uuml;discher K&ouml;nige, in denen
                  der K&ouml;nig &#8222;Sohn Gottes&#8220; genannt wird. Im 2.
                  Psalm spricht Gott:<br>
&#8222;
                  Ich selbst habe meinen K&ouml;nig eingesetzt, auf Zion, meinem
                  heiligen Berg.&#8220; Der Beter f&auml;hrt dann fort: &#8222;Den
                  Beschlu&szlig; des Herrn will ich kundtun. Er sprach zu mir: Mein
                  Sohn bist du. Heute habe ich dich gezeugt. Fordere von mir und
                  ich gebe dir die V&ouml;lker zum Erbe, die Enden der Erde zum Eigentum.&#8220; (Verse
                  6-8) Dieser Psalm wurde schon von den J&uuml;ngern auf Jesus &uuml;bertragen,
                  da&szlig; Gott ihn mit der Auferstehung zum Herrn der ganzen
                  Welt inthronisiert hat. Er wird heute noch in der Osterliturgie
                  gebetet. <br>
                  Die Ebioniten, eine aus dem Judentum hervorgegangene christliche
                  Gruppe, hat diese Auffassung vertreten. Jesus w&auml;re demnach
                  ein besonderer Prophet, herausgehoben sogar gegen&uuml;ber
                  den Gro&szlig;en des Alten Testaments, aber nicht wesenhaft
                  von ihnen verschieden. Diese Frage brach im Streit mit Arius neu
                  auf. Dieser betrachtete Jesus als den menschgewordenen Logos.
                  Der Logos existierte
                  schon vor der Geburt Jesu, aber er war ein Gesch&ouml;pf.
                  Bei <a href="arianismus.php">Arius</a> stand
                  weniger das Modell der Prophetenberufung im Hintergrund als
                  die philosophische &Uuml;berzeugung
                  der Griechen, da&szlig; Gott nur einer sein kann und daher
                  in Gott nicht mehrere Personen gedacht werden k&ouml;nnen. </font></P>
            <P><font face="Arial, Helvetica, sans-serif"><strong>Jesus nennt
                  Gott seinen Vater<br>
            </strong>Die Frage wurde auf dem Konzil von <a href="gottessohn.php">Nic&auml;a
                325</a> entschieden, da&szlig; Jesus
                kein Gesch&ouml;pf
              ist, sondern aus dem Vater hervorgegangen und damit Gott wesensgleich
              ist. Wie k&ouml;nnen wir Zugang zu dieser Aussage des Konzils finden,
              das sich auf Aussagen der biblischen Schriften st&uuml;tzt. Matth&auml;us &uuml;berliefert
              das Bekenntnis des Petrus, der auf die Frage Jesu &#8222;Ihr aber,
              f&uuml;r wen haltet ihr mich?&#8220; antwortet:<br>
              &quot;Du bist der Messias, der Sohn des lebendigen Gottes! Jesus sagte
              zu ihm: Selig bist du, Simon, Sohn des Jona; denn nicht Fleisch
              und Blut haben dir das offenbart, sondern mein Vater im Himmel.&#8220; (Kap.
              16,15-17)<br>
              Dieses Bekenntnis ist Petrus nicht in diesem Moment eingefallen,
              sondern da&szlig; Jesus der Sohn des lebendigen Gottes ist, hat
              er am Sprechen und der inneren Beziehung Jesu zu Gott abgelesen.
              Jesus hat sich selbst nur selten Sohn genannt, aber sehr h&auml;ufig
              von Gott als seinem Vater gesprochen.<br>
              In den Evangelien nennt Jesus 174mal Gott Abba, die im Aram&auml;ischen
              gebr&auml;uchliche vertraute Anrede f&uuml;r den Vater, so wie
              unser Papa. Weil er mit Gott wie ein Sohn mit seinem Vater lebt
              und spricht, kann er seine J&uuml;nger in seine Beziehung zu Gott
              hinein nehmen, so da&szlig; diese bis heute das &#8222;Vater unser&#8220; sprechen
              k&ouml;nnen. So kann er in seiner Einf&uuml;hrung in das Beten
              Gott als den Vater vorstellen:<br>
              Die Gl&auml;ubigen sollen sich nicht an die Stra&szlig;enecken
              stellen, damit die anderen sehen, wie sie beten. &#8222;Du aber
              geh in deine Kammer, wenn du betest, und schlie&szlig; die T&uuml;r
              zu; dann bete zu deinem Vater, der im Verborgenen ist.&#8220; Weiter
              hei&szlig;t es bei Matth&auml;us:<br>
&#8222;
              Wenn ihr betet, sollt ihr nicht plappern wie die Heiden, die meinen,
              sie werden nur erh&ouml;rt, wenn sie viele Worte machen. Macht
              es nicht wie sie; denn euer Vater wei&szlig;, was ihr braucht,
              noch ehe ihr bittet. So sollt ihr beten:<br>
              Vater unser im Himmel, dein Name werde geheiligt;<br>
              Dein Reich komme, dein Wille geschehe, wie im Himmel, so auf Erden.<br>
              Gib uns heute das Brot, das wir brauchen.<br>
              Und erla&szlig; uns unsere Schuld, wie auch wir sie unseren Schuldnern
              erlassen haben.<br>
              Und f&uuml;hre uns nicht in Versuchung, sondern rette uns vor dem
              B&ouml;sen.&#8220;<br>
              Kap. 6, 6-13<br>
              Die J&uuml;nger Jesu haben ihn in dieser unmittelbaren Beziehung
              zu Gott erlebt und ihn so als Sohn erkennen k&ouml;nnen. Weil Jesus
              die Menschen in diese Beziehung hinein nimmt, k&ouml;nnen wir das &#8222;Vater
              unser&#8220; als Kinder Gottes beten.<br>
              Paulus f&uuml;hrt diese Gebetspraxis mit dem gleichen Wort &#8222;Abba&#8220; weiter.
              Er schreibt an die Galater:<br>
&#8222;
              Weil ihr aber S&ouml;hne seid, sandte Gott den Geist seines Sohnes
              in unser Herz, den Geist, der ruft: Abba, Vater. Daher bist du
              nicht mehr Sklave, sondern Sohn; bist du aber Sohn, dann auch Erbe,
              Erbe durch Gott.&quot; Kap. 4, 6-7</font></P>
            <P><font face="Arial, Helvetica, sans-serif"><strong>Zitate</strong><br>
              In jener Zeit sprach Jesus:<br>
              Ich preise dich, Vater, Herr des Himmels und der Erde, weil du
              all das den Weisen und Klugen verborgen, den Unm&uuml;ndigen aber
              offenbart hast. Ja, Vater, so hat es dir gefallen. Mir ist von
              meinem Vater alles &uuml;bergeben worden; Niemand kennt den Sohn,
              nur der Vater, und niemand kennt den Vater, nur der Sohn und der,
              dem der Sohn es offenbaren will.&#8220;<br>
              Matth&auml;us 11,25-27</font></P>
            <p><font face="Arial, Helvetica, sans-serif">Das Gebet Jesu am &Ouml;lberg, als er in Todesangst fiel, &uuml;berliefert
              Markus:<br>
              Und er nahm Petrus, Jakobus und Johannes mit sich. Da ergriff ihn
              Furcht und Angst, und er sagte zu ihnen: Meine Seele ist zu Tode
              betr&uuml;bt. Bleibt hier und wacht. Und er ging ein St&uuml;ck
              weiter, warf sich auf die Erde nieder und betete, da&szlig; die
              Stunde, wenn m&ouml;glich, an ihm vor&uuml;bergehe. Er sprach:
              Abba, Vater, alles ist dir m&ouml;glich. Nimm diesen Kelch von
              mir. Aber nicht, was ich will, sondern was du willst, soll geschehen.
              Kap. 14,33-36</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Markus im Bericht &uuml;ber
                die Hinrichtung Jesu:<br>
  Jesus aber schrie laut auf. Dann hauchte er den Geist aus. Da ri&szlig; der
              Vorhang im Tempel von oben bis unten entzwei Als der Hauptmann,
              der Jesus gegen&uuml;berstand, ihn auf diese Weise sterben sah,
              sagte er: Wahrhaftig, dieser Mensch war Gottes Sohn. Kap 15,37-39</font></p>
            <p><font face="Arial, Helvetica, sans-serif">  Wenn also der Sohn, bevor die Welt entstand, die Herrlichkeit inne
                hatte und der h&ouml;chste Herr der Herrlichkeit war, er vom
                Himmel herabstieg und weiterhin anbetungsw&uuml;rdig ist, so
                wurde er, indem er herabstieg, nicht besser, vielmehr verbesserte
                er das Besserungsbed&uuml;rftige. Und wenn er, um zu verbessern,
                herabgestiegen ist, so wurde ihm daf&uuml;r nicht die Belohnung
                als Sohn und Gott zuerkannt, sondern er machte vielmehr uns zu
                S&ouml;hnen des Vaters. Selbst Mensch geworden, verg&ouml;ttlichte
                er die Menschen.<br>
              Athanasius, Reden gegen die Arianer (um 335) Nr. 1,38</font></p>
            <p><font face="Arial, Helvetica, sans-serif">              Cyrill von Jerusalem (313-387)<br>
                Glaube aber auch an den Sohn Gottes, den einen und einzigen, unseren
                Herrn Jesus Christus, der aus Gott als Gott gezeugt wurde, der
                aus dem Leben als Leben erzeugt, aus dem Licht als Licht erzeugt
                wurde; er ist in allem dem &auml;hnlich, der ihn gezeugt hat. Er
                hat nicht in der Zeit das Sein empfangen, sondern vor aller Zeit
                wurde er ewig und auf unbegreifliche Weise gezeugt. <br>
                Katechesen 4,7</font></p>
            <p><font size="2" face="Arial, Helvetica, sans-serif">Eckhard Bieger</font><font face="Arial, Helvetica, sans-serif">&nbsp;<br>
                </font>&copy;<a href="http://www.kath.de"> <font face="Arial, Helvetica, sans-serif">www.kath.de</font></a></p></TD>
          <TD background=boxright.gif><IMG height=8 alt="" 
            src="boxright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=8 alt="" src="boxbottomleft.gif" 
            width=8></TD>
          <TD background=boxbottom.gif><IMG height=8 alt="" 
            src="boxbottom.gif" width=8></TD>
          <TD><IMG height=8 alt="" src="boxbottomright.gif" 
            width=8></TD></TR></TBODY></TABLE></TD><td style='vertical-align:top;'>
 <script type="text/javascript"><!--
        google_ad_client = "pub-9537752113242914";
        google_ad_width = 336;
        google_ad_height = 280;
        google_ad_format = "336x280_as";
        google_ad_type = "text_image";
        //2006-10-23: Lexikon Symbole
        google_ad_channel = "8495253357";
        //--></script>
        <script type="text/javascript"
          src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
        </script>
</td>
</TR>
  <TR>
    <TD vAlign=top align=left>&nbsp; </TD>
  </TR></TBODY></TABLE></BODY></HTML>
