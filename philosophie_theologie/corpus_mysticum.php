<HTML><HEAD><TITLE>Corpus Mysticum</TITLE>
<meta name="title" content="Philosophie&amp;Theologie">
<meta name="author" content="Redaktion kath.de">
<meta name="publisher" content="kath.de">
<meta name="copyright" content="kath.de">
<meta name="description" content="">
<meta name="abstract" content="Lexikon &uuml;ber Philosophie&amp;Theologie">
<meta http-equiv="content-language" content="de">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="date" content="2006-00-01">
<meta name="robots" content="index,follow">
<meta name="revisit-after" content="10 days">
<meta name="revisit" content="after 10 days">
<meta name="DC.Title" content="Philosophie&amp;Theologie">
<meta name="DC.Creator" content="Redaktion kath.de">
<meta name="DC.Contributor" content="J�rgen Pelzer">
<meta name="DC.Rights" content="kath.de">
<meta name="DC.Publisher" content="kath.de">
<meta name="DC.Date" content="2006-00-01">
<meta name="DC.Description" content="Lexikon &uuml;ber Philosophie&amp;Theologie">
<meta name="DC.Language" content="de">
<meta name="DC.Type" content="Text">
<meta name="DC.Format" content="text/html">
<meta name="DC.Identifier" content="http://www.kath.de/lexikon/philosophie_theologie/">
<meta name="keywords" lang="de" content="Christus, Kirche, Eucharistie, Einheit der Kirche, Corpus Mysticum, Bengar, de Lubac">
</HEAD>
<BODY bgColor=#ffffff leftMargin=6 topMargin=6 marginheight="6" marginwidth="6">
<TABLE cellSpacing=0 cellPadding=6 width="100%" border=0>
  <TBODY>
  <TR>
    <TD vAlign=top align=left width=100> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b>Philosophie&amp;Theologie</b></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td> <?php include("logo.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10"> <?php include("az.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </TD>
    <TD vAlign=top rowSpan=2>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
        <TBODY>
        <TR vAlign=top align=left>
          <TD width=8><IMG height=8 alt="" 
            src="boxtopleftcorner.gif" width=8></TD>
          <TD background=boxtop.gif><IMG height=8 alt="" 
            src="boxtop.gif" width=8></TD>
          <TD width=8><IMG height=8 alt="" 
            src="boxtoprightcorner.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxtopleft.gif><IMG height=8 alt="" 
            src="boxtopleft.gif" width=8></TD>
          <TD bgColor=#e2e2e2> 
            <H1><font face="Arial, Helvetica, sans-serif"> Corpus Mysticum</font></H1>
          </TD>
          <TD background=boxtopright.gif><IMG height=8 
            alt="" src="boxtopright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=13 alt="" src="boxdividerleft.gif" 
            width=8></TD>
          <TD background=boxdivider.gif><IMG height=13 
            alt="" src="boxdivider.gif" width=8></TD>
          <TD><IMG height=13 alt="" 
            src="boxdividerright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxleft.gif><IMG height=8 alt="" 
            src="boxleft.gif" width=8></TD>
          <TD class=L12>
            <P><STRONG><font face="Arial, Helvetica, sans-serif">Zeichenhafter,
                  geheimnisvoller Leib</font></STRONG></P>
            <P><font face="Arial, Helvetica, sans-serif">Die Kirche lebt mit
                dem Anspruch, den Auftrag von Jesus Christus zu erf&uuml;llen und sein Erl&ouml;sung bewirkendes Gedenken und
              seine Offenbarung weiter zu tragen. Ausgehend vom Apostel Paulus,
              der im Brief an die Gemeinde in Korinth sagt, &#8222;Ihr aber seid
              der Leib Christi, und jeder einzelne ist ein Glied an ihm&#8220;,
              glaubt die Kirche, dass der Geist des auferstandenen Christus in
              der Kirche wirkt und sie zum Leib Christi formt.<br>
              Wie aber kann die Kirche zum Leib Christi werden und wie kann diese
              gestaltende Kraft in menschlicher Kommunikation vermittelt werden?<br>
              Wie kann die Beziehung, die Christus mit der Kirche hat, dargestellt
              werden? Vor allem angesichts der Tatsache, dass die Kirche, trotz
              ihres theologischen Anspruches, als empirisch greifbare Gemeinschaft
              von Menschen erscheint, die fehlerbehaftet sind und die sich nicht
              immer koh&auml;rent mit ihrem Selbstverst&auml;ndnis verhalten?
              Schlie&szlig;lich kann an der Kirche von au&szlig;en nicht zweifelsfrei
              abgelesen werden, dass es ihr in erster Linie um die Fortf&uuml;hrung
              des Verm&auml;chtnisses Jesu geht.<br>
              In der Brotrede im Johannesevangelium (Joh 6,54) bespricht Jesus
              dieses Problem: &#8222;Wer mein Fleisch i&szlig;t und mein Blut
              trinkt, der bleibt in mir, und ich bleibe in ihm. Wer mein Fleisch
              i&szlig;t und mein Blut trinkt, hat das ewige Leben, und ich werde
              ihn auferwecken am Letzten Tag.&#8220;</font></P>
            <p><font face="Arial, Helvetica, sans-serif">Jesus stellt die Beziehung
                zu ihm in die Metapher einer Mahlgemeinschaft dar, deren Speise
                er selbst ist. Er macht die Mahlgemeinschaft
              im Abendmahl zum zentralen Erinnerungszeichen an seine Erl&ouml;sungstat,
              das sich sehr fr&uuml;h in der Christengemeinde als Ritus des &#8222;Brotbrechen&#8220; etabliert.
              Dazu sagt der Apostel Paulus: <br>
              Denn ich habe vom Herrn empfangen, was ich euch dann &uuml;berliefert
              habe: Jesus, der Herr, nahm in der Nacht, in der er ausgeliefert
              wurde, Brot, sprach das Dankgebet, brach das Brot und sagte: Das
              ist mein Leib f&uuml;r euch. Tut dies zu meinem Ged&auml;chtnis!
              Ebenso nahm er nach dem Mahl den Kelch und sprach: Dieser Kelch
              ist der Neue Bund in meinem Blut. Tut dies, sooft ihr daraus trinkt,
              zu meinem Ged&auml;chtnis! Denn sooft ihr von diesem Brot e&szlig;t
              und aus dem Kelch trinkt, verk&uuml;ndet ihr den Tod des Herrn,
              bis er kommt. 1Kor 11, 23 &#8211; 26</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Der Ritus des Brotbrechens
                formt also aus der Vielzahl der Einzelmitglieder, aus den Christen,
                die Kirche als Leib Christi. Dieser wird vollzogen
              durch die Teilnahme am Ritus des Brotbrechens. Der Empfang des
              Leibes Christi im rituellen Vollzug, bestehend im Brotbrechen und
              im Ritus des Kelches, ist also verbunden mit seiner Bedeutung,
              der in der Formung der Gemeinschaft der Kirche zum Leib Christi
              besteht. Dieser Zusammenhang, diese Beziehung zwischen Zeichen
              (Brotbrechen) und der darin sich bergenden Wirklichkeit (Kirche,
              Verbundenheit in einem Leib mit Christus) wurde ab dem 4. Jahrhundert
              als &#8222;mystisch&#8220; bezeichnet. Es handelt sich dabei, kommunikationstheoretisch
              gesprochen, um eine Symbolhandlung mit identit&auml;tsstiftender
              Wirkung, die dem Glauben nach jedoch real ist, d.h. das Zeichen,
              dem Sinn und der Form nach richtig vollzogen, bewirkt, was es bezeichnet,
              den Leib Christi.<br>
            </font><font face="Arial, Helvetica, sans-serif">Dadurch
              entsteht ein enger Zusammenhang zwischen der Feier der Eucharistie
              und der
                Identit&auml;t der Kirche. Es ist die Feier
              der Eucharistie, durch die Kirche Einheit bekommt, und es ist die
              Kirche als Gemeinschaft der Glaubenden, die sich in der Feier der
              Eucharistie mit Christus vereint.<br>
              Urspr&uuml;nglich bedeutet Corpus mysticum die Beziehung zwischen
              dem Ritus des Brotbrechens und seiner Wirkung, der Leib Christi-Werdung
              der Kirche. Diesen komplexe Zusammenhang im Blick zu behalten ohne
              die Interdependenz von Kirche und Eucharistie zugunsten eines der
              beiden Pole zu vereinseitigen, ist nicht immer einfach. <br>
              Manche Theologen gerieten in den Verdacht, die Beziehung zu Jesus
              Christus nur symbolisch zu verstehen, so dass die seinsm&auml;&szlig;ige
              (ontologische) Gegenwart des physischen Leibes in den Zeichen von
              Brot und Wein nicht hinreichend klar wurde. Die Gegenwart Christi
              wurde als eine Art virtuelle Gegenwart verstanden wird. (z. B.
              Berengar) Deshalb entstand gegen Ende des ersten Jahrtausends das
              Bed&uuml;rfnis, die Gegenwart Christi in den Zeichen von Brot und
              Wein, unabh&auml;ngig von deren Wirkungen, eigens zu definieren.
              (<a href="realpraesenz.php">Realpr&auml;senz</a>) <br>
            </font><font face="Arial, Helvetica, sans-serif">Damit
              wurde, so de Lubac, die Rechtgl&auml;ubigkeit bewahrt, aber
              der vitale Nerv durchtrennt. Dieser besteht darin, dass durch die
              Teilnahme am Ritus des Brotbrechens nicht nur die Gaben Brot und
              Wein, sondern die Teilnehmer verwandelt und zum Leib Christi geformt
              werden, so dass beides, die Wandlung der Gaben und die Wandlung
              der feiernden Gemeinde, einander gegenseitig bedingen.<br>
              Aus apologetischen Bem&uuml;hungen heraus ging man zunehmend dazu &uuml;ber,
              den biblischen Leib (der gelitten hat und auferstanden ist) und
              den sakramentalen Leib (Brot und Wein) nicht mehr begrifflich zu
              trennen. So sollte die reale Pr&auml;senz Christi in der Feier
              der Eucharistie definitorisch garantiert werden. <br>
              Das Symbol mit seiner Mehrdeutigkeit und Dynamik wurde nicht mehr
              gebraucht, ja wurde zum Gegenbegriff zur Transsubstantiation. Dadurch
              ging der Begriff Coprus mysticum von dem verwandelten Brot und
              Wein auf die dadurch verursachte Wirkung, die in zum Lieb Christi
              gewordene Kirche, &uuml;ber und verselbst&auml;ndigte sich. Der
              Zusammenhang zwischen dem rituell-symbolischen Vollzug der Eucharistiefeier
              und der dadurch entstehenden Einheit der Kirche ging zunehmend
              verloren. Corpus Mysticum wurde zum eher statischen Begriff, der
              die ontologischen Qualit&auml;ten der Kirche, unabh&auml;ngig von
              der ihr inh&auml;renten spirituellen Dynamik, bezeichnet.<br>
              <br>
              <strong>Theologische Argumente</strong><br>
              Die Beziehung zum Jesus des Glaubens ist &#8222;mystisch&#8220;,
              d. h. sie realisiert sich in zeichenhaft-symbolischen Aktionen
              (<a href="http://www.kath.de/lexikon/liturgie/ritus_sinnhandlung.php">Riten</a>) und in Meditation und Gebet. Diese Kommunikationen vollziehen
              sich in zwei Dimensionen: Zum einen zeichenhaften und damit nicht
              physisch-real. Wir begegnen Jesus nicht wie einem Gegenwartsmenschen. <br>
              Zum anderen geheimnishaft, im Gegensatz zum empirisch-faktischen.
              Die Beziehung ist nicht deshalb geheimnishaft, weil jemand ein
              Geheimnis h&uuml;tet, sondern weil sie sich, schritt- und erfahrungsweise
              erschlie&szlig;t. Sie ist aber ihrer Art nach so, dass sie niemals
              im Sinne einer Beherrschung ersch&ouml;pfend erfasst werden kann. <br>
              In der Abendmahlsszene hat Jesus selbst dem Ritus des Kelches und
              des Brotbrechens eine neue, weiterf&uuml;hrende Bedeutung mit den
              Worten gegeben: &#8222;Das ist mein Leib, das ist mein Blut. Tut
              dies zu meinem Ged&auml;chtnis.&#8220; <br>
              Brot und Wein werden damit konstituiert als Beziehungsmedium mit
              Jesus Christus durch sein Fleisch und Blut. Sie werden zum sakramentalen
              (zeichenhaften und objektiv wirksamen) Leib Jesu Christi. Durch
              die Feier des Brotbrechens (Eucharistie) wird dadurch eine neue
              Qualit&auml;t der Beziehung geschaffen, die in der Kommunion (Verbindung)
              mit Jesus Christus durch den Empfang seines Fleisches und Blutes
              vollzogen wird. <br>
              Dadurch bekommt der einzelne Anteil an der Erl&ouml;sungswirkung
              der Selbstmitteilung Gottes. Diese objektiv-pers&ouml;nliche Beziehungsnahme
              mit Jesus Christus, die durch die Eucharistie m&ouml;glich wird,
              verbindet die Teilnehmenden der Eucharistiefeier.<br>
            </font><font face="Arial, Helvetica, sans-serif">- mit dem Leib Christi, der am Kreuz gemartert und der durch die
              Auferstehung in einen himmlischen Leib verwandelt wurde, <br>
              -	mit dem sakramentalen Leib (Brot und Wein) <br>
              -	und macht sie dadurch zu dem &#8222;Leib Christi&#8220;, Corpus
              Mysticum, verstanden als Kirche.</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Hier bekommt &#8222;Corpus Mysticum&#8220; eine
                korporativ-soziologische Bedeutung und meint die Kirche, sowohl
                in ihren verfassten Strukturen
              als auch in ihrer Bedeutung als Zeichen der Gegenwart des Geistes
              Gottes in der Welt. </font><font face="Arial, Helvetica, sans-serif">Corpus
              Mysticum hat sich in der nachmittelalterlichen Theologie als exklusive
              Begriff
                f&uuml;r die Bezeichnung der Kirche entwickelt,
              w&auml;hrend die sakramentale Gegenwart Jesu Christi in Brot und
              Wein mit &#8222;corpus reale&#8220; bezeichnet wurde. Diese Entwicklung
              wurde durch die Enzyklika &#8222;Mystici Corporis&#8220; von Pius
              XII. vom Lehramt &uuml;bernommen.</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Diese &#8222;Ratio mystica&#8220;, die Verbindung des geschichtlichen
              Leibes Christi mit dem sakramentalen unter den Zeichen von Brot
              und Wein und dem kirchlichen, beschreibt eine Relation von Zeichen
              und der im Zeichen sich verbergenden Sache. Mysterium besagt die
              Beziehung des sinnlichen Zeichens zu bezeichneten Sache, eine f&uuml;r
              den Au&szlig;enstehenden verborgene Beziehung (deshalb Mysterium),
              die aber den Glaubenden fortschreitend geoffenbart wird.<br>
              <br>
              <strong>Theologische L&ouml;sung</strong><br>
              Corpus Mysticum als urspr&uuml;nglicher Begriff zur Bezeichnung
              der mehrfachen Bedeutungen der Eucharistie ist ein generischer
              Begriff im Gegensatz zu den spezifischen Begriffen f&uuml;r die
              Eucharistie in Brot und Wein, wie beispielsweise &#8222;wahrer
              Leib&#8220; oder eucharistisches Opfer oder eucharistisches Sakrament.
              Letzteren ist ein statisches Moment zu eigen. <br>
              Der wahre Leib wird angebetet, Corpus Mysticum wird in einem kommunikativen
              Geschehen realisiert.<br>
              Corpus Mysticum meint urspr&uuml;nglich den dynamischen Zusammenhang,
              in dem durch die rituelle Feier der Eucharistie mit den mystischen
              Zeichen von Brot und Wein Christus re-pr&auml;sentiert, vergegenw&auml;rtigt
              wird auf mystische Weise. <br>
              Der Begriff selbst stellt dabei urspr&uuml;nglich die ontologische
              Dimension der Gegenwart Christi im Zeichen keineswegs in Frage,
              sondern verdeutlicht sie.<br>
              <br>
              <strong>Zitate</strong><br>
              Ich Berengar, glaube von Herzen und bekenne mit dem Mund, dass
              das Brot und der Wein, die auf dem Altar liegen, durch das Geheimnis
              des heiligen Gebets und durch die Worte unseres Erl&ouml;sers wesentlich
              gewandelt werden in das wahre, eigentliche, lebenspendende Fleisch
              und Blut unseres Herrn Jesus Christus; und nach der Weihe sind
              sie der wahre Leib Christi, der aus der Jungfrau geboren wurde,
              der, geopfert f&uuml;r das Heil der Welt, am Kreuze hin und der
              zu Rechten des Vaters sitzt, und das wahre Blut Christi, das aus
              seiner Seite flo&szlig; nicht nur im Zeichen und in der Wirksamkeit
              des Sakramentes, sondern in seiner eigentlichen Natur und in seiner
              wahren Wesenheit...R&ouml;mische Partikularsynode 1079, Berengars
              Glaubensbekenntnis</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Diese Interpretation
                der Realpr&auml;senz trug wesentlich dazu
              bei, dass der Sinnzusammenhang zwischen der Christusvereinigung
              in der Eucharistie und der Vitalit&auml;t und Einheit der Kirche
              verloren ging. Einheit und Vitalit&auml;t der Kirche werden zuk&uuml;nftig
              weniger von der Feier der Eucharistie als von der Stiftung durch
              Christus her begr&uuml;ndet. </font><br>
              <font face="Arial, Helvetica, sans-serif">Um
              aber den Sinnkn&auml;uel, den das einfache Wort corpus mysticum
              in sich birgt, und verworrener ist, als auf den ersten Blick zu
              vermuten w&auml;re, zu entwirren, sind noch zwei wichtige Eigent&uuml;mlichkeiten
              zu beachten. Zun&auml;chst bedeuten mysterium seiner urspr&uuml;nglichen
              Verwendung nach mehr eine Handlung als eine Sache; hierin liegt
              ein neuer Gesichtspunkt es von &#8222;sacramentum&#8220; zu unterscheiden.
              Dieses aktive Moment ist schon in der Verwendung Pauli sp&uuml;rbar...
              So sprechen wir heute von der Feier der heiligen Mysterien, und
              im Gegensatz dazu von der Anbetung des allerheiligsten Sakraments.
              Das Vollbringen des Mysteriums bringt das Sakramentum hervor. <br>
              Sodann aber ist das Mysterium nicht allein in seinem Gehalt, der
              bezeichnet wird, wesentlich aktiv. Es ist das noch radikaler, wenn
              auch weniger greifbar, in seiner Form... Die urspr&uuml;ngliche
              Wortbedeutung beh&auml;lt zwar etwas Verworrenes und Flie&szlig;endes.
              Sie ist synthetisch und dynamisch. Sie bezieht sich nicht so sehr
              auf das erscheinende Zeichen oder &#8230; auf die sich darin bergende
              Wirklichkeit, als vielmehr auf beides zugleich: auf ihre Beziehung,
              ihr Einsein, ihr gegenseitiges sich bedingen, auf den &Uuml;bergang
              vom einen zum anderen. ... Sie zeigt den Aufruf, der vom Zeichen
              her an das Bezeichnete ergeht, oder besser: auf die dunkle, aber
              schon im geheimen wirksame Gegenwart des Bezeichneten im Zeichen.
              Im eigentlichen Sinn &#8222;mystisch&#8220; ist somit das verborgene
              und bewegende Band der allusio, der significatio. (de Lubac, Corpus
              mysticum, 65 ff)</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Ihr inneres Band einb&uuml;&szlig;end
                h&auml;tte die Kirche ihr
              Wesen verloren. Sie w&auml;re nicht mehr ein wahres, reales Ganzes,
              ein lebendiger Leib mitsamt seinen Organen, ein sprechendes Abbild
              des Erl&ouml;sers, ebenso wahrhaft eins, wenn auch auf andere Art,
              wie sein individueller Leib... Sie erschiene mehr oder weniger
              als eine blo&szlig;e moralische K&ouml;rperschaft oder gar als
              eine einfache politische Korporation, wo die Mitglieder mit ihrem
              F&uuml;hrer in blo&szlig; &auml;u&szlig;erlicher Verbindung stehen.
              de Lubac, Die Kirche, S. 114</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Das zweite Vatikanische
                Konzil versucht diesen Zusammenhang wieder herzustellen. Dies
                geschieht aber eher synthetisch, nicht in der
              von De Lubac erhellten kommunikationspraktischen Funktionalit&auml;t:<br>
&#8222;              Die Kirche, das hei&szlig;t das im Mysterium schon gegenw&auml;rtige
              Reich Christi, w&auml;chst durch die Kraft Gottes sichtbar in der
              Welt. Dieser Anfang und diese Wachstum werden zeichenhaft angedeutet
              durch Blut und Wasser, die der ge&ouml;ffneten Seite des gekreuzigten
              Christus entstr&ouml;men und vorherverk&uuml;ndet durch die Worte
              des Herrn &uuml;ber seinem Tod am Kreuz.... Sooft das Kreuzesopfer,
              in dem Christus, unser Osterlamm, dahingegeben wurde, auf dem Altar
              gefeiert wird, vollzieht sich das Werk unserer Erl&ouml;sung. Zugleich
              wird durch das Sakrament des eucharistischen Brotes die Einheit
              der Gl&auml;ubigen, die einen Leib in Christus bilden, dargestellt
              und verwirklicht.&quot;<br>
              Lumen Gentium
            </font></p>
            <P><font face="Arial, Helvetica, sans-serif">Theo Hipp</font></P>
            <P>&copy;<a href="http://www.kath.de"> <font face="Arial, Helvetica, sans-serif">www.kath.de</font></a></P>
            </TD>
          <TD background=boxright.gif><IMG height=8 alt="" 
            src="boxright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=8 alt="" src="boxbottomleft.gif" 
            width=8></TD>
          <TD background=boxbottom.gif><IMG height=8 alt="" 
            src="boxbottom.gif" width=8></TD>
          <TD><IMG height=8 alt="" src="boxbottomright.gif" 
            width=8></TD></TR></TBODY></TABLE></TD><td style='vertical-align:top;'>
 <script type="text/javascript"><!--
        google_ad_client = "pub-9537752113242914";
        google_ad_width = 336;
        google_ad_height = 280;
        google_ad_format = "336x280_as";
        google_ad_type = "text_image";
        //2006-10-23: Lexikon Symbole
        google_ad_channel = "8495253357";
        //--></script>
        <script type="text/javascript"
          src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
        </script>
</td>
</TR>
  <TR>
    <TD vAlign=top align=left>&nbsp; </TD>
  </TR></TBODY></TABLE></BODY></HTML>
