<HTML><HEAD><TITLE>Gottesbeweis aus der Wahrheit</TITLE>
<meta name="title" content="Philosophie&amp;Theologie">
<meta name="author" content="Redaktion kath.de">
<meta name="publisher" content="kath.de">
<meta name="copyright" content="kath.de">
<meta name="description" content="">
<meta name="abstract" content="Lexikon &uuml;ber Philosophie&amp;Theologie">
<meta http-equiv="content-language" content="de">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="date" content="2006-00-01">
<meta name="robots" content="index,follow">
<meta name="revisit-after" content="10 days">
<meta name="revisit" content="after 10 days">
<meta name="DC.Title" content="Philosophie&amp;Theologie">
<meta name="DC.Creator" content="Redaktion kath.de">
<meta name="DC.Contributor" content="J�rgen Pelzer">
<meta name="DC.Rights" content="kath.de">
<meta name="DC.Publisher" content="kath.de">
<meta name="DC.Date" content="2006-00-01">
<meta name="DC.Description" content="Lexikon &uuml;ber Philosophie&amp;Theologie">
<meta name="DC.Language" content="de">
<meta name="DC.Type" content="Text">
<meta name="DC.Format" content="text/html">
<meta name="DC.Identifier" content="http://www.kath.de/lexikon/philosophie_theologie/">
<meta name="keywords" lang="de" content="Christus, Anspruch, Unbedingte, augustinus, Rahner, Horizont, Freiheit">
</HEAD>
<BODY bgColor=#ffffff leftMargin=6 topMargin=6 marginheight="6" marginwidth="6">
<TABLE cellSpacing=0 cellPadding=6 width="100%" border=0>
  <TBODY>
  <TR>
    <TD vAlign=top align=left width=100> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b>Philosophie&amp;Theologie</b></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td> <?php include("logo.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10"> <?php include("az.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </TD>
    <TD vAlign=top rowSpan=2>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
        <TBODY>
        <TR vAlign=top align=left>
          <TD width=8><IMG height=8 alt="" 
            src="boxtopleftcorner.gif" width=8></TD>
          <TD background=boxtop.gif><IMG height=8 alt="" 
            src="boxtop.gif" width=8></TD>
          <TD width=8><IMG height=8 alt="" 
            src="boxtoprightcorner.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxtopleft.gif><IMG height=8 alt="" 
            src="boxtopleft.gif" width=8></TD>
          <TD bgColor=#e2e2e2> 
            <H1><font face="Arial, Helvetica, sans-serif">Gottesbeweis aus
                der Wahrheit</font></H1>
          </TD>
          <TD background=boxtopright.gif><IMG height=8 
            alt="" src="boxtopright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=13 alt="" src="boxdividerleft.gif" 
            width=8></TD>
          <TD background=boxdivider.gif><IMG height=13 
            alt="" src="boxdivider.gif" width=8></TD>
          <TD><IMG height=13 alt="" 
            src="boxdividerright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxleft.gif><IMG height=8 alt="" 
            src="boxleft.gif" width=8></TD>
          <TD class=L12>
            <P><STRONG><font face="Arial, Helvetica, sans-serif">Das Erkennen
                  ist unter den Anspruch der Wahrheit gestellt</font></STRONG></P>
            <P><font face="Arial, Helvetica, sans-serif">Der Mensch in seiner
                Freiheit treibt nicht ohne Erkenntnis und ohne Ausrichtung auf
                ein Ziel durch sein Leben. Die Freiheit w&auml;re
              nicht wirklich frei, wenn der Mensch nicht zur Erkenntnis f&auml;hig
              w&auml;re, denn nur eine erkennende Freiheit kann sinnvoll w&auml;hlen.
              W&uuml;rde der Mensch nicht erkennen, was f&uuml;r sein Leben wichtig
              w&auml;re, k&ouml;nnte er zwar immer noch w&auml;hlen, aber seine
              Wahl w&auml;re nur zuf&auml;llig, &#8222;ohne Sinn und Verstand&#8220; und
              damit nicht frei, sondern eben abh&auml;ngig vom Zufall und nicht
              von der eigenen Einsicht. Ohne das Erkenntnisverm&ouml;gen w&auml;re
              der Freiheit &#8222;blind&#8220;, der Mensch k&ouml;nnte seine
              Freiheit nicht aus&uuml;ben. Die Erkenntnis ist auf die Wahrheit
              ausgerichtet. Das nehmen wir im Alltag als selbstverst&auml;ndlich
              an. Wenn wir mit dem Auto ein Ziel erreichen wollen, dann verlassen
              wir uns darauf, da&szlig; die Stra&szlig;enkarten und die Hinweisschilder
              der Wirklichkeit entsprechen, also &#8222;wahr&#8220; sind, und
              wir nicht anderswo ankommen, wenn wir der Karte und den Schildern
              folgen. Wenn wir Nudeln einkaufen, dann gehen wir davon aus, da&szlig; in
              der Packung, auf der &#8222;Rigatoni&#8220; steht, kein Zucker,
              sondern Nudeln aus Mehl sind. Schwieriger wird die Wahrheitsfrage
              beim Arzt. Es ist oft schwierig, f&uuml;r eine Krankheit die Ursache
              zu finden. Wir wissen aber, da&szlig; eine Heilung nur dann m&ouml;glich
              ist, wenn eine Diagnose, d.h. die Wahrheit &uuml;ber unseren gesundheitlichen
              Zustand, vorliegt.<br>
              Nun behauptet der Gottesbeweis, der von der Erkenntnisf&auml;higkeit
              ausgeht, da&szlig; in jeder als wahr erkannten Sache etwas mitschwingt,
              das auf Gott hinweist. Dieser Anspruch leitet sich davon ab, da&szlig; das
              Richtungsschild, auf dem Frankfurt steht, den Autofahrer tats&auml;chlich
              nach Frankfurt lenkt, da&szlig; eben die &Uuml;bereinstimmung der
              Aussage mit der Realit&auml;t nicht beliebig sein kann. Es w&auml;re
              dann so, da&szlig; ein Schild mit der Aufschrift &#8222;Frankfurt&#8220; mal
              in die richtige Richtung zeigt, ein andermal in eine andere. F&uuml;r
              unser praktisches Leben sind wir darauf angewiesen, da&szlig; alle
              Schilde uns jeweils zu unserem Ziel weiter leiten. Diesen Anspruch
              haben wir aber nicht nur wegen der unabsehbaren Konsequenzen, wenn
              die Aussagen auf Stra&szlig;enschildern, auf Lebensmittelverpackungen,
              Medikamenten nicht mit der Wirklichkeit &uuml;bereinstimmen. F&uuml;r
              alles was ist, haben wir in unserem Erkenntnisverm&ouml;gen die
              Tendenz, da&szlig; wir &uuml;ber die Welt insgesamt und unser Leben
              die Wahrheit wissen wollen. </font></P>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Der Zweifel als Indiz unserer unbedingten Wahrheitssuche</strong><br>
  Man k&ouml;nnte jetzt sagen, der Mensch sei zwar zur Erkenntnis
              bef&auml;higt, ob in einer T&uuml;te, auf der &#8222;Rigatoni&#8220; steht,
              auch tats&auml;chlich als R&ouml;hrchen geformte Nudeln drin sind.
              Jedoch sei er &uuml;berfordert, wenn es um die Erkenntnis Gottes
              gehe. Auch reiche sein Erkenntniskraft nicht aus, f&uuml;r sein
              eigenes Leben einen letzten Sinn zu erkennen. Auch wer das sagt,
              gibt damit seinem Willen Ausdruck, die Wahrheit zu erkennen. Denn
              wer zweifelt, will nicht Beliebigkeit, sondern er zweifelt um der
              Wahrheit willen. Denn zweifeln kann nur jemand, der eine Vorstellung
              von der Wahrheit hat. Er stellt eine Aussage in Frage, weil er
              nicht sicher ist, ob sie wahr ist. Der Zweifel zeigt an sich selbst
              den Anspruch der Wahrheit. Der Erkennende kann nur dem zustimmen,
              was er als Wahrheit erkannt hat. Das zeigt: Die Wahrheit ist dem
              Menschen als unausweichliche Aufgabe gestellt. Gerade im Zweifel
              meldet sich der Anspruch der Wahrheit.</font></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Die Wahrheit ist mehr als eine Idee</strong><br>
  Der Anspruch der Wahrheit kommt von au&szlig;en auf den Menschen
              zu. Es ist nicht so, da&szlig; der Mensch sich eine Idee von der
              Wahrheit macht, um dieser zu folgen, so wie ein Maler die Idee
              eines Bildes hat und diese dann auf der Leinwand umsetzt. Die Wahrheit
              liegt vielmehr dem erkennenden Menschen voraus. Er richtet sein
              Erkenntnisverm&ouml;gen an der Wahrheit aus, so da&szlig; die Wahrheit
              eine Gr&ouml;&szlig;e ist, die den Menschen &uuml;bersteigt. Dieser
              Vorrang der Wahrheit vor dem menschlichen Erkennen ist nicht beliebig,
              sondern gilt ohne &#8222;Wenn und Aber&#8220;. Der Mensch kann
              der Wahrheit keine Bedingungen stellen, vielmehr stellt die Wahrheit
              dem Menschen eine Bedingung, n&auml;mlich nur das f&uuml;r wahr
              zu halten, was auch &#8222;ist&#8220;. Der Anspruch der Wahrheit
              ist unbedingt. </font></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Der Anspruch der Wahrheit und die Freiheit</strong><br>
  Wenn die Wahrheit mit ihrem unbedingten Anspruch nicht aus dem
                Menschen abzuleiten ist, sondern der Mensch diesem Anspruch unterstellt
                ist, ob er will oder nicht, dann verf&uuml;gt die menschliche
                Freiheit nicht &uuml;ber die Wahrheit. Gegen&uuml;ber der Wahrheit
                ist der Mensch nicht frei. Vielmehr erm&ouml;glicht die Wahrheit
                die Freiheit, denn w&uuml;rde der Mensch die Wahrheit nicht erkennen,
                k&ouml;nnte er seine Freiheit nicht nutzen. Das Thema der Freiheit
                ist also nicht die Wahrheit, sondern was der Mensch mit seiner
                Lebenszeit anf&auml;ngt, f&uuml;r was er sich entscheidet. Auch
                hier ist der Mensch auf etwas ausgerichtet, n&auml;mlich auf
                das Gute. Nur wenn er das Gute anstrebt, kann seine Freiheit
                gelingen. Deshalb f&auml;llt die Freiheit auf den Menschen zur&uuml;ck,
                der sie nutzt, um sich oder andere zu schaden. Wer anderen schadet,
                macht sich damit unfrei.<br>
                Wenn der Mensch die Wahrheit nicht hervorbringen kann, sondern
              alles, was er erkennt, am Anspruch der Wahrheit messen mu&szlig;,
              von welcher Instanz kommt dann dieser Anspruch der Wahrheit? Die
              Frage ist auch deshalb unausweichlich, weil der Mensch in seiner
              Irrtumsf&auml;higkeit und der Versuchbarkeit, aus b&ouml;ser Absicht
              die Wahrheit zu verdrehen, eine Instanz braucht, die die Wahrheit
              garantiert. Ohne diese Instanz h&auml;tte er keinen Leuchtturm,
              an dem er sich bei allen Irrt&uuml;mern und Mi&szlig;griffen neu
              orientieren k&ouml;nnte.</font></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Der Anspruch, den die Wahrheit stellt, leitet sich von einem Unbedingten
              her</strong><br>
              Wenn der Satz gilt &#8222;Die Wahrheit und nichts als die Wahrheit&#8220;,
              dann kann nichts in dieser Welt diesen Anspruch stellen. Denn in
              unserer Welt geht es erst einmal ums &Uuml;berleben und ein jeweils
              besseres Leben. Die Wahrheit kann dazu helfen, aber manchmal ist
              es auch vorteilhafter, nicht die Wahrheit zu sagen. Unsere Welt
              ist zu sehr vom Zuf&auml;lligen gepr&auml;gt, als da&szlig; sie
              einen unbedingten Anspruch an unsere Erkenntniskraft stellen k&ouml;nnte.
              Die Wahrheit, die uns unbedingt einfordert, die uns jedes &#8222;Wenn
              und Aber&#8220; aus der Hand nimmt, ist keine weltliche Instanz.
              Sie kann diesen Anspruch auch nur stellen, wenn sie in sich voll
              wahr ist. Die Instanz mu&szlig; also in sich wahr sein, sie mu&szlig; sich
              selbst erkennen und alles, was dem menschlichen Erkenntnisverm&ouml;gen
              irgendwann einmal zug&auml;nglich sein k&ouml;nnte. Die Wahrheit,
              die uns so unbedingt einfordert, mu&szlig; selbst &uuml;ber die
              ganze Wahrheit verf&uuml;gen. Auch mu&szlig; die Wahrheit, die
              unsere Freiheit einfordert, selbst frei sein. W&auml;re sie nicht
              selbst eine frei erkennende Wahrheit, w&uuml;rde sie von einer
              anderen Wahrheit abh&auml;ngen. Sie kann n&auml;mlich nicht wie
              ein Milchstra&szlig;ensystem nach physikalischen Gesetzen funktionieren,
              sondern sie mu&szlig; diese Gesetze erkennen k&ouml;nnen. Denn
              nur ein geistiges Wesen erkennt die Gesetze, nach denen eine Milchstra&szlig;e
              funktioniert, die Milchstra&szlig;e erkennt diese nicht. Freiheit
              geh&ouml;rt deshalb notwendig zur Wahrheit, denn nur ein Wesen,
              das in seinem Geist vergleichen kann, ist in der Lage festzustellen,
              ob etwas wahr ist oder nicht.<br>
              Ein wahrheitsf&auml;higes Wesen wie der Mensch mu&szlig; dar&uuml;ber
              hinaus &uuml;ber sich selbst eine durchdringende Erkenntnis haben.
              Denn das Erkennen kann nicht bestimmte Bereiche der Wirklichkeit
              ausschlie&szlig;en. Da die Bef&auml;higung zur Wahrheit verlangt,
              da&szlig; das erkennende Wesen nicht einfach funktioniert, sondern
              um die Wahrheit wei&szlig;, ist es der Reflexion, des Gespr&auml;ches
              mit sich selbst, f&auml;hig. Aber in der Erkenntnis seiner selbst
              ist der Mensch begrenzt. Deshalb mu&szlig; die letzte Wahrheit,
              anders als der menschliche Geist, sich selbst ganz durchdringen,
              denn sonst w&uuml;rde ihr etwas verborgen bleiben und es g&auml;be
              eine h&ouml;here Instanz, die dieses Wesen bis ins Letzte kennt.
              Daher mu&szlig; das h&ouml;chste, der Wahrheit f&auml;hige Wesen,
              sich auch selbst ganz erkannt haben.</font></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Die Dynamik auf die ganze Wahrheit hin</strong><br>
  Bisher wurde der Anspruch der Wahrheit in den Mittelpunkt gestellt.
                Sie stellt einen unbedingten Anspruch, ohne &#8222;Wenn und Aber&#8220; zur
                Wahrheit zu stehen. Dieser Anspruch kann nicht aus dem zuf&auml;lligen
                und vielen Einfl&uuml;ssen unterliegenden Wesen Mensch kommen.
                Im Menschen findet sich allerdings mehr als das Betroffensein
                durch die Wahrheit. Der Mensch hat in sich eine Dynamik, m&ouml;glichst
                umgreifend alles zu erkennen. Das ist der Grund, warum der Mensch
                im Unterschied zu den Menschenaffen Wissenschaft treibt. Er ist
                eben nicht mit dem Wissen zufrieden, was er zu f&uuml;r die Bew&auml;ltigung
                des Alltags braucht. Er will m&ouml;glichst alles &uuml;ber die
                Natur wissen und auch Erkenntnisse &uuml;ber Gott gewinnen. Diese
                Dynamik auf das Ganze der Wahrheit beinhaltet, da&szlig; der
                Mensch das Ganze der Wirklichkeit bejaht. Denn wer etwas erkennen
                will, mu&szlig; vorher akzeptieren, da&szlig; es das zu Erkennende
                auch tats&auml;chlich gibt bzw. da&szlig; es m&ouml;glich ist,
                mit dem Erkenntnisverm&ouml;gen auf das zu sto&szlig;en, was
                man erkennen will. Karl Rahner bezeichnet diese Dynamik auf das
                Ganze des Erkennbaren als einen Vorgriff. Vorgriff deshalb, weil
                der Mensch Vieles noch nicht erkannt hat, es aber zu erkennen
                sucht. Rahner vergleicht das konkrete menschliche Erkennen in
                Bezug auf das Ganze mit dem Horizont, der unserem Sehverm&ouml;gen
                den Rahmen gibt. Der Horizont spannt den Blick weit aus, innerhalb
                des Horizontes kann sich dann, wenn wir am Meer stehen, ein Schiff
                zeigen. Der geistige Horizont des menschlichen Erkenntnisverm&ouml;gens
                ist grunds&auml;tzlich f&uuml;r alles offen. Auch wenn der Mensch
                vieles noch nicht erkannt hat, in seinem Horizont kann es auftauchen,
                um dann wahrgenommen zu werden. Zugleich beschreibt das Bild
                des Horizontes die Unabgeschlossenheit des Erkennens. In unserem
                geistigen Horizont kann immer Neues auftauchen, wir werden nie
                alles erfassen. Der Horizont selbst bleibt dem Menschen immer
                vorgegeben. Er kann den Horizont nicht so erfassen wie die Gegenst&auml;nde,
                die im Horizont auftauchen. Mit dem Bild des Horizontes ist dann
                auch unsere M&ouml;glichkeit, Gott zu erkennen, beschrieben.
                Er ist unserem Denken immer voraus, wir sind in unserem Denken
                auf die absolute Wahrheit bezogen, erfassen sie aber nie so wie
                einen Gegenstand, der sich im Horizont unseres Denkens zeigt.<br>
&Auml;hnlich wie es eine Dynamik der ganzen Wahrheit gibt, strebt der
              Mensch das Gute nicht nur teilweise an, sondern will es ganz und
              bejaht damit im einzelnen Streben das Gute insgesamt. Diese Tendenz
              auf die umfassende Wahrheit, die nichts ausschlie&szlig;t, n&auml;mlich
              auf das Gute, das ganz gut ist, kann als <a href="gottesbeweis_hoechstes_gut.php">Bejahung
              Gottes</a> gesehen
              werden.</font></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Zitate</strong><br>
  Augustinus in &#8222;De libero arbitrio&#8220;, &#8222;Vom Freien
              Willen&#8220;, der als Dialog mit einem Mann namens Evodius gefa&szlig;t
              ist.<br>
&#8222;
              Du wirst deshalb keinesfalls leugnen, da&szlig; es eine unwandelbare
              Wahrheit gibt, die all das in sich schlie&szlig;t, was unwandelbar
              wahr ist, die weder dein noch mein noch irgendeines Menschen Eigentum
              hei&szlig;en kann, sondern allen, die das unwandelbar Wahre erblicken,
              es als wundersam geheimes und doch jedermann gegenw&auml;rtiges
              und damit zug&auml;ngliches Licht kundtut.&#8220; Nr. 130</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Augustinus &#8222;&Uuml;ber
                die wahren Religion&#8220;<br>
&#8222;
              Geh nicht nach drau&szlig;en, kehre wieder ein bei dir selbst!
              Im inneren Menschen wohnt die Wahrheit. Und wenn du deine Natur
              noch unwandelbar findest, so schreite &uuml;ber sie selbst hinaus!
              Doch bedenke, da&szlig;, wenn du &uuml;ber dich hinaus schreitest,
              du &uuml;ber deine verst&auml;ndige Seele hinaus schreitest. Dorthin
              also trachte, von wo der Lichtstrahl kommt, der deine Vernunft
              erleuchtet. Denn wohin sonst gelangt, wer seine Vernunft recht
              gebraucht, wenn nicht zur Wahrheit? Die Wahrheit kommt ja nicht
              durch Vernunftgebrauch zu sich selbst, sondern sie ist das, wonach
              alle, die ihre Vernunft gebrauchen, trachten. So ist hier die denkbar
              h&ouml;chste &Uuml;bereinstimmung, und nun stimme auch du mit ihr &uuml;berein.
              Bekenne, da&szlig; du nicht bist, was sie ist. Denn sie selbst
              sucht sich nicht. Du aber bist suchend zu ihr gelangt, nicht einen
              Raum durchmessend, sondern von der Sehnsucht des Geistes getrieben.&#8220;<br>
&#8222;
              Jeder der einsieht, da&szlig; er zweifelt, sieht etwas Wahres ein
              und ist dessen, was er einsieht, auch gewi&szlig;. Also ist er
              eines Wahren gewi&szlig;. Jeder, der also daran zweifelt, ob es
              eine Wahrheit gibt, hat in sich selbst etwas Wahres, woran er nicht
              zweifelt. Da nun alles Wahre nur durch die Wahrheit wahr ist, kann
              niemand an der Wahrheit zweifeln, der &uuml;berhaupt zweifeln kann.
              Wo man dies sieht, gl&auml;nzt jenes Licht, das nicht von Raum-
              und Zeitgr&ouml;&szlig;en, auch nicht von r&auml;umlich oder zeitlich
              gedachten Phantasiebildern wei&szlig;.&#8220; Kap 39, 202 und 206</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Ren&egrave; Descartes hat in seiner III. Meditation (s. <a href="ontologischer_gottesbeweis.php">ontologischer
              Gottesbeweis</a>) aufgezeigt, da&szlig; der Mensch das Endliche nicht
              einfach erkennt, sondern es nur als endlich erfa&szlig;t, weil
              er es auf dem Hintergrund des Unendlichen erkennt:<br>
&#8222;
              Auch darf ich nicht glauben, ich ergriffe das Unendliche nicht
              in einer wahrhaften Vorstellung, sondern nur durch Verneinung des
              Endlichen, so wie ich Ruhe und Dunkelheit durch Verneinung von
              Bewegung und Licht begreife. Denn ganz im Gegenteil sehe ich offenbar
              ein, da&szlig; mehr Sachgehalt in der unendlichen Substanz als
              in der endlichen enthalten ist und da&szlig; demnach der Begriff
              des Unendlichen dem des Endlichen, d.i. der Gottes in meiner selbst
              gewisserma&szlig;en vorhergeht. Wie sollte ich sonst begreifen
              k&ouml;nnen, da&szlig; ich zweifle, da&szlig; ich etwas w&uuml;nsche,
              d.i. da&szlig; mir etwas mangelt und ich nicht ganz vollkommen
              bin, wenn gar keine Vorstellung von einem vollkommeneren Wesen
              in mir w&auml;re, womit ich mich vergleiche und so meine M&auml;ngel
              erkenne?&#8220; Meditationen III, 24</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Karl Rahner in H&ouml;rer
                des Wortes, 1941<br>
&#8222;
              Dadurch also, da&szlig; das Bewu&szlig;tsein seinen einzelnen Gegenstand in
              einem Vorgriff ... auf die absolute Weite seiner m&ouml;glichen Gegenst&auml;nde
              erfa&szlig;t, greift es in jeder Einzelerkenntnis immer schon &uuml;ber den
              Einzelgegenstand hinaus und erfa&szlig;t ihn damit gerade nict blo&szlig;
              in seiner beziehungslosen dumpfen Diesheit, sondern in seiner
              Gegrenztheit und Bezogenheit auf die Ganzheit aller m&ouml;glichen Gegenst&auml;nde,
              weil es, indem es beim Einzelnen ist und um beim Einzelnen wissend
              sein zu k&ouml;nnen, immer auch schon &uuml;ber das Einzelne als solches
              hinaus ist. ..., Was wir mit dem Vorgriff meinen, ... ist ein
              apriori mit dem menschlichen Wesen gegebenes Verm&ouml;gen der dynamischen
              Hinbewegung
              des Geistes auf die absolute Weite aller m&ouml;glichen Gegenst&auml;nde...
              Durch den Vorgriff wird der einzelne Gegenstand gleichsam schon
              immer unter dem Horizont des absoluten Erkenntnisideals erkannt,
              er ist deshalb auch schon immer hingestellt in den bewu&szlig;ten Raum
              alles Erkennbaren.... Der Vorgriff ist die bewu&szlig;tmachende
              Er&ouml;ffnung
              des Horizontes, innerhalb dessen das einzelne Objekt der menschlichen
              Erkenntnis
              gewu&szlig;t wird.&#8220; S. 77<br>
              Es ist ein Vorgriff auf das an sich unbegrenzte Sein ... Mit der
              Notwendigkeit, mit der dieser Vorgriff gesetzt wird, ist auch das
              unendliche Sein Gottes mitbejaht. Zwar stellt der Vorgriff nicht
              unmittelbar Gott als Gegenstand dem Geist vor, weil der Vorgriff
              als Bedingung der M&ouml;glichkeit der gegenst&auml;ndlichen Erkenntnis
              von sich &uuml;berhaupt keinen Gegenstand in seinem Sein vorstellt.
              Aber in diesem Vorgriff als notwendiger und immer schon vollzogener
              Bedingung jeder menschlichen Erkenntnis und jedes menschlichen
              Handelns ist doch auch schon die Existenz eines absoluten Seins,
              also Gottes, mitbejaht.&#8220; S. 81</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Eckhard Bieger</font><br>&copy;
              <a href="http://www.kath.de"> <font face="Arial, Helvetica, sans-serif">www.kath.de</font></a></p>
            </TD>
          <TD background=boxright.gif><IMG height=8 alt="" 
            src="boxright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=8 alt="" src="boxbottomleft.gif" 
            width=8></TD>
          <TD background=boxbottom.gif><IMG height=8 alt="" 
            src="boxbottom.gif" width=8></TD>
          <TD><IMG height=8 alt="" src="boxbottomright.gif" 
            width=8></TD></TR></TBODY></TABLE></TD><td style='vertical-align:top;'>
 <script type="text/javascript"><!--
        google_ad_client = "pub-9537752113242914";
        google_ad_width = 336;
        google_ad_height = 280;
        google_ad_format = "336x280_as";
        google_ad_type = "text_image";
        //2006-10-23: Lexikon Symbole
        google_ad_channel = "8495253357";
        //--></script>
        <script type="text/javascript"
          src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
        </script>
</td>
</TR>
  <TR>
    <TD vAlign=top align=left>&nbsp; </TD>
  </TR></TBODY></TABLE></BODY></HTML>
