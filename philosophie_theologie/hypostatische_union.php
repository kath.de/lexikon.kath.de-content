<HTML><HEAD><TITLE>Hypostatische Union</TITLE>
<meta name="title" content="Philosophie&amp;Theologie">
<meta name="author" content="Redaktion kath.de">
<meta name="publisher" content="kath.de">
<meta name="copyright" content="kath.de">
<meta name="description" content="">
<meta name="abstract" content="Lexikon &uuml;ber Philosophie&amp;Theologie">
<meta http-equiv="content-language" content="de">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="date" content="2006-00-01">
<meta name="robots" content="index,follow">
<meta name="revisit-after" content="10 days">
<meta name="revisit" content="after 10 days">
<meta name="DC.Title" content="Philosophie&amp;Theologie">
<meta name="DC.Creator" content="Redaktion kath.de">
<meta name="DC.Contributor" content="J�rgen Pelzer">
<meta name="DC.Rights" content="kath.de">
<meta name="DC.Publisher" content="kath.de">
<meta name="DC.Date" content="2006-00-01">
<meta name="DC.Description" content="Lexikon &uuml;ber Philosophie&amp;Theologie">
<meta name="DC.Language" content="de">
<meta name="DC.Type" content="Text">
<meta name="DC.Format" content="text/html">
<meta name="DC.Identifier" content="http://www.kath.de/lexikon/philosophie_theologie/">
<meta name="keywords" lang="de" content="Hypostase, Hypostatishce Union, Enhypostase, Hypostasis. Physis, Prosopon, Zweinaturen, Gottesmutter, g�ttliche Natur, Einigung der Naturen">
</HEAD>
<BODY bgColor=#ffffff leftMargin=6 topMargin=6 marginheight="6" marginwidth="6">
<TABLE cellSpacing=0 cellPadding=6 width="100%" border=0>
  <TBODY>
  <TR>
    <TD vAlign=top align=left width=100> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b>Philosophie&amp;Theologie</b></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td> <?php include("logo.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10"> <?php include("az.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </TD>
    <TD vAlign=top rowSpan=2>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
        <TBODY>
        <TR vAlign=top align=left>
          <TD width=8><IMG height=8 alt="" 
            src="boxtopleftcorner.gif" width=8></TD>
          <TD background=boxtop.gif><IMG height=8 alt="" 
            src="boxtop.gif" width=8></TD>
          <TD width=8><IMG height=8 alt="" 
            src="boxtoprightcorner.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxtopleft.gif><IMG height=8 alt="" 
            src="boxtopleft.gif" width=8></TD>
          <TD bgColor=#e2e2e2> 
            <H1><font face="Arial, Helvetica, sans-serif">Hypostatische
                Union</font></H1>
          </TD>
          <TD background=boxtopright.gif><IMG height=8 
            alt="" src="boxtopright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=13 alt="" src="boxdividerleft.gif" 
            width=8></TD>
          <TD background=boxdivider.gif><IMG height=13 
            alt="" src="boxdivider.gif" width=8></TD>
          <TD><IMG height=13 alt="" 
            src="boxdividerright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxleft.gif><IMG height=8 alt="" 
            src="boxleft.gif" width=8></TD>
          <TD class=L12>
            <P><font face="Arial, Helvetica, sans-serif"><STRONG>Unio
                  hypostatica, Einigung nach der Substanz</STRONG></font></P>
            <P><font face="Arial, Helvetica, sans-serif">Als die christlichen
                Prediger in die griechisch denkende Welt wanderten, predigten
                sie zuerst in den j&uuml;dischen Synagogen.
              Sie waren ja Juden und genossen dort Gastrecht. Wie eine christliche
              Gemeinde heute freute sich auch die j&uuml;dische Gemeinde, von
              einem fremden Prediger etwas Neues zu h&ouml;ren. Diese verk&uuml;ndeten,
              da&szlig; der Messias gekommen sei. Obwohl er von der j&uuml;dischen
              Obrigkeit abgelehnt worden und den R&ouml;mern zur Hinrichtung &uuml;bergeben
              worden sei, habe Gott sich zu ihm bekannt. Er sei auferweckt und
              in den Himmel zur Rechten Gottes erhoben. Am Ende der Zeit werde
              er als Weltenrichter wiederkommen. Das war mehr als die Auslegung
              der B&uuml;cher des Moses und der Propheten. In der Apostelgeschichte
              ist beschrieben, wie sich ein Teil der Juden den christlichen Predigern
              anschlossen, die meisten aber den Neuen Weg ablehnten. &Uuml;ber
              diese Predigtmethode erreichten die christlichen Boten auch Nicht-Juden,
              die sich dem j&uuml;dischen Glauben angeschlossen hatten wie auch
              solche griechisch sprechenden Bewohner des r&ouml;mischen Reiches,
              die noch keinen Zugang zum einen Gott gefunden hatten. <br>
              <strong><br>
              D</strong></font><font face="Arial, Helvetica, sans-serif"><strong>ie christliche
                  Botschaft in der griechischen Welt</strong>
              </font><br>
              <font face="Arial, Helvetica, sans-serif">Die sich
                f&uuml;r die Botschaft von dem einen Gott ansprechen lie&szlig;en,
                hatten sich meist von der Vielg&ouml;tterei abgewandt und glaubten,
                im Gefolge der griechischen Philosophie, da&szlig; es nur einen
                Gott geben kann. Platon, der zwischen 427 und 347 v. Chr. lebte,
                war auf gedanklichem Weg zu der Erkenntnis gekommen, da&szlig; die
                griechischen Mythen wenig &uuml;ber das Wesen Gottes aussagen,
                da&szlig; vor allem nicht mehrere Gottheiten nebeneinander existieren
                konnten. Gott konnte nur einer sein. Das G&ouml;ttliche mu&szlig; aber
                auch verschieden von dem Nicht-G&ouml;ttlichen gedacht werden,
                sonst w&auml;re die Gottheit ein Teil der Welt. Die Christen brachten
                vom j&uuml;dischen Denken diesen Sch&ouml;pfungsglauben mit, da&szlig; n&auml;mlich
                die Materie einen Anfang hat und alles, was der Mensch beobachten
                kann, die Sterne, das Meer, Pflanzen und Tiere und sogar er selbst
                von Gott geschaffen worden sind. F&uuml;r das griechische Denken
                hatte Aristoteles die Vorstellung entwickelt, da&szlig; alle Bewegung
                von Gott ausgehe und den Begriff vom &#8222;unbewegten Beweger&#8220; gepr&auml;gt.
                Es zeigt sich also im Denken schon, da&szlig; es nicht das G&ouml;ttliche
                als ein unbeschreibbares Etwas gibt, sondern da&szlig; dieses G&ouml;ttliche
                in einem Subjekt existiert. Das ist nicht so selbstverst&auml;ndlich,
                Gott als ein Subjekt zu denken, der handelt, einen Willen hat,
                sich in Beziehung zu seiner Sch&ouml;pfung setzt, den Menschen
                pers&ouml;nlich anspricht. Viele Menschen auch damals sehen im
                G&ouml;ttlichen eine Macht, eine Kraft, aber nicht unbedingt ein
                pers&ouml;nliches Gegen&uuml;ber des Menschen, wie die Juden den
                sich offenbarenden Gott erfahren hatten. <br>
                <br>
                <strong>Der Begriff Hypostasis</strong><br>
                Mit dem griechischen Wort &#8222;Hypostasis&#8220;,
                wird das bezeichnet, was darunter liegt, was das Ganze tr&auml;gt.
                Wenn Jesus wirklich Gott ist, dann ist Gott dem Menschen nicht
                als blo&szlig; unfa&szlig;bares Wesen entgegen getreten, sondern
                mit einem Gesicht. Das Wort <a href="person.php">Person</a> leitet sich im Griechischen
                von Antlitz, &#8222;Prosopon&#8220; ab. Aber war dieser Mensch,
                den die Christen als Messias verk&uuml;ndeten, wirklich Gott? Dann
                w&uuml;rde es ja wieder mehr als einen Gott geben. <br>
              </font></P>
            <P><font face="Arial, Helvetica, sans-serif"><strong>Ewiger Sohn Gottes und
                wahrer Mensch<br>
            </strong></font><font face="Arial, Helvetica, sans-serif">D
              er strikte Monotheismus
                der griechischen Philosophie wurde f&uuml;r
              die christliche Theologie ein Problem. Als die christliche Kirche
              unter Kaiser Konstantin die &auml;u&szlig;ere Freiheit erhielt,
              brach das Problem auf. Im Konzil von Nic&auml;a (325) ging es um
              die Frage, ob Jesus nur Mensch, oder ob er wirklich Gott war. In
              der Treue zu den Aussagen der christlichen Bibel hielt das Konzil
              gegen die Vorgaben der griechischen Philosophie daran fest, da&szlig; Jesus
              von Nazareth wirklich <a href="gottessohn.php">Gottes Sohn</a> ist.
              Es stellte fest, da&szlig; er
              nicht geschaffen, sondern der aus dem Vater gezeugte Sohn und damit
              wirklich Gott ist. Gibt es also in Gott doch mehrere Subjekte?
              Um zu einer weiteren begrifflichen Kl&auml;rung zu kommen, hat
              das I.  Konzil von Konstantinopel  im Jahr 381 die Gottheit,
              die allen g&ouml;ttlichen Personen gleicherma&szlig;en zugeschireben
              wird, mit Ousia (Wesen, Sein) benannt und die Unterschiedenheit
              der drei Personen mit Hypostase. Eine Ousia in drei Hypostasen
              war also die Begrifflichkeit, die man ein halbes Jahrhundert nach
              dem Konzil von Nic&auml;a gew&auml;hlt hat. Wenn Jesus aber Gott
              ist, dann ergibt sich sofort das Problem, wie in Jesus Christus
              Gottheit/Gott-Sein
              und Menschheit/Mensch-Sein unterschieden werden k&ouml;nnen. War
              die Menschheit Jesu nur &#8222;Schein&#8220; oder war nur der K&ouml;rper
              Jesu menschlich und die g&ouml;ttliche Hypostase, n&auml;mlich
              die zweite g&ouml;ttliche Person,  an die Stelle der menschlichen
              Seele getreten? Alle Varianten wurden durchgespielt. s. <a href="christologische_streitigkeiten.php">christologische
              Streitigkeiten</a>. <br>
              Konnte der Begriff Hypostase bei der Kl&auml;rung
              weiterhelfen? Da man die Erl&ouml;sung in Frage stellte, wenn nur
              der K&ouml;rper des Jesus von Nazareth wirklich menschlich war,
              mu&szlig;te man zu der &Uuml;berzeugung kommen, da&szlig; Jesus
              eine menschliche Seele mit einem eigenen Willen hat. Denn wenn
              die Seele nicht in Jesus erl&ouml;st wird, dann hat keine wirkliche
              Erl&ouml;sung stattgefunden.<br>
              Wenn Jesus wirklicher Mensch mit Leib und Seele war, dann war er
              ein menschliches Subjekt und damit eine menschliche Hypostase.
              Dabei wird im 4. Jahrhundert der Begriff Hypostase noch sehr allgemein
              gebraucht, er konnte z.B. auch materiellen Besitz bedeuten; erst
              durch die christologischen Auseinandersetzungen ist er dann im
              6. Jh. verfeinert worden. Unser heutiger Subjektbegriff hat
              sich in dieser Ziet erst entwickeln k&ouml;nnen. <br>
              Theodoret, Bischof von Cyrus in den Jahren 423-466, schreibt, &#8222;da&szlig; die
              Hypostasis des Gott-Logos vor den Weltzeiten vollkommen da war
              und da&szlig; von ihr eine vollkommene Knechtsgestalt angenommen
              wurde. ... Wenn nur jede Natur ihre Vollkommenheit besitzt, aber
              beide in eins zusammengekommen sind, ... dann ist es fromm, eine
              Person und ebenso einen Sohn und Christus zu bekennen, und es ist
              nicht unvern&uuml;nftig, sondern ganz folgerichtig, von zwei vereinigten
              Hypostasen oder Naturen zu sprechen.&#8220; <br>
              Wenn aber in Jesus Christus zwei Hypostasen unterschieden werden,
              dann mu&szlig; man auch von zwei S&ouml;hnen sprechen. Dagegen
              wendet sich die Schule von Alexandrien, die heute noch in der koptischen
              Kirche weiterlebt. Sie sehen das gro&szlig;e Erl&ouml;sungsgeschehen
              auseinandergerissen, wenn zwischen dem Sohn Gottes und dem Menschen
              Jesus nur eine lose Verbindung bestehen sollte. Deshalb schreibt
              der ma&szlig;gebliche Vertreter dieser Theologenschule, Cyrill
              von Alexandrien, im Jahr 431 in der Verteidigung seiner 12 Kapitel
              gegen Theodoret von Cyrus, zu Kap.2:<br>
&#8222;
              Es erfolgt die Einigung nach der Hypostasis, wobei der Ausdruck &#8222;nach
              der Hypostasis&#8220; nichts anderes bedeutet als nur dies, da&szlig; die
              Natur oder Hypostasis des Logos, d.h. der Logos selbst, mit seiner
              menschlichen Natur wahrhaft geeint wird ohne jede Ver&auml;nderung
              und Vermischung und &#8230; als Ein-Christus gedacht wird und es
              auch ist, derselbe Gott und Mensch.&#8220;<br>
              Hypostatische Union bedeutet dann, da&szlig; die Einheit von Gottheit
              und Menschheit in Jesus durch die Hypostase zustande kommt.<br>
              Cyrill spricht sogar von einer neuen Natur, die durch die Menschwerdung
              entsteht (<a href="monophysiten.php">s. Monophysitismus</a>), weil
              die alexandrinische Theologie die Einheit herausstellen will. Die
              Schule von Antiochien, vertreten
              durch Nestorius und Theodoret, betont, da&szlig; Gottheit und Menschheit
              in Jesus Christus nicht vermischt sind (was auch die Alexandriner
              nicht leugnen wollen) und daher unterschieden bleiben m&uuml;ssen.
              Das erkl&auml;rt warum Theodoret &#8222;Hypostase&#8220; mehr im
              Sinne von Natur gebraucht. Seine Schule sieht in Christus zwei
              Naturen,
              die unterschieden bleiben. Hypostase wird von Cyrill dagegen mehr
              im Sinne von Person gebraucht. Das ist problematisch: f&uuml;r
              Theodoret wie f&uuml;r Cyrill ist eine Natur ohne Hypostasis nicht
              vorstellbar. Weil Cyrill die Einheit der Person Christi so wichtig
              ist und er die Vermischung nicht f&uuml;rchtet, bleibt er bei Hypostasis
              oder Physis, die Antiochener dagegen vermeiden den Begriff Hypostasis,
              um die einheit in Jesus christus zu bezeichnen
              und verwenden statt dessen den unbelasteten Begriff <a href="person.php">Prosopon</a>.
              Erst im 6. Jh. kommt es im Gefolge des Konzils von Chalcedon zur &Uuml;berlegung,
              dass die menschliche Natur Christi wohl eine Hypostase habe, aber
              eben vom ersten Moment der Empf&auml;ngnis an die der zweiten g&ouml;ttlichen
              Person, und so ist sie enhypostatisch, d.h. hat ihre Hypostase
              in einer anderen. Diese Begriffsbildung von der Enhypostasie hat
              dann die strikt alexandrinische Richtung und bis heute die koptische
              und armenische Kirche nicht mitvollzogen.<br>
              Die theologische Diskussion zwischen der alexandrinischen und antiochenischen
              Schule zeigt, da&szlig; der Begriff Hypostase nicht deutlich genug
              das einigende Prinzip in Jesus Christus bezeichnen kann. Der Begriff
              leistet jedoch in dieser Zeit noch etwas, n&auml;mlich da&szlig; er
              die subjekthafte Wirklichkeit bezeichnet, das, was tr&auml;gt,
              w&auml;hrend der griechische Begriff Antlitz (Prosopon) die Einheit
              noch nicht deutlich genug als wirkliche, in sich selbst existierende
              deutlich machen kann. In der Diskussion bildet sich aber der Begriff
              Person immer mehr heraus, so da&szlig; er wie f&uuml;r uns heute
              die Wirklichkeit des Subjektes bezeichnen kann. So schreibt Theodoret: <br>
              &quot;Als wahrhaftigen Gott und wahrhaftigen Menschen bekennen wir uns
              Herrn Jesus Christus, und wir teilen den Einen nicht in zwei Prosopa
              (Personen), sondern glauben, da&szlig; zwei Naturen unvermischt
              geeint sind.&#8220; </font></P>            <p><font face="Arial, Helvetica, sans-serif">In der sog. Unionsformel
                zwischen beiden Schulen aus dem Jahr 433 wird nicht mehr von
                Hypostasen gesprochen, sondern diese l&auml;uft
              auf den Begriff Prosopon zu und zeigt, wie sich in den Auseinandersetzungen
              um das Verst&auml;ndnis der Menschwerdung sich der Personbegriff
              langsam herausgebildet hat.<br>
&#8222;
              Wir bekennen, da&szlig; unser Herr Jesus Christus, der einziggeborene
              Sohn Gottes, vollkommener Gott und vollkommener Mensch aus vern&uuml;nftiger
              Seele und Leib, vor den Weltzeiten aus dem Vater nach der Gottheit,
              aber am Ende der Tage als derselbe um unseretwillen und um unserer
              Errettung willen aus Maria der Jungfrau nach der Menschheit geboren
              wurde, dem Vater wesensgleich nach der Gottheit und derselbe uns
              wesensgleich nach der Menschheit. Denn es ist eine Einigung der
              Naturen erfolgt. Einen Christus, Einen Sohn, Einen Herrn bekennen
              wir daher.<br>
              Gem&auml;&szlig; dieser Vorstellung von der unvermischten Einigung
              bekennen wir die heilige Jungfrau als Gottesmutter, weil der Gott-Logos
              Fleisch und Mensch geworden ist und unmittelbar von der Empf&auml;ngnis
              an den aus ihr genommenen Tempel (d.i. sein Leib) mit sich vereinigt
              hat.<br>
              Wir wissen aber, da&szlig; die von Gott lehrenden M&auml;nner die
              evangelischen und apostolischen Worte &uuml;ber unseren Herrn teils
              gemeinsam auf eine Person (Prosopon) beziehen, teils gleichsam
              auf zwei Naturen verteilen und die gottgem&auml;&szlig;en Worte
              der Gottheit Christi, die niedrigen aber entsprechend seiner Menschheit
              erkl&auml;ren.&#8220;<br>
              Mit den hier &#8222;niedrigen Worten&#8220; sind u.a. die im Garten
              Gethsemane gemeint, als Jesus den Vater bittet, &#8222;da&szlig; der
              Kelch an ihm vor&uuml;bergehe.&#8220;<br>
              <br>
              <strong>Zur Begriffsentwicklung</strong><br>
              Der Begriff &#8222;Hypostatische Union&#8220; geht auf Cyrill von
            Alexandrien zur&uuml;ck. Der Theologe Maximus Confessor ging 200
            Jahre nach den oben beschriebenen theologischen Diskussionen der
            Frage nach, ob Jesus einen menschlichen Willen hatte. (s. <a href="monotheletismus_monergetismus.php">Monotheletismus</a>)            <br>
&#8222;
            Von gleicher Hypostase (Subjektsein) ist, was mit einem anderen zu
            einer und derselben Hypostase zusammengef&uuml;gt ist, wie es sich
            verh&auml;lt bei Seele und Leib und allem anderen, was bei unterschiedlicher
            und andersartiger Natur gem&auml;&szlig; der Hypostase geeint ist.
            Wenn also etwas mit einem anderen in Einheit verbunden ist, dann
            existiert es als dasselbe gem&auml;&szlig; der Hypostase &#8230; es
            ist also die hypostatische Union, die die unterschiedlichen Wesenheiten
            oder Naturen zu einer Person und ein und derselben Hypostase (Subjektsein)
            zusammenf&uuml;hrt und bindet. &#8230;.<br>
            Wenn wir von einer Einigung gem&auml;&szlig; der Hypostase sprechen,
            dann erkennen und bekennen wir, da&szlig; die Einigung der Naturen
            zu einer Hypostase erfolgt ist. &#8230;<br>
            Durch seine Gottheit wurde er nicht gehindert, Mensch zu werden,
            noch wurde er durch seine Menschwerdung im Gottsein gemindert, der
            eine und selbe besteht ganz in beiden Naturen &#8230;<br>
            Opuscula theologica, Ambiguorum liber II um 640 bzw. um 630<br>
            </font></p>
              <p><font size="2" face="Arial, Helvetica, sans-serif">Eckhard Bieger</font></p>              <P>&nbsp;</P>
            <P><font face="Arial, Helvetica, sans-serif">&copy;<a href="http://www.kath.de"> www.kath.de</a></font></P>
            </TD>
          <TD background=boxright.gif><IMG height=8 alt="" 
            src="boxright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=8 alt="" src="boxbottomleft.gif" 
            width=8></TD>
          <TD background=boxbottom.gif><IMG height=8 alt="" 
            src="boxbottom.gif" width=8></TD>
          <TD><IMG height=8 alt="" src="boxbottomright.gif" 
            width=8></TD></TR></TBODY></TABLE></TD><td style='vertical-align:top;'>
 <script type="text/javascript"><!--
        google_ad_client = "pub-9537752113242914";
        google_ad_width = 336;
        google_ad_height = 280;
        google_ad_format = "336x280_as";
        google_ad_type = "text_image";
        //2006-10-23: Lexikon Symbole
        google_ad_channel = "8495253357";
        //--></script>
        <script type="text/javascript"
          src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
        </script>
</td>
</TR>
  <TR>
    <TD vAlign=top align=left>&nbsp; </TD>
  </TR></TBODY></TABLE></BODY></HTML>
