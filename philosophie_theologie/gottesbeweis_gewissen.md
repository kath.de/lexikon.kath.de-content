---
title: Gottesbeweis aus dem Gewissen
author: 
tags: 
created_at: 
images: []
---


# **Die Stimme, die im Inneren des Menschen spricht*
***Im Gewissen werden wir von innen angesprochen. Es spricht allerdings nicht unsere Stimme zu uns, sondern die der sittlichen Verpflichtung. Das Gewissen stellt an uns Forderungen, in bestimmter Weise zu handeln. Diese Forderungen beziehen sich nicht darauf, daß wir unseren Vorteil optimal ausnutzen, sondern widerstreiten gerade dem, was wir als vorteilhaft oder angenehm erstreben. Im Gewissen meldet sich eine Norm, die uns auf andere hin und auf deren Vorteil orientiert. Das Gewissen fordert uns unbedingt, also „Ohne Wenn und Aber“. Mit dem Gewissen ist auch erst die Voraussetzung gegeben, daß wir uns schuldig fühlen können. Denn eigentlich müßten wir mit uns zufrieden sein, wenn wir z.B. jemanden erfolgreich betrogen haben. Wir waren ja erfolgreich. Wenn wir aber trotz des äußeren Erfolges nicht zufrieden sind, weil wir uns im Unrecht fühlen, dann beurteilen wir unsere Handlung nicht nur unter dem Gesichtspunkt des Erfolges, sondern auch danach, ob sie ethisch gerechtfertigt ist. Dieses Wissen um ethische Werte konkretisiert sich im Gewissen auf die Beurteilung einzelner Handlungen. Wir kennen nicht nur die sittlichen Vorschriften, wie sie z.B. in den 10 Geboten zusammengestellt sind, sondern „wissen“ auch um den ethischen Wert bzw. Unwert einer Tat. Das Gewissen hat eine urteilende Funktion, es beurteilt unsere Überlegungen, wenn wir etwas vorhaben und fällt auch ein Urteil über die durchgeführte Handlung. Da uns der Spruch des Gewissens wie von außen fordert und uns auch keine Wahl läßt, weil er uns unbedingt verpflichtet, sehen nicht wenige Philosophen im Gewissen die am deutlichsten erkennbare Gegenwart Gottes, die dem Menschen zugänglich ist.  

# **Johann Gottlieb Fichte**
„ Die Stimme des Gewissens, die jedem seine besondere Pflicht auflegt, ist der Strahl, an welchem wir aus dem Unendlichen ausgehen, und als einzelne und besondere Wesen hingestellt werden; sie zieht die Grenzen unserer Persönlichkeit; sie also ist unserer wahrer Urbestandteil, der Grund und Stoff alles Lebens, welches wir leben. Die absolute Freiheit des Willens, die wir gleichfalls aus dem Unendlichen mit herab nehmen in die Welt der Zeit, ist als Prinzip dieses unseres Lebens.“ ***

 Die Bestimmung des Menschen, Berlin 1800, S. 139f 

***„Wir sind in seiner Hand, und bleiben in derselben, und niemand kann uns daraus reißen. Wir sind ewig, weil Er es ist. Erhabener lebendiger Wille, den kein Name nennt und kein Begriff umfaßt, wohl darf ich mein Gemüt zu dir erheben: denn du und ich sind nicht getrennt. Deine Stimme ertönt in mir, die meinige tönt in dir wieder; und alle meine Gedanken, wenn sie nur wahr und gut sind, sind in dir gedacht. – In dir, dem Unbegreiflichen, werde ich mir selbst, und wird mir die Welt vollkommen begreiflich, alle Rätsel meines Daseins werden gelöst, und die vollendete Harmonie entsteht in meinem Geiste.“ ebd. S. 143f; zitiert nach Philosophische Bibliothek, Hamburg 2000 

# **Friedrich Wilhelm Schelling**
„ Wir haben in uns einen einzigen offenen Punkt, durch den der Himmel hereinscheint. Dieser ist unser Herz oder, richtiger zu reden, unser Gewissen. Wir finden in diesem ein Gesetz und eine Bestimmung, die nicht von dieser Welt sein kann, mit der sie vielmehr gewöhnlich im Kampf ist, und so dient es uns zu dem Unterpfand einer höheren Welt, und erhebt den, der ihm folgen gelernt hat, zu dem trostreichen Gedanken der Unsterblichkeit.“***


Ü ber den Zusammenhang der Natur mit der Geisterwelt, 1810, Sämtliche Werke Bd. 1,S. 17, zitiert nach Philosophische Bibliothek, Hamburg 1992 

# **  John Henry Newman hat in seien Schrift „An Essay in Aid of a Grammar of Assent“ aus dem Jahre 1870 am deutlichsten die Stimme des Gewissens als Hinweis auf Gott herausgearbeitet; deutsch: „Entwurf einer Zustimmungslehre“, Mainz 1961**
„ Das Gewissen aber ruht nicht in sich selbst, sondern langt in vager Weise vor zu etwas jenseits seiner selbst und erkennt undeutlich eine Billigung seiner Entscheidungen, die höher ist als es selbst und bewiesen ist in jenem scharfen Sinn für Verpflichtung und Verantwortung, der sie trägt. Daher kommt es, daß wir gewohnt sind, vom Gewissen zu sprechen als von einer Stimme – ein Ausdruck, den auf den Sinn für das Schöne anzuwenden, uns niemals einfallen würde. Und überdies ist es eine Stimme oder das Echo einer Stimme, herrisch und nötigend wie kein anderer Befehl im ganzen Bereich unserer Erfahrung.“ S.75***


„ Wenn wir, wie es ja der Fall ist, uns verantwortlich fühlen, beschämt sind, erschreckt sind bei einer Verfehlung gegen die Stimme des Gewissens, so schließt das ein, daß hier Einer ist, dem wir verantwortlich sind; vor dem wir beschämt sind, dessen Ansprüche an uns wir fürchten....***

 Wenn die Ursachen dieser Gemütsbewegungen nicht dieser sichtbaren Welt angehören, so muß der Gegensand, auf den seine Wahrnehmung gerichtet ist, übernatürlich und göttlich sein. So ist das Phänomen des Gewissens als das eines Befehls dazu geeignet, dem Geist das Bild eines höchsten Herrschers einzuprägen, eines Richters, heilig, gerecht, mächtig, allsehend, vergeltend. Es ist das schöpferische Prinzip der Religion, wie der Sinn für das Sittliche das Prinzip der Ethik ist.“ S. 77 

# **Die Einwände der Tiefenpsychologe****
 So unbestreitbar das Gewissen im einzelnen Menschen spricht, so wenig deutlich kann es sein, daß im Gewissen sich die Stimme Gottes meldet. Siegmund Freud ist bei der Erforschung des Unbewußten darauf gestoßen, daß Menschen in sich ständig die Stimme ihrer Eltern oder ihrer Umwelt hören. Freud ordnet das Gewissen daher nicht dem Personkern des Menschen zu, sondern seinem Über-Ich. Das Über-Ich ist eine Instanz im Menschen, aber sie ist von außen verinnerlicht. Der Mensch hört die Stimmen, die ihm von außen Vorschriften machen, ihn in eine bestimmte Richtung drängen wollen, in sich sprechen. Da diese Stimmen Schuldgefühle einflößen, sind die Anhänger von Freud sofort skeptisch, wenn es um Schuldgefühle geht. Diese leiten sie erst einmal vom Über-Ich her, also von einer Instanz, die der Mensch zu seinem Schaden verinnerlicht hat. ***

 Daß Gewissen aber nicht einfach die stimme der Eltern ist, zeigen die Menschen, die sich gegen die Meinungstrends ihrer Umwelt ein eigenes Urteil erhalten haben und sich z.B. in der Zeit des Nationalsozialismus der Rassenideologie widersetzt haben. Nicht wenige haben ihren Widerstand mit dem Leben bezahlt. ***

 Es zeigt sich, daß eine Stimme, die der einzelne in sich hört, nicht unbesehen mit der Stimme des Gewissens identifiziert werden kann. Das ist zudem eine alte Erfahrung der christlichen Spiritualität, daß der Mensch verschiedene Stimmen in seinem Inneren hört und manche Stimmen Einflüsterungen des bösen Geistes sind. Deshalb gehört es zum Grundbestand einer christlichen Spiritualität, die Unterscheidung der Geister einzuüben. ***

 Daß neben der Stimme des Gewissens sich auch andere im Menschen melden, zeigt weiter die Notwendigkeit, eine äußere Norm zu finden, an der der einzelne überprüfen kann, ob eine Eingebung vom guten oder bösen Geist kommt. Die sittlichen Werte, die in den 10 Geboten oder in den Menschenrechten zusammengestellt sind, dienen als ein solcher Maßstab. Der Vorteil besteht darin, daß diese Normen nicht allein intuitiv erfaßt und begründet werden können, sondern der Argumentation zugänglich sind. Diese Argumentation leistet die philosophische Ethik.***

 Wenn auf der einen Seite die Stimme Gottes im Gewissen zu vernehmen ist, auf der anderen Seite aber der Mensch auch andere Stimmen hört, zeigt sich der Gottesbeweis, der aus dem Gewissen erschlossen wird, als ein gangbarer Weg, der jedoch der Ergänzung bedarf. Diese findet sich in der Analyse der Freiheit. Da das Gewissen nur auf Grund der Freiheit überhaupt möglich ist, ist ein enger Zusammenhang von Gewissen und ***[Freiheit](gottesbeweis_freiheit.php) gegeben, wenn der Mensch sich seiner Herkunft von Gott vergewissern will. 

# **Eckhard Bieger**
# *
© ***[***www.kath.de](http://www.kath.de) 
