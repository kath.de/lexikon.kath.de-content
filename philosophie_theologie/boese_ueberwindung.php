<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <title>Das B&ouml;se - &Uuml;berwindung</title>


  <meta name="title" content="Philosophie&amp;Theologie">

  <meta name="author" content="Redaktion kath.de">

  <meta name="publisher" content="kath.de">

  <meta name="copyright" content="kath.de">

  <meta name="description" content="">

  <meta name="abstract" content="Lexikon &uuml;ber Philosophie&amp;Theologie">

  <meta http-equiv="content-language" content="de">

  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

  <meta name="date" content="2006-00-01">

  <meta name="robots" content="index,follow">

  <meta name="revisit-after" content="10 days">

  <meta name="revisit" content="after 10 days">

  <meta name="DC.Title" content="Philosophie&amp;Theologie">

  <meta name="DC.Creator" content="Redaktion kath.de">

  <meta name="DC.Contributor" content="J&uuml;rgen Pelzer">

  <meta name="DC.Rights" content="kath.de">

  <meta name="DC.Publisher" content="kath.de">

  <meta name="DC.Date" content="2006-00-01">

  <meta name="DC.Description" content="Lexikon &uuml;ber Philosophie&amp;Theologie">

  <meta name="DC.Language" content="de">

  <meta name="DC.Type" content="Text">

  <meta name="DC.Format" content="text/html">

  <meta name="DC.Identifier" content="http://www.kath.de/lexikon/philosophie_theologie/">

  <meta name="keywords" content="Anspruch, Gott, Entscheidung, Freiheit, Trotzalter, Sarte" lang="de">

</head>
<body leftmargin="6" topmargin="6" style="background-color: rgb(255, 255, 255);" marginheight="6" marginwidth="6">

<table border="0" cellpadding="6" cellspacing="0" width="100%">

  <tbody>

    <tr>

      <td align="left" valign="top" width="100">
      <table border="0" cellpadding="0" cellspacing="0" width="216">

        <tbody>

          <tr align="left" valign="top">

            <td width="8"><img src="boxtopleftcorner.gif" alt="" height="8" width="8"></td>

            <td background="boxtop.gif" width="200"><img src="boxtop.gif" alt="" height="8" width="8"></td>

            <td width="8"><img src="boxtoprightcorner.gif" alt="" height="8" width="8"></td>

          </tr>

          <tr align="left" valign="top">

            <td background="boxtopleft.gif"><img src="boxtopleft.gif" alt="" height="8" width="8"></td>

            <td bgcolor="#e2e2e2"><b>Philosophie&amp;Theologie</b></td>

            <td background="boxtopright.gif"><img src="boxtopright.gif" alt="" height="8" width="8"></td>

          </tr>

          <tr align="left" valign="top">

            <td><img src="boxdividerleft.gif" alt="" height="13" width="8"></td>

            <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" height="13" width="8"></td>

            <td><img src="boxdividerright.gif" alt="" height="13" width="8"></td>

          </tr>

          <tr align="left" valign="top">

            <td background="boxleft.gif"><img src="boxleft.gif" alt="" height="8" width="8"></td>

            <td><?php include("logo.html"); ?> </td>

            <td background="boxright.gif"><img src="boxright.gif" alt="" height="8" width="8"></td>

          </tr>

          <tr align="left" valign="top">

            <td><img src="boxbottomleft.gif" alt="" height="8" width="8"></td>

            <td background="boxbottom.gif"><img src="boxbottom.gif" alt="" height="8" width="8"></td>

            <td><img src="boxbottomright.gif" alt="" height="8" width="8"></td>

          </tr>

        </tbody>
      </table>

      <br>

      <table border="0" cellpadding="0" cellspacing="0" width="216">

        <tbody>

          <tr align="left" valign="top">

            <td width="8"><img src="boxtopleftcorner.gif" alt="" height="8" width="8"></td>

            <td background="boxtop.gif" width="200"><img src="boxtop.gif" alt="" height="8" width="8"></td>

            <td width="8"><img src="boxtoprightcorner.gif" alt="" height="8" width="8"></td>

          </tr>

          <tr align="left" valign="top">

            <td background="boxtopleft.gif"><img src="boxtopleft.gif" alt="" height="8" width="8"></td>

            <td bgcolor="#e2e2e2"><strong>Begriff
anklicken</strong></td>

            <td background="boxtopright.gif"><img src="boxtopright.gif" alt="" height="8" width="8"></td>

          </tr>

          <tr align="left" valign="top">

            <td><img src="boxdividerleft.gif" alt="" height="13" width="8"></td>

            <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" height="13" width="8"></td>

            <td><img src="boxdividerright.gif" alt="" height="13" width="8"></td>

          </tr>

          <tr align="left" valign="top">

            <td background="boxleft.gif"><img src="boxleft.gif" alt="" height="8" width="8"></td>

            <td class="V10"><?php include("az.html"); ?> </td>

            <td background="boxright.gif"><img src="boxright.gif" alt="" height="8" width="8"></td>

          </tr>

          <tr align="left" valign="top">

            <td><img src="boxbottomleft.gif" alt="" height="8" width="8"></td>

            <td background="boxbottom.gif"><img src="boxbottom.gif" alt="" height="8" width="8"></td>

            <td><img src="boxbottomright.gif" alt="" height="8" width="8"></td>

          </tr>

        </tbody>
      </table>

      </td>

      <td rowspan="2" valign="top">
      <table border="0" cellpadding="0" cellspacing="0" width="100%">

        <tbody>

          <tr align="left" valign="top">

            <td width="8"><img alt="" src="boxtopleftcorner.gif" height="8" width="8"></td>

            <td background="boxtop.gif"><img alt="" src="boxtop.gif" height="8" width="8"></td>

            <td width="8"><img alt="" src="boxtoprightcorner.gif" height="8" width="8"></td>

          </tr>

          <tr align="left" valign="top">

            <td background="boxtopleft.gif"><img alt="" src="boxtopleft.gif" height="8" width="8"></td>

            <td bgcolor="#e2e2e2">
            <h1><font face="Arial, Helvetica, sans-serif">
Das B&ouml;se - &Uuml;berwindung</font></h1>

            </td>

            <td background="boxtopright.gif"><img alt="" src="boxtopright.gif" height="8" width="8"></td>

          </tr>

          <tr align="left" valign="top">

            <td><img alt="" src="boxdividerleft.gif" height="13" width="8"></td>

            <td background="boxdivider.gif"><img alt="" src="boxdivider.gif" height="13" width="8"></td>

            <td><img alt="" src="boxdividerright.gif" height="13" width="8"></td>

          </tr>

          <tr align="left" valign="top">

            <td background="boxleft.gif"><img alt="" src="boxleft.gif" height="8" width="8"></td>

            <td class="L12"><br>

            <p style="font-family: Helvetica,Arial,sans-serif;" class="MsoNormal">Wenn das <a href="boese_begehren.php"><u>Begehren</u></a>&nbsp;<span style="color: red;"></span>und die
Entt&auml;uschung, das Begehrte nicht zu erhalten,
den Boden f&uuml;r das Verbrechen bereiten, weil es im Kampf um
das, was der andere
hat, zur Gewaltanwendung kommt, dann muss das Begehren
zur&uuml;ckgedr&auml;ngt werden.
Genau das finden wir in der Geschichte vom Bruderm&ouml;rder Kain:</p>

            <p style="font-family: Helvetica,Arial,sans-serif;" class="MsoNormal"><cite>&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;
&bdquo;<span style="font-size: 12pt;">Der Herr sprach
zu Kain: Warum &uuml;berl&auml;uft es dich
hei&szlig; und warum senkt sich dein Blick? </span></cite><a name="a7"><span style="font-size: 12pt;"><cite>Nicht
wahr, wenn du recht tust, darfst du
aufblicken; wenn du nicht recht tust, lauert an der T&uuml;r die
&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp; S&uuml;nde als D&auml;mon. Auf
dich hat er es abgesehen, doch du werde Herr &uuml;ber
ihn!&ldquo;</cite> </span></a><span style="font-size: 12pt;"><o:p></o:p></span></p>

            <p style="font-family: Helvetica,Arial,sans-serif;" class="MsoNormal"><o:p></o:p>Das Rechte tun,
bewahrt vor dem Begehren. Daraus erw&auml;chst
die Rechtsordnung. Die Anerkennung dieser Ordnung ist nicht allein
darin
begr&uuml;ndet, dass die einzelnen Vorschriften einsichtig gemacht
werden. Das ist
zwar notwendig und w&uuml;rde auch korrekte Menschen bereits
&uuml;berzeugen, es sch&uuml;tzt
aber die Gesellschaft zu wenig vor Gewalt. Die Z&auml;hmung des
Begehrens muss
tiefer verankert werden. Die Menschen m&uuml;ssen verstehen, dass
ohne Rechtsordnung
die Existenz der Gemeinschaft bedroht ist. Wir sto&szlig;en hier in
eine tiefere Schicht
vor. Kain geht es um seinen Platz in der Welt, ob dieser durch eine
himmlische
Macht, wir sagen heute meist Gl&uuml;ck dazu, oder durch die
Familie, die Gruppe
gew&auml;hrleistet wird. Ihm scheint der Boden unter den
F&uuml;&szlig;en zu schwanken. In
solchen Situationen nicht gewaltt&auml;tig zu werden, sondern der
Ordnung zu
vertrauen, das entscheidet &uuml;ber den Fortbestand der
Gemeinschaft. Aus diesem
Grund brauchen Mafiaclans eine strikte Ordnung, gerade weil sie sich
nicht aus
den Prinzipien der Verfassung ableiten und daher nicht durch das
Rechtssystem
der &uuml;brigen Gesellschaft gesch&uuml;tzt werden.</p>

            <p style="font-family: Helvetica,Arial,sans-serif;" class="MsoNormal">Dass es um die B&auml;ndigung des
Begehrens geht, kann man
&uuml;brigens bei allen spirituellen Schulen nachlesen. Der
Buddhismus hat f&uuml;r den
Weg zur Vollkommenheit, d.h. zum Ablegen jedes Begehrens, 8 Stufen. Es
gibt die
Entwicklungslehre der &auml;gyptischen M&ouml;nche, die dann
wesentlich die
abendl&auml;ndische Kultur mit geformt hat, bis hin zu der
Entwicklung der
Moralit&auml;t, f&uuml;r die Kohlberg 6 Stufen gefunden hat. In
der j&uuml;dischen Gebotstafel
hei&szlig;t es zum Abschluss der 10 Gebote:<br>

            <a name="a17"><br>

            <span style="">&nbsp; </span><span style="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            </span></a><cite><span style=""><span style="font-size: 12pt;">&bdquo;Du sollst nicht nach dem
Haus deines N&auml;chsten
verlangen. Du sollst nicht nach der<span style="">&nbsp;</span><span style=""></span><span style=""></span>Frau
deines N&auml;chsten verlangen, nach seinem Sklaven oder seiner
Sklavin, seinem<span style="">&nbsp;&nbsp; </span><br>

            <span style="">&nbsp;</span><span style="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            </span>Rind
oder seinem Esel oder nach irgendetwas, das deinem N&auml;chsten
geh&ouml;rt</span>.</span>&ldquo;
            </cite><br>

            <span style="">&nbsp; </span><span style="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            </span>Exodus
20,17</p>

            <p style="font-family: Helvetica,Arial,sans-serif;" class="MsoNormal"><o:p></o:p><b style="">Vers&ouml;hnung<o:p></o:p></b><br>

Wenn das Begehren und die Entt&auml;uschung, das Begehrte nicht
zu erhalten, den Boden f&uuml;r das Verbrechen bereiten, weil es im
Kampf um das,
was der andere hat, zur Gewaltanwendung kommt, dann muss das Begehren
zur&uuml;ckgedr&auml;ngt werden. Genau das finden wir in der
Geschichte vom Bruderm&ouml;rder
Kain:</p>

            <p style="font-family: Helvetica,Arial,sans-serif;" class="MsoNormal"><cite>&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;
&bdquo;</cite><span style="font-size: 12pt;"><cite>Der
Herr sprach zu Kain: Warum &uuml;berl&auml;uft es dich
hei&szlig; und warum senkt sich dein Blick? <o:p></o:p>Nicht
wahr, wenn du recht tust, darfst du
aufblicken; wenn du nicht recht tust, lauert&nbsp;<br>

&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp; an der T&uuml;r die S&uuml;nde
als D&auml;mon. Auf
dich hat er es abgesehen, doch du werde Herr &uuml;ber
ihn!&ldquo;</cite> <o:p></o:p></span></p>

            <p style="font-family: Helvetica,Arial,sans-serif;" class="MsoNormal"><o:p>&nbsp;</o:p>Das
Rechte tun, bewahrt vor dem Begehren. Daraus erw&auml;chst
die Rechtsordnung. Die Anerkennung dieser Ordnung ist nicht allein
darin
begr&uuml;ndet, dass die einzelnen Vorschriften einsichtig gemacht
werden. Das ist
zwar notwendig und w&uuml;rde f&uuml;r einen korrekten Menschen
wie Immanuel Kant
vielleicht reichen, es sch&uuml;tzt aber die Gesellschaft zu wenig
vor Gewalt. Die
Z&auml;hmung des Begehrens muss tiefer verankert werden,
n&auml;mlich dass die Menschen
verstehen, dass ohne Rechtsordnung die Existenz der Gemeinschaft
bedroht ist. Wir
sto&szlig;en hier in eine tiefere Schicht vor. Kain geht es um
seinen Platz in der
Welt, ob dieser durch eine himmlische Macht, wir sagen heute meist
Gl&uuml;ck dazu,
oder durch die Familie, die Gruppe gew&auml;hrleistet wird. Ihm
scheint der Boden
unter den F&uuml;&szlig;en zu schwanken. In solchen Situationen
nicht gewaltt&auml;tig zu
werden, sondern der Ordnung zu vertrauen, das entscheidet &uuml;ber
den Fortbestand
der Gemeinschaft. Aus diesem Grund brauchen Mafiaclans eine strikte
Ordnung,
gerade weil diese sich nicht aus den Prinzipien der Verfassung ableiten
und
daher nicht durch das Rechtssystem der &uuml;brigen Gesellschaft
gesch&uuml;tzt werden.</p>

            <p style="font-family: Helvetica,Arial,sans-serif;" class="MsoNormal">Dass es um die B&auml;ndigung des
Begehrens geht, kann man
&uuml;brigens bei allen spirituellen Schulen nachlesen. Der
Buddhismus hat f&uuml;r den
Weg zur Vollkommenheit, d.h. zum Ablegen jedes Begehrens, 8 Stufen. Es
gibt die
Entwicklungslehre der &auml;gyptischen M&ouml;nche, die dann
wesentlich die
abendl&auml;ndische Kultur mit geformt hat, bis hin zu der
Entwicklung der
Moralit&auml;t, f&uuml;r die Kohlberg 6 Stufen gefunden hat. In
der j&uuml;dischen Gebotstafel
hei&szlig;t es zum Abschluss der 10 Gebote:<br>

            <br>

            <span style="">&nbsp; </span><span style="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<cite>
            </cite></span><cite><span style="font-size: 12pt;">&bdquo;Du sollst nicht nach dem
Haus deines N&auml;chsten verlangen. Du sollst
nicht nach der<span style="">&nbsp;</span><span style=""></span><span style=""></span>Frau
deines N&auml;chsten verlangen, nach seinem Sklaven oder seiner
Sklavin, seinem<span style="">&nbsp;&nbsp; </span><br>

            <span style="">&nbsp;</span><span style="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            </span>Rind
oder seinem Esel oder nach irgendetwas, das deinem N&auml;chsten
geh&ouml;rt</span>.&ldquo; </cite><br>

            <span style="">&nbsp; </span><span style="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            </span>Exodus
20,17</p>

            <p style="font-family: Helvetica,Arial,sans-serif;" class="MsoNormal"><o:p></o:p><b style="">Vers&ouml;hnung
und
Integration<o:p></o:p></b><br>

Das B&ouml;se, wenn es sich in Mobbing und Gewalt verdichtet,
bedroht die Gesellschaft, es muss herausgeschafft werden. Wenn es sich
auf
einen Tr&auml;ger, einen S&uuml;ndenbock verdichtet, kann die
Sippe, die Schulklasse, die
Abteilung, das Unternehmen und manchmal auch das Land
&uuml;berleben. Aber kann man
von einer Rechtsordnung abschlie&szlig;end sprechen, wenn diese nur
durch immer neue
Aussto&szlig;ung der T&auml;ter aufrecht erhalten werden kann?
Wenn man sich auf den
ethischen Standard einl&auml;sst, den das Christentum
eingef&uuml;hrt hat, n&auml;mlich dass
es Vergebung gibt, dann kann der Krimi eigentlich nicht damit enden,
dass der
Verbrecher dem Richter vorgef&uuml;hrt wird. Denn es bleibt die
Frage: Wie kommt er
in die Gesellschaft zur&uuml;ck? Es braucht eigentlich ein
Vers&ouml;hnungsritual. Das
funktioniert im Christentum dann, wenn jeder sich als S&uuml;nder
versteht,
zumindest dass auch seine S&uuml;nden durch den Tod Jesu aus der
Welt getragen
werden mussten. Im ersten Jahrtausend hatte die Kirche einen Ritus
ausgebildet.
Es gab f&uuml;r schwere Vergehen die M&ouml;glichkeit, sich am
Beginn der Fastenzeit in
den B&uuml;&szlig;erstand versetzen zu lassen. Am
Gr&uuml;ndonnerstag wurde man wieder in die
Gemeinschaft aufgenommen. Die Wallfahrt nach Santiago de Compostella
konnte
einen Mord s&uuml;hnen.</p>

            <p style="font-family: Helvetica,Arial,sans-serif;" class="MsoNormal">Wenn die Gesellschaft Rituale der
Vers&ouml;hnung entwickelt oder
aus fr&uuml;heren Best&auml;nden reaktiviert, kann sie den
Verbrecher in die Gesellschaft
zur&uuml;ckholen. Denn erst dann w&auml;re die Geschichte der
b&ouml;sen Tat abgeschlossen und
das B&ouml;se im Verbrecher &uuml;berwunden. Das geht aber nur,
wenn die Gesellschaft
vers&ouml;hnlich ist. Weil das B&ouml;se im
&Uuml;belt&auml;ter&uuml;berwunden werden muss, besteht das
ungute Gef&uuml;hl, dass das &bdquo;Absitzen&ldquo; einer
Strafe das Problem nicht l&ouml;st. Auch
wenn man die Todesstrafe vollstreckt, hat man das B&ouml;se nicht
wirklich
&uuml;berwundne, sondern nur den &Uuml;belt&auml;ter
beseitigt. Das zeigt sich nicht zuletzt
an der geringen Abschreckungswirkung der Todesstrafe. Nur eine Kultur
der
Vers&ouml;hnung, wie sie z.B. nach dem Zweiten Weltkrieg zwischen
Frankreich und
Deutschland aufgebaut wurde, kann letztlich das B&ouml;se
&uuml;berwinden. Gleiches gilt
f&uuml;r Schulklassen, unternehmen, und auch Staaten. Das
Aufflammen von
B&uuml;rgerkriegen nach dem Ende des Kalten Krieges zeigt, dass die
Unterdr&uuml;ckung
von Rivalit&auml;t und Aggression durch die Hegemonie Russlands das
Konfliktpotential nur eingemauert, nicht &uuml;berwunden hat.<span style="">&nbsp; </span>Eine Religion, die sich
auf die Feier des Bestehenden
zur&uuml;ckzieht, so wie es die christlichen Kirchen in Deutschland
praktizieren,
ist dann auch selbst unf&auml;hig, mit eigenen
Mi&szlig;st&auml;nden so umzugehen, dass nicht nur
T&auml;ter, wie bei sexuellem Missbrauch, ihrer Strafe
zugef&uuml;hrt werden, sondern
dass das &uuml;bel selbst &uuml;berwunden wird. </p>

            <p style="font-family: Helvetica,Arial,sans-serif;" class="MsoNormal"><o:p>&nbsp;</o:p></p>

            <p style="font-family: Helvetica,Arial,sans-serif;" class="MsoNormal">Eckhard Bieger S.J.</p>

            <br>

            <br>

            <p>&copy;<a href="http://www.kath.de">
            <font face="Arial, Helvetica, sans-serif">www.kath.de</font></a></p>

            </td>

            <td background="boxright.gif"><img alt="" src="boxright.gif" height="8" width="8"></td>

          </tr>

          <tr align="left" valign="top">

            <td><img alt="" src="boxbottomleft.gif" height="8" width="8"></td>

            <td background="boxbottom.gif"><img alt="" src="boxbottom.gif" height="8" width="8"></td>

            <td><img alt="" src="boxbottomright.gif" height="8" width="8"></td>

          </tr>

        </tbody>
      </table>

      </td>

      <td style="vertical-align: top;">
      <script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
google_ad_width = 336;
google_ad_height = 280;
google_ad_format = "336x280_as";
google_ad_type = "text_image";
//2006-10-23: Lexikon Symbole
google_ad_channel = "8495253357";
//--></script>
      <script type="text/javascript" src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
      </script></td>

    </tr>

    <tr>

      <td align="left" valign="top">&nbsp; </td>

    </tr>

  </tbody>
</table>

</body>
</html>
