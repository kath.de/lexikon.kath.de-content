<HTML><HEAD><TITLE>Gottesbeweise</TITLE>
<meta name="title" content="Philosophie&amp;Theologie">
<meta name="author" content="Redaktion kath.de">
<meta name="publisher" content="kath.de">
<meta name="copyright" content="kath.de">
<meta name="description" content="">
<meta name="abstract" content="Lexikon &uuml;ber Philosophie&amp;Theologie">
<meta http-equiv="content-language" content="de">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="date" content="2006-00-01">
<meta name="robots" content="index,follow">
<meta name="revisit-after" content="10 days">
<meta name="revisit" content="after 10 days">
<meta name="DC.Title" content="Philosophie&amp;Theologie">
<meta name="DC.Creator" content="Redaktion kath.de">
<meta name="DC.Contributor" content="J�rgen Pelzer">
<meta name="DC.Rights" content="kath.de">
<meta name="DC.Publisher" content="kath.de">
<meta name="DC.Date" content="2006-00-01">
<meta name="DC.Description" content="Lexikon &uuml;ber Philosophie&amp;Theologie">
<meta name="DC.Language" content="de">
<meta name="DC.Type" content="Text">
<meta name="DC.Format" content="text/html">
<meta name="DC.Identifier" content="http://www.kath.de/lexikon/philosophie_theologie/">
<meta name="keywords" lang="de" content="Christus, Konzil, Monotheismus, Freiheit">
</HEAD>
<BODY bgColor=#ffffff leftMargin=6 topMargin=6 marginheight="6" marginwidth="6">
<TABLE cellSpacing=0 cellPadding=6 width="100%" border=0>
  <TBODY>
  <TR>
    <TD vAlign=top align=left width=100>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b>Philosophie&amp;Theologie</b></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td> <?php include("logo.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10"> <?php include("az.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </TD>
    <TD vAlign=top rowSpan=2>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
        <TBODY>
        <TR vAlign=top align=left>
          <TD width=8><IMG height=8 alt=""
            src="boxtopleftcorner.gif" width=8></TD>
          <TD background=boxtop.gif><IMG height=8 alt=""
            src="boxtop.gif" width=8></TD>
          <TD width=8><IMG height=8 alt=""
            src="boxtoprightcorner.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxtopleft.gif><IMG height=8 alt=""
            src="boxtopleft.gif" width=8></TD>
          <TD bgColor=#e2e2e2>
            <H1><font face="Arial, Helvetica, sans-serif">Gottesbeweise</font></H1>
          </TD>
          <TD background=boxtopright.gif><IMG height=8
            alt="" src="boxtopright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=13 alt="" src="boxdividerleft.gif"
            width=8></TD>
          <TD background=boxdivider.gif><IMG height=13
            alt="" src="boxdivider.gif" width=8></TD>
          <TD><IMG height=13 alt=""
            src="boxdividerright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxleft.gif><IMG height=8 alt=""
            src="boxleft.gif" width=8></TD>
          <TD class=L12>
            <P><STRONG><font face="Arial, Helvetica, sans-serif">Sicherheit dar&uuml;ber,
                ob Gott wirklich existiert</font></STRONG></P>
            <P><font face="Arial, Helvetica, sans-serif">Wer behauptet, die Zugspitze
                existiere nicht, wird nicht ernst genommen. Wer behauptet, der
                Mensch sei gar nicht frei &#8211; wie
              das Hirnforscher behaupten, kann mit einer Diskussion rechnen.
              Wer sagt, Gott existiere nicht, dem wird erst einmal Recht gegeben.
              Denn anderes als f&uuml;r die Existenz der Zugspitze, des Vogelgrippe-Virus,
              eines Milliarden Lichtjahren entfernten Sonnensystems gibt es kein
              vergleichbares Beweisverfahren, da&szlig; Gott existiert. Das kann
              es auch nicht geben. Da Gott kein Teil der Welt ist, kann man ihn
              keinem Experiment unterwerfen. Die Menschen wollen aber Sicherheit,
              denn es w&auml;re f&uuml;r einen gl&auml;ubigen Menschen nicht
              hinnehmbar, wenn die Existenz Gottes im Vagen bliebe. In der Philosophiegeschichte
              hat es viele Versuche gegeben, dem Menschen durch gedankliches
              Schlu&szlig;folgern Sicherheit &uuml;ber die Existenz Gottes zu
              geben. F&uuml;r die Religionen war das &uuml;ber Jahrhunderte kein
              Problem, denn sie konnten bei den Menschen mit der selbstverst&auml;ndlichen &Uuml;berzeugung
              rechnen, da&szlig; Gott existiert. So haben die Religionen eigentlich
              nicht die Existenz Gottes zum Thema, sondern ihr Thema ist das
              Verh&auml;ltnis Gottes zu den Menschen und der Menschen zu Gott.
              Denn wenn es Gott gibt, ist ja noch nicht entschieden, ob er dem
              Menschen wohlwollend gegen&uuml;ber tritt und was mit der Schuld
              geschieht, die der einzelne auf sich geladen hat. So wie die Schuld
              die Menschen voneinander trennt, k&ouml;nnte sie auch von Gott
              trennen. Dazu kann die Philosophie wenig sagen, denn sie bezieht
              sich vor allem auf die Verstandeskr&auml;fte des Menschen. Diese
              sind schon auf der Ebene des Menschlichen begrenzt, denn der einzelne
              kann bei anderen Menschen nicht durch Nachdenken darauf schlie&szlig;en,
              ob er vom anderen geachtet oder sogar geliebt wird. Das mu&szlig; der
              andere sagen oder durch Gesten zum Ausdruck bringen. Deshalb braucht
              es die Religion, damit der Mensch sich gegen&uuml;ber Gott verhalten
              kann. Doch wie kann man die Existenz Gottes mit hoher Sicherheit
              aussagen? </font></P>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Gott ist kein
                  Teil der Welt und kann daher nur schlu&szlig;folgernd
              erkannt werden</strong><br>
              Die Naturwissenschaften und der von ihr gepr&auml;gte westliche
              Mensch gehen davon aus, da&szlig; die Gegenst&auml;nde, die die
              Naturwissenschaften durch Mikroskop, Elektronenbeschleuniger oder
              andere Ger&auml;te erkennen, absolut sicher erkannt sind. Es gibt
              keinen Grund, an den Erkenntnissen zu zweifeln, jedoch mu&szlig; man
              auch sehen, da&szlig; die Naturwissenschaften keine unmittelbare
              Erkenntnis haben. Wenn es um die Gravitationstheorie von Newton
              oder die Relativit&auml;tstheorie von Einstein geht, es sind immer
              mehrere Experimente und Beobachtungen notwendig, um die Theorie
              zu best&auml;tigen. Auch im Alltag erkennen wir nur, indem wir
              verschiedene Sinneseindr&uuml;cke kombinieren und mit unserer Erinnerung
              abgleichen. Das f&uuml;hrt dazu, da&szlig; der Kranke, der sein
              Erinnerungsverm&ouml;gen verloren hat, manchmal nicht mehr das
              Licht im Zimmer einschalten kann, weil er den Schalter nicht findet.
              (s. <a href="glauben_wissen.php">Glaube und Wissen</a>) Es zeigt sich, da&szlig; Wissen in mehreren
              Schritten entsteht. Das ist in Bezug auf Gott nicht anders, allerdings
              helfen die Schritte, die die Naturwissenschaften anwenden, f&uuml;r
              die Erkenntnis Gottes nichts. Denn wenn Gott nicht wie ein Atom,
              ein anderer Stern oder ein Mensch erkannt werden kann, mu&szlig; es
              einen anderen Erkenntnisweg geben. Die Religionen und viele Philosophen
              gehen davon aus, da&szlig; Gott aus seinen Werken mit Sicherheit
              erkannt werden kann. Gottes Werke sind der Kosmos und die Natur.
              Zu der Natur geh&ouml;rt auch der menschliche Geist mit seiner
              F&auml;higkeit, die Wahrheit zu erkennen. Im Gewissen meldet sich
              eine Stimme, die den Menschen ohne Wenn und Aber zu sittlichen
              Handlungen verpflichtet.<br>
              Der Blick auf die Natur und die Besinnung auf geistige Ph&auml;nomene
              sind die Ansatzpunkte f&uuml;r die Philosophie, die Existenz Gottes
              nicht nur als wahrscheinlich, sondern als sicher aufzuzeigen.<br>
              Die Methode der Philosophie mu&szlig; jedoch eine andere sein als
              die der Naturwissenschaften, denn die genaue Beobachtung der Natur
              hat zu keiner Antwort auf die Frage nach der Existenz Gottes gef&uuml;hrt.
              Mit jedem Schritt, den die Forscher tiefer in den Atomkern vordrangen
              oder die Bausteine der Zelle entschl&uuml;sselten, stie&szlig;en
              sie immer nur auf physikalische Me&szlig;gr&ouml;&szlig;en und
              chemische Prozesse. Gott hat sich dem naturwissenschaftlich Forschenden
              nicht gezeigt, auch wenn einzelne Naturwissenschaftler durch ihr
              Forschen zu gl&auml;ubigen Menschen geworden sind. Als die Naturwissenschaften
              ihren Siegeszug begannen, war es allerdings selbstverst&auml;ndlich,
              da&szlig; ein Forscher Atheist sein mu&szlig;te. Denn er stand
              sonst in Verdacht, ungekl&auml;rte Fragen der Forschung durch Gottes
              Handeln zu erkl&auml;ren, anstatt intensiver weiter zu forschen,
              um eine Antwort innerhalb der Physik, der Chemie oder Biologie
              zu finden. Heute ist der Druck nicht mehr so gro&szlig;, als Naturwissenschaftler
              die Frage nach der Existenz Gottes mit einem strikten Nein zu beantworten,
              denn die Naturwissenschaften sehen deutlicher die Grenzen ihrer
              Methoden, die eben nur naturwissenschaftliche Fragen beantworten
              und keine philosophischen oder theologischen.</font></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Mu&szlig; etwas
                  Gedachtes wirklich sein?</strong><br>
  Vielen Menschen scheinen die Antworten der Naturwissenschaften
                tragf&auml;higer als die der Philosophie. Wenn einmal etwas durch
                Experiment und mathematische Berechnung erwiesen ist, dann mu&szlig; man
                nicht mehr zweifeln. Dagegen sind die philosophischen formulierten
                Argumente nur &#8222;gedacht&#8220;. Es k&ouml;nnte daher sein,
                da&szlig; bei aller Schl&uuml;ssigkeit der philosophischen Argumentation
                eben alles nur gedacht und nicht wirklich existent ist. Wenn
                der Aufweis der Existenz Gottes aus seinen Werken &uuml;ber diese
                Werke hinausf&uuml;hren soll, dann kann das naturwissenschaftliche
                Vorgehen nicht zum Ziel f&uuml;hren, denn die Naturwissenschaften
                liefern nur Aussagen &uuml;ber etwas innerhalb der Welt. So ergeben
                sich logische Schritte aus folgender &Uuml;berlegung:<br>
                Wenn Gott ein Teil der Welt w&auml;re, w&auml;re er nicht wirklich
              Gott, sondern nur wie einer der germanischen oder griechischen
              G&ouml;tter. Wenn er also nicht Teil der Welt sein kann, dann mu&szlig; das
              zumindest an der Welt erkennbar sein. Das ist dann der Fall, wenn
              sich die Welt nicht aus sich selbst erkl&auml;rt, also eine Ursache
              au&szlig;erhalb ihrer selbst haben mu&szlig;. Das versucht der
              sog. <a href="kosmologischer_gottesbeweis.php">kosmologische Gottesbeweis</a>, der die Ursachenkette zur&uuml;ckgeht,
              um zu einer ersten Ursache zu kommen.<br>
              Es gibt dar&uuml;ber hinaus im menschlichen Geist Ph&auml;nomene,
              die unmittelbarer &uuml;ber die Welt hinausweisen. Der Mensch erf&auml;hrt
              z.B. im Gewissen etwas, das ihn anders anspricht als die Natur.
              Er soll z.B. ohne &#8222;Wenn und Aber&#8220; dem anderen nichts
              wegnehmen. Die von den Naturwissenschaften beschreibbare Welt stellt
              diese Forderung nicht, was sich allein daran zeigt, da&szlig; trotz
              des sittlichen Anspruchs, den das Gewissen stellt, t&auml;glich
              gestohlen wird. Aus diesem Ph&auml;nomen wird der <a href="gottesbeweis_gewissen.php">Gottesbeweis
              aus dem Gewissen</a> entwickelt. Aus dem unbedingten Anspruch, den
              die Freiheit an den einzelnen stellt, n&auml;mlich sein Leben in
              die Hand zu nehmen, kann ebenfalls ein Gottesbeweis abgeleitet
              werden.<br>
              Ausgehend von den Erfahrungen mit dem menschlichen Geist kann weiter
              gezeigt werden, da&szlig; der Mensch &uuml;ber die Welt hinausdenken
              kann, denn er kann sich vorstellen, da&szlig; es neben dieser Welt
              noch viele andere Welten gibt und sogar ein geistiges Wesen, das
              diese Welten geschaffen hat. Das deutet darauf hin, da&szlig; es
              im Menschen bereits etwas gibt, mit dem er &uuml;ber den Kosmos,
              in dem wir uns befinden, hinaus gelangen kann. Hier setzten die
              <a href="gottesbeweis_wahrheit.php">Gottesbeweise aus der Wahrheit</a> und dem <a href="gottesbeweis_hoechstes_gut.php">h&ouml;chsten Guten</a> an. <br>
              Im <a href="ontologischer_gottesbeweis.php">ontologischen Gottesbeweis</a> vertraut der Mensch auf sein Denken,
              indem er ein so vollkommenes Wesen denkt, das nur dann vollkommen
              sein kann, wenn es nicht nur ganz gut, allm&auml;chtig, f&auml;hig
              zur Schaffung einer Welt gedacht werden kann, sondern auch wirklich
              existiert. Denn wenn ein solches Wesen tats&auml;chlich existiert,
              ist es vollkommener, als wenn es nur gedacht ist. </font></p>
            <p><font face="Arial, Helvetica, sans-serif">Diese &Uuml;berlegungen zeigen bereits. Der Mensch ist f&auml;hig,
              nicht nur naturwissenschaftliche Fragen zu bearbeiten. Er philosophiert
              bereits, wenn er naturwissenschaftlichen Fragen nachgeht. Denn
              das Erkennen der Natur setzt Erkenntnisf&auml;higkeit, also etwas
              Geistiges voraus, was nicht einfach in den chemischen und physikalischen
              Vorgegebenheiten enthalten ist. Wenn die Naturwissenschaften etwas
              als Materie untersuchen, dann haben sie bereits festgestellt, da&szlig; das
              Materielle sich vom Geistigen unterscheidet. Denn wer von Materie
              spricht, sagt das von einem Standpunkt au&szlig;erhalb der Materie.
              Nur wenn es noch etwas anderes gibt als Materie, kann man Materie
              denken, sonst w&auml;re man wie ein Atom oder ein Stern eben nur
              Materie und k&auml;me nicht darauf, die Gesetzm&auml;&szlig;igkeiten
              der Materie zu untersuchen.<br>
                <br>
                <strong>Empirisches an den Gottesbeweisen</strong><br>
                Zur der Frage, ob die Philosophie Gott zwar denken kann, aber
                daraus nicht auf die Existenz Gottes geschlossen werden kann,
                zeigt ein erster &Uuml;berblick &uuml;ber die Gottesbeweise,
                da&szlig; es sie bei etwas Empirischem ansetzen. Denn die Gottesbeweise,
                die von den geistigen F&auml;higkeiten des Menschen ausgehen,
                sind nicht ohne Empirie. Wenn Philosophen sagen, im Gewissen
                melde sich eine Stimme, die nicht aus der Natur komme, dann ist
                diese Stimme nicht erfunden. Der Mensch wird auf sie aufmerksam.
                Er denkt sich die Stimme nicht aus und h&ouml;rt sie dann, sondern
                er h&ouml;rt sie zuerst, versucht ihr auszuweichen, bis er sie
                ernst nimmt. Auf das Gewissen st&ouml;&szlig;t der einzelne nicht
                allein, sondern andere Menschen aus. Ebenso sind geistige Erfahrungen
                nicht einem einzelnen Menschen vorbehalten. Denn was die Philosophen
                bedenken, ist jedem Menschen zug&auml;nglich. Wir brauchen nur
                die Hilfe der Philosophie daf&uuml;r, diese geistigen Erfahrungen
                besser zu verstehen. Aber beweisen die Gottesbeweise die Existenz
                Gottes wirklich.</font></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Was leisten die Gottesbeweise?</strong><br>
  Wer einen Gottesbeweis entwickelt, ist von seiner Schl&uuml;ssigkeit &uuml;berzeugt.
              Viele lassen sich von dem Beweis &uuml;berzeugen, andere aber nicht.
              Offensichtlich &#8222;funktionieren&#8220; die Gottesbeweise nicht
              wie die Verfahren der naturwissenschaftlichen Erkenntnis. Deshalb
              sind sie im heutigen Sinne keine Beweise. Das hat bereits der mittelalterliche
              Theologe Thomas v. Aquin erkannt. Er nennt die Gottesbeweise &#8222;Wege&#8220; zu
              Gott. Das ist erkl&auml;rbar, denn Gott kann nicht einem Experiment
              unterworfen werden. Im Blick auf die Religion sind die Gottesbeweise
              auch deshalb unzureichend, weil die Religion den Menschen nicht
              nur auf der Ebne des Verstandes einfordert. Gebet, Gottesdienst,
              die Auseinandersetzung mit der eigenen Schuld, die Hoffnung auf
              Erl&ouml;sung erfassen mehr vom Menschen als seinen Verstand. Das
              Herz mu&szlig; schlagen und die ganze Seele, nicht nur die Verstandeskr&auml;fte,
              m&uuml;ssen angesprochen werden, wenn es um eine religi&ouml;se
              Beziehung zu Gott geht. Es ist wohl so, da&szlig; der Mensch durch
              Erfahrungen auf den Weg zu Gott kommt. Er st&ouml;&szlig;t dann
              auch auf die Fragen, um die sich die Gottesbeweise m&uuml;hen.
              Das ist auch deshalb sinnvoll, weil ein Glaubenszeugnis immer auf
              Skepsis und Widerspruch st&ouml;&szlig;t. Diejenigen, die den Glauben
              des anderen nicht akzeptieren, argumentieren meist auf der Ebene
              des Verstandes. Es geht dann um die Argumente, die in den Gottesbeweisen
              durchdacht werden. Neben diesen Argumenten gibt es den gewichtigen
              Einwand, der mit dem &Uuml;bel in der Welt argumentiert s. <a href="theodizee.php">Theodizee</a> &#8211; die
              Rechtfertigung Gottes angesichts der &Uuml;bel in der Welt.</font></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Zitate</strong><br>
&#8222;
              Der Zorn Gottes wird vom Himmel herab offenbar wider alle Gottlosigkeit
              und Ungerechtigkeit der Menschen, die die Wahrheit durch Ungerechtigkeit
              niederhalten. Denn was man von Gott erkennen kann, ist ihnen offenbar.
              Seit Erschaffung der Welt wird seine unsichtbare Wirklichkeit an
              den Werken der Sch&ouml;pfung mit der Vernunft wahrgenommen.&#8220; Paulus
              im 1. Kap. seines R&ouml;merbriefes (Verse 18-20)</font></p>
            <p><font face="Arial, Helvetica, sans-serif">&#8222;Wir finden n&auml;mlich unter den Dingen solche, welche
              die M&ouml;glichkeit haben, zu sein und nicht zu sei, da sich einiges
              findet, das entsteht und vergeht und infolgedessen die M&ouml;glichkeit
              hat, zu sein und auch nicht zu sein. Es ist aber unm&ouml;glich,
              da&szlig; alles von dieser Art sei, weil das, was m&ouml;glicherweise
              nicht ist, auch einmal nicht ist (bzw. nicht war). Wenn also alles
              die M&ouml;glichkeit hat, nicht zu sein, dann war hinsichtlich
              der Dinge auch einmal nichts. Wenn dies aber wahr ist, dann w&auml;re
              auch jetzt nichts, weil das, was nicht ist, nur anf&auml;ngt zu
              sein durch das, was ist. Wenn also (fr&uuml;her einmal) nichts
              Seiendes war, dann war es unm&ouml;glich, da&szlig; etwas zu sein
              anfing, und so w&auml;re jetzt nichts: was offenbar nicht stimmt.
              Thomas v. Aquin, Summa theologica I, quaestio 2, a 3</font></p>
            <p><font face="Arial, Helvetica, sans-serif">&#8222;Ist es nur griechisch zu glauben, da&szlig; vernunftwidrig
              zu handeln dem Wesen Gottes zuwider ist, oder gilt das immer und
              in sich selbst? &#8230;.. Den ersten Vers der Genesis abwandelnd,
              hat Johannes den Prolog seines Evangeliums mit dem Wort er&ouml;ffnet:
              Im Anfang war der Logos. Dies ist genau das Wort, das der Kaiser
              gebraucht: Gott handelt mit Logos. Logos ist Vernunft und Wort
              zugleich &#8211; eine Vernunft, die sch&ouml;pferisch ist und sich
              mitteilen kann, aber eben als Vernunft. Johannes hat uns damit
              das abschlie&szlig;ende Wort des biblischen Gottesbegriffs geschenkt,
              in dem alle die oft m&uuml;hsamen und verschlungenen Wege des biblischen
              Glaubens an ihr Ziel kommen und ihre Synthese finden. Im Anfang
              war der Logos, und der Logos ist Gott, so sagt uns der Evangelist.
              Das Zusammentreffen der biblischen Botschaft und des griechischen
              Denkens war kein Zufall. &#8230;.. dieses Zugehen (war) l&auml;ngst
              im Gang. Schon der geheimnisvolle Gottesname vom brennenden Dornbusch,
              der diesen Gott aus den G&ouml;ttern mit den vielen Namen herausnimmt
              und von ihm einfach das Sein aussagt, ist eine Bestreitung des
              Mythos, zu der der sokratische Versuch, den Mythos zu &uuml;berwinden
              und zu &uuml;bersteigen, in einer inneren Analogie steht. Der am
              Dornbusch begonnene Proze&szlig; kommt im Innern des Alten Testaments
              zu einer neuen Reife w&auml;hrend des Exils, wo nun der landlos
              und kultlos gewordene Gott Israels sich als den Gott des Himmels
              und der Erde verk&uuml;ndet und sich mit einer einfachen, das Dornbusch-Wort
              weiterf&uuml;hrenden Formel vorstellt: &#8222;Ich bin&#8217;s.&#8220; Mit
              diesem neuen Erkennen Gottes geht eine Art von Aufkl&auml;rung
              Hand in Hand, die sich im Spott &uuml;ber die G&ouml;tter drastisch
              ausdr&uuml;ckt, die nur Machwerke der Menschen sind (vgl. Ps 115).
              So geht der biblische Glaube in der hellenistischen Epoche bei
              aller Sch&auml;rfe des Gegensatzes zu den hellenistischen Herrschern,
              die die Angleichung an die griechische Lebensweise und ihren G&ouml;tterkult
              erzwingen wollten, dem Besten des griechischen Denkens von innen
              her entgegen zu einer gegenseitigen Ber&uuml;hrung, wie sie sich
              dann besonders in der sp&auml;ten Weisheits-Literatur vollzogen
              hat. &#8230; Nicht &#8222;mit dem Logos&#8220; handeln, ist dem
              Wesen Gottes zuwider&#8230;.<br>
              In diesen gro&szlig;en Logos, in diese Weite der Vernunft laden
              wir beim Dialog der Kulturen unsere Gespr&auml;chspartner ein.
              Sie selber immer wieder zu finden, ist die gro&szlig;e Aufgabe
              der Universit&auml;t.&#8220;<br>
              Benedikt XVI in einer Rede in der Universit&auml;t Regensburg am
              12.9.06</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Eckhard Bieger</font></p>
            <P>&copy;<a href="http://www.kath.de"> <font face="Arial, Helvetica, sans-serif">www.kath.de</font></a></P>
            </TD>
          <TD background=boxright.gif><IMG height=8 alt=""
            src="boxright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=8 alt="" src="boxbottomleft.gif"
            width=8></TD>
          <TD background=boxbottom.gif><IMG height=8 alt=""
            src="boxbottom.gif" width=8></TD>
          <TD><IMG height=8 alt="" src="boxbottomright.gif"
            width=8></TD></TR></TBODY></TABLE></TD><td style='vertical-align:top;'>
            <p><a href="http://www.update-seele.de" target="_blank"><img src="banner-update-seele-02.jpg" width="286" height="70" border="0"></a> </p>
            <p>

            <a href="http://www.echter.konkordanz.de/product_info.php?info=p16485_Der-fremde-Gott.html">
            <h4>Das Buch zum Thema: "Der fremde Gott"</h4>

              <script type="text/javascript"><!--
        google_ad_client = "pub-9537752113242914";
        google_ad_width = 336;
        google_ad_height = 280;
        google_ad_format = "336x280_as";
        google_ad_type = "text_image";
        //2006-10-23: Lexikon Symbole
        google_ad_channel = "8495253357";
        //--></script>
              <script type="text/javascript"
          src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
                  </script>
                        </p></td>
</TR>
  <TR>
    <TD vAlign=top align=left>&nbsp; </TD>
  </TR></TBODY></TABLE></BODY></HTML>