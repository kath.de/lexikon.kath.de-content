<HTML><HEAD><TITLE>Monotheletismus oder Monergetismus</TITLE>
<META http-equiv=Content-Type content="text/html; charset=iso-8859-1"><LINK 
title=fonts href="kaltefleiter.css" type=text/css 
rel=stylesheet>
<META content="MSHTML 5.50.4134.600" name=GENERATOR></HEAD>
<BODY bgColor=#ffffff leftMargin=6 topMargin=6 marginheight="6" marginwidth="6">
<TABLE cellSpacing=0 cellPadding=6 width="100%" border=0>
  <TBODY>
  <TR>
    <TD vAlign=top align=left width=100> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b>Philosophie&amp;Theologie</b></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td> <?php include("logo.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10"> <?php include("az.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </TD>
    <TD vAlign=top rowSpan=2>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
        <TBODY>
        <TR vAlign=top align=left>
          <TD width=8><IMG height=8 alt="" 
            src="boxtopleftcorner.gif" width=8></TD>
          <TD background=boxtop.gif><IMG height=8 alt="" 
            src="boxtop.gif" width=8></TD>
          <TD width=8><IMG height=8 alt="" 
            src="boxtoprightcorner.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxtopleft.gif><IMG height=8 alt="" 
            src="boxtopleft.gif" width=8></TD>
          <TD bgColor=#e2e2e2> 
            <H1><font face="Arial, Helvetica, sans-serif">Monotheletismus oder
                Monergetismus</font></H1>
          </TD>
          <TD background=boxtopright.gif><IMG height=8 
            alt="" src="boxtopright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=13 alt="" src="boxdividerleft.gif" 
            width=8></TD>
          <TD background=boxdivider.gif><IMG height=13 
            alt="" src="boxdivider.gif" width=8></TD>
          <TD><IMG height=13 alt="" 
            src="boxdividerright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxleft.gif><IMG height=8 alt="" 
            src="boxleft.gif" width=8></TD>
          <TD class=L12>
            <P><STRONG><font face="Arial, Helvetica, sans-serif">Hatte Jesus
                  einen menschlichen Willen?</font></STRONG></P>
            <P><font face="Arial, Helvetica, sans-serif">Wenn Jesus wahrer Gott
                und wahrer Mensch ist und Menschheit und Gottheit in ihm in einer
                Person verbunden sind, dann entsteht die
              Frage, wer die innere Glaubenshaltung und das Handeln dieser Person
              bestimmt. Unzweifelhaft ist, da&szlig; der Sohn Gottes bestimmend
              sein mu&szlig;, denn der Sohn Gottes hat die menschliche Natur
              angenommen. Um keinen Zweifel aufkommen zu lassen, da&szlig; die
              menschliche Natur ganz im Dienst des Heilswerkes steht, das Gott
              durch seinen in Ewigkeit gezeugten Sohn vollbringen will, neigten
              griechische Theologen dazu, Jesus nur einen Willen zuzusprechen
              (mono &#8211; eins, Thelema &#8211; Willen). Im Griechischen kann
              man auch von Energeia sprechen. Das bezeichnet nicht das, was wir
              in unserem Sprachgebrauch mit &#8222;Energie&#8220; ausdr&uuml;cken,
              sondern meint &quot;Wirkkraft&quot;. <br>
              Die Frage nach dem Willen in Jesus wurde im 7. Jahrhundert heftig
              diskutiert, so da&szlig; der Kaiser verbot, &uuml;berhaupt &uuml;ber
              die Frage den theologishcen Disput auszutragen. Severos von Antiochien,
              Johannes Philoponos u.a. Gegner des Konzils von <a href="christologische_streitigkeiten.php">Chalcedon</a>              vertraten
              die Position, da&szlig; in Jesus
              Christus nur ein Wille m&ouml;glich ist. Sie wollten damit jedem Mi&szlig;verst&auml;ndnis
              zuvorkommen, da&szlig; in Jesus zwei Subjekte handeln. </font></P>
            <P><font face="Arial, Helvetica, sans-serif"><strong>Wahrer Mensch ohne menschlichen
                Willen?</strong><br>
                Wie kann man aber davon sprechen, da&szlig; der Sohn Gottes
                wirklich Mensch geworden ist, wenn er keinen menschlichen Willen
                hat? Mu&szlig;te
              Gott die menschliche Natur sozusagen in ihrem Kern ausschlie&szlig;en,
              damit sein Heilswerk gelingen konnte? Bei der Ank&uuml;ndigung
              der Geburt des Erl&ouml;sers legt der biblische Bericht gerade
              auf die freie Zustimmung Marias Wert. Offensichtlich will Gott
              die freie Zustimmung des Menschen zu seinem Heilswerk. Die Betonung
              des Glaubens als Grundlage der Beziehung des Menschen zu Gott zeigt
              die Bedeutung der Frage. Diese ist aber nicht einfach zu l&ouml;sen,
              n&auml;mlich ob der Mensch Jesus auch einen eigenen Willen hat.
              H&auml;tte er einen eigenen Willen, dann k&ouml;nnte theoretisch
              der Mensch Jesus etwas anderes wollen als der Sohn Gottes. Dann
              w&auml;ren in Jesus Christus doch &#8222;zwei S&ouml;hne&#8220;,
              der Mensch und der ewige Sohn Gottes, die gegeneinander stehen.
              Die Alternative, da&szlig; in Jesus Christus nur ein Wille wirksam
              war, w&uuml;rde die Ganzheit der menschlichen Natur in Frage stellen.
              Der Mensch Jesus w&auml;re nicht vollst&auml;ndig. Die Meditation
              des Weihnachtsgeheimnisses hatte sehr fr&uuml;h zu der Einsicht
              gef&uuml;hrt, da&szlig; mit der Menschwerdung etwas
              f&uuml;r jeden
              Menschen geschehen sein mu&szlig;. Wenn Gott menschliche Natur
              annimmt, dann ist die ganze menschliche Natur geheiligt, nicht
              nur das Kind in der Krippe, sondenr alle Menschen. Wie konnte
              man einen Ausweg aus der Problemstellung
              finden? Auf der einen Seite gibt es in den Evangelien keine Hinweise,
              da&szlig; der Mensch Jesus sich gegen den g&ouml;ttlichen Auftrag,
              seine g&ouml;ttliche Bestimmung aufgelehnt h&auml;tte, auf der
              anderen Seite wird Jesus als voll handlungsf&auml;higer Mensch
              geschildert. <br>
              Ein Hinweis, da&szlig; der menschliche Wille dem g&ouml;ttlichen
              Auftrag zustimmen mu&szlig;te, findet sich in dem Gebet Jesu am &Ouml;lberg.
              Jesu wei&szlig;, da&szlig; er dem Unheil nicht entkommen wird und
              betet: &#8222;Vater, nicht mein Wille geschehe, sondern der Deine.&#8220; </font></P>
            <P><font face="Arial, Helvetica, sans-serif"><strong>Die Person eint
                  den g&ouml;ttlichen und den menschlichen Willen</strong><br>
                H&auml;lt
                sich die theologische Begriffsbildung an den Berichten der Evangelien,
                wird sie Jesus
                nicht einen eigenen menschlichen
              Willen absprechen k&ouml;nnen. Die L&ouml;sung konnte nicht in
              der Durchsetzung einer Begrifflichkeit liegen, sondern in der Weiterentwicklung
              des Verst&auml;ndnisses von Jesus Christus. In der L&ouml;sung
              der Frage konnte man auf eine &Uuml;berlegung des Papstes Leo I
              zur&uuml;ckgreifen, die bereits dem Konzil von Chalcedon f&uuml;r
              dessen Formulierungsarbeit vorlag. Die beiden Naturen in Jesus
              Christus stehen nicht einfach nebeneinander oder gar gegeneinander,
              sondern jede Natur wirkt ihr Eigenes im Zusammenspiel mit der anderen.
              Man kann sogar noch weitergehen und sagen, da&szlig; die Gottheit
              es der Menschheit Jesu erm&ouml;glicht, <a href="menschwerdung_jesu.php">noch
              mehr Mensch</a>  zu werden.<br>
              Das VI. &Ouml;kumenische Konzil von Konstantinopel (680/681) schlo&szlig; die
              Aussage vom Einen Willen in Jesus Christus als m&ouml;gliche Beschreibung
              der Inkarnation aus dem Sprachgebrauch der Kirche aus. </font></P>
            <p><font face="Arial, Helvetica, sans-serif">Zitate</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Die Ablehung eines menshclichen
                Willens Jesu wird einem Kaiser behauptet:              <br>
              So folgen wir den heiligen V&auml;tern in allem und auch in diesem
              Punkt und bekennen den einen Willen unseres Herrn Jesu Christi,
              der wahrhaft Gott ist, weil niemals das geistig beseelte Fleisch
              vom Logos getrennt, aus eigenem Antrieb und gegen das mit ihm hypostatisch
              geeinte g&ouml;ttliche Wort seine eigene nat&uuml;rliche Bewegung
              hervorgebracht hat &#8230;<br>
              Edikt des Kaisers Heraklius, 638</font></p>
            <p><font face="Arial, Helvetica, sans-serif"> 3. Konzil von Konstantinopel
                680/81:  Die zwei Willen in Jesus:<br>
                Ebenso verk&uuml;nden wir gem&auml;&szlig; der Lehre der heiligen
                V&auml;ter, da&szlig; sowohl zwei nat&uuml;rliche Weisen des Wollens
                bzw. Willen als auch zwei nat&uuml;rliche T&auml;tigkeiten ungetrennt,
                unver&auml;nderlich, unteilbar und unvermischt in ihm sind; und
                die zwei nat&uuml;rlichen Willen sind einander nicht entgegengesetzt
                - das sei ferne! -, wie die ruchlosen H&auml;retiker behaupteten;
                vielmehr ist sein menschlicher Wille folgsam und widerstrebt und
                widersetzt sich nicht, sondern ordnet sich seinem g&ouml;ttlichen
                und allm&auml;chtigen Willen unter; denn der Wille des Fleisches
                mu&szlig;te sich regen, sich aber nach dem allweisen Athanasius
                dem g&ouml;ttlichen Willen unterordnen; denn wie sein Fleisch Fleisch
                des Wortes Gottes genannt wird und ist, so wird auch der nat&uuml;rliche
                Wille seines Fleisches als dem Wort Gottes eigen bezeichnet und
                ist es, wie er selbst sagt: &#8222;Denn ich bin herabgestiegen
                aus dem Himmel, nicht um meinen eigenen Willen zu tun, sondern
                den Willen des Vaters, der mich gesandt hat&quot; (Johannesevangelium
                6,38); dabei nannte er den Willen des Fleisches seinen eigenen
                Willen, da auch das Fleisch ihm eigen geworden ist; denn wie sein
                ganzheiliges und makelloses beseeltes Fleisch trotz seiner Verg&ouml;ttlichung
                nicht aufgehoben wurde, sondern in der ihm eigenen Abgrenzung und
                dem ihm eigenen Begriff verblieb, so wurde auch sein menschlicher
                Wille trotz seiner Verg&ouml;ttlichung nicht aufgehoben, sondern
                ist vielmehr gewahrt, wie der Gottesgelehrte Gregor sagt: &#8222;Denn
                sein Wollen, verstanden in Bezug auf den Erl&ouml;ser, ist Gott
                nicht entgegengesetzt, da es ganz verg&ouml;ttlicht ist&quot;.</font><br>
            </p>
            <p>                <font size="2" face="Arial, Helvetica, sans-serif">Eckhard Bieger</font><br>
            </p>
            <p>&copy;<a href="http://www.kath.de"> <font face="Arial, Helvetica, sans-serif">www.kath.de</font></a></p></TD>
          <TD background=boxright.gif><IMG height=8 alt="" 
            src="boxright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=8 alt="" src="boxbottomleft.gif" 
            width=8></TD>
          <TD background=boxbottom.gif><IMG height=8 alt="" 
            src="boxbottom.gif" width=8></TD>
          <TD><IMG height=8 alt="" src="boxbottomright.gif" 
            width=8></TD></TR></TBODY></TABLE></TD><td style='vertical-align:top;'>
 <script type="text/javascript"><!--
        google_ad_client = "pub-9537752113242914";
        google_ad_width = 336;
        google_ad_height = 280;
        google_ad_format = "336x280_as";
        google_ad_type = "text_image";
        //2006-10-23: Lexikon Symbole
        google_ad_channel = "8495253357";
        //--></script>
        <script type="text/javascript"
          src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
        </script>
</td>
</TR>
  <TR>
    <TD vAlign=top align=left>&nbsp; </TD>
  </TR></TBODY></TABLE></BODY></HTML>
