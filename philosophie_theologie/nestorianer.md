---
title: Nestorianer
author: 
tags: 
created_at: 
images: []
---


# **Eine andere Vorstellung von der Einheit in Jesus Christus*
# **Es ist die gleiche Frage wie bei den ***[Monophysiten](monophysiten.php) – nämlich wie man das Zusammen von Sohn Gottes und dem Menschen Jesus denken und mit der Begrifflichkeit der griechischen Philosophie ausdrücken kann. Für die Nestorianer steht die Frage im Vordergrung, wie man dem Göttlichen gerecht wird. Die Theologenschule von Antiochien in Syrien wahrt die Einzigartigkeit der göttlichen Natur, indem sie die Unterschiedlichkeit betont. Göttliches und Menschliches dürfen sich nicht vermischen. Die andere Konzeption, nämlich die der Schule von Alexandrien, sieht die Menschheit in Jesus durch das Göttliche durchdrungen, so daß etwas Neues entsteht, eine neue Natur - Physis, Monophysis genannt. Die alexandrinische Schule wirft den Antiochenern vor, sie sehe in Jesus Christus „zwei Söhne“.**
 Im Konzil von Ephesus  wurde diese Position verurteilt, indem gesagt wurde, daß es keine Existenz nur des Menschen Jesus gab, sondern der Sohn Gottes Mensch geworden ist. Maria hat den Sohn Gottes geboren, der Menschennatur angenommen hat, aber der eine Sohn des Vaters ist. 

# **Die große Ausbreitung der Nestorianischen Kirche****
 Es gibt heute noch kleine christliche Kirchen, die nach einem Bischof von Konstantinopel aus dem 4. Jahrhundert benannt werden. Sie erkennen das Konzil von Ephesus nicht an. ***

 Diese in Persien entstandene Kirche hatte Gemeinden entlang der Seidenstraße gegründet und war mit ihren Missionaren lange vor Marco Polo in China. Timur Lenk hat diese Kirche praktisch vernichtet, viele sind in bei der Ausrottung der Armenier Anfang des 20. Jahrhunderts getötet worden, es gibt nur noch kleine Reste dieser Kirche.  

# **Nestorius hat keine zwei Subjekte in Christus angenommen, auch wenn er den Titel "Gottesgebärerin" ablehnt. Er***                wendet sich in einer Predigt aus dem Jahr 429 gegen die Bezeichnung Marias als Gottesgebärerin:**
 Hat denn Gott eine Mutter? .... Nicht gebar Maria Gott, denn was aus dem Fleische geboren ist, ist Fleisch, nicht gebar das Geschöpf den, der nicht erschaffen werden kann, sondern sie gebar den Menschen, der das Werkzeug der Gottheit war. Der Hl. Geist schuf nicht (in Maria) das Gott-Wort, weil nämlich das, was aus ihr geboren wurde, vom Hl. Geist ist, sondern er hat dem göttlichen Logos einen Tempel errichtet, den er bewohnen sollte. Der Tempel ist aus der Jungfrau. Auf keinen Fall ist der inkarnierte Gott gestorben, sondern er hat den, in dem er inkarniert war, auferweckt. (***[Inkarnation](inkarnation.php) – Fleischwerdung)***

  

***Eckhard Bieger  

©***[ ***www.kath.de](http://www.kath.de) 
