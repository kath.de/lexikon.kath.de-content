<HTML><HEAD><TITLE>Kosmologischer Gottesbeweis</TITLE>
<meta name="title" content="Philosophie&amp;Theologie">
<meta name="author" content="Redaktion kath.de">
<meta name="publisher" content="kath.de">
<meta name="copyright" content="kath.de">
<meta name="description" content="">
<meta name="abstract" content="Lexikon &uuml;ber Philosophie&amp;Theologie">
<meta http-equiv="content-language" content="de">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="date" content="2006-00-01">
<meta name="robots" content="index,follow">
<meta name="revisit-after" content="10 days">
<meta name="revisit" content="after 10 days">
<meta name="DC.Title" content="Philosophie&amp;Theologie">
<meta name="DC.Creator" content="Redaktion kath.de">
<meta name="DC.Contributor" content="J�rgen Pelzer">
<meta name="DC.Rights" content="kath.de">
<meta name="DC.Publisher" content="kath.de">
<meta name="DC.Date" content="2006-00-01">
<meta name="DC.Description" content="Lexikon &uuml;ber Philosophie&amp;Theologie">
<meta name="DC.Language" content="de">
<meta name="DC.Type" content="Text">
<meta name="DC.Format" content="text/html">
<meta name="DC.Identifier" content="http://www.kath.de/lexikon/philosophie_theologie/">
<meta name="keywords" lang="de" content="Anfang, Ursprung, Kosmologisch, Null, Zeit, Weltall, Materie ewig ">
</HEAD>
<BODY bgColor=#ffffff leftMargin=6 topMargin=6 marginheight="6" marginwidth="6">
<TABLE cellSpacing=0 cellPadding=6 width="100%" border=0>
  <TBODY>
  <TR>
    <TD vAlign=top align=left width=100>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b>Philosophie&amp;Theologie</b></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td> <?php include("logo.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10"> <?php include("az.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </TD>
    <TD vAlign=top rowSpan=2>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
        <TBODY>
        <TR vAlign=top align=left>
          <TD width=8><IMG height=8 alt=""
            src="boxtopleftcorner.gif" width=8></TD>
          <TD background=boxtop.gif><IMG height=8 alt=""
            src="boxtop.gif" width=8></TD>
          <TD width=8><IMG height=8 alt=""
            src="boxtoprightcorner.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxtopleft.gif><IMG height=8 alt=""
            src="boxtopleft.gif" width=8></TD>
          <TD bgColor=#e2e2e2>
            <H1><font face="Arial, Helvetica, sans-serif">Kosmologischer
                Gottesbeweis</font></H1>
          </TD>
          <TD background=boxtopright.gif><IMG height=8
            alt="" src="boxtopright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=13 alt="" src="boxdividerleft.gif"
            width=8></TD>
          <TD background=boxdivider.gif><IMG height=13
            alt="" src="boxdivider.gif" width=8></TD>
          <TD><IMG height=13 alt=""
            src="boxdividerright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxleft.gif><IMG height=8 alt=""
            src="boxleft.gif" width=8></TD>
          <TD class=L12>
            <P><STRONG><font face="Arial, Helvetica, sans-serif">Der Ursprung
                  des Zeitlichen</font></STRONG><br>
                  <font face="Arial, Helvetica, sans-serif"><br>
                  Wenn, wie die Religionen behaupten, Gott aus seinen Werken
                  erkannt wird, mu&szlig; das &uuml;ber den Eindruck eines Sonnenaufgangs,
                  den Anblick eines Berges, der Sch&ouml;nheit einer Blume hinausgehen.
                  Diese Eindr&uuml;cke f&uuml;hren Menschen dazu, einen Sch&ouml;pfer
                  hinter der Sch&ouml;nheit, der Gr&ouml;&szlig;e, den Kr&auml;ften
                  der Natur zu sehen. Aber der Mensch k&ouml;nnte sich t&auml;uschen,
                  vielleicht existiert nur die Welt und kein Sch&ouml;pfer. An
                  sich m&uuml;&szlig;te die Naturwissenschaft, die weit tiefer
                  in die Materie und in die Abl&auml;ufe des Lebens hineinschauen
                  kann, als es dem Menschen mit blo&szlig;en Augen m&ouml;glich
                  ist, die Grenzen des Nat&uuml;rlichen erreicht haben. Offensichtlich
                  st&ouml;&szlig;t die Naturwissenschaft aber, wenn sie die Protonen
                  und Neutronen im Atomkern erkannt hat, auf noch kleinere Teilchen,
                  die Quarks, hinter denen vielleicht ein Energiefeld liegt.
                  Auch die Biologie st&ouml;&szlig;t auf immer neue Vorg&auml;nge
                  im Stoffwechsel der Zelle, ohne die Grenze der Natur bisher
                  erreicht zu haben. Das ist erkl&auml;rbar, wenn die Welt aus
                  dem sog. Urknall entstanden ist und sich immer weiter ausdehnt
                  und entwickelt. Die Natur gibt soviel zu Wissen auf, da&szlig; wenig
                  Aussicht besteht, da&szlig; der Menschen die Grenze erreicht,
                  wo er nicht mehr auf Natur st&ouml;&szlig;t. Daf&uuml;r scheint
                  das Weltall zu gro&szlig; und das Lebendige zu komplex. Wie
                kann die Philosophie die Grenzen der Natur im Denken &uuml;berschreiten? </font></P>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Die F&auml;higkeit zu z&auml;hlen</strong><br>
  Der Mensch verf&uuml;gt &uuml;ber eine F&auml;higkeit, die er Maschinen
              zwar einprogrammieren kann. Diese werden aber diese F&auml;higkeit
              nicht verstehen: Der Mensch kann &uuml;ber jede Zahl weiter hinaus
              z&auml;hlen. Man kann sich eine gr&ouml;&szlig;tm&ouml;gliche Zahl
              ausdenken, der andere wird eine noch gr&ouml;&szlig;ere Zahl bilden
              k&ouml;nnen. Der Mensch kann aber nicht eine Zahl nennen, &uuml;ber
              die hinaus nicht weiter gez&auml;hlt werden kann. Diese F&auml;higkeit, &uuml;ber
              jede Zahl hinaus zu gelangen, ohne je mit dem Z&auml;hlen an ein
              Ende zu kommen, beschreibt den menschlichen Verstand angemessen:
              Der Mensch kann jede Grenze &uuml;berschreiten, aber er bleibt
              immer innerhalb der Grenzen der Welt. </font></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Der Anfang mu&szlig; au&szlig;erhalb
                der Welt liegen</strong><br>
  Wie der Mensch nicht bis zu allergr&ouml;&szlig;ten Zahl gelangen
              kann, &uuml;ber der nicht eine noch gr&ouml;&szlig;ere liegt, kann
              er auch nicht an den Anfang gelangen. Prinzipiell reichen die Erkenntnism&ouml;glichkeiten
              der Naturwissenschaften nicht &uuml;ber den Urknall zur&uuml;ck.
              Und wenn sich herausstellen sollte, da&szlig; man noch weiter zur&uuml;ckgehen
              k&ouml;nnte, blieben die Naturwissenschaften immer an diese Welt
              gebunden, denn sie k&ouml;nnen ja nur Energie, Masse, Licht u.a.
              Naturph&auml;nomene erfassen und daraus Gesetze ableiten. Wenn
              es einen Kosmos gegeben h&auml;tte, der dem Urknall voraus gelegen
              h&auml;tte, w&auml;re auch dieser materiell gewesen und man k&ouml;nnte
              mit den Mitteln der Naturwissenschaften nur Materielles erforschen.<br>
              Die Philosophie fragt dann noch einmal &uuml;ber diesen ersten,
              naturwissenschaftlich fa&szlig;baren Anfang hinaus. Wenn unser
              Universum sich auf der Zeitachse bewegt, die man bis zum Urknall
              und vielleicht in andere Welten zur&uuml;ckverfolgen kann, mu&szlig; es
              irgendeinen Anfang geben. Wie mu&szlig; aber dieser Anfang beschaffen
              sein, wenn er nicht wie der Urknall oder fr&uuml;here Welten von
              irgendwoher entstanden ist? </font></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Ist die Materie ewig?</strong><br>
  Das uns bekannte Weltall, in dem wir leben und Naturwissenschaften
                betreiben k&ouml;nnen, hat offensichtlich einen Anfang gehabt.
                Da das Weltall sich mit gro&szlig;er Geschwindigkeit ausdehnt,
                mu&szlig; dieser Proze&szlig; der Ausdehnung in irgendeinem Punkt
                angefangen haben, den man weithin rekonstruieren kann. Im Moment
                k&ouml;nnen wir mit den Erkenntnismitteln der Naturwissenschaften
                nicht hinter diesen Anfang, &#8222;Urknall&#8220; genannt, zur&uuml;ck.
                Aber es w&auml;re denkbar, da&szlig; der &#8222;Urknall&#8220; aus
                einem fr&uuml;heren Weltall entstanden ist, das als gro&szlig;es &#8222;Schwarzes
                Loch&#8220; geendet hat, indem es auf einen Punkt voller Energie
                geschrumpft war, so wie das jetzige Weltall einmal seine Energie
                verbraucht haben wird, um dann in sich zusammenzufallen. Es kann
                also eine endlose Folge m&ouml;glich sein, in der ein Weltall
                aus dem jeweils fr&uuml;heren neu entsteht. Ein solcher Gedanke
                kommt uns heute nicht so leicht, denn wir leben in einer Zeit,
                in der alles immer schneller zu einem Ergebnis f&uuml;hren mu&szlig;.
                Fr&uuml;here Epochen haben allerdings solche &Uuml;berlegungen
                angestellt, lange bevor die moderne Astronomie erkannte, da&szlig; das
                Weltall sich st&auml;ndig ausdehnt und daher irgendwann einmal
                angefangen haben mu&szlig;. Der Hinduismus hat solche Vorstellungen
                entwickelt, der griechische Philosoph Aristoteles ging davon
                aus, da&szlig; die Materie, aus der alles geformt wird, ewig
                sei. Da der Kreislauf der Sterne in sich zu ruhen schien, galt
                der Himmel als etwas Ewiges. Ob das Weltall ewig ist, kann mit
                den Mitteln der Naturwissenschaften heute nicht entscheiden werden.
                Wir wissen nur, da&szlig; unser Weltall aus einem Kern h&ouml;chster
                Energie entstanden ist und sich seit dem Urknall ausdehnt. Der
                Versuch, das jetzige Universum auf fr&uuml;here Welten zur&uuml;ckzuf&uuml;hren,
                ist vergleichbar dem Z&auml;hlen. Wir k&ouml;nnen uns im Denken
                viele fr&uuml;here Welten vorstellen. Es gibt jedoch eine Frage,
                die dar&uuml;ber hinausf&uuml;hrt. Denn was geschieht zwischen
                Null und Eins? Null hei&szlig;t, da&szlig; einmal nichts war
                und dann war etwas.</font></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Was anf&auml;ngt, braucht etwas, das es anfangen l&auml;&szlig;t</strong><br>
  Wenn wir zur&uuml;ckz&auml;hlen, kommen wir ohne Schwierigkeiten
              zur Null. Jeder Grundsch&uuml;ler lernt die rechnerische Bedeutung
              der Null. Wir gehen davon aus, da&szlig; der Abstand zwischen der
              Null und der Eins gleich gro&szlig; ist wie der Abstand zwischen
              der Eins und der Zwei. Gehen wir aber in den Bereich der existierenden
              Dinge, dann ist zwischen Null und Eins ein grundlegender Unterschied,
              n&auml;mlich der zwischen Nichts und Existenz. Alles, was nicht
              ewig ist, hat einmal angefangen. Wenn es aber angefangen hat, dann
              mu&szlig; ein anderes dagewesen sein, das es ins Dasein gehoben
              hat. Wie eines aus dem anderen wird, das erforschen die Wissenschaften.
              Wie aber aus Nichts &#8222;Etwas&#8220; wird, das zu erforschen,
              daf&uuml;r fehlen den Naturwissenschaften die Instrumente. Da die
              Naturwissenschaften den &Uuml;bergang vom Nichts in das Sein nicht
              beschreiben k&ouml;nnen, kann die Philosophie weiter fragen. Sie
              hat allerdings auch keine Methode, um diesen &Uuml;bergang zu erfassen.
              Sie kann aber fordern, da&szlig; irgend etwas da sein mu&szlig;te,
              das nicht aus einem anderen entstanden war, wenn die erste Welt
              geschaffen wurde. Das zu denken, sind wir mit unserer Vernunft
              f&auml;hig: Alles, was aus anderem geworden ist, kann nicht den
              Sprung vom Nichts in die Existenz erkl&auml;ren, sondern nur wie
              aus etwas Seiendem ein anderes wird. Heute wissen wir, da&szlig; die
              Zeit direkt mit dem Raum verbunden ist. Die Relativit&auml;tstheorie
              hat den Zusammenhang zur Grundlage, da&szlig; Raum und Zeit innerlich
              zusammenh&auml;ngen. Daher gibt es keine Zeit unabh&auml;ngig von
              einem Weltall. Zeitliches kann aber nur Zeitliches hervorbringen.
              Zeitliches hat einen Anfang. Irgendwann mu&szlig; dieser Anfang
              von etwas aus dem Nichts in die Existenz gebracht worden sein.
              Das kann nicht eine zeitliche geistige oder materielle Macht gewesen
              sein. Denn Zeitliches entsteht entweder aus anderem Zeitlichen
              oder aus Nicht-Zeitlichem. Wenn etwas aus Zeitlichem entstanden
              ist, mu&szlig; das andere Zeitliche irgendwann angefangen haben.
              Auch wenn wir das Zeitliche hunderte von Milliarden Jahre zur&uuml;ckverfolgen,
              irgendwann mu&szlig; die Zeit begonnen haben. Das, was die Zeit
              hat beginnen lassen, mu&szlig; au&szlig;erhalb der Zeit sein, anfangslos,
              denn h&auml;tte es einen Anfang, w&auml;re es wiederum aus Zeitlichem
              geworden oder w&auml;re selbst von etwas au&szlig;erhalb der Zeit
              ins Dasein gesetzt.<br>
              Zeitliches hat einen Anfang. Irgendwann hat die Zeit einmal angefangen.
              Dieser Anfang mu&szlig; von einer Macht herkommen, die sich selbst
              nicht auf eine andere Macht zur&uuml;ckf&uuml;hren l&auml;&szlig;t,
              also anfangslos ist. Wenn Gott, den die Religionen im Gebet anrufen,
              der Sch&ouml;pfer der Welt ist, dann mu&szlig; er au&szlig;erhalb
              der Zeit sein. Er kann nur Sch&ouml;pfer sein, wenn er selbst nicht
              der Zeit unterworfen ist. </font></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Existiert eine anfangslose Macht?</strong><br>
  Aus der Struktur der Zeit kann man darauf schlie&szlig;en, da&szlig; Zeitliches
              irgendwann einmal angefangen haben mu&szlig;. Es kann aus anderem
              Zeitlichen geworden sein. Aber auch das mu&szlig; einmal angefangen
              haben. Ist damit aber erwiesen, da&szlig; es au&szlig;erhalb der
              Zeit eine Macht gibt, die selbst nicht der Zeit unterworfen ist,
              die keinen Anfang hat? Platon und Plotin und in ihrem Gefolge die
              Philosophen des Mittelalters haben das angenommen. In der Neuzeit
              h&auml;lt man die Schlu&szlig;folgerung f&uuml;r nicht gerechtfertigt,
              denn die Philosophen verlangen, da&szlig; das menschliche Erkenntnisverm&ouml;gen
              nicht nur r&uuml;ckschlie&szlig;end zur Idee Gottes gelangt, sondern
              da&szlig; sich diese au&szlig;erzeitliche Macht dem menschlichen
              Geist unzweifelhaft zu erkennen gibt. Sie soll so zug&auml;nglich
              sein wie der Atomkern oder die Chromosomen in der Zelle. Das ist
              aber nicht Gott, denn er ist kein Wesen, das innerhalb des zeitlichen
              Weltalls zu finden w&auml;re. Die gro&szlig;e Tradition der Negativen
              Theologie betont daher, da&szlig; Gott g&auml;nzlich verschieden
              von der Welt und dem Menschen ist. Sie will verhindern, da&szlig; sich
              der Mensch Gott mit seinen Verstandeskr&auml;ften vorstellt. Die
              j&uuml;dische Religion kennt daher ein striktes Bilderverbot. Das
              Nachdenken &uuml;ber Zeit und zeitliches Existierendes f&uuml;hrt
              zu einem Anfang, der au&szlig;erhalb der Zeit liegen mu&szlig;.
              Zu mehr f&uuml;hrt dieser Gedankengang nicht. Er erg&auml;nzt sich
              jedoch durch andere Denkwege, die auf andere Weise zu Gott f&uuml;hren.
              Denn der Mensch st&ouml;&szlig;t auf etwas Unbedingtes, das pl&ouml;tzlich
              in unserer Welt aufblitzt und nicht durch die Naturwissenschaften
              erkl&auml;rt werden kann, die <a href="gottesbeweis_freiheit.php">Freiheit</a>, der Anspruch aus dem <a href="gottesbeweis_gewissen.php">Gewissen</a>,
              der Frage nach der letzten Zweckbestimmung des Universums.<br>
              Gottesbeweis aus der Freiheit. Auch die Ausrichtung des Menschen
              auf die <a href="gottesbeweis_wahrheit.php">Wahrheit</a> und ein <a href="gottesbeweis_hoechstes_gut.php">h&ouml;chstes Gut</a> erg&auml;nzen die &Uuml;berlegungen,
              die im kosmologischen Gottesbeweis angestellt werden.<br>
              <br>
              Eckhard Bieger</font></p>
            <P>&copy;<a href="http://www.kath.de"> <font face="Arial, Helvetica, sans-serif">www.kath.de</font></a></P>
            </TD>
          <TD background=boxright.gif><IMG height=8 alt=""
            src="boxright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=8 alt="" src="boxbottomleft.gif"
            width=8></TD>
          <TD background=boxbottom.gif><IMG height=8 alt=""
            src="boxbottom.gif" width=8></TD>
          <TD><IMG height=8 alt="" src="boxbottomright.gif"
            width=8></TD></TR></TBODY></TABLE></TD><td style='vertical-align:top;'>

            <a href="http://www.echter.konkordanz.de/product_info.php?info=p16456_Urknall--Evolution---Schoepfung.html">
            <h4>Das Buch zum Thema: "Urknall"</h4>

 <script type="text/javascript"><!--
        google_ad_client = "pub-9537752113242914";
        google_ad_width = 336;
        google_ad_height = 280;
        google_ad_format = "336x280_as";
        google_ad_type = "text_image";
        //2006-10-23: Lexikon Symbole
        google_ad_channel = "8495253357";
        //--></script>
        <script type="text/javascript"
          src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
        </script>
</td>
</TR>
  <TR>
    <TD vAlign=top align=left>&nbsp; </TD>
  </TR></TBODY></TABLE></BODY></HTML>