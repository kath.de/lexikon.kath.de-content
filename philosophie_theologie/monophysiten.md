---
title: Monophysiten
author: 
tags: 
created_at: 
images: []
---


# **Die theologische Konzeption der Kopten, Armenier und anderer Kirchen*
# **Die Einheit des menschgewordenen Sohnes Gottes****
# **Wenn Gott Mensch wird und nicht nur mit einem Scheinleib den Eindruck erweckt, Menschen würden einem von Ihresgleichen begegnen, dann stellt sich die Frage, wie diese Tatsache angemessen zum Ausdruck gebracht werden kann. Für den größeren Teil der Christen in Ost und West ist die Frage mit dem Konzil von Chalzedon geklärt. Aber nicht für alle. Vor allem die koptische Kirche in Ägypten hält an der Theologie des Cyrill von Alexandrien fest, der die Einheit von Gottheit und Menschheit betonte. Cyrill entwickelte für das Neue, das in Christus geschehen ist, den Begriff „Hypostatische Union“, Mit Hypostase ist das gemeint, das ein Individuum trägt, man könnte es in etwa it Substanz übersetzen. Um die Einheit von Menschheit und Gottheit in Jesus zu betonen, sprach er von einer neuen Wirklichkeit und nannte diese eine Natur, nur "eine" Natur, weil die Einheit nicht mehr aufgelöst und in Jesus Christus nicht Zwei, sondern nur einer verehrt werden kann. Mia Physis – eine Natur.**
 Da aber die Begrifflichkeit ***[„Natur“](christologische_streitigkeiten.php)  in der theologischen Tradition  das Menschliche und das Göttliche in Christus bezeichnet, also gerade für die Gegebenheit, in der Jesus als „Zwei“, eben in zwei Naturen beschrieben werden kann, ist es verwirrend, den Begriff Natur zugleich für das zu verwenden, was wir im Unterschied zu den Naturen als ***[Person](person.php)  bezeichnen. Die göttliche udn die menschliche Natur brauchen ein Einheitsprinzip. Die Auseinandersetzungen begannen im 5. Jahrhundert und führten zu einem ersten Ergebnis beim Konzil von Ephesus 433. Hier setze sich die Richtung durch, die der Patriarch von Alexandrien, Cyrill vertrat. Um die Einheit in Christus zu betonen, nennt das Konzil von Ephesus Maria nicht nur Christusgebärerin, sondern Gottesgebärerin (griechisch Theotokos) genannt wird. Damit sollte deutlich werden, daß der Mensch Jesus nicht zuerst für sich existiert hatte und ihm später der Sohn Gottes einwohnte. Im Augenblick der Empfängnis nimmt das Wort, der ewige Sohn Gottes, Fleisch an und damit konstituiert sich erst die menschliche Person Jesu. Aber muß man Gottheit und Menschheit in Jesus nicht doch deutlicher auseinanderhalten? Auch wenn man mit Cyrill darin übereinstimmt, daß Jesus Christus nicht zwei Personen sind, es nicht zwei Söhne gab, den von Ewigkeit gezeugten Sohn Gottes und den vom Heiligen Geist gezeugten Menschen Jesus, vermischen sich die Naturen nicht. ***

 Man kann die Einheit in Christus auch anders denken. Das war der Ansatz der Schule von Antiochien in Syrien. Sie betont die Verschiedenheit von Gottheit und Menschheit. (***[s. Nestorianer](nestorianer.php))                  ***

 Daß Nestorius sich nicht mit dem Titel „Gottesgebärerin“ für Maria einverstanden erklären konnte, liegt auch daran, daß er ein Mißverständnis befürchtete, nämlich daß der ewige Logos selbst aus Maria geboren wurde, also für den Logos ein menschlicher Geburtsvorgang behauptet werden könnte. In seinem Brief an Cyrill v. Alexandrien aus dem Jahr 430 schreibt er:***


„Überall, wo die heiligen Schriften vom Heilswirken des Herrn berichten, schreiben sie Geburt und Leiden nicht der Gottheit, sondern der Menschheit Christi zu. Will man sich daher möglichst exakt ausdrücken, dann muß man die heilige Jungfrau „Christusgebärerin“ (Christotokos) und nicht „Gottesgebärerin“ (Theotokos) nenne. Hören wir das Evangelium, das laut verkündet: „Stammbaum Jesu Christi, des Sohnes Davids, des Sohnes Abrahams (Mt 1,1). Natürlich war das göttliche Wort nicht Sohn Davids. ..... Wer könnte auf den Gedanken kommen, daß die Gottheit des eingeborenen Sohnes ein Geschöpf des Geistes wäre? ..... „Und dies ist mein Leib (nicht meine Gottheit), der für euch hingegeben wird.“ Und noch Tausende von anderen Aussagen bezeugen dem Menschengeschlecht, daß man nicht annehmen kann, die Gottheit des Sohnes sei erst vor einiger Zeit geboren worden oder sie sei körperlicher Leiden fähig. Vielmehr war das die mit der Gottheit vereinigte Menschennatur.“ 

# **Die Unterscheidung von Natur und Person****
 Wenn wir heute vom ***[Personbegriff](person.php) her die Einheit von Gottheit und Menschheit ganz anders denken können, wird deutlich, daß mit den Begriffen, die die griechische Philosophie damals bereitstellte, ein Ausweg aus dem Streit der theologischen Schulen nicht zu finden war, denn die Griechen kannten nur Seele udn Leib udn kein Prinzip, das die geistige und die körperliche Natur des Menschen eint.***

 Ein wichtiger Schritt zu einer theologischen Weiterentwicklung wurde durch das ***[Konzil von Chalcedon](christologische_streitigkeiten.php) erreicht (456).  Es vermittelt zwischen den beiden Positionen, die einmal die Einheit und zum anderen die Getrenntheit der Naturen betonen durch die Formel: 

# ** Unvermischt - bezogen auf Gottheit und Menschheit und**
 Ungetrennt - bezogen auf den einen, die Person Jesu.***

# **Das Konzil unterscheidet dann auch zwischen Natur (Physis) und Person (Prosopon - griechisch Antlitz).**
 Einige Kirchen haben sich der Konzilsentscheidung bis heute nicht angeschlossen, so die koptische und mit ihr die äthiopische, die syrische und die armenische Kirche. Das war nicht nur in den unterschiedlichen Akzentsetzungen der theologischen Schulen von Alexandrien und Antiochien begründet, sondern auch in regionalen Differenzen und einer starken Tendenz, sich von der damaligen Hauptstadt, Byzanz nicht dominieren zu lassen. So hatte der Patriarch von Byzanz beim 1. Konzil von Konstantinopel 381 durchgesetzt, daß in der Rangfolge Alexandrien vom 2. Platz verdrängt wurde. Konstantinopel rückte auf den 2. Platz, Alexandrien mußte mit dem 3. Platz vorlieb nehmen. Zwischen der koptischen und der chaldäischen Kirche im Irak wird die Diskussion, die das 5., 6. und 7. Jahrhundert beherrschte, immer noch fortgeführt. Die orthodoxen Kirchen Griechenlands, Rußlands und die anderen europäischen Kirchen stehen auf dem Boden des Konzils von Chalcedon.***

 Dieser komplizierte Streit um Begriffe verdeckt, daß die getrennten Kirchen in dem Verständnis der Person Jesu Christi nicht entscheidend auseinander liegen. Denn diese Kirchen behaupten von Jesus Christus „nicht eine einfache Natur, sondern eher eine einzige zusammengesetzte Natur, in der Gottheit und Menschheit ungetrennt und unvermischt vereinigt sind.“ (Wiener christologische Erklärung vom 29.8.1976) Da aber die Theologie des Cyrill von Alexandrien für die koptische Kirche identitätsbildend ist und die syrische Kirche sich später der Sicht Cyrills anschloß, bleiben die Gegensätze bis heute bestehen.  

# **Zitate **
 Die Idee von der neuen Natur, die durch die Menschwerdung des Sohnes entstanden sein soll, findet sich bei Appolinaris von Laodizea der zwischen 310 und 390 lebte, also  vor Cyrill von Alexandrien:***


„ Es wird aber in ihm (Jesus Christus) das Geschaffene in Einheit mit dem Ungeschaffenen bekannt, das Ungeschaffene in Vermischung mit dem Geschaffenen, wobei eine einzige Natur aus den beiden Teilen konstituiert wird, da nämlich der Logos mit seiner göttlichen Vollkommenheit eine Teilkraft zu dem Ganzen beiträgt. Ähnlich wie beim Menschen aus zwei unvollkommenen Teilen die eine Natur entsteht.“ Fragmente 

# ** Er hat sich dessen entäußert, was er war, und hat angenommen, was er nicht war. Er wurde nicht zu Zweien, sondern hat es auf sich genommen, eins aus Zweien zu werden. Gott ist nämlich beides, das Empfangende und das Empfagnene. Zwei Naturen fließen in eins zusammen, es gibt nicht zwei Söhne.**
 Gregor von Nanzianz , Oratio37,2 um 380            

# ****
 Die Einheit der Naturen ist nicht getrennt ... Das Getrenntsein besteht nämlich nicht in der Aufhebung der Einheit, sondern in der Vorstellung des Fleisches und der Gottheit. .... Christus ist unteilbar in seinem Christsein, er ist aber zweifach in dem Gott- und Menschsein. Er ist einfach in der Sohnschaft.... In dem Antlitz (dem Prosopon) des Sohnes ist er ein einziger, aber ... geschieden in den Naturen der Menschheit und der Gottheit. Denn wir kennen nicht zwei Christus‘ noch zwei Söhne oder Eingeborene oder Herren, nicht den einen und den anderen Sohn, .... sondern ein und denselben, der erblickt worden ist in seiner erschaffenen und unerschaffenen Natur.***

 Predigt des Nestorius über Matthäus 22,2,, aus dem Jahr 429 

# **So bekennen wir einen Christus und Herrn. Dabei beten wir nicht etwa einen Menschen mit dem Wort (gemeint ist der ewige Sohn Gottes) zusammen an, damit nicht durch die Wörter „mit“ und „zusammen“ die Vorstellung einer Scheidung eingeführt wird. Wir beten vielmehr einen und denselben Christus an. Denn sein Leib ist dem göttlichen Wort nicht fremd. Mit diesem Leibe thront er ja auch zur Rechten des Vaters, weil wiederum nicht zwei Söhne an der Seite des Vaters sitzen, sondern ein Sohn entsprechend der Einigung mit dem eigenen Fleisch. Wollten wir aber die Einigung der Person nach als unverständlich oder nicht angemessen ablehnen, so wären wir gezwungen, zwei Söhne anzunehmen. Dann kämen wir nämlich nicht umhin, zwischen einem Menschen einerseits, der durch den Namen „Sohn“ allenfalls geehrt wäre, und andererseits dem Wort Gottes, dem von Natur aus Name und Wirklichkeit der Sohnschaft zueigen ist. Es darf demnach der eine Herr Jesus Christus nicht in zwei Söhne gespalten werden. …. Die Schrift (d.h. die Bibel) sagt ja nicht, das Wort habe sich mit der Person eines Menschen vereinigt, sondern es sei selber Fleisch geworden. …… **
 Die Worte des Heilandes aber, die sich im Evangelium finden, verteilen wir nicht auf zwei Hypostasen (Subjekte) oder Personen. Der eine und einzige Christus ist nicht zwiespältig, obwohl er aus zwei und zwar zwei verschiedenen Wirklichkeiten besteht. Diese sind aber zu einer unteilbaren Einheit verbunden, wie etwa auch der Mensch aus Leib und Seele besteht und doch nicht zweifach, sondern aus beiden Teilen ist. Wir lassen daher gemäß dem rechten Glauben die menschlichen wie auch göttlichen Aussagen von einem gesprochen sein.***

 Cyrill von Alexandrien, aus dem Brief an Nestorius, 430 

# **Anathemata des Konzils von Ephesus**
 Nr. 3: Wer nach erfolgter Vereinigung in dem einen Christus die Hypostasen (Substanzen) auseinanderreißt, indem er sie nur durch eine äußere Verbindung der Würde nach, durch ihre Hoheit und Macht, verbunden sein läßt und nicht vielmehr durch eine Vereinigung im Sinne einer physischen Einswerdung, der sei ausgeschlossen. 

# **Der mittelalterliche Theologe Thomas v. Aquin hat die Frage noch einmal behandelt. Aus dem Text wird deutlich, daß die Person als das einigende Prinzip von Leib und Seele gedacht wird, die Person also nicht mit der Seele identisch ist:**
 Weil bei uns Leib und Seele zusammen eine Person bilden, glaubten manche, die Vereinigung von Leib und Seele in Christus leugnen zu müssen. Sie fürchteten nämlich, sonst die Annahme einer zweiten Person nicht umgehen zu können, weil sie sahen, daß bei den übrigen Menschen aus der Verbindung von Leib und Seele die Person erstellt wird. Nun verbinden sich aber gerade bei allen anderen Menschen Leib und Seele, um für sich zu bestehen, im Gegensatz zu Christus, wo sie sich vereinigen, um von einer höheren Person getragen zu werden. Deshalb ersteht in Christus aus Leib und Seele kein neuer Träger seiner menschlichen Natur, sondern beide gehen zusammen in eine Person ein, die schon vorher bestand. Daraus, daß Leib und Seele nicht wie bei uns eine eigene Person ergeben, folgt jedoch nicht, daß ihre Vereinigung in Christus weniger kraftvoll ist. Eine Vereinigung mit einer vornehmeren, höheren Person mindert nämlich in keiner Weise Kraft und Würde, sie erhöht sie. ***

 Summa theologica II q.2 a 5, Nr. 54-55 

***Eckhard Bieger 

  

©***[ ***www.kath.de](http://www.kath.de) 
