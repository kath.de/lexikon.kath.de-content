<HTML><HEAD><TITLE>Ontologischer Gottesbeweis</TITLE>
<meta name="title" content="Philosophie&amp;Theologie">
<meta name="author" content="Redaktion kath.de">
<meta name="publisher" content="kath.de">
<meta name="copyright" content="kath.de">
<meta name="description" content="">
<meta name="abstract" content="Lexikon &uuml;ber Philosophie&amp;Theologie">
<meta http-equiv="content-language" content="de">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="date" content="2006-00-01">
<meta name="robots" content="index,follow">
<meta name="revisit-after" content="10 days">
<meta name="revisit" content="after 10 days">
<meta name="DC.Title" content="Philosophie&amp;Theologie">
<meta name="DC.Creator" content="Redaktion kath.de">
<meta name="DC.Contributor" content="J�rgen Pelzer">
<meta name="DC.Rights" content="kath.de">
<meta name="DC.Publisher" content="kath.de">
<meta name="DC.Date" content="2006-00-01">
<meta name="DC.Description" content="Lexikon &uuml;ber Philosophie&amp;Theologie">
<meta name="DC.Language" content="de">
<meta name="DC.Type" content="Text">
<meta name="DC.Format" content="text/html">
<meta name="DC.Identifier" content="http://www.kath.de/lexikon/philosophie_theologie/">
<meta name="keywords" lang="de" content="Et<font face="Arial, Helvetica, sans-serif">Etwas ueber das hinaus Groesseres nicht gedacht
              werden kann, Anselm Canterbury, Descartes, Horizont, Geist">
</HEAD>
<BODY bgColor=#ffffff leftMargin=6 topMargin=6 marginheight="6" marginwidth="6">
<TABLE cellSpacing=0 cellPadding=6 width="100%" border=0>
  <TBODY>
  <TR>
    <TD vAlign=top align=left width=100> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b>Philosophie&amp;Theologie</b></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td> <?php include("logo.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10"> <?php include("az.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </TD>
    <TD vAlign=top rowSpan=2>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
        <TBODY>
        <TR vAlign=top align=left>
          <TD width=8><IMG height=8 alt="" 
            src="boxtopleftcorner.gif" width=8></TD>
          <TD background=boxtop.gif><IMG height=8 alt="" 
            src="boxtop.gif" width=8></TD>
          <TD width=8><IMG height=8 alt="" 
            src="boxtoprightcorner.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxtopleft.gif><IMG height=8 alt="" 
            src="boxtopleft.gif" width=8></TD>
          <TD bgColor=#e2e2e2> 
            <H1><font face="Arial, Helvetica, sans-serif">Ontologischer
                Gottesbeweis</font></H1>
          </TD>
          <TD background=boxtopright.gif><IMG height=8 
            alt="" src="boxtopright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=13 alt="" src="boxdividerleft.gif" 
            width=8></TD>
          <TD background=boxdivider.gif><IMG height=13 
            alt="" src="boxdivider.gif" width=8></TD>
          <TD><IMG height=13 alt="" 
            src="boxdividerright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxleft.gif><IMG height=8 alt="" 
            src="boxleft.gif" width=8></TD>
          <TD class=L12>
            <P><STRONG><font face="Arial, Helvetica, sans-serif">Der
                  Gottesbeweis, der allein aus dem Denken abgleitet wird</font></STRONG></P>
            <P><font face="Arial, Helvetica, sans-serif">Der Erweis der Existenz Gottes aus dem Sein (Ontologie ist die
              Wissenschaft vom Sein) ist eigentlich der gedanklich klarste. Er
              wurde von Anselm von Canterbury (1033-1109) ausgearbeitet. Die
              Argumentation ist so aufgebaut:<br>
              Der Mensch kann sich etwas ausdenken, das durch nichts &uuml;bertroffen
              wird. Wenn es das h&ouml;chste und vollkommenste Wesen ist, das
              sich jemand ausdenkt, dann gibt es etwas noch h&ouml;heres und
              vollkommneres, n&auml;mlich wenn dieses Wesen nicht nur als M&ouml;glichkeit
              gedacht wird, sondern wenn es wirklich existiert.</font></P>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Der geistige Hintergrund
                des Beweises</strong><br>
            </font><font face="Arial, Helvetica, sans-serif">Der
                Erweis Gottes aus dem Denken heraus setzt ein gro&szlig;es
              Vertrauen in das eigene Denken voraus. Dieses Vertrauen gewinnt
              der mittelalterliche Denker aus dem Sch&ouml;pfungsglauben. Wenn
              der Verstand, den Gott geschaffen hat, ein h&ouml;chstes Wesen
              denken kann, das aus sich heraus notwendig existiert, dann mu&szlig; es
              dieses Wesen geben. Denn der von Gott geschaffene Verstand kann
              den Menschen nicht in die Irre f&uuml;hren. Als sich im 14. Jahrhundert
              das Vertrauen in die Erkenntnisf&auml;higkeit des Menschen langsam
              aufl&ouml;ste, f&uuml;hrte das nicht nur in der Reformation zum
              Ringen um die Frage, ob Gott dem Menschen tats&auml;chlich gn&auml;dig
              ist, sondern zu einer Skepsis, was der Mensch &uuml;berhaupt erkennen
              kann. Die &#8222;Kritik der reinen Vernunft&#8220; von Immanuel
              Kant legt dar, da&szlig; der Mensch weder &#8222;das Ding an sich&#8220; noch
              sein eigenes Ich noch Gott erkennen kann. Er erkennt nur Ph&auml;nomene,
              die er im Raum-Zeit-Schema einander zuordnen kann. Es geht also
              darum, was das Denken des Menschen zu leisten vermag.<br>
              Der ontologische Gottesbeweis vertraut ganz auf das Denken des
              Menschen. Allerdings gab es schon im Mittelalter Anfragen an den
              ontologischen <a href="gottesbeweise.php">Gottesbeweis</a>. So f&uuml;hrte der M&ouml;nch Gaunilo,
              ein Zeitgenosse Anselms, folgende &Uuml;berlegung an: Der Mensch
              k&ouml;nne sich die allersch&ouml;nste Insel ausdenken. Zwar sei
              auch diese Insel noch vollkommener, wenn sie nicht nur in den Gedanken,
              sondern auch tats&auml;chlich existiere. Die tats&auml;chliche
              Existenz der Insel folge aber nicht aus der Vorstellung. Auch Thomas
              von Aquin zeigt sich in der Summa contra Gentiles I, 10 und 11
              skeptisch, ob die menschliche Geisteskraft f&auml;hig ist, allein
              vom Gedanken her die gemeinte Wirklichkeit zu erfassen. Der Mensch
              m&uuml;sse von dem ausgehen, was er direkt erkenne. <br>
              Es bleibt jedoch das Denken des Menschen, worauf es ankommt. Denn
              weil Gott kein Gegenstand in dieser Welt ist, kann er nicht mit
              den Sinnen erkannt werden. Allerdings ist der Mensch f&auml;hig,
              ein Wesen zu denken, das absolut vollkommen ist, so da&szlig; nicht
              Gr&ouml;&szlig;eres &uuml;ber dieses Wesen hinaus gedacht werden
              kann. Dieses Wesen ist nicht nur vollkommen, es ist auch nicht
              vom Zuf&auml;lligen in Frage gestellt, es existiert nicht von anderem
              her, sondern aus sich. Das sind alles Gedanken, die der Mensch
              denken kann. Gott kann nicht nicht existieren, denn sonst w&auml;re
              er nicht Gott, sondern etwas Endliches. Folgt daraus, da&szlig; das
              Gedachte, weil es ja in sich notwendig existieren mu&szlig;, auch
              tats&auml;chlich existiert? Dazu kann eine &Uuml;berlegung Immanuel
              Kants &uuml;ber das M&ouml;gliche sowie eine genauere Analyse des
              menschlichen Geistes, die Karl Rahner entwickelt hat, hin f&uuml;hren.</font></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>1. Das M&ouml;gliche
                setzt ein Notwendiges voraus</strong><br>
  Bei allem Zweifel an der Erkenntnisf&auml;higkeit kommt der Mensch
              am Ende doch darauf zur&uuml;ck, da&szlig; nicht alles gedacht
              sein kann, sondern etwas existiert, zumindest er als Denkender.
              Von dem, was unzweifelhaft existiert, kann der Mensch ausgehen.
              Kant selbst hat diesen Gedanken in seiner Schrift &#8222;Beweisgrund
              zu einer Demonstration des Daseins Gottes&#8220; aus dem Jahr 1762
              getan. Diese Argumentation nimmt er allerdings in seiner &#8222;Kritik
              der reinen Vernunft&#8220; wieder zur&uuml;ck. <br>
&#8222;
              Wenn nun alles Dasein aufgehoben wird, so ist nichts schlechthin
              gesetzt, es ist &uuml;berhaupt gar nichts gegeben, kein Materiale
              zu irgend etwas Denklichem (Denkbarem), und alle M&ouml;glichkeit
              f&auml;llt g&auml;nzlich weg. &#8230;. Demnach zu sagen: es existiert
              nichts, hei&szlig;t ebensoviel als: es ist ganz und gar nichts;
              und es widerspricht sich offenbar, dessenungeachtet hinzuf&uuml;gen,
              es sei etwas m&ouml;glich. <br>
              Wodurch alle M&ouml;glichkeit &uuml;berhaupt aufgehoben wird, das
              ist schlechterdings unm&ouml;glich. &#8230;. Allein wodurch das
              Materiale und die Data zu allem M&ouml;glichen aufgehoben werden,
              dadurch wird auch alles M&ouml;gliche verneint. Nun geschieht dieses
              durch die Aufhebung alles Daseins, als wenn alles Dasein verneint
              wird, so wird auch alle M&ouml;glichkeit aufgehoben. Mithin ist
              schlechterdings unm&ouml;glich, da&szlig; gar nichts existiere.&#8220; Ausz&uuml;ge
              aus Nr. 2 und 3 der zweiten Betrachtung.<br>
              Die Schlu&szlig;folgerungen Kants finden sich in den &Uuml;berschriften
              zu den Kapiteln 2-4<br>
              - Die innere M&ouml;glichkeit aller Dinge setzt irgendein Dasein
              voraus.<br>
              - Es ist schlechterdings unm&ouml;glich, da&szlig; gar nichts existiere.<br>
              - Alle M&ouml;glichkeit ist in irgend etwas Wirklichem gegeben.</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Die M&ouml;glichkeit leitet sich daraus ab, da&szlig; es auch
              sein kann. Wenn man sich einen K&ouml;rper vorstellt, der ausgedehnt
              ist, es aber gar keinen Raum gibt, dann ist Ausdehnung als eine
              der Bestimmungen eines K&ouml;rpers nicht m&ouml;glich. Mit dieser &Uuml;berlegung
              (in Nr. 4) zeigt Kant, da&szlig; das M&ouml;gliche irgendeinen
              Bezug zum Wirklichen haben mu&szlig;. Da&szlig; nichts m&ouml;glich
              ist, wird dadurch ausgeschlossen, da&szlig; etwas existiert. Wie
              kommt Kant aber vom M&ouml;glichen auf ein absolutes Wesen, das
              in sich notwendig ist und nicht von au&szlig;en abh&auml;ngt? </font></p>
            <p><font face="Arial, Helvetica, sans-serif">&#8222;Es existiert ein schlechterdings notwendiges Wesen: Alle
              M&ouml;glichkeit setzt etwas Wirkliches voraus, worin und wodurch
              alles Denkliche gegeben ist. Demnach ist (das) eine gewisse Wirklichkeit,
              deren Aufhebung selbst alle innere M&ouml;glichkeit &uuml;berhaupt
              aufheben w&uuml;rde. Dasjenige aber, dessen Aufhebung oder Verneinung
              alle M&ouml;glichkeit vertilgt, ist schlechterdings notwendig.
              Demnach existiert etwas absolut notwendigerweise. Bis dahin erhellt,
              da&szlig; ein Dasein eines oder mehrere Dinge selbst aller M&ouml;glichkeit
              zugrunde liege, und da&szlig; dieses Dasein an sich selbst notwendig
              sei.&#8220; Nr. 2 aus der Dritten Betrachtung<br>
              Kant kommt auf diesem Weg zur gleichen Erkenntnis wie der <a href="kosmologischer_gottesbeweis.php">kosmologische
              Gottesbeweis:</a> Alles, was auch nicht sein kann, setzt ein Sein voraus,
              das aus sich heraus notwendig ist. Vom M&ouml;glichen kommt man
              deshalb zum Notwendigen, weil alles Zuf&auml;llige auch nicht sein
              k&ouml;nnte und daher das M&ouml;gliche ebenfalls ins Nichts fallen
              w&uuml;rde. Nur ein Absolutes, aus sich selbst existierendes Sein
              kann als Basis gesehen werden, da&szlig; ein Geist sich etwas ausdenkt,
              das nicht nur gedacht, sondern auch m&ouml;glich, d.h. existieren
              kann. Das M&ouml;gliche ist ja nicht nur gedacht, es mu&szlig; so
              gedacht werden, da&szlig; es auch sein kann. Daf&uuml;r mu&szlig; es
              in sich ohne Widerspruch sein. Kant benutzt als Beispiel die Triangel:
              Sie kann nicht viereckig gedacht werden. Sie kann aber als mit
              einem Rechten Winkel ausgestattet gedacht werden, dann ist sie
              nicht gleichschenklig, aber weiterhin ein Dreieck. Aber auch wenn
              etwas ohne innere Widerspr&uuml;che gedacht wird, ist es deshalb
              noch nicht m&ouml;glich. Es kommt die Dimension des Wirklichen
              hinzu. Das M&ouml;gliche mu&szlig; auch wirklich werden k&ouml;nnen.
              Wenn es keinen Raum g&auml;be, k&ouml;nnte man sich einen K&ouml;rper
              zwar als ausgedehnt vorstellen, aber es k&ouml;nnte ihn nicht geben.
              Nur etwas, das in sich notwendig ist, gibt letztlich die Basis
              daf&uuml;r her, da&szlig; etwas m&ouml;glich ist. Was aber die
              letzte Basis f&uuml;r das M&ouml;gliche ist, mu&szlig; deshalb
              in sich notwendig sein, weil ohne eine letzte Notwendigkeit das
              M&ouml;gliche nicht mehr m&ouml;glich w&auml;re. Was die Unm&ouml;glichkeit
              des M&ouml;glichen verhindert, kann selbst nicht mehr nur m&ouml;glich
              sein, es mu&szlig; aus eigener Notwendigkeit existieren, ohne sich
              von anderem herzuleiten.<br>
              Diese Schlu&szlig;folgerungen Kants m&ouml;gen schwer nachzuvollziehen
              sein. Sie zeigen zumindest, da&szlig; das menschliche Denken immer
              wieder auf das &#8222;In-sich-Notwendige&#8220;, das absolute Sein
              zur&uuml;ckkommt, das nicht von etwas anderem abh&auml;ngt. Eine
              genauere Analyse der menschlichen Dynamik f&uuml;hrt noch einen
              Schritt weiter:</font></p>
            <p><font face="Arial, Helvetica, sans-serif"><br>
                <strong>2. Die innere Dynamik des Geistes zielt auf das Absolute</strong><br>
              Kant hat in seiner &#8222;Kritik der reinen Vernunft&#8220; einen
              neue Denkweg er&ouml;ffnet, n&auml;mlich nicht die Ergebnisse des
              Denkens in den Blick zu nehmen, sondern zu fragen: Wenn der Mensch
              das und das erkennen bzw. denken kann, was sind die Bedingungen
              daf&uuml;r, da&szlig; er das &uuml;berhaupt kann?<br>
              Aus der Analyse der Dynamik des Denkens kann Karl Rahner in &#8222;H&ouml;rer
              des Wortes&#8220; ableiten, da&szlig; in seinem Streben nach Wahrheit
              und Erf&uuml;llung der Mensch schon die Wirklichkeit bejaht. Er
              kann nur etwas erkennen, wenn er aktiv auf das zu Erkennende zugeht.
              Weil er in seinem Wahrheitsstreben auf das Ganze der Wirklichkeit
              zielt und sein Verlangen nur durch das h&ouml;chste Gute erf&uuml;llt
              werden kann, bejaht der Mensch in seinem Erkennen und Streben bereits
              die ganze Wirklichkeit. Das ist dem Menschen nicht direkt bewu&szlig;t,
              da sein Geist auf das in der Welt Erkennbare ausgerichtet ist.
              Aber in der Dynamik des Erkennens und Wollens &uuml;berschreitet
              der Mensch bereits alles Endliche und bejaht somit das Absolute. <br>
              Indem Rahner das Bild des Horizonts nutzt, in dem alles, was wir
              erkennen, auftaucht, kann er zugleich zeigen, da&szlig; dieser
              Horizont in jedem Erkennen pr&auml;sent ist. Der Erkennende mu&szlig; sich
              diesen Horizont nicht ausdr&uuml;cklich bewu&szlig;t machen, trotzdem
              ist der Horizont immer da. Der dem Menschen in seinem Erkenntnisverm&ouml;gen
              mitgegebene Horizont ist immer da. Da der Mensch, um einzelnes
              zu erkennen, immer auch den Horizont mit erkennt, erkennt er Gott,
              aber in besonderer Weise: </font></p>
            <p><font face="Arial, Helvetica, sans-serif">Es ist ein Vorgriff
                auf das an sich unbegrenzte Sein ... Mit der Notwendigkeit, mit
                der dieser Vorgriff gesetzt wird, ist auch das
              unendliche Sein Gottes mitbejaht. Zwar stellt der Vorgriff nicht
              unmittelbar Gott als Gegenstand dem Geist vor, weil der Vorgriff
              als Bedingung der M&ouml;glichkeit der gegenst&auml;ndlichen Erkenntnis
              von sich &uuml;berhaupt keinen Gegenstand in seinem Sein vorstellt.
              Aber in diesem Vorgriff als notwendiger und immer schon vollzogener
              Bedingung jeder menschlichen Erkenntnis und jedes menschlichen
              Handelns ist doch auch schon die Existenz eines absoluten Seins,
              also Gottes, mitbejaht. S. 81</font></p>
            <p><font face="Arial, Helvetica, sans-serif">&#8222;.... der Vorgriff
                und seine Weite lassen sich als vorhandene und f&uuml;r alle
                Erkenntnis notwendige Bedingung nur erkennen und als solche bejahen
                in der aposteriorischen Erfassung eines
              realen Seienden als deren notwendige Bedingung. So ist unsere Fassung
              der Gotteserkenntnis nur die erkenntnismetaphysische Wendung der
              realontologischen Formulierung der traditionellen Gottesbeweise.&#8220; S.
              82</font></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Reflexion auf
                  den Beweisgang</strong><br>
  Der ontologische Gottesbeweis konfrontiert den Menschen auf der
                einen Seite mit der F&auml;higkeit, die ihm mit seiner Vernunft
                gegeben sind und zum anderen mit der Grenze des Denkens. Er kann
                Gott denken und er bejaht eine umfassende Wirklichkeit, indem
                er erkennend auf alles ausgreift. Aber der Mensch kann Gott nicht
                erfassen und damit geistig beherrschen. Deshalb hat der ontologische
                Gottesbeweis direkt mit dem menschlichen Geist zu tun. Der Mensch
                kann das Gr&ouml;&szlig;te denken, dieses Gr&ouml;&szlig;te entzieht
                sich ihm aber, weil es seinen Geist unendlich &uuml;bersteigt. </font></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Zitate</strong><br>
  Die Argumentation f&uuml;r den ontologischen Gottesbeweis findet
              sich bei Anselm von Canterbury in seinem Proslogion:<br>
&#8222;
              Ein anderes ist es, da&szlig; etwas im Verstande ist, ein anderes,
              einzusehen, da&szlig; etwas existiert. Denn wenn ein Maler vorausdenkt,
              was er schaffen wird, hat er es zwar im Verstand, erkennt aber
              noch nicht, da&szlig; existiert, was er noch nicht geschaffen hat.
              Wenn er aber schon geschaffen hat, hat er sowohl im Verstande,
              als er auch einsieht, da&szlig; existiert, war er bereits geschaffen
              hat. So wird auch der Tor (der deshalb ein Tor ist, weil er die
              Existenz Gottes nicht erkennt) &uuml;berf&uuml;hrt, da&szlig; wenigstens
              im Verstande etwas ist, &uuml;ber das hinaus Gr&ouml;&szlig;eres
              nicht gedacht werden kann, wie er das versteht, wenn er es h&ouml;rt,
              und was immer verstanden ist, ist im Verstande. Und sicherlich
              kann das, &uuml;ber das hinaus Gr&ouml;&szlig;eres nicht gedacht
              werden kann, nicht im Verstande allein sein. Denn wenn es wenigstens
              im Verstande allein ist, kann gedacht werden, da&szlig; es auch
              in Wirklichkeit existiere &#8211; was gr&ouml;&szlig;er ist. Wenn
              also das, &uuml;ber das hinaus Gr&ouml;&szlig;eres nicht gedacht
              werden kann, im Verstande allein ist, so ist eben das, &uuml;ber
              das hinaus Gr&ouml;&szlig;eres nicht gedacht werden kann, etwas &uuml;ber
              das hinaus Gr&ouml;&szlig;eres gedacht werden kann. Das aber kann
              nicht sein. Es existiert also ohne Zweifel etwas, &uuml;ber das
              hinaus Gr&ouml;&szlig;eres nicht gedacht werden kann, sowohl im
              Verstande als auch in Wirklichkeit.&#8220; Aus Kap. 2<br>
&#8222;
              Das existiert so schlechthin wahrhaft, da&szlig; sein Nicht-Sein
              nicht einmal gedacht werden kann. Denn es kann gedacht werden,
              da&szlig; etwas existiert, dessen Nicht-Sein gedacht werden kann,
              was ein Gr&ouml;&szlig;eres ist als das, wenn Nicht-Sein gedacht
              werden kann, was ein Gr&ouml;&szlig;eres ist als das, dessen Nicht-Sein
              gedacht werden kann. Wenn daher das, &uuml;ber das hinaus Gr&ouml;&szlig;eres
              nicht gedacht werden kann, als nicht-existierend gedacht werden
              kann, ist eben das, &uuml;ber das hinaus Gr&ouml;&szlig;eres nicht
              gedacht werden kann, nicht das, &uuml;ber das hinaus Gr&ouml;&szlig;eres
              nicht gedacht werden kann, was sich nicht miteinander vereinbaren
              l&auml;&szlig;t. So wahrhaft existiert also etwas, &uuml;ber das
              hinaus Gr&ouml;&szlig;eres nicht gedacht werden kann, da&szlig; sein
              Nicht-Sein nicht einmal gedacht werden kann. Und das bist Du, Herr,
              unser Gott. So wahrhaft existierst Du also, Herr mein Gott, da&szlig; Dein
              Nicht-Sein nicht einmal gedacht werden kann. Wenn n&auml;mlich
              ein Geist etwas Besseres als dich denken k&ouml;nnte, erh&ouml;be
              sich das Gesch&ouml;pf &uuml;ber den Sch&ouml;pfer und urteilte &uuml;ber
              den Sch&ouml;pfer, was g&auml;nzlich widersinnig ist. Allerdings
              kann einzig mit Ausnahme von Dir alles, was sonst noch existiert,
              als nicht-existierend gedacht werden. Du allein besitzt somit am
              wahrhaftigsten von allem und deshalb am meisten Existenz, weil
              alles, was sonst nicht existiert, nicht so wahrhaft und deswegen
              in geringerem Ma&szlig; Existenz besitzt.&#8220; Aus Kap. 3</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Ontologischer Gottesbeweis nach Descartes:<br>
  Ren&eacute; Descartes, ein Philosoph des 17. Jahrhunderts steht
              am Beginn des neuzeitlichen Nachdenkens &uuml;ber Gott. Descartes &uuml;berpr&uuml;ft
              das Erkenntnisverm&ouml;gen des Menschen. Da nach seiner Analyse
              dem Menschen nur innere Erkenntnisse eine gen&uuml;gende Sicherheit
              geben, leitet Descartes die Einsicht in die Existenz Gottes von
              inneren Vorstellungen ab. Er begr&uuml;ndet die Existenz Gottes
              nicht wie Anselm dadurch, da&szlig; ein h&ouml;chstes Sein, wenn
              es wirklich vollkommen sein soll, nicht nur vom Menschen gedacht,
              sondern auch existieren mu&szlig;. Er fragt nach der Ursache, die
              die Vorstellung eines h&ouml;chsten Wesens im menschlichen Geist &uuml;berhaupt
              m&ouml;glich macht. Descartes versteht den Menschen als ein endliches
              Wesen, das aus sich selbst nicht in der Lage ist, ein h&ouml;chstes,
              absolut vollkommenes und gutes Wesen zu denken. Daher k&ouml;nne
              dieser Gedanke nicht aus dem menschlichen Geist selbst entspringen,
              sondern mu&szlig; von diesem h&ouml;heren Wesen selbst kommen.</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Ren&eacute; Descartes, Meditationen &uuml;ber
                die Grundlagen der Philosophie,<br>
              Dritte Meditation &#8222;&Uuml;ber das Dasein Gottes&#8220;<br>
&#8222;
              Unter dem Namen &#8222;Gott&#8220; verstehe ich eine Substanz,
              die unendlich, unabh&auml;ngig, allwissend und allm&auml;chtig
              ist und von der ich selbst geschaffen bin, ebenso wie alles andere
              Existierende, falls es solches gibt. Dies alles ist nun in der
              Tat so vorz&uuml;glich, da&szlig; mir dessen Abstammung aus mir
              allein um so weniger m&ouml;glich erscheint, je sorgf&auml;ltiger
              ich es betrachte. Man mu&szlig; daher aus dem Zuvorgesagten schlie&szlig;en,
              da&szlig; Gott notwendig existiert.<br>
              Denn: Zwar habe ich eine Vorstellung von Substanz, eben weil ich
              ja selbst Substanz bin; dennoch w&auml;re das deshalb noch nicht
              die Vorstellung einer unendlichen Substanz, da ich ja endlich bin;
              es sei denn, sie r&uuml;hrte von einer Substanz her, die in Wahrheit
              unendlich ist. <br>
              Auch darf ich nicht glauben, ich begriffe das Unendliche nicht
              in einer wahrhaften Vorstellung, sondern nur durch Verneinung des
              Endlichen, so wie ich Ruhe und Dunkelheit durch Verneinung von
              Bewegung und Licht begreife. Denn ganz im Gegenteil sehe ich offenbar
              ein, da&szlig; mehr Sachgehalt in der unendlichen Substanz als
              in der endlichen enthalten ist und da&szlig; demnach der Begriff
              des Unendlichen dem des Endlichen, d.i. der Gottes dem meinen,
              selbst gewisserma&szlig;en vorhergeht. Wie sollte ich sonst auch
              begreifen k&ouml;nnen, da&szlig; ich zweifle, da&szlig; ich etwas
              w&uuml;nsche, d.i. da&szlig; mir etwas mangelt und ich nicht ganz
              vollkommen bin, wenn gar keine Vorstellung von einem vollkommeneren
              Wesen in mir w&auml;re, womit ich mich vergleiche und so meine
              M&auml;ngel erkenne?&#8220; Nr. 22-24<br>
&#8222;
              Doch vielleicht bin ich etwas mehr, als ich selbst wei&szlig;,
              und sind alle die Vollkommenheiten, die ich Gott zuschreibe, als
              M&ouml;glichkeiten irgendwie in mir angelegt, wenngleich sie sich
              noch nicht entfalten und noch nicht zur Wirklichkeit gelangt sind.
              Mache ich doch die Erfahrung, da&szlig; meine Erkenntnis schon
              jetzt langsam w&auml;chst. Auch sehe ich nicht, was im Wege st&auml;nde,
              da&szlig; sie so mehr und mehr w&uuml;chse bis ins Unendliche und
              warum ich nicht mit so gewachsener Erkenntnis alle &uuml;brigen
              Vollkommenheiten Gottes sollte erreichen k&ouml;nnen. Und schlie&szlig;lich,
              warum, wenn ich doch einmal die Anlage zu diesen Vollkommenheiten
              besitze, sie nicht hinreichen sollte, um eine Vorstellung von ihnen
              hervorzurufen.<br>
              Indessen kann nichts von all dem der Fall sein. Denn erstens, mag
              es nun wahr sein, da&szlig; meine Erkenntnis gradweise w&auml;chst,
              und da&szlig; in mir vieles zwar als M&ouml;glichkeit angelegt,
              aber noch nicht wirklich ist, so geh&ouml;rt doch nichts davon
              zur Vorstellung Gottes, in der n&auml;mlich nichts blo&szlig;e
              Anlage ist. Denn eben dieses gradweise Anwachsen ist der sicherste
              Beweis der Unvollkommenheit. Au&szlig;erdem, wenn auch meine Erkenntnis
              stets weiter und weiter w&uuml;chse, so sehe ich nichtsdestoweniger
              ein, da&szlig; sie darum doch niemals aktuell unendlich sein wird,
              da sie doch niemals soweit gelangen wird, da&szlig; sie immer noch
              eines weiteren Zuwachses f&auml;hig w&auml;re. Gott aber, urteile
              ich, ist in der Weise aktuell unendlich, da&szlig; seiner Vollkommenheit
              sich nichts hinzuf&uuml;gen l&auml;&szlig;t.&#8220; Nr. 26-27<br>
&#8222;
              Bleibt nur zu untersuchen, wie ich jene Vorstellung von Gott erhalten
              habe. Denn weder habe ich sie aus den Sinnen gesch&ouml;pft, noch
              ist sie mir jemals wider Erwarten gekommen, wie es die Vorstellung
              der sinnlich wahrnehmbaren Dinge zu tun pflegen, wenn sie sich
              den &auml;u&szlig;eren Sinnesorganen zeigen oder zu zeigen scheinen;
              noch auch habe ich sie mir ausgedacht, denn ich kann ganz und gar
              nichts von ihr wegnehmen und ihr auch nichts hinzuf&uuml;gen. Daher
              bleibt nur &uuml;brig, da&szlig; sie mir angeboren ist, ebenso
              wie mir auch die Vorstellung von mir selbst angeboren ist. Es ist
              auch gar nicht zu verwundern, da&szlig; Gott mir, als er mich schuf,
              diese Vorstellung eingepflanzt hat, damit sie gleichsam als das
              Zeichen sei, mit der der K&uuml;nstler sein Werk signiert. &Uuml;brigens
              braucht dieses Zeichen gar nicht etwas von dem Werke selbst Verschiedener
              zu sein, sondern einzig und allein daher, da&szlig; Gott mich geschaffen
              hat, ist es ganz glaubhaft, da&szlig; ich gewisserma&szlig;en nach
              seinem Bild und seinem Gleichnis geschaffen bin und da&szlig; dieses
              Gleichnis &#8211; in dem die Idee Gottes steckt &#8211; von mir
              durch dieselbe F&auml;higkeit erfa&szlig;t wird, durch die ich
              mich selbst erfasse.&#8220;<br>
              Nr. 37 f</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Eckhard Bieger</font><br>
            </p>
            <P>&copy;<a href="http://www.kath.de"> <font face="Arial, Helvetica, sans-serif">www.kath.de</font></a></P>
            </TD>
          <TD background=boxright.gif><IMG height=8 alt="" 
            src="boxright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=8 alt="" src="boxbottomleft.gif" 
            width=8></TD>
          <TD background=boxbottom.gif><IMG height=8 alt="" 
            src="boxbottom.gif" width=8></TD>
          <TD><IMG height=8 alt="" src="boxbottomright.gif" 
            width=8></TD></TR></TBODY></TABLE></TD><td style='vertical-align:top;'>
 <script type="text/javascript"><!--
        google_ad_client = "pub-9537752113242914";
        google_ad_width = 336;
        google_ad_height = 280;
        google_ad_format = "336x280_as";
        google_ad_type = "text_image";
        //2006-10-23: Lexikon Symbole
        google_ad_channel = "8495253357";
        //--></script>
        <script type="text/javascript"
          src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
        </script>
</td>
</TR>
  <TR>
    <TD vAlign=top align=left>&nbsp; </TD>
  </TR></TBODY></TABLE></BODY></HTML>
