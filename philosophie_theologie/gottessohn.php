<HTML><HEAD><TITLE>Gottessohn</TITLE>
<META http-equiv=Content-Type content="text/html; charset=iso-8859-1"><LINK 
title=fonts href="kaltefleiter.css" type=text/css 
rel=stylesheet>
<META content="MSHTML 5.50.4134.600" name=GENERATOR></HEAD>
<BODY bgColor=#ffffff leftMargin=6 topMargin=6 marginheight="6" marginwidth="6">
<TABLE cellSpacing=0 cellPadding=6 width="100%" border=0>
  <TBODY>
  <TR>
    <TD vAlign=top align=left width=100> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b>Philosophie&amp;Theologie</b></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td> <?php include("logo.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10"> <?php include("az.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </TD>
    <TD vAlign=top rowSpan=2>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
        <TBODY>
        <TR vAlign=top align=left>
          <TD width=8><IMG height=8 alt="" 
            src="boxtopleftcorner.gif" width=8></TD>
          <TD background=boxtop.gif><IMG height=8 alt="" 
            src="boxtop.gif" width=8></TD>
          <TD width=8><IMG height=8 alt="" 
            src="boxtoprightcorner.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxtopleft.gif><IMG height=8 alt="" 
            src="boxtopleft.gif" width=8></TD>
          <TD bgColor=#e2e2e2> 
            <H1><font face="Arial, Helvetica, sans-serif">Gottessohn</font></H1>
          </TD>
          <TD background=boxtopright.gif><IMG height=8 
            alt="" src="boxtopright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=13 alt="" src="boxdividerleft.gif" 
            width=8></TD>
          <TD background=boxdivider.gif><IMG height=13 
            alt="" src="boxdivider.gif" width=8></TD>
          <TD><IMG height=13 alt="" 
            src="boxdividerright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxleft.gif><IMG height=8 alt="" 
            src="boxleft.gif" width=8></TD>
          <TD align="right" class=L12>
            <P align="left"><STRONG><font face="Arial, Helvetica, sans-serif">Hyios
                  Theou, Filius Dei</font></STRONG></P>
            <P align="left"><font face="Arial, Helvetica, sans-serif">Jesus hat eine besondere N&auml;he zu Gott. Dazu nur zwei Zitate
              aus dem Neuen Testament. Als Jesus sich von Johannes taufen lie&szlig;, &#8222;sah
              er, da&szlig; der Himmel sich &ouml;ffnete und der Geist wie eine
              Taube auf ihn herabkam. Und eine Stimme aus dem Himmel sprach:
              Du bist mein geliebter Sohn, an dir habe ich Gefallen gefunden.&#8220; Markus
              1,11. <br>
              Jesus nennt Gott seinen Vater, nicht nur im Vater unser. &#8222;Mir
              ist von meinem Vater alles &uuml;bergeben worden, niemand kennt
              den Sohn, nur der Vater, und niemand kennt den Vater, nur der Sohn
              und der, dem es der Sohn offenbaren will.&#8220; (Matth&auml;us
              11,27) Was soll das genau hei&szlig;en? Bezeichnet sich Jesus als
              wirklichen Sohn, der mehr ist als ein besonders geliebtes Gesch&ouml;pf
              Gottes oder ist er von Gott adoptiert, wie es z.B. von j&uuml;dischen
              K&ouml;nigen im Kr&ouml;nungsritual hei&szlig;t: &#8222;Mein Sohn
              bist du, heute habe ich dich gezeugt.&#8220; Dieser Vers Psalm
              2 besagt, da&szlig; mit der Inthronisation Gott den K&ouml;nig
              zu seinem Sohn erhebt. Als Jesus lebte und predigte, entstand bereits
              die Frage. Er konfrontierte die Menschen nicht nur mit seiner Predigt
              und heilte sie &#8211; er sprach ihnen die Vergebung der S&uuml;nden
              zu. Als er das &ouml;ffentlich bei der Heilung eines Gel&auml;hmten
              sagte, stutzen die j&uuml;dischen Schriftgelehrten: &#8222;Wie
              kann dieser Mensch so reden? Er l&auml;stert Gott. Wer kann S&uuml;nden
              vergeben au&szlig;er dem einen Gott?&#8220; Markus 2,7 Hingerichtet
              wurde Jesus wegen Gottesl&auml;sterung, weil er im Verh&ouml;r
              auf die Frage &#8222;Bist du der Messias, der Sohn des lebendigen
              Gottes?&#8220; antwortete: &#8222;Du hast es gesagt.&#8220; Matth&auml;us
              26,63<br>
              Im griechischen Kulturraum konnte die Frage noch pr&auml;ziser
              gestellt werden. Wird Jesus nur als Sohn Gottes bezeichnet oder
              ist er nach seiner &#8222;Natur&#8220; nach seinem &#8222;Wesen&#8220; wirklich
              Gott? Die Frage konnte nicht einfach unentschieden beiseite gelegt
              werden, denn nur wenn dieser Jesus von Nazareth wirklich Gott ist,
              konnte er ein f&uuml;r alle mal die Menschen erl&ouml;st haben.
              Ansonsten w&auml;re er wie die vielen Priester und Propheten gewesen,
              die in ihrer Zeit gewirkt haben, aber nach ihrem Tod Nachfolger
              brauchten, weil ihre Predigt und die von ihnen dargebrachten Opfer
              nicht endg&uuml;ltig waren. </font></P>
            <P align="left"><font face="Arial, Helvetica, sans-serif"><strong>Unzul&auml;ngliche Antworten </strong><br>
              Eine Antwort auf die Frage besteht
              darin, Jesus als einen Propheten zu 
              bezeichnen, vielleicht als den wichtigsten - aber nicht als wirklichen
              Sohn Gottes. Die <a href="christologische_streitigkeiten.php">Ebioniten</a> 
              und andere vertraten diese Auffassung. Die Juden hatten vor den
              Griechen die Vielg&ouml;tterei &uuml;berwunden.
              Die Griechen waren nicht zuletzt durch philosophische &Uuml;berlegungen
              zu der &Uuml;berzeugung gekommen, da&szlig; es nicht mehrere G&ouml;tter
              geben kann, wenn man von Gott spricht. F&uuml;r das griechische
              Denken war es eine gro&szlig;e Erkenntnis gewesen, da&szlig; Gott
              nur einer sein kann, denn nur wenn etwas in sich eins und unteilbar
              ist, kann es das h&ouml;chste Wesen sein. So konnten sie den Polytheismus
              der alten griechischen G&ouml;tterwelt &uuml;berwinden. Denn wenn
              es mehrere G&ouml;tter gibt, dann sind sie nicht wirklich g&ouml;ttlich.
              Das war auch die Position des Priesters <a href="arianismus.php">Arius.</a>              Wie f&uuml;r das griechische Denken hei&szlig;t f&uuml;r ihn Vielheit
              Mangel an Vollkommenheit. Was man teilen kann, kann nicht an der
              obersten Stelle im Kosmos stehen. Das Eine kann Ursprung des Vielen
              sein. Dann bleibt das Viele immer unterhalb des Einen. Vielheit
              hat die Tendenz zu zerfallen. Wenn in Gott schon Vielheit vorgefunden
              wird, dann wird das G&ouml;ttliche auch einmal zerfallen, so wie
              die Gestalten, die der Mensch kennt. Sie zerfallen alle, auch der
              menschliche Leib. Deshalb braucht der Leib etwas, das nur &#8222;eins&#8220; ist,
              womit die Griechen die Seele bezeichnen, das unteilbar Letzte des
              Menschen, das, da es nicht teilbar ist, &uuml;ber den Tod hinaus
              Bestand hat. <br>
              Da Arius viel Zustimmung erfahren hat und viele Christen die gedankliche
              Klarheit der griechischen Philosophie anerkannten, war es zu einem
              tiefen Dissens gekommen. Solange die Christen verfolgt wurden,
              konnte die Frage nicht abschlie&szlig;end gekl&auml;rt werden.
              Als Kaiser Konstantin im Mail&auml;nder Edikt im Jahr 313 den Christen
              nicht nur Religionsfreiheit gew&auml;hrte, sondern die christliche
              Religion zur geistigen Grundlage des r&ouml;mischen Staates erkl&auml;rte,
              bestand auch ein politisches Interesse an der Bereinigung dieser
              Frage. 325 ludt der Kaiser die Bisch&ouml;fe des Reiches nach Nic&auml;a,
              heute ein Vorort von Konstantinopel, ein. Wie konnte das Konzil
              eine L&ouml;sung finden, die von den Christen auch akzeptiert werden
              w&uuml;rde? Einen Beweis, da&szlig; Jesus von Nazareth wirklich
              der Sohn Gottes war, konnte mit philosophischer Argumentation nicht
              gefunden werden. Es gab und gibt auch keinen naturwissenschaftlichen
              Beweis im heutigen Sinn. Man kann Gott keinem Experiment unterziehen.
              Beweismittel ist allein das Zeugnis der biblischen Schriften. Die
              Evangelien und die Apostelbriefe kommen in ihren Aussagen den Anforderungen
              des griechischen Denkens nicht nahe genug. Das oben zitierte Wort: &#8222;Du
              bist mein geliebter Sohn, an dir habe ich Gefallen gefunden.&#8220; k&ouml;nnte
              auch so verstanden werden, da&szlig; Jesus bei der Taufe im Jordan
              von Gott gleichsam <a href="christologische_streitigkeiten.php">adoptiert</a> 
              wurde. Ob Gott ihm damit Anteil an seinem g&ouml;ttlichen
              Wesen gegeben hat, mu&szlig; offen bleiben. Da es aber eindeutige
              Aussagen (Texte s.u.) gibt, die besagen, da&szlig; Jesus nicht
              erst Mensch und dann von Gott als Sohn angenommen wurde, sondern
              schon vor seiner
              Geburt bei Gott war, konnte das Konzil nur entscheiden, da&szlig; Jesus
              mit Gott wesensgleich ist. Das entspricht der Tauftradition der
              jungen Kirche. Der Aufschwung des <a href="http://www.kath.de/Kirchenjahr/weihnachten.php">Weihnachtsfestes</a> ist eine Antwort
              auf die Infragestellung der Gottessohnschaft Jesu Christi.</font></P>
            <P align="left"><font face="Arial, Helvetica, sans-serif"><strong>Theologische
                  Aussagen in der Taufliturgie</strong><br>
              In der Taufe wird der Glaube an den dreieinigen Gott schon nach
              den Schriften des Neuen Testaments bekannt. Bei Matth&auml;us gibt
              Jesus, bevor in den Himmel aufgenommen wird, den J&uuml;ngern folgenden
              Auftrag:<br>
              &#8222;
              Mir ist alle Macht gegeben im Himmel und auf der Erde. Darum geht
              zu allen V&ouml;lkern und macht alle Menschen zu meinen J&uuml;ngern;
              tauft sie auf den Namen des Vaters und des Sohnes und des Heiligen
              Geistes, und lehrt sie, alles zu befolgen, was ich euch geboten
              habe. Seid gewi&szlig;: Ich bin bei euch alle Tage bis ans Ende
              der Welt.&#8220; Matth&auml;us 28,18-20</font></P>
            <P align="left"><font face="Arial, Helvetica, sans-serif"><strong>Theologische L&ouml;sung</strong><br>
              Da Jesus sich selbst als <a href="sohn_gottes.php">Sohn</a> 
              bezeichnete, ist es offensichtlich, da&szlig; der
              Sohn aus dem Vater hervorgegangen ist. Deshalb unterscheidet das
              Konzil, da&szlig; Jesus &#8222;gezeugt&#8220; aber
              nicht &#8222;geschaffen&#8220; ist. W&auml;re der Sohn Gottes Gesch&ouml;pf,
              dann w&auml;re er geschaffen. Mit dem Verb &#8222;zeugen&#8220; kann
              ausgedr&uuml;ckt werden, da&szlig; es ein innerg&ouml;ttlicher
              Vorgang ist, wenn der Sohn aus dem Vater hervorgeht. Das innerg&ouml;ttliche
              Leben ist nicht nur das Hervorgehen des Sohnes aus dem Vater, sondern
              im Heiligen Geist wird diese Beziehung personhaft Wirklichkeit.
              Das Konzil war so wichtig, da&szlig; es in den Gottesdiensten pr&auml;sent
              gehalten wird, durch das sog. Grosse<br>
              Glaubensbekenntnis. Gebetet wird es heute in der Fassung, die
              es auf dem Konzil von Konstantinopel von 381, dem sog. 2. &ouml;kumenischen
              Konzil, erhalten hat. Dieses Bekenntnis ist allen christlichen
              Kirchen gemeinsam.<br>
              <br>
                <strong>Zitate</strong><br>
                Und das Wort ist Fleisch geworden,<br>
                und hat unter uns gewohnt,<br>
                und wir haben seine Herrlichkeit gesehen,<br>
                die Herrlichkeit des einzigen Sohnes vom Vater,<br>
                voll Gnade und Wahrheit .....<br>
                Niemand hat Gott je gesehen.<br>
                Der Einzige, der Gott ist und am Herzen des Vaters ruht,<br>
                er hat Kunde gebracht.<br>
                Johannes 1,14,18            </font></P>
            <p align="left"><font face="Arial, Helvetica, sans-serif">In jener Zeit sprach Jesus:<br>
              Ich preise dich, Vater, Herr des Himmels und der Erde, weil du
                all das den Weisen und Klugen verborgen, den Unm&uuml;ndigen
                aber offenbart hast. Ja, Vater, so hat es dir gefallen. Mir ist
                von meinem Vater alles &uuml;bergeben worden; Niemand kennt den
                Sohn, nur der Vater, und niemand kennt den Vater, nur der Sohn
                und der, dem der Sohn es offenbaren will.&#8220;<br>
                Matth&auml;us 11,25-27            </font></p>
            <p align="left"><font face="Arial, Helvetica, sans-serif">Jesus wird vom Hohen Rat wegen Gottesl&auml;sterung verurteilt:<br>
              Sie sagten: Wenn du der Messias bist, dann sag es uns! Er antwortete
                ihnen: auch wenn ich es euch sage &#8211; ihr glaubt mir ja doch
                nicht. Von nun an wird der Menschensohn zur Rechten des allm&auml;chtigen
                Gottes sitzen. Da sagten alle: du bist also der Sohn Gottes.
                Er antwortete ihnen: Ihr sagt es, ich bin es. Da riefen sie:
                Was brauchen wir noch Zeugenaussagen? Wir haben es selbst aus
                seinem eigenen Mund geh&ouml;rt.<br>
              Lukas 22,67-71</font></p>
            <p align="left"><font face="Arial, Helvetica, sans-serif">Jesus, als er Maria von Magdala am Ostermorgen begegnet:<br>
              Jesus sagte zu ihr: Halte mich nicht fest; denn ich bin noch nicht
                zum Vater hinaufgegangen. Geh aber zu meinen Br&uuml;dern und
                sage ihnen: Ich gehe hinauf zu meinem Vater und zu eurem Vater,
                zu meinem Gott und eurem Gott.<br>
              Maria von Magdala ging zu den J&uuml;ngern und verk&uuml;ndete
              ihnen: Ich habe den Herrn gesehen. Und sie richtete aus, was er
              gesagt hatte.<br>
              Johannes 20,17-18</font></p>
            <p align="left"><font face="Arial, Helvetica, sans-serif">Vor allen Gesch&ouml;pfen als Anfang hat Gott aus sich eine vern&uuml;nftige
              Kraft erzeugt, welche vom Heiligen Geist auch Herrlichkeit des
              Herrn, ein andermal Sohn, dann Weisheit, bald Engel, bald Gott,
              bald Herr und Logos genannt wird &#8230; Alle Attribute kommen
              derselben zu, weil sie dem v&auml;terlichen Willen dient, und weil
              sie aus dem Vater durch das Wollen erzeugt worden ist. Doch sehen
              wir nicht &auml;hnliche Vorg&auml;nge auch bei uns? Wenn wir n&auml;mlich
              ein Wort aussprechen, erzeugen wir ein Wort, ohne damit etwas zu
              verlieren, ohne da&szlig; also die Vernunft in uns weniger wird.<br>
              Justin Dialog mit dem Juden Tryphon etwas 150 erfa&szlig;t</font></p>
            <p align="left"><font face="Arial, Helvetica, sans-serif">Einer ist Arzt, aus dem Fleische zugleich und aus Geist gezeugt
              und ungezeugt, im Fleische erschienener Gott, im Tod wahrhaftiges
              Leben aus Maria, sowohl wie aus Gott, leidensf&auml;hig und leidensunf&auml;hig,
              Jesus Christus, unser Herr. &#8230;<br>
              Denn unser Gott, Jesus, der Christus, wurde von Maria im Scho&szlig;e
              getragen, nach Gottes Heilsplan aus Davids Stamm und doch aus heiligem
              Geist.<br>
              Ignatius von Antiochien an die Kirche von Ephesus m 110</font></p>
            <p align="left"><font face="Arial, Helvetica, sans-serif"><br>
              Wir glauben an den einen Gott, den Vater, den allm&auml;chtigen,
              den Sch&ouml;pfer aller sichtbaren und unsichtbaren Dinge, <br>
              und an den einen Herrn Jesus Christus, den Sohn Gottes, gezeugt
              aus dem Vater als Einziggeborener, aus dem Wesen des Vaters, Gott
              aus Gott, Licht aus Licht, wahrer Gott aus wahrem Gott,<br>
              gezeugt, nicht geschaffen, eines Wesens (homoousios) mit dem Vater,
              durch alles geworden ist, sowohl was im Himmel als auch auf der
              Erde ist, &#8230;..<br>
              diejenigen aber, die sagen: Es war einmal eine Zeit, da er nicht
              war, und bevor er gezeugt wurde, war er nicht, und da&szlig; er
              aus dem Nichts geworden sei, und die behaupten, aus einer anderen
              Hypostase (Substanz) oder Wesenheit oder ver&auml;nderlich oder
              wandelbar sei der Sohn Gottes, diese schlie&szlig;t die katholische
              und apostolische Kirche aus.<br>
              Konzil von Nic&auml;a, 325<br>
              <br>
              Denn der eine Christus ist selbst sowohl immer Gottes Sohn durch
              seine Natur als auch Menschensohn, der durch die Gnade in der Zeit
              angenommen wurde. Keinesfalls wurde er auf solche Weise angenommen,
              da&szlig; er zuerst erschaffen und dann angenommen wurde, sondern
              er (der Mensch Jesus) wurde durch die Annahme selbst erschaffen.<br>
              Augustinus Gegen die Lehre der Arianer, um 418<br>
            </font></p>
            <p align="left"><font size="2" face="Arial, Helvetica, sans-serif">Text: Eckhard
                Bieger S.J.
            </font></p>
            <P align="left"><font face="Arial, Helvetica, sans-serif">                </font><font face="Arial, Helvetica, sans-serif">&copy;<a href="http://www.kath.de"> www.kath.de</a></font></P>
            </TD>
          <TD background=boxright.gif><IMG height=8 alt="" 
            src="boxright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=8 alt="" src="boxbottomleft.gif" 
            width=8></TD>
          <TD background=boxbottom.gif><IMG height=8 alt="" 
            src="boxbottom.gif" width=8></TD>
          <TD><IMG height=8 alt="" src="boxbottomright.gif" 
            width=8></TD></TR></TBODY></TABLE></TD><td style='vertical-align:top;'>
 <script type="text/javascript"><!--
        google_ad_client = "pub-9537752113242914";
        google_ad_width = 336;
        google_ad_height = 280;
        google_ad_format = "336x280_as";
        google_ad_type = "text_image";
        //2006-10-23: Lexikon Symbole
        google_ad_channel = "8495253357";
        //--></script>
        <script type="text/javascript"
          src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
        </script>
</td>
</TR>
  <TR>
    <TD vAlign=top align=left>&nbsp; </TD>
  </TR></TBODY></TABLE></BODY></HTML>
