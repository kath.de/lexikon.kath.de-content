<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <title>Das B&ouml;se - der Drache</title>


  <meta name="title" content="Philosophie&amp;Theologie">

  <meta name="author" content="Redaktion kath.de">

  <meta name="publisher" content="kath.de">

  <meta name="copyright" content="kath.de">

  <meta name="description" content="">

  <meta name="abstract" content="Lexikon &uuml;ber Philosophie&amp;Theologie">

  <meta http-equiv="content-language" content="de">

  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

  <meta name="date" content="2006-00-01">

  <meta name="robots" content="index,follow">

  <meta name="revisit-after" content="10 days">

  <meta name="revisit" content="after 10 days">

  <meta name="DC.Title" content="Philosophie&amp;Theologie">

  <meta name="DC.Creator" content="Redaktion kath.de">

  <meta name="DC.Contributor" content="J&uuml;rgen Pelzer">

  <meta name="DC.Rights" content="kath.de">

  <meta name="DC.Publisher" content="kath.de">

  <meta name="DC.Date" content="2006-00-01">

  <meta name="DC.Description" content="Lexikon &uuml;ber Philosophie&amp;Theologie">

  <meta name="DC.Language" content="de">

  <meta name="DC.Type" content="Text">

  <meta name="DC.Format" content="text/html">

  <meta name="DC.Identifier" content="http://www.kath.de/lexikon/philosophie_theologie/">

  <meta name="keywords" content="Freiheit, Stimme Gottes, Gewissen, Tiefenpsychologie, Ueber-Ich, Newman, Schelling, Johann Gottlieb Fichte" lang="de">

</head>
<body leftmargin="6" topmargin="6" style="background-color: rgb(255, 255, 255);" marginheight="6" marginwidth="6">

<table border="0" cellpadding="6" cellspacing="0" width="100%">

  <tbody>

    <tr>

      <td align="left" valign="top" width="100">
      <table border="0" cellpadding="0" cellspacing="0" width="216">

        <tbody>

          <tr align="left" valign="top">

            <td width="8"><img src="boxtopleftcorner.gif" alt="" height="8" width="8"></td>

            <td background="boxtop.gif" width="200"><img src="boxtop.gif" alt="" height="8" width="8"></td>

            <td width="8"><img src="boxtoprightcorner.gif" alt="" height="8" width="8"></td>

          </tr>

          <tr align="left" valign="top">

            <td background="boxtopleft.gif"><img src="boxtopleft.gif" alt="" height="8" width="8"></td>

            <td bgcolor="#e2e2e2"><b>Philosophie&amp;Theologie</b></td>

            <td background="boxtopright.gif"><img src="boxtopright.gif" alt="" height="8" width="8"></td>

          </tr>

          <tr align="left" valign="top">

            <td><img src="boxdividerleft.gif" alt="" height="13" width="8"></td>

            <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" height="13" width="8"></td>

            <td><img src="boxdividerright.gif" alt="" height="13" width="8"></td>

          </tr>

          <tr align="left" valign="top">

            <td background="boxleft.gif"><img src="boxleft.gif" alt="" height="8" width="8"></td>

            <td><?php include("logo.html"); ?> </td>

            <td background="boxright.gif"><img src="boxright.gif" alt="" height="8" width="8"></td>

          </tr>

          <tr align="left" valign="top">

            <td><img src="boxbottomleft.gif" alt="" height="8" width="8"></td>

            <td background="boxbottom.gif"><img src="boxbottom.gif" alt="" height="8" width="8"></td>

            <td><img src="boxbottomright.gif" alt="" height="8" width="8"></td>

          </tr>

        </tbody>
      </table>

      <br>

      <table border="0" cellpadding="0" cellspacing="0" width="216">

        <tbody>

          <tr align="left" valign="top">

            <td width="8"><img src="boxtopleftcorner.gif" alt="" height="8" width="8"></td>

            <td background="boxtop.gif" width="200"><img src="boxtop.gif" alt="" height="8" width="8"></td>

            <td width="8"><img src="boxtoprightcorner.gif" alt="" height="8" width="8"></td>

          </tr>

          <tr align="left" valign="top">

            <td background="boxtopleft.gif"><img src="boxtopleft.gif" alt="" height="8" width="8"></td>

            <td bgcolor="#e2e2e2"><strong>Begriff
anklicken</strong></td>

            <td background="boxtopright.gif"><img src="boxtopright.gif" alt="" height="8" width="8"></td>

          </tr>

          <tr align="left" valign="top">

            <td><img src="boxdividerleft.gif" alt="" height="13" width="8"></td>

            <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" height="13" width="8"></td>

            <td><img src="boxdividerright.gif" alt="" height="13" width="8"></td>

          </tr>

          <tr align="left" valign="top">

            <td background="boxleft.gif"><img src="boxleft.gif" alt="" height="8" width="8"></td>

            <td class="V10"><?php include("az.html"); ?> </td>

            <td background="boxright.gif"><img src="boxright.gif" alt="" height="8" width="8"></td>

          </tr>

          <tr align="left" valign="top">

            <td><img src="boxbottomleft.gif" alt="" height="8" width="8"></td>

            <td background="boxbottom.gif"><img src="boxbottom.gif" alt="" height="8" width="8"></td>

            <td><img src="boxbottomright.gif" alt="" height="8" width="8"></td>

          </tr>

        </tbody>
      </table>

      </td>

      <td rowspan="2" valign="top">
      <table border="0" cellpadding="0" cellspacing="0" width="100%">

        <tbody>

          <tr align="left" valign="top">

            <td width="8"><img alt="" src="boxtopleftcorner.gif" height="8" width="8"></td>

            <td background="boxtop.gif"><img alt="" src="boxtop.gif" height="8" width="8"></td>

            <td width="8"><img alt="" src="boxtoprightcorner.gif" height="8" width="8"></td>

          </tr>

          <tr align="left" valign="top">

            <td background="boxtopleft.gif"><img alt="" src="boxtopleft.gif" height="8" width="8"></td>

            <td bgcolor="#e2e2e2">
            <h1><font face="Arial, Helvetica, sans-serif">Das
B&ouml;se - der Drache<br>

            </font></h1>

            </td>

            <td background="boxtopright.gif"><img alt="" src="boxtopright.gif" height="8" width="8"></td>

          </tr>

          <tr align="left" valign="top">

            <td><img alt="" src="boxdividerleft.gif" height="13" width="8"></td>

            <td background="boxdivider.gif"><img alt="" src="boxdivider.gif" height="13" width="8"></td>

            <td><img alt="" src="boxdividerright.gif" height="13" width="8"></td>

          </tr>

          <tr align="left" valign="top">

            <td background="boxleft.gif"><img alt="" src="boxleft.gif" height="8" width="8"></td>

            <td style="font-family: Helvetica,Arial,sans-serif;" class="L12">



            <p class="MsoNormal"><o:p></o:p>Dem Menschen zwingt sich
die Frage auf, woher das B&ouml;se
kommt. Denn es begegnet ihm auf jeden Fall, in Form von &uuml;bler
Nachrede, dass er
belogen, bestohlen wird, dass ihm Gewalt angetan wird. Und es sind
Gewitterst&uuml;rme, Erdbeben, Tsunamis, die sein Leben
beeintr&auml;chtigen. Es ist also
zuerst die Realit&auml;t des B&ouml;sen, mit der der einzelne
konfrontiert wird. Aus dieser
Konfrontation w&auml;chst die Frage nach Woher und Warum. Die
Antwortm&ouml;glichkeiten
sind gro&szlig;. Prinzipiell kann man die Ursache des
B&ouml;sen bei sich suchen oder es
als Macht erfahren, die von au&szlig;en in das eigene Leben
eindringt. F&uuml;r das B&ouml;se
haben die Menschen fr&uuml;h Bilder gefunden. Eines der
eindr&uuml;cklichsten ist der
Drache.</p>



            <p class="MsoNormal"><b style=""><o:p></o:p>Lindwurm,
Leviathan, Siegfried
            <o:p></o:p></b><br>
Auf der ganzen Welt
&uuml;bt das Bild des Drachen Faszination aus
und wirkt zugleich bedrohlich. Er wird als Bild des B&ouml;sen
gebraucht. In der
Bibel wird der Drache im Meer lokalisiert und zugleich als Himmelswesen
gesehen. </p>

            <p class="MsoNormal">Diese Vorstellungswelt
ist &auml;lter als die j&uuml;dische Theologie.
Zudem finden wir den Drachen nicht nur in unserem Kulturkreis. Unsere
Vorfahren, vor allem in der Zeit der Romanik, haben sich mit dem
B&ouml;sen im Bild
des Drachen auseinandergesetzt. Wir finden die Darstellungen an den
T&uuml;ren und
bis in die Gotik hinein sind die Wasserspeier als Drachenk&ouml;pfe
gestaltet. Der
Drache speit Feuer und wird daher mit dem Ausbruch eines Vulkans
verbunden. </p>

            <p class="MsoNormal">Auch die
Milchstra&szlig;e verk&ouml;rpert den Drachen. Es sind die vom
Menschen nicht beherrschbaren Kr&auml;fte, die sich in dieser
Tiergestalt verdichten.
Manchmal gibt es eine Eruption des B&ouml;sen, meist
w&auml;chst es unbemerkt, denn die
Drachenz&auml;hne lassen das B&ouml;se immer wieder
nachwachsen. Der sexuelle Missbrauch
von Kindern ist eine solche nachwachsende Saat, die man kaum fassen
kann. </p>



















            <p class="MsoNormal"><o:p></o:p><span style="font-weight: bold;">Das Krokodil wird mit
Attributen des Drachen dargestellt:</span><span style="font-size: 12pt; color: windowtext;"><br>
            <cite>&bdquo;So
k&uuml;hn ist keiner, es zu reizen; wer k&ouml;nnte ihm wohl
trotzen? Wer begegnete ihm und bliebe heil? Unter dem ganzen Himmel
gibt es so
einen nicht. Ich will nicht schweigen von seinen Gliedern, wie
gro&szlig; und
m&auml;chtig, wie wohl geschaffen es ist.<o:p></o:p> Wer
&ouml;ffnet die H&uuml;lle seines Kleides, wer dringt in seinen
Doppelpanzer ein?<o:p></o:p><br>
Wer
&ouml;ffnet die Tore seines Mauls? Rings um seine Z&auml;hne
lagert
Schrecken.<o:p></o:p></cite></span><cite><a name="a7"><span style="font-size: 12pt; color: windowtext;"> Reihen
von Schilden sind sein R&uuml;cken, verschlossen mit Siegel
aus Kieselstein.</span></a><span style="font-size: 12pt; color: windowtext;"><o:p></o:p></span><a name="a8"><span style="font-size: 12pt; color: windowtext;"> Einer
reiht sich an den andern, kein Lufthauch dringt
zwischen ihnen durch.</span></a><span style="font-size: 12pt; color: windowtext;"><o:p></o:p></span><a name="a9"><span style="font-size: 12pt; color: windowtext;">Fest
haftet jeder an dem andern, sie sind verklammert, l&ouml;sen
sich nicht.</span></a><a name="a10"></a><span style=""><span style="font-size: 12pt; color: windowtext;">
Sein Niesen l&auml;sst Licht aufleuchten; seine Augen sind wie des
Fr&uuml;hrots Wimpern.</span></span><span style="font-size: 12pt; color: windowtext;">
            <a name="a11">Aus seinem Maul fahren brennende
Fackeln, feurige Funken schie&szlig;en
hervor.</a> Rauch dampft aus seinen N&uuml;stern, wie aus
kochendem, hei&szlig;em Topf. Sein
Atem entflammt gl&uuml;hende Kohlen, eine Flamme schl&auml;gt
aus seinem Maul hervor.<o:p></o:p> St&auml;rke
wohnt in seinem Nacken, vor ihm her h&uuml;pft bange
Furcht.<a name="a15"> Straff liegt seines Wanstes Fleisch,
wie angegossen,
unbewegt.</a> <a name="a16">Sein Herz ist fest wie
Stein, fest wie der untere
M&uuml;hlstein.</a> <a name="a17">Erhebt es
sich, erschrecken selbst die Starken; vor
Schrecken wissen sie nicht aus noch ein. </a><o:p></o:p></span><a name="a18"><span style="font-size: 12pt; color: windowtext;">Trifft
man es, kein Schwert h&auml;lt stand, nicht Lanze noch
Geschoss und Pfeil.</span></a><span style="font-size: 12pt; color: windowtext;"><o:p></o:p></span><a name="a19"><span style="font-size: 12pt; color: windowtext;"> Eisen
achtet es wie Stroh, Bronze wie morsch gewordenes Holz.</span></a><span style="font-size: 12pt; color: windowtext;"><o:p></o:p></span><a name="a20"><span style="font-size: 12pt; color: windowtext;"> Kein
Bogenpfeil wird es verjagen, in Stoppeln verwandeln sich
ihm die Steine der Schleuder.</span></a><span style="font-size: 12pt; color: windowtext;"><o:p></o:p></span><a name="a21"><span style="font-size: 12pt; color: windowtext;"> Wie
Stoppeln d&uuml;nkt ihm die Keule, es lacht nur &uuml;ber
Schwertergerassel.</span></a><span style="font-size: 12pt; color: windowtext;"><o:p></o:p></span><a name="a22"><span style="font-size: 12pt; color: windowtext;"> Sein
Unteres sind Scherbenspitzen; ein Dreschbrett breitet es
&uuml;ber den Schlamm.</span></a><span style="font-size: 12pt; color: windowtext;"><o:p></o:p></span><a name="a23"><span style="font-size: 12pt; color: windowtext;"> Die
Tiefe l&auml;sst es brodeln wie den Kessel, macht das Meer zu
einem Salbentopf.</span></a></cite><span style="font-size: 12pt; color: windowtext;"><cite><o:p></o:p> Es
hinterl&auml;sst eine leuchtende Spur; man meint, die Flut sei
Greisenhaar.<o:p></o:p> Auf
Erden gibt es seinesgleichen nicht, dazu geschaffen, um
sich nie zu f&uuml;rchten.<o:p></o:p> Alles
Hohe blickt es an; K&ouml;nig ist es &uuml;ber alle stolzen
Tiere."</cite> <o:p></o:p></span><br>
Hiob kap. 41,2-26</p>


            <p class="MsoNormal"><o:p></o:p>In der Gestalt des
Krokodils wird der Drache als Gesch&ouml;pf
Gottes beschrieben. Er ist furchterregend, die Gr&ouml;&szlig;e
und St&auml;rke Gottes scheint
in dem Tier auf. Anders der Drache, von dem das letzte Buch der Bibel,
die Offenbarung
des Johannes spricht. Der Drache ist der Feind der Frau. Er bewohnt den
Himmel.
von hier leitet sich die Identifikation von Drache und
Milchstra&szlig;e her. In der
Frau wird die Kirche dargestellt, der Drache will sie vernichten. In
diesem
Bild verdichten sich die Erfahrungen der ersten Christenverfolgung.</p>


            <p class="MsoNormal" style=""><o:p></o:p><b><span style="font-size: 12pt; color: windowtext;">Die
Frau und der Drache</span></b><span style="font-size: 12pt; color: windowtext;"><br>

            <cite><b>&bdquo;</b>Dann erschien ein
gro&szlig;es Zeichen am Himmel: eine Frau, mit der Sonne
bekleidet; der Mond war unter ihren F&uuml;&szlig;en und ein
Kranz von zw&ouml;lf Sternen auf
ihrem Haupt. Sie war schwanger und schrie vor Schmerz in ihren
Geburtswehen. <br>

Ein anderes Zeichen erschien am Himmel: ein Drache, gro&szlig; und
feuerrot, mit
sieben K&ouml;pfen und zehn H&ouml;rnern und mit sieben
Diademen auf seinen K&ouml;pfen.<br>

Sein Schwanz fegte ein Drittel der Sterne vom Himmel und warf sie auf
die Erde
herab. Der Drache stand vor der Frau, die geb&auml;ren sollte; er
wollte ihr Kind
verschlingen, sobald es geboren war. Und sie gebar ein Kind, einen
Sohn, der
&uuml;ber alle V&ouml;lker mit eisernem Zepter herrschen wird.
Und ihr Kind wurde zu Gott
und zu seinem Thron entr&uuml;ckt. Die Frau aber floh in die
W&uuml;ste, wo Gott ihr
einen Zufluchtsort geschaffen hatte; dort wird man sie mit Nahrung
versorgen,
zw&ouml;lfhundertsechzig Tage lang."</cite> <o:p></o:p></span></p>

            <p class="MsoNormal" style=""><b><span style="font-size: 12pt; color: windowtext;">Der
Sturz des Drachen</span></b><span style="font-size: 12pt; color: windowtext;"><br>
            <cite>
&bdquo;Da entbrannte im Himmel ein Kampf; Michael und seine Engel
erhoben sich, um
mit dem Drachen zu k&auml;mpfen. Der Drache und seine Engel
k&auml;mpften, aber sie
konnten sich nicht halten und sie verloren ihren Platz im Himmel. Er
wurde
gest&uuml;rzt, der gro&szlig;e Drache, die alte Schlange, die
Teufel oder Satan hei&szlig;t und
die ganze Welt verf&uuml;hrt; der Drache wurde auf die Erde
gest&uuml;rzt und mit ihm
wurden seine Engel hinabgeworfen.<br>

Da h&ouml;rte ich eine laute Stimme im Himmel rufen: Jetzt ist er
da, der rettende
Sieg, die Macht und die Herrschaft unseres Gottes und die Vollmacht
seines
Gesalbten; denn gest&uuml;rzt wurde der Ankl&auml;ger unserer
Br&uuml;der, der sie bei Tag und
bei Nacht vor unserem Gott verklagte. Sie haben ihn besiegt durch das
Blut des
Lammes und durch ihr Wort und Zeugnis; sie hielten ihr Leben nicht
fest, bis
hinein in den Tod. Darum jubelt ihr Himmel und alle, die darin wohnen.
Weh aber
euch, Land und Meer! Denn der Teufel ist zu euch hinabgekommen; seine
Wut ist gro&szlig;,
weil er wei&szlig;, dass ihm nur noch eine kurze Frist
bleibt.&ldquo;</cite><br>
Offenbarung 12, 1-12<o:p></o:p></span></p>

            <p class="MsoNormal" style=""><span style="color: windowtext;">Von
dieser Vision leiten sich die Darstellungen des Erzengels Michael her,
der den
Drachen besiegen kann. Dieser Kampf findet in Regionen oberhalb der
Erde ab. Der
Drache ist auch hier noch ein Gegner, der Gott anerkennt, denn er
versucht den
Gl&auml;ubigen dadurch zu schaden, dass er als &bdquo;Vater der
L&uuml;ge&ldquo; diese vor Gott
anschw&auml;rzt. <o:p></o:p></span></p>



            <p class="MsoNormal"><b><span style="color: windowtext;">Der
Kampf des Drachen gegen die Kirche<br>

            </span></b><span style="color: windowtext;">Weil
der Drache aus dem Himmel
gest&uuml;rzt wurde, verfolgt er die Kirche auf der Erde. Der
Drache wird zur
Schlange, so wie der Versucher in der Geschichte vom
S&uuml;ndenfall beschrieben
wurde.<br>
            </span><b><span style="font-size: 12pt; color: windowtext;"></span></b><cite><b><span style="font-size: 12pt; color: windowtext;"><br>
&bdquo;</span></b></cite><span style="font-size: 12pt; color: windowtext;"><cite>Als
der Drache erkannte, dass er auf die Erde gest&uuml;rzt war,
verfolgte er die Frau, die den Sohn geboren hatte. Aber der Frau wurden
die
beiden Fl&uuml;gel des gro&szlig;en Adlers gegeben, damit sie
in die W&uuml;ste an ihren Ort
fliegen konnte. Dort ist sie vor der Schlange sicher und wird eine Zeit
und
zwei Zeiten und eine halbe Zeit lang ern&auml;hrt.<o:p></o:p><br>
Die
Schlange spie einen Strom von Wasser aus ihrem Rachen
hinter der Frau her, damit sie von den Fluten fortgerissen werde. Aber
die Erde
kam der Frau zu Hilfe; sie &ouml;ffnete sich und verschlang den
Strom, den der
Drache aus seinem Rachen gespien hatte. Da geriet der Drache in Zorn
&uuml;ber die
Frau und er ging fort, um Krieg zu f&uuml;hren mit ihren
&uuml;brigen Nachkommen, die den
Geboten Gottes gehorchen und an dem Zeugnis f&uuml;r Jesus
festhalten."</cite> <o:p></o:p><br>
Offenbarung
12, 1-17<o:p></o:p></span></p>






            <p class="MsoNormal"><span style="font-size: 12pt; color: windowtext;"><o:p></o:p></span><span style="color: windowtext;">Das Bild des Wassers in
Verbindung mit dem Drachen
findet sich an vielen mittelalterlichen Kathedralen. Dort sind die
Wasserspeier
am Dach als Drachenk&ouml;pfe gestaltet.<o:p></o:p></span><br>
            <o:p></o:p>Bilder vom Drachen finden
sich auch in den Psalmen. Gott
besiegt die Chaosm&auml;chte, er ist st&auml;rker als der
Leviathan, ein Meeresdrache:<br>
            <cite><a name="a12"><b><span style="font-size: 12pt; color: windowtext;">&bdquo;</span></b></a><span style=""><span style="font-size: 12pt; color: windowtext;">Doch
Gott ist mein K&ouml;nig von alters her, Taten des Heils
vollbringt er auf Erden.</span></span><a name="a13"></a><span style=""><span style="font-size: 12pt; color: windowtext;">
Mit deiner Macht hast du das Meer zerspalten, die H&auml;upter
der Drachen &uuml;ber den Wassern zerschmettert.</span></span><a name="a14"></a></cite><span style=""><span style="font-size: 12pt; color: windowtext;"><cite>
Du hast die K&ouml;pfe des Leviathan zermalmt, ihn zum
Fra&szlig;
gegeben den Ungeheuern der See.&ldquo;</cite> </span></span><span style="font-size: 12pt; color: windowtext;"><o:p></o:p></span><br>
Psalm 74</p>








            <p class="MsoNormal"><o:p>&nbsp;</o:p>Im Psalm 104 wird der
Leviathan als Gesch&ouml;pf beschrieben,
mit dem Gott sogar spielt:<a name="a24"><b><span style="font-size: 12pt; color: windowtext;"></span></b></a><br>
            <cite><a name="a24"><b><span style="font-size: 12pt; color: windowtext;">&bdquo;</span></b></a><span style=""><span style="font-size: 12pt; color: windowtext;">Herr,
wie zahlreich sind deine Werke! Mit Weisheit hast du
sie alle gemacht, die Erde ist voll von deinen Gesch&ouml;pfen.</span></span><span style="font-size: 12pt; color: windowtext;"><o:p></o:p></span><br>
            <a name="a25"><span style="font-size: 12pt; color: windowtext;">Da
ist das Meer, so gro&szlig; und weit, darin ein Gewimmel ohne
Zahl: kleine und gro&szlig;e Tiere.</span></a><span style="font-size: 12pt; color: windowtext;"><o:p></o:p></span><br>
            <a name="a26"><span style="font-size: 12pt; color: windowtext;">Dort
ziehen die Schiffe dahin, auch der Leviathan, den du
geformt hast, um mit ihm zu spielen."</span></a></cite><span style="font-size: 12pt; color: windowtext;"><o:p></o:p></span><br>
Psalm 104, 24-26<o:p></o:p></p>



            <p class="MsoNormal"><o:p></o:p><b style="">Die
Schlange und der
Apfel<o:p></o:p></b><br>
In der Schlange kehrt der
Drache in verkleinerter Form als
Anstifter zum B&ouml;sen zur&uuml;ck. Wie ist aber die
Geschichte vom Apfel zu verstehen?
Bereits im 3. Kapitel der Bibel liest man von dem S&uuml;ndenfall.
Die Erz&auml;hlung
soll begr&uuml;nden, warum der Mensch au&szlig;erhalb des
Paradieses, jenseits von Eden
leben und warum er sterben muss. Wir gehen im
j&uuml;disch-christlich gepr&auml;gten
Kulturkreis davon aus, dass Eva verbotener Weise von einen Baum
gegessen hat,
darauf hin wurden die Menschen aus dem Garten Eden vertrieben. Aber was
ist
geschehen, als beide in die Frucht bissen? Sie erkannten, dass sie
nackt waren.
Sie erkannten ihre Sexualit&auml;t und hatten damit ihre Unschuld,
ihre kindliche
Unbek&uuml;mmertheit verloren. Daraus folgt deshalb der Tod, weil
die
Elterngeneration den eignen Kindern Platz machen muss. Freud wird dann,
nach
den Erfahrungen des 1. Weltkriegs, neben den Sexualtrieb etwas
unverbunden den
Todestrieb setzen. Die Bibel zeigte bereits, warum Sexualit&auml;t
und Tod logisch
zusammenh&auml;ngen.<o:p></o:p></p>
            <p class="MsoNormal"><o:p></o:p>Die eigentliche
Erkl&auml;rung des B&ouml;sen findet sich ein Kapitel weiter,
das den <a href="boese_brudermord.php">Brudermord</a> zum Thema hat.</p>


            <p class="MsoNormal"><o:p></o:p>Eckhard Bieger S.J.</p>

            <p><br>

&copy; <a href="http://www.kath.de">www.kath.de</a></p>

            </td>

            <td background="boxright.gif"><img alt="" src="boxright.gif" height="8" width="8"></td>

          </tr>

          <tr align="left" valign="top">

            <td><img alt="" src="boxbottomleft.gif" height="8" width="8"></td>

            <td style="font-family: Helvetica,Arial,sans-serif;" background="boxbottom.gif"><img alt="" src="boxbottom.gif" height="8" width="8"></td>

            <td><img alt="" src="boxbottomright.gif" height="8" width="8"></td>

          </tr>

        </tbody>
      </table>

      </td>

      <td style="vertical-align: top;">
      <script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
google_ad_width = 336;
google_ad_height = 280;
google_ad_format = "336x280_as";
google_ad_type = "text_image";
//2006-10-23: Lexikon Symbole
google_ad_channel = "8495253357";
//--></script>
      <script type="text/javascript" src="show_ads.js">
      </script></td>

    </tr>

    <tr>

      <td align="left" valign="top">&nbsp; </td>

    </tr>

  </tbody>
</table>

</body>
</html>
