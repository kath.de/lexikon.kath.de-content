<HTML><HEAD><TITLE>Realpr&auml;senz</TITLE>
<META http-equiv=Content-Type content="text/html; charset=iso-8859-1"><LINK 
title=fonts href="kaltefleiter.css" type=text/css 
rel=stylesheet>
<META content="MSHTML 5.50.4134.600" name=GENERATOR></HEAD>
<BODY bgColor=#ffffff leftMargin=6 topMargin=6 marginheight="6" marginwidth="6">
<TABLE cellSpacing=0 cellPadding=6 width="100%" border=0>
  <TBODY>
  <TR>
    <TD vAlign=top align=left width=100> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b>Philosophie&amp;Theologie</b></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td> <?php include("logo.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10"> <?php include("az.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </TD>
    <TD vAlign=top rowSpan=2>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
        <TBODY>
        <TR vAlign=top>
          <TD width=8 align="left"><IMG height=8 alt="" 
            src="boxtopleftcorner.gif" width=8></TD>
          <TD align="left" background=boxtop.gif><IMG height=8 alt="" 
            src="boxtop.gif" width=8></TD>
          <TD width=8 align="left"><IMG height=8 alt="" 
            src="boxtoprightcorner.gif" width=8></TD>
        </TR>
        <TR vAlign=top>
          <TD align="left" background=boxtopleft.gif><IMG height=8 alt="" 
            src="boxtopleft.gif" width=8></TD>
          <TD align="left" bgColor=#e2e2e2> 
            <H1><font face="Arial, Helvetica, sans-serif">Realpr&auml;senz</font></H1>
          </TD>
          <TD align="left" background=boxtopright.gif><IMG height=8 
            alt="" src="boxtopright.gif" width=8></TD>
        </TR>
        <TR vAlign=top>
          <TD align="left"><IMG height=13 alt="" src="boxdividerleft.gif" 
            width=8></TD>
          <TD align="left" background=boxdivider.gif><IMG height=13 
            alt="" src="boxdivider.gif" width=8></TD>
          <TD align="left"><IMG height=13 alt="" 
            src="boxdividerright.gif" width=8></TD>
        </TR>
        <TR vAlign=top>
          <TD align="left" background=boxleft.gif><IMG height=8 alt="" 
            src="boxleft.gif" width=8></TD>
          <TD class=L12>
            <P><STRONG><font face="Arial, Helvetica, sans-serif">Die reale Gegenwart
                  Jesu in Brot und Wein<br>
            </font></STRONG> </P>
            <P><font face="Arial, Helvetica, sans-serif">Realpr&auml;senz ist
              das lateinische Wort f&uuml;r &#8222;wirkliche Gegenwart&#8220;.
              Dieser Begriff ist f&uuml;r eine theologische Fragestellung entwickelt
              worden: Wie ist Jesus Christus in Brot und Wein gegenw&auml;rtig,
              wenn die Christen sein Ged&auml;chtnismahl feiern? <br>
              Im Mittelalter wurde um diese Frage heftig gerungen. Man wollte
              genauer beschreiben k&ouml;nnen, was das f&uuml;r ein Brot ist,
              das die Christen beim Kommuniongang empfangen und was sie verehren,
              wenn die Hostien im Tabernakel aufbewahrt werden.<br>
              Jesus hat selbst im <a href="http://www.kath.de/Kirchenjahr/gruendonnerstag.php">Abendmahlssaal</a> das Brot mit den Worten gebrochen: &#8222;Dies
              ist mein Leib&#8220; und den Kelch mit der Erkl&auml;rung herum
              gereicht: &#8222;Dies ist mein Blut&#8220;. [Mt 26,26-28;Mk 14,22-24;Lk
              22,19f;1 Kor 11,23-26] Wenn diese Worte vom Priester bei der Eucharistiefeier
              gesprochen werden, dann spricht man von Wandlung. Was geschieht
              in der Wandlung? Etwas &auml;ndert sich. Offensichtlich werden
              das Brot und der Wein gewandelt. Wie kann man sich das aber vorstellen,
              denn Brot und Wein ver&auml;ndern sich nach au&szlig;en nicht,
              sie schmecken wie Brot und wie Wein. Wenn aber Brot und Wein nicht
              nur Hinweise auf den Leib und das Blut Jesu sein sollen, mu&szlig; man
              die offensichtliche Beobachtung mit der Aussage der S&auml;tze
              Jesu in &Uuml;bereinstimmung bringen &#8222;Dies ist mein Leib&#8220;, &#8222;Dies
              ist mein Blut&#8220;.<br>
              Wenn Jesus Christus wirklich und wesenhaft und nicht rein gedacht
              unter den Gestalten von Brot und Wein gegenw&auml;rtig sein soll,
              gen&uuml;gt es nicht zu sagen, da&szlig; etwas in die Gaben hineingekommen
              (Impanation) und bzw. etwas hinzugekommen ist (Konsubstantiation),
              sondern da&szlig; Brot und Wein in ihrem Wesen verwandelt sind.
              Daf&uuml;r wurde der Begriff &#8222;Transsubstantiation&#8220; als
              geeignet gesehen. Wenn sich das Wesen der Gaben ver&auml;ndert,
              also eine andere Substanz wird, dann ist Jesus Christus in Brot
              und Wein wirklich gegenw&auml;rtig, real pr&auml;sent. Die Substanz
              selbst ist verwandelt, daher Trans-Substantiation. <br>
              Die wirkliche <a href="gegenwart_jesu.php">Gegenwart</a> hei&szlig;t dann, da&szlig; Jesus in jeder
              Hostie und jedem Schluck Wein unzertrennt gegenw&auml;rtig ist.
              Wenn die Gl&auml;ubigen das Brot essen und den Wein trinken, wird
              Jesus nicht zerkaut und auch nicht verdaut, sondern von den Gl&auml;ubigen &#8222;empfangen&#8220;,
              aufgenommen. Das zeigt, da&szlig; man das Physische, das Brot und
              den Wein, nicht physikalisch und chemisch bis zum Ende durchdenken
              kann, sondern da&szlig; die Gegenwart Jesu sich physikalischen
              Begriffen entzieht. Deshalb ist der Begriff &#8222;Transubstantiation&#8220; ein
              Begriff, mit dem die neue Realit&auml;t von Brot und Wein gedacht
              werden kann. Es gibt auch neuere Vorstellungsmodelle, die heutige
              Denkkategorien zur Verf&uuml;gung stellen, z.B. Transfinalisation.
              F&uuml;r die Teilnahme an der Eucharistiefeier ist daher nur notwendig,
              da&szlig; man eine wirkliche Gegenwart Jesu in den Gestalten von
              Brot und Wein annimmt, nicht aber eine bestimmten Vorstellung folgen
              mu&szlig;, wie diese Gegenwart in Beziehung zu der bleibenden Gestalt
              von Brot und Wein stehen.<br>
              Jesus Christus selbst ist der Handelnde, er ist Gabe und Geber
              zugleich. Die Kommunion hei&szlig;t Begegnung mit ihm.<br>
              Nach dieser Lehre ist Jesus Christus unter den konsekrierten Mahlgaben
              auch &uuml;ber die Eucharistiefeier hinaus wirklich gegenw&auml;rtig
              (im Gegensatz zum Glaubensverst&auml;ndnis Luthers, das die wirkliche
              Gegenwart Christi auf den Augenblick des Empfanges der eucharistischen
              Gaben einschr&auml;nkt). Deshalb wird das eucharistische Brot an
              einem besonderen Ort (tabernaculum, Tabernakel) einer Kirche oder
              Kapelle zur Speisung der Kranken (Krankenkommunion) und Todesgef&auml;hrdeten
              aufbewahrt, da das eigentliche Sakrament in der Sterbestunde nicht
              die Krankensalbung (unctio infirmorum) darstellt, sondern der Kommunionempfang
              (viaticum, Wegzehrung). Dar&uuml;ber hinaus wird das eucharistische
              Brot im Tabernakel auch zur stillen Verehrung und Anbetung aufbewahrt.
              Deshalb ist es katholischer Brauch, dass sich die Gl&auml;ubigen
              beim Passieren einer Kirche mit dem Kreuz bezeichnen und beim Betreten
              einer Kirche ihre Knie in Richtung des Tabernakels beugen. Auch
              das sogenannte &#8222;ewige Licht&#8220;, eine in unmittelbarer
              N&auml;he zum Tabernakel fortw&auml;hrend (r&ouml;tlich) brennende &Ouml;llampe,
              verweist in katholischen Kirchengeb&auml;uden auf die Pr&auml;senz
              des in den eucharistischen Gaben gegenw&auml;rtigen Herrn. Das
              Herausnehmen aus dem Tabernakel und Einsetzen des eucharistischen
              Brotes (Hostie) in ein Zeigegef&auml;&szlig; (Monstranz) nennt
              man &#8222;Aussetzung des Allerheiligsten&#8220; (Expositio). Durch
              diese Zur-Schau-Stellung k&ouml;nnen die Gl&auml;ubigen den eucharistischen
              Herrn anbeten und verehren. Der &#8222;eucharistische Segen&#8220;,
              in dem mit der Monstranz ein Kreuzzeichen gemacht wird, schlie&szlig;t
              eine solche Anbetungsandacht feierlich ab. <br>
              Im Mittelalter, zur Zeit der Gotik, entwickelte sich eine starke
              Verehrung des im eucharistischen Brot gegenw&auml;rtigen Herrn.
              Ausdruck dieser Verehrung ist das Fronleichnamsfest. Es wurde von
              der Reformation nicht weiter gef&uuml;hrt, so da&szlig; es seitdem
              als Besonderheit der katholischen Fr&ouml;mmigkeit gilt. Fronleichnam,
              von mittelhochdeutsch fron: herrlich, heer, Lichnam bedeutet entgegen
              dem heutigen Sprachgebrauch den lebendigen Leib. Es wird zehn Tage
              nach Pfingsten, je nach dem Ostertermin im Fr&uuml;hsommer Ende
              Mai bis Mitte Juni gefeiert. An diesem Festtag wird das eucharistische
              Brot in einer Monstranz in einer Prozession durch den Stadtteil
              und auf dem Land auch durch die Felder getragen. (<a href="http://www.kath.de/Kirchenjahr/fronleichnam.php">Fronleichnamsprozession</a>).
              Singend und betend schreitet die Gemeinde auf dem von Blumen(-teppichen),
              frischem Gr&uuml;n, beflaggten H&auml;usern und traditionell vier
              Stationsalt&auml;ren (Himmelsrichtungen) ges&auml;umten Prozessionsweg
              und bittet um Segen f&uuml;r die Menschen und die Fluren. </font></P>            
            <p><font face="Arial, Helvetica, sans-serif">&#8222;Wir glauben, dass in der Weise, wie Brot und Wein vom Herrn
              beim letzten Abendmahl konsekriert und in Seinen Leib und Sein
              Blut verwandelt worden sind, die Er f&uuml;r uns am Kreuze geopfert
              hat, auch Brot und Wein, wenn sie vom Priester konsekriert werden,
              in den Leib und das Blut Christi verwandelt werden, der glorreich
              in den Himmel aufgefahren ist. Und wir glauben, dass die geheimnisvolle
              Gegenwart des Herrn unter den &auml;u&szlig;eren Gestalten, die
              f&uuml;r unsere Sinne in derselben Weise wie vorher fortzubestehen
              scheinen, eine wahre, wirkliche und wesentliche Gegenwart ist.&#8220; <br>
              Paul VI., Credo des Gottesvolkes, Feierliches Glaubensbekenntnis
              vom 30. Juni 1968, Nr. 24-26, AAS 60 (1968), S. 442-443<br>
              <br>
&#8222;
              Es geh&ouml;rt zum christ- katholischen Glauben, dass Jesus Christus
              mit Gottheit und Menschheit unter den eucharistischen Gestalten
              wahrhaft gegenw&auml;rtig ist. Gewiss ist diese Gegenwart unter
              den Symbolen menschlicher Nahrung ausgerichtet auf den wirklichen
              Empfang und Genuss dieser eucharistischen Speise. Aber das &auml;ndert
              nichts daran, dass in dieser Speise Jesus Christus mit Gottheit
              und Menschheit nicht nur gegenw&auml;rtig ist, indem er empfangen
              wird, sondern zuvor gegenw&auml;rtig ist, damit er leibhaftig empfangen
              werden k&ouml;nne. Und darum kann der katholische Christ Jesus,
              das g&ouml;ttliche Unterpfand seines Heiles, unter diesen eucharistischen
              Zeichen anbeten. Solche Anbetung ist im Vergleich zum wirklichen
              Empfang des himmlischen Brotes zwar nicht der H&ouml;hepunkt des
              sakramentalen Geschehens, wohl aber eine legitime Konsequenz aus
              dem katholischen Glauben an die wahre Gegenwart des Herrn im Sakrament...Wir
              katholischen Christen wollen in Gemeinschaft und als Einzelne auf
              das Zeichen der Gegenwart dessen blicken, der uns geliebt hat und
              sich f&uuml;r uns dahingegeben hat. Es sollte f&uuml;r uns nicht
              fremd sein, auch einmal in privatem Gebet vor dem Herrn zu knien,
              der uns erl&ouml;st hat.&#8220;<br>
              Karl Rahner, Geist und Leben 54 (1981), 188-191</font></p>
            <p><font face="Arial, Helvetica, sans-serif">&quot;Seit mehr als einem halben Jahrhundert sind meine Augen
              jeden Tag auf die wei&szlig;e Hostie gerichtet, in der Zeit und
              Raum in gewisser Weise zusammenfallen und in der das Drama von
              Golgota lebendig gegenw&auml;rtig wird.&quot;<br>
              Johannes Paul II.,
              Ecclesia de eucharistia vom 17. April 2003, Nr. 52: AAS 95 (2003)
              468</font></p>
            <p><font face="Arial, Helvetica, sans-serif"> &#8222; Empfangt, was
                ihr seht: Leib Christi. Werdet, was ihr empfangt: Leib Christi.&#8220;<br>
              Augustinus, Sermo 272 (PL 38) Corpus Augustinianum Gissense a
              C. Mayer editum</font></p>
            <p><font face="Arial, Helvetica, sans-serif"><br>
  Peter M&uuml;nch </font><br>
            </p>
            <P>&copy;<a href="http://www.kath.de"> <font face="Arial, Helvetica, sans-serif">www.kath.de</font></a></P>
            </TD>
          <TD align="left" background=boxright.gif><IMG height=8 alt="" 
            src="boxright.gif" width=8></TD>
        </TR>
        <TR vAlign=top>
          <TD align="left"><IMG height=8 alt="" src="boxbottomleft.gif" 
            width=8></TD>
          <TD align="left" background=boxbottom.gif><IMG height=8 alt="" 
            src="boxbottom.gif" width=8></TD>
          <TD align="left"><IMG height=8 alt="" src="boxbottomright.gif" 
            width=8></TD>
        </TR></TBODY></TABLE></TD><td style='vertical-align:top;'>
          <p><a href="http://www.update-seele.de" target="_blank"><img src="banner-update-seele-02.jpg" width="286" height="70" border="0"></a> </p>
          <p>
            <script type="text/javascript"><!--
        google_ad_client = "pub-9537752113242914";
        google_ad_width = 336;
        google_ad_height = 280;
        google_ad_format = "336x280_as";
        google_ad_type = "text_image";
        //2006-10-23: Lexikon Symbole
        google_ad_channel = "8495253357";
        //--></script>
            <script type="text/javascript"
          src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
              </script>
                    </p></td>
</TR>
  <TR>
    <TD vAlign=top align=left>&nbsp; </TD>
  </TR></TBODY></TABLE></BODY></HTML>
