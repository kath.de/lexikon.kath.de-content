---
title: Alleinseligmachen Kirche
author: 
tags: 
created_at: 
images: []
---


# **Extra ecclesiam nulla salus**
# ******
 D***as Axiom von der „alleinseligmachenden Kirche“ riecht nach Gruppenegoismus, Größenwahn und Intoleranz. Es will dem heutigen Menschen nicht mehr über die Lippen. Dennoch: Man darf sich durch die Anstößigkeit des Wortlauts die gemeinte Sache nicht verdecken lassen. Was ist mit dem Axiom gemeint? Zunächst zu seinem Ursprung: Bereits Origenes hat formuliert: „Außerhalb der Kirche wird niemand gerettet“ (Origenes, In Jesu Nave 3,5; PG 12, 841). Später wurde daraus der Satz: „Außerhalb der Kirche kein Heil“ (Extra Ecclesiam nulla salus). Dieser Satz gehört zum festen Glaubensgut der Kirche. Denn er speist sich aus zwei anderen Glaubensaussagen: 

# **1.***[ Christus](sohn_gottes.php) allein ist die Wahrheit und der Weg für das Heil der Welt (vgl. Joh 14,6). **
 2.	Die Kirche ist der Ort unter den Völkern, wo das von Christus geschaffene Heil anwesend und wirksam ist. – Weil beides vom Neuen Testament her völlig eindeutig ist, kann die Kirche das „alleinseligmachend“ nicht zurücknehmen. Sie würde sich sonst von der Erlösung durch Christus und von ihrer Indienstnahme als „Sakrament des Heils für die Welt“ verabschieden. 

***Die Frage „Können auch Nichtgetaufte ewig selig werden?“ engt das, worum es bei dem Axiom geht, in einer unerträglichen Weise ein. Was diese Spezialfrage angeht, hat die Kirche seit Jahrhunderten gesagt, dass es ein votum ecclesiae gibt, das heißt, eine Sehnsucht nach der Wahrheit, nach dem Guten, nach der richtigen Gesellschaftsordnung. Wer von Christus nichts weiß und nie erfahren hat, was Kirche ist, aber in solcher Sehnsucht lebt und handelt, wird sein ewiges Heil nicht verlieren. 

***Bei dem „alleinseligmachend“ geht es aber gar nicht in erster Linie um diese Spezialfrage. Die Verheißungen der Bibel meinen mehr als die ewige Seligkeit des einzelnen nach dem Tod. Es geht immer auch und sogar vor allem um diese Welt – ob es in ihr Frieden und Glück, Freiheit und Menschenwürde, Solidarität und Menschlichkeit gibt. Gott will nichts sehnlicher als eben dieses „Heil der Welt“. 

***Wie kann es Realität werden? Die Bibel sagt: Es hat in der Welt an einer Stelle angefangen: mit dem Glauben und dem Gehorsam Abrahams. Es hat Form angenommen in Israel, wenn das Gottesvolk die Sozialordnung vom Sinai lebte und auf seine Propheten hörte. Und es ist endgültig in der Welt festgemacht durch die Botschaft und die Lebenshingabe Jesu, der Israel zum endzeitlichen Gottesvolk gesammelt hat. 

***„Außerhalb der Kirche kein Heil“ heißt also vor allem: Auf einem langen Weg wurde mitten in der Welt in einem unglaublichen Experiment unter vielen Opfern die richtige Gesellschaft gefunden, die dem Willen Gottes entspricht. Diese Gesellschaft zu leben und auszubreiten, wäre das Glück für die Völker. Die Erfahrung des Glaubens sagt: Es ist der einzige Weg! 

***Professor Dr. Gerhard Lohfink, Bad Tölz 

 

©***[  ](%20)******[Akademie für die Theologie des Volkes Gottes](http://www.akademie-cavalletti.de/) 
