---
title: Freiheit und Entscheidung
author: 
tags: 
created_at: 
images: []
---


# ** Friheit verwirklich sich durch Entscheidungen*
# **Die Freiheit ist erst einmal nur ein Vermögen. Damit aus diesem Vermögen Wirklichkeit wird, muß noch etwas hinzukommen. Da Freiheit  in der Zeit „geschehen“ muß, braucht es etwas, was die Zeit organisiert. Mit einer Entscheidung verwirklicht sich die Freiheit in der Zeit, denn sie bestimmt mit der Entscheidung, wie das Leben verlaufen soll. Konkret ist es die Entscheidung für einen bestimmten Beruf, die Partnerschaft mit einem bestimmten Menschen, das Eingehen von Freundschaften, aber auch die Entscheidung für eine Weltanschauung. In der Entscheidung wird das „wirklich“, was die Freiheit will. **
 Von einer Entscheidung kann man nur sprechen, wenn es mehr als eine Alternative gibt. Das ist bei der Berufs- wie bei der Partnerwahl offenkundig. Es gilt auch für die Weltanschauung, für die sich die Freiheit entscheidet. Denn keine Weltanschauung läßt sich in der Weise empirisch beweisen wie ein Naturgesetz. Da es von seiner Weltanschauung abhängt, wie der einzelne sich in bestimmten Situationen verhält, beeinflußt die Entscheidung für eine bestimmte Weltanschauung den Verlauf des Lebens in hohem Maß. Die Weltanschauung, daß es auf Erfolg und Sich-Durchsetzen ankommt, macht aus demjenigen einen anderen Menschen als wenn er sich für die Weltanschauung entschieden hätte, daß Leben in der Gemeinschaft gelingt und daher Solidarität und der Blick für diejenigen, die am Rand stehen, für das Gelingen des eigenen Lebens entscheidend sind. Die Weltanschauung, für die sich jemand entschieden hat, steuert das Leben bis in die alltäglichen kleinen Entscheidungen. Da die Freiheit sich ***[unerbittlich](freiheit.php) einfordert und wir gegenüber unserer Freiheit nicht frei sind, ist die Entscheidung für eine Weltanschauung die, die unser Leben am nachhaltigsten beeinflußt.  

# **Eckhard Bieger S.J.**
******[Buchtipp](http://www.kathshop.de/wwwkathde/product_info.php?cPath=&products_id=195&anbieter=17&page=&) 

©***[ ***www.kath.de](http://www.kath.de) 
