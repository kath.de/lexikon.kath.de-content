---
title: Menschwerdung Jesu
author: 
tags: 
created_at: 
images: []
---


# **Jesus von Nazareth macht eine Entwicklung durch*
***Jesus von Nazareth wird im Glaubensbekenntnis als der ewige Sohn Gottes bekannt. Dieser Sohn, von Johannes „Logos“ genannt, ist Mensch geworden, geboren aus der Jungfrau Maria. War er sich dessen, vielleicht schon als Kind, bewußt? Oder gab es in ihm nicht nur ein körperliches, sondern auch ein geistiges Wachstum? Wie andere Kinder hat Jesus sprechen gelernt, in der Synagogenschule von Nazareth wurde er mit der Bibel der Juden vertraut und konnte als junger Mann mit den Schriftgelehrten über die richtige Auslegung einzelner Bibelstellen diskutieren. Hat er auch gelernt, sich als Sohn Gottes zu verstehen oder hatte er das von Anfang gewußt?  

# **Ausmalung des Lebens Jesu in den sog. apokryphen Evangelien****
 In sog. apokryphen Evangelien, die nicht in den Bestand des Neuen Testaments aufgenommen wurden, wird mehr über die Kindheit Jesu berichtet als wir aus dem Lukasevangelium entnehmen können. Dem kleinen Jesus werden bereits Wundertaten zugeschrieben, ein überlegenes Wissen und vieles andere, um ihn als den menschgewordenen Gott darzustellen. Auch in späteren Jahrhunderten waren einzelne Theologen und religiöse Schriftsteller davon überzeugt, daß schon der junge Jesus in einer direkten Schau Gottes lebte und selbst am Kreuz Gott direkt schaute.***

 Wie die apokryphen Evangelien wollen diese Vorstellungen das Besondere an Jesus herausstellen. Das Göttliche des Sohnes Gottes muß das Bestimmende sein. Entsprechen diese Vorstellungen aber dem, was im Neuen Testament überliefert wird? 

# **Hinweise in den Evangelien****
 Die wenigen Hinweise, die sich in der Bibel über Jesus finden, besagen, daß er sich durch seine Wunder wie auch durch Worte als der ***[Sohn Gottes](sohn_gottes.php)  zu erkennen gab. Zugleich geht die Bibel von einer persönlichen Entwicklung Jesu aus.***

 Lukas berichtet von einer Wallfahrt der Familie nach Jerusalem. Jesus ist 12 Jahre alt, d.h. im jüdischen Verständnis volles Mitglied der Gottesdienstgemeinschaft in der Synagoge. Die Eltern sind bereits auf dem Rückweg, als sie feststellen müssen, daß Jesus auch nicht mit anderen Pilgern zurückkehrt, sondern in Jerusalem geblieben sein muß. Dorthin zurückgekehrt finden sie ihn im Lehrgespräch mit den Schriftgelehrten. Er kehrt dann mit seinen Eltern nach Nazareth zurück. Lukas faßt seine Entwicklung so zusammen: „Jesus aber wuchs heran und seine Weisheit nahm zu, und er fand Gefallen bei Gott und den Menschen.“ Kap 3, 52***

 Der Hebräerbrief schreibt noch deutlicher:***

 Darum mußte er (Jesus) in allem seinen Brüdern gleich sein, um ein barmherziger und treuer Hoherpriester vor Gott zu sein und die Sünden des Volkes zu sühnen. Denn da er selbst in Versuchung geführt wurde und gelitten hat, kann er denen helfen, die in Versuchung geraten (Kap. 3,11-18)***

# *
 Bewußtsein Jesu als Sohn Gottes*****

 Wie war sich aber Jesus seiner ***[Gottessohnschaft](gottessohn.php) bewußt? Wenn ihm mit seinem menschlichen Bewußtsein verborgen gewesen wäre, daß er Sohn Gottes ist, dann wäre der Mensch Jesus vom Sohn Gottes fremdgesteuert, ohne daß er es wußte – und damit wäre er unfrei. Die theologische Reflexion ist aber zu dem Ergebnis gekommen, daß Jesus nicht nur einen göttlichen, sondern auch einen ***[menschlichen Willen](monotheletismus_monergetismus.php) hatte. Das schreibt der Autor des Hebräerbriefes ausdrücklich. ***

 In den Evangelien finden sich weitere Hinweise, wie Jesus sich seiner Sohnschaft bewußt war. Er lebte in einer unmittelbaren Beziehung zu Gott, den er seinen Vater nennt. Darin zeigt sich viel unmittelbarer als es in Begriffen ausgedrückt werden kann, wie Jesus von Gott her lebt und daß er sich unmittelbar als Gottes Sohn versteht. Das Johannesevangelium entfaltet diese Nähe Jesu zu seinem Vater in den Gesprächen Jesu, die er mit seinen Jüngern im Abendmahlssaal führt. Die Christen haben die Nachfolge Jesu so verstanden, daß sie in sein Verhältnis zu seinem Vater aufgenommen werden und daher das "Vater unser" beten können.***

 Die Evangelien berichten an mehreren Stellen, daß Jesus sich zum Gebet zurückzieht, wie er am Ölberg in der Nacht seiner Gefangennahme seinen Vater anfleht. Bei Matthäus findet sich ein kurzer Text, in dem Jesus sein Verhältnis zum Vater in Worte faßt:******

 "Ich preise dich, Vater, Herr des Himmels und der Erde, weil du all das den Weisen und Klugen verborgen, den Unmündigen aber offenbart hast. Ja Vater, so hat es dir gefallen. Mir ist von meinem Vater alles übergeben worden; niemand kennt den Sohn, nur der Vater, und niemand kennt den Vater, der Sohn und der, dem es der Sohn offenbaren will."***

 Matthäus 11,25-27 

# **Zitate**
# ****Er (Jesus) war Gott gleich, **
 hielt aber nicht daran fest, wie Gott zu sein,***

 sondern entäußerte sich und wurde wie ein Sklave,***

 und den Menschen gleich.***

 Sein Leben war das eines Menschen,***

 er erniedrigt sich und war gehorsam bis zum Tod,***

 bis zum Tod am Kreuz.***

 Darum hat ihn Gott über alle erhoben,***

 und ihm einen Namen verliehen, ***

 der größer ist als alle Namen,***

 damit alle im Himmel, auf der Erde und unter der Erde,***

 ihre Knie beugen vor dem Namen Jesu,***

 und jeder Mund bekennt:***

 Jesus Christus ist der Herr,***

 zur Ehre Gottes des Vaters.***

 Philipperbrief 2,6-11            

# **Aus dem Johannesevangelium**
 Der Vater liebt den Sohn und hat alles in seine Hand gegeben. Wer an den Sohn glaubt, hat das ewige Leben; wer aber dem Sohn nicht gehorcht, wird das Leben nicht sehen, sondern Gottes Zorn bleibt auf ihm." Kap. 3, 35-36***

 Ich bin der gute Hirt; ich kenne die Meinen und die Meinen kennen mich, wie mich der Vater kennt und ich den Vater kenne, und ich gebe mein Leben für meine Schafe."Kap.10,14-15***

 Und er erhob seine Augen zum Himmel und sprach: Vater, die Stunde ist da. Verherrliche deinen Sohn, damit der Sohn dich verherrlicht. Denn du hast ihm die Macht über alle Menschen gegeben, damit er allen, die du ihm gegeben hast, ewiges Leben schenkt. .... Ich habe dich auf der Erde verherrlicht und das Werk zu Ende geführt, das du mir aufgetragen hast. Vater, verherrliche du mich jetzt bei dir mit der Herrlichkeit, die ich bei dir hatte, bevor die Welt war. Kap.17,1-2, 4-5 

***Dann verließ Jesus die Stadt und ging, wie er es gewohnt war, zum Ölberg; seine Jünger folgten ihm. Als er dort war, sagte er zu ihnen: Betet darum, daß ihr nicht in Versuchung geratet. Dann entfernte er sich von ihnen ungefähr einen Steinwurf weit, kniete nieder und betete: Vater, wenn du willst, nimm diesen Kelch von mir. Aber nicht mein, sondern dein Wille soll geschehen. Lukas 22,39-42 

# **Die Vorstellung, daß der Logos geschaffen ist und anstelle der Geistseele bei der Menschwerdung trat, so daß Jesus keine menschliche Seele hat, findet sich bei Exodius, der von 360 bis 370 Patriarch von Konstantinopel war:**
 Wir glauben an den einen, den allein wahren Gott und Vater, die alleinige (göttliche) Natur, der ungezeugt und ohne Vater ist, ....***

 und an den einen Herrn, den Sohn, der gottesfürchtig war, weil er den Vater heilig hielt. Er ist zwar der Erstgeborene, gewaltiger als die ganze Schöpfung nach ihm, Erstgeborener ist er deshalb, weil er das am meisten herausragende und erste von allen Geschöpfen ist. Er ist Fleisch geworden, nicht Mensch; denn er nahm keine menschliche Seele an, sondern wurde nur Fleisch, damit Gott sich durch das Fleisch den Menschen wie durch einen Vorhang hindurch offenbare. Es gab also nicht zwei Naturen, weil der Mensch nicht vollständig war, sondern statt der Seele Gott im Fleisch war. 

# **Cyrill von Jerusalem 3313-387, zeigt auf, daß der Sohn Gottes wirklich Mensch geworden ist:**
 Er (Jesus) kam nicht wie durch eine Röhre durch die Jungfrau, sondern er hat aus ihr wahrhaft Fleisch angenommen ... Wenn nämlich die Menschwerdung eine Fiktion war, ist auch unsere Erlösung nur eine Fiktion.***

 Katechesen 4.9 

# **So tritt denn der Sohn Gottes in diese niedrige Welt ein und steigt von seinem himmlischen Thron herab, ohne dabei die Herrlichkeit seines Vaters zu verlassen. Er kommt zur Welt in einer neuen Ordnung und in einer neuen Geburt. In einer neuen Ordnung, denn der in dem Seinen (der göttlichen Natur) Unsichtbare ist in Unserem sichtbar geworden, und der Unbegreifliche wollte begriffen werden. Der vor aller Zeit Seiende hat in der Zeit begonnen zu sein. Der Herr des Alls hat Knechtsgestalt angenommen und so seine unermeßliche Majestät verhüllt. Der leidensunfähige Gott hat es nicht verschmäht, ein leidensfähiger Mensch zu sein, und der Unsterbliche nicht, sich dem Gesetz des Todes zu unterwerfen. **
 Papst Leo I im Brief an Flavian von Konstantinopel 449 

# **Christus schuf sich die Mutter, als er beim Vater war, und als er geboren wurde aus der Mutter, blieb er im Vater. Wie sollte er aufhören, Gott zu sein, als er Mensch zu sein begann, der seiner Gebärerin gewährte, nicht aufzuhören, Jungfrau zu sein, als sie gebar? Dadurch, daß das Wort Fleisch geworden ist, ist das Wort nicht untergegangen und auch nicht in Fleisch verwandelt worden, sondern das Fleisch kam zum Wort hinzu, damit es selbst nicht untergehe. Wie der Mensch aus Seele und Leib besteht, so ist Christus Gott und Mensch. Derselbe Gott, der auch Mensch ist, und der Gott ist, derselbe ist auch Mensch, nicht durch Vermischung der Naturen, sondern durch die Einheit der Person.**
 Augustinus Sermo 186, 1, um 411 

# **Friedrich Schleiermacher hat im 19. Jahrhundert einen neuen Zugang zur Person Jesu gesucht und die Entwicklung des Selbstbewußtseins Jesu als Gott so erklärt:**
 Da wir aber doch den Anfang des Lebens nie eigentlich begreifen … gilt dieses auch von seinem (Jesu) Gottesbewußtsein, …welches zwar auch anderen ebensowenig als ihm irgendwann erst durch Erziehung eingeflößt wird, sondern dessen Keim in allen schon ursprünglich liegt, welches sich aber auch in ihm wie in allen erst allmählich nach menschlicher Weise zum wirklich erscheinenden Bewußtsein entwickeln mußte, und vorher nur als Keim, wenngleich in gewissem Sinn immer als wirksame Kraft, vorhanden war. Zu der reinen Geschichtlichkeit der Person des Erlösers gehört aber auch dieses, daß er sich nur in einer gewissen Ähnlichkeit mit seinen Umgebungen, also im allgemeinen volkstümlich, entwickeln konnte. Denn da Sinn und Verstand nur aus dieser ihn umgebenden Welt genährt wurden, und auch seine freie Selbsttätigkeit in dieser ihren bestimmten Ort hatte: so konnte sich auch sein Gottesbewußtsein, wie ursprünglich auch die höhere Kraft desselben sei, doch nur ausdrücken und mitteilen in Vorstellungen, die er sich aus diesem Gebiet angeeignet hatte, und in Handlungen, welche in demselben ihrer Möglichkeit nach vorbestimmt waren***

 Der christliche Glaube, Aus dem Ersten Lehrstück über die Person Jesu, § 93,39 

# **Paul Tillich über Jesus als den Menschen, der Gott repräsentiert:**
 Wenn darum der Christus als Mittler und Erlöser erwartet wird, ist er keine dritte Wirklichkeit zwischen Gott und dem Menschen. Er ist derjenige, der Gott den Menschen gegenüber repräsentiert. Er repräsentiert nicht den Menschen Gott gegenüber. Er zeigt vielmehr, was Gott wünscht, daß der Mensch sei. Er zeigt denen, die unter den Bedingungen der Existenz leben, was der Mensch essentiell ist und darum sein sollte. Es ist unangemessen und führt zu einer falschen Christologie, wenn man sagt, daß der Mittler eine eigene ontologische Realität neben Gott und Mensch sei. Nur ein Halb-Gott könnte das sein, der gleichzeitig ein Halb-Mensch wäre. Solch ein drittes Wesen könnte weder Gott den Menschen gegenüber repräsentieren, noch könnte es das wesenhafte Menschsein ausdrücken. Aber der Mittler repräsentiert das wesenhafte Menschsein; und damit repräsentiert er Gott. Anders ausgedrückt: Er repräsentiert das Bild Gottes, das ursprünglich im Menschen verkörpert ist, aber er tut es unter den Bedingungen der Entfremdung zwischen Gott und Mensch.***

 Systematische Theologie, 1957, S. 103 

# **Text: Eckhard Bieger S.J***.**
 ©***[ ***www.kath.de](http://www.kath.de) 
