---
title: Leib Jesu
author: 
tags: 
created_at: 
images: []
---


# **Jesus hatte nicht nur einen Scheinleib*
***Jesus ist ein wirklicher Mensch, der predigend durch Palästina gezogen ist, der Menschen berührt hat, um sie zu heilen oder zu trösten. Sein Leib wurde geschunden, gegeißelt und gekreuzigt. Aber er ist nirgendwo begraben. Zwar wurde er nach seinem Tod am Kreuz in einer Grabkammer beigesetzt. Als die Frauen nach dem Sabbat, am ersten Tag der jüdischen Woche, den Leichnam endgültig versorgen wollten, fanden sie das Grab offen, der Leichnam war verschwunden. Gibt es den Leib Jesu? Zumindest beim Kommuniongang wird den Gläubigen die Hostie mit den Wort: „Leib Christi“ in die Hand oder auf die Zunge gelegt.  

# **Unzulängliche Antworten****
 In den ersten christlichen Jahrhunderten wurden unterschiedliche Meinungen vertreten. Wenn Jesus von Nazareth wirklich Gott war, konnte er dann wirklich Mensch sein? Jesus, der Mensch, war geschaffen, Jesus, der Sohn Gottes, ist nicht geschaffen. Wie kann aber Geschaffenes mit dem Göttlichen eine Einheit bilden? In den ***[christologischen Streitigkeiten](christologische_streitigkeiten.php) werden die unterschiedlichsten Denkmodelle durchgespielt. Eine erste Lösung war die der Doketen, die sich vom griechischen Wort für "Schein" herleiten. Der Leib Jesu war nicht wirklich, Gott hat sich in einem Scheinleib gezeigt. Die Gnostiker hatten eine ähnliche Vorstellung, weil für sie die Leiblichkeit eine Art Verbannung der Seele in die Materie bedeutet. Erlösung ist für die Gnostiker Befreiung der Seele von allem Körperlichen - durch Erkenntnis, im Griechischen Gnosis. 

# **Die Realität der Passion****
 Wenn man die Realität des Leibes Jesu nicht anerkennt, dann sind auch Geißelung und Kreuzigung Jesu nicht wirklich geschehen. Verschiedene Reaktionen auf den Film „Die Passion Christi“ im Jahr 2004 zeigen, wie schwer es fällt, die Realität dieser grausamen Hinrichtung zu akzeptieren. Den ***[Passionsberichten](http://www.kath.de/Kirchenjahr/karfreitag.php) der Evangelien wird man nur gerecht, wenn der Leib Jesu nicht ein Scheinleib war, sondern den Qualen ausgesetzt war, die in den nüchternen Berichten der Bibel angedeutet werden. Der Theologe Irenäus von Lyon im 3. Jahrhundert bringt das auf die Formel „Caro Cardo Salutis“ „Das Fleisch ist die Türangel des Heils“.***

# *
 Theologische Lösung*****

 Die Bedeutung des Leibes wird noch einmal mehr in der ***[Auferstehung ](auferstehung_jesu.php)deutlich. Wenn man das Leiden Jesu als körperlich real akzeptieren muß, dann könnten die Begegnungen der Jünger Jesu mit dem Auferstandenen als Visionen gesehen werden, in denen sie sich nur eines Scheinleibes ansichtig wurden. Um dieser Interpretation vorzubeugen, schildern Lukas und Johannes in ihren Evangelien die reale leibliche Anwesenheit des Auferstandenen. Beide Evangelisten waren wahrscheinlich schon mit gnostischen Vorstellungen konfrontiert. (Texte s.u.)***

 Mit seinem Leib wird Jesus erst zu einem Teil der menschlichen Geschichte. Durch seinen ***[Leib](inkarnation.php) wird sein Leiden und Sterben real. Durch die Auferstehung des Leibes wird der Tod erst überwunden. Während das griechische Denken im Tod die Befreiung der Seele von allem Materiellen sah, sehen die Christen in der Auferstehung die Vollendung des Materiellen. Jesus ist leiblich in den Himmel aufgefahren und seine Wundmale bleiben im Himmel leiblich präsent. Gerade die Auferstehung des Leibes gibt dem Körper des Menschen eine letzte Bedeutung, macht ihn unantastbar.Die Bedeutung des Leibes Jesu setzt sich fort in einer Beschreibung der Kirche. Sie wird im Epheserbrief der Leib Christi genannt. 

# **Zitate**
 Lukas 24,36-40***

 ***Während sie noch miteinander redeten, trat er selbst in ihre Mitte und sagte zu ihnen: Friede sei mit euch! Sie erschraken und hatten große Angst, denn sie meinten, einen Geist zu sehen. Da sagte er zu ihnen: Was seid ihr so bestürzt? Warum laßt ihr in euren Herzen solche Zweifel aufkommen, Seht meine Hände und meine Füße an: Ich bin es selbst. Faßt mich doch an und begreift: Kein Geist hat Fleisch und Knochen, wie ihr es an mir seht. Bei diesen Worten zeigte er ihnen seine Hände und Füße.  

# **Johannes zeigt in der Begegnung des Auferstandenen mit dem Zweifler Thomas die Realität des Leibes Jesu auf:**
 Acht Tage darauf waren die Jünger wieder versammelt und Thomas war dabei. Die Türen waren verschlossen. Da kam Jesus, trat in ihre Mitte und sagte: Friede sei mit euch. Dann sagte er zu Thomas: Streck deine Finger aus – hier sind meine Hände. Streck deine Hand aus und leg sie in meine Seite, und sei nicht ungläubig, sondern gläubig. Thomas antwortete ihm: Mein Herr und mein Gott. Kap. 20,26-28 

***Gott hat seine Macht an Christus erwiesen, den er von den Toten auferweckt und im Himmel auf den Platz zu seiner Rechten erhoben hat, hoch über alle Fürsten und Gewalten, Mächte und Herrschaften und über jeden Namen, der nicht nur in dieser Welt, sondern auch in der zukünftigen genannt wird. Alles hat er ihm zu Füßen gelegt und ihn, der als Haupt alles überragt, über die Kirche gesetzt. Sie ist sein Leib und wird von ihm erfüllt, der das All ganz und gar beherrscht. Epheserbrief 1,20-23 

# **.. daß wir vollkommen überzeugt sind von der Geburt, dem Leiden und der Auferstehung, die während der Regierungszeit von Pontius Pilatus erfolgt ist; wirklich und gewiß wurde dies vollbracht von Jesus Christus, unserer Hoffnung….**
 Brief des Ignatius v. Antiochien an die Gemeinde in Ephesus 117  

***Text: Eckhard Bieger  

***©***[ www.kath.de](http://www.kath.de) 
