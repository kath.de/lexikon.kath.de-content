<HTML><HEAD><TITLE>Christologische Streitigkeiten</TITLE>
<META http-equiv=Content-Type content="text/html; charset=iso-8859-1"><LINK 
title=fonts href="kaltefleiter.css" type=text/css 
rel=stylesheet>
<META content="MSHTML 5.50.4134.600" name=GENERATOR></HEAD>
<BODY bgColor=#ffffff leftMargin=6 topMargin=6 marginheight="6" marginwidth="6">
<TABLE cellSpacing=0 cellPadding=6 width="100%" border=0>
  <TBODY>
  <TR>
    <TD vAlign=top align=left width=100> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b>Philosophie&amp;Theologie</b></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td> <?php include("logo.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10"> <?php include("az.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </TD>
    <TD vAlign=top rowSpan=2>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
        <TBODY>
        <TR vAlign=top align=left>
          <TD width=10><IMG height=8 alt="" 
            src="boxtopleftcorner.gif" width=8></TD>
          <TD width="516" background=boxtop.gif><IMG height=8 alt="" 
            src="boxtop.gif" width=8></TD>
          <TD width=8><IMG height=8 alt="" 
            src="boxtoprightcorner.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxtopleft.gif><IMG height=8 alt="" 
            src="boxtopleft.gif" width=8></TD>
          <TD bgColor=#e2e2e2> 
            <H1><font face="Arial, Helvetica, sans-serif">Christologische Streitigkeiten </font></H1>
          </TD>
          <TD background=boxtopright.gif><IMG height=8 
            alt="" src="boxtopright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=13 alt="" src="boxdividerleft.gif" 
            width=8></TD>
          <TD background=boxdivider.gif><IMG height=13 
            alt="" src="boxdivider.gif" width=8></TD>
          <TD><IMG height=13 alt="" 
            src="boxdividerright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxleft.gif><IMG height=8 alt="" 
            src="boxleft.gif" width=8></TD>
          <TD class=L12>
            <P><STRONG><font face="Arial, Helvetica, sans-serif">Die Logik der
                  theologischen Streitigkeiten der ersten Jahrhunderte
                  </font></STRONG></P>
            <P><font face="Arial, Helvetica, sans-serif">&Uuml;ber Jesus von Nazareth, den Gesalbten, den Christus, wurde &uuml;ber
              Jahrhunderte gestritten. Theologische Schulen bek&auml;mpften sich.
              Konzilien konnten nur nach langen Debatten eine Kl&auml;rung herbeif&uuml;hren.
              Heute m&uuml;ssen sich Studenten der Theologie m&uuml;hsam einen
              Denkweg durch die Theologiegeschichte der ersten Jahrhunderte erarbeiten.
              Welche Logik steckt hinter den Auseinandersetzungen, die als christologische
              Streitigkeiten in die Geschichte eingegangen sind?<br>
              Es geht um die Frage, was Jesus f&uuml;r den einzelnen Christen
              bedeutet. Da&szlig; er f&uuml;r Maria, seine J&uuml;nger, Maria
              und Martha und viele andere etwas bedeutet hat, bezweifelt niemand.
              Auch ist es unstrittig, da&szlig; er eine Bewegung ausgel&ouml;st
              hat, so da&szlig; die <a href="suendenbock.php">j&uuml;dische
              Obrigkeit</a> Angst vor den Auswirkungen
              seiner Predigt und seiner Heilungswunder 
              ergriff. Mit der Kreuzigung hatten sie das Problem nicht aus der
              Welt geschafft. Seine Anh&auml;nger behaupteten, da&szlig; er lebt
              und viele Juden wie auch Nichtjuden lie&szlig;en sich taufen, um
              so Anh&auml;nger des Mannes aus Nazareth zu werden. In ihren Predigten
              behaupteten seine J&uuml;nger, da&szlig; er der verhei&szlig;ene<a href="messias_christus.php"> Messias</a>,
              der Gesalbte, der Christus sei, der Israel befreien werde. Als
              die christlichen Prediger Anh&auml;nger au&szlig;erhalb Israels
              gewannen und das Christentum eine geistige Kraft in der griechisch
              gepr&auml;gten Kultur des r&ouml;mischen Reiches wurde, wollten
              die Intellektuellen der damaligen Zeit genauer wissen, wer er wirklich
              sei. Sie wandten zur Kl&auml;rung ihre philosophisch gepr&auml;gten
              Begriffe an. Die christlichen Theologen mu&szlig;ten die Botschaft
              von Jesus in eine Denkwelt einbringen, die mehr verlangte als eine &Uuml;bersetzung
              der biblischen Texte. War aber eine Frage einigerma&szlig;en gel&ouml;st,
              entstand aus der Antwort jeweils wenigstens eine neue. Der Denkweg,
              der zu den Lehraussagen der Konzilien &uuml;ber Jesus Christus
              f&uuml;hrte, hat eine innere Logik, die mit der Frage beginnt:
              Was hei&szlig;t es, da&szlig; Jesus von Nazareth der Sohn Gottes
              ist &#8211; wie es mehrfach in den biblischen Schriften steht:</font></P>
            <P><font face="Arial, Helvetica, sans-serif"><strong>Jesus Christus:
                  Als Sohn Gottes geschaffen oder aus Gott hervorgegangen? Konzil
                  von Nic&auml;a 325</strong><br>
              Ist Jesus Gottes Sohn in dem Sinne, da&szlig; Gott ihn geschaffen
              und dann als Sohn adoptiert hat, so wie die sog. Adoptianer es
              behaupteten? Das w&uuml;rde bedeuten, da&szlig; Jesus zuerst als
              Mensch geboren wurde und dann, etwa bei der Taufe im Jordan, von
              Gott als Sohn adoptiert wurde. Diese Lehre war &uuml;berwunden,
              als Arius auftrat. Er geht davon aus, da&szlig; der Sohn vor der
              Erschaffung der Welt als Weisheit Gottes existierte, jedoch nicht
              Gott gleich, sondern als Gesch&ouml;pf.<a href="arianismus.php"> Arius</a> war Priester und
              Theologe. Er behauptete, da&szlig; Jesus geschaffen sei und konnte
              sich damit der Unterst&uuml;tzung der Intellektuellen seiner Zeit
              sicher sein. F&uuml;r diese war v&ouml;llig evident, da&szlig; Gott
              nur einer sein kann, denn vollkommen kann etwas sein, das es nur
              einmal und ungetielt gibt. Das Konzil von Nic&auml;a  stellte 325
              auf der Basis des Neuen Testaments fest, da&szlig; der Sohn Gottes nicht
              geschaffen sein kann:<br>
              Wir glauben an den einen Gott, der aber dreifaltig in drei Personen
              existiert. Jesus kommt vom Vater her, er ist als Sohn des Vaters
              kein Gesch&ouml;pf, er ist in einem innerg&ouml;ttlichen Hervorgehen
              gezeugt und nicht au&szlig;erhalb Gottes geschaffen.<br>
&#8222;              Gezeugt nicht geschaffen&#8220; ist die Formel von Nic&auml;a,
              die im Glaubensbekenntnis fest verankert ist.</font></P>
            <P><font face="Arial, Helvetica, sans-serif"><strong>Die Menschheit Jesu nur eine Erscheinung?</strong><br>
              Wenn Jesus wahrer Gott ist, wie ist dann zu verstehen, da&szlig; er
              auch wahrer Mensch ist?<br>
              Ein erster Antwortversuch: Dann ist er nicht wirklicher Mensch,
              sondern als wahrer Gott in menschlicher Gestalt erschienen. Das
              ist die These der Gnostiker <br>
              Wenn die menschliche Gestalt aber nur ein Erscheinungsbild ist,
              dann hat die Kreuzigung Jesu nicht wirklich stattgefunden, er w&auml;re
              dann auch nicht gestorben und nicht mit seinem Leib in den Himmel
              aufgefahren. Ohne Konzil war f&uuml;r die Theologen evident: Jesus
              Christus ist wirklicher Mensch und leiblich auferstanden.</font></P>
            <P><font face="Arial, Helvetica, sans-serif">Ist Jesus nur mit dem Leib Mensch, hat aber anstatt einer geschaffenen
              Seele den Logos, den Sohn Gottes?<br>
              Wenn Jesus wirklicher Mensch gewesen sein mu&szlig; und zugleich
              der ungeschaffene <a href="sohn_gottes.php">Sohn Gottes</a> ist,
              wie ist dann die Einheit von Sohn Gottes und Mensch zu sehen? In
              der griechischen
              Philosophie ist das organisierende Prinzip des Leibes die Seele.
              Eine einfache L&ouml;sung w&auml;re, da&szlig; der Sohn Gottes,
              der Logos, anstelle einer geschaffenen Seele den K&ouml;rper beseelt.
              Das vertrat Apollinaris von Laodic&auml;a.<br>
              Aber: W&auml;re die Menschheit erl&ouml;st, wenn die Seele des
              Menschen nicht durch Jesus mit erl&ouml;st worden w&auml;re? Da
              die S&uuml;nde aus dem Herzen, also aus der Seele kommt, w&auml;re
              die Erl&ouml;sung fraglich. Daraus folgt: <br>
              Jesus, der Christus,
              mu&szlig; ganz Mensch sein, mit Leib und 
  Seele und zugleich Sohn Gottes.</font></P>
            <P><font face="Arial, Helvetica, sans-serif"><strong>Wenn Jesus Christus
                  eine eigene, geschaffene menschliche Seele hat, ist er dann &#8222;Zwei&#8220;;
                  Konzil von Ephesus 431</strong><br>
              Wenn Jesus aber Mensch mit Leib und Seele ist, wie kann er da noch
              einer sein. Gibt es dann nicht &#8222;zwei S&ouml;hne&#8220;, d.h.
              zwei Individuen? Diese Frage f&uuml;hrte zum n&auml;chsten wichtigen
              Konzil, 100 Jahre nach Nic&auml;a. 431 trafen sich Bisch&ouml;fe
              in Ephesus und erkl&auml;rten, da&szlig; Jesus, der Christus, als
              Sohn Gottes geboren ist. Maria ist Gottesgeb&auml;rerin. Damit
              wird klargestellt, da&szlig; Jesus nur einer ist und von Anfang
              an Sohn Gottes war und nicht sp&auml;ter erst von Gott als Sohn
              adoptiert wurde. <br>
              Jesus Christus ist von seiner Geburt an Sohn Gottes
              und Wenn Jesus aber Mensch mit Leib und Seele ist, wie kann er
              da noch einer sein. Gibt es dann nicht &#8222;zwei S&ouml;hne&#8220;,
              d.h. zwei Individuen? Diese Frage f&uuml;hrte zum n&auml;chsten
              wichtigen Konzil 100 Jahre nach Nic&auml;a. 431 trafen sich Bisch&ouml;fe
              in Ephesus und erkl&auml;rten, da&szlig; Jesus, der Christus, als
              Sohn Gottes geboren ist. Maria ist Gottesgeb&auml;rerin. Damit
              wird klargestellt, da&szlig; Jesus nur einer ist und von Anfang
              an Sohn Gottes war und nicht sp&auml;ter erst von Gott als Sohn
              adoptiert wurde. <br>
              Jesus Christus ist von seiner Geburt an Sohn Gottes
              und wahrer Mensch.</font></P>
            <p><font face="Arial, Helvetica, sans-serif">Das Konzil verurteilte
                den damaligen Patriarchen von Konstantinopel, Nestorius. Die
                Verurteilung war nach heutiger Kenntnis der Texte
              nicht gerechtfertigt, denn Nestorius hat die Lehre von den sog. &#8222;Zwei
              S&ouml;hnen&#8220; in Jesus nicht vertreten. Ein Teil der Bisch&ouml;fe
              hat sich dem Konzil nicht angeschlossen, weil sie eine Vermischung
              von G&ouml;ttlichem und Menschlichem bef&uuml;rchteten. So ist
              der Eindruck entstanden, sie w&uuml;rden in Jesus Christus &#8222;zwei
              S&ouml;hne&#8220; verehren. Diese in Persien beheimatete Kirche
              wurde mit dem Schimpfwort <a href="nestorianer.php">&#8222;Nestorianer&#8220;</a> belegt.
              Diese Kirche hatte Gemeinden entlang der Seidenstra&szlig;e
              gegr&uuml;ndet und war mit ihren Missionaren lange vor Marco Polo
              in China. Timur Lenk hat diese Kirche praktisch vernichtet, viele
              sind bei der Ausrottung der Armenier Anfang des 20. Jahrhunderts
              get&ouml;tet worden, es gibt nur noch kleine Reste dieser Kirche. </font></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Ist Jesus eine
                  neue Natur, die sozusagen aus Gottheit und Menschheit vereinigt
                  ist oder gibt es ein einigendes Prinzip,
                das beide Naturen
              umfa&szlig;t?</strong><br>
              Wenn Jesus Christus wahrer Mensch und wahrer Gott ist, was ist
              das einigende Prinzip? Wenn man den Begriff &#8222;Physis&#8220; -
              Natur benutzt, kann man von g&ouml;ttlicher und menschlicher Natur
              sprechen. Benutzt man das gleiche Wort, um das einigende Prinzip
              von Gottheit und Menschheit in Jesus zu benennen, dann dr&uuml;ckt
              der gleiche Begriff sowohl die Verschiedenheit aus wie auch die
              Einheit. Die Theologenschule von Alexandrien, die das Konzil von
              Ephesus bestimmt hatte, ist diesen Weg gegangen. Sie bef&uuml;rchtete,
              da&szlig;  der Mensch Jesus und der Sohn Gottes nur als lose
              verbunden gedacht werden, ohne da&szlig; die Einigung zwischen
              Gottheit und Menschheit als endg&uuml;ltig gesehen wird. Weil dieser
              Schule die Durchdringung der menschlichen Natur durch das G&ouml;ttliche
              bis heute als inneres Prinzip auch der christlichen Spiritualit&auml;t
              sieht, spricht sie von einer neuen Physis, einer neuen Natur, um
              die Einheit zwischen Gottheit und Menschheit zu betonen. Sie werden
              von ihren Gegnern als <a href="monophysiten.php"> Monophysiten </a> bezeichnet.
              So verstehen sich die Kopten und Teile der syrischen Kirche, sowie
              die &auml;thiopische und armenische Kirche bis heute. Zum Bruch
              kam es nach dem Konzil von Chalcedon, das den ersten Schritt einer
              Kl&auml;rung einleitete. </font></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Neben den zwei
                  Naturen &#8222;Gottheit&#8220; und &#8222;Menschheit&#8220; gibt
              es die Person, Jesus Christus ist nur einer, aber in zwei Naturen.
              Konzil von Chalzedon 451</strong><br>
              Um das Einigende in Jesus Christus zu benennen, reicht der Begriff &#8222;Physis&#8220; nicht.
              Langsam entwickelte sich eine Begrifflichkeit, die in dem <a href="person.php">Personbegriff</a> m&uuml;ndete. Aber erst im 6. Jahrhundert war dieser Denkweg einigerma&szlig;en
              abgeschlossen. Im Konzil von Chalzedon 451 wird der Begriff bereits
              gebraucht. Die Formel des Konzils ist <br>
- unvermischt und ungetrennt<br>
- zwei Naturen, die in einer Person geeinigt sind<br>
Die Naturen vermischen sich
nicht, aber sie f&uuml;hren auch nicht
              zu zwei Individuen. Das Einigende ist die Person. Damit ist f&uuml;r
              das Menschenbild des Abendlandes Neues in Gang gesetzt: Der Mensch
              ist nicht nur in seiner Seele wichtig, die den K&ouml;rper nach
              dem Tod verl&auml;&szlig;t, vielmehr wird der Leib eine himmlische
              Existenz haben. Unser Leib geh&ouml;rt zur Person gleicherweise
              wie das geistige Prinzip, die Seele.</font></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Hatte Jesus
                  neben seinem g&ouml;ttlichen auch einen menschlichen
              Willen?<br>
              Der Monothelethismus verneinte das.</strong><br>
              Es bleibt noch eine Frage: Wenn Jesus nur eine Person ist, der
              Sohn Gottes, der die Menschennatur angenommen hat, dann kann er
              nur einen Willen gehabt haben, denn eine Person zeichnet sich gerade
              dadurch aus, da&szlig; sie nur einem Willen folgt. Das legt die &Uuml;berlegung
              nahe, da&szlig; Jesus sich in seiner Menschheit nicht gegen den
              Sohn Gottes aufgelehnt haben kann, er war immer einer, der auf
              Gott h&ouml;rte. Seine oberste Richtschnur war, den Willen Gottes
              zu erf&uuml;llen. Das ist die Position der Monotheleten, der Verfechter
              nur eines Willens in Jesus. </font></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Jesus hat auch einen menschlichen Willen. Das 3. Konzil von Konstantinopel
              680 </strong><br>
              Wenn Jesus wirklich <a href="menschwerdung_jesu.php">Mensch</a> war,
              mit Seele und Leib, mu&szlig;te er auch einen menschlichen Willen
              haben. Die Menschheit Jesu durfte ja nicht amputiert sein.<br>
              Das 3. Konzils von Konstantinopel 680 verurteilte die Lehre des
              Monotheletismus. Jesus Christus hat nicht nur zwei Naturen, sondern
              auch zwei Willen, einen g&ouml;ttlichen und einen menschlichen.
              Der menschliche Wille ist dem g&ouml;ttlichen untergeordnet.<br>
              </font></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Zitate</strong><br>
  Konzil von Nic&auml;a 325<br>
&#8230;
              . Jesus Christus, .. Sohn Gottes, geboren vom Vater, eingeboren,
              das hei&szlig;t von des Vaters Wesen, Gott von Gott, Licht von
              Licht, wahrhaftiger Gott vom wahrhaftigen Gott, geboren, nicht
              geschaffen, mit dem Vater eines Wesens, durch den alles geschaffen
              ist, was im Himmel und auf Erden ist, der f&uuml;r uns Menschen
              und um unsrer Seligkeit willen herabgekommen und Mensch geworden
              ist &#8230;.<br>
              Die da sagen: es gab eine Zeit, da er nicht war, und ehe er geboren
              ward, war er nicht, und da&szlig; er aus dem ward, was nicht ist,
              oder die ihn f&uuml;r eine andere Hypostase oder Wesen halten oder
              sagen, Gottes Sohn sei geschaffen oder ver&auml;nderlich, die verdammt
              die allgemeine Kirche.</font></p>
            <p><font face="Arial, Helvetica, sans-serif"> Brief des Patriarchen Johannes
                von Antiochien an Cyrill von Alexandrien, 433<br>
                Wir bekennen, da&szlig; unser Herr Jesus Christus, der eingeborene
                  Sohn Gottes, vollkommener Gott und vollkommener Mensch ist mit
                  einer Vernunftseele und einem Leib. Er ist von Ewigkeit her vom
                  Vater gezeugt der Gottheit nach. Am Ende der Tage aber ist derselbe
                  Christus f&uuml;r uns und unseres Heiles willen der Menschheit
                  nach geboren worden aus Maria der Jungfrau. Er ist wesengleich
                  mit dem Vater der Gottheit und wesensgleich der Menschheit nach.
                  Es hat n&auml;mlich eine Vereinigung beider Naturen stattgefunden
                  und deshalb bekennen wir einen Christus, einen Sohn, einen Herrn.
                  Wegen der Vereinigung ohne Vermischung bekennen wir, da&szlig; die
                  heilige Jungfrau Gottesgeb&auml;rerin (Theotokos) ist, weil das
                  g&ouml;ttliche Wort Fleisch und Mensch geworden ist und schon von
                  der Empf&auml;ngnis an den aus ihr genommenen Tempel mit sich
                selbst vereinigt hat. </font></p>
            <p><font face="Arial, Helvetica, sans-serif"> Konzil von Chalzedon 451<br>
  In der Nachfolge der heiligen V&auml;ter also lehren wir alle &uuml;bereinstimmend,
                unseren Herrn Jesus Christus als ein und denselben Sohn zu bekennen:
                derselbe ist vollkommen in der Gottheit und derselbe ist vollkommen
                in der Menschheit: derselbe ist wahrhaft Gott und wahrhaft Mensch
                aus vernunftbegabter Seele und Leib; derselbe ist der Gottheit
                nach dem Vater wesensgleich und der Menschheit nach uns wesensgleich,
                in allem uns gleich au&szlig;er der S&uuml;nde; derselbe wurde
                einerseits der Gottheit nach vor den Zeiten aus dem Vater gezeugt,
                andererseits der Menschheit nach in den letzten Tagen unsertwegen
                und um unseres Heiles willen aus Maria, der Jungfrau (und) Gottesgeb&auml;rerin,
                geboren; ein und derselbe ist Christus, der einziggeborene Sohn
                und Herr, der in zwei Naturen unvermischt, unver&auml;nderlich,
                ungetrennt und unteilbar erkannt wird, wobei nirgends wegen der
                Einung der Unterschied der Naturen aufgehoben ist, vielmehr die
                Eigent&uuml;mlichkeit jeder der beiden Naturen gewahrt bleibt und
                sich in einer Person und einer Hypostase vereinigt; der einziggeborene
                Sohn, Gott, das Wort, der Herr Jesus Christus, ist nicht in zwei
                Personen geteilt oder getrennt, sondern ist ein und derselbe, wie
                es fr&uuml;her die Propheten &uuml;ber ihn und Jesus Christus selbst
                es uns gelehrt und das Bekenntnis der V&auml;ter es uns &uuml;berliefert
                hat.</font></p>
            <p><font face="Arial, Helvetica, sans-serif">3. Konzil von Konstantinopel 680/81<br>
                Die zwei Willen in Jesus<br>
                Ebenso verk&uuml;nden wir gem&auml;&szlig; der Lehre der heiligen
              V&auml;ter, da&szlig; sowohl zwei nat&uuml;rliche Weisen des Wollens
              bzw. Willen als auch zwei nat&uuml;rliche T&auml;tigkeiten ungetrennt,
              unver&auml;nderlich, unteilbar und unvermischt in ihm sind; und
              die zwei nat&uuml;rlichen Willen sind einander nicht entgegengesetzt
              - das sei ferne! -, wie die ruchlosen H&auml;retiker behaupteten;
              vielmehr ist sein menschlicher Wille folgsam und widerstrebt und
              widersetzt sich nicht, sondern ordnet sich seinem g&ouml;ttlichen
              und allm&auml;chtigen Willen unter; denn der Wille des Fleisches
              mu&szlig;te sich regen, sich aber nach dem allweisen Athanasius
              dem g&ouml;ttlichen Willen unterordnen; denn wie sein Fleisch des
              Wortes Gottes genannt wird und ist, so wird auch der nat&uuml;rliche
              Wille seines Fleisches als dem Wort Gottes eigen bezeichnet und
              ist es, wie er selbst sagt: &#8222;Denn ich bin herabgestiegen
              aus dem Himmel, nicht um meinen eigenen Willen zu tun, sondern
              den Willen des Vaters, der mich gesandt hat&quot; (Johannesevangelium
              6,38); dabei nannte er den Willen des Fleisches seinen eigenen
              Willen, da auch das Fleisch ihm eigen geworden ist; denn wie sein
              ganzheiliges und makelloses beseeltes Fleisch trotz seiner Verg&ouml;ttlichung
              nicht aufgehoben wurde, sondern in der ihm eigenen Abgrenzung und
              dem ihm eigenen Begriff verblieb, so wurde auch sein menschlicher
              Wille trotz seiner Verg&ouml;ttlichung nicht aufgehoben, sondern
              ist vielmehr gewahrt, wie der Gottesgelehrte Gregor sagt: &#8222;Denn
              sein Wollen, verstanden in Bezug auf den Erl&ouml;ser, ist Gott
              nicht entgegengesetzt, da es ganz verg&ouml;ttlicht ist&quot;.</font></p>
            <p><font face="Arial, Helvetica, sans-serif">              Origines v. Alexandrien, 185-254 von den Prinzipien IV,4,4<br>
                Als nun der Sohn Gottes zum Heile der Menschheit sich den Menschen
                offenbaren und unter den Menschen wandeln wollte, nahm er nicht,
                wie manche glauben, nur einen menschlichen Leib an, sondern auch
                eine Seele an. Diese war zwar ihrer Natur nach unseren Seelen gleich,
                aber nach ihrer Willensrichtung und ihrem sittlichen Verhalten
                ihm selber gleich und so beschaffen, da&szlig; sie alle Entschl&uuml;sse
                und Heilsvorhaben des Logos und der Weisheit unverf&auml;lscht
                ausf&uuml;hren konnte.</font></p>
            <p><font face="Arial, Helvetica, sans-serif">4. Synode von Toledo 633<br>
  Derselbe Christus, unser Herr Jesus, einer aus der Dreifaltigkeit,
              hat &#8211; abgesehen von der S&uuml;nde &#8211; einen an Seele
              und Leib vollkommenen Menschen angenommen, er blieb dabei, was
              er war, er nahm an, war er nicht war; er war gleich dem Vater in
              seiner G&ouml;ttlichkeit, er war geringer als der Vater in seiner
              Menschlichkeit und besa&szlig; in einer Person die Eigenschaften
              zweier Naturen; Naturen waren n&auml;mlich in ihm zwei, Gott und
              Mensch, nicht aber zwei S&ouml;hne und zwei G&ouml;tter, sondern
              die eine und selbe Person in beiden Naturen.</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Konzil von Frankfurt
                794<br>
                Wir bekennen unseren Herrn Jesus Christus als wahren Gott und
                wahren Mensch in einer Person. Er blieb also die Person des Sohnes
                in der Trinit&auml;t, zu welcher Person die menschliche Natur
                hinzukam, damit auch eine Person sei Gott und Mensch &#8211; nicht
                ein verg&ouml;ttlichter
                Mensch oder ein erniedrigter Gott, sondern Gott-Mensch und Mensch-Gott;
                wegen der Einheit der Person ist der eine Sohn Gottes zugleich
                Menschensohn, vollkommener Gott und vollkommener Mensch. Vollkommen
                ist der Mensch aber nur mit Leib und Seele.</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Text: Eckhard Bieger
                S.J.<br>
            </font>&copy;<a href="http://www.kath.de"> <font face="Arial, Helvetica, sans-serif">www.kath.de</font></a></p>
            </TD>
          <TD background=boxright.gif><IMG height=8 alt="" 
            src="boxright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=8 alt="" src="boxbottomleft.gif" 
            width=8></TD>
          <TD background=boxbottom.gif><IMG height=8 alt="" 
            src="boxbottom.gif" width=8></TD>
          <TD><IMG height=8 alt="" src="boxbottomright.gif" 
            width=8></TD></TR></TBODY></TABLE></TD><td style='vertical-align:top;'>
 <script type="text/javascript"><!--
        google_ad_client = "pub-9537752113242914";
        google_ad_width = 336;
        google_ad_height = 280;
        google_ad_format = "336x280_as";
        google_ad_type = "text_image";
        //2006-10-23: Lexikon Symbole
        google_ad_channel = "8495253357";
        //--></script>
        <script type="text/javascript"
          src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
        </script>
</td>
</TR>
  <TR>
    <TD vAlign=top align=left>&nbsp; </TD>
  </TR></TBODY></TABLE></BODY></HTML>
