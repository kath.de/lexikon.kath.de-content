---
title: Arianismus
author: 
tags: 
created_at: 
images: []
---


# ** Motiv für das Weihnachtsfest*
***Für die junge christliche Kirche war es eine große Herausforderung, die Botschaft von Jesus, dem Sohn Gottes und Erlöser aller Menschen, gegenüber der griechischen Philosophie zu vertreten. Die Theologen stießen nicht mehr auf eine religiöse Kultur mit vielen Götterkulten, so wie später die Missionare bei den germanischen Stämmen. Die Griechen hatten schon seit der Zeit Platons auf denkerischem Weg die Einsicht gewonnen, daß das Göttliche nur als Eines gedacht werden kann. Der Grundgedanke, der zu dieser Einsicht führte, war der der Vollkommenheit. Etwas, das teilbar ist, kann sich verändern und kann auch zerstört werden. Auch die menschliche Seele wird als unteilbar gedacht und damit als unsterblich, wie es Platon in seinem Dialog Phaidon entwickelt. Die Frage spitze sich Anfang des 4. Jahrhunderts zu, ob Jesus wirklich Gottes Sohn sein konnte. Diese Auseinandersetzugn hat sicher Weihanchten, das Fest der Menschwerdung Gottes, beflügelt. 

# **Ungenügende Antworten auf die Herausforderung der griechsichen Philosophie:****
 Wenn die christliche Predigt auf das griechische Denken trifft, dann wird sie an den Prämissen dieses Denkens gemessen. Den Griechen war nicht zuzumuten, ein Gottesbild zu übernehmen, das sie wieder zu den Göttervorstellungen zurückzuführen schien, die der volkstümlichen Religiosität zwar entgegenkommen, aber als Rückschritt gegenüber dem erreichten Stand des Denkens gesehen werden konnten. Der Priester Arius aus Ägypten machte sich zum Sprecher der griechischen Überzeugungen. Da der ***[Doketismus](christologische_streitigkeiten.php)  wie auch der   ***[Adoptianismus ](gottessohn.php)überwunden waren, konnte Arius nicht erklären, Jesus habe nur einen Scheinleib angenommen  noch daß er nach seiner Geburt erst von Gott als Messias berufen und als Sohn „adoptiert“ worden sei. Er nahm die Brücke, die der Evangelist Johannes zum griechischen Denken geschlagen hatte, auf und sagte, daß der Logos Mensch geworden ist. Aber dieser Logos ist nicht der ewige Sohn Gottes, der mit Gott gleich ist, sondern ein geschaffenes Wesen.             

# **Theologische Argumente****
# **Was auf den ersten Blick als eine Aussage über den Sohn erscheint, ist eigentlich eine Aussage über den Vater. Wenn über den Messias Jesus Christus gesagt wird, er ist „Nicht-Gott“, dann ist das zuerst eine Festlegung Gottes: Gott kann, soll er wirklich Gott sein, keinen Sohn aus sich gezeugt haben, der kein Geschöpf, sondern wie der Vater Gott ist. Aus der inneren Logik des Denkens schien es den griechischen Intellektuellen unausweichlich, daß Gott nur einer ist. Wenn das Denken den wirklichen Gott erreichen will, dann kann es ihn nur als unteilbar, als Einen denken. Genau das bekennen wir bis heute im Glaubensbekenntnis: Ich glaube an den einen Gott. Aber kann Gott vom Denken so festgelegt werden?**
 Dieser eine Gott wurde von den Griechen als ein Wesen gedacht, das seine Göttlichkeit verlieren muß, wenn er mit dem Endlichen in Berührung kommt. Deshalb hat Gott vor der Erschaffung der Welt den Logos geschaffen. Dieser entspricht dem inneren Logos, dem Denken in Gott, ist aber endlich und die vermittelnde Instanz, um die sichtbare Welt zu schaffen. Zu dieser sichtbaren Welt hat Gott keine Beziehung. Er kann deshalb keine haben, weil er sich dann an die Vielheit verlieren würde und damit sein Gott-Sein in Frage stünde. In den Texten, die von Arius überliefert sind, kommt diese Grundintention zum Ausdruck: Gott ist eine in sich geschlossene Monade. (s.u. Zitate) Er kann etwas schaffen, aber das Geschaffene hat keinen Zugang zu ihm. Dieser weltenferne Gott, der in seiner Gottheit sozusagen eingeschlossen ist und, um Gott zu bleiben, in absoluter Distanz zur Welt stehen muß, war nicht der Gott, den das jüdische Volk in seiner Geschichte kennengelernt hatte und den Jesus in seinen Gleichnisreden und seiner Predigt den Menschen nahe gebracht hatte. Arius stellt also nicht nur einen Teil der christlichen Botschaft in Frage, sondern zeichnet einen ganz anderen Gott als er von den Juden erfahren und im Geist von den Christen erlebt wurde. Gott ist nicht fern, sondern den Menschen ganz nah. Er hat sich den Menschen zugewandt. Er selbst ist gekommen, um die Menschen zu retten. Das ***[Konzil von Nicäa](gottessohn.php)              hat deshalb die biblische Überlieferung bestätigt und daran festgehalten, daß Jesus Christus der Sohn Gottes ist. Er offenbart den Vater, weil er allein den Vater kennt. Er ist in seinem Sohnsein nicht geschaffen, sondern in einem innergöttlichen Vorgang gezeugt. Daraus entwickelten sich neue Fragen, nämlich wie in Jesus Christus das Verhältnis zwischen dem göttlichen ***[Logos und dem Menschen Jesus](christologische_streitigkeiten.php)              gedacht werden kann. 

# **Zitate****
 Die Thesen des Arius finden sich in einem von ihm verfaßten Glaubensbekenntnis, das er 320 in einem Brief an den Patriarchen Alexander von Alexandrien niedergeschrieben hat:***

 Wir kennen nur einen Gott, den allein ungewordenen, den allein ewigen, allein ursprungslosen, allein wahren, allein die Unsterblichkeit Besitzenden, allein weisen, allein guten; den Alleinherrscher, den Richter aller, den Ordner und Verwalter, unwandelbar und unveränderlich, gerecht und gut, den Gott des Gesetzes, der Propheten und des Neuen Bundes, der den eingeborenen Sohn vor ewigen Zeiten hervorgebracht hat, durch den er auch die Äonen und das All machte; er hat ihn nicht dem Scheine nach hervorgebracht, sondern in Wahrheit, als in eigenem Willen Wesenden, als Unwandelbaren und Unveränderlichen, als Gottes vollkommenes Geschöpf, aber nicht wie eines der Geschöpfe …..***

 Durch den Willen Gottes vor den Zeiten und Äonen geschaffen, der vom Vater des Leben hat sowie die Ehre, so daß der Vater mit ihm zusammen existiert. Denn der Vater hat sich nicht selbst beraubt, da er ihm als Erbe all jenes gegeben hat, was er ungeschaffen in sich trägt. Er ist die Quelle von allem. So gibt es drei Subjekte (Hypostasen). Gott der Vater ist die Ursache aller, ganz allein ohne Ursprung, der Sohn aber, vom Vater vor der Zeit hervorgebracht und vor den Äonen geschaffen und gegründet, war nicht, bevor er hervorgebracht wurde, …. Er hat vom Vater seien Existenz erhalten. Denn er ist nicht ewig oder gleich ewig oder gleich ungeworden wie der Vater, noch hat er zugleich mit dem Vater das Sein … der Vater ist, da er wie eine Monade oder ein Prinzip von allem ist, auch Gott vor allem. So ist er auch vor dem Sohn. ….             

# **Athanasius hat die sog Blasphemien des Arius in seiner Schrift über das Konzil von Nicäa formuliert:**
 Gott selbst, wie er ist, ist unaussprechlich für alle.***

 Er allein hat weder Seinesgleichen noch einen ihm Ähnlichen, noch einen von gleicher Herrlichkeit.***

 Wir nennen ihn den Ungewordenen wegen des von Natur aus Gewordenen ….***

 Der Ursprungslose setzte den Sohn als Anfang der Gewordenen und***

 bestimmte sich zum Sohn den, den er als Kind gemacht hatte; ***

 dieser hat nichts Gott-Eigenes in dem, was ihm auf Grund seines Subjektseins (Hypostase) eigen ist;***

 denn er ist ihm nicht gleich und auch nicht wesensgleich. ….***

 Sicher ist die Aussage, daß Gott für alle unsichtbar ist;***

 er selbst ist unsichtbar allen, die durch den Sohn sind, auch dem Sohn selber;***

 Ausdrücklich sage ich aber, wie dem Sohn der Unsichtbare sichtbar ist:***

 Entsprechend der Kraft und dem eigenen Maß, mit dem der Logos schaut kann der Sohn den Vater sehen, wie es rechtens ist.***

 Es gibt also eine Trias, nicht aber von gleichen Ehren, denn ihre Hypostasen sind einander nicht vermischt;***

 denn die eine hat gegenüber der anderen unendlich mehr Ehre.***

 Dem Wesen nach ist der Vater dem Sohn gegenüber fremd, da Er ursprungslos west. …***

 Solange der Sohn nicht ist, ist der Gott nicht Vater.***

 Zuvor war der Sohn nicht, trat aber ins Dasein durch den väterlichen Willen; …..***

 Durch Gottes Willen hat der Sohn dieses Alter und diese Größe;***

 Seine Existenz aus Gott ist bestimmt durch ein „seit wann“ und ein „von woher“ und ein „von da ab“; …***

 Um zusammenzufassen: Der Gott west für den Sohn als Unaussprechlicher.***

 er ist nämlich für sich, was er ist, das heißt er ist unsagbar,***

 so daß der Sohn selbst nichts von dem Gesagten adäquat auszusagen versteht. ….***

 auch der Sohn hat seine (des Vaters) Wesenheit nicht gesehen,***

 da er als Sohn in Wirklichkeit nur durch den Willen des Vaters besteht.***

 Wem ist also gestattet zu sagen, daß der, der aus dem Vater ist,***

 den, der ihn hervorgebracht hat, erkennen oder begreifen kann?***

 Es ist also deutlich, daß der, der einen Anfang hat, den Anfanglosen, so wie er ist, nicht umgreifen und nicht erfahren kann. 

# ****
 Wie Arius sind auch das Evangelium des Johannes und seine Briefe sehr zurückhaltend in der Frage, ob der Mensch Zugang zu Gott finden kann. Bereits im Prolog des Evangeliums heißt es: „Niemand hat Gott je gesehen.“ (Kap. 1,18) In dem 1. Johannesbrief heißt es: „niemand hat Gott je geschaut.“ Kap 4,12***

 Aber anders als für Arius ist die Distanz zwischen dem Menschen und Gott überwunden, durch den Sohn. Im Prolog endet der Satz nicht damit „Niemand hat Gott je gesehen.“, sondern wird fortgesetzt: „Der Einzige, der Gott ist und am Herzen des Vaters ruht, er hat Kunde gebracht.“ (Kap. 1,18)***

 Der oben zitierte Satz aus dem 1. Johannesbrief beinhaltet ein kurzes Glaubensbekenntnis: „Niemand hat Gott je geschaut. Wenn wir einander lieben, bleibt Gott in uns, und seine Liebe ist in uns vollendet. Daran erkennen wir, daß wir in ihm bleiben und er in uns bleibt: Er hat uns seinen Geist gegeben. Wir haben gesehen und bezeugen, daß der Vater den Sohn gesandt hat als den Retter der Welt. Wer bekennt, daß Jesus der Sohn Gottes ist, in dem bleibt Gott, und er bleibt in Gott.“ (Kap. 4,12-15)***

 Im Evangelium findet sich ein Abschnitt, der wir in einer Lehrstunde aufzeigt, daß wir Menschen Gott in seinem menschgewordenen Sohn erkennen.***


„ Philippus sagte zu ihm: Herr, zeig uns den Vater; das genügt uns. Jesus antworte ihm: Schon so lange bin ich bei euch, und du hast mich nicht erkannt, Philippus? Wer mich gesehen hat, hat den Vater gesehen. Wie kannst du sagen: Zeig uns den Vater? Glaubst du nicht, daß ich im Vater bin und der Vater in mir ist? Die Worte, die ich euch sage, habe ich nicht aus mir selbst. Der Vater, der in mir bleibt, vollbringt seine Werke. Glaubt mir doch, daß ich im Vater bin und daß der Vater in mir ist.“ (Kap. 14,8-11)***

 Gott ist nicht ein Gott, der in sich als Monade abgeschlossen, unzugänglich, nur für sich allein existiert, sondern der die Beziehung zum Menschen will. 

# ****
 ***Eckhard Bieger S.J. 

©***[ ***www.kath.de](http://www.kath.de) 
