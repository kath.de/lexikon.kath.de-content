<HTML><HEAD><TITLE>Gottesbeweis aus der religi&ouml;sen Erfahrung</TITLE>
<meta name="title" content="Philosophie&amp;Theologie">
<meta name="author" content="Redaktion kath.de">
<meta name="publisher" content="kath.de">
<meta name="copyright" content="kath.de">
<meta name="description" content="">
<meta name="abstract" content="Lexikon &uuml;ber Philosophie&amp;Theologie">
<meta http-equiv="content-language" content="de">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="date" content="2006-00-01">
<meta name="robots" content="index,follow">
<meta name="revisit-after" content="10 days">
<meta name="revisit" content="after 10 days">
<meta name="DC.Title" content="Philosophie&amp;Theologie">
<meta name="DC.Creator" content="Redaktion kath.de">
<meta name="DC.Contributor" content="J�rgen Pelzer">
<meta name="DC.Rights" content="kath.de">
<meta name="DC.Publisher" content="kath.de">
<meta name="DC.Date" content="2006-00-01">
<meta name="DC.Description" content="Lexikon &uuml;ber Philosophie&amp;Theologie">
<meta name="DC.Language" content="de">
<meta name="DC.Type" content="Text">
<meta name="DC.Format" content="text/html">
<meta name="DC.Identifier" content="http://www.kath.de/lexikon/philosophie_theologie/">
<meta name="keywords" lang="de" content="Religionspsychologie, Freiheit, Glauben, Wissen, religi�se Erfahrungen, Ungen�gen">
</HEAD>
<BODY bgColor=#ffffff leftMargin=6 topMargin=6 marginheight="6" marginwidth="6">
<TABLE cellSpacing=0 cellPadding=6 width="100%" border=0>
  <TBODY>
  <TR>
    <TD vAlign=top align=left width=100> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b>Philosophie&amp;Theologie</b></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td> <?php include("logo.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10"> <?php include("az.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </TD>
    <TD vAlign=top rowSpan=2>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
        <TBODY>
        <TR vAlign=top align=left>
          <TD width=8><IMG height=8 alt="" 
            src="boxtopleftcorner.gif" width=8></TD>
          <TD background=boxtop.gif><IMG height=8 alt="" 
            src="boxtop.gif" width=8></TD>
          <TD width=8><IMG height=8 alt="" 
            src="boxtoprightcorner.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxtopleft.gif><IMG height=8 alt="" 
            src="boxtopleft.gif" width=8></TD>
          <TD bgColor=#e2e2e2> 
            <H1><font face="Arial, Helvetica, sans-serif">Gottesbeweis aus
                der religi&ouml;sen Erfahrung</font></H1>
          </TD>
          <TD background=boxtopright.gif><IMG height=8 
            alt="" src="boxtopright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=13 alt="" src="boxdividerleft.gif" 
            width=8></TD>
          <TD background=boxdivider.gif><IMG height=13 
            alt="" src="boxdivider.gif" width=8></TD>
          <TD><IMG height=13 alt="" 
            src="boxdividerright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxleft.gif><IMG height=8 alt="" 
            src="boxleft.gif" width=8></TD>
          <TD class=L12>
            <P><STRONG><font face="Arial, Helvetica, sans-serif">                Erfahrungen
                  f&uuml;hren zu Gott</font></STRONG></P>
            <P><font face="Arial, Helvetica, sans-serif">Menschen, die sich zu
                einer Religion bekennen, berufen sich selten auf Argumente, sondern
                auf Erfahrungen. Sie f&uuml;hlen sich aus
              einer Krise oder Krankheit gerettet, haben zu einem tieferen Verst&auml;ndnis
              ihrer Existenz gefunden oder berichten von einer mit Licht vergleichbaren
              Erfahrung. Deshalb spricht man in religi&ouml;sen Zusammenh&auml;ngen
              von Erleuchtung. Charakteristisch f&uuml;r diese Erfahrungen ist
              das Bewu&szlig;tsein einer gr&ouml;&szlig;eren Tiefe, einer umfassenderen
              Sicht der Welt. Es wird nicht wie in den Naturwissenschaften ein
              einzelner Zusammenhang, ein bestimmter Ausschnitt der Wirklichkeit,
              die Funktionsweise eines Enzyms erkannt, sondern die eigene Existenz
              wird tiefer verstanden und in einem gr&ouml;&szlig;eren Zusammenhang
              gesehen.<br>
              F&uuml;r den, der eine solche Erfahrung gemacht hat, ist diese &uuml;berzeugend.
              Religi&ouml;se Pers&ouml;nlichkeiten, die einen spirituelle Schule,
              einen Orden gr&uuml;nden, berufen sich meist auf derartige Erfahrungen.
              F&uuml;r diejenigen, die ihnen folgen, haben diese Erfahrungen
              eine besondere Autorit&auml;t, auch wenn sie selbst diese religi&ouml;sen
              Erfahrungen nicht gemacht haben. Auch in der Bibel werden von den
              gro&szlig;en Gestalten nicht nur deren Taten, sondern auch religi&ouml;se
              Erfahrungen berichtet. Sie haben eine Stimme geh&ouml;rt (Abraham),
              haben besondere Tr&auml;ume gehabt (Joseph, den seine Br&uuml;der
              nach &Auml;gypten verkauft haben), haben einen Dornbusch gesehen,
              der brannte, ohne zu verbrennen (Moses), Gott hat zu ihnen aus
              einem sanften Wind gesprochen (Elias), Jesus ist ihm in einem besonderen
              Licht erschienen (Paulus).<br>
              Die Erfahrungen begr&uuml;nden meist etwas Neues, im Leben des
              einzelnen, oder wenn derjenige, der aus den Erfahrungen einen besonderen
              Auftrag heraus geh&ouml;rt hat, seine religi&ouml;se Sicht weitergibt
              und eine spirituelle Schule gr&uuml;ndet. Allerdings, auch wenn
              sich andere einem Menschen anschlie&szlig;en, der sich auf religi&ouml;se
              Erfahrungen beruft, und die Erfahrungen damit eine Verbindlichkeit
              bekommen, viele werden nicht &uuml;berzeugt. Es l&auml;&szlig;t
              sie unber&uuml;hrt, wenn sie von den Erfahrungen h&ouml;ren. Das
              gilt sogar innerhalb einer Religionsgemeinschaft. Die spirituellen
              Protagonisten, z.B. die Ordensgr&uuml;nder wie der heilige Franziskus,
              sind f&uuml;r einige interessant, andere bleiben distanziert. Als
              Mitglied einer Religionsgemeinschaft mu&szlig; der einzelne sich
              nicht f&uuml;r jede religi&ouml;se Erfahrung interessieren. Er
              wird demjenigen, der von einer solchen Erfahrung berichtet, jedoch
              nicht absprechen, da&szlig; es solche Erfahrungen geben kann, auch
              wenn er selbst solche Erfahrungen nicht gemacht haben sollte.</font></P>
            <p><font face="Arial, Helvetica, sans-serif">Auch Menschen, die keine
                religi&ouml;se Erfahrung gemacht haben,
              die sie auf einen bestimmten Tag datieren k&ouml;nnten, haben einen
              Zugang zum Religi&ouml;sen. Es ist einmal das Bewu&szlig;tsein,
              da&szlig; es neben der greifbaren Welt eine gr&ouml;&szlig;ere,
              umfassendere Wirklichkeit gibt, an der der Mensch durch seine Geistigkeit
              teilhat und die weit &uuml;ber das hinausreicht, was der Mensch
              in seinem Erkenntnisverm&ouml;gen klar und deutlich erfassen k&ouml;nnte.
              Karl Rahner beschreibt das als den Horizont, in dem wir das einzelne
              erkennen. Dieser Horizont &uuml;bersteigt uns, wir k&ouml;nnen
              ihn nicht so umgreifen wie eine einzelne Erkenntnis. Der Horizont
              ist uns in unserem Erkennen mit gegeben, wir erfassen ihn nicht
              direkt sondern nur insofern wir etwas Konkretes wahrnehmen und
              verstehen, das in dem <a href="gottesbeweis_wahrheit.php">umfassenden
              Horizont</a> auftaucht.  Simone Weil spricht von einem Sehnen, das uns mit allem,
              was wir einzelne erkennen und erreichen k&ouml;nnen, unzufrieden
              sein l&auml;&szlig;t. (s. bei Zitate unten)<br>
              Ein weiteres Grundgef&uuml;hl teilen die Menschen, n&auml;mlich
              da&szlig; mit dieser Welt etwas grunds&auml;tzlich nicht in Ordnung
              ist und das der Mensch nicht in Ordnung bringen kann. In dieses
              Grundgef&uuml;hl mischt sich auch das Bewu&szlig;tsein des eigenen
              Versagens. Die eigene Schuld f&uuml;llt diese Grundannahme aber
              nicht aus, da&szlig; die Welt etwas Grunds&auml;tzliches braucht,
              damit sie in den gew&uuml;nschten Zustand kommt. Neben meiner
              Schuld gibt es noch vieles andere, was die Welt unvollkommen macht.
              Die
              Religionen legen diese Grunderfahrung als das Bewu&szlig;tsein
              f&uuml;r die
              Erl&ouml;sungsbed&uuml;rftigkeit
              aus, das alle Menschen in sich tragen. Die Revolutionen leiten
              aus dem Grundgef&uuml;hl den Auftrag an die Politik ab, grunds&auml;tzlich
              etwas zu ver&auml;ndern. <br>
              Die Ahnung, da&szlig; es jenseits dieser Welt noch etwas geben
              mu&szlig; und da&szlig; diese Welt einer tiefgreifenden Ver&auml;nderung
              bedarf, erkl&auml;rt vielleicht, warum Menschen, die von einer
              konkreten religi&ouml;sen Erfahrung berichten, nur bei eingien,
              nicht aber bei allen anderen auf Ablehnung sto&szlig;en.</font></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>&Uuml;berzeugungskraft religi&ouml;ser Erfahrungen</strong><br>
            </font><font face="Arial, Helvetica, sans-serif">Es
                zeigt sich n&auml;mlich, da&szlig; religi&ouml;se Erfahrungen &Uuml;berzeugungskraft
              haben, nicht nur f&uuml;r denjenigen, der sie gemacht hat, sondern
              auch f&uuml;r andere, die sich von dem religi&ouml;sen Weg, der
              Spiritualit&auml;t des anderen ansprechen lassen. Das Besondere
              der religi&ouml;sen Erfahrung besteht darin, da&szlig; der einzelne
              sie nicht herbeif&uuml;hren kann, sondern da&szlig; sie wie ein
              Geschenk erlebt werden. Im Christentum gilt das auch f&uuml;r den
              Gauben des einzelnen. Schon in der Bibel wird deutlich gesagt,
              da&szlig; der Glaube nicht durch menschliche Anstrengung erreicht
              werden kann, sondern von Gott geschenkt werden mu&szlig;. Damit
              entzieht sich die religi&ouml;se Erfahrung dem naturwissenschaftlichen
              Denken. Denn die Methodik der Naturwissenschaften sagt, da&szlig; eine
              Beobachtung, z.B. in der Atomphysik oder der Zellbiologie, an jedem
              Ort der Welt wiederholt werden kann. Die Natur ist jederzeit verf&uuml;gbar.
              Hier liegt der Unterschied der Verfahren, der dazu f&uuml;hrt,
              da&szlig; Religion nicht einfach Objekt der Naturwissenschaften
              werden kann. Die religi&ouml;sen Erfahrungen kann man nicht am
              Objekt beobachten. Sie fordern ein Subjekt, das von sich berichtet.
              Zwar kann die Hirnforschung die Str&ouml;me im Hirn beobachten,
              wenn z.B. ein Mensch meditiert. Aber der Inhalt des Bewu&szlig;tseins
              ist prinzipiell nicht mit Magnetresonanz-Tomographie o.a. Verfahren
              abzulesen.</font></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Religionspsychologie</strong><br>
              Auch
                wenn religi&ouml;se
                Erfahrungen nicht wie Beobachtungen an der Zelle oder dem Atomkern
                beliebig an jedem Ort der Welt wiederholbar
              sind, sind religi&ouml;se Erfahrungen zum Objekt der Forschung,
              der Religionspsychologie, geworden. Die Tatsache solcher Erfahrungen
              wird kaum bestritten. Aber ist es gerechtfertigt, von solchen Erfahrungen
              auf die Existenz Gottes zu schlie&szlig;en? Ein Vergleich hilft
              einen Schritt weiter:<br>
              Menschen berichten von tiefen Liebeserfahrungen. Auch wenn diese
              hormonell bedingt sein m&ouml;gen, die Erfahrungen sind nicht einfach
              herstellbar. Die Struktur der Liebeserfahrungen &auml;hnelt der
              der religi&ouml;sen. Sie k&ouml;nnen nicht einfach hergestellt
              werden, sondern haben auch den Charakter eines Geschenkes. Es besteht
              jedoch ein wichtiger Unterschied. Auch wenn die beiden Erfahrungen
              in ihrer Struktur vergleichbar sind, die Liebeserfahrung macht
              es nicht notwendig, auf die Existenz des geliebten Partners zu
              schlie&szlig;en. Man kann ihn den Freunden und Freundinnen vorstellen,
              er, bzw. sie haben eine physische Realit&auml;t. Da Gott kein Teil
              der wahrnehmbaren Welt ist, ist seine Existenz nicht in der Weise
              aufweisbar wie die des geliebten Partners. <br>
              Der Skeptiker k&ouml;nnte nun in Bezug auf die Liebes- wie auf
              die religi&ouml;se Erfahrung behaupten, diese seien Illusionen
              oder durch Hormone oder Gehirnstr&ouml;me bedingt. <br>
              Diesem Einwand kann man auf der Ebene der naturwissenschaftlichen &Uuml;berpr&uuml;fbarkeit
              nicht entgegentreten. Zwar kann der Skeptiker seine Interpretation
              auch nicht beweisen, er kann aber auch nicht mit Argumenten &uuml;berzeugt
              werden, da&szlig; aus der religi&ouml;sen Erfahrung die Existenz
              Gottes f&uuml;r jeden einsichtig folge. Es gibt allerdings eine
              andere Ebene, auf der deutlich wird, da&szlig; diese Erfahrungen
              nicht verfliegen, wenn die Hormone nicht mehr ausgesch&uuml;ttet
              werden oder die Gehirnstr&ouml;me nicht mehr flie&szlig;en. Denn
              Menschen ziehen aus der Liebeserfahrung weitreichende Konsequenzen.
              Sie wollen mit dem Geliebten zusammenleben, sie &uuml;berstehen
              Konflikte, helfen dem anderen, wenn dieser krank wird. Wie die
              Liebeserfahrungen f&uuml;hren auch die religi&ouml;sen Erfahrungen
              auf der Ebene des Verhaltens zu weitreichenden Konsequenzen. Menschen
              ver&auml;ndern ihr Leben, manche ziehen sich in die W&uuml;ste
              zur&uuml;ck, andere widmen ihr Leben den &Auml;rmsten und verzichten
              auf jeden Vorteil. Auch wenn diese Erfahrungen nicht empirisch &uuml;berpr&uuml;fbar
              sind, sie sind keine theoretischen, sondern haben Konsequenzen
              f&uuml;r das Verhalten der Menschen. <a href="glauben_wissen.php">Religion
              wie auch Liebe</a> sind
              immer empirisch. Sie haben auch immer mit den Wertvorstellungen
              der Menschen zu tun. Wer einen Partner, einen Freund gefunden hat,
              f&uuml;hlt sich ohne &auml;u&szlig;eren Zwang der Treue zu der
              Beziehung, zu der Freundschaft verpflichtet. So haben auch religi&ouml;se
              Erfahrungen in der Regel eine h&ouml;here Bereitschaft zur Folge,
              das Sittengesetz in seinem Leben zu verwirklichen, in seinem ethischen
              Verhalten weniger lax zu sein. Die N&auml;he, die religi&ouml;se
              Erfahrungen zu dem Anspruch der 10 Gebote haben, wird von vielen
              Philosophen als ein Weg zur Erkenntnis Gottes gesehen, das <a href="gottesbeweis_gewissen.php">Gewissen</a>              wird als <a href="gottesbeweis_gewissen.php">Stimme Gottes</a> gedeutet, die Verpflichtung, das Gute zu
              tun, als Hinweis auf ein <a href="gottesbeweis_hoechstes_gut.php">h&ouml;chstes Gutes</a>.</font></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Zitate</strong><br>
&#8222;
              Es gibt eine Realit&auml;t au&szlig;erhalb der Welt, das hei&szlig;t:
              au&szlig;erhalb von Raum und Zeit, au&szlig;erhalb des geistigen
              Universums des Menschen, au&szlig;erhalb jeglicher Sph&auml;re,
              die dem Verm&ouml;gen des Menschen irgendwie zug&auml;nglich ist.
              Dieser Realit&auml;t entsprechend gibt es im Innersten des menschlichen
              Herzens das Sehnen nach etwas absolut Gutem, ein Sehnen, das f&uuml;r
              immer besteht und durch keinen Gegenstand in dieser Welt jemals
              gestillt wird.&#8220; Simone Weil 1957, S. 74</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Es gibt ein Unbehagen, &#8222;ein gemeinsames Gef&uuml;hl, da&szlig; mit
              uns etwas verkehrt ist, so wie wir von Natur aus daran sind.&#8220; Weiter
              gibt es &#8222;ein Gef&uuml;hl, da&szlig; wir von der Verkehrtheit
              erl&ouml;st werden, dadurch, da&szlig; wir die richtige Verbindung
              mit h&ouml;heren Kr&auml;ften herstellen. William James; The Varieties
              of Religious Experience, Cambridge MA, 1985, 508/400</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Der Religi&ouml;se Mensch &#8222;wird sich bewu&szlig;t, da&szlig; sein
              h&ouml;herer Teil angrenzt an und in Kontinuit&auml;t steht ist
              mit einem MEHR derselben Qualit&auml;t, das wirksam ist im Universum
              au&szlig;erhalb von ihm und mit dem er in funktionierender Ber&uuml;hrung
              bleiben und bei dem er gewisserma&szlig;en an Bord gehen und sich
              retten kann, wenn sein gesamtes niedriges Sein beim Schiffbruch
              zerborsten ist.&#8220; <br>
              William James, ebd. 508/400</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Es gibt einen Geisteszustand,
                den religi&ouml;se Menschen, aber
              niemand anders kennt, in dem der Wille, uns selbst zu behaupten
              und unser Eigenes festzuhalten, durch die Bereitwilligkeit ersetzt
              worden ist, unseren Mund zu schlie&szlig;en und nichts zu sein
              in den Fluten und Wasserhosen Gottes ... die Zeit der Anspannung
              in unserer Seele ist vor&uuml;ber, und die einer gl&uuml;cklichen
              Entspannung, ruhigen tiefen Atmens, ewiger Gegenwart, ohne eine
              disharmonische Zukunft, die Sorgen macht, ist angebrochen.&#8220;<br>
              William James, ebd. 47/46            </font></p>
            <P><font face="Arial, Helvetica, sans-serif">Eckhard Bieger</font></P>
            <P>&copy;<a href="http://www.kath.de"> <font face="Arial, Helvetica, sans-serif">www.kath.de</font></a></P>
            </TD>
          <TD background=boxright.gif><IMG height=8 alt="" 
            src="boxright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=8 alt="" src="boxbottomleft.gif" 
            width=8></TD>
          <TD background=boxbottom.gif><IMG height=8 alt="" 
            src="boxbottom.gif" width=8></TD>
          <TD><IMG height=8 alt="" src="boxbottomright.gif" 
            width=8></TD></TR></TBODY></TABLE></TD><td style='vertical-align:top;'>
 <script type="text/javascript"><!--
        google_ad_client = "pub-9537752113242914";
        google_ad_width = 336;
        google_ad_height = 280;
        google_ad_format = "336x280_as";
        google_ad_type = "text_image";
        //2006-10-23: Lexikon Symbole
        google_ad_channel = "8495253357";
        //--></script>
        <script type="text/javascript"
          src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
        </script>
</td>
</TR>
  <TR>
    <TD vAlign=top align=left>&nbsp; </TD>
  </TR></TBODY></TABLE></BODY></HTML>
