<HTML><HEAD><TITLE>Nestorianer</TITLE>
<META http-equiv=Content-Type content="text/html; charset=iso-8859-1"><LINK 
title=fonts href="kaltefleiter.css" type=text/css 
rel=stylesheet>
<META content="MSHTML 5.50.4134.600" name=GENERATOR></HEAD>
<BODY bgColor=#ffffff leftMargin=6 topMargin=6 marginheight="6" marginwidth="6">
<TABLE cellSpacing=0 cellPadding=6 width="100%" border=0>
  <TBODY>
  <TR>
    <TD vAlign=top align=left width=100> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b>Philosophie&amp;Theologie</b></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td> <?php include("logo.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10"> <?php include("az.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </TD>
    <TD vAlign=top rowSpan=2>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
        <TBODY>
        <TR vAlign=top align=left>
          <TD width=8><IMG height=8 alt="" 
            src="boxtopleftcorner.gif" width=8></TD>
          <TD background=boxtop.gif><IMG height=8 alt="" 
            src="boxtop.gif" width=8></TD>
          <TD width=8><IMG height=8 alt="" 
            src="boxtoprightcorner.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxtopleft.gif><IMG height=8 alt="" 
            src="boxtopleft.gif" width=8></TD>
          <TD bgColor=#e2e2e2> 
            <H1><font face="Arial, Helvetica, sans-serif">Nestorianer</font></H1>
          </TD>
          <TD background=boxtopright.gif><IMG height=8 
            alt="" src="boxtopright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=13 alt="" src="boxdividerleft.gif" 
            width=8></TD>
          <TD background=boxdivider.gif><IMG height=13 
            alt="" src="boxdivider.gif" width=8></TD>
          <TD><IMG height=13 alt="" 
            src="boxdividerright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxleft.gif><IMG height=8 alt="" 
            src="boxleft.gif" width=8></TD>
          <TD class=L12>
            <P><STRONG><font face="Arial, Helvetica, sans-serif">Eine andere
                  Vorstellung von der Einheit in Jesus Christus</font></STRONG></P>
            <P><font face="Arial, Helvetica, sans-serif">Es ist die gleiche Frage
                wie bei den <a href="monophysiten.php">Monophysiten</a> &#8211; n&auml;mlich
              wie man das Zusammen von Sohn Gottes und dem Menschen Jesus denken
              und mit der Begrifflichkeit der griechischen Philosophie ausdr&uuml;cken
              kann. F&uuml;r die Nestorianer steht die Frage im Vordergrung,
              wie man dem G&ouml;ttlichen
              gerecht wird. Die Theologenschule von Antiochien in Syrien wahrt
              die Einzigartigkeit
              der g&ouml;ttlichen Natur, indem sie die Unterschiedlichkeit betont.
              G&ouml;ttliches und Menschliches d&uuml;rfen sich nicht vermischen.
              Die andere Konzeption, n&auml;mlich die der Schule von Alexandrien,
              sieht die Menschheit in Jesus durch das G&ouml;ttliche durchdrungen,
              so da&szlig; etwas Neues entsteht, eine neue Natur - Physis, Monophysis
               genannt. Die alexandrinische Schule wirft
              den Antiochenern vor, sie sehe in Jesus Christus &#8222;zwei S&ouml;hne&#8220;.<br>
              Im Konzil von Ephesus  wurde diese
              Position verurteilt, indem gesagt wurde, da&szlig; es keine Existenz
              nur des Menschen Jesus gab, sondern der Sohn Gottes Mensch geworden
              ist. Maria hat den Sohn Gottes geboren, der Menschennatur angenommen
              hat, aber der eine Sohn des Vaters ist.</font></P>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Die gro&szlig;e Ausbreitung
                der Nestorianischen Kirche</strong><br>
              Es gibt heute noch kleine christliche Kirchen, die nach einem Bischof
                von Konstantinopel aus dem 4. Jahrhundert benannt werden. Sie
                erkennen das Konzil von Ephesus nicht
              an. <br>
                Diese in Persien entstandene Kirche hatte Gemeinden entlang der
              Seidenstra&szlig;e gegr&uuml;ndet und war mit ihren Missionaren
              lange vor Marco Polo in China. Timur Lenk hat diese Kirche praktisch
              vernichtet, viele sind in bei der Ausrottung der Armenier Anfang
              des 20. Jahrhunderts get&ouml;tet worden, es gibt nur noch kleine
              Reste dieser Kirche. </font></p>
            <p><font face="Arial, Helvetica, sans-serif">Nestorius hat keine
                zwei Subjekte in Christus angenommen, auch wenn er den Titel
                &quot;Gottesgeb&auml;rerin&quot; ablehnt. Er</font><font face="Arial, Helvetica, sans-serif">                wendet
                sich in einer Predigt aus dem Jahr 429 gegen die Bezeichnung
                Marias
                als Gottesgeb&auml;rerin:<br>
                Hat denn Gott eine Mutter? .... Nicht gebar Maria Gott, denn
                was aus dem Fleische geboren ist, ist Fleisch, nicht gebar das
                Gesch&ouml;pf
              den, der nicht erschaffen werden kann, sondern sie gebar den Menschen,
              der das Werkzeug der Gottheit war. Der Hl. Geist schuf nicht (in
              Maria) das Gott-Wort, weil n&auml;mlich das, was aus ihr geboren
              wurde, vom Hl. Geist ist, sondern er hat dem g&ouml;ttlichen Logos
              einen Tempel errichtet, den er bewohnen sollte. Der Tempel ist
              aus der Jungfrau. Auf keinen Fall ist der inkarnierte Gott gestorben,
              sondern er hat den, in dem er inkarniert war, auferweckt. (<a href="inkarnation.php">Inkarnation</a> &#8211; Fleischwerdung)<br>
                </font></p>            
            <p><font size="2" face="Arial, Helvetica, sans-serif">Eckhard Bieger</font> </p>
            <P>&copy;<a href="http://www.kath.de"> <font face="Arial, Helvetica, sans-serif">www.kath.de</font></a></P>
            </TD>
          <TD background=boxright.gif><IMG height=8 alt="" 
            src="boxright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=8 alt="" src="boxbottomleft.gif" 
            width=8></TD>
          <TD background=boxbottom.gif><IMG height=8 alt="" 
            src="boxbottom.gif" width=8></TD>
          <TD><IMG height=8 alt="" src="boxbottomright.gif" 
            width=8></TD></TR></TBODY></TABLE></TD><td style='vertical-align:top;'>
 <script type="text/javascript"><!--
        google_ad_client = "pub-9537752113242914";
        google_ad_width = 336;
        google_ad_height = 280;
        google_ad_format = "336x280_as";
        google_ad_type = "text_image";
        //2006-10-23: Lexikon Symbole
        google_ad_channel = "8495253357";
        //--></script>
        <script type="text/javascript"
          src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
        </script>
</td>
</TR>
  <TR>
    <TD vAlign=top align=left>&nbsp; </TD>
  </TR></TBODY></TABLE></BODY></HTML>
