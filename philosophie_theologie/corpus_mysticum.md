---
title: Corpus Mysticum
author: 
tags: 
created_at: 
images: []
---


# **Zeichenhafter, geheimnisvoller Leib*
# **Die Kirche lebt mit dem Anspruch, den Auftrag von Jesus Christus zu erfüllen und sein Erlösung bewirkendes Gedenken und seine Offenbarung weiter zu tragen. Ausgehend vom Apostel Paulus, der im Brief an die Gemeinde in Korinth sagt, „Ihr aber seid der Leib Christi, und jeder einzelne ist ein Glied an ihm“, glaubt die Kirche, dass der Geist des auferstandenen Christus in der Kirche wirkt und sie zum Leib Christi formt.**
 Wie aber kann die Kirche zum Leib Christi werden und wie kann diese gestaltende Kraft in menschlicher Kommunikation vermittelt werden?***

 Wie kann die Beziehung, die Christus mit der Kirche hat, dargestellt werden? Vor allem angesichts der Tatsache, dass die Kirche, trotz ihres theologischen Anspruches, als empirisch greifbare Gemeinschaft von Menschen erscheint, die fehlerbehaftet sind und die sich nicht immer kohärent mit ihrem Selbstverständnis verhalten? Schließlich kann an der Kirche von außen nicht zweifelsfrei abgelesen werden, dass es ihr in erster Linie um die Fortführung des Vermächtnisses Jesu geht.***

 In der Brotrede im Johannesevangelium (Joh 6,54) bespricht Jesus dieses Problem: „Wer mein Fleisch ißt und mein Blut trinkt, der bleibt in mir, und ich bleibe in ihm. Wer mein Fleisch ißt und mein Blut trinkt, hat das ewige Leben, und ich werde ihn auferwecken am Letzten Tag.“ 

# **Jesus stellt die Beziehung zu ihm in die Metapher einer Mahlgemeinschaft dar, deren Speise er selbst ist. Er macht die Mahlgemeinschaft im Abendmahl zum zentralen Erinnerungszeichen an seine Erlösungstat, das sich sehr früh in der Christengemeinde als Ritus des „Brotbrechen“ etabliert. Dazu sagt der Apostel Paulus: **
 Denn ich habe vom Herrn empfangen, was ich euch dann überliefert habe: Jesus, der Herr, nahm in der Nacht, in der er ausgeliefert wurde, Brot, sprach das Dankgebet, brach das Brot und sagte: Das ist mein Leib für euch. Tut dies zu meinem Gedächtnis! Ebenso nahm er nach dem Mahl den Kelch und sprach: Dieser Kelch ist der Neue Bund in meinem Blut. Tut dies, sooft ihr daraus trinkt, zu meinem Gedächtnis! Denn sooft ihr von diesem Brot eßt und aus dem Kelch trinkt, verkündet ihr den Tod des Herrn, bis er kommt. 1Kor 11, 23 – 26 

# **Der Ritus des Brotbrechens formt also aus der Vielzahl der Einzelmitglieder, aus den Christen, die Kirche als Leib Christi. Dieser wird vollzogen durch die Teilnahme am Ritus des Brotbrechens. Der Empfang des Leibes Christi im rituellen Vollzug, bestehend im Brotbrechen und im Ritus des Kelches, ist also verbunden mit seiner Bedeutung, der in der Formung der Gemeinschaft der Kirche zum Leib Christi besteht. Dieser Zusammenhang, diese Beziehung zwischen Zeichen (Brotbrechen) und der darin sich bergenden Wirklichkeit (Kirche, Verbundenheit in einem Leib mit Christus) wurde ab dem 4. Jahrhundert als „mystisch“ bezeichnet. Es handelt sich dabei, kommunikationstheoretisch gesprochen, um eine Symbolhandlung mit identitätsstiftender Wirkung, die dem Glauben nach jedoch real ist, d.h. das Zeichen, dem Sinn und der Form nach richtig vollzogen, bewirkt, was es bezeichnet, den Leib Christi.**
# **Dadurch entsteht ein enger Zusammenhang zwischen der Feier der Eucharistie und der Identität der Kirche. Es ist die Feier der Eucharistie, durch die Kirche Einheit bekommt, und es ist die Kirche als Gemeinschaft der Glaubenden, die sich in der Feier der Eucharistie mit Christus vereint.**
 Ursprünglich bedeutet Corpus mysticum die Beziehung zwischen dem Ritus des Brotbrechens und seiner Wirkung, der Leib Christi-Werdung der Kirche. Diesen komplexe Zusammenhang im Blick zu behalten ohne die Interdependenz von Kirche und Eucharistie zugunsten eines der beiden Pole zu vereinseitigen, ist nicht immer einfach. ***

 Manche Theologen gerieten in den Verdacht, die Beziehung zu Jesus Christus nur symbolisch zu verstehen, so dass die seinsmäßige (ontologische) Gegenwart des physischen Leibes in den Zeichen von Brot und Wein nicht hinreichend klar wurde. Die Gegenwart Christi wurde als eine Art virtuelle Gegenwart verstanden wird. (z. B. Berengar) Deshalb entstand gegen Ende des ersten Jahrtausends das Bedürfnis, die Gegenwart Christi in den Zeichen von Brot und Wein, unabhängig von deren Wirkungen, eigens zu definieren. (***[Realpräsenz](realpraesenz.php)) ***

# **Damit wurde, so de Lubac, die Rechtgläubigkeit bewahrt, aber der vitale Nerv durchtrennt. Dieser besteht darin, dass durch die Teilnahme am Ritus des Brotbrechens nicht nur die Gaben Brot und Wein, sondern die Teilnehmer verwandelt und zum Leib Christi geformt werden, so dass beides, die Wandlung der Gaben und die Wandlung der feiernden Gemeinde, einander gegenseitig bedingen.**
 Aus apologetischen Bemühungen heraus ging man zunehmend dazu über, den biblischen Leib (der gelitten hat und auferstanden ist) und den sakramentalen Leib (Brot und Wein) nicht mehr begrifflich zu trennen. So sollte die reale Präsenz Christi in der Feier der Eucharistie definitorisch garantiert werden. ***

 Das Symbol mit seiner Mehrdeutigkeit und Dynamik wurde nicht mehr gebraucht, ja wurde zum Gegenbegriff zur Transsubstantiation. Dadurch ging der Begriff Coprus mysticum von dem verwandelten Brot und Wein auf die dadurch verursachte Wirkung, die in zum Lieb Christi gewordene Kirche, über und verselbständigte sich. Der Zusammenhang zwischen dem rituell-symbolischen Vollzug der Eucharistiefeier und der dadurch entstehenden Einheit der Kirche ging zunehmend verloren. Corpus Mysticum wurde zum eher statischen Begriff, der die ontologischen Qualitäten der Kirche, unabhängig von der ihr inhärenten spirituellen Dynamik, bezeichnet.***

# *
 Theologische Argumente*****

 Die Beziehung zum Jesus des Glaubens ist „mystisch“, d. h. sie realisiert sich in zeichenhaft-symbolischen Aktionen (***[Riten](http://www.kath.de/lexikon/liturgie/ritus_sinnhandlung.php)) und in Meditation und Gebet. Diese Kommunikationen vollziehen sich in zwei Dimensionen: Zum einen zeichenhaften und damit nicht physisch-real. Wir begegnen Jesus nicht wie einem Gegenwartsmenschen. ***

 Zum anderen geheimnishaft, im Gegensatz zum empirisch-faktischen. Die Beziehung ist nicht deshalb geheimnishaft, weil jemand ein Geheimnis hütet, sondern weil sie sich, schritt- und erfahrungsweise erschließt. Sie ist aber ihrer Art nach so, dass sie niemals im Sinne einer Beherrschung erschöpfend erfasst werden kann. ***

 In der Abendmahlsszene hat Jesus selbst dem Ritus des Kelches und des Brotbrechens eine neue, weiterführende Bedeutung mit den Worten gegeben: „Das ist mein Leib, das ist mein Blut. Tut dies zu meinem Gedächtnis.“ ***

 Brot und Wein werden damit konstituiert als Beziehungsmedium mit Jesus Christus durch sein Fleisch und Blut. Sie werden zum sakramentalen (zeichenhaften und objektiv wirksamen) Leib Jesu Christi. Durch die Feier des Brotbrechens (Eucharistie) wird dadurch eine neue Qualität der Beziehung geschaffen, die in der Kommunion (Verbindung) mit Jesus Christus durch den Empfang seines Fleisches und Blutes vollzogen wird. ***

 Dadurch bekommt der einzelne Anteil an der Erlösungswirkung der Selbstmitteilung Gottes. Diese objektiv-persönliche Beziehungsnahme mit Jesus Christus, die durch die Eucharistie möglich wird, verbindet die Teilnehmenden der Eucharistiefeier.***

# **- mit dem Leib Christi, der am Kreuz gemartert und der durch die Auferstehung in einen himmlischen Leib verwandelt wurde, **
 -	mit dem sakramentalen Leib (Brot und Wein) ***

 -	und macht sie dadurch zu dem „Leib Christi“, Corpus Mysticum, verstanden als Kirche. 

***Hier bekommt „Corpus Mysticum“ eine korporativ-soziologische Bedeutung und meint die Kirche, sowohl in ihren verfassten Strukturen als auch in ihrer Bedeutung als Zeichen der Gegenwart des Geistes Gottes in der Welt. ***Corpus Mysticum hat sich in der nachmittelalterlichen Theologie als exklusive Begriff für die Bezeichnung der Kirche entwickelt, während die sakramentale Gegenwart Jesu Christi in Brot und Wein mit „corpus reale“ bezeichnet wurde. Diese Entwicklung wurde durch die Enzyklika „Mystici Corporis“ von Pius XII. vom Lehramt übernommen. 

# **Diese „Ratio mystica“, die Verbindung des geschichtlichen Leibes Christi mit dem sakramentalen unter den Zeichen von Brot und Wein und dem kirchlichen, beschreibt eine Relation von Zeichen und der im Zeichen sich verbergenden Sache. Mysterium besagt die Beziehung des sinnlichen Zeichens zu bezeichneten Sache, eine für den Außenstehenden verborgene Beziehung (deshalb Mysterium), die aber den Glaubenden fortschreitend geoffenbart wird.**
# *
 Theologische Lösung*****

 Corpus Mysticum als ursprünglicher Begriff zur Bezeichnung der mehrfachen Bedeutungen der Eucharistie ist ein generischer Begriff im Gegensatz zu den spezifischen Begriffen für die Eucharistie in Brot und Wein, wie beispielsweise „wahrer Leib“ oder eucharistisches Opfer oder eucharistisches Sakrament. Letzteren ist ein statisches Moment zu eigen. ***

 Der wahre Leib wird angebetet, Corpus Mysticum wird in einem kommunikativen Geschehen realisiert.***

 Corpus Mysticum meint ursprünglich den dynamischen Zusammenhang, in dem durch die rituelle Feier der Eucharistie mit den mystischen Zeichen von Brot und Wein Christus re-präsentiert, vergegenwärtigt wird auf mystische Weise. ***

 Der Begriff selbst stellt dabei ursprünglich die ontologische Dimension der Gegenwart Christi im Zeichen keineswegs in Frage, sondern verdeutlicht sie.***

# *
 Zitate*****

 Ich Berengar, glaube von Herzen und bekenne mit dem Mund, dass das Brot und der Wein, die auf dem Altar liegen, durch das Geheimnis des heiligen Gebets und durch die Worte unseres Erlösers wesentlich gewandelt werden in das wahre, eigentliche, lebenspendende Fleisch und Blut unseres Herrn Jesus Christus; und nach der Weihe sind sie der wahre Leib Christi, der aus der Jungfrau geboren wurde, der, geopfert für das Heil der Welt, am Kreuze hin und der zu Rechten des Vaters sitzt, und das wahre Blut Christi, das aus seiner Seite floß nicht nur im Zeichen und in der Wirksamkeit des Sakramentes, sondern in seiner eigentlichen Natur und in seiner wahren Wesenheit...Römische Partikularsynode 1079, Berengars Glaubensbekenntnis 

# **Diese Interpretation der Realpräsenz trug wesentlich dazu bei, dass der Sinnzusammenhang zwischen der Christusvereinigung in der Eucharistie und der Vitalität und Einheit der Kirche verloren ging. Einheit und Vitalität der Kirche werden zukünftig weniger von der Feier der Eucharistie als von der Stiftung durch Christus her begründet. **
# **Um aber den Sinnknäuel, den das einfache Wort corpus mysticum in sich birgt, und verworrener ist, als auf den ersten Blick zu vermuten wäre, zu entwirren, sind noch zwei wichtige Eigentümlichkeiten zu beachten. Zunächst bedeuten mysterium seiner ursprünglichen Verwendung nach mehr eine Handlung als eine Sache; hierin liegt ein neuer Gesichtspunkt es von „sacramentum“ zu unterscheiden. Dieses aktive Moment ist schon in der Verwendung Pauli spürbar... So sprechen wir heute von der Feier der heiligen Mysterien, und im Gegensatz dazu von der Anbetung des allerheiligsten Sakraments. Das Vollbringen des Mysteriums bringt das Sakramentum hervor. **
 Sodann aber ist das Mysterium nicht allein in seinem Gehalt, der bezeichnet wird, wesentlich aktiv. Es ist das noch radikaler, wenn auch weniger greifbar, in seiner Form... Die ursprüngliche Wortbedeutung behält zwar etwas Verworrenes und Fließendes. Sie ist synthetisch und dynamisch. Sie bezieht sich nicht so sehr auf das erscheinende Zeichen oder … auf die sich darin bergende Wirklichkeit, als vielmehr auf beides zugleich: auf ihre Beziehung, ihr Einsein, ihr gegenseitiges sich bedingen, auf den Übergang vom einen zum anderen. ... Sie zeigt den Aufruf, der vom Zeichen her an das Bezeichnete ergeht, oder besser: auf die dunkle, aber schon im geheimen wirksame Gegenwart des Bezeichneten im Zeichen. Im eigentlichen Sinn „mystisch“ ist somit das verborgene und bewegende Band der allusio, der significatio. (de Lubac, Corpus mysticum, 65 ff) 

***Ihr inneres Band einbüßend hätte die Kirche ihr Wesen verloren. Sie wäre nicht mehr ein wahres, reales Ganzes, ein lebendiger Leib mitsamt seinen Organen, ein sprechendes Abbild des Erlösers, ebenso wahrhaft eins, wenn auch auf andere Art, wie sein individueller Leib... Sie erschiene mehr oder weniger als eine bloße moralische Körperschaft oder gar als eine einfache politische Korporation, wo die Mitglieder mit ihrem Führer in bloß äußerlicher Verbindung stehen. de Lubac, Die Kirche, S. 114 

# **Das zweite Vatikanische Konzil versucht diesen Zusammenhang wieder herzustellen. Dies geschieht aber eher synthetisch, nicht in der von De Lubac erhellten kommunikationspraktischen Funktionalität:**
„              Die Kirche, das heißt das im Mysterium schon gegenwärtige Reich Christi, wächst durch die Kraft Gottes sichtbar in der Welt. Dieser Anfang und diese Wachstum werden zeichenhaft angedeutet durch Blut und Wasser, die der geöffneten Seite des gekreuzigten Christus entströmen und vorherverkündet durch die Worte des Herrn über seinem Tod am Kreuz.... Sooft das Kreuzesopfer, in dem Christus, unser Osterlamm, dahingegeben wurde, auf dem Altar gefeiert wird, vollzieht sich das Werk unserer Erlösung. Zugleich wird durch das Sakrament des eucharistischen Brotes die Einheit der Gläubigen, die einen Leib in Christus bilden, dargestellt und verwirklicht."***

 Lumen Gentium  

***Theo Hipp 

©***[ ***www.kath.de](http://www.kath.de) 
