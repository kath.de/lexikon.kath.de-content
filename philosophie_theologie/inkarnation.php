<HTML><HEAD><TITLE>Inkarnation</TITLE>
<META http-equiv=Content-Type content="text/html; charset=iso-8859-1"><LINK 
title=fonts href="kaltefleiter.css" type=text/css 
rel=stylesheet>
<META content="MSHTML 5.50.4134.600" name=GENERATOR></HEAD>
<BODY bgColor=#ffffff leftMargin=6 topMargin=6 marginheight="6" marginwidth="6">
<TABLE cellSpacing=0 cellPadding=6 width="100%" border=0>
  <TBODY>
  <TR>
    <TD vAlign=top align=left width=100> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b>Philosophie&amp;Theologie</b></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td> <?php include("logo.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10"> <?php include("az.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </TD>
    <TD vAlign=top rowSpan=2>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
        <TBODY>
        <TR vAlign=top align=left>
          <TD width=8><IMG height=8 alt="" 
            src="boxtopleftcorner.gif" width=8></TD>
          <TD background=boxtop.gif><IMG height=8 alt="" 
            src="boxtop.gif" width=8></TD>
          <TD width=8><IMG height=8 alt="" 
            src="boxtoprightcorner.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxtopleft.gif><IMG height=8 alt="" 
            src="boxtopleft.gif" width=8></TD>
          <TD bgColor=#e2e2e2> 
            <H1><font face="Arial, Helvetica, sans-serif">Inkarnation</font></H1>
          </TD>
          <TD background=boxtopright.gif><IMG height=8 
            alt="" src="boxtopright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=13 alt="" src="boxdividerleft.gif" 
            width=8></TD>
          <TD background=boxdivider.gif><IMG height=13 
            alt="" src="boxdivider.gif" width=8></TD>
          <TD><IMG height=13 alt="" 
            src="boxdividerright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxleft.gif><IMG height=8 alt="" 
            src="boxleft.gif" width=8></TD>
          <TD class=L12>
            <P><STRONG><font face="Arial, Helvetica, sans-serif">&quot;Fleischwerdung&quot;
                des Sohnes Gottes</font></STRONG></P>
            <P><font face="Arial, Helvetica, sans-serif">An Weihnachten feiern
                wir, da&szlig; der Sohn Gottes Mensch geworden
              ist. Das ist f&uuml;r das Denken eine Provokation. Mu&szlig; Gott
              Mensch werden, wenn er die Menschheit erl&ouml;sen will? Wenn Gott
              allm&auml;chtig ist, dann gen&uuml;gt dazu eine Willensentscheidung.
              Was das Geheimnis und den Charme des Weihnachtsfestes ausmacht,
              das Kind in Krippe, endet, wenn dieses Kind erwachsen geworden
              ist, mit dem Kreuzestod. Der Evangelist Johannes spitzt die Aussage
              noch zu, er sagt n&auml;mlich, da&szlig; das Wort Fleisch geworden
              ist. Text s.u. Mu&szlig; das Fleisch so betont werden?</font></P>
            <P><font face="Arial, Helvetica, sans-serif"><strong>Unzul&auml;ngliche Antworten</strong><br>
              Wenn man es wirklich ernst nimmt, da&szlig; Jesus von Nazareth
              der Sohn Gottes ist, dann besteht die Gefahr, da&szlig; die Menschheit
              Jesu so in den Vordergrund r&uuml;ckt, da&szlig; die Gottheit in
              der Wahrnehmung der Menschen undeutlich wird. Um das zu verhindern,
              schw&auml;cht man die Menschheit Jesu ab und sagt, da&szlig; diese
              nur eine Art Gewand ist, in dem Gott erscheint. In der fr&uuml;hen
              Kirche waren es die Doketisten, die die menschliche Gestalt Jesu
              nur als Schein bezeichneten. Von &#8222;scheinen&#8220; haben sie
              ihren Namen, denn dokein hei&szlig;t im Griechischen &#8222;scheinen&#8220;.
              Der fr&uuml;hen Kirche wurde bald klar, da&szlig; die Theorie der
              Doketisten nicht nur den biblischen Berichten nicht gerecht wird,
              die Jesus als wirklichen Menschen mit Gef&uuml;hlen zeigen, der
              k&ouml;rperlich gelitten hat und leiblich von den Toten auferstanden
              ist. Denkt man die doketistische Sehweise bis zum Ende durch, stellt
              man die Tats&auml;chlichkeit der Erl&ouml;sung in Frage, denn es
              ist gerade der Tod am Kreuz, den Jesus wirklich erlitten hat, der
              f&uuml;r uns Menschen die Umkehr gebracht und zur Auferstehung
              als Zielpunkt der Existenz Jesu gef&uuml;hrt hat. </font></P>
            <P><font face="Arial, Helvetica, sans-serif"><strong>Theologische Argumentation</strong><br>
              Der Evangelist Johannes hat sich wahrscheinlich in seinem Evangelium
              bereits mit der doketistischen Sichtweise  auseinandergesetzt
              und betont an mehreren Stellen, da&szlig; es wirklich passiert
              ist, was er von Jesus berichtet. Als Thomas zweifelt, da&szlig; Jesus              leiblich auferstanden ist und den J&uuml;ngern unterstellt, sie
              h&auml;tten ein Gespenst gesehen, l&auml;&szlig;t Jesus ihn seine
              H&auml;nde ber&uuml;hren und fordert Thomas auf, da&szlig; dieser
              seine Hand in die Seitenwunde Jesu legt. (Kap. 20,29)<br>
              Erst wenn der Sohn Gottes leiblich Mensch geworden ist, wird er
              zu einem Teil der Menschheitsgeschichte und erst dann ereignet
              sich die Erl&ouml;sung der Menschen in der Geschichte. Deshalb
              sagt Iren&auml;us von Lyon &#8222;caro cardo salutis&#8220;, &#8222;das
              Fleisch ist die T&uuml;rangel der Erl&ouml;sung&#8220;. Es ist
              wie mit dem, was wir denken. Gedachtes gibt es viel, viele gute
              Gedanken, sie werden aber nur wirklich, wenn sie Fleisch werden.<br>
              Die Menschwerdung, die Fleischwerdung des Logos ist der Inhalt
              des <a href="http://www.kath.de/Kirchenjahr/weihnachten.php">Weihnachtsfestes.</a>            </font><font face="Arial, Helvetica, sans-serif"><br>
                  <br>
                  <strong>Theologische L&ouml;sung </strong><br>
                  Inkarnation, Fleischwerdung bedeutet Existenz in der Geschichte.
                Wenn Gott sich zum Teil der menschlichen Geschichte macht, dann
                k&ouml;nnen wir die Hoffnung haben, da&szlig; er die Geschichte,
                unsere Geschichte, zu einem guten Ende bringt. Wenn Jesus mit Leib
                und Seele auferstanden ist, dann werden auch wir nicht nur mit
                unserer Seele ewig leben, sondern unser Leib geh&ouml;rt zu unserer <a href="person.php">himmlischen Existenz</a>.
                Die Bedeutung, die der Leib durch die Fleischwerdung des Sohnes
                Gottes bekommt,
                f&uuml;hrt zur Erkenntnis, da&szlig; er
                nicht nur eine Seele hat, f&uuml;r die der Leib allenfalls ein
                Anh&auml;ngsel w&auml;re, das im Tod abgesto&szlig;en wird. Nicht
                nur die Seele, sondern auch der Leib sind von unersetzlichem Wert.
                Deshalb mu&szlig; sich die christliche N&auml;chstenliebe um den
                Leib k&uuml;mmern. Da&szlig; das nicht nur Theorie ist, zeigen
                die Krankenh&auml;user, die schon in der Fr&uuml;hzeit von Christen
                gegr&uuml;ndet wurden. </font></P>
            <P><font face="Arial, Helvetica, sans-serif">Zitate<br>
              Im Anfang war das Wort, und das Wort war bei Gott und das Wort
              war Gott. Im Anfang war es bei Gott. Alles ist durch das Wort geworden,
              und ohne das Wort wurde nichts. .....<br>
              Und das Wort ist Fleisch geworden und hat unter uns gewohnt, und
              wir haben seine Herrlichkeit gesehen, die Herrlichkeit des einzigen
              Sohnes vom Vater, voll Gnade und Wahrheit. Kap. 1,1-3, 14</font></P>
            <p><font face="Arial, Helvetica, sans-serif">Er (Jesus) war Gott gleich, <br>
              hielt aber nicht daran fest, wie Gott zu sein,<br>
              sondern ent&auml;u&szlig;erte sich und wurde wie ein Sklave,<br>
              und den Menschen gleich.<br>
              Sein Leben war das eines Menschen,<br>
              er erniedrigte sich und war gehorsam bis zum Tod,<br>
              bis zum Tod am Kreuz.<br>
              Darum hat ihn Gott &uuml;ber alle erhoben,<br>
              und ihm einen Namen verliehen, <br>
              der gr&ouml;&szlig;er ist als alle Namen,<br>
              damit alle im Himmel, auf der Erde und unter der Erde,<br>
              ihre Knie beugen vor dem Namen Jesu,<br>
              und jeder Mund bekennt:<br>
              Jesus Christus ist der Herr,<br>
              zur Ehre Gottes des Vaters.<br>
              Philipperbrief 2,6-11</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Werden wir wie Christus,
                da Christus gleich uns geworden ist. Werden wir seinetwillen
                G&ouml;tter, da er unsertwegen Mensch geworden
              ist. Das Geringere nahm er an, um das Bessere zu geben. Er wurde
              arm, damit wir durch seine Armut reich w&uuml;rden. Er nahm die
              Gestalt eines Knechtes an, damit wir die Freiheit erhielten. Er
              stieg auf die Erde hinab, damit wir erh&ouml;ht w&uuml;rden.<br>
              Gregor von Nazaninz, Oratio 1,5 etwa 363</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Wir bekennen unseren
                Herrn Jesus Christus als wahren Gott und wahren Menschen in einer
                Person. Er bleib also die Person des Sohnes
              in der Trinit&auml;t, zu welcher Person die menschliche Natur hinzukam,
              damit auch eine Person sei Gott und Mensch &#8211; nicht ein verg&ouml;ttlichter
              Mensch oder ein erniedrigter Gott, sondern Gott-Mensch und Mensch-Gott.
              Wegen der Einheit der eine Sohn Gottes und zugleich auch Menschensohn,
              vollkommener Gott und vollkommener Mensch. Vollkommen aber ist
              der Mensch mit Leib und Seele&#8230;<br>
              Konzil von Frankfurt, 794</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Paul Tillich &uuml;ber
                die Wirklichkeit der Menschwerdung<br>
  Wenn die christliche Theologie das historische Faktum ignoriert,
                auf das der Name Jesus von Nazareth hinweist, dann ignoriert
                sie damit die grundlegende christliche Aussage, da&szlig; die
                wesenhafte Gott-Mensch-Einheit in der Existenz erschienen ist
                und sich den Bedingungen der Existenz unterworfen hat, ohne von
                ihnen &uuml;berwunden zu werden. G&auml;be es kein personhaftes
                Leben, in dem die existentielle Entfremdung &uuml;berwunden ist,
                dann w&uuml;rde das Neue Sein eine Forderung und eine Erwartung
                sein und nicht Wirklichkeit in Raum und Zeit. &#8230; Das ist
                der Grund daf&uuml;r, da&szlig; die christliche Theologie auf
                der Anerkennung der historischen Faktizit&auml;t des Jesus von
                Nazareth bestehen mu&szlig;.<br>
              Systematische Theologie, 1957, S. 108</font></p>
            <p><font size="2" face="Arial, Helvetica, sans-serif">Text: Eckhard Bieger
                S.J.</font></p>
            <P>&copy;<a href="http://www.kath.de"> <font face="Arial, Helvetica, sans-serif">www.kath.de</font></a></P>
            </TD>
          <TD background=boxright.gif><IMG height=8 alt="" 
            src="boxright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=8 alt="" src="boxbottomleft.gif" 
            width=8></TD>
          <TD background=boxbottom.gif><IMG height=8 alt="" 
            src="boxbottom.gif" width=8></TD>
          <TD><IMG height=8 alt="" src="boxbottomright.gif" 
            width=8></TD></TR></TBODY></TABLE></TD><td style='vertical-align:top;'>
 <script type="text/javascript"><!--
        google_ad_client = "pub-9537752113242914";
        google_ad_width = 336;
        google_ad_height = 280;
        google_ad_format = "336x280_as";
        google_ad_type = "text_image";
        //2006-10-23: Lexikon Symbole
        google_ad_channel = "8495253357";
        //--></script>
        <script type="text/javascript"
          src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
        </script>
</td>
</TR>
  <TR>
    <TD vAlign=top align=left>&nbsp; </TD>
  </TR></TBODY></TABLE></BODY></HTML>
