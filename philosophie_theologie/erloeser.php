<HTML><HEAD><TITLE>Erl&ouml;ser</TITLE>
<meta name="title" content="Philosophie&amp;Theologie">
<meta name="author" content="Redaktion kath.de">
<meta name="publisher" content="kath.de">
<meta name="copyright" content="kath.de">
<meta name="description" content="">
<meta name="abstract" content="Lexikon &uuml;ber Philosophie&amp;Theologie">
<meta http-equiv="content-language" content="de">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="date" content="2006-00-01">
<meta name="robots" content="index,follow">
<meta name="revisit-after" content="10 days">
<meta name="revisit" content="after 10 days">
<meta name="DC.Title" content="Philosophie&amp;Theologie">
<meta name="DC.Creator" content="Redaktion kath.de">
<meta name="DC.Contributor" content="J�rgen Pelzer">
<meta name="DC.Rights" content="kath.de">
<meta name="DC.Publisher" content="kath.de">
<meta name="DC.Date" content="2006-00-01">
<meta name="DC.Description" content="Lexikon &uuml;ber Philosophie&amp;Theologie">
<meta name="DC.Language" content="de">
<meta name="DC.Type" content="Text">
<meta name="DC.Format" content="text/html">
<meta name="DC.Identifier" content="http://www.kath.de/lexikon/philosophie_theologie/">
<meta name="keywords" lang="de" content="Erl�sung, das B�se, S�nde, Tod, S�ndenbock, Erleiden, S�ndenvergebung, Messias">
</HEAD>
<BODY bgColor=#ffffff leftMargin=6 topMargin=6 marginheight="6" marginwidth="6">
<TABLE cellSpacing=0 cellPadding=6 width="100%" border=0>
  <TBODY>
  <TR>
    <TD vAlign=top align=left width=100> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b>Philosophie&amp;Theologie</b></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td> <?php include("logo.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10"> <?php include("az.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </TD>
    <TD vAlign=top rowSpan=2>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
        <TBODY>
        <TR vAlign=top align=left>
          <TD width=8><IMG height=8 alt="" 
            src="boxtopleftcorner.gif" width=8></TD>
          <TD background=boxtop.gif><IMG height=8 alt="" 
            src="boxtop.gif" width=8></TD>
          <TD width=8><IMG height=8 alt="" 
            src="boxtoprightcorner.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxtopleft.gif><IMG height=8 alt="" 
            src="boxtopleft.gif" width=8></TD>
          <TD bgColor=#e2e2e2> 
            <H1><font face="Arial, Helvetica, sans-serif">Erl&ouml;ser</font></H1>
          </TD>
          <TD background=boxtopright.gif><IMG height=8 
            alt="" src="boxtopright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=13 alt="" src="boxdividerleft.gif" 
            width=8></TD>
          <TD background=boxdivider.gif><IMG height=13 
            alt="" src="boxdivider.gif" width=8></TD>
          <TD><IMG height=13 alt="" 
            src="boxdividerright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxleft.gif><IMG height=8 alt="" 
            src="boxleft.gif" width=8></TD>
          <TD class=L12>
            <P><font face="Arial, Helvetica, sans-serif"><STRONG>Jesus
                  Christus, der Erl&ouml;ser, Redemptor</STRONG></font></P>
            <P><font face="Arial, Helvetica, sans-serif"><strong>Wie wurde Jesus
                  zum Erl&ouml;ser?</strong><br>
                  Jesus wird von Paulus als der bezeichnet, der
                wegen unserer S&uuml;nden
              hingerichtet und wegen unserer Rettung auferweckt wurde. (R&ouml;merbrief
              4,25) Die S&uuml;nden der Menschen machen den Tod Jesu notwendig.
              Was das bedeutet, ist f&uuml;r den christlichen Glauben wichtig,
              denn jeden Sonntag werden Tod und Auferstehung Jesu in der Eucharistiefeier
              vergegenw&auml;rtigt, damit die Gl&auml;ubigen von Neuem an ihrer
              Erl&ouml;sung teilhaben. Auch in anderen Gottesdiensten und in
              vielen biblischen Lesungen wird h&auml;ufig auf die Erl&ouml;sung
              Bezug genommen. Da&szlig; die Erl&ouml;sung einer Sehnsucht der
              Menschen entspricht, deutet sich nicht nur in individuellen Schuldgef&uuml;hlen
              an, sondern zeigt sich heute in der Hoffnung auf Frieden. Immer
              wieder berichten die Nachrichten &uuml;ber Morde, &Uuml;berf&auml;lle,
              Auseinandersetzungen zwischen Stra&szlig;enbanden, von B&uuml;rgerkriegen
              und blutigen Aufst&auml;nden. Offensichtlich hat der Mensch etwas
              in sich, das ihn immer wieder vom Frieden abbringt. 
              Aber mu&szlig;te Jesus wegen der Erl&ouml;sung &uuml;berhaupt sterben
              und was nutzt sein Tod im Hinblick auf Frieden und Vers&ouml;hnung?
              Sind nicht nach ihm viele Menschen umgebracht worden, ohne da&szlig; sich
              die Situation irgendwie verbessert h&auml;tte?</font></P>
            <p><strong><font face="Arial, Helvetica, sans-serif">Jesus hat bereits
                  vor seinem Tod S&uuml;nden vergeben<br>
            </font></strong><font face="Arial, Helvetica, sans-serif">Die
                  Vergebung der S&uuml;nde
                  macht den Tod des Messias nicht notwendig.<br>
                  Jesus hatte selbst einen Neuanfang angek&uuml;ndigt. &#8222;er
                verk&uuml;ndete das Evangelium Gottes und sprach: die Zeit ist
                erf&uuml;llt, das Reich Gottes ist nahe. Kehrt um und glaubt
                an das Evangelium,&#8220; hei&szlig;t es bei Markus (1,14-15)
                Der Inhalt dieser guten Nachricht ist die N&auml;he des Reiches
                Gottes, die sich in den Heilungen und der Predigt Jesu erweist.
                Zu der
                Predigt Jesu geh&ouml;rt ausdr&uuml;cklich die Vergebung der
                S&uuml;nden.
                Einige Zeilen sp&auml;ter im Markusevangelium wird berichtet,
                da&szlig; ein
                Gel&auml;hmter zu Jesus getragen wurde. &#8222;als Jesus ihren
                Glauben sah, sagte er zu dem Gel&auml;hmten: Mein Sohn, deine
                S&uuml;nden
                sind dir vergeben.&#8220; (Markus 2,5) Die Schriftgelehrten
                reagieren mit Recht: &quot;Einige Schriftgelehrten aber, die dort
                sa&szlig;en,
                dachten im Stillen: Wie kann dieser Mensch so reden. Er l&auml;stert
                Gott. Wer kann S&uuml;nden vergeben au&szlig;er dem einen Gott?&#8220; Um
                seine Vollmacht auszuweisen, heilt Jesus den Gel&auml;hmten.
                Die Reich-Gottes-Botschaft war eine Botschaft der Heilung und
                Erl&ouml;sung,
                die die S&uuml;ndenvergebung einschlo&szlig;. Jesus mu&szlig;te
                nicht sterben, damit die Menschen Erl&ouml;sung empfangen konnten,
                eben sich das zusprechen lassen konnten, was untereinander nur
                anfanghaft
                m&ouml;glich ist, zu verzeihen. Gott kann S&uuml;nden wirklich
                vergeben. Weil Gott die Schuld des Menschen vergibt, soll der
                Mensch ebenfalls Nachsicht &uuml;ben und verzeihen. Das verdeutlicht
                Jesus im Gleichnis von dem Verwalter, der seinem Herrn eine Riesensumme
                schuldet. Er bekommt sie erlassen, kaum ist er frei, begegnet
                er
                einem, der ihm einen viel geringeren Betrag schuldet und l&auml;&szlig;t
                ihn ins Gef&auml;ngnis werfen. &#8222;Da lie&szlig; ihn sein
                Herr zu isch rufen und sagte zu ihm: Du elender Diener! Deine
                ganze Schuld habe ich
                dir erlassen, weil du mich so angefleht hast. H&auml;ttest nicht
                auch du mit jenem, der gemeinsam mit dir in meinem Dienst steht,
                Erbarmen haben m&uuml;ssen, so wie ich Erbarmen mit dir hatte?
                Und in seinem Zorn &uuml;bergab ihn der Herr den Folterknechten,
                bis er die ganze Schuld bezahlt habe. Ebenso wird mein himmlischer
                Vater jeden von euch behandeln, der seinem Bruder nicht von ganzem
                Herzen vergibt.&#8220; Matth&auml;us 18,32-35 Dieses Gleichnis
                Jesu beinhaltet nicht nur die Forderung, dem Mitmenschen zu verzeihen,
                sondern geht auch davon aus, da&szlig; Gott den Menschen bereits
                verziehen hat. Dazu gibt es viele Aussagen im Alten Testament.
                Warum war der
                Tod Jesu dann aber noch notwendig?<br>
                <br>
                <strong>Jesu &ouml;ffentliches Auftreten f&uuml;hrt zu Konflikten<br>
                </strong>                Die Bibel berichtet davon, da&szlig; Jesus zuerst gro&szlig;e
                Zustimmung erfuhr.<br>
                Johannes der T&auml;ufer hat als Vorl&auml;ufer den Messias,
                also den, auf den die Juden sehns&uuml;chtig warteten, angek&uuml;ndigt.
                Viele Menschen sind Jesus gefolgt, haben sich seine Predigt zu
                Herzen genommen und ihn durch die Heilungswunder als von Gott
                legitimierten Boten gesehen. Als sich politische Erwartungen
                auf Jesus richteten,
                hat er sich nach dem Einzug n Jerusalem (<a href="http://www.kath.de/Kirchenjahr/palmsonntag.php">Palmsonntag</a>) entzogen.
                Er ist nicht zum Anf&uuml;hrer
                eines Aufstandes gegen die R&ouml;mer geworden. Einer der von
                ihm Entt&auml;uschten
                war Judas, der sein Verr&auml;ter wurde. Obwohl Jesus keine politische
                Macht anstrebte, war er zu einem politischen Faktor geworden.
                Der Hohe Rat in Jerusalem schickte Beobachter aus, Schriftgelehrte
                verwickelten ihn theologische Dispute. Der Druck auf Jesus wurde
                gr&ouml;&szlig;er, so da&szlig; er sich in das au&szlig;erj&uuml;dische
                Gebiet von Tyrus zur&uuml;ckzog. Der R&uuml;ckhalt im j&uuml;dischen
                Volk lie&szlig; nach, als deutlich wurde, da&szlig; Jesus seine
                Sendung nicht politisch verstand, n&auml;mlich mit der Hilfe
                Gottes die Fremdherrschaft der R&ouml;mer abzusch&uuml;tteln.
                Jesus entschlie&szlig;t
                sich dann doch, zum Paschafest wie die anderen Juden aus dem
                n&ouml;rdlichen
                Galil&auml;a nach Jerusalem zu gehen.</font></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Es l&auml;uft
                  auf den Tod Jesu zu</strong><br>
  Der Hohe Rat der Juden bef&uuml;rchtete Aufruhr, als Jesus zum
              Passahfest erschien. Aus den Beratungen des Hohen Rates berichtet
              Johannes folgende &Uuml;berlegung: &#8222;Sie sagten: Was sollen
              wir tun. Dieser Mensch tut viele Zeichen. Wenn wir ihn gew&auml;hren
              lassen, werden alle an ihn glauben. Dann werden die R&ouml;mer
              kommen und uns die heiligen St&auml;tten und das ganze Volk nehmen.&#8220; (Kap.
              11,47-48)<br>
              Jesus wird nach einem l&auml;ngeren Hin und Herr zwischen j&uuml;dischem
              Hohen Rat, dem obersten r&ouml;mischen Beamten, Pilatus, und Herodes
              zum Tode verurteilt. Er l&auml;&szlig;t seine Anh&auml;nger in
              tiefer Entt&auml;uschung und ratlos zur&uuml;ck. F&uuml;r die damaligen
              Machthaber schien die Sache ausgestanden. <br>
              Als dann seine J&uuml;nger 50 Tage nach den Ereignissen im Zusammenhang
              mit dem j&uuml;dischen F&uuml;nfwochenfest (<a href="http://www.kath.de/Kirchenjahr/pfingsten.php">Pfingsten</a>) ihr Predigt
              begannen, selbst Heilungen wirkten und gro&szlig;e Zulauf des Volkes
              erhielten, war der Prediger aus Nazareth wieder pr&auml;sent. Und
              er wurde als Erl&ouml;ser verk&uuml;ndet. Als die Leute nach der
              Predigt des Petrus am j&uuml;dischen Pfingstfest fragten: &#8222;Was
              sollen wir tun, Br&uuml;der?&#8220; antwortet Petrus: &#8222;Kehrt
              um, und jeder von euch lasse sich auf den Namen Jesu Christi taufen
              zur Vergebung seiner S&uuml;nden. Dann werdet ihr die Gabe des
              Heiligen Geistes empfangen.&#8220; Apostelgeschichte 2,38</font></p>
            <p><font face="Arial, Helvetica, sans-serif">anders als f&uuml;r die Christen
                war f&uuml;r
                die Juden schnell deutlich, da&szlig; Jesus nicht
              der Messias gewesen sein konnte. Bis heute erwarten sie den wirklichen
              Messias, der das Reich Davids wieder herstellt und Israel als selbst&auml;ndigen
              Gottesstaat aufrichtet. Ein Messias, der allenfalls die Herzen
              der Menschen gewinnt, kann nicht der erhoffte Nachkomme des K&ouml;nigs
              David sein, wenn er nicht imstande ist, die &auml;u&szlig;eren
              Verh&auml;ltnisse grundlegend zum Besseren zu wenden. <br>
              Zweifellos stellt die Hinrichtung Jesu am Kreuz eine Herausforderung
              dar, der sich die Anh&auml;nger Jesu stellen mu&szlig;ten. Aber
              ist Jesus nicht selbst von seinem Schicksal &uuml;berrascht worden
              und hat er &uuml;berhaupt selbst damit gerechnet, da&szlig; seine
              Mission nicht nur mit dem Risiko des Scheiterns, sondern auch des
              gewaltsamen Todes befrachtet war?<br>
              Jesus rechnete mit seinem gewaltsamen Ende. Auf einen ernsthaften
              Konflikt deutet hin, da&szlig; er sich einer zunehmenden feindlichen
              Beobachtung und Streitgespr&auml;chen durch die j&uuml;dische Obrigkeit
              und die Schriftgelehrten ausgesetzt sah. &Uuml;berliefert sind
              Leidensweissagungen, gegen die sich die J&uuml;nger heftig wehren.
              (S.u. bei Zitaten)<br>
              Offensichtlich hat Jesus dann das Mahl zum Paschahfest der Juden
              im Bewu&szlig;tsein seines baldigen Todes gefeiert. Das Wort &uuml;ber
              den Becher Wein wird von Matth&auml;us mit folgenden Worten &uuml;berliefert: &#8222;Trinkt
              alle daraus, das ist mein Blut, das Blut des Bundes, das f&uuml;r
              viele vergossen wird zur Vergebung der S&uuml;nden. Ich sage euch:
              von jetzt an werde ich nicht mehr von der Frucht des Weinstocks
              trinken, bis zu dem Tag, an dem ich mit euch von neuem davon trinke
              im Reich meines Vaters.&#8220; (Matth. 26, 27-29) Offensichtlich
              m&uuml;ndet die Mission Jesu in einem t&ouml;dlichen Konflikt.
              Jesus geht diesen Weg. Er betet am &Ouml;lberg &#8222;Meine Seele
              ist bis zum Tode betr&uuml;bt ... Abba, Vater, alles ist dir m&ouml;glich.
              Nimm diesen Kelch von mir! Aber nicht, was ich will, geschehe,
              sondern was du willst.&#8220; (Markus 14,34 u 36) Jesus akzeptiert
              seine Verurteilung und Hinrichtung als Willen Gottes. Was aber
              bedeutet dieser Wille Gottes, welcher Sinn liegt in dem Sterben
              Jesu?</font></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Der Sinn des Todes Jesu</strong><br>
  Die erste Deutung, die sich den J&uuml;ngern er&ouml;ffnet, ist
              die Begegnung mit dem Auferstandenen. Jesus ist nicht im Tod geblieben,
              sondern Gott hat ihn auferweckt und damit endg&uuml;ltig als Messias
              erwiesen. Dies wird in der Predigt des Petrus direkt formuliert: &#8222;mit
              Gewi&szlig;heit erkenne also das ganze Haus Israel: Gott hat ihn
              zum Herrn und Messias gemacht, diesen Jesus, den ihr gekreuzigt
              habt.&#8220; Apg 2,36 <br>
              F&uuml;r den Tod Jesu fanden die J&uuml;nger im Alten Testament
              Deutungen, vor allem in den Liedern vom Gottesknecht. Das 4. dieser
              Lieder sagt vom Gottesknecht: &#8222;Er hat unsere Krankheiten
              getragen und unsere Schmerzen auf sich genommen. Wir meinten, er
              sei vom Unheil getroffen, von Gott gebeugt und geschlagen. Doch
              er wurde durchbohrt wegen unserer Verbrechen, wegen unserer S&uuml;nden
              mi&szlig;handelt.&#8220;<br>
              Von Gott hei&szlig;t es: &#8222;Doch der Herr warf all unsere S&uuml;nden
              auf ihn.&#8220; (Text s.u. bei Zitate)</font></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Die S&uuml;nde
                  f&uuml;hrt
                zum Tod</strong> - <strong>der Krimi erkl&auml;rt es</strong><br>
  Der Tod des Unschuldigen ist durch die S&uuml;nde verursacht. Hier
              entsteht eine neue Frage: Warum macht die S&uuml;nde den Tod des
              Gottesknechtes notwendig? Anselm von Canterbury, ein englischer
              Theologe des 11. Jahrhunderts, geht davon aus, da&szlig; die S&uuml;nde
              des Menschen eine solche Entehrung Gottes darstellt, da&szlig; kein
              Gesch&ouml;pf daf&uuml;r Genugtuung leisten, sondern allein der
              menschgewordene Sohn Gottes die Ehre Gottes wieder herstellen kann.
              Darin spiegeln sich die Vorstellungen des germanischen Ehrenkodexes.
              F&uuml;r eine solche Interpretation finden sich in der Bibel keine
              stichhaltigen Anhaltspunkte. Es ist nicht die vom Menschen gekr&auml;nkte
              Ehre Gottes, die ein solches Opfer verlangt, sondern die S&uuml;nde
              selbst, die Bosheit des Menschen verbunden mit seiner Angst, f&uuml;hren
              mit innerer Logik zum Tod des Unschuldigen.<br>
            </font><font face="Arial, Helvetica, sans-serif">Da&szlig; das B&ouml;se
            zum Tod f&uuml;hrt, erkl&auml;rt uns jeder
              Krimi. Immer gibt es etwas im Menschen, das ihn t&ouml;ten l&auml;&szlig;t &#8211; Eifersucht,
              Neid, nicht erwiderte Liebe, Spielleidenschaft oder weil ein Verbrechen
              entdeckt werden k&ouml;nnte, bringt der Verbrecher den unschuldigen
              Zeugen um. Was viele Sagen in mythischen Bildern zeigen, ist offensichtlich
              nicht nur Phantasie, sondern eine Tatsache in der realen, der Welt
              der Menschen: Der Held wird erst zum Held, wenn er den Kampf mit
              dem B&ouml;sen bestanden hat. Offensichtlich war das auch f&uuml;r
              den Lebensweg Jesu eine unausweichliche Notwendigkeit. Im Unterschied
              zu vielen Helden der Sagen und Mythen &uuml;berwindet Jesus das
              B&ouml;se
              aber nicht durch k&ouml;rperliche &Uuml;berlegenheit und Mut, sondern
              indem er es erleidet. Offensichtlich kann das B&ouml;se nicht von
              au&szlig;en einfach nur bek&auml;mpft werden. Die meisten Kriege,
              die das B&ouml;se beseitigen sollten, sind selbst &#8222;b&ouml;se&#8220; geworden.
              Offensichtlich ist das B&ouml;se auch nicht mit dem Sieg Jesu durch
              sein Sterben am Kreuz und seine Auferstehung aus der Welt verschwunden.
              Deshalb kann das Kino die Sagen, ob &#8222;Starwars&#8220;, &#8222;Herr
              der Ringe&#8220; oder &#8222;K&ouml;nig von Narnia&#8220; weiter
              erz&auml;hlen. <br>
              Im Leben Jesu geht es aber nicht nur um die &Uuml;berwindung des
              B&ouml;sen, so wie das jedem Menschen als Aufgabe gestellt ist.
              Die Sagen und die Kinofilme malen das in gro&szlig;en Bildern,
              was oft im Herzen des einzelnen stattfindet. Jesus hatte eine definitive
              Botschaft: Das Reich Gottes ist nahe und die N&auml;he zeigt sich
              in seiner Predigt, in der von ihm zugesprochenen Vergebung der
              S&uuml;nden,
              in der Heilung von Blinden und Auss&auml;tzigen, im Zur&uuml;ckweichen
              der b&ouml;sen Geister und der Speisung der Zuh&ouml;rer, die ihm
              in gro&szlig;er Zahl, die Bibel spricht von 5000, gefolgt waren.
              Nach anf&auml;nglicher Zustimmung st&ouml;&szlig;t Jesus auf Ablehnung,
              vor allem bei der j&uuml;dischen Obrigkeit und den Theologen, die &uuml;ber
              die richtige Auslegung der B&uuml;cher des Moses wachten. Jesus
              reagiert auf diese sich abzeichnende Ablehnung mit teils scharfen
              Worten. Am deutlichsten geschieht das in dem Gleichnis vom Weinbergsbesitzer.
              Dieser hatte seinen Weinberg verpachtet und schickte Angestellte,
              um die Pacht einzufordern. Die P&auml;chter verweigerten nicht
              nur die Pachtzahlung, sondern mi&szlig;handelten die Abgesandten
              des Besitzers. &#8222;Zuletzt sandte er seinen Sohn zu ihnen; denn
              er dachte: Vor meinem Sohn werden sie Achtung haben. Als die Winzer
              den Sohn sahen, sagten sie zueinander: Das ist der Erbe. Auf, wir
              wollen ihn t&ouml;ten, damit wir seinen Besitz erben. Und sie packten
              ihn, warfen ihn aus dem Weinberg hinaus und brachten ihn um.&#8220; Matth&auml;us
              21,37-39 <br>
              Die j&uuml;dischen Zuh&ouml;rer verstanden, da&szlig; mit
              dem Weinberg das Volk Israel gemeint ist. Die Pacht, die Gott erwartet,
              sind nicht Brandopfer, sondern die Befolgung seiner Gebote, Gerechtigkeit
              und da&szlig; die Armen und Witwen unterst&uuml;tzt werden.<br>
              Jede Generation erkennt im Sterben des Messias den Tod der Gerechten
              ihrer Kriege und Verfolgungen. Offensichtlich laden sich im menschliche
              Zusammenleben durch Mi&szlig;gunst, Eifersucht, durch R&uuml;cksichtslosigkeit
              und nicht zuletzt durch Angst Schuldpotentiale auf, die sich einen
              Ungerechten als Opfer suchen. Ren&eacute; Girard hat in neuerer
              Zeit diesen Mechanismus beschrieben und daf&uuml;r das Bild des <a href="suendenbock.php">S&uuml;ndenbocks</a>              gew&auml;hlt.
              Die Notwendigkeit des Todes Jesu kommt nicht aus dem Willen Gottes,
              sondern aus der Bosheit des Menschen. Die klassischen
              Mythen wie auch die Hollywoodfilme zeigen das bleibende Interesse
              an der Frage, wie das B&ouml;se &uuml;berwunden werden kann und
              die Sehnsucht nach einem Helden, der das mit dem Einsatz seiner
              Kr&auml;fte
              erreicht. Allerdings ist die L&ouml;sung, die Jesus gelebt und
              erlitten hat, anders als die der Sagen, seien sie von Herkules
              oder Anakin Skywalker
              in Starwars. Die &Uuml;berwindung des B&ouml;sen wird im Christentum
              nicht durch die gr&ouml;&szlig;ere Kraft des Helden gew&auml;hrleistet,
              sondern im Erleiden des B&ouml;sen. Das Christentum h&auml;tte
              keinen Antwort auf das B&ouml;se gegeben, wenn Jesu nicht bis aufs
              Letzte mit der Mi&szlig;gunst, dem Ha&szlig;, der Folter, der Gewalt
              konfrontiert worden w&auml;re. <br>
              Da&szlig; der Tod des Messias den Menschen zum Heil werden konnte,
              das ist nach durchgehender Aussage der neutestamentlichen Texte von
              Gott so gewirkt. Damit erhellt sich auch der Zusammenhang von Reich
              Gottes mit der Hinrichtung und Auferstehung des Messias: Das Reich
              Gottes kann nur anbrechen, wenn das B&ouml;se wirklich, d.h. von
              innen &uuml;berwunden ist.<br>
              Da&szlig; das Sterben Jesu tats&auml;chlich das B&ouml;se &uuml;berwunden
              hat, zeigt sich an der Reaktion seiner Anh&auml;nger. Sie r&auml;chen
              den Tod Jesu nicht, sondern interpretieren die Hinrichtung des Messias
              als neues Angebot Gottes zur Vergebung der Schuld. Eine auf den ersten
              Blick sehr gewagte, wenn nicht abenteuerliche Auslegung. Die Betrachtung
              des Leidens Jesu soll den Beter in diese Haltung Jesu ein&uuml;ben.
              Viele Christen haben diesen Weg Jesu verstanden, auch wenn viele
              andere auch heute noch meinen, Gewalt im Namen Jesu aus&uuml;ben
              zu d&uuml;rfen.<br>
              </font></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Zitate</strong><br>
              Jesus k&uuml;ndigt sein Leiden an: Markus 8,31-33<br>
              Dann begann er, sie dar&uuml;ber zu belehren, der Menschensohn
              m&uuml;sse vieles erleiden und von den &Auml;ltesten und den Hohenpriestern
              verworfen werden; er werde get&ouml;tet, aber nach drei Tagen werde
              er auferstehen. Und er redete ganz offen dar&uuml;ber. Da nahm
              ihn Petrus beiseite und machte ihm Vorw&uuml;rfe. Jesus wandte
              sich um, sah seine J&uuml;nger an und wies Petrus mit den Worten
              zurecht: Weg mit dir, Satan, geh mir aus den Augen. Denn du hast
              nicht im Sinn, was Gott will, sondern was die Menschen wollen. </font></p>
            <p><font face="Arial, Helvetica, sans-serif">Die Predigt Johannes
                des T&auml;ufers nach Matth&auml;us:<br>
  In jenen Tagen trat Johannes der T&auml;ufer auf und verk&uuml;ndete
              in der W&uuml;ste von Juda: Kehrt um! Denn das Himmelreich ist
              nahe.....<br>
              Die Leute von Jerusalem und ganz Jud&auml;a und aus der ganzen
              Jordangegend zogen zu ihm hinaus, sie bekannten ihre S&uuml;nden
              und lie&szlig;en sich im Jordan von ihm taufen. Als Johannes sah,
              da&szlig; viele Pharis&auml;er und Sadduz&auml;er zur Taufe kamen,
              sagte er zu ihnen: Ihr Schlangenbrut, wer hat euch denn gelehrt,
              da&szlig; ihr dem kommenden Gericht entkommen k&ouml;nnt. Bringt
              Frucht hervor, die eure Umkehr zeigt.<br>
              Kap 3, 1-2, 5-7</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Das 4. Lied vom Gottesknecht:<br>
              Seht,
                mein Knecht hat Erfolg, er wird gro&szlig; sein und hoch
              erhaben.<br>
              Viele haben sich &uuml;ber ihn entsetzt, denn er sah entstellt
              aus, nicht wie ein Mensch, seine Gestalt war nicht mehr die eines
              Menschen.<br>
              Jetzt aber setzt er viele V&ouml;lker in Staunen, K&ouml;nige m&uuml;ssen
              vor ihm verstummen.<br>
              Denn wovon ihnen kein Mensch je erz&auml;hlt hat, das sehen sie
              nun;<br>
              was sie niemals h&ouml;rten, das erfahren sie jetzt.<br>
              Wer hat geglaubt, was uns berichtet wurde?<br>
              Die Hand des Herrn - wer hat ihr Wirken erkannt?<br>
              Vor den Augen des Herrn wuchs er auf wie ein junger Spro&szlig;,
              wie der Trieb einer Wurzel aus trockenem Boden.<br>
              Er hatte keine sch&ouml;ne und edle Gestalt, und niemand von uns
              blickte ihn an.<br>
              Er sah nicht so aus, da&szlig; er unser Gefallen erregte.<br>
              Er wurde verachtet und von den Menschen gemieden, <br>
              ein Mann voller Schmerzen, mit der Krankheit vertraut.<br>
              Wie ein Mensch, vor dem man das Gesicht verh&uuml;llt,<br>
              war er bei uns verfemt und verachtet.<br>
              Aber er hat unsere Krankheiten getragen <br>
              und unsere Schmerzen auf sich genommen. <br>
              Wir meinten, er sei vom Unheil getroffen,<br>
              von Gott gebeugt und geschlagen. <br>
              Doch er wurde durchbohrt wegen unserer Verbrechen,<br>
              wegen unserer S&uuml;nden mi&szlig;handelt.<br>
              Weil die Strafe auf ihm lag, sind wir gerettet,<br>
              durch seine Wunden sind wir geheilt. <br>
              Wir hatten uns alle verirrt wie die Schafe, jeder ging f&uuml;r
              sich seinen Weg.<br>
              Doch der Herr warf all unsere S&uuml;nden auf ihn. <br>
              Er wurde geplagt und niedergedr&uuml;ckt, aber er tat seinen Mund
              nicht auf.<br>
              Wie ein Lamm, das man wegf&uuml;hrt, um es zu schlachten,<br>
              und wie ein Schaf, das verstummt, wenn man es schert,<br>
              so tat auch er seinen Mund nicht auf. <br>
              Durch Haft und Gericht kam er ums Leben, doch wen k&uuml;mmerte
              sein Geschick?<br>
              Er wurde aus dem Land der Lebenden versto&szlig;en<br>
              und wegen der Verbrechen seines Volkes get&ouml;tet. <br>
              Bei den Gottlosen gab man ihm sein Grab,<br>
              bei den Verbrechern seine Ruhest&auml;tte,<br>
              obwohl er kein Unrecht getan hat,<br>
              und aus seinem Mund kein unwahres Wort kam. <br>
              Doch der Herr fand Gefallen an seinem mi&szlig;handelten (Knecht),<br>
              er rettete den, der sein Leben als S&uuml;hneopfer hingab.<br>
              Er wird lange leben und viele Nachkommen sehen.<br>
              Durch ihn setzt der Wille des Herrn sich durch. <br>
              Nachdem er so vieles ertrug, erblickt er wieder das Licht<br>
              Und wird erf&uuml;llt von Erkenntnis.<br>
              Mein Knecht ist gerecht, darum macht er viele gerecht;<br>
              er nimmt ihre Schuld auf sich. <br>
              Deshalb gebe ich ihm seinen Anteil unter den Gro&szlig;en<br>
              und mit den M&auml;chtigen teilt er die Beute;<br>
              denn er gab sein Leben hin und wurde zu den Verbrechern gerechnet.<br>
              Er trug die S&uuml;nden von vielen und trat f&uuml;r die Schuldigen
              ein.	(Jesaia 52,13-53, 12)</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Karl Barth, evangelischer
                Theologe, in seinem R&ouml;merbriefkommentar:<br>
  Jesus, erkannt als der Christus, best&auml;tigt, bew&auml;hrt und
              bekr&auml;ftigt alles menschliche Harren. Er ist die Mitteilung,
              da&szlig; es nicht der Mensch, sondern Gott in seiner Treue ist,
              der harrt. &#8211; Da&szlig; wir gerade in Jesus von Nazareth den
              Christus gefunden haben, bew&auml;hrt sich darin, da&szlig; alle
              Kundgebungen der Treue Gottes Hinweise und Weissagungen sind auf
              das, was uns eben in Jesus begegnet ist. Die verborgene Kraft des
              Gesetzes und der Propheten (das sind die B&uuml;cher des Alten
              Testaments) ist der Christus, der uns in Jesus begegnet. Der Sinn
              aller Religion ist die Erl&ouml;sung, die Zeitenwende, die Auferstehung,
              das Unanschauliche Gottes, das uns eben in Jesus zum Stillstehen
              zwingt. Der Gehalt allen menschlichen Geschehens ist die Vergebung,
              unter der es steht, wie sie eben von Jesus verk&uuml;ndigt, in
              ihm verk&ouml;rpert ist, Da&szlig; diese Kraft, dieser Sinn, dieser
              Gehalt auch anderswo als in Jesus gefunden werde, das braucht
              uns niemand vorzuhalten, wir selbst sind es ja, die ja gerade das
              behaupten,
              gerade wir k&ouml;nnen es behaupten.  Denn da&szlig; Gott allenthalben
              gefunden wird, da&szlig; die Menschheit vor und nach Jesus von
              Gott gefunden ist, den Ma&szlig;stab, an dem alles Finden Gottes,
              alles Vor-Gott-Gefunden-Werden als solches erkennbar wird, die
              M&ouml;glichkeit dieses Findens und Gefunden Werdens zu begreifen
              als Wahrheit ewiger Ordnung &#8211; das eben wird in Jesus erkannt
              und gefunden. Viele wandeln im Lichte der Erl&ouml;sung, der Vergebung,
              der Auferstehung; da&szlig; wir sie wandeln sehen, da&szlig; wir
              Augen daf&uuml;r haben, das verdanken wir dem Einen. In seinem
              Licht sehen wir das Licht. &#8211; Und da&szlig; es der Christus
              ist, was wir in Jesus gefunden, das bew&auml;hrt sich darin, da&szlig; Jesus
              das letzte, das alle andern erkl&auml;rende und auf den sch&auml;rfsten
              Ausdruck bringende Wort der vom Gesetz und den Propheten bezeugten
              Treue Gottes ist. Die Treue Gottes ist sein Hineingehen und Verharren
              in der tiefsten menschlichen Fragw&uuml;rdigkeit und Finsternis.<br>
              Der R&ouml;merbrief, 1922, S. 73<br>
              <br>
              <font size="2">Eckhard Bieger
              </font></font> </p>
            <p><font face="Arial, Helvetica, sans-serif">&copy;<a href="http://www.kath.de"> www.kath.de</a></font></p>
            </TD>
          <TD background=boxright.gif><IMG height=8 alt="" 
            src="boxright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=8 alt="" src="boxbottomleft.gif" 
            width=8></TD>
          <TD background=boxbottom.gif><IMG height=8 alt="" 
            src="boxbottom.gif" width=8></TD>
          <TD><IMG height=8 alt="" src="boxbottomright.gif" 
            width=8></TD></TR></TBODY></TABLE></TD><td style='vertical-align:top;'>
            <p><a href="http://www.update-seele.de" target="_blank"><img src="banner-update-seele-02.jpg" width="286" height="70" border="0"></a> </p>
            <p>
              <script type="text/javascript"><!--
        google_ad_client = "pub-9537752113242914";
        google_ad_width = 336;
        google_ad_height = 280;
        google_ad_format = "336x280_as";
        google_ad_type = "text_image";
        //2006-10-23: Lexikon Symbole
        google_ad_channel = "8495253357";
        //--></script>
              <script type="text/javascript"
          src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
                  </script>
                        </p></td>
</TR>
  <TR>
    <TD vAlign=top align=left>&nbsp; </TD>
  </TR></TBODY></TABLE></BODY></HTML>
