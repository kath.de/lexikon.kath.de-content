<HTML><HEAD><TITLE>Messias - Christus</TITLE>
<META http-equiv=Content-Type content="text/html; charset=iso-8859-1"><LINK
title=fonts href="kaltefleiter.css" type=text/css
rel=stylesheet>
<META content="MSHTML 5.50.4134.600" name=GENERATOR></HEAD>
<BODY bgColor=#ffffff leftMargin=6 topMargin=6 marginheight="6" marginwidth="6">
<TABLE cellSpacing=0 cellPadding=6 width="100%" border=0>
  <TBODY>
  <TR>
    <TD vAlign=top align=left width=100>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b>Philosophie&amp;Theologie</b></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td> <?php include("logo.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10"> <?php include("az.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </TD>
    <TD vAlign=top rowSpan=2>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
        <TBODY>
        <TR vAlign=top align=left>
          <TD width=8><IMG height=8 alt=""
            src="boxtopleftcorner.gif" width=8></TD>
          <TD background=boxtop.gif><IMG height=8 alt=""
            src="boxtop.gif" width=8></TD>
          <TD width=8><IMG height=8 alt=""
            src="boxtoprightcorner.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxtopleft.gif><IMG height=8 alt=""
            src="boxtopleft.gif" width=8></TD>
          <TD bgColor=#e2e2e2>
            <H1><font face="Arial, Helvetica, sans-serif">Messias</font></H1>
          </TD>
          <TD background=boxtopright.gif><IMG height=8
            alt="" src="boxtopright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=13 alt="" src="boxdividerleft.gif"
            width=8></TD>
          <TD background=boxdivider.gif><IMG height=13
            alt="" src="boxdivider.gif" width=8></TD>
          <TD><IMG height=13 alt=""
            src="boxdividerright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxleft.gif><IMG height=8 alt=""
            src="boxleft.gif" width=8></TD>
          <TD class=L12>
            <P><STRONG><font face="Arial, Helvetica, sans-serif">Christus, Christos,
                Massiah, Gesalbter</font></STRONG></P>
            <P><font face="Arial, Helvetica, sans-serif">Auf den Messias richten
                sich die Erwartungen der Juden. Wenn er kommt, wird er ein Reich
                des Friedens f&uuml;r das Volk Israel
              bringen. Die Christen behaupten, da&szlig; Jesus der Messias ist.
              Welchen Messias erwarten die Juden und warum sind die Christen &uuml;berzeugt,
              da&szlig; Jesus der Messias ist, die Juden aber nicht?<br>
              Da&szlig; Jesus gegen&uuml;ber der j&uuml;dischen &Ouml;ffentlichkeit
              mit einem Anspruch aufgetreten ist, geht aus den Berichten der
              Bibel deutlich hervor. Warum w&auml;re es auch sonst zu einem Proze&szlig; vor
              dem j&uuml;dischen Religionsgericht gekommen. Jesus wurde vom j&uuml;dischen
              Hohen Rat unter Vorsitz des Hohenpriesters Kaiphas wegen Gottesl&auml;sterung
              verurteilt. Die Verurteilung durch Pilatus hatte wohl eher einen
              politischen Hintergrund. Die Kreuzesinschrift, die Pilatus anbringen
              l&auml;&szlig;t, bezeichnet Jesus als den K&ouml;nig der Juden.<br>
              Das griechische Christos ist die w&ouml;rtliche &Uuml;bersetzung
              des hebr&auml;ischen Messias, w&ouml;rtlich als &#8222;Gesalbter&#8220; &uuml;bersetzt.
              Sowohl bei der Geburt wie beim Besuch der Sterndeuter wird das
              neugeborene Kind als der Messias verk&uuml;ndet.</font></P>
            <P><font face="Arial, Helvetica, sans-serif"><strong>Der von den
                  Juden erwartete messias h&auml;tte nicht sterben d&uuml;rfen</strong><br>
            </font><font face="Arial, Helvetica, sans-serif">Die Evangelien
                von Matth&auml;us und Lukas haben in einem
                  Stammbaum die Vorfahren Jesu aufgelistet, um ihn als Nachkomme
                  des K&ouml;nigs
                Davids zu erweisen. In mittelalterlichen Kirchen wird das durch
                die Wurzel Jesse dargestellt. Jesse oder Isai war der Vater Davids.
                Aber es hat viele Nachkommen Davids gegeben. In den B&uuml;chern
                der K&ouml;nige wird ihre Regentschaft beschrieben. Die Eroberung
                Jerusalems durch Nebukadnezar hat dem K&ouml;nigtum ein Ende gesetzt.
                Herodes, zur Zeit Jesu K&ouml;nig von Roms Gnaden, war ein Empork&ouml;mmling
                und konnte sich nicht auf die Abstammung aus dem j&uuml;dischen
                K&ouml;nigsgeschlecht berufen.<br>
                Aber selbst wenn Jesus ein Nachkomme Davids war, er h&auml;tte
                nach j&uuml;discher Vorstellung das Reich seines Vaters Davids
                wieder aufrichten m&uuml;ssen, um sich f&uuml;r die Juden als der
                Messias zu erweisen. Statt dessen ist er elendig am Kreuz verendet.
                W&auml;re er der von Gott erw&auml;hlte Messias, dann h&auml;tte
                Gott ihn vor diesem Schicksal bewahrt. Das dachten nicht nur die
                Gegner Jesu, sondern auch seine J&uuml;nger. Wenn Jesus wirklich
                der Gesandte Gottes gewesen sein soll, dann h&auml;tte Gott verhindern
                m&uuml;ssen, da&szlig; er so umgebracht wird und das noch von der
                r&ouml;mischen Besatzungsmacht. Gott, da war man sich sicher, w&uuml;rde
                seinen Erw&auml;hlten nicht einem solchen Schicksal &uuml;berantworten.<br>
                Ein gekreuzigter Messias, das ist unvorstellbar. So schreibt
                es auch Paulus: &#8222;Die Juden fordern Zeichen, die Griechen suchen
                Weisheit, Wir dagegen verk&uuml;ndigen Christus als den Gekreuzigten:
                f&uuml;r Juden ein emp&ouml;rendes &Auml;rgernis, f&uuml;r Heiden
                eine Torheit.&#8220; 1. Korintherbrief 1, 22-23<br>
                Die Christen sind dann nach Ostern mit dem Anspruch aufgetreten,
                Jesus von Nazareth sei der den Juden verhei&szlig;ene Messias. &#8222;Mit
                Gewi&szlig;heit erkenne also das ganze Haus Israel: Gott hat ihn
                zum Herrn und Messias gemacht, diesen Jesus, den ihr gekreuzigt
                habt.&#8220; Apostelgeschichte 2,36. Sie m&uuml;ssen erkannt haben,
                da&szlig; Gott die Kreuzigung seines Gesalbten zugelassen hat &#8211; wegen
                der Erl&ouml;sung der Menschen. Das Gottesknechtslied (Link zu
                Gottesknecht) im Prophetenbuch des Jesaja ist daf&uuml;r der Schl&uuml;ssel.
                Bereits w&auml;hrend des &ouml;ffentlichen Auftretens Jesu wird
                die Messiasfrage diskutiert.<br>
                </font></P>
            <P><font face="Arial, Helvetica, sans-serif"><strong>Jesus vollbringt die
                Taten eines Messias</strong><br>
            </font><font face="Arial, Helvetica, sans-serif">Johannes
                der T&auml;ufer
                  hatte den Juden gesagt: &#8222;Ich taufe
                euch nur mit Wasser. Der aber, der nach mir kommt, ist st&auml;rker
                als ich, und ich bin es nicht wert, ihm die Schuhe auszuziehen.
                Er wird euch mit dem Heiligen Geist und mit Feuer taufen.&#8220; Matth&auml;us
                3,11<br>
                Lukas berichtet &#8222;Das Volk war voll Erwartung und alle &uuml;berlegten
                im stillen, ob Johannes nicht vielleicht selbst der Messias sei.&#8220; Kap.
                3,15<br>
                Als Johannes von Herodes ins Gef&auml;ngnis geworfen worden war,
                schickt er zwei seiner J&uuml;nger zu Jesus, um sich noch einmal
                zu vergewissern, da&szlig; Jesus wirklich der Messias ist. &#8222;Als
                die beiden M&auml;nner zu Jesus kamen, sagten sie: Johannes der
                T&auml;ufer hat uns zu dir geschickt und l&auml;&szlig;t dich fragen:
                bist du der, der kommen soll, oder m&uuml;ssen wir auf einen anderen
                warten? Damals heilte Jesus viele Menschen von ihren Krankheiten
                und Leiden, befreite sie von b&ouml;sen Geistern und schenkte vielen
                Blinden das Augenlicht. Er antwortete den beiden: Geht und berichtet
                Johannes, was ihr gesehen habt: Blinde sehen wieder, Lahme gehen,
                und Auss&auml;tzige werden rein; Taube h&ouml;ren, Tote stehen
                auf, und den Armen wird das Evangelium verk&uuml;ndet. Selig, wer
                keinen Ansto&szlig; nimmt.&#8220; Lukas 7, 20-23 Jesus sagt nicht
                direkt, da&szlig; er der Messias ist, aber er zitiert mit dem Satz &#8222;Blinde
                sehen wieder ...&#8220; den Propheten Jesaja, der besonders auf
                das Kommen des Messias, des Gesalbten hingewiesen hat. Wenn seine
                Prophetenworte jetzt eingetreten sind, dann mu&szlig; Jesus der
                Messias sein.</font></P>
            <P><font face="Arial, Helvetica, sans-serif"><strong>Die<a href="auferstehung_jesu.php"> Auferstehung</a>                  erweist Jesus als Messias </strong><br>
                Die M&auml;nner
                und Frauen, die Jesus gefolgt waren, hatten in ihm den verhei&szlig;enen
                Messias erkannt. Sonst h&auml;tten sie
              sich ihm nicht angeschlossen. Durch die Hinrichtung Jesu am Kreuz
              war dieser Glaube, da&szlig; Jesu der Gesalbte Gottes ist, zerbrochen,
              zumindest bei den M&auml;nnern unter seinen Anh&auml;ngern. Erst
              die <a href="http://www.kath.de/Kirchenjahr/advent.php">Auferweckung von
              den Toten</a> hat sie erkennen lassen, da&szlig; Jesus
              wirklich der Messias ist. So predigt Petrus am Pfingsttag:<br>
&#8222;
              Jesus, den Nazor&auml;er, den Gott vor euch beglaubigt hat durch
              machtvolle Taten, Wunder und Zeichen, die er durch ihn in eurer
              Mitte getan hat, wie ihr selbst wi&szlig;t &#8211; ihn, der nach
              Gottes beschlossenem Willen und Vorauswissen hingegeben wurde,
              habt ihr durch die Hand von Gesetzlosen ans Kreuz geschlagen und
              umgebracht. Gott aber hat ihn von den Wehen des Todes befreit und
              auferweckt .... Gott hat ihn zum Herrn und Messias gemacht.&#8220; Apostelgeschichte
              2, 22-24, 36<br>
              Auch wenn die Christen Jesus als den von den Propheten angek&uuml;ndigten
              Messias erkennen, k&ouml;nnen die Juden mit Recht fragen, wo das
              angek&uuml;ndigte Reich des Friedens und der Gerechtigkeit bleibt.
              Und sie k&ouml;nnen die Christen mit Recht fragen, warum gerade
              den Juden von Christen soviel Unrecht angetan wurde. F&uuml;r die
              Christen erf&uuml;llt sich die Erwartung an das Reich Gottes erst,
              wenn Jesus ein zweites Mal wiederkommt, zum letzten Gericht und
              damit die Geschichte zu ihrem Ende bringt. Link Zu Wiederkunft
              Christi und zu <a href="menschensohn.php">Menschensohn</a><br>
              Der Titel Messias ist faktisch zum Eigennamen Jesu geworden, denn
              Christos ist der Gesalbte, die Bedeutung des Wortes Messias im
              Hebr&auml;ischen.<br>
            </font></P>
            <P><font face="Arial, Helvetica, sans-serif">Zitate<br>
              Seht, der Tag wird kommen, Spruch des Herrn, <br>
              da werde ich f&uuml;r David einen gerechten Spro&szlig; erwecken.<br>
              Er wird als K&ouml;nig herrschen und weise handeln,<br>
              f&uuml;r Recht und Gerechtigkeit wird er sorgen im Land. <br>
              Jeremia, 23,5</font></P>
            <p><font face="Arial, Helvetica, sans-serif">H&ouml;rt her, ihr vom
                Haus David! ..<br>
              Seht, die Jungfrau wird ein Kind empfangen,<br>
              sie wird einen Sohn geb&auml;ren,<br>
              und sie wird ihm den Namen Immanuel &#8211; Gott mit uns &#8211; geben.<br>
              Jesaja 7,14</font></p>
            <p><font face="Arial, Helvetica, sans-serif">An jenem Tag w&auml;chst aus dem Baumstumpf Isais (Geschlecht
              Davids) ein Reis hervor, ein junger Trieb aus seinen Wurzeln bringt
              Frucht. Der Geist des Herrn l&auml;&szlig;t sich nieder auf ihm
              ...<br>
              Er richtet nicht nach Augenschein und nicht nach dem H&ouml;rensagen
              entscheidet er,<br>
              sondern er richtet die Hilflosen gerecht und<br>
              entscheidet f&uuml;r die Armen, was recht ist....<br>
              Gerechtigkeit ist der G&uuml;rtel um seine H&uuml;ften,<br>
              Treue der G&uuml;rtel um seinen Leib.<br>
              Dann wohnt der Wolf beim Lamm,<br>
              der Panther liegt beim B&ouml;cklein....<br>
              Der L&ouml;we fri&szlig;t Stroh wie das Rind.<br>
              Der S&auml;ugling spielt am Schlupfloch der Natter.<br>
              Das Kind streckt seine Hand in die H&ouml;hle der Schlange.<br>
              Man tut nichts B&ouml;ses mehr und begeht keine Verbrechen.....<br>
              Denn das Land ist erf&uuml;llt von der Erkenntnis des Herrn, so
              wie das Meer mit Wasser gef&uuml;llt ist.<br>
              An jenem Tag wird der Spro&szlig; aus der Wurzel sein,<br>
              der dasteht als Zeichen f&uuml;r die Nationen.<br>
              Aus Kap 11 im Jesajabuch</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Eine Stimme ruft:<br>
  Bahnt f&uuml;r den Herrn einen Weg durch die W&uuml;ste!<br>
  Baut in der Steppe eine ebene Stra&szlig;e f&uuml;r unseren Gott!<br>
              Jedes Tal soll sich heben, jeder Berg sich senken.<br>
              Was krumm ist, soll gerade werden und was h&uuml;gelig ist, werde
              eben ....<br>
              Seht, Gott der Herr, kommt mit Macht, er herrscht mit starkem Arm.....<br>
              Wie ein Hirt f&uuml;hrt er seine Herde zur Weide, er sammelt sie
              mit starker Hand.<br>
              Die L&auml;mmer tr&auml;gt er auf dem Arm, die Mutterschafe f&uuml;hrt
              er behutsam.<br>
              Aus Kap. 40 im Jesajabuch</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Spruch Bileams, des
                Sohnes Beors, Spruch des Mannes mit geschlossenem Auge, Spruch
                dessen, der Gottesworte h&ouml;rt, der die Gedanken
              des H&ouml;chsten kennt, der eine Vision des Allm&auml;chtigen
              sieht, der daliegt mit entschleierten Augen:<br>
              Ich sehe, aber nicht jetzt,<br>
              ich erblicke ihn, aber nicht in der N&auml;he:<br>
              Ein Stern geht in Jakob auf, ein Zepter erhebt sich in Israel.<br>
              Buch Numeri, Kap 24, 15-17 <br>
              Bileam ist kein Jude, er soll eigentlich Israel verfluchen, erkennt
              aber, da&szlig; es das von Gott auserw&auml;hlte Volk ist und verhei&szlig;t
              f&uuml;r die Zukunft einen Heilbringer, einen Stern, der &uuml;ber
              Jakob aufgeht.</font></p>
            <p><font face="Arial, Helvetica, sans-serif">So spricht der Herr: du Bethlehem-Efrata,<br>
              so klein unter den Gauen Judas,<br>
              aus dir wird einer hervorgehen,<br>
              der &uuml;ber Israel herrschen soll....<br>
              Er wird auftreten und ihr Hirt sein in der Kraft des Herrn,<br>
              im hohen Namen Jahwes, seines Gottes.<br>
              Sie werden in Sicherheit leben,<br>
              denn nun reicht seine Macht bis an die Grenzen der Erde.<br>
              Und er wird ihr Friede sein.<br>
              Prophet Micha, im Kap.5</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Der Geist Gottes, des Herrn ruht auf mir;<br>
              Denn der Herr hat mich gesalbt. (Der Gesalbte ist der Messias)<br>
              Er hat mich gesandt, damit ich den Armen die frohe Botschaft bringe<br>
              Und alle heile, deren Herz zerbrochen ist,<br>
              damit ich den Gefangenen Entlassung verk&uuml;nde<br>
              und den Gefesselten Befreiung.<br>
              Jesaja 61,1</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Weihnachten ist nach
                Matth&auml;us und Lukas der Tag der Geburt
              des Messias. K&ouml;nig Herodes empf&auml;ngt die Sterndeuter,
              die den neu geborenen K&ouml;nig der Juden suchen. Herodes &#8222;lie&szlig; alle
              Hohenpriester und Schriftgelehrten des Volkes zusammenkommen und
              erkundigte sich bei ihnen, wo der Messias geboren werde solle.
              Sie antworteten ihm: In Bethlehem in Jud&auml;a.&#8220; Matth&auml;us
              2,4-5</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Die Hirten erhalten von den Engel die Nachricht:<br>
  Heute ist euch in der Stadt Davids der Retter geboren; er ist der
                Messias, der Herr. Und das soll euch als Zeichen dienen: Ihr
                werdet ein Kind finden, das in Windeln gewickelt, in einer Krippe
                liegt.&#8220; Lukas, 2,11-12</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Karl Rahner zeigt auf,
                wie sich der Mensch bereits unbewu&szlig;t
              auf einen Heilbringer, auf den Gesalbten ausrichtet.              Ausgehend von dem Wort Jesu, da&szlig; man in dem Armen ihm selbst
              begegnet (s. Matth&auml;us 25) folgert Rahner:<br>
&#8222;
              Wenn man aus dem Wort Jesu, er selbst werde wahrhaft in jedem N&auml;chsten
              geliebt, nicht ein &#8222;als ob&#8220; oder nur die Theorie einer
              juristischen Anrechnung macht, dann sagt dieses von der Erfahrung
              der Liebe selbst her gelesene Wort, da&szlig; eine absolute Liebe,
              die sich radikal und vorbehaltlos auf einen Menschen einl&auml;&szlig;t,
              implizit Christus glaubend und immer liebend bejaht. Und das ist
              richtig. Denn der blo&szlig; endliche und immer unzuverl&auml;ssige
              Mensch kann f&uuml;r sich allein die ihm entgegengebrachte, absolute
              Liebe, in der eine Person sich schlechthin &#8222;engagiert&#8220; und
              an den anderen wagt, nicht als sinnvoll rechtfertigen; f&uuml;r
              sich allein k&ouml;nnte er nur unter Vorbehalt geliebt werden. &#8230;.
              Aber die Liebe &#8230;. will mehr als nur eine ihr transzendent
              bleibende g&ouml;ttliche &#8222;Garantie&#8220;: sie will eine
              Einheit von Gottes- und N&auml;chstenliebe, in der N&auml;chstenliebe
              und &#8211; wenn
              evtl. auch blo&szlig; unthematisch &#8211; Gottesliebe so erst
              ganz absolut ist. Damit aber sucht sie den Gottmenschen, d.h. jenen,
              der als Mensch mit der Absolutheit der Liebe zu Gott geliebt werden
              kann.&#8220;<br>
              Ein weiteres Indiz, da&szlig; der Mensch auf den g&ouml;ttlichen
              Messias wartet, leitet Rahner aus der Tatsache des Todes ab:<br>
&#8222;
              Der Tod ist die eine, das ganze Leben durchwaltende Tat, in der
              der Mensch als Wesen der Freiheit &uuml;ber sich als ganzes verf&uuml;gt,
              und zwar so, da&szlig; diese Verf&uuml;gung die Annahme der absoluten
              Verf&uuml;gtheit in der radikalen Ohnmacht ist (bzw. sein soll),
              die im Tod erscheint und erlitten wird. Soll aber die freie, bereite
              Annahme der radikalen Ohnmacht durch das &uuml;ber sich selbst
              verf&uuml;gende und verf&uuml;gen-wollende Freiheitswesen nicht
              die Annahme des Absurden sein, die dann mit ebenso gutem &#8222;Recht&#8220; unter
              Protest abgelehnt werden k&ouml;nnte, dann impliziert diese Annahme
              bei dem Menschen, der zutiefst nicht abstrakte Ideen und Normen,
              sondern in seiner Geschichtlichkeit (schon gegebene oder zuk&uuml;nftige)
              Wirklichkeit als Grund seines Daseins bejaht, die ahnende Erwartung
              oder Bejahung eines (schon gegebenen oder k&uuml;nftig erhofften)
              Todes, in dem die &#8211; bei uns empirisch bleibende &#8211; Dialektik
              von Tat und ohnm&auml;chtigem Leiden im Tod vers&ouml;hnt ist.
              Das ist aber nur dann der Fall, wenn diese reale Dialektik dadurch &#8222;aufgehoben&#8220; ist,
              da&szlig; sie die  Wirklichkeit dessen selbst ist, der der letzte
              Grund dieser Zweiheit ist.&#8220;<br>
              Die mit der Zukunft verbundene Hoffnung sucht einen Grund:<br>
&#8222;
              Sein (des Menschen) Gang in die Zukunft ist das best&auml;ndige
              Bem&uuml;hen, seine inneren und &auml;u&szlig;eren Selbstentfremdungen
              und den Abstand zu verringern zwischen dem, was er ist, und dem,
              was er sein will und soll. Ist die absolute Vers&ouml;hnung (individuell
              und kollektiv) nur das ewig ferne, immer nur asymptotisch angezielte,
              nur in Distanz bewegende Ziel oder als absolute Zukunft das erreichbare
              Ziel, ohne da&szlig; es, als erreichtes, das Endliche abschaffen
              und in der Absolutheit Gottes verschlingen m&uuml;&szlig;te? &#8230; Der
              Christ hat von dieser Hoffnung her ein Verst&auml;ndnis f&uuml;r
              das, was der Glaube in der Inkarnation und Auferstehung Jesu Christi
              bekennt als den irreversiblen Anfang des Kommens Gottes als der
              absoluten Zukunft der Welt und der Geschichte.&#8220;<br>
              Jesus Christus, in Sacramentum Mundi, Theologisches Lexikon f&uuml;r
              die Praxis, Bd. 2, 1968, S. 61ff</font></p>
            <p><font size="2" face="Arial, Helvetica, sans-serif">Eckhard Bieger<br>
              </font>&copy;<a href="http://www.kath.de"> <font face="Arial, Helvetica, sans-serif">www.kath.de</font></a></p></TD>
          <TD background=boxright.gif><IMG height=8 alt=""
            src="boxright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=8 alt="" src="boxbottomleft.gif"
            width=8></TD>
          <TD background=boxbottom.gif><IMG height=8 alt=""
            src="boxbottom.gif" width=8></TD>
          <TD><IMG height=8 alt="" src="boxbottomright.gif"
            width=8></TD></TR></TBODY></TABLE></TD><td style='vertical-align:top;'>

            <a href="http://www.echter.konkordanz.de/product_info.php?info=p15466_Der-Messias.html">
            <h4>Das Buch zum Thema: "Der Messias"</h4>

 <script type="text/javascript"><!--
        google_ad_client = "pub-9537752113242914";
        google_ad_width = 336;
        google_ad_height = 280;
        google_ad_format = "336x280_as";
        google_ad_type = "text_image";
        //2006-10-23: Lexikon Symbole
        google_ad_channel = "8495253357";
        //--></script>
        <script type="text/javascript"
          src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
        </script>
</td>
</TR>
  <TR>
    <TD vAlign=top align=left>&nbsp; </TD>
  </TR></TBODY></TABLE></BODY></HTML>