<HTML><HEAD><TITLE>Alleinseligmachen Kirche</TITLE>
<META http-equiv=Content-Type content="text/html; charset=iso-8859-1"><LINK 
title=fonts href="kaltefleiter.css" type=text/css 
rel=stylesheet>
<META content="MSHTML 5.50.4134.600" name=GENERATOR></HEAD>
<BODY bgColor=#ffffff leftMargin=6 topMargin=6 marginheight="6" marginwidth="6">
<TABLE cellSpacing=0 cellPadding=6 width="100%" border=0>
  <TBODY>
  <TR>
    <TD vAlign=top align=left width=100> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b>Philosophie&amp;Theologie</b></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td> <?php include("logo.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10"> <?php include("az.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </TD>
    <TD vAlign=top rowSpan=2>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
        <TBODY>
        <TR vAlign=top align=left>
          <TD width=8><IMG height=8 alt="" 
            src="boxtopleftcorner.gif" width=8></TD>
          <TD background=boxtop.gif><IMG height=8 alt="" 
            src="boxtop.gif" width=8></TD>
          <TD width=8><IMG height=8 alt="" 
            src="boxtoprightcorner.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxtopleft.gif><IMG height=8 alt="" 
            src="boxtopleft.gif" width=8></TD>
          <TD bgColor=#e2e2e2><H1><font face="Arial, Helvetica, sans-serif"> Alleinseligmachende
                Kirche</font></H1>
          </TD>
          <TD background=boxtopright.gif><IMG height=8 
            alt="" src="boxtopright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=13 alt="" src="boxdividerleft.gif" 
            width=8></TD>
          <TD background=boxdivider.gif><IMG height=13 
            alt="" src="boxdivider.gif" width=8></TD>
          <TD><IMG height=13 alt="" 
            src="boxdividerright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxleft.gif><IMG height=8 alt="" 
            src="boxleft.gif" width=8></TD>
          <TD class=L12>
            <P><STRONG><font face="Arial, Helvetica, sans-serif">Extra ecclesiam
                  nulla salus<br>
            </font></STRONG><font face="Arial, Helvetica, sans-serif"><br>
               D</font><font face="Arial, Helvetica, sans-serif">as Axiom von
               der &#8222;alleinseligmachenden Kirche&#8220; riecht
              nach Gruppenegoismus, Gr&ouml;&szlig;enwahn und Intoleranz. Es
              will dem heutigen Menschen nicht mehr &uuml;ber die Lippen. Dennoch:
              Man darf sich durch die Anst&ouml;&szlig;igkeit des Wortlauts die
              gemeinte Sache nicht verdecken lassen. Was ist mit dem Axiom gemeint?
              Zun&auml;chst zu seinem Ursprung: Bereits Origenes hat formuliert: &#8222;Au&szlig;erhalb
              der Kirche wird niemand gerettet&#8220; (Origenes, In Jesu Nave
              3,5; PG 12, 841). Sp&auml;ter wurde daraus der Satz: &#8222;Au&szlig;erhalb
              der Kirche kein Heil&#8220; (Extra Ecclesiam nulla salus). Dieser
              Satz geh&ouml;rt zum festen Glaubensgut der Kirche. Denn er speist
              sich aus zwei anderen Glaubensaussagen:</font></P>
            <p><font face="Arial, Helvetica, sans-serif">1.<a href="sohn_gottes.php"> Christus</a> allein ist
                die Wahrheit und der Weg f&uuml;r das Heil
              der Welt (vgl. Joh 14,6). <br>
              2.	Die Kirche ist der Ort unter den V&ouml;lkern, wo das von Christus
              geschaffene Heil anwesend und wirksam ist. &#8211; Weil beides
              vom Neuen Testament her v&ouml;llig eindeutig ist, kann die Kirche
              das &#8222;alleinseligmachend&#8220; nicht zur&uuml;cknehmen. Sie
              w&uuml;rde sich sonst von der Erl&ouml;sung durch Christus und
              von ihrer Indienstnahme als &#8222;Sakrament des Heils f&uuml;r
              die Welt&#8220; verabschieden.</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Die Frage &#8222;K&ouml;nnen auch Nichtgetaufte ewig selig werden?&#8220; engt
              das, worum es bei dem Axiom geht, in einer unertr&auml;glichen
              Weise ein. Was diese Spezialfrage angeht, hat die Kirche seit Jahrhunderten
              gesagt, dass es ein votum ecclesiae gibt, das hei&szlig;t, eine
              Sehnsucht nach der Wahrheit, nach dem Guten, nach der richtigen
              Gesellschaftsordnung. Wer von Christus nichts wei&szlig; und nie
              erfahren hat, was Kirche ist, aber in solcher Sehnsucht lebt und
              handelt, wird sein ewiges Heil nicht verlieren.</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Bei dem &#8222;alleinseligmachend&#8220; geht es aber gar nicht
              in erster Linie um diese Spezialfrage. Die Verhei&szlig;ungen der
              Bibel meinen mehr als die ewige Seligkeit des einzelnen nach dem
              Tod. Es geht immer auch und sogar vor allem um diese Welt &#8211; ob
              es in ihr Frieden und Gl&uuml;ck, Freiheit und Menschenw&uuml;rde,
              Solidarit&auml;t und Menschlichkeit gibt. Gott will nichts sehnlicher
              als eben dieses &#8222;Heil der Welt&#8220;.</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Wie kann es Realit&auml;t werden? Die Bibel sagt: Es hat in der
              Welt an einer Stelle angefangen: mit dem Glauben und dem Gehorsam
              Abrahams. Es hat Form angenommen in Israel, wenn das Gottesvolk
              die Sozialordnung vom Sinai lebte und auf seine Propheten h&ouml;rte.
              Und es ist endg&uuml;ltig in der Welt festgemacht durch die Botschaft
              und die Lebenshingabe Jesu, der Israel zum endzeitlichen Gottesvolk
              gesammelt hat.</font></p>
            <p><font face="Arial, Helvetica, sans-serif">&#8222;Au&szlig;erhalb der Kirche kein Heil&#8220; hei&szlig;t
              also vor allem: Auf einem langen Weg wurde mitten in der Welt in
              einem unglaublichen Experiment unter vielen Opfern die richtige
              Gesellschaft gefunden, die dem Willen Gottes entspricht. Diese
              Gesellschaft zu leben und auszubreiten, w&auml;re das Gl&uuml;ck
              f&uuml;r die V&ouml;lker. Die Erfahrung des Glaubens sagt: Es ist
              der einzige Weg!</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Professor Dr. Gerhard
                Lohfink, Bad T&ouml;lz</font></p>
            <p></p>
            <P>&copy;<a href="%20">  </a><font size="2" face="Arial, Helvetica, sans-serif"><a href="http://www.akademie-cavalletti.de/">Akademie
              f&uuml;r die Theologie des Volkes Gottes</a></font></P>
            </TD>
          <TD background=boxright.gif><IMG height=8 alt="" 
            src="boxright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=8 alt="" src="boxbottomleft.gif" 
            width=8></TD>
          <TD background=boxbottom.gif><IMG height=8 alt="" 
            src="boxbottom.gif" width=8></TD>
          <TD><IMG height=8 alt="" src="boxbottomright.gif" 
            width=8></TD></TR></TBODY></TABLE></TD><td style='vertical-align:top;'>
 <script type="text/javascript"><!--
        google_ad_client = "pub-9537752113242914";
        google_ad_width = 336;
        google_ad_height = 280;
        google_ad_format = "336x280_as";
        google_ad_type = "text_image";
        //2006-10-23: Lexikon Symbole
        google_ad_channel = "8495253357";
        //--></script>
        <script type="text/javascript"
          src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
        </script>
</td>
</TR>
  <TR>
    <TD vAlign=top align=left>&nbsp; </TD>
  </TR></TBODY></TABLE></BODY></HTML>
