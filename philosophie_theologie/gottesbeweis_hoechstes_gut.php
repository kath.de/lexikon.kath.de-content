<HTML><HEAD><TITLE>Gottesbeweis aus dem h&ouml;chsten Guten</TITLE>
<meta name="title" content="Philosophie&amp;Theologie">
<meta name="author" content="Redaktion kath.de">
<meta name="publisher" content="kath.de">
<meta name="copyright" content="kath.de">
<meta name="description" content="">
<meta name="abstract" content="Lexikon &uuml;ber Philosophie&amp;Theologie">
<meta http-equiv="content-language" content="de">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="date" content="2006-00-01">
<meta name="robots" content="index,follow">
<meta name="revisit-after" content="10 days">
<meta name="revisit" content="after 10 days">
<meta name="DC.Title" content="Philosophie&amp;Theologie">
<meta name="DC.Creator" content="Redaktion kath.de">
<meta name="DC.Contributor" content="J�rgen Pelzer">
<meta name="DC.Rights" content="kath.de">
<meta name="DC.Publisher" content="kath.de">
<meta name="DC.Date" content="2006-00-01">
<meta name="DC.Description" content="Lexikon &uuml;ber Philosophie&amp;Theologie">
<meta name="DC.Language" content="de">
<meta name="DC.Type" content="Text">
<meta name="DC.Format" content="text/html">
<meta name="DC.Identifier" content="http://www.kath.de/lexikon/philosophie_theologie/">
<meta name="keywords" lang="de" content="h�chstes Gut, h�chstes abgeleitetes Gut, Kant, Gl�ck, Gott">
</HEAD>
<BODY bgColor=#ffffff leftMargin=6 topMargin=6 marginheight="6" marginwidth="6">
<TABLE cellSpacing=0 cellPadding=6 width="100%" border=0>
  <TBODY>
  <TR>
    <TD vAlign=top align=left width=100> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b>Philosophie&amp;Theologie</b></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td> <?php include("logo.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10"> <?php include("az.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </TD>
    <TD vAlign=top rowSpan=2>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
        <TBODY>
        <TR vAlign=top align=left>
          <TD width=8><IMG height=8 alt="" 
            src="boxtopleftcorner.gif" width=8></TD>
          <TD background=boxtop.gif><IMG height=8 alt="" 
            src="boxtop.gif" width=8></TD>
          <TD width=8><IMG height=8 alt="" 
            src="boxtoprightcorner.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxtopleft.gif><IMG height=8 alt="" 
            src="boxtopleft.gif" width=8></TD>
          <TD bgColor=#e2e2e2> 
            <H1><font face="Arial, Helvetica, sans-serif">Gottesbeweis aus
                dem h&ouml;chsten Guten</font></H1>
          </TD>
          <TD background=boxtopright.gif><IMG height=8 
            alt="" src="boxtopright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=13 alt="" src="boxdividerleft.gif" 
            width=8></TD>
          <TD background=boxdivider.gif><IMG height=13 
            alt="" src="boxdivider.gif" width=8></TD>
          <TD><IMG height=13 alt="" 
            src="boxdividerright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxleft.gif><IMG height=8 alt="" 
            src="boxleft.gif" width=8></TD>
          <TD class=L12>
            <P><STRONG><font face="Arial, Helvetica, sans-serif">Der Mensch ist
                  auf ein h&ouml;chstes Gute ausgerichtet<br>
                  </font></STRONG><font face="Arial, Helvetica, sans-serif"><br>
                  Der Mensch erf&auml;hrt sich in seinen Vorhaben und Handlungen
                  auf das Gute ausgerichtet. Er erstrebt, was ihm gut tut, Nahrung,
                  Einkommen, eine gegl&uuml;ckte Beziehung. Im Gewissen wird
                  ihm deutlich, wie er sich ethisch verhalten soll, also dem
                  anderen die Wahrheit sagen, ihm nichts wegnehmen, einen angemessenen
                  Preis zahlen, den anderen nicht verletzen. Das Gewissen lenkt
                  das Handeln auf Gutes, das nicht nur f&uuml;r mich, sondern
                  f&uuml;r den anderen gut ist. Diese Tendenz auf das Gute wird
                  von uns zwar begrenzt, wenn wir anderen aus Neid und Eifersucht
                  das Gute, konkret ihre Bef&ouml;rderung, die Geburt eines Kindes,
                  beruflichen oder sportlichen Erfolg, nicht g&ouml;nnen. Jedoch
                  stellen Neid und Eifersucht die grunds&auml;tzliche Tendenz
                  hin auf das Gute nicht in Frage. Diese Tendenz beinhaltet,
                  da&szlig; wir zur Welt Ja sagen und da&szlig; wir davon ausgehen,
                  da&szlig; die Welt insgesamt gut ist, auch wenn wir &Uuml;bles
                  erleben und Ungl&uuml;ck erleiden. Da die Tendenz auf das Gute
                  insgesamt nicht begrenzt ist, k&ouml;nnte daraus gefolgert
                  werden, da&szlig; wir nicht nur viele einzelne G&uuml;ter bejahen,
                  sondern in unserem Streben auch eine Instanz erkennen, die
                  daf&uuml;r einsteht, da&szlig; letztlich alles gut ist. Zwei
                  Philosophen haben diesen Ansatz ihrem Denken &uuml;ber Gott
                  zugrunde gelegt, Platon und Kant. Ihre Argumentation soll kurz
                vorgestellt und durch Zitate belegt werden.</font></P>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Das letzthin Gute macht das begrenzte Gute erst gut</strong><br>
  Platon lebte in einer Zeit des Umbruchs, bisherige Wertvorstellungen
                zerbrachen, Skeptiker traten auf, andere erkl&auml;rten, da&szlig; Macht
                der h&ouml;chste Wert sei. In dieser Situation wird die Frage
                brennend: Wie kann das Gute, das wir tun sollen, gerechtfertigt
                werden? Warum soll der Tyrann seine Macht nicht zur Unterdr&uuml;ckung
                anderer gebrauchen d&uuml;rfen, wenn f&uuml;r ihn Macht das H&ouml;chste
                ist, was er anstrebt. Warum sind das Eigentum und das Leben des
                anderen von h&ouml;herem Wert als das Machtstreben des Tyrannen?
                Die sittlichen Werte, die in der Achtung des anderen Menschen
                gr&uuml;nden, m&uuml;ssen so fest verankert sein, da&szlig; kein
                Dieb, kein Betr&uuml;ger, kein Politiker und sonst jemand behaupten
                kann, da&szlig; sie f&uuml;r ihn nicht gelten. Platon nennt den
                Grund, der das einzelne &#8222;gut&#8220; sein l&auml;&szlig;t,
                die &#8222;Idee des Guten&#8220;. Diese Idee macht nicht nur
                das einzelne gut, sondern der menschliche Verstand zielt auf
                die Idee des Guten, wenn er etwas Konkretes als gut erkennt.
                Wir sto&szlig;en hier auf die gleiche Tendenz des menschlichen
                Geistes wie bei der <a href="gottesbeweis_wahrheit.php">Wahrheit</a>. Die einzelne Wahrheit wie auch
                das einzelne Gute sind in den Gesamthorizont einer allumfassenden,
                absolut g&uuml;ltigen Wahrheit eingebettet. So ist das einzelne
                nur gut, wenn es im Gesamthorizont des Guten eingeordnet werden
                kann.<br>
                Diese Idee des Guten kann nicht darin bestehen, da&szlig; der Mensch
              sie sich ausdenkt oder sie ihm eingeboren ist. Denn dann m&uuml;&szlig;te
              der Mensch ihr nur solange folgen, wie sie seinem &Uuml;berleben
              nutzt. Au&szlig;erdem w&uuml;rde die Idee des Guten verschwinden,
              wenn der Mensch es sich anders &uuml;berlegt. Das ist nicht so
              einfach m&ouml;glich, denn in der Idee des Guten meldet sich ein
              unbedingter Anspruch. Der Anspruch kann aber nur wirklich bestehen,
              wenn die Instanz, die ihn erhebt, selbst in sich gut ist. Denn
              etwas, das nicht gut ist, k&ouml;nnte den Anspruch an unseren Geist
              nicht stellen, immer auf das Gute aus zu sein, sowohl in dem, was
              ich f&uuml;r mich unternehme wie auch in den Folgen, die meine
              Handlungen f&uuml;r andere haben. W&auml;re die Instanz, die das
              Gute letztlich begr&uuml;ndet, auf etwas anderes ausgerichtet,
              dann w&auml;re das Gute in etwas anderem begr&uuml;ndet. Das ist
              sie auch, denn das Gute ist etwas, das nicht nur gedacht, sondern
              angestrebt wird. Das hei&szlig;t notwendig, da&szlig; das Gute
              auch wirklich existieren mu&szlig;. Als Idee w&auml;re es nur der
              Hinweis auf das tats&auml;chlich existierende Gute. </font></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Immanuel Kant:
                  Eine Instanz mu&szlig; garantieren, da&szlig; das
              Gute zum Gl&uuml;ck des einzelnen f&uuml;hrt.</strong><br>
              Kant geht davon aus, da&szlig; uns im Gewissen die Forderungen
              zu sittlichem Handeln unmittelbar erkennbar sind. Dieses sittliche
              Handeln kann nicht beim Gedanken stehen bleiben, sondern mu&szlig; in
              der konkreten Welt umgesetzt werden. Diese Welt funktioniert aber
              nicht nach den sittlichen, sondern nach den Naturgesetzen. Sittliches
              und Naturgesetzliches kommen also nicht einfach &uuml;berein. Der
              Gerechte wird genauso krank und mu&szlig; sterben wie der Ungerechte.
              Die Welt, in der der Mensch sich vorfindet, ist zudem durch menschliche
              Schw&auml;che und Unlauterkeit gepr&auml;gt. Nun zielt aber das
              sittliche Handeln auf eine Welt, in der alle Menschen friedlich
              und in eigener W&uuml;rde zusammenleben k&ouml;nnen. In dieser
              Welt findet der Mensch sein Gl&uuml;ck. Denn sittliches Handeln
              kann nicht gleichwertig dem unsittlichen Verhalten sein, es mu&szlig; dem
              Menschen das h&ouml;chstm&ouml;glich Gute bringen, eben sein Gl&uuml;ck.
              Wenn das Sittengesetz den Menschen nicht zu seinem Gl&uuml;ck f&uuml;hren
              w&uuml;rde, w&auml;re es falsch und der Mensch m&uuml;&szlig;te
              sich ein anderes Gesetz f&uuml;r sein Handeln suchen. Da das Sittengesetz
              den Menschen aber unbedingt, d.h. ohne Wenn und Aber fordert, mu&szlig; es
              eine Instanz geben, die das Sittengesetz mit der Welt zusammenf&uuml;hrt,
              so da&szlig; der einzelne sein Gl&uuml;ck finden kann. Diese Instanz
              kann aber nur die sein, die auf der einen Seite das Sittengesetz
              in den Menschen hineingelegt hat und zum anderen Sch&ouml;pfer
              der Welt ist, in der sich der Mensch vorfindet.</font></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Vergleich Argumentationswege von Platon und Kant:</strong><br>
  Das Gute wird dem Menschen in seinem Streben deutlich. Er will
                das Gute f&uuml;r sich verwirklichen. Gut ist aber etwas erst,
                wenn es unter der Perspektive der &#8222;Idee des Guten&#8220; erkannt
                wird. Diese Idee kann nicht nur gedacht sein, sondern mu&szlig; real
                existieren, denn etwas kann den Menschen nur zum Guten hinlenken,
                wenn es in sich selbst gut ist. Das h&ouml;chste Gute mu&szlig; existieren,
                es ist identisch mit dem absoluten Sein.<br>
                Anders Kant. Er erschlie&szlig;t eine Instanz, die die sittliche
              und die von Naturgesetzen regierte Welt &uuml;berein bringt. Kant
              erkennt, da&szlig; der Mensch zwei Gesetzm&auml;&szlig;igkeiten
              unterliegt. Einmal ist er durch das Sittengesetz dem Anspruch des
              Guten unterstellt, zum anderen findet er sich in einer Welt vor,
              die nach naturwissenschaftlichen Gesetzen funktioniert und gutes
              Handeln nicht mit Gl&uuml;cklichsein belohnt. Es mu&szlig; also
              eine Instanz geben, die den sittlich handelnden Menschen mit Gl&uuml;cklichsein
              belohnt und die zugleich Urheber der Welt des Sittlichen wie der
              Welt ist. Da der Gute wie der B&ouml;se gleicherweise krank werden
              und sterben, mu&szlig; die Zusammenf&uuml;hrung von sittlicher
              und naturhafter Welt in einem anderen Leben erfolgen. Daraus folgt,
              da&szlig; der Mensch unsterblich sein mu&szlig;. Die Argumentation
              Kants h&auml;ngt ganz an der bedingungslosen Verpflichtung, das
              Gute zu tun und damit eine Welt aufzubauen, in der jeder Mensch
              gl&uuml;cklich sein kann. Platons Beweisgang geht von dem begrenzten
              Guten aus, das der Mensch erkennt und anstrebt, das aber nur m&ouml;glich
              ist, wenn es ein unbedingtes Gutes gibt, das identisch ist mit
              dem h&ouml;chsten Sein. Kant geht von der sittlichen Verpflichtung
              aus, die eine Instanz fordert, die das Gute belohnt. Kant spricht
              daher von einem Postulat, das allerdings f&auml;hig ist, die ganze
              Beweislast zu tragen, denn das Sittengesetz ist in seiner G&uuml;ltigkeit
              nicht in die Disposition des Menschen gestellt. Die unbedingte
              Geltung fordert, da&szlig; das Ergebnis der Befolgung des Sittengesetzes
              die Erlangung des Gl&uuml;cks ist. Denn w&auml;ren die Folgen gleich,
              ob jemand die Gebote beachtet oder nicht, w&auml;re das ein innerer
              Widerspruch, der nicht m&ouml;glich ist, wenn das Sittengesetz
              mit einem unbedingten Anspruch an den Menschen herantritt. </font></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Voraussetzung: Die Welt ist letztlich gut</strong><br>
  Sowohl f&uuml;r Platon wie f&uuml;r Kant steht fest, da&szlig; die
              Welt letztlich gut ist und der Urheber der Welt den sittlich handelnden
              Menschen belohnt. Wird diese Voraussetzung in Frage gestellt, dann
              gilt der Beweisgang nicht mehr. Nietzsche und Schopenhauer sind
              den Weg gegangen. Wer jedoch die Grundlage f&uuml;r das Gute wegnimmt,
              entzieht dem sittlichen Handeln den Boden. Der Nationalsozialismus
              hat die Moral ausgehebelt und das Recht des St&auml;rkeren als
              das proklamiert, was gut ist. Der Leidtragende, der eben vom St&auml;rkeren
              ausgenutzt und umgebracht wird, hat im Sinne dieser Ideologie kein
              Recht. Das Experiment des Nationalsozialismus hat gezeigt, da&szlig; menschliches
              Handeln von der Idee des Guten geleitet sein mu&szlig;, soll der
              Mensch sich nicht selbst vernichten.</font></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Zitate:</strong><br>
              Platon:<br>
              &quot;
            </font><font face="Arial, Helvetica, sans-serif">Wenn auch in den
            Augen Gesicht ist und, wer sie hat, versucht es zu gebrauchen, und
                wenn auch Farbe f&uuml;r sie da ist, so wei&szlig;t du wohl, wenn nicht
                ein drittes Wesen hinzukommt, welches eigens hierzu da ist seiner
                Natur nach, da&szlig; dann das Gesicht doch nichts sehen wird und
                die Faben unsichtbar bleiben. .. das Licht&quot; Politeia
                507 d<br>
                  &quot;Die Sonne, denke ich, wirst du sagen, verleihe dem Sichtbaren
                nicht nur das Verm&ouml;gen, gesehen zu werden, sondern auch das Werden
                und Wachstum und Nahrung ... Ebenso nun sage auch, da&szlig; dem Erkennbaren
                nicht nur das Erkanntwerden von dem Guten komme, sondern auch
                      das Sein und Wesen habe es von ihm, da doch das Gute selbst
                      nicht
                das Sein ist, sondern noch &uuml;ber das Sein an W&uuml;rde und Kraft hinausragt.&quot;
                Politeia 509b</font><font face="Arial, Helvetica, sans-serif"><br>
&#8222;
                  Im Bereich des Erkennbaren zeigt sich zuletzt und schwer zu erfassen
                  die Idee des Guten.; hat sie sich einmal gezeigt, so mu&szlig; man
                  folgern, da&szlig; sie f&uuml;r alles die Ursache alles Rechten
                  und Sch&ouml;nen ist, das sie im Bereich des Sichtbaren das Licht
                  und dessen Herr, (die Sonne) erzeugt, im Bereich des Denkbaren
                  aber selbst als Herrscherin waltend uns zu Wahrheit und Vernunft
                  verhilft. Daher mu&szlig; also diese Idee erkannt haben, wer einsichtig
                  handeln will, sei es in pers&ouml;nlichen oder in &ouml;ffentlichen
                  Angelegenheiten. Politeia 517 b/c<br>
                  Die Sonne wird von Platon als Bild f&uuml;r das Gute gesehen. Wie
                  wir in ihrem Licht erst die Gegenst&auml;nde erkennen k&ouml;nnen,
                  so geht von der Idee des Guten jenes h&ouml;here Licht aus, in
                  welchem wir zu geistigen Einsichten gelangen und die uns das Gutsein
                des jeweils einzelnen erst erkennen l&auml;&szlig;t. </font></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Kant</strong><br>
  Es kann der Vernunft unm&ouml;glich gleichg&uuml;ltig sein, wie
              die Beantwortung der Frage ausfallen m&ouml;ge: was denn bei unserem
              Rechthandeln herauskomme, und worauf wir, gesetzt wir h&auml;tten
              dieses nicht v&ouml;llig in unserer Gewalt, doch als einen Zweck
              unser Tun und Lassen richten k&ouml;nnen, um damit wenigstens zuzustimmen.&#8220; Religion
              innerhalb der Grenzen der blo&szlig;en Vernunft. S. 4 <br>
&#8222;
              Es ist unser nat&uuml;rliches Bed&uuml;rfnis, &#8222;zu allem unseren
              Tun und Lassen im Ganzen genommen irgend einen Endzweck, der von
              der Vernunft gerechtfertigt werden kann, zu denken.&#8220; Ebd.
              5</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Die Welt, in der sittliches
                Handeln nicht nur den so Handelnden dem Gl&uuml;ck w&uuml;rdig macht, sondern wo er auch dieses Gl&uuml;ck
              erlangt, nennt Kant das &#8222;h&ouml;chste abgeleitete Gut.&#8220; Das
              h&ouml;chste Gut selbst ist Gott. <br>
              Die notwendige Verkn&uuml;pfung von Gl&uuml;cksw&uuml;rdigkeit
              durch Befolgen des Sittengesetzes und erlangter Gl&uuml;ckseligkeit &#8222;kann
              durch die Vernunft nicht erkannt werden, wenn man blo&szlig; die
              Natur zum Grunde legt, sondern darf nur gehofft werden, wenn eine
              h&ouml;chste Vernunft, die nach moralischen Gesetzen gebietet,
              zugleich als Ursache der Natur zum Grunde gelegt wird.&#8220;<br>
              Kritik der reinen Vernunft, (abgek&uuml;rzt KrV) A 810/B 838<br>
&#8222;
              Daher auch jedermann die moralischen Gesetze als Gebote ansieht,
              welches sie nicht sein k&ouml;nnten, wenn sie nicht a priori angemessene
              Folgen mit der ihrer Regel verkn&uuml;pfen, und also Verhei&szlig;ungen
              und Drohungen bei sich f&uuml;hrten. Dies k&ouml;nnten sie aber
              nicht tun, wo sie nicht in einem notwendigen Wesen, als dem h&ouml;chsten
              Gut liegen, welches eine solche zweckm&auml;&szlig;ige Einheit
              allein m&ouml;glich machen k&ouml;nnte.&#8220;<br>
              KrV A811, B 839</font></p>
            <p><font face="Arial, Helvetica, sans-serif">&#8222;Denn der Gl&uuml;ckseligkeit bed&uuml;rftig, ihrer auch
              w&uuml;rdig, dennoch aber derselben nicht teilhaftig zu sein, kann
              mit dem vollkommenen Wollen eines vern&uuml;nftigen Wesens, welches
              zugleich alle Gewalt h&auml;tte, wenn wir uns nur eines solches
              zum Versuche denken, gar nicht zusammen bestehen k&ouml;nnen.&#8220;<br>
              Kritik der Praktischen Vernunft (KpV) 199</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Gott mu&szlig; von der
                Moral postuliert werden. Der Mensch ist verpflichtet, das &#8222;h&ouml;chste
                abgeleitete Gut&#8220;, d.h.
              durch sein Handeln eine von dem Sittengesetz durchdrungene Welt
              hervorzubringen. Das h&ouml;chste urspr&uuml;ngliche Gut ist Gott: <br>
                <br>
&#8222;
                Gl&uuml;ckseligkeit ist der Zustand eines vern&uuml;nftigen Wesens
                in der Welt, dem es, im Ganzen seiner Existenz, alles nach Wunsch
                und Willen geht, und beruht also auf der &Uuml;bereinstimmung
                der Natur zu seinem guten Zwecke, imgleichen zum wesentlichen
                Bestimmungsgrund seines Willens. Nun gebietet das moralische
                Gesetz, als ein Gesetz der Freiheit, durch Bestimmungsgr&uuml;nde,
                die von der Natur und der &Uuml;bereinstimmung derselben zu unserem
                Begehrungsverm&ouml;gen (als Triebfedern) ganz unabh&auml;ngig
                sein sollen; das handelnde vern&uuml;nftige Wesen in der Welt
                aber ist doch nicht zugleich Ursache der Welt und der Natur selbst.
                Also ist in dem moralischen Gesetze nicht der mindeste Grund
                zu einem notwendigen Zusammenhang zwischen Sittlichkeit und der
                ihr proportionierten Gl&uuml;ckseligkeit eines zur Welt als Teil
                geh&ouml;rigen, und daher von ihr abh&auml;ngigen Wesens, welches
                eben darum durch seinen Willen nicht Ursache dieser Natur sein,
                und sie, was seine Gl&uuml;ckseligkeit betrifft, mit seinen praktischen
                Grunds&auml;tzen aus eigenen Kr&auml;ften nicht durchg&auml;ngig
                einstimmig machen kann. Gleichwohl wird in der praktischen Aufgabe
                der reinen Vernunft, d.i. der notwendigen Bearbeitung zum h&ouml;chsten
                Gute, ein solcher Zusammenhang als notwendig postuliert: wir
                sollen das h&ouml;chste Gut (welches also doch m&ouml;glich sein
                mu&szlig;) zu bef&ouml;rdern suchen. Also wird auch das Dasein
                einer von der Natur verschiedenen Ursache der gesamten Natur,
                welche den Grund dieses Zusammenhanges, n&auml;mlich der genauen &Uuml;bereinstimmung
                der Gl&uuml;ckseligkeit mit der Sittlichkeit, enthalte, postuliert.
                Diese oberste Ursache aber soll den Grund der &Uuml;bereinstimmung
                der Natur nicht blo&szlig; mit einem Gesetz des Willens der vern&uuml;nftigen
                Wesen, sondern mit der Vorstellung dieses Gesetzes, sofern diese
                es ich zum obersten Bestimmungsgrunde des Willens setzen, als
                nicht blo&szlig; mit den Sitten der Form nach, sondern auch ihrer
                Sittlichkeit, als dem Beweggrunde derselben, d.i. mit ihrer moralischen
                Gesinnung, enthalten. Also ist das h&ouml;chste Gut in der Welt
                nur m&ouml;glich, so fern eine oberste Ursache der Welt angenommen
                wird, die eine der moralischen Gesinnung gem&auml;&szlig;e Kausalit&auml;t
                hat. Nun ist ein Wesen, das der Handlungen nach der Vorstellung
                von Gesetzen f&auml;hig ist, eine Intelligenz (vern&uuml;nftig
                Wesen) und die Kausalit&auml;t eines solchen Wesens nach dieser
                Vorstellung der Gesetze ein Wille desselben. Also ist die oberste
                Ursache der Natur, sofern sie zum h&ouml;chsten Gut vorausgesetzt
                werden mu&szlig;, ein Wesen, das durch Verstand und Wille die
                Ursache (folglich der Urheber) der Natur ist, d.i. Gott. Folglich
                ist das Postulat der h&ouml;chsten M&ouml;glichkeit des h&ouml;chsten
                abgeleiteten Guts (der besten Welt) zugleich das Postulat der
                Wirklichkeit eines h&ouml;chsten urspr&uuml;nglichen Guts, n&auml;mlich
                der Existenz Gottes. Nun war es Pflicht f&uuml;r uns, das h&ouml;chste
                Gut zu bef&ouml;rdern, mithin nicht allein Befugnis, sondern
                auch der der Pflicht als Bed&uuml;rfnis verbundene Notwendigkeit,
                die M&ouml;glichkeit dieses h&ouml;chsten Guts vorauszusetzen,
                welche, da es nur unter der Bedingung des Daseins Gottes stattfindet,
                die Voraussetzung desselben mit der Pflicht unzertrennlich verbindet,
                d.i. es ist moralisch notwendig, das Dasein Gottes anzunehmen.&#8220;<br>
                KpV 225f<br>
                <br>
                Eckhard Bieger
                </font><br>&copy;
                <a href="http://www.kath.de"> <font face="Arial, Helvetica, sans-serif">www.kath.de</font></a></p>
            </TD>
          <TD background=boxright.gif><IMG height=8 alt="" 
            src="boxright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=8 alt="" src="boxbottomleft.gif" 
            width=8></TD>
          <TD background=boxbottom.gif><IMG height=8 alt="" 
            src="boxbottom.gif" width=8></TD>
          <TD><IMG height=8 alt="" src="boxbottomright.gif" 
            width=8></TD></TR></TBODY></TABLE></TD><td style='vertical-align:top;'>
 <script type="text/javascript"><!--
        google_ad_client = "pub-9537752113242914";
        google_ad_width = 336;
        google_ad_height = 280;
        google_ad_format = "336x280_as";
        google_ad_type = "text_image";
        //2006-10-23: Lexikon Symbole
        google_ad_channel = "8495253357";
        //--></script>
        <script type="text/javascript"
          src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
        </script>
</td>
</TR>
  <TR>
    <TD vAlign=top align=left>&nbsp; </TD>
  </TR></TBODY></TABLE></BODY></HTML>
