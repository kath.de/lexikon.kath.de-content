<HTML><HEAD><TITLE>Auferstehung Jesu</TITLE>
<META http-equiv=Content-Type content="text/html; charset=iso-8859-1"><LINK 
title=fonts href="kaltefleiter.css" type=text/css 
rel=stylesheet>
<META content="MSHTML 5.50.4134.600" name=GENERATOR></HEAD>
<BODY bgColor=#ffffff leftMargin=6 topMargin=6 marginheight="6" marginwidth="6">
<TABLE cellSpacing=0 cellPadding=6 width="100%" border=0>
  <TBODY>
  <TR>
    <TD vAlign=top align=left width=100> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b>Philosophie&amp;Theologie</b></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td> <?php include("logo.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10"> <?php include("az.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </TD>
    <TD vAlign=top rowSpan=2>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
        <TBODY>
        <TR vAlign=top align=left>
          <TD width=8><IMG height=8 alt="" 
            src="boxtopleftcorner.gif" width=8></TD>
          <TD background=boxtop.gif><IMG height=8 alt="" 
            src="boxtop.gif" width=8></TD>
          <TD width=8><IMG height=8 alt="" 
            src="boxtoprightcorner.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxtopleft.gif><IMG height=8 alt="" 
            src="boxtopleft.gif" width=8></TD>
          <TD bgColor=#e2e2e2> 
            <H1><font face="Arial, Helvetica, sans-serif">Auferstehung Jesu</font></H1>
          </TD>
          <TD background=boxtopright.gif><IMG height=8 
            alt="" src="boxtopright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=13 alt="" src="boxdividerleft.gif" 
            width=8></TD>
          <TD background=boxdivider.gif><IMG height=13 
            alt="" src="boxdivider.gif" width=8></TD>
          <TD><IMG height=13 alt="" 
            src="boxdividerright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxleft.gif><IMG height=8 alt="" 
            src="boxleft.gif" width=8></TD>
          <TD class=L12>
            <P><STRONG><font face="Arial, Helvetica, sans-serif">Der historische
                  Hintergrund des christlichen Auferstehungsglaubens</font></STRONG></P>
            <P><font face="Arial, Helvetica, sans-serif">Als Jesus am Kreuz hingerichtet
                worden war, hatten seine Anh&auml;nger
              keine Erwartungen mehr. Sie waren deprimiert, ihrer Hoffnungen
              beraubt und verlie&szlig;en Jerusalem. Nach einiger Zeit erkl&auml;rten
              sie in &ouml;ffentlichen Predigten, da&szlig; Jesus von Gott aus
              dem Tod auferweckt und in den Himmel erh&ouml;ht worden sei. Wie
              ist dieser Umschwung erkl&auml;rbar?</font></P>
            <P><font face="Arial, Helvetica, sans-serif"> <strong>Erkl&auml;rungen, die zu
                kurz reichen<br>
            </strong>Die &auml;lteste Erkl&auml;rung findet sich beim Evangelisten
              Matth&auml;us. Im 27. Kapitel seines Evangeliums wird folgende
              Begebenheit nach der Grablegung Jesu berichtet:<br>
&#8222;
              Am n&auml;chsten Tag gingen die Hohenpriester und Pharis&auml;er
              gemeinsam zu Pilatus; es war der Tag nach dem R&uuml;sttag. Sie
              sagten: Herr, es fiel uns ein, da&szlig; dieser Betr&uuml;ger,
              als er noch lebte, behauptet hat: Ich werde nach drei Tagen auferstehen.
              Gib also den Befehl, da&szlig; das Grab bis zum dritten Tag sicher
              bewacht wird. Sonst k&ouml;nnten seine J&uuml;nger kommen, ihn
              stehlen und dem Volk sagen: Er ist von den Toten auferstanden.
              Und dieser letzte Betrug w&auml;re noch schlimmer als alles zuvor.
              Pilatus antwortete ihnen: Ihr sollt eine Wache haben. Geht und
              sichert das Grab, so gut ihr k&ouml;nnt. Darauf gingen sie, um
              das Grab zu sichern. Sie versiegelten den Eingang und lie&szlig;en
              die Wache dort.&#8220; Kap. 27, 62-66<br>
              Nachdem Jesus sich am Ostermorgen den Frauen zu erkennen gegeben
              hatte und die W&auml;chter Zeugen der Auferstehung geworden waren, &#8222;kamen
              einige (der W&auml;chter) in die Stadt und berichteten den Hohenpriestern
              alles, was geschehen war. Diese fa&szlig;ten gemeinsam mit den &Auml;ltesten
              den Beschlu&szlig;, die Soldaten zu bestechen. Sie gaben ihnen
              viel Geld und sagten: Erz&auml;hlt den Leuten: Seine J&uuml;nger
              sind bei Nacht gekommen und haben ihn gestohlen, w&auml;hrend wir
              schliefen. Falls der Statthalter davon h&ouml;rt, werden wir ihn
              beschwichtigen und daf&uuml;r sorgen, da&szlig; ihr nichts zu bef&uuml;rchten
              habt. Die Soldaten nahmen das Geld und machten alles so, wie man
              es ihnen gesagt hatte. So kommt es, da&szlig; das Ger&uuml;cht
              bis heute verbreitet ist.&#8220; Matth&auml;us 28,11-15<br>
              Die Bef&uuml;rchtung der j&uuml;dischen Obrigkeit, die J&uuml;nger
              w&uuml;rden unmittelbar nach der Kreuzigung behaupten, Jesus lebe,
              war unbegr&uuml;ndet. Die Evangelien berichten dann auch, da&szlig; die
              J&uuml;nger voller Angst in der Stadt bleiben und nur die Frauen
              zum Grab gehen, um den Leichnam endg&uuml;ltig zu versorgen, der
              am Abend des Karfreitages schnell begraben werden mu&szlig;te,
              denn die Sabbatruhe, die mit der D&auml;mmerung begann, mu&szlig;te
              beachtet werden. Eine Absicht der J&uuml;nger, den Leichnam zu
              stehlen, war nicht zu erkennen. <br>
              Eine andere Version wird im Islam erz&auml;hlt: Jesus sei nicht
              am Kreuz gestorben, sondern wieder losgekommen, w&auml;re mit Maria
              von Magdala nach Persien gegangen und sei dort gestorben. Eine
              andere Version wird auch schon fr&uuml;h von den Gnostikern  verbreitet:
              Jesus, der nicht wirklich Mensch war, sondern nur in einem Scheinleib
              sich zeigte, verlie&szlig;t die Erde vor
              der Kreuzigung, gekreuzigt wurde an seiner Stelle Simon von Cyrene.<br>
              <br>
              <strong>              Der Innere Proze&szlig; der J&uuml;nger</strong><br>
            </font><font face="Arial, Helvetica, sans-serif">Die
              Bibel berichtet davon, da&szlig; die Anh&auml;nger Jesu nur
                langsam zum Glauben an die Auferstehung gekommen sind, so da&szlig; es
                historisch unwahrscheinlich ist, da&szlig; sie selbst die Idee
                entwickelt h&auml;tten, Jesus sei von den Toten auferstanden. Mit
                seinem Tod waren ihre Hoffnungen, Jesus w&uuml;rde das messianische
                Reich aufrichten, zerbrochen. F&uuml;r einen Juden war es besonders
                schlimm, da&szlig; Jesus nicht nur von der j&uuml;dischen Obrigkeit
                verurteilt wurde, sondern da&szlig; er an die R&ouml;mer, d.h.
                Heiden, ausgeliefert worden war. Gott mu&szlig;te seinen Gesalbten,
                den sie als den <a href="messias_christus.php">Messias</a>  erkannt hatten,
                verlassen haben. Sie hatten sich Jesus deshalb angeschlossen,
                weil sie durch
                seine Predigt und seine Wundertaten &uuml;berzeugt wurden, da&szlig; er
                der Gesandte Gottes sein mu&szlig;te. Gott konnte seinen Gesandten
                nicht so schm&auml;hlich enden lassen.<br>
                Wenn sie von sich aus nicht in der Lage waren, das Scheitern
                des Messias in eine Frohe Botschaft umzuinterpretieren, dann
                m&uuml;ssen
                sie von au&szlig;en zu der Vorstellung gebracht worden sein, da&szlig; Jesus
                nicht bei den Toten geblieben ist, sondern lebt. Es war dann auch
                die Botschaft von der Auferstehung und nicht der Bericht &uuml;ber
                die Hinrichtung am Kreuz, die Ausgangspunkt des &#8222;Neuen Weges&#8220; wurde,
                den die Anh&auml;nger Jesu innerhalb des Judentums gehen wollten,
                ehe es zur Trennung von Christen und Juden kam. Der Sonntag als
                Feiertag innerhalb der Woche hat von Anfang an die Auferstehung
                zum Inhalt, <a href="http://www.kath.de/Kirchenjahr/ostern_ewiges-leben.php">Ostern</a>  ist
                das zentrale Fest der christlichen Kirche, das von Anfang an
                gefeiert wurde, w&auml;hrend unser Weihnachtsfest erst im 4. Jahrhundert
                entstand.<br>
&#8222;
                Wenn aber verk&uuml;ndet wird, da&szlig; Christus von den Toten
                auferweckt worden ist, wie k&ouml;nnen dann einige von euch sagen:
                eine Auferstehung der Toten gibt es nicht. Wenn es keine Auferstehung
                der Toten gibt, ist auch Christus nicht auferweckt worden. Ist
                aber Christus nicht auferweckt worden, dann ist unsere Verk&uuml;ndigung
                leer und euer Glaube sinnlos. .... Wenn wir unsere Hoffnung nur
                in diesem Leben auf Christus gesetzt haben, sind wir erb&auml;rmlicher
                daran als alle anderen Menschen. Nun aber ist Christus von den
                Toten auferweckt worden als der Erste der Entschlafenen.&#8220; schreibt
                Paulus im 1. Korintherbrief 15,12-14, 19-20<br>
                Dieser Brief ist &auml;lter als die Evangelien, er ist zwischen
                53 und 55 geschrieben, also 20 Jahre nach der Kreuzigung Jesu.
                In ihm findet sich der &auml;lteste schriftliche Bericht &uuml;ber
                die Auferstehung. Paulus fa&szlig;t die Gute Botschaft, das Evangelium,
                in einem fr&uuml;hen Credo zusammen:<br>
&#8222;
                Christus ist f&uuml;r unsere S&uuml;nden gestorben, gem&auml;&szlig; der
                Schrift,<br>
                und ist begraben worden.                Er ist am dritten Tag
                auferweckt worden, gem&auml;&szlig; der
                Schrift.                Und er erschien dem Kephas, dann den Zw&ouml;lf. Danach erschien
                er mehr als f&uuml;nfhundert Br&uuml;dern zugleich, die meisten
                von ihnen sind noch am Leben, einige sind entschlafen. Danach
                erschien er dem Jakobus, dann allen Aposteln. Als letztem von
                allen erschien
                er mir, dem Unerwarteten, der 'Mi&szlig;geburt&#8216;. Denn
                ich bin der Geringste von den Aposteln; ich bin nicht wert, Apostel
                genannt zu werden, weil ich die Kirche Gottes verfolgt habe. Doch
                durch die Gnade Gottes bin ich, was ich bin.&#8220; Kap 15,3-10<br>
                Die von Paulus aufgez&auml;hlte Liste derjenigen, die eine Auferstehungserfahrung
                gemacht haben, umfa&szlig;t mehr Namen als in den Auferstehungsberichten
                der Evangelien zu finden sind.<br>
              </font></P>
            <P><font face="Arial, Helvetica, sans-serif"><strong>Die &Uuml;berzeugung,
                  da&szlig; Jesus lebt</strong><br>
                  Der Bericht des Paulus wie auch die der Evangelisten
                zeigen, da&szlig; Jesus
              sich seinen Anh&auml;ngern gezeigt hat und sie durch diese Erscheinungen
              zur &Uuml;berzeugung kamen, da&szlig; er nicht im Tod geblieben,
              sondern auferweckt worden ist, da&szlig; er lebt.<br>
              Um die in den Berichten beschriebenen Ph&auml;nomene einordnen
              zu k&ouml;nnen, mu&szlig; im Auge behalten werden, da&szlig; Jesus
              mit der Auferstehung nicht wie der aus dem Grab gerufene Lazarus
              in dieses Leben und damit in die menschliche Geschichte zur&uuml;ckgekehrt
              ist. Jesus bleibt im Sinne der menschlichen Existenz tot. Die Auferstehung
              bezieht sich auf eine andere Wirklichkeit, Jesus ist mit seinem
              Leib (und seiner Seele) in eine himmlische Existenz hin&uuml;ber
              gegangen. Er bleibt dadurch <a href="gegenwart_jesu.php">gegenw&auml;rtig</a>,
              nicht nur im Ged&auml;chtnis
              seiner Anh&auml;nger oder so
              wie Mozart gegenw&auml;rtig bleibt, wenn seine Kompositionen erklingen.
              Aus dem neuen Leben kann Jesus sich seinen Anh&auml;ngern zeigen,
              so wie er Paulus auf der Stra&szlig;e von Jerusalem nach Damaskus
              begegnet ist. Den Christen ist verhei&szlig;en, da&szlig; sie wie
              Jesus auch in einer himmlische Existenz, mit Leib und Seele, aufgenommen
              werden. Dieser Glaube auf einen Leben nach dem Tod bezieht ausdr&uuml;cklich
              die Existenz des Leibes ein.<br>
            </font></P>
            <P><font face="Arial, Helvetica, sans-serif"><strong>Zitate</strong></font></P>
            <P><font face="Arial, Helvetica, sans-serif">Von den Begegnungen
                Jesu mit seinen J&uuml;ngern gibt es verschiedene
              Berichte. In der Apostelgeschichte wird &uuml;ber Paulus berichtet:<br>
              Saulus w&uuml;tete immer noch mit Drohung und Mord gegen die J&uuml;nger
              des Herrn. Er ging zum Hohenpriester und erbat von ihm Briefe an
              die Synagogen in Damaskus, um die Anh&auml;nger des Neues Weges,
              M&auml;nner und Frauen, die er dort finde, zu fesseln und nach
              Jerusalem zu bringen. Unterwegs aber, als er sich bereits Damaskus
              n&auml;herte, geschah es, da&szlig; ihn pl&ouml;tzlich ein Licht
              vom Himmel umstrahlte. Er st&uuml;rzte zu Boden und h&ouml;rte,
              wie eine Stimme zu ihm sagte: Saul, Saul, warum verfolgst du mich?
              Er antwortete: Wer bist du Herr? Dieser sagte: Ich bin Jesus, den
              du verfolgst. Steh auf und geh in die Stadt; dort wird dir gesagt
              werden, was du tun sollst. Seine Begleiter standen sprachlos da;
              sie h&ouml;rten zwar die Stimme, sahen aber niemand.&#8220; Kap.
              9,1-7<br>
              Offensichtlich hatte Saulus eine Vision. In dem Bericht ist zu
              lesen, da&szlig; er die Augen geschlossen hatte und danach nicht
              mehr sehen konnte. In Damaskus wird er von einem Mann namens Hananias
              besucht und kann wieder sehen.<br>
              Der Autor der Apostelgeschichte, Lukas, berichtet in seinem Evangelium
              von zwei J&uuml;ngern, die Jerusalem entt&auml;uscht in Richtung
              Emmaus verlassen. Sie erkennen Jesus nicht, der sich ihnen zugesellt
              und ihnen an Schriftstellen des Alten Testaments aufzeigt, da&szlig; der &#8222;Messias
              all das erleiden mu&szlig;te, um so in seine Herrlichkeit zu gelangen.&#8220; Die
              beiden bitten ihn, mit in das Gasthaus zu gehen. &#8222;und als
              er mit ihnen bei Tisch war, nahm er das Brot, sprach den Lobpreis,
              brach das Brot und gab es ihnen. Da gingen ihnen die Augen auf
              und sie erkannten ihn; dann sahen sie ihn nicht mehr.&#8220; Kap.
              24, 30-31<br>
              Mehrere andere Berichte von einer Begegnung mit Jesus finden ebenfalls
              bei einem Mahl statt. Lukas 24,36-49 Johannes 20,19-23, dann der
              Bericht &uuml;ber den ungl&auml;ubigen Thomas 20,24-29 und das
              Mahl am See Genesareth 21,1-14</font></P>
            <p><font face="Arial, Helvetica, sans-serif">Paul Tillich zeigt,
                wie die Auferstehung nicht nur ein geschichtliches Ereignis ist,
                sondern ein Neues Sein herbeif&uuml;hrt. <br>
              Als Petrus Jesus &#8222;den Christus&#8220; nannte, erwartete er
              das Kommen eines neuen Standes der Dinge durch ihn. Diese Erwartung
              liegt im Titel &#8222;Christus&#8220; beschlossen. Aber das, was
              die J&uuml;nger erwartet hatten, erf&uuml;llte sich nicht. Der
              Stand der Dinge &#8211; in der Natur wie in der Geschichte &#8211;blieb
              unver&auml;ndert, und der, von dem man geglaubt hatte, da&szlig; er
              den neuen &Auml;on bringen w&uuml;rde, wurde durch die M&auml;chte
              des alten &Auml;ons zerbrochen. Das bedeutete f&uuml;r die J&uuml;nger,
              da&szlig; sie entweder den Zusammenbruch ihrer Hoffnung hinnehmen
              oder deren Inhalt radikal verwandeln mu&szlig;ten. Sie w&auml;hlten
              den zweite Weg und setzten das Neue Sein mit dem Sein Jesu als
              des Gekreuzigten gleich. Die synoptischen Bereichte (d.h. die Evangelien
              von Matth&auml;us, Markus und Lukas) wollen zeigen, da&szlig; Jesus
              selbst darin vorangegangne war und seinen messianischen Anspruch
              mit der Voraussicht seines gewaltsamen Todes vereinigt hatte. Die
              gleichen Berichte zeigen, da&szlig; die J&uuml;nger zun&auml;chst
              dieser Verbindung Widerstand leisteten. Erst die Erfahrungen, die
              als Ostern und Pfingsten beschrieben werden, haben ihren Glauben
              an den paradoxen Charakter des messianischen Anspruchs geschaffen,
              und es war Paulus, der den theologischen Rahmen gab, innerhalb
              dessen das Paradox verstanden und gerechtfertigt werden konnte.
              Einer der Wege zur L&ouml;sung des Problems war die Unterscheidung
              zwischen dem ersten und dem zweiten Kommen des Christus. Der neue
              Stand der Dinge wird mit dem zweiten Kommen, n&auml;mlich der Wiederkunft
              Christi in Herrlichkeit, geschaffen werden. In der Periode zwischen
              dem ersten und dem zweiten Kommen ist das Neue Sein nur in ihm
              gegenw&auml;rtig, nicht in der Welt.<br>
              Systematische Theologie 1957, S. 129</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Wolfhart Pannenberg &uuml;ber die Bedeutung der Auferstehung f&uuml;r
              Jesus uns seine J&uuml;nger:<br>
              Die Erwartung des irdischen Jesus richtete sich &#8230; aller Wahrscheinlichkeit
              nach nicht auf eine nur ihm sozusagen privat widerfahrende Auferweckung
              der Toten, sondern auf die nahe bevorstehende allgemeine Totenauferweckung,
              die nat&uuml;rlich auch ihm selbst, falls er zuvor sterben sollte,
              widerfahren w&auml;re. Als den J&uuml;ngern Jesu dann der Auferstandene
              begegnete, da haben sie das zweifellos ebenfalls als den Beginn
              der Endereignisse verstanden. &#8230;.<br>
              Die endg&uuml;ltige g&ouml;ttliche Best&auml;tigung Jesu wird erst
              durch das Geschehen seiner Wiederkunft erfolgen. Dann erst wird
              die Offenbarung Gottes in Jesus in ihrer endg&uuml;ltigen, unwiderstehlichen
              Herrlichkeit sichtbar werden. <br>
              Grundz&uuml;ge der Christologie, 1964 S. 61 und 105</font></p>
            <p><font size="2" face="Arial, Helvetica, sans-serif">Text: Eckhard
                Bieger S.J.</font><font face="Arial, Helvetica, sans-serif"><br>
            </font>&copy;<a href="http://www.kath.de"> <font face="Arial, Helvetica, sans-serif">www.kath.de</font></a></p>
            </TD>
          <TD background=boxright.gif><IMG height=8 alt="" 
            src="boxright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=8 alt="" src="boxbottomleft.gif" 
            width=8></TD>
          <TD background=boxbottom.gif><IMG height=8 alt="" 
            src="boxbottom.gif" width=8></TD>
          <TD><IMG height=8 alt="" src="boxbottomright.gif" 
            width=8></TD></TR></TBODY></TABLE></TD><td style='vertical-align:top;'>
 <script type="text/javascript"><!--
        google_ad_client = "pub-9537752113242914";
        google_ad_width = 336;
        google_ad_height = 280;
        google_ad_format = "336x280_as";
        google_ad_type = "text_image";
        //2006-10-23: Lexikon Symbole
        google_ad_channel = "8495253357";
        //--></script>
        <script type="text/javascript"
          src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
        </script>
</td>
</TR>
  <TR>
    <TD vAlign=top align=left>&nbsp; </TD>
  </TR></TBODY></TABLE></BODY></HTML>
