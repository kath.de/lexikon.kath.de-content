---
title: Monotheletismus oder Monergetismus
author: 
tags: 
created_at: 
images: []
---


# **Hatte Jesus einen menschlichen Willen?*
# **Wenn Jesus wahrer Gott und wahrer Mensch ist und Menschheit und Gottheit in ihm in einer Person verbunden sind, dann entsteht die Frage, wer die innere Glaubenshaltung und das Handeln dieser Person bestimmt. Unzweifelhaft ist, daß der Sohn Gottes bestimmend sein muß, denn der Sohn Gottes hat die menschliche Natur angenommen. Um keinen Zweifel aufkommen zu lassen, daß die menschliche Natur ganz im Dienst des Heilswerkes steht, das Gott durch seinen in Ewigkeit gezeugten Sohn vollbringen will, neigten griechische Theologen dazu, Jesus nur einen Willen zuzusprechen (mono – eins, Thelema – Willen). Im Griechischen kann man auch von Energeia sprechen. Das bezeichnet nicht das, was wir in unserem Sprachgebrauch mit „Energie“ ausdrücken, sondern meint "Wirkkraft". **
 Die Frage nach dem Willen in Jesus wurde im 7. Jahrhundert heftig diskutiert, so daß der Kaiser verbot, überhaupt über die Frage den theologishcen Disput auszutragen. Severos von Antiochien, Johannes Philoponos u.a. Gegner des Konzils von ***[Chalcedon](christologische_streitigkeiten.php)              vertraten die Position, daß in Jesus Christus nur ein Wille möglich ist. Sie wollten damit jedem Mißverständnis zuvorkommen, daß in Jesus zwei Subjekte handeln.  

# **Wahrer Mensch ohne menschlichen Willen?****
 Wie kann man aber davon sprechen, daß der Sohn Gottes wirklich Mensch geworden ist, wenn er keinen menschlichen Willen hat? Mußte Gott die menschliche Natur sozusagen in ihrem Kern ausschließen, damit sein Heilswerk gelingen konnte? Bei der Ankündigung der Geburt des Erlösers legt der biblische Bericht gerade auf die freie Zustimmung Marias Wert. Offensichtlich will Gott die freie Zustimmung des Menschen zu seinem Heilswerk. Die Betonung des Glaubens als Grundlage der Beziehung des Menschen zu Gott zeigt die Bedeutung der Frage. Diese ist aber nicht einfach zu lösen, nämlich ob der Mensch Jesus auch einen eigenen Willen hat. Hätte er einen eigenen Willen, dann könnte theoretisch der Mensch Jesus etwas anderes wollen als der Sohn Gottes. Dann wären in Jesus Christus doch „zwei Söhne“, der Mensch und der ewige Sohn Gottes, die gegeneinander stehen. Die Alternative, daß in Jesus Christus nur ein Wille wirksam war, würde die Ganzheit der menschlichen Natur in Frage stellen. Der Mensch Jesus wäre nicht vollständig. Die Meditation des Weihnachtsgeheimnisses hatte sehr früh zu der Einsicht geführt, daß mit der Menschwerdung etwas für jeden Menschen geschehen sein muß. Wenn Gott menschliche Natur annimmt, dann ist die ganze menschliche Natur geheiligt, nicht nur das Kind in der Krippe, sondenr alle Menschen. Wie konnte man einen Ausweg aus der Problemstellung finden? Auf der einen Seite gibt es in den Evangelien keine Hinweise, daß der Mensch Jesus sich gegen den göttlichen Auftrag, seine göttliche Bestimmung aufgelehnt hätte, auf der anderen Seite wird Jesus als voll handlungsfähiger Mensch geschildert. ***

 Ein Hinweis, daß der menschliche Wille dem göttlichen Auftrag zustimmen mußte, findet sich in dem Gebet Jesu am Ölberg. Jesu weiß, daß er dem Unheil nicht entkommen wird und betet: „Vater, nicht mein Wille geschehe, sondern der Deine.“  

# **Die Person eint den göttlichen und den menschlichen Willen****
 Hält sich die theologische Begriffsbildung an den Berichten der Evangelien, wird sie Jesus nicht einen eigenen menschlichen Willen absprechen können. Die Lösung konnte nicht in der Durchsetzung einer Begrifflichkeit liegen, sondern in der Weiterentwicklung des Verständnisses von Jesus Christus. In der Lösung der Frage konnte man auf eine Überlegung des Papstes Leo I zurückgreifen, die bereits dem Konzil von Chalcedon für dessen Formulierungsarbeit vorlag. Die beiden Naturen in Jesus Christus stehen nicht einfach nebeneinander oder gar gegeneinander, sondern jede Natur wirkt ihr Eigenes im Zusammenspiel mit der anderen. Man kann sogar noch weitergehen und sagen, daß die Gottheit es der Menschheit Jesu ermöglicht, ***[noch mehr Mensch](menschwerdung_jesu.php)  zu werden.***

 Das VI. Ökumenische Konzil von Konstantinopel (680/681) schloß die Aussage vom Einen Willen in Jesus Christus als mögliche Beschreibung der Inkarnation aus dem Sprachgebrauch der Kirche aus.  

***Zitate 

# **Die Ablehung eines menshclichen Willens Jesu wird einem Kaiser behauptet:              **
 So folgen wir den heiligen Vätern in allem und auch in diesem Punkt und bekennen den einen Willen unseres Herrn Jesu Christi, der wahrhaft Gott ist, weil niemals das geistig beseelte Fleisch vom Logos getrennt, aus eigenem Antrieb und gegen das mit ihm hypostatisch geeinte göttliche Wort seine eigene natürliche Bewegung hervorgebracht hat …***

 Edikt des Kaisers Heraklius, 638 

# ** 3. Konzil von Konstantinopel 680/81:  Die zwei Willen in Jesus:**
 Ebenso verkünden wir gemäß der Lehre der heiligen Väter, daß sowohl zwei natürliche Weisen des Wollens bzw. Willen als auch zwei natürliche Tätigkeiten ungetrennt, unveränderlich, unteilbar und unvermischt in ihm sind; und die zwei natürlichen Willen sind einander nicht entgegengesetzt - das sei ferne! -, wie die ruchlosen Häretiker behaupteten; vielmehr ist sein menschlicher Wille folgsam und widerstrebt und widersetzt sich nicht, sondern ordnet sich seinem göttlichen und allmächtigen Willen unter; denn der Wille des Fleisches mußte sich regen, sich aber nach dem allweisen Athanasius dem göttlichen Willen unterordnen; denn wie sein Fleisch Fleisch des Wortes Gottes genannt wird und ist, so wird auch der natürliche Wille seines Fleisches als dem Wort Gottes eigen bezeichnet und ist es, wie er selbst sagt: „Denn ich bin herabgestiegen aus dem Himmel, nicht um meinen eigenen Willen zu tun, sondern den Willen des Vaters, der mich gesandt hat" (Johannesevangelium 6,38); dabei nannte er den Willen des Fleisches seinen eigenen Willen, da auch das Fleisch ihm eigen geworden ist; denn wie sein ganzheiliges und makelloses beseeltes Fleisch trotz seiner Vergöttlichung nicht aufgehoben wurde, sondern in der ihm eigenen Abgrenzung und dem ihm eigenen Begriff verblieb, so wurde auch sein menschlicher Wille trotz seiner Vergöttlichung nicht aufgehoben, sondern ist vielmehr gewahrt, wie der Gottesgelehrte Gregor sagt: „Denn sein Wollen, verstanden in Bezug auf den Erlöser, ist Gott nicht entgegengesetzt, da es ganz vergöttlicht ist".***

# **Eckhard Bieger**
©***[ ***www.kath.de](http://www.kath.de)
