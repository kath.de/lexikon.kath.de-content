---
title: Zweinaturenlehre
author: 
tags: 
created_at: 
images: []
---


# **Wie kann man Jesus als den Sohn Gottes verstehen?*
***Als das Christentum in die griechische Kultur gelangte, stieß es auf eine entwickelte Philosophie, die begriffliche Klarheit verlangte. Die Christen mußten Antwort geben, wer dieser Jesus von Nazareth ist. Er ist Heilbringer. Das war in der griechischen Welt nichts Besonderes. Viele Prediger sind aufgetreten und haben sich als Heilbringer ausgegeben. Über solche Männer wurde auch erzählt, daß sie Wunder vollbringen würden oder vollbracht hätten. Von Jesus behaupteten die Christen, daß er Gottes Sohn und zugleich Mensch sei. Das Konzil von Nicäa hatte 325 festgestellt, daß er wirklich Gott ist, ungeschaffen, Gott gleich, der ***[Sohn Gottes](gottessohn.php). Wie sollte man das verstehen? 

# **Wenn dieser Jesus von Nazareth wirklich Gott ist, dann konnte er in den Augen der griechischen Welt nicht wirklich Mensch sein. Sie dachten von Jesus so, wie es aus griechischen Göttersagen überliefert, war, daß ein Gott auf der Erde erschienen ist. Seine Gestalt wäre nur eine Art Hülle, um sich den Menschen sichtbar zu machen, die jedoch nur als ***[Form der Erscheinung](christologische_streitigkeiten.php) zu verstehen war, nicht wirklich ein Mensch.**
 Im griechischen Denken lag noch eine andere Verstehensformel bereit. Der Philosoph Aristoteles hatte schon 300 Jahre vor den christlichen Predigern dem Menschen nicht nur eine Seele zugesprochen, sondern auch erklärt, wie die Seele mit dem Körper verbunden ist. Er hatte nicht die Vorstellung, die der heutigen Medizin zugrunde liegt, daß nämlich der Körper nach chemisch-biologischen Gesetzen funktioniert und irgendwo im Brustkorb oder Kopf die Seele in dem Körper haust. Aristoteles hatte eine Vorstellung entwickelt, die viel mehr mit unseren Erfahrungen übereinstimmt: Die Seele ist das organisierende Prinzip, das Lebensprinzip jedes Lebewesens. Auch Tiere haben eine Seele, die dem materiellen Substrat Leben gibt, so daß wir von einem Körper als belebter Materie sprechen können. Die menschliche Seele hat darüber hinaus noch eine besondere geistige Qualität, sie steht in unmittelbarer Beziehung zur Vernunft, die dem ganzen Kosmos zugrunde liegt. Der Mensch ist das Wesen, das Vernunft hat und über Sprache verfügt. Eine Möglichkeit, den Heilbringer Jesus zu erklären, liegt in diesem griechischen Modell: Da der menschliche Geist unmittelbar auf die Weltvernunft bezogen ist, könnte doch der Logos, die Weltvernunft an die Stelle der Seele treten. Jesus von Nazareth hätte dann keine menschliche Seele. Wie reagierten die christlichen Denker auf diese Vorstellung? 

# **Zuerst einmal stimmte die griechische Vorstellung von einem Scheinleib nicht mit den Berichten über Jesus überein. Wäre sein Leib nur eine Hülle, wäre der Tod Jesu nicht wirklich geschehen. Wenn das geistige organisierende Prinzip der göttliche Logos wäre, wären die menschlichen Reaktionen nicht verständlich, denn er wird als einer geschildert, der sich freut, der Trauer empfindet, der sogar in große Angst gerät, als er seine Verurteilung unausweichlich kommen sieht.**
 Ein gewichtiger theologischer Grund für die Annahme, daß bei Jesus nicht anstelle der menschlichen Seele der Logos das organisierende Prinzip des Körpers ist, sondern daß er eine menschliche Seele hatte, ist die Dimension der Erlösung. Denn mit der Menschwerdung des Sohnes Gottes wird die Menschennatur insgesamt in den Erlösungsprozeß einbezogen. Nachdem Jesus im Tod am Kreuz mit allen ungerecht Verurteilten solidarisch geworden und als Mensch mit Leib und Seele eine neue, himmlische Existenz gefunden hatte, ist er der erste der von den Toten ***[Auferstandenen](auferstehung_jesu.php). Mit ihm sind alle Menschen anfänglich aus dem Tod gerettet. Wie kann das für den griechischen Kulturraum sprachlich deutlich gemacht werden? 

# **Begriffliche Klärung****
 Wenn in Jesus nicht zwei Personen gemeint sind, sondern uns einer entgegentritt, dann muß die Zweiheit auf einer anderen Ebene liegen. Um die Zweiheit von Gottheit und Menschheit  zu unterscheiden, bezeichnen die Theologen des 5. und 6. Jahrhunderts mit dem Naturbegriff die göttliche und die menschliche Natur. (Physis ist das griechische Wort für Natur) Die ***[Monophysiten](monophysiten.php) sprechen von einer neuen Natur, die durch die Einigung von menschlicher und göttlicher Natur entsteht. Diese Bezeichnung ist jedoch nicht klar genug, denn der Begriff Natur - Physis bzeichnet gerade die Zweiheit in Jesus.  Der ***[Personbegriff](person.php)  ist eine bessere begriffliche Beschreibung,  denn er bezieht sich auf den einen Jesus von Nazareth, den menschgewordenen Sohn Gottes. Der Personbegriff bezeichnet den Einen, der Naturberiff die Zweiheit in Jesus.  

# **Zitate**
„Er war Gott gleich, hielt aber nicht daran fest, wie Gott zu sein, sondern er entäußerte sich und wurde wie ein Sklave und den Menschen gleich. Sein Leben war das eines Menschen, er erniedrigte sich und war gehorsam bis zum Tod am Kreuz.“ Paulus zitiert in seinem Brief an die Philipper 2,6-8 einen Hymnus aus der christlichen Liturgie. 

***"Und das Wort ist Fleisch geworden und hat unter uns gewohnt, und wir haben seine Herrlichkeit gesehen, die Herrlichkeit des einzigen Sohnes vom Vater, voll Gnade und Wahrheit." Johannesevangelium 1, 14 

# **"Es ist zu bekennen, daß die Weisheit selbst, das Wort, der Sohn Gottes einen menschlichen Leibe, eine Geist-Seele und eine sensitive Seele angenommen hat, d.h. den ganzen Adam … unseren ganzen alten Menschen außer der Sünde. … Wenn aber einer sagen würde, das Wort habe anstelle der menschlichen sensitiven Seele im Fleisch des Herrn verweilt, den schließt die katholische Kirche aus, ebenso auch die, die zwei Söhne in unserem Heiland bekennen, einen vor der Inkarnation und einen anderen nach der Annahme des Fleisches aus der Jungfrau." (Die sensitive Seele ist nach der damaligen Anthropologie der Sitz der Gefühle und Empfindungen, diese wird von der Geist-Seele unterschieden.)**
 Papst Damasus I. im Brief an Paulinus v. Antiochien 375 

# **Eckhard Bieger*** ***S.J.**
 ©***[ ***www.kath.de](http://www.kath.de) 
