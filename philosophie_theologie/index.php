<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Methodenkompetenz</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="../religioeses_leben/kaltefleiter.css" title="fonts">
</head>
<body bgcolor="#ffffff" leftmargin="6" topmargin="6" marginwidth="6" marginheight="6">
<table width="100%" border="0" cellspacing="0" cellpadding="6">
  <tr>
    <td width="100" align="left" valign="top"><table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left">
          <td width="8"><img src="../religioeses_leben/boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="../religioeses_leben/boxtop.gif"><img src="../religioeses_leben/boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="../religioeses_leben/boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="../religioeses_leben/boxtopleft.gif"><img src="../religioeses_leben/boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b>Philosophie&amp;Theologie</b></td>
          <td background="../religioeses_leben/boxtopright.gif"><img src="../religioeses_leben/boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="../religioeses_leben/boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="../religioeses_leben/boxdivider.gif"><img src="../religioeses_leben/boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="../religioeses_leben/boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="../religioeses_leben/boxleft.gif"><img src="../religioeses_leben/boxleft.gif" width="8" height="8" alt=""></td>
          <td>
            <?php include("logo.html"); ?>
          </td>
          <td background="../religioeses_leben/boxright.gif"><img src="../religioeses_leben/boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="../religioeses_leben/boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="../religioeses_leben/boxbottom.gif"><img src="../religioeses_leben/boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="../religioeses_leben/boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
    <td rowspan="2" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left">
          <td width="8"><img src="../religioeses_leben/boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td background="../religioeses_leben/boxtop.gif"><img src="../religioeses_leben/boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="../religioeses_leben/boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="../religioeses_leben/boxtopleft.gif"><img src="../religioeses_leben/boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2">
            <h1> <font face="Arial, Helvetica, sans-serif">Philosophie&amp;Theologie</font></h1>
          </td>
          <td background="../religioeses_leben/boxtopright.gif"><img src="../religioeses_leben/boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="../religioeses_leben/boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="../religioeses_leben/boxdivider.gif"><img src="../religioeses_leben/boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="../religioeses_leben/boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="../religioeses_leben/boxleft.gif"><img src="../religioeses_leben/boxleft.gif" width="8" height="8" alt=""></td>
          <td class="L12">
            <p class=MsoNormal> <font face="Arial, Helvetica, sans-serif"><strong>Religion
                und Argumentation</strong><br>
                <br>
              Die christliche wie schon die j&uuml;dische Religion vermitteln
              sich zuerst durch Erz&auml;hlungen, von Abraham, Isaak und Jakob,
              von der Befreiung des Volkes aus Israel aus &Auml;gypten und dem
              Zug durch die W&uuml;ste, von David und Salomo. Die christliche
              Religion orientiert sich ganz am Lebensweg Jesu, der in den Festen
              des <a href="http://www.kath.de/Kirchenjahr/">Kirchenjahres</a> pr&auml;sent gehalten wird, Geburt, Bescheidung,
              Darstellung im Tempel, Taufe im Jordan, Leiden und Auferstehung,
              Sendung des Geistes. F&uuml;r was braucht es dann Theologie und
              Philosophie in einem katholischen Portal? Es entstehen in der Rede &uuml;ber
              Jesus und die Erl&ouml;sung Fragen, die nicht mehr durch R&uuml;ckgriff
              auf Erz&auml;hlungen gekl&auml;rt werden k&ouml;nnen, sondern der
              Argumentation bed&uuml;rfen. Meist entstehen die Fragen aus den
              Entwicklungen, in die die Religionsgemeinschaft gef&uuml;hrt wird.
              Eine solche Frage ergab sich, als Nicht-Juden sich zum Christentum
              bekehrten und sich taufen lie&szlig;en. Waren sie auch den j&uuml;dischen
              Religionsgesetzen unterworfen? Dazu gab es keine Anweisung Jesu.
              Das 1.Konzil entschied, da&szlig; das nicht notwendig sei. Die
              Apostelgeschichte berichtet in Kapitel 15 von diesem ersten Konzil.
              Die Frage war damit nicht erledigt, denn Christen, die aus dem
              Judentum stammten, sahen das Gesetz weiterhin als notwendig, an,
              um das Heil zu erlangen. Deshalb war der Theologe Paulus gefordert,
              der entsprechend den Prinzipien der j&uuml;dischen Theologie argumentierte.
              In seinem R&ouml;merbrief bezieht er sich auf Abraham, der lebte,
              als das Gesetz noch nicht erlassen war. Paulus zeigt, da&szlig; der
              Glaube die zentrale Gr&ouml;&szlig;e ist und nicht das Gesetz,
              denn Abraham wurde gesegnet, weil er dem Anruf Gottes Glauben schenkte.
              Die christliche Theologie kennt auch die Schriftauslegung, die
              Exegese, als Weg, Fragen zu kl&auml;ren. In der Begegnung mit der
              griechischen Kultur, in die das Christentum schon fr&uuml;h getragen
              worden war, mu&szlig;te sie sich mit der Philosophie auseinandersetzen.
              Das hat sich als sehr fruchtbar erwiesen. Daher geh&ouml;rt, gerade
              in der katholischen Tradition, die Philosophie mit in das Studienprogramm
              des theologischen Nachwuchses. Nicht nur Platon und Aristoteles
              haben das theologische Denken befruchtet, sondern auch die neue
              Philosophie: Kant, Schelling, Hegel, Kierkegaard und Heidegger.
              Im Mittelalter waren die meisten der Philosophen wie Albert d.
              Gr., Thomas v. Aquin und Duns Scotus selbst Theologen. <br>
              In der christlichen Theologie geht es um viele Fragen, besonders
              wichtig sind aber die, die sich mit der <a href="erloeser.php">Erl&ouml;sung</a>, der von
              Gott geschenkten Gnade, der Gerechtmachung besch&auml;ftigen. Bereits
              Paulus hat sich intensiv mit diesen Fragen auseinandergesetzt,
              dann Augustinus und Luther. Die junge Kirche wurde in tiefgehende
              theologische und philosophische Auseinandersetzungen gest&uuml;rzt,
              denn sie mu&szlig;te im griechischen Kulturraum genauer erkl&auml;ren,
              wer Jesus war, was es bedeutet, da&szlig; er Gott und Mensch ist.
              Daraus entstanden Fragen, ob er eine eigene menschliche Seele und
              einen eigenen <a href="monotheletismus_monergetismus">menschlichen
              Willen</a> hatte. Das Ringen um diese Frage
              dauerte mehr als 300 Jahre und spaltet bis jetzt die orthodoxen
              Kirchen von der &auml;gyptischen und syrischen. Ergebnis dieser
              philosophischen und theologischen Diskussion ist der Personbegriff,
            Basis der europ&auml;ischen Kultur.</font></P>
			<p><font face="Arial, Helvetica, sans-serif">Weil die Theologie wie auch die
			    Philosophie durch Probleme in Gang gesetzt werden, sind die Artikel dieses
			    Lexikons von der Problemstellung her aufgebaut.
			  Oft zeigt sich, da&szlig; ein theologischer Begriff wie &#8222;<a href="person.php">Person</a>&#8220; oder &#8222;<a href="realpraesenz.php">Realpr&auml;senz</a>&#8220; Ergebnis
			  einer theologischen Diskussion sind und damit auf eine Problemstellung eine
			  Antwort geben. Damit unterscheidet sich das Lexikon von zwei anderen Modellen.
			  Denn es ist sehr gut m&ouml;glich, einen Beitrag mit der Erkl&auml;rung
			  des Begriffs zu beginnen. Ein anderer Weg, ist die Theologiegeschichte abzuschreiten
			  und zu zeigen, wie im Alten und Neuen Testament wie auch durch die Theologen
			  der Antike, des Mittelalters und der Neuzeit die Fragestellung bedacht wurde.
			  Da letzter Weg sehr langwierig ist und in die Ausbildungsg&auml;nge der
			  Theologie geh&ouml;rt, wird hier eine einfachere L&ouml;sung umgesetzt:
			  Im Anschlu&szlig; an die Problemdarstellung werden die Bibel und die Theologen
			  zitiert, so da&szlig; der Klang ihres Denkens direkter deutlich wird.</font></p>
			<p><font face="Arial, Helvetica, sans-serif">Dieses Lexikon wird sich nicht
			    so schnell abschlie&szlig;en lassen. Wer
			  mitarbeiten will, kann sich gerne an die kath.de Redaktion wenden.
		    <a href="mailto:redaktion@kath.de">redaktion@kath.de</a> </font></p>
			<p><font face="Arial, Helvetica, sans-serif">&copy; www.kath.de</font><br>
	        </p>
          <td background="../religioeses_leben/boxright.gif"><img src="../religioeses_leben/boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="../religioeses_leben/boxbottomleft.gif" width="8" height="8" alt=""></td>
         <td background="../religioeses_leben/boxbottom.gif"><img src="../religioeses_leben/boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="../religioeses_leben/boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
    
                <td valign="top" align=Right>
                <p><a href="http://www.neufeld-verlag.de/" target="_blank"><img src="http://www.kath.de/lexikon/b_hinweise/neufeld.jpg" width="286" height="238" border="0"></a><br>
                 <script type="text/javascript">

<script type="text/javascript"><!--
        google_ad_client = "pub-9537752113242914";
        google_ad_width = 336;
        google_ad_height = 280;
        google_ad_format = "336x280_as";
        google_ad_type = "text_image";
        //2006-10-23: Lexikon Symbole
        google_ad_channel = "8495253357";
        //--></script>
        <script type="text/javascript"
          src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
        </script>
</td>
  </tr>
  <tr>
    <td align="left" valign="top"><table width="216" border="0" cellpadding="0" cellspacing="0">
       <tr valign="top" align="left">
          <td width="8"><img src="../religioeses_leben/boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="../religioeses_leben/boxtop.gif"><img src="../religioeses_leben/boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="../religioeses_leben/boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="../religioeses_leben/boxtopleft.gif"><img src="../religioeses_leben/boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="../religioeses_leben/boxtopright.gif"><img src="../religioeses_leben/boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="../religioeses_leben/boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="../religioeses_leben/boxdivider.gif"><img src="../religioeses_leben/boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="../religioeses_leben/boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="../religioeses_leben/boxleft.gif"><img src="../religioeses_leben/boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10">
            <?php include("az.html"); ?>
          </td>
          <td background="../religioeses_leben/boxright.gif"><img src="../religioeses_leben/boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="../religioeses_leben/boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="../religioeses_leben/boxbottom.gif"><img src="../religioeses_leben/boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="../religioeses_leben/boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</body>
</html>