<HTML><HEAD><TITLE>Menschensohn</TITLE>
<META http-equiv=Content-Type content="text/html; charset=iso-8859-1"><LINK 
title=fonts href="kaltefleiter.css" type=text/css 
rel=stylesheet>
<META content="MSHTML 5.50.4134.600" name=GENERATOR></HEAD>
<BODY bgColor=#ffffff leftMargin=6 topMargin=6 marginheight="6" marginwidth="6">
<TABLE cellSpacing=0 cellPadding=6 width="100%" border=0>
  <TBODY>
  <TR>
    <TD vAlign=top align=left width=100> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b>Philosophie&amp;Theologie</b></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td> <?php include("logo.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10"> <?php include("az.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </TD>
    <TD vAlign=top rowSpan=2>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
        <TBODY>
        <TR vAlign=top align=left>
          <TD width=8><IMG height=8 alt="" 
            src="boxtopleftcorner.gif" width=8></TD>
          <TD background=boxtop.gif><IMG height=8 alt="" 
            src="boxtop.gif" width=8></TD>
          <TD width=8><IMG height=8 alt="" 
            src="boxtoprightcorner.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxtopleft.gif><IMG height=8 alt="" 
            src="boxtopleft.gif" width=8></TD>
          <TD bgColor=#e2e2e2> 
            <H1><font face="Arial, Helvetica, sans-serif">Menschensohn</font></H1>
          </TD>
          <TD background=boxtopright.gif><IMG height=8 
            alt="" src="boxtopright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=13 alt="" src="boxdividerleft.gif" 
            width=8></TD>
          <TD background=boxdivider.gif><IMG height=13 
            alt="" src="boxdivider.gif" width=8></TD>
          <TD><IMG height=13 alt="" 
            src="boxdividerright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxleft.gif><IMG height=8 alt="" 
            src="boxleft.gif" width=8></TD>
          <TD class=L12>
            <P><STRONG><font face="Arial, Helvetica, sans-serif">Ben Adam - Der
                  Menschensohn, der am Ende der Zeiten wiederkehren wird<br>
                  </font></STRONG><font face="Arial, Helvetica, sans-serif"><br>
                  Jesus hat das Reich Gottes verk&uuml;ndet. Dieses Reich wird
                  endg&uuml;ltig den Frieden beinhalten. Es wird von Gott geschaffen
                  und ist damit frei von den menschlichen Unzul&auml;nglichkeiten.
                  Durch seine Predigt und die Wundertaten hat Jesus seine J&uuml;nger &uuml;berzeugt,
                  da&szlig; das Reich mit ihm gekommen ist. Sie sahen in Jesus
                  den <a href="messias_christus.php">Messias</a>, den Gesalbten,
                  Christus. Es gibt im Neuen Testament danbeben noch den Titel
                  &quot;Menschensohn&quot; f&uuml;r Jesus. In den Gleichnissen vom Samenkorn
                  und Sauerteig weist er darauf hin,
                  da&szlig; dieses
                  Reich im Verborgenen w&auml;chst. Irgendwann mu&szlig; es aber
                  zum Durchbruch kommen. Die Offenbarung des Johannes best&auml;tigt
                  diese Erwartung. Dieser Text und andere Stellen des Neuen Testaments
                  sprechen von dem Menschensohn, der am Ende der Zeiten das Reich
                  Gottes durchsetzen wird &#8211; mit dem die Geschichte abschlie&szlig;enden
                  Gericht. Wer ist dieser Menschensohn und wird es Jesus Christus
                sein? </font> </P>
            <P><font face="Arial, Helvetica, sans-serif"><strong>Endzeiterwartung der ersten Christen</strong><br>
              Die Christen der ersten Generation erwarteten noch zu ihren Lebzeiten
              die Wiederkunft Christi und damit die Vollendung der Geschichte.
              Paulus. Im 1. Korintherbrief schreibt er: &#8222;Wir werden nicht
              alle entschlafen.&#8220; (Kap. 15,51)<br>
              Was sp&auml;ter individuell f&uuml;r den eigenen Tod erhofft wurde,
              da&szlig; man ihn wachen Auges erf&auml;hrt und ihm bewu&szlig;t
              entgegengeht, war anfangs die Vorbereitung auf den &#8222;Tag des
              Herrn&#8220;, eben der Wiederkunft Christi. Die Christen sollen
              ihn n&uuml;chtern und wach erwarten, so da&szlig; sie nicht &uuml;berrascht
              werden. In der Liturgie der fr&uuml;hen Kirche wurde die Wiederkunft
              mit dem Ruf Maranatha &#8211; Herr, komme bald, herbei gebetet.
              Der <a href="http://www.kath.de/Kirchenjahr/advent.php">Advent</a> ist nicht nur die Vorbereitung auf das Geburtsfest Jesu,
              sondern hat zu Beginn die eng&uuml;ltige Wiederkehr Jesu zum Thema.</font></P>
            <P><font face="Arial, Helvetica, sans-serif"><strong>Jesus bezeichnete sich
                wahrscheinlich selbst als Menschensohn<br>
            </strong></font><font face="Arial, Helvetica, sans-serif">Dieser
                Erwartung, da&szlig; Gott sein Reich aufrichten und die
              Menschen aus dem Hin- und Herr der Geschichte herausholen wird,
              bestimmte zur Zeit Jesu das Bewu&szlig;tsein der Menschen. Man
              erwartete das Ende der Welt. Der Prophet Daniel hatte das in gro&szlig;en
              Visionen beschrieben, in der von einem Menschensohn die Rede ist.
              Diesem Menschensohn wird die Herrschaft &uuml;ber das Reich Gottes &uuml;bergeben.
              <br>
              F&uuml;r die Christen besteht kein Zweifel daran, da&szlig; Christus,
              der von den Toten Auferstandene, der ist, der zum Endgericht wieder
              kommt und mit dem Menschensohn, wie Daniel ihn in seiner Vision
              gesehen hat, identisch ist. Das findet sich z.B. in den Weltgerichtsszenen &uuml;ber
              den Portalen mittelalterlicher Kathedralen.<br>
              In den Evangelien bezieht sich Jesus auf das Kommen des Menschensohns
              und erkl&auml;rt sich selbst auch mit dem Titel Menschensohn zum
              Herrn &uuml;ber den Sabbat. Die Bibelwissenschaftler diskutieren,
              ob Jesus sich selbst als Menschensohn bezeichnet hat oder ihm der
              Titel &#8220;Menschensohn&#8220; von den Evangelisten in den
              Mund gelegt wurde. Da der Titel in den Apostelbriefen nicht gebraucht
              wird, die ja bereits die fr&uuml;hchristliche Liturgie widerspiegeln,
              kann man annehmen, da&szlig; die Evangelien sich auf die Aussagen
              Jesu beziehen.</font></P>
            <P><font face="Arial, Helvetica, sans-serif"><strong>Auch Jesus erwartete
                  das Ende der Welt</strong><br>
              Jesus selbst k&uuml;ndigt das Ende der Welt an, l&auml;&szlig;t
              aber den Zeitpunkt offen:<br>
              Als Jesus von den Pharis&auml;ern gefragt wurde, wann das Reich
              Gottes komme, antworte er: &quot;Das Reich kommt nicht so, da&szlig; man
              es an &auml;u&szlig;eren Zeichen erkennen k&ouml;nnte. Man kann
              auch nicht sagen: Seht, hier ist es, oder: Dort ist es. Denn:
              Das Reich Gottes ist schon mitten unter euch. <br>
              Er sagte zu den J&uuml;ngern: Es wird eine Zeit kommen, in der
              ihr auch danach sehnt, auch nur einen von den Tagen des Menschensohnes
              zu erleben; aber ihr werdet ihn nicht erleben. Und wenn man zu
              euch sagt: Dort ist er! Hier ist er!, so geht nicht hin und lauft
              nicht hinterher. Denn wie der Blitz von einem Ende des Himmels
              bis zum anderen leuchtet, so wird der Menschensohn an seinem Tag
              erscheinen.&quot;<br>
              Lukas 17,20-24</font></P>
            <p><font face="Arial, Helvetica, sans-serif">Als seine J&uuml;nger
                am Sabbat &Auml;hren zupfen und die Pharis&auml;er
              sich dar&uuml;ber beschweren, da&szlig; sie damit die strengen
              Regelungen f&uuml;r die ruhe an diesem heiligen Tag brechen, antwortet
              Jesus:<br>
              &quot;Habt ihr nie gelesen, was David getan hat, als er und seine Begleiter
              hungrig waren und nichts zu essen hatten &#8211; wie er zur Zeit
              des Hohenpriesters Abjatar in das Haus Gottes ging und die heiligen
              Brote a&szlig;, die au&szlig;er den Priestern niemand essen darf,
              auch seinen Begleitern davon gab? Und Jesus f&uuml;gte hinzu: Der
              Sabbat ist f&uuml;r den Menschen da, nicht der Mensch f&uuml;r
              den Sabbat. Deshalb ist auch der Menschensohn Herr auch &uuml;ber
              den Sabbat.&quot;<br>
              Markus 2, 25-28</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Da&szlig; das Reich
                Gottes schon angebrochen ist und der Menschensohn in Jesus aufgetreten
                ist, zeit sich in folgenden Aussagen Jesu:<br>
              &quot;Selig seid ihr, wenn euch die Menschen hassen und aus ihrer Gemeinschaft
              ausschlie&szlig;en, wenn sie euch beschimpfen und euch in Verruf
              bringen um des Menschensohnes willen. Freut euch und jauchzt an
              jenem Tag; euer Lohn im Himmel wird gro&szlig; sein.<br>
              Lukas 6,22-23</font></p>
            <p><font face="Arial, Helvetica, sans-serif">In der dritten Person
                sind verschiedene Aussagen gefa&szlig;t,
              wenn Jesus &uuml;ber sein Leiden spricht:<br>
              &quot;
            </font><font face="Arial, Helvetica, sans-serif">Dann
              begann er, sie dar&uuml;ber zu belehren, der Menschensohn
              m&uuml;sse vieles erleiden und von den &Auml;ltesten, den Hohenpriestern
              und den Schriftgelehrten verworfen werden; er werde get&ouml;tet,
              aber nach drei Tagen werde er auferstehen.&quot;<br>
              Markus 8,31</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Am &Ouml;lberg weckt
                er die J&uuml;nger
                mit folgenden Worten:<br>
              &quot;Schlaft ihr immer noch und ruht euch aus? Es ist genug. Die Stunde
              ist gekommen; jetzt wird der Menschensohn den S&uuml;ndern ausgeliefert.
              Steht auf, wir wollen gehen. Seht, der Verr&auml;ter, der mich
              ausliefert, ist da.&quot;<br>
              Markus 14, 41-42</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Provokativ identifiziert
                sich Jesus in seinem Verh&ouml;r vor
              dem Hohen Rat mit dem Menschensohn:<br>
              &quot;Da wandte sich der Hohepriester nochmals an ihn und fragte: Bist
              du der Messias, der Sohn des Hochgelobten?<br>
              Jesus sagte: Ich bin es. Und ihr werdet den Menschensohn zur Rechten
              der Macht sitzen und mit den Wolken des Himmels kommen sehen.<br>
              Da zerri&szlig; der Hohepriester sein Gewand und rief: Wozu brauchen
              wir noch Zeugen? Ihr habe die Gottesl&auml;sterung geh&ouml;rt.&quot;<br>
              Markus 14, 60-63</font></p>
            <p><font size="2" face="Arial, Helvetica, sans-serif">Text: Eckhard Bieger
                S.J.</font><font face="Arial, Helvetica, sans-serif"><br>
            </font></p>
            <P>&copy;<a href="http://www.kath.de"> <font face="Arial, Helvetica, sans-serif">www.kath.de</font></a></P>
            </TD>
          <TD background=boxright.gif><IMG height=8 alt="" 
            src="boxright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=8 alt="" src="boxbottomleft.gif" 
            width=8></TD>
          <TD background=boxbottom.gif><IMG height=8 alt="" 
            src="boxbottom.gif" width=8></TD>
          <TD><IMG height=8 alt="" src="boxbottomright.gif" 
            width=8></TD></TR></TBODY></TABLE></TD><td style='vertical-align:top;'>
 <script type="text/javascript"><!--
        google_ad_client = "pub-9537752113242914";
        google_ad_width = 336;
        google_ad_height = 280;
        google_ad_format = "336x280_as";
        google_ad_type = "text_image";
        //2006-10-23: Lexikon Symbole
        google_ad_channel = "8495253357";
        //--></script>
        <script type="text/javascript"
          src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
        </script>
</td>
</TR>
  <TR>
    <TD vAlign=top align=left>&nbsp; </TD>
  </TR></TBODY></TABLE></BODY></HTML>
