---
title: Jesus, der Sündenbock
author: 
tags: 
created_at: 
images: []
---


# **Wie kam es zur Hinrichtung Jesu?*
# **Der Tod Jesu gibt viele Fragen auf. Diese Fragen sind nicht nur theoretischer Natur, sondern je nach ihrer Beantwortung gestaltet sich das Verhältnis der Christen zu den Juden. Wenn Pilatus den Tod Jesu zu verantworten hat, dann wäre der römische Staat verantwortlich für dne Tod. Die Evangelien berichten aber, daß Pilatus Jesus frei bekommen wollte. Er ließ den Barrabas, „einen Aufrührer, der bei einem Aufstand einen Mord begangen hatte“ vorführen. Die Juden konnten wählen, ob dieser zum Fest freikommen sollte oder Jesus. „Pilatus fragte sie: Wollt ihr, daß ich den König der Juden freilasse? Er merkte nämlich, daß die Hohenpriester nur aus Neid Jesus an ausgeliefert hatten. Die Hohenpriester wiegelten die Menge auf, lieber die Freilassung des Barrabas zu fordern. Pilatus wandte sich von neuem an sie und fragte: Was soll ich dann mit dem tun, den ihr den König der Juden nennt? Da schrieen sie: Kreuzige ihn.“ Markus 15,7-13**
 Dieses „Kreuzige ihn“ hallt durch die Geschichte. Christen bezeichneten die Juden als „Gottesmörder“. Unschuldig waren die Juden sicher nicht.***

 Wenn Jesus als der Gesandte Gottes, als ***[Messias](messias_christus.php)  hingerichtet wurde, hat dann Gott selbst ihn dem Tod überantwortet?***

# *
 Gott brauchte keine Genugtuung für die Sünden der Menschen*****

 Anselm von Canterbury, ein Theologe, der um 1100 gelebt hat, entwickelte die sog. Satisfaktionstheorie. Er wendet sich mit seiner Argumentation an Juden und den Islam, die Jesus Christus nicht als den Erlöser anerkennen, um zu erklären, warum Jesus Mensch wurde. SeineÜberlegung: Weil der Mensch durch seine Sünde die Ordnung des Kosmos gestört hat, ist eine Wiederherstellung der ursprünglichen Ordnung notwendig. Die Sünde verlangt Wiedergutmachung, entweder durch Strafe oder durch Genugtuung. Da Gott das ewige Heil des Menschen will, bleibt nur die Genugtuung als Möglichkeit. Da die Schuld unendlich groß ist, denn Gott wurde beleidigt, ist der Mensch zu klein und zu schwach, aus eigenen Kräften Genugtuung zu leisten. Nur wenn Gott selbst Mensch wird, kann die ursprüngliche Ordnung und damit die Ehre Gottes wieder hergestellt werden. Die Erklärung des Anselms fand schon zu seiner Zeit nicht allgemeine Zustimmung, der französische Theologe Peter Abaelard zeigt, daß eine juristisch gefaßte Satisfaktionstheorie die Dimension der Liebe, die für Jesus zentral war, nicht erfaßt. Auch erklärt die Theorie nicht, wie es zur Überwindung der sündigen Haltung im Menschen kommt. Die Sündhaftigkeit des Menschen ist in einer religiösen und nicht politischen Interpretation die Ursache für die Verurteilung Jesu. Denn wäre der Mensch nicht Sünder, wäre auch nicht auf die Idee gekommen, andere durch Geißelung und Kreuz hinzurichten.  

# **Die Hinrichtung Jesu proviziert keinen Haß seiner Jünger gegen die Juden****
 Daß die Juden am Tod Jesu eine Mitschuld tragen, wird von der ersten christlichen Generation behauptet, aber daraus folgt kein Haß auf die Juden. Vielmehr ruft Petrus seine jüdischen Mitbürger auf, in dem auferstandenen Jesus den verheißenen ***[Messias,](messias.php) den Christus, den Gesalbten zu erkennen. Das Sterben Jesu wird nicht auf die Tat einzelner, auch nicht des jüdischen Volkes bezogen, sondern auf die Sünden aller. Die Evangelien liefern selbst eine Interpretation, die den Mechanismus erklärt, der zum Tod Jesu führte. Diese Interpretation bezieht auch die Stimmungslage ein, in der sich die Bevölkerung damals befunden hat. 

# **Die Ausstoßung des Sündenbocks:** **
 Das Volk fühlte sich unterdrückt, Das Land war vom römischen Militär besetzt, Es gab Aufstände, die jeweils niedergeschlagen wurden. Ob in einer Abteilung, in einer Sportmannschaft oder in einem Volk, wenn die Stimmung immer schlechter wird, muß ein Entlastungsmechanismus greifen, damit das Zusammenleben wieder erträglich wird. ***

 Ein wirksamer Mechanismus, der sowohl das Streitpotential wegschafft wie auch eine neue Solidarität bewirkt, ist die Ausstoßung oder sogar Hinrichtung eines Sündenbocks. Der Begriff leitet sich von einem Ritus des Alten Testaments her. Jährlich wurde ein Bock in die Wüste getrieben, dem man vorher die Schuldlasten in der Form aufgeladen hat, daß die Männer ihre Fäuste auf das Haupt des Bockes gestemmt hatten. Viel wirkungsvoller als ein Tieropfer sind, zumindest in der Neuzeit, Menschenopfer. Der Mechanismus funktioniert so, daß ein Außenseiter zum Schuldigen erklärt, immer mehr in die Enge getrieben und schließlich umgebracht wird. In vielen Fällen läuft das so ab: Die unguten Gefühle und Mißstimmungen werden einer Person zur Last gelegt. Wenn diese beseitigt ist, ist man die Gefühle los und empfindet eine neue Solidarität. Wir nennen das Mobbing. Im Johannesevangelium wird der Sachverhalt genau beschrieben. Der Hohepriester Kajaphas stellt fest: "Ihr bedenkt nicht, daß es besser für euch ist, wenn ein einziger Mensch für das Volk stirbt, als wenn das ganze Volk zugrunde geht.“ Text s.u.***

 Offensichtlich hat auch Jesus den Mechanismus durchschaut, denn er hat nicht für sein Überleben gekämpft – wohl deshalb, um den Mechanismus zu überwinden. Denn der Mechanismus funktioniert nur so lange, wie er nicht durchschaut wird, z.B. wenn eine Gruppe, die z.B.im eigenen Land Fremde sind, oder ein anderes Volk als „böse“ hingestellt werden kann, darf man diese Menschen hassen. Das bewirkt bei der Mehrheit ein Gefühl der Zusammengehörigkeit. Als Pilatus den durch die Geißelung zerschundnen jungen Mann der Menge vorführt, bewirkt das nicht etwa Mitleid, sondern den Ruf „Kreuzige ihn“. Der Sündenbock muß tatsächlich getötet werden, sonst wirkt der Mechanismus nicht. Indem Jesus sich nicht gegen seine Tötung stemmt, überwindet er den Mechanismus, den er vorher schon im Gebot der Feindesliebe angeprangert hat. Feindesliebe heißt nämlich nicht, daß ich alle Menschen sympathisch finden muß, wohl aber, daß meine Antipathien und Haßgefühle mir nicht erlauben, den anderen zu mißachten, ihm zu schaden, ihn umzubringen. Der ***[Karfreitag](http://www.kath.de/Kirchenjahr/karfreitag.php)              erinnert jedes Jahr an diesen Mechanismus, den Jesus hingenommen hat.***

 Mobbing will nicht den physischen, wohl aber den sozialen Tod des Opfers. Diesen Mechanismus von innen her zu überwinden, das könnte Jesus als seinen Auftrag verstanden haben. Viele, die ihm nachgefolgt sind, wurden von dem Mechanismus von Verfolgung, Rache und Haß nicht mehr in Besitz genommen, so wie wir es die letzten Jahrzehnten in vielen religiös bestimmten Auseinandersetzungen erleben, ob in Palästina oder Nordirland. Alle Getauften, die Jesus nachfolgen, könnten sich dem Sündenbockmechanismus entziehen. Die Jünger und die Märtyrer der frühen Kirche haben Jesus verstanden. Das zeigt sich daran, daß sie nicht mit Haß- und Rachegefühlen auf die Verfolgung und ihr Hinrichtung reagiert haben. Die Anhänger Jesu lernten durch die Lieder vom***[ Gottesknecht](gottesknecht.php), den Sinn des Leidens zu verstehen.***

# **Der Mechanismus von Ausgrenzung und Vernichtung eines Opfers kann nur solange funktionieren, wie er von den Beteiligten nicht durchschaut wird. Erlösung ist durchaus für die real, die der Mechanismus nicht mehr in Besitz nehmen kann. Wie wirksam der Mechanismus allerdings ist, zeigt die Judenvernichtung im Nationalsozialismus. Diese war nur möglich, weil die Juden als Feinde des deutschen Volkes und Verursacher der wirtschaftlichen Probleme hingestellt wurden und die Deutschen das geglaubt haben.**
 Christentum und Islam gehen von dem jüdischen Geschichtsbild aus, daß am Ende der Geschichte Gott selbst in einem Weltgericht endgültig die Trennung zwischen Gut und Böse vornimmt. Zugleich ist Gott die Instanz, die dem Menschen auch gravierendes Fehlverhalten vergeben und damit in den Zustand der Sündenlosigkeit versetzen kann. Daß Gott nicht nur die Sünden vergibt, sondern den entscheidenden Mechanismus, mit dem Menschen sich gegenseitig vernichten, überwinden wollte, das hat zum Kreuzestod Jesu geführt. Das leistet die Vernunft nicht, denn dafür ist der Mechanismus zu stark, nicht zuletzt deshalb, weil nach der Ausstoßung und Vernichtung des Sündenbocks sich ein neues Gemeinschaftsgefühl herstellt. Das hat der Evangelist Lukas beobachtet. Dieser Evangelist überliefert, daß Pilatus Jesus zu Herodes geschickt hat. Dieser wollte ihn verhören, doch Jesus schwieg. ***


„ Herodes und seine Soldaten zeigten ihm offen ihre Verachtung. Er trieb seinen Spott mit Jesus, ließ ihm ein Prunkgewand umhängen und schickte ihn zu Pilatus zurück. An diesem Tag wurden Herodes und Pilatus Freunde; vorher waren sie Feinde gewesen.“ Kap.23,11-12 ***

 Wenn zwei Freunde werden, rechtfertigt das für sie die Ausstoßung des Opfers. 

# **Zitate****
 Daß Jesus von der jüdischen Obrigkeit die Rolle des Sündenbocks bewußt zugeteilt wurde, berichtet Johannes nach der spektakulären Auferweckung des Lazarus:***


„ Da beriefen die Hohenpriester und die Pharisäer eine Versammlung des Hohen Rates ein. Sie sagten: Was sollen wir tun? Dieser Mensch tut viele Zeichen. Wenn wir ihn gewähren lassen, werden alle an ihn glauben. Dann werden die Römer kommen und uns die heilige Stätte und das Volk nehmen. Einer von ihnen, Kajaphas, der Hohepriester jener Jahre, sagte zu ihnen: Ihr versteht überhaupt nichts. Ihr bedenkt nicht, daß es besser für euch ist, wenn ein einziger Mensch für das Volk stirbt, als wenn das ganze Volk zugrunde geht. Das sagte er nicht aus sich selbst, sondern weil er der Hohepriester jenes Jahres war, sagte er aus prophetischer Eingebung, daß Jesus für das Volk sterben werde. Aber er sollte nicht nur für das Volk sterben, sondern auch, um die versprengten Kinder Gottes wieder zu sammeln. Von diesem Tag an waren sie entschlossen, ihn zu töten.“ Kap.11, 47-53 

# **Wie die Jünger seinen Tod sehen, zeigt folgende überlieferte Predigt des Petrus:**
„ Der Gott Abrahams, Isaaks und Jakobs hat seinen Knecht Jesus verherrlicht, den ihr verraten und vor Pilatus verleugnet habt, obwohl dieser entschieden hatte, ihn freizulassen. Ihr aber hat den Heiligen und Gerechten verleugnet und die Freilassung eines Mörders gefordert. Den Urheber des Lebens habt ihr getötet, aber Gott hat ihn von den Toten auferweckt. Dafür sind wir Zeugen, ebenso wie eure Führer. Gott aber hat auf diese Weise erfüllt, was er durch den Mund aller Propheten im Voraus verkündet hat: daß der Messias leiden werde. Also, kehrt um und tut Buße, damit eure Sünden getilgt werden und der Herr Zeiten des Aufatmens kommen läßt .... Nun Brüder, ich weiß, ihr habt aus Unwissenheit gehandelt, .... für euch zuerst hat Gott seinen Knecht erweckt und gesandt, damit er euch segnet und von jeder Bosheit abbringt.***

 Apostelgeschichte 3, 11-15, 17-20, 26 

***Eckhard Bieger 
 ** 

©***[ ***www.kath.de](http://www.kath.de) 
