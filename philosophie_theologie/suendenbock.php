<HTML><HEAD><TITLE>Jesus, der S&uuml;ndenbock</TITLE>
<META http-equiv=Content-Type content="text/html; charset=iso-8859-1"><LINK 
title=fonts href="kaltefleiter.css" type=text/css 
rel=stylesheet>
<META content="MSHTML 5.50.4134.600" name=GENERATOR></HEAD>
<BODY bgColor=#ffffff leftMargin=6 topMargin=6 marginheight="6" marginwidth="6">
<TABLE cellSpacing=0 cellPadding=6 width="100%" border=0>
  <TBODY>
  <TR>
    <TD vAlign=top align=left width=100> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b>Philosophie&amp;Theologie</b></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td> <?php include("logo.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10"> <?php include("az.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </TD>
    <TD vAlign=top rowSpan=2>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
        <TBODY>
        <TR vAlign=top align=left>
          <TD width=8><IMG height=8 alt="" 
            src="boxtopleftcorner.gif" width=8></TD>
          <TD background=boxtop.gif><IMG height=8 alt="" 
            src="boxtop.gif" width=8></TD>
          <TD width=8><IMG height=8 alt="" 
            src="boxtoprightcorner.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxtopleft.gif><IMG height=8 alt="" 
            src="boxtopleft.gif" width=8></TD>
          <TD bgColor=#e2e2e2> 
            <H1><font face="Arial, Helvetica, sans-serif"> Jesus, der S&uuml;ndenbock</font></H1>
          </TD>
          <TD background=boxtopright.gif><IMG height=8 
            alt="" src="boxtopright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=13 alt="" src="boxdividerleft.gif" 
            width=8></TD>
          <TD background=boxdivider.gif><IMG height=13 
            alt="" src="boxdivider.gif" width=8></TD>
          <TD><IMG height=13 alt="" 
            src="boxdividerright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxleft.gif><IMG height=8 alt="" 
            src="boxleft.gif" width=8></TD>
          <TD class=L12>
            <P><STRONG><font face="Arial, Helvetica, sans-serif">Wie kam es
                  zur Hinrichtung Jesu?</font></STRONG></P>
            <P><font face="Arial, Helvetica, sans-serif">Der Tod Jesu gibt viele
                Fragen auf. Diese Fragen sind nicht nur theoretischer Natur,
                sondern je nach ihrer Beantwortung gestaltet
              sich das Verh&auml;ltnis der Christen zu den Juden. Wenn Pilatus
              den Tod Jesu zu verantworten hat, dann w&auml;re der r&ouml;mische
              Staat verantwortlich f&uuml;r dne Tod. Die Evangelien berichten
              aber, da&szlig; Pilatus
              Jesus frei bekommen wollte. Er lie&szlig; den Barrabas, &#8222;einen
              Aufr&uuml;hrer, der bei einem Aufstand einen Mord begangen hatte&#8220; vorf&uuml;hren.
              Die Juden konnten w&auml;hlen, ob dieser zum Fest freikommen sollte
              oder Jesus. &#8222;Pilatus fragte sie: Wollt ihr, da&szlig; ich
              den K&ouml;nig der Juden freilasse? Er merkte n&auml;mlich, da&szlig; die
              Hohenpriester nur aus Neid Jesus an ausgeliefert hatten. Die Hohenpriester
              wiegelten die Menge auf, lieber die Freilassung des Barrabas zu
              fordern. Pilatus wandte sich von neuem an sie und fragte: Was soll
              ich dann mit dem tun, den ihr den K&ouml;nig der Juden nennt? Da
              schrieen sie: Kreuzige ihn.&#8220; Markus 15,7-13<br>
              Dieses &#8222;Kreuzige ihn&#8220; hallt durch die Geschichte. Christen
              bezeichneten die Juden als &#8222;Gottesm&ouml;rder&#8220;. Unschuldig
              waren die Juden sicher nicht.<br>
              Wenn Jesus als der Gesandte Gottes, als <a href="messias_christus.php">Messias</a> 
              hingerichtet wurde, hat dann Gott selbst ihn dem Tod &uuml;berantwortet?<br>
              <br>
              <strong>Gott brauchte keine Genugtuung f&uuml;r die S&uuml;nden der Menschen</strong><br>
              Anselm von Canterbury, ein Theologe, der um 1100 gelebt hat, entwickelte
              die sog. Satisfaktionstheorie. Er wendet sich mit seiner Argumentation
              an Juden und den Islam, die Jesus Christus nicht als den Erl&ouml;ser
              anerkennen, um zu erkl&auml;ren, warum Jesus Mensch wurde. Seine&Uuml;berlegung:
              Weil der Mensch durch seine S&uuml;nde die Ordnung des Kosmos gest&ouml;rt
              hat, ist eine Wiederherstellung der urspr&uuml;nglichen Ordnung
              notwendig. Die S&uuml;nde verlangt Wiedergutmachung, entweder durch
              Strafe oder durch Genugtuung. Da Gott das ewige Heil des Menschen
              will, bleibt nur die Genugtuung als M&ouml;glichkeit. Da die Schuld
              unendlich gro&szlig; ist, denn Gott wurde beleidigt, ist der Mensch
              zu klein und zu schwach, aus eigenen Kr&auml;ften Genugtuung zu
              leisten. Nur wenn Gott selbst Mensch wird, kann die urspr&uuml;ngliche
              Ordnung und damit die Ehre Gottes wieder hergestellt werden. Die
              Erkl&auml;rung des Anselms fand schon zu seiner Zeit nicht allgemeine
              Zustimmung, der franz&ouml;sische Theologe Peter Abaelard zeigt,
              da&szlig; eine juristisch gefa&szlig;te Satisfaktionstheorie die
              Dimension der Liebe, die f&uuml;r Jesus zentral war, nicht erfa&szlig;t.
              Auch erkl&auml;rt die Theorie nicht, wie es zur &Uuml;berwindung
              der s&uuml;ndigen Haltung im Menschen kommt. Die S&uuml;ndhaftigkeit
              des Menschen ist in einer religi&ouml;sen und nicht politischen
              Interpretation die Ursache f&uuml;r die Verurteilung Jesu. Denn
              w&auml;re
              der Mensch nicht S&uuml;nder, w&auml;re auch nicht auf die Idee
              gekommen, andere durch Gei&szlig;elung und Kreuz hinzurichten. </font></P>
            <P><font face="Arial, Helvetica, sans-serif"><strong>Die Hinrichtung Jesu
                proviziert keinen Ha&szlig; seiner J&uuml;nger gegen die Juden</strong><br>
                Da&szlig; die
                Juden am Tod Jesu eine Mitschuld tragen, wird von der ersten
                christlichen
                Generation behauptet, aber daraus folgt
              kein Ha&szlig; auf die Juden. Vielmehr ruft Petrus seine j&uuml;dischen
              Mitb&uuml;rger auf, in dem auferstandenen Jesus den verhei&szlig;enen
              <a href="messias.php">Messias,</a> den Christus, den Gesalbten zu erkennen. Das Sterben Jesu
              wird nicht auf die Tat einzelner, auch nicht des j&uuml;dischen
              Volkes bezogen, sondern auf die S&uuml;nden aller. Die Evangelien
              liefern selbst eine Interpretation, die den Mechanismus erkl&auml;rt,
              der zum Tod Jesu f&uuml;hrte. Diese Interpretation bezieht auch
              die Stimmungslage ein, in der sich die Bev&ouml;lkerung damals
              befunden hat.</font></P>
            <P><font face="Arial, Helvetica, sans-serif"><strong>Die Aussto&szlig;ung des S&uuml;ndenbocks:</strong> <br>
  Das Volk f&uuml;hlte sich unterdr&uuml;ckt, Das Land war vom r&ouml;mischen
              Milit&auml;r besetzt, Es gab Aufst&auml;nde, die jeweils niedergeschlagen
              wurden. Ob in einer Abteilung, in einer Sportmannschaft oder in
              einem Volk, wenn die Stimmung immer schlechter wird, mu&szlig; ein
              Entlastungsmechanismus greifen, damit das Zusammenleben wieder
              ertr&auml;glich wird. <br>
              Ein wirksamer Mechanismus, der sowohl das Streitpotential wegschafft
              wie auch eine neue Solidarit&auml;t bewirkt, ist die Aussto&szlig;ung
              oder sogar Hinrichtung eines S&uuml;ndenbocks. Der Begriff leitet
              sich von einem Ritus des Alten Testaments her. J&auml;hrlich wurde
              ein Bock in die W&uuml;ste
              getrieben, dem man vorher die Schuldlasten in der Form aufgeladen
              hat, da&szlig; die M&auml;nner ihre F&auml;uste auf das Haupt des
              Bockes gestemmt hatten. Viel wirkungsvoller als ein Tieropfer sind,
              zumindest in der Neuzeit, Menschenopfer. Der Mechanismus funktioniert
              so, da&szlig; ein Au&szlig;enseiter zum Schuldigen erkl&auml;rt,
              immer mehr in die Enge getrieben und schlie&szlig;lich umgebracht
              wird. In vielen F&auml;llen l&auml;uft das so ab: Die unguten Gef&uuml;hle
              und Mi&szlig;stimmungen werden einer Person zur Last gelegt. Wenn
              diese beseitigt ist, ist man die Gef&uuml;hle los und empfindet
              eine neue Solidarit&auml;t. Wir nennen das Mobbing. Im Johannesevangelium
              wird der Sachverhalt genau beschrieben. Der Hohepriester Kajaphas
              stellt fest: &quot;Ihr bedenkt nicht, da&szlig; es besser f&uuml;r
              euch ist, wenn ein einziger Mensch f&uuml;r das Volk stirbt, als
              wenn das ganze Volk zugrunde geht.&#8220; Text s.u.<br>
              Offensichtlich hat auch Jesus den Mechanismus durchschaut, denn
              er hat nicht f&uuml;r sein &Uuml;berleben gek&auml;mpft &#8211; wohl
              deshalb, um den Mechanismus zu &uuml;berwinden. Denn der Mechanismus
              funktioniert nur so lange, wie er nicht durchschaut wird, z.B.
              wenn eine Gruppe, die z.B.im eigenen Land Fremde sind, oder ein
              anderes Volk als &#8222;b&ouml;se&#8220; hingestellt
              werden kann, darf man diese Menschen hassen. Das bewirkt bei der
              Mehrheit ein Gef&uuml;hl der Zusammengeh&ouml;rigkeit. Als Pilatus
              den durch die Gei&szlig;elung zerschundnen jungen Mann der Menge
              vorf&uuml;hrt, bewirkt das nicht etwa Mitleid, sondern den Ruf &#8222;Kreuzige
              ihn&#8220;. Der S&uuml;ndenbock mu&szlig; tats&auml;chlich get&ouml;tet
              werden, sonst wirkt der Mechanismus nicht. Indem Jesus sich nicht
              gegen seine T&ouml;tung stemmt, &uuml;berwindet er den Mechanismus,
              den er vorher schon im Gebot der Feindesliebe angeprangert hat.
              Feindesliebe hei&szlig;t n&auml;mlich nicht, da&szlig; ich alle
              Menschen sympathisch finden mu&szlig;, wohl aber, da&szlig; meine
              Antipathien und Ha&szlig;gef&uuml;hle mir nicht erlauben, den anderen
              zu mi&szlig;achten, ihm zu schaden, ihn umzubringen. Der <a href="http://www.kath.de/Kirchenjahr/karfreitag.php">Karfreitag</a>              erinnert jedes Jahr an diesen Mechanismus, den Jesus hingenommen
              hat.<br>
              Mobbing will nicht
              den physischen, wohl aber den sozialen Tod des Opfers. Diesen Mechanismus
              von innen her zu &uuml;berwinden, das k&ouml;nnte Jesus als seinen
              Auftrag verstanden haben. Viele, die ihm nachgefolgt sind, wurden
              von dem Mechanismus von Verfolgung, Rache und Ha&szlig; nicht mehr
              in Besitz genommen, so wie wir es die letzten Jahrzehnten in vielen
              religi&ouml;s bestimmten Auseinandersetzungen erleben, ob in Pal&auml;stina
              oder Nordirland. Alle Getauften, die Jesus nachfolgen, k&ouml;nnten
              sich dem S&uuml;ndenbockmechanismus entziehen. Die
              J&uuml;nger und die M&auml;rtyrer der fr&uuml;hen Kirche haben
              Jesus verstanden. Das zeigt sich daran, da&szlig; sie nicht
              mit Ha&szlig;-
              und Rachegef&uuml;hlen auf die Verfolgung und ihr Hinrichtung reagiert
              haben. Die Anh&auml;nger Jesu lernten durch die Lieder vom<a href="gottesknecht.php"> Gottesknecht</a>,
              den Sinn des Leidens zu verstehen.<br>
            </font><font face="Arial, Helvetica, sans-serif">Der Mechanismus
              von Ausgrenzung und Vernichtung eines Opfers kann nur solange funktionieren,
                wie er von den Beteiligten nicht
                durchschaut wird. Erl&ouml;sung ist durchaus f&uuml;r die real,
                die der Mechanismus nicht mehr in Besitz nehmen kann. Wie wirksam
                der Mechanismus allerdings ist, zeigt die Judenvernichtung im
                Nationalsozialismus. Diese war nur m&ouml;glich, weil die Juden
                als Feinde des deutschen Volkes und Verursacher der wirtschaftlichen
                Probleme hingestellt wurden und die
                Deutschen das geglaubt haben.<br>
                Christentum und Islam gehen von dem j&uuml;dischen Geschichtsbild
                aus, da&szlig; am Ende der Geschichte Gott selbst in einem Weltgericht
                endg&uuml;ltig die Trennung zwischen Gut und B&ouml;se vornimmt.
                Zugleich ist Gott die Instanz, die dem Menschen auch gravierendes
                Fehlverhalten vergeben und damit in den Zustand der S&uuml;ndenlosigkeit
                versetzen kann. Da&szlig; Gott nicht nur die S&uuml;nden vergibt,
                sondern den entscheidenden Mechanismus, mit dem Menschen sich gegenseitig
                vernichten, &uuml;berwinden wollte, das hat zum Kreuzestod Jesu
                gef&uuml;hrt. Das leistet die Vernunft nicht, denn daf&uuml;r ist
                der Mechanismus zu stark, nicht zuletzt deshalb, weil nach der
                Aussto&szlig;ung und Vernichtung des S&uuml;ndenbocks sich ein
                neues Gemeinschaftsgef&uuml;hl herstellt. Das hat der Evangelist
                Lukas beobachtet. Dieser Evangelist &uuml;berliefert, da&szlig; Pilatus
                Jesus zu Herodes geschickt hat. Dieser wollte ihn verh&ouml;ren,
                doch Jesus schwieg. <br>
&#8222;
                Herodes und seine Soldaten zeigten ihm offen ihre Verachtung. Er
                trieb seinen Spott mit Jesus, lie&szlig; ihm ein Prunkgewand umh&auml;ngen
                und schickte ihn zu Pilatus zur&uuml;ck. An diesem Tag wurden Herodes
                und Pilatus Freunde; vorher waren sie Feinde gewesen.&#8220; Kap.23,11-12 <br>
                Wenn zwei Freunde werden, rechtfertigt das f&uuml;r sie die Aussto&szlig;ung
                des Opfers.</font></P>
            <P><font face="Arial, Helvetica, sans-serif"><strong>Zitate</strong><br>
  Da&szlig; Jesus von der j&uuml;dischen Obrigkeit die Rolle des
              S&uuml;ndenbocks bewu&szlig;t zugeteilt wurde, berichtet Johannes
              nach der spektakul&auml;ren Auferweckung des Lazarus:<br>
&#8222;
              Da beriefen die Hohenpriester und die Pharis&auml;er eine Versammlung
              des Hohen Rates ein. Sie sagten: Was sollen wir tun? Dieser Mensch
              tut viele Zeichen. Wenn wir ihn gew&auml;hren lassen, werden alle
              an ihn glauben. Dann werden die R&ouml;mer kommen und uns die heilige
              St&auml;tte und das Volk nehmen. Einer von ihnen, Kajaphas, der
              Hohepriester jener Jahre, sagte zu ihnen: Ihr versteht &uuml;berhaupt
              nichts. Ihr bedenkt nicht, da&szlig; es besser f&uuml;r euch ist,
              wenn ein einziger Mensch f&uuml;r das Volk stirbt, als wenn das
              ganze Volk zugrunde geht. Das sagte er nicht aus sich selbst, sondern
              weil er der Hohepriester jenes Jahres war, sagte er aus prophetischer
              Eingebung, da&szlig; Jesus f&uuml;r das Volk sterben werde. Aber
              er sollte nicht nur f&uuml;r das Volk sterben, sondern auch, um
              die versprengten Kinder Gottes wieder zu sammeln. Von diesem Tag
              an waren sie entschlossen, ihn zu t&ouml;ten.&#8220; Kap.11, 47-53</font></P>
            <p><font face="Arial, Helvetica, sans-serif">Wie die J&uuml;nger seinen Tod sehen, zeigt folgende &uuml;berlieferte
              Predigt des Petrus:<br>
&#8222;
              Der Gott Abrahams, Isaaks und Jakobs hat seinen Knecht Jesus verherrlicht,
              den ihr verraten und vor Pilatus verleugnet habt, obwohl dieser
              entschieden hatte, ihn freizulassen. Ihr aber hat den Heiligen
              und Gerechten verleugnet und die Freilassung eines M&ouml;rders
              gefordert. Den Urheber des Lebens habt ihr get&ouml;tet, aber Gott
              hat ihn von den Toten auferweckt. Daf&uuml;r sind wir Zeugen, ebenso
              wie eure F&uuml;hrer. Gott aber hat auf diese Weise erf&uuml;llt,
              was er durch den Mund aller Propheten im Voraus verk&uuml;ndet
              hat: da&szlig; der Messias leiden werde. Also, kehrt um und tut
              Bu&szlig;e, damit eure S&uuml;nden getilgt werden und der Herr
              Zeiten des Aufatmens kommen l&auml;&szlig;t .... Nun Br&uuml;der,
              ich wei&szlig;, ihr habt aus Unwissenheit gehandelt, .... f&uuml;r
              euch zuerst hat Gott seinen Knecht erweckt und gesandt, damit er
              euch segnet und von jeder Bosheit abbringt.<br>
              Apostelgeschichte 3, 11-15, 17-20, 26</font></p>
            <p><font size="2" face="Arial, Helvetica, sans-serif">Eckhard Bieger</font></p>
            <P><strong>            </strong></P>
            <P>&copy;<a href="http://www.kath.de"> <font face="Arial, Helvetica, sans-serif">www.kath.de</font></a></P>
            </TD>
          <TD background=boxright.gif><IMG height=8 alt="" 
            src="boxright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=8 alt="" src="boxbottomleft.gif" 
            width=8></TD>
          <TD background=boxbottom.gif><IMG height=8 alt="" 
            src="boxbottom.gif" width=8></TD>
          <TD><IMG height=8 alt="" src="boxbottomright.gif" 
            width=8></TD></TR></TBODY></TABLE></TD><td style='vertical-align:top;'>
 <script type="text/javascript"><!--
        google_ad_client = "pub-9537752113242914";
        google_ad_width = 336;
        google_ad_height = 280;
        google_ad_format = "336x280_as";
        google_ad_type = "text_image";
        //2006-10-23: Lexikon Symbole
        google_ad_channel = "8495253357";
        //--></script>
        <script type="text/javascript"
          src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
        </script>
</td>
</TR>
  <TR>
    <TD vAlign=top align=left>&nbsp; </TD>
  </TR></TBODY></TABLE></BODY></HTML>
