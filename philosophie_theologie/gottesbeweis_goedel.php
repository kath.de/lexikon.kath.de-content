<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title>G&ouml;dels ontologischer Gottesbeweis - Philosophie&amp;Theologie</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/kaltefleiter.css" title="fonts">
</head>
<body bgcolor="#ffffff" leftmargin="6" topmargin="6" marginwidth="6" marginheight="6">
<table width="100%" border="0" cellspacing="0" cellpadding="6">
  <tr>
    <td width="100" align="left" valign="top" height="3"> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxtop.gif"><img src="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxtopleft.gif"><img src="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b>Philosophie&amp;Theologie</b></td>
          <td background="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxtopright.gif"><img src="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxdivider.gif"><img src="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxleft.gif"><img src="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxleft.gif" width="8" height="8" alt=""></td>
          <td> <?php include("logo.html"); ?> </td>
          <td background="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxright.gif"><img src="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxbottom.gif"><img src="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxtop.gif"><img src="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxtopleft.gif"><img src="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><font face="Arial, Helvetica, sans-serif"><strong>Inhaltsverzeichnis 
            </strong></font></td>
          <td background="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxtopright.gif"><img src="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxdivider.gif"><img src="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxleft.gif"><img src="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10"> <?php include("az.html"); ?> </td>
          <td background="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxright.gif"><img src="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxbottom.gif"><img src="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
    <td rowspan="2" valign="top">
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td background="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxtop.gif" colspan="2"><img src="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxtopleft.gif" width="8"><img src="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2" colspan="2"> 
            <h1>G&ouml;dels ontologischer Gottesbeweis</h1>
          </td>
          <td background="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxtopright.gif" width="8"><img src="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8"><img src="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxdivider.gif" colspan="2"><img src="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxdivider.gif" alt="" width="8" height="13"></td>
          <td width="8"><img src="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxleft.gif" width="8"><img src="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxleft.gif" width="8" height="8" alt=""></td>
          <td class="L12" width="70%">
		  
		  
		  
		  <p><br>
		    <font face="Arial, Helvetica, sans-serif">Wirklich neue Ideen hat G&ouml;del nicht gebracht. Die Idee, dass man aus
		    Gottes m&ouml;glicher Existenz auf seine notwendige schlie&szlig;en kann,
		    findet sich schon bei Kant. Und dass man f&uuml;r diese Beweisf&uuml;hrung
		    zeigen muss, dass Gottes Existenz m&ouml;glich ist, hat Leibnitz als erster
		    formuliert. Von Ihm stammt auch die Idee der positiven Eigenschaft:<br>
		    Eine Eigenschaft ist positiv, wenn sie keiner anderen Eigenschaft
		    widerspricht. <br>
		    Bei dem Versuch, diese Eigenschaften genauer zu bestimmen, verzettelt
		    er sich. Der Beweis scheint nicht durchf&uuml;hrbar zu sein, ohne dass
		    man sich irgend wo widerspricht.<br>
		    G&ouml;del hat bewiesen, dass es kein formales System gibt, das absolut
		    widerspruchsfrei ist. Wenn der ontologische Gottesbeweis im Widerspruch
		    m&uuml;ndet, liegt das also nicht notwendig am Beweis, sondern kann an
		    dem formalen System, der Sprache liegen, in der der Beweis gef&uuml;hrt
		    wird. G&ouml;del formuliert f&uuml;r seine Beweisf&uuml;hrung also ein
		    eigenes formales System. Er setzt die Existenz der positiven Eigenschaften
		    einfach voraus. Beispiele nennt er keine, sondern formuliert: <br>
		    Eine Eigenschaft ist entweder positiv oder negativ. <br>
		    Und weiter: <br>
		    Jede Eigenschaft, die notwendig eine positive Eigenschaft enth&auml;lt,
		    ist ebenfalls positiv.<br>
		    In dem System definiert G&ouml;del dann g&ouml;ttlich: <br>
		    Ein g&ouml;ttliches Wesen enth&auml;lt alle positiven Eigenschaften.<br>
		    Daraus folgt:<br>
		    G&ouml;ttlich ist eine positive Eigenschaft, sie enth&auml;lt ja alle anderen
		    positiven Eigenschaften.</font></p>
            <p><font face="Arial, Helvetica, sans-serif">G&ouml;del hat damit die Vorraussetzung geschaffen, um den Ansatz
              von Leibnitz, die m&ouml;gliche Existenz Gottes zu beweisen, widerspruchsfrei
              durchf&uuml;hren zu k&ouml;nnen.<br>
              Dazu behauptet er:<br>
              Es ist m&ouml;glich, dass es zu jeder positiven Eigenschaft mindestens
              ein Wesen gibt, das diese Eigenschaft besitzt.<br>
              Beweisen will er seine Behauptung, indem er vom Gegenteil ausgeht.
              Dazu &uuml;berlegt er, was passiert, wenn ein Wesen eine positive
              Eigenschaft hat, die von keinem Wesen besessen werden kann. Die
              Annahme f&uuml;hrt zu einem Widerspruch, z.B. das Wesen ist nicht
              mit sich selbst identisch. Also w&auml;re die Eigenschaft, nicht
              mit sich selbst identisch zu sein, positiv. Also w&auml;re das
              Gegenteil - die Selbstidentit&auml;t - eine negative Eigenschaft.<br>
              Wir wissen aber, dass jedes Wesen mit sich selbst identisch ist.
              Also ist die Selbstidentit&auml;t eine positive Eigenschaft. <br>
              Eine Eigenschaft kann aber nicht positiv und zugleich negativ sein.
              Also gibt es keine positive Eigenschaft, die von keinem Wesen besessen
              werden kann.</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Jetzt kann G&ouml;del den ontologischen Gottesbeweis selber angehen.
              Dazu definiert er die &#8222;notwendige Existenz&#8220;:<br>
              Etwas existiert genau dann notwendig, wenn f&uuml;r alle Eigenschaften,
              die sein Wesen ausmachen, gilt: Es ist m&ouml;glich, dass es zu
              dieser Eigenschaft mindestens ein Wesen gibt, das diese Eigenschaft
              besitzt.<br>
              Gottes Wesen macht es aus, dass er alle positiven Eigenschaften
              besitzt. Das bedeutet zum einem, dass die notwendige Existenz ebenfalls
              eine positive Eigenschaft ist. Zum anderen existiert Gott notwendig,
              wenn es ihn gibt.<br>
            </font></p>
            <p><font face="Arial, Helvetica, sans-serif">Weil G&ouml;del aber zeigen will, dass Gott auch notwendig existiert,
              wenn seine Existenz m&ouml;glich ist, verwendet er eine logische
              Regel: <br>
              Wir k&ouml;nnen sagen, wenn wir etwas Hei&szlig;es anfassen, verbrennen
              wir uns die Finger. <br>
              Daraus k&ouml;nnen wir schlie&szlig;en: <br>
              Wenn wir etwas Hei&szlig;es anfassen k&ouml;nnen, k&ouml;nnen wir
              uns die Finger verbrennen.<br>
              Mit Hilfe dieser Regel formuliert G&ouml;del:<br>
              Wenn es m&ouml;glich ist, dass Gott existiert, dann ist es m&ouml;glich,
              dass Gott notwendig existiert.<br>
              Wenn Gott aber in irgendeiner Welt notwendig existiert, existiert
            er in allen m&ouml;glichen Welten, auch in unserer.</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Benedikt Richter</font>            </p>
            <p><font face="Arial, Helvetica, sans-serif"><b><font size="2"><br>
              </font></b><font size="2">&copy; kath.de<b><br>
              </b></font></font><font size="2">F<font face="Arial, Helvetica, sans-serif">eedback
              bitte an redaktion@kath.de</font></font></p>
          </td>
          <td class="L12" width="30%">            <div align="right"> 
              <p align="right">
			  
			  
			 <script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
google_ad_width = 336;
google_ad_height = 280;
google_ad_format = "336x280_as";
google_ad_type = "text_image";
//2006-10-23: Lexikon Symbole
google_ad_channel = "8495253357";
//--></script>
<script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
			  
			  
			  <br>
                <br>
                <font size="2" face="Arial, Helvetica, sans-serif"><br>
                <br>
                </font></p>
              <p>&nbsp;</p>
              <p><br>
              </p>
            </div>
          </td>
          <td background="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxright.gif" width="8"><img src="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8"><img src="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxbottom.gif" colspan="2"><img src="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxbottom.gif" width="8" height="8" alt=""></td>
          <td width="8"><img src="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td width="100" align="left" valign="top">&nbsp;</td>
  </tr>
</table>
</body>
</html>
