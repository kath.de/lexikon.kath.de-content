<HTML><HEAD><TITLE>Menschwerdung Jesu</TITLE>
<META http-equiv=Content-Type content="text/html; charset=iso-8859-1"><LINK 
title=fonts href="kaltefleiter.css" type=text/css 
rel=stylesheet>
<META content="MSHTML 5.50.4134.600" name=GENERATOR></HEAD>
<BODY bgColor=#ffffff leftMargin=6 topMargin=6 marginheight="6" marginwidth="6">
<TABLE cellSpacing=0 cellPadding=6 width="100%" border=0>
  <TBODY>
  <TR>
    <TD vAlign=top align=left width=100> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b>Philosophie&amp;Theologie</b></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td> <?php include("logo.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10"> <?php include("az.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </TD>
    <TD vAlign=top rowSpan=2>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
        <TBODY>
        <TR vAlign=top align=left>
          <TD width=8><IMG height=8 alt="" 
            src="boxtopleftcorner.gif" width=8></TD>
          <TD background=boxtop.gif><IMG height=8 alt="" 
            src="boxtop.gif" width=8></TD>
          <TD width=8><IMG height=8 alt="" 
            src="boxtoprightcorner.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxtopleft.gif><IMG height=8 alt="" 
            src="boxtopleft.gif" width=8></TD>
          <TD bgColor=#e2e2e2> 
            <H1><font face="Arial, Helvetica, sans-serif">Menschwerdung Jesu</font></H1>
          </TD>
          <TD background=boxtopright.gif><IMG height=8 
            alt="" src="boxtopright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=13 alt="" src="boxdividerleft.gif" 
            width=8></TD>
          <TD background=boxdivider.gif><IMG height=13 
            alt="" src="boxdivider.gif" width=8></TD>
          <TD><IMG height=13 alt="" 
            src="boxdividerright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxleft.gif><IMG height=8 alt="" 
            src="boxleft.gif" width=8></TD>
          <TD class=L12>
            <P><STRONG><font face="Arial, Helvetica, sans-serif">Jesus von Nazareth
                macht eine Entwicklung durch</font></STRONG></P>
            <P><font face="Arial, Helvetica, sans-serif">Jesus von Nazareth wird
                im Glaubensbekenntnis als der ewige Sohn Gottes bekannt. Dieser
                Sohn, von Johannes &#8222;Logos&#8220; genannt,
              ist Mensch geworden, geboren aus der Jungfrau Maria. War er sich
              dessen, vielleicht schon als Kind, bewu&szlig;t? Oder gab es in
              ihm nicht nur ein k&ouml;rperliches, sondern auch ein geistiges
              Wachstum? Wie andere Kinder hat Jesus sprechen gelernt, in der
              Synagogenschule von Nazareth wurde er mit der Bibel der Juden vertraut
              und konnte als junger Mann mit den Schriftgelehrten &uuml;ber die
              richtige Auslegung einzelner Bibelstellen diskutieren. Hat er auch
              gelernt, sich als Sohn Gottes zu verstehen oder hatte er das von
              Anfang gewu&szlig;t? </font></P>
            <P><font face="Arial, Helvetica, sans-serif"><strong>Ausmalung des Lebens Jesu in den sog. apokryphen Evangelien</strong><br>
              In sog. apokryphen Evangelien, die nicht in den Bestand des Neuen
              Testaments aufgenommen wurden, wird mehr &uuml;ber die Kindheit
              Jesu berichtet als wir aus dem Lukasevangelium entnehmen k&ouml;nnen.
              Dem kleinen Jesus werden bereits Wundertaten zugeschrieben, ein &uuml;berlegenes
              Wissen und vieles andere, um ihn als den menschgewordenen Gott
              darzustellen. Auch in sp&auml;teren Jahrhunderten waren einzelne
              Theologen und religi&ouml;se Schriftsteller davon &uuml;berzeugt,
              da&szlig; schon der junge Jesus in einer direkten Schau Gottes
              lebte und selbst am Kreuz Gott direkt schaute.<br>
              Wie die apokryphen Evangelien wollen diese Vorstellungen das Besondere
              an Jesus herausstellen. Das G&ouml;ttliche des Sohnes Gottes mu&szlig; das
              Bestimmende sein. Entsprechen diese Vorstellungen aber dem, was
              im Neuen Testament &uuml;berliefert wird?</font></P>
            <P><font face="Arial, Helvetica, sans-serif"><strong>Hinweise in den Evangelien</strong><br>
              Die wenigen Hinweise, die sich in der Bibel &uuml;ber Jesus finden,
              besagen, da&szlig; er sich durch seine Wunder wie auch durch Worte
              als der <a href="sohn_gottes.php">Sohn Gottes</a> 
              zu erkennen gab. Zugleich geht die Bibel von einer pers&ouml;nlichen
              Entwicklung Jesu aus.<br>
              Lukas berichtet von einer Wallfahrt der Familie nach Jerusalem.
              Jesus ist 12 Jahre alt, d.h. im j&uuml;dischen Verst&auml;ndnis
              volles Mitglied der Gottesdienstgemeinschaft in der Synagoge. Die
              Eltern sind bereits auf dem R&uuml;ckweg, als sie feststellen m&uuml;ssen,
              da&szlig; Jesus auch nicht mit anderen Pilgern zur&uuml;ckkehrt,
              sondern in Jerusalem geblieben sein mu&szlig;. Dorthin zur&uuml;ckgekehrt
              finden sie ihn im Lehrgespr&auml;ch mit den Schriftgelehrten. Er
              kehrt dann mit seinen Eltern nach Nazareth zur&uuml;ck. Lukas fa&szlig;t
              seine Entwicklung so zusammen: &#8222;Jesus aber wuchs heran und
              seine Weisheit nahm zu, und er fand Gefallen bei Gott und den Menschen.&#8220; Kap
              3, 52<br>
              Der Hebr&auml;erbrief schreibt noch deutlicher:<br>
              Darum mu&szlig;te er (Jesus) in allem seinen Br&uuml;dern gleich
              sein, um ein barmherziger und treuer Hoherpriester vor Gott zu
              sein und die S&uuml;nden des Volkes zu s&uuml;hnen. Denn da er
              selbst in Versuchung gef&uuml;hrt wurde und gelitten hat, kann
              er denen helfen, die in Versuchung geraten (Kap. 3,11-18)<br>
              <br>
              <strong>Bewu&szlig;tsein Jesu als Sohn Gottes</strong><br>
              Wie war sich aber Jesus seiner <a href="gottessohn.php">Gottessohnschaft</a>
               bewu&szlig;t? Wenn ihm mit seinem menschlichen Bewu&szlig;tsein
              verborgen gewesen w&auml;re, da&szlig; er Sohn Gottes ist, dann
              w&auml;re der Mensch Jesus vom Sohn Gottes fremdgesteuert, ohne
              da&szlig; er es wu&szlig;te &#8211; und damit w&auml;re er unfrei.
              Die theologische Reflexion ist aber zu dem Ergebnis gekommen, da&szlig; Jesus
              nicht nur einen g&ouml;ttlichen, sondern auch einen <a href="monotheletismus_monergetismus.php">menschlichen
              Willen</a> hatte. Das schreibt der Autor
              des Hebr&auml;erbriefes ausdr&uuml;cklich. <br>
              In den Evangelien finden sich weitere Hinweise, wie Jesus sich
              seiner Sohnschaft bewu&szlig;t war. Er lebte in einer unmittelbaren
              Beziehung zu Gott, den er seinen Vater nennt. Darin zeigt sich
              viel unmittelbarer als es in Begriffen ausgedr&uuml;ckt werden
              kann, wie Jesus von Gott her lebt und da&szlig; er sich unmittelbar
              als Gottes Sohn versteht. Das Johannesevangelium entfaltet diese
              N&auml;he Jesu zu seinem Vater in den Gespr&auml;chen Jesu, die
              er mit seinen J&uuml;ngern im Abendmahlssaal f&uuml;hrt. Die Christen
              haben die Nachfolge Jesu so verstanden, da&szlig; sie in sein Verh&auml;ltnis
              zu seinem Vater aufgenommen werden und daher das &quot;Vater unser&quot;
              beten k&ouml;nnen.<br>
              Die Evangelien berichten an mehreren Stellen, da&szlig; Jesus sich
              zum Gebet zur&uuml;ckzieht, wie er am &Ouml;lberg in der Nacht
              seiner Gefangennahme seinen Vater anfleht. Bei Matth&auml;us findet
              sich ein kurzer Text, in dem Jesus sein Verh&auml;ltnis zum Vater
              in Worte fa&szlig;t:</font><font face="Arial, Helvetica, sans-serif"><br>
              &quot;Ich preise dich, Vater, Herr des Himmels und der Erde, weil du
              all das den Weisen und Klugen verborgen, den Unm&uuml;ndigen aber
              offenbart hast. Ja Vater, so hat es dir gefallen. Mir ist von meinem
              Vater alles &uuml;bergeben worden; niemand kennt den Sohn, nur
              der Vater, und niemand kennt den Vater, der Sohn und der, dem es
              der Sohn offenbaren will.&quot;<br>
              Matth&auml;us 11,25-27</font></P>
            <P><font face="Arial, Helvetica, sans-serif"><strong>Zitate<br>
            </strong></font><font face="Arial, Helvetica, sans-serif">Er (Jesus) war Gott gleich, <br>
              hielt aber nicht daran fest, wie Gott zu sein,<br>
              sondern ent&auml;u&szlig;erte sich und wurde wie ein Sklave,<br>
              und den Menschen gleich.<br>
              Sein Leben war das eines Menschen,<br>
              er erniedrigt sich und war gehorsam bis zum Tod,<br>
              bis zum Tod am Kreuz.<br>
              Darum hat ihn Gott &uuml;ber alle erhoben,<br>
              und ihm einen Namen verliehen, <br>
              der gr&ouml;&szlig;er ist als alle Namen,<br>
              damit alle im Himmel, auf der Erde und unter der Erde,<br>
              ihre Knie beugen vor dem Namen Jesu,<br>
              und jeder Mund bekennt:<br>
              Jesus Christus ist der Herr,<br>
              zur Ehre Gottes des Vaters.<br>
              Philipperbrief 2,6-11</font></P>            <p><font face="Arial, Helvetica, sans-serif">Aus dem Johannesevangelium<br>
              Der Vater liebt den Sohn und hat alles in seine Hand gegeben.
                  Wer an den Sohn glaubt, hat das ewige Leben; wer aber dem Sohn
                  nicht
                gehorcht, wird das Leben nicht sehen, sondern Gottes Zorn bleibt
                auf ihm.&quot; Kap. 3, 35-36<br>
                Ich bin der gute Hirt; ich kenne die Meinen und die Meinen kennen
              mich, wie mich der Vater kennt und ich den Vater kenne, und ich
              gebe mein Leben f&uuml;r meine Schafe.&quot;Kap.10,14-15<br>
              Und er erhob seine Augen zum Himmel und sprach: Vater, die Stunde
              ist da. Verherrliche deinen Sohn, damit der Sohn dich verherrlicht.
              Denn du hast ihm die Macht &uuml;ber alle Menschen gegeben, damit
              er allen, die du ihm gegeben hast, ewiges Leben schenkt. .... Ich
              habe dich auf der Erde verherrlicht und das Werk zu Ende gef&uuml;hrt,
              das du mir aufgetragen hast. Vater, verherrliche du mich jetzt
              bei dir mit der Herrlichkeit, die ich bei dir hatte, bevor die
              Welt war. Kap.17,1-2, 4-5</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Dann verlie&szlig; Jesus die Stadt und ging, wie er es gewohnt
              war, zum &Ouml;lberg; seine J&uuml;nger folgten ihm. Als er dort
              war, sagte er zu ihnen: Betet darum, da&szlig; ihr nicht in Versuchung
              geratet. Dann entfernte er sich von ihnen ungef&auml;hr einen Steinwurf
              weit, kniete nieder und betete: Vater, wenn du willst, nimm diesen
              Kelch von mir. Aber nicht mein, sondern dein Wille soll geschehen.
              Lukas 22,39-42</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Die Vorstellung, da&szlig; der Logos geschaffen ist und anstelle
              der Geistseele bei der Menschwerdung trat, so da&szlig; Jesus keine
              menschliche Seele hat, findet sich bei Exodius, der von 360 bis
              370 Patriarch von Konstantinopel war:<br>
              Wir glauben an den einen, den allein wahren Gott und Vater, die
              alleinige (g&ouml;ttliche) Natur, der ungezeugt und ohne Vater
              ist, ....<br>
              und an den einen Herrn, den Sohn, der gottesf&uuml;rchtig war,
              weil er den Vater heilig hielt. Er ist zwar der Erstgeborene, gewaltiger
              als die ganze Sch&ouml;pfung nach ihm, Erstgeborener ist er deshalb,
              weil er das am meisten herausragende und erste von allen Gesch&ouml;pfen
              ist. Er ist Fleisch geworden, nicht Mensch; denn er nahm keine
              menschliche Seele an, sondern wurde nur Fleisch, damit Gott sich
              durch das Fleisch den Menschen wie durch einen Vorhang hindurch
              offenbare. Es gab also nicht zwei Naturen, weil der Mensch nicht
              vollst&auml;ndig war, sondern statt der Seele Gott im Fleisch war.</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Cyrill von Jerusalem
                3313-387, zeigt auf, da&szlig; der Sohn Gottes wirklich Mensch geworden
                ist:<br>
  Er (Jesus) kam nicht wie durch eine R&ouml;hre durch die Jungfrau,
              sondern er hat aus ihr wahrhaft Fleisch angenommen ... Wenn n&auml;mlich
              die Menschwerdung eine Fiktion war, ist auch unsere Erl&ouml;sung
              nur eine Fiktion.<br>
              Katechesen 4.9</font></p>
            <p><font face="Arial, Helvetica, sans-serif">So tritt denn der Sohn
                Gottes in diese niedrige Welt ein und steigt von seinem himmlischen
                Thron herab, ohne dabei die Herrlichkeit
              seines Vaters zu verlassen. Er kommt zur Welt in einer neuen Ordnung
              und in einer neuen Geburt. In einer neuen Ordnung, denn der in
              dem Seinen (der g&ouml;ttlichen Natur) Unsichtbare ist in Unserem
              sichtbar geworden, und der Unbegreifliche wollte begriffen werden.
              Der vor aller Zeit Seiende hat in der Zeit begonnen zu sein. Der
              Herr des Alls hat Knechtsgestalt angenommen und so seine unerme&szlig;liche
              Majest&auml;t verh&uuml;llt. Der leidensunf&auml;hige Gott hat
              es nicht verschm&auml;ht, ein leidensf&auml;higer Mensch zu sein,
              und der Unsterbliche nicht, sich dem Gesetz des Todes zu unterwerfen. <br>
              Papst Leo I im Brief an Flavian von Konstantinopel 449</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Christus schuf sich
                die Mutter, als er beim Vater war, und als er geboren wurde aus
                der Mutter, blieb er im Vater. Wie sollte
              er aufh&ouml;ren, Gott zu sein, als er Mensch zu sein begann,
              der seiner Geb&auml;rerin gew&auml;hrte, nicht aufzuh&ouml;ren,
              Jungfrau zu sein, als sie gebar? Dadurch, da&szlig; das Wort Fleisch
              geworden ist, ist das Wort nicht untergegangen und auch nicht in
              Fleisch verwandelt worden, sondern das Fleisch kam zum Wort hinzu,
              damit es selbst nicht untergehe. Wie der Mensch aus Seele und Leib
              besteht, so ist Christus Gott und Mensch. Derselbe Gott, der auch
              Mensch ist, und der Gott ist, derselbe ist auch Mensch, nicht durch
              Vermischung der Naturen, sondern durch die Einheit der Person.<br>
              Augustinus Sermo 186, 1, um 411</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Friedrich Schleiermacher
                hat im 19. Jahrhundert einen neuen Zugang zur Person Jesu gesucht
                und die Entwicklung des Selbstbewu&szlig;tseins
              Jesu als Gott so erkl&auml;rt:<br>
              Da wir aber doch den Anfang des Lebens nie eigentlich begreifen &#8230; gilt
              dieses auch von seinem (Jesu) Gottesbewu&szlig;tsein, &#8230;welches
              zwar auch anderen ebensowenig als ihm irgendwann erst durch Erziehung
              eingefl&ouml;&szlig;t wird, sondern dessen Keim in allen schon
              urspr&uuml;nglich liegt, welches sich aber auch in ihm wie in allen
              erst allm&auml;hlich nach menschlicher Weise zum wirklich erscheinenden
              Bewu&szlig;tsein entwickeln mu&szlig;te, und vorher nur als Keim,
              wenngleich in gewissem Sinn immer als wirksame Kraft, vorhanden
              war. Zu der reinen Geschichtlichkeit der Person des Erl&ouml;sers
              geh&ouml;rt aber auch dieses, da&szlig; er sich nur in einer gewissen &Auml;hnlichkeit
              mit seinen Umgebungen, also im allgemeinen volkst&uuml;mlich, entwickeln
              konnte. Denn da Sinn und Verstand nur aus dieser ihn umgebenden
              Welt gen&auml;hrt wurden, und auch seine freie Selbstt&auml;tigkeit
              in dieser ihren bestimmten Ort hatte: so konnte sich auch sein
              Gottesbewu&szlig;tsein, wie urspr&uuml;nglich auch die h&ouml;here
              Kraft desselben sei, doch nur ausdr&uuml;cken und mitteilen in
              Vorstellungen, die er sich aus diesem Gebiet angeeignet hatte,
              und in Handlungen, welche in demselben ihrer M&ouml;glichkeit nach
              vorbestimmt waren<br>
              Der christliche Glaube, Aus dem Ersten Lehrst&uuml;ck &uuml;ber
              die Person Jesu, &sect; 93,39</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Paul Tillich &uuml;ber
                Jesus als den Menschen, der Gott repr&auml;sentiert:<br>
  Wenn darum der Christus als Mittler und Erl&ouml;ser erwartet wird,
              ist er keine dritte Wirklichkeit zwischen Gott und dem Menschen.
              Er ist derjenige, der Gott den Menschen gegen&uuml;ber repr&auml;sentiert.
              Er repr&auml;sentiert nicht den Menschen Gott gegen&uuml;ber. Er
              zeigt vielmehr, was Gott w&uuml;nscht, da&szlig; der Mensch sei.
              Er zeigt denen, die unter den Bedingungen der Existenz leben, was
              der Mensch essentiell ist und darum sein sollte. Es ist unangemessen
              und f&uuml;hrt zu einer falschen Christologie, wenn man sagt, da&szlig; der
              Mittler eine eigene ontologische Realit&auml;t neben Gott und Mensch
              sei. Nur ein Halb-Gott k&ouml;nnte das sein, der gleichzeitig ein
              Halb-Mensch w&auml;re. Solch ein drittes Wesen k&ouml;nnte weder
              Gott den Menschen gegen&uuml;ber repr&auml;sentieren, noch k&ouml;nnte
              es das wesenhafte Menschsein ausdr&uuml;cken. Aber der Mittler
              repr&auml;sentiert das wesenhafte Menschsein; und damit repr&auml;sentiert
              er Gott. Anders ausgedr&uuml;ckt: Er repr&auml;sentiert das Bild
              Gottes, das urspr&uuml;nglich im Menschen verk&ouml;rpert ist,
              aber er tut es unter den Bedingungen der Entfremdung zwischen Gott
              und Mensch.<br>
              Systematische Theologie, 1957, S. 103</font></p>
            <p><font size="2" face="Arial, Helvetica, sans-serif">Text: Eckhard
                Bieger S.J</font><font face="Arial, Helvetica, sans-serif">.<br>
                </font> &copy;<a href="http://www.kath.de"> <font face="Arial, Helvetica, sans-serif">www.kath.de</font></a></p>
            </TD>
          <TD background=boxright.gif><IMG height=8 alt="" 
            src="boxright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=8 alt="" src="boxbottomleft.gif" 
            width=8></TD>
          <TD background=boxbottom.gif><IMG height=8 alt="" 
            src="boxbottom.gif" width=8></TD>
          <TD><IMG height=8 alt="" src="boxbottomright.gif" 
            width=8></TD></TR></TBODY></TABLE></TD><td style='vertical-align:top;'>
 <script type="text/javascript"><!--
        google_ad_client = "pub-9537752113242914";
        google_ad_width = 336;
        google_ad_height = 280;
        google_ad_format = "336x280_as";
        google_ad_type = "text_image";
        //2006-10-23: Lexikon Symbole
        google_ad_channel = "8495253357";
        //--></script>
        <script type="text/javascript"
          src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
        </script>
</td>
</TR>
  <TR>
    <TD vAlign=top align=left>&nbsp; </TD>
  </TR></TBODY></TABLE></BODY></HTML>
