<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Gottesdienste im Fernsehen</title>
<meta name="title" content="Anti-Judaismus in der Passionsgeschichte des Johannes">
<meta name="author" content="kath.de">
<meta name="publisher" content="kath.de">
<meta name="copyright" content="kath.de">
<meta name="description" content="Anti-Judaismus in der Passionsgeschichte des Johannes">
<meta name="abstract" content="Anti-Judaismus in der Passionsgeschichte des Johannes">
<meta http-equiv="content-language" content="de">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="date" content="2006-00-02">
<meta name="robots" content="index,follow">
<meta name="revisit-after" content="30 days">
<meta name="revisit" content="after 30 days">
<meta name="DC.Title" content="Anti-Judaismus in der Passionsgeschichte des Johannes">
<meta name="DC.Creator" content="kath.de">
<meta name="DC.Contributor" content="N.N.">
<meta name="DC.Rights" content="kath.de">
<meta name="DC.Publisher" content="kath.de">
<meta name="DC.Date" content="2006-00-02">
<meta name="DC.Description" content="Anti-Judaismus in der Passionsgeschichte des Johannes", passion of christ, antijüdische Tendenzen, Hohepriester, Bund Gottes">
<meta name="DC.Language" content="de">
<meta name="DC.Type" content="Text">
<meta name="DC.Format" content="text/html">
<meta name="DC.Identifier" content="http://www.kath.de/kurs/medien_oeffentlichkeit/">
<meta name="keywords" lang="de" content="Anti-Judaismus in der Passionsgeschichte des Johannes">

<link rel="stylesheet" type="text/css" href="../../../../Projekte/Gott-lexikon/n%20eu/kaltefleiter.css" title="fonts">
</head>
<body bgcolor="#ffffff" leftmargin="6" topmargin="6" marginwidth="6" marginheight="6">
<table width="100%" border="0" cellspacing="0" cellpadding="6">
  <tr>
    <td width="100" align="left" valign="top"><table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left">
          <td width="8"><img src="../../../../Projekte/Gott-lexikon/n%20eu/boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="../../../../Projekte/Gott-lexikon/n%20eu/boxtop.gif"><img src="../../../../Projekte/Gott-lexikon/n%20eu/boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="../../../../Projekte/Gott-lexikon/n%20eu/boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="../../../../Projekte/Gott-lexikon/n%20eu/boxtopleft.gif"><img src="../../../../Projekte/Gott-lexikon/n%20eu/boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b>Theologie und Philosophie</b></td>
          <td background="../../../../Projekte/Gott-lexikon/n%20eu/boxtopright.gif"><img src="../../../../Projekte/Gott-lexikon/n%20eu/boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="../../../../Projekte/Gott-lexikon/n%20eu/boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="../../../../Projekte/Gott-lexikon/n%20eu/boxdivider.gif"><img src="../../../../Projekte/Gott-lexikon/n%20eu/boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="../../../../Projekte/Gott-lexikon/n%20eu/boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="../../../../Projekte/Gott-lexikon/n%20eu/boxleft.gif"><img src="../../../../Projekte/Gott-lexikon/n%20eu/boxleft.gif" width="8" height="8" alt=""></td>
          <td>
            <?php include("logo.html"); ?>
          </td>
          <td background="../../../../Projekte/Gott-lexikon/n%20eu/boxright.gif"><img src="../../../../Projekte/Gott-lexikon/n%20eu/boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="../../../../Projekte/Gott-lexikon/n%20eu/boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="../../../../Projekte/Gott-lexikon/n%20eu/boxbottom.gif"><img src="../../../../Projekte/Gott-lexikon/n%20eu/boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="../../../../Projekte/Gott-lexikon/n%20eu/boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
    <td rowspan="2" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left">
          <td width="8"><img src="../../../../Projekte/Gott-lexikon/n%20eu/boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td background="../../../../Projekte/Gott-lexikon/n%20eu/boxtop.gif"><img src="../../../../Projekte/Gott-lexikon/n%20eu/boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="../../../../Projekte/Gott-lexikon/n%20eu/boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="../../../../Projekte/Gott-lexikon/n%20eu/boxtopleft.gif"><img src="../../../../Projekte/Gott-lexikon/n%20eu/boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2">
            <h1> Anti-Judaismus in der Passionsgeschichte des Johannes?</h1>
          </td>
          <td background="../../../../Projekte/Gott-lexikon/n%20eu/boxtopright.gif"><img src="../../../../Projekte/Gott-lexikon/n%20eu/boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="../../../../Projekte/Gott-lexikon/n%20eu/boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="../../../../Projekte/Gott-lexikon/n%20eu/boxdivider.gif"><img src="../../../../Projekte/Gott-lexikon/n%20eu/boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="../../../../Projekte/Gott-lexikon/n%20eu/boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="../../../../Projekte/Gott-lexikon/n%20eu/boxleft.gif"><img src="../../../../Projekte/Gott-lexikon/n%20eu/boxleft.gif" width="8" height="8" alt=""></td>
          <td class="L12"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td valign="top" class="L12"><p class=MsoNormal><font face="Arial, Helvetica, sans-serif"><br>
                 Im Jahre 2004
                    erregte <strong>Mel Gibsons</strong> Film &#8222;The Passion
                    of the Christ&#8220; internationales Aufsehen. Ihm wurden
                    neben &uuml;bertriebener Gewaltdarstellung auch anti-j&uuml;dische
                    Tendenzen vorge-worfen. Gibson berief sich besonders auf
                    die Passion Jesu im Johannesevangelium.<br>
                    <br>
                    Gerade beim genaueren Lesen dieser Textstelle wird diese
                    Problematik deutlich: Besonders im Verh&ouml;r Jesu vor Pilatus
                    (18,28-19,18) scheinen anti-j&uuml;dische Tendenzen vorzukommen.
                    W&auml;hrend Pilatus eher unentschlossen wirkt und es so
                    scheint als sei er nur ein Mittel der Juden zu Durchsetzung
                    ihrer Interessen, sind es die Juden, die schreiend den Kreuzestod
                    Jesu fordern (Vgl. 19,15: &#8222;Hinweg, hinweg, kreuzige
                    ihn!&#8220;). Wie ist dieser Antijudaismus zu erkl&auml;-ren
                    und wie k&ouml;nnen wir heute damit umgehen?<br>
                    <br>
                    Um dieses Problem zu verstehen, ist es notwendig, auf die
                    damalige Leserschaft einzugehen, die christliche Gemeinde,
                    in deren Kontext der Text geschrieben worden ist. Auch ein
                    Blick auf die Entstehungsgeschichte des Johannesevangeliums
                    insgesamt bietet sich in diesem Zusammenhang an. In der Forschung
                    besteht ein Konsens dar&uuml;ber, dass das Johannes-Evangelium
                    nicht auf einen einzigen Verfasser zur&uuml;ckgef&uuml;hrt
                    werden kann. Man geht davon aus, dass es eine l&auml;ngere
                    Entstehungsgeschichte hatte, jedoch besteht Uneinigkeit dar&uuml;ber,
                    wie genau dieser Prozess sich vollzogen haben soll. Man geht
                    im Allgemeinen davon aus, dass zu Beginn umfangreiche Traditionen &uuml;ber
                    das Leben Jesus existierten, welche sp&auml;ter zusammengef&uuml;gt
                    wurden. Die anf&auml;nglichen Traditionen entstanden im johanneischen
                    Gemeindekreis. Diese Traditionen wurden von einem Theologen,
                    welcher der johanneischen Schule angeh&ouml;rte, zur Gattung
                    Evangelium geb&uuml;ndelt. In einer dritten Phase erfolgte
                    ein gestufter Aneignungsprozess durch die kirchliche Redaktion.<br>
                    F&uuml;r das Verst&auml;ndnis des Antijudaismus ist besonders
                    der geschichtliche Hintergrund der johanneischen Gemeinde
                    besonders relevant. Die starken antij&uuml;dischen Tendenzen,
                    mit welchen der Verfasser arbeitet, werden nur durch diese
                    Gemeindegeschichte verst&auml;ndlich: Die johanneische Gemeinde
                    bildete zun&auml;chst eine Art j&uuml;dische Sondergruppe
                    innerhalb des ortho-doxen Judentums. Sie nahm auf Grund ihres
                    Bekenntnisses zu Jesus als dem Messias eine au&szlig;erordentliche
                    Stellung ein. Mit der Zeit kam es zum Bruch mit dem Judentum,
                    was sowohl auf Auseinandersetzungen &uuml;ber die Christologie
                    der Gemeinde zur&uuml;ckzuf&uuml;hren, jedoch auch ein Resultat
                    der Aufnahme von Halb- und Nichtjuden in den johanneischen
                    Kreis ist, da dies von den Juden missbilligt wurde. Die johanneische
                    Gemeinde wurde nun gegen ihren Willen aus der Synagoge ausgeschlossen,
                    verlie&szlig; die j&uuml;disch dominierte Umgebung und entwickelte
                    eine Ethik der defensiven Weltdistanz. Von diesem Horizont
                    aus erscheinen verschiedene Aspekte in einem neuen Licht.
                    So ist es auf den Bruch mit der j&uuml;dischen Gemeinde zur&uuml;ck-zuf&uuml;hren,
                    dass die johanneische Schule &#8222;die Juden&#8220; hier
                    in einem negativen Licht erscheinen l&auml;sst und ihren
                    Hass gegen Jesus, der schlie&szlig;lich zu seiner Kreuzigung
                    f&uuml;hrt, besonders betont, um so ihrer eigenen Entt&auml;uschung &uuml;ber
                    den Synagogenausschluss Ausdruck zu geben. In der Perikope
                    wird dies besonders an der Stelle deutlich an der Jesus sagt,
                    dass der, der ihn Pilatus &uuml;bergeben hat gr&ouml;&szlig;ere
                    Schuld habe (Vgl. 19,11d). Obwohl dieser Antijudaismus, welcher
                    im Johannes-Evangelium noch an mehreren Stellen auftaucht,
                    in keiner Weise gerechtfertigt wer-den kann, erkl&auml;rt
                    sich hieran doch seine Verursachung. <br>
                    Wir k&ouml;nnen die Passion im Johannesevangelium also nicht
                    richtig verstehen, wenn wir uns diesen geschichtlichen Hintergrund
                    nicht vor Augen f&uuml;hren. Der historische Kontext er&ouml;ffnet
                    auch einen weiteren Blickwinkel im Zusammenhang mit der Frage,
                    wer genau mit &#8222;den Ju-den&#8220; gemeint ist? Der Terminus &#8222;hoi
                    Ioudaioi&#8220; war
                    keine Selbstbezeichnung des j&uuml;dischen
                    Volkes, sondern eine r&ouml;mische Benennung. In der Forschung
                    geht man davon aus, dass im Verh&ouml;r vor Pilatus mit diesem
                    Begriff nicht das j&uuml;dische Volk, sondern die Hohenpriester,
                    die Gegner Jesu bezeichnet werden. Es ist wichtig, in diesem
                    Zusammenhang darauf zu verwei-sen, dass der Bund Gottes mit
                    dem j&uuml;dischen Volk nicht aufgek&uuml;ndigt worden ist,
                    wie es uns Substitutionsmodelle des latenten Markionismus
                    weismachen wollen. Darin wird die Kirche als das neue Israel
                    propagiert, w&auml;hrend die Beziehung zwischen Gott und
                    den Juden rela-tiviert wird. Doch dieser besondere Bund zwischen
                    Gott und dem j&uuml;dischen Volk ist ungebrochen, worauf
                    das Alte Testament verweist, indem es den Gnadenbund betont.
                    Im Buch Genesis spricht Gott zu Noach: &quot;Aber mit dir
                    will ich meinen Bund aufrichten&quot; (6,18). Dieser Bund
                    mit dem j&uuml;dischen Volk ist somit ein unk&uuml;ndbarer
                    Bund, da er von Gott gesetzt wurde.<br>
                  </font></P>
                  <p class=MsoNormal><font face="Arial, Helvetica, sans-serif">Claudia Schwarz<br>
                      <a href="www.morgenlandfahrerin.blogspot.com%20" target="_blank">www.morgenlandfahrerin.blogspot.com </a> </font></P>
                  <p class=MsoNormal><font face="Arial, Helvetica, sans-serif">&copy; <font size="2">www.kath.de</font></font></P></td>
                <td valign="top"><script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
google_ad_width = 160;
google_ad_height = 600;
google_ad_format = "160x600_as";
google_ad_type = "text_image";
//2007-09-05: Bildung und Erziehung
google_ad_channel = "3955521420";
google_ui_features = "rc:6";
//-->
            </script>
                    <script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
              </script>
                </td>
              </tr>
            </table>
            <p class=MsoNormal>&nbsp;</P>
          <td background="../../../../Projekte/Gott-lexikon/n%20eu/boxright.gif"><img src="../../../../Projekte/Gott-lexikon/n%20eu/boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="../../../../Projekte/Gott-lexikon/n%20eu/boxbottomleft.gif" width="8" height="8" alt=""></td>
         <td background="../../../../Projekte/Gott-lexikon/n%20eu/boxbottom.gif"><img src="../../../../Projekte/Gott-lexikon/n%20eu/boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="../../../../Projekte/Gott-lexikon/n%20eu/boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td align="left" valign="top"><table width="216" border="0" cellpadding="0" cellspacing="0">
       <tr valign="top" align="left">
          <td width="8"><img src="../../../../Projekte/Gott-lexikon/n%20eu/boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="../../../../Projekte/Gott-lexikon/n%20eu/boxtop.gif"><img src="../../../../Projekte/Gott-lexikon/n%20eu/boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="../../../../Projekte/Gott-lexikon/n%20eu/boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="../../../../Projekte/Gott-lexikon/n%20eu/boxtopleft.gif"><img src="../../../../Projekte/Gott-lexikon/n%20eu/boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="../../../../Projekte/Gott-lexikon/n%20eu/boxtopright.gif"><img src="../../../../Projekte/Gott-lexikon/n%20eu/boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="../../../../Projekte/Gott-lexikon/n%20eu/boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="../../../../Projekte/Gott-lexikon/n%20eu/boxdivider.gif"><img src="../../../../Projekte/Gott-lexikon/n%20eu/boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="../../../../Projekte/Gott-lexikon/n%20eu/boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="../../../../Projekte/Gott-lexikon/n%20eu/boxleft.gif"><img src="../../../../Projekte/Gott-lexikon/n%20eu/boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10">
            <?php include("az.html"); ?>
          </td>
          <td background="../../../../Projekte/Gott-lexikon/n%20eu/boxright.gif"><img src="../../../../Projekte/Gott-lexikon/n%20eu/boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="../../../../Projekte/Gott-lexikon/n%20eu/boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="../../../../Projekte/Gott-lexikon/n%20eu/boxbottom.gif"><img src="../../../../Projekte/Gott-lexikon/n%20eu/boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="../../../../Projekte/Gott-lexikon/n%20eu/boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</body>
</html>
