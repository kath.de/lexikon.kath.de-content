<HTML><HEAD><TITLE>Gottesknecht</TITLE>
<META http-equiv=Content-Type content="text/html; charset=iso-8859-1"><LINK 
title=fonts href="kaltefleiter.css" type=text/css 
rel=stylesheet>
<META content="MSHTML 5.50.4134.600" name=GENERATOR></HEAD>
<BODY bgColor=#ffffff leftMargin=6 topMargin=6 marginheight="6" marginwidth="6">
<TABLE cellSpacing=0 cellPadding=6 width="100%" border=0>
  <TBODY>
  <TR>
    <TD vAlign=top align=left width=100> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b>Philosophie&amp;Theologie</b></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td> <?php include("logo.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10"> <?php include("az.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </TD>
    <TD vAlign=top rowSpan=2>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
        <TBODY>
        <TR vAlign=top align=left>
          <TD width=8><IMG height=8 alt="" 
            src="boxtopleftcorner.gif" width=8></TD>
          <TD background=boxtop.gif><IMG height=8 alt="" 
            src="boxtop.gif" width=8></TD>
          <TD width=8><IMG height=8 alt="" 
            src="boxtoprightcorner.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxtopleft.gif><IMG height=8 alt="" 
            src="boxtopleft.gif" width=8></TD>
          <TD bgColor=#e2e2e2> 
            <H1><font face="Arial, Helvetica, sans-serif">Gottesknecht</font></H1>
          </TD>
          <TD background=boxtopright.gif><IMG height=8 
            alt="" src="boxtopright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=13 alt="" src="boxdividerleft.gif" 
            width=8></TD>
          <TD background=boxdivider.gif><IMG height=13 
            alt="" src="boxdivider.gif" width=8></TD>
          <TD><IMG height=13 alt="" 
            src="boxdividerright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxleft.gif><IMG height=8 alt="" 
            src="boxleft.gif" width=8></TD>
          <TD align="right" class=L12>
            <P align="left"><STRONG><font face="Arial, Helvetica, sans-serif">Eine Erkl&auml;rung
                  f&uuml;r die Hinrichtung Jesu aus dem Alten Testament</font></STRONG></P>
            <P align="left"><font face="Arial, Helvetica, sans-serif">Die J&uuml;nger
                hatten sich Jesus angeschlossen, weil sie in ihm den<a href="messias_christus.php"> Messias</a>,
                erkannt hatten, den das Volk Israel erwartete. Die Wunder, die
                Jesus wirkte und seine &uuml;berzeugende
              Predigt hatten best&auml;tigt, da&szlig; er im Namen Gottes aufgetreten
              konnte. Als Jesus zum bevorstehenden <a href="http://www.kath.de/Kirchenjahr/palmsonntag.php">Paschafest</a> in Jerusalem einzog
              und die Menschen ihm zujubelten, konnten seine Anh&auml;nger eigentlich
              damit rechnen, da&szlig; er das Reich Gottes, von dem er oft gesprochen
              hatte, aufrichten werde. Statt dessen wurde er an die R&ouml;mer,
              also Heiden, ausgeliefert und von diesen auf die erniedrigenste
              Form, durch das <a href="http://www.kath.de/Kirchenjahr/karfreitag.php">Kreuz</a>, hingerichtet. Die J&uuml;nger mu&szlig;ten
              sich fragen, ob Gott diesen Messias im Stich gelassen hatte. Von
              zwei J&uuml;ngern, die von Jerusalem weggingen, wird berichtet,
              da&szlig; sie sehr betr&uuml;bt und entt&auml;uscht waren. Durch
              die Begegnung mit dem Auferstandenen kamen zuerst die Frauen und
              dann auch die Apostel zum Glauben, da&szlig; Jesus lebt. Als Juden
              mu&szlig;ten sich die J&uuml;nger erkl&auml;ren k&ouml;nnen, warum
              Jesus leiden mu&szlig;te, denn in vielen Psalmen und Berichten
              der Bibel wird immer wieder betont, da&szlig; Gott den Gerechten
              aus der Hand seiner Feinde rettet. Die ersten Christen schauten
              in ihrer Bibel nach, weil sie als gl&auml;ubige Juden nach dem
              Willen Gottes suchten. Da die Evangelien noch nicht geschrieben
              waren, stand ihnen nur die j&uuml;dische Bibel zur Verf&uuml;gung.</font></P>
            <P align="left"><font face="Arial, Helvetica, sans-serif"><strong>Die Verurteilung Jesu war kein Versehen</strong><br>
              Versuche, den Tod Jesus als ein Versehen der j&uuml;dischen Obrigkeit
              zu deuten, konnten f&uuml;r die junge christliche Gemeinde nicht &uuml;berzeugen,
              denn sie wurden ja selbst verfolgt, weil sie sich zu Jesus als
              Messias bekannten. (Messias hei&szlig;t der Gesalbte, griechisch
              Christos) Da sie aber keinen anderen Gott verehrten als die Juden,
              mu&szlig;ten sie eine Kontinuit&auml;t erkennen k&ouml;nnen. </font></P>
            <P align="left"><font face="Arial, Helvetica, sans-serif"><strong>Eine Antwort im Buch Jesaja</strong><br>
              Im Prophetenbuch des Jesaja finden sich Texte, die vom Leiden des
              Gerechten sprechen und ausdr&uuml;cklich sein Leiden als S&uuml;hne
              verstehen. Es sind die Gottesknechtslieder, die heute noch in der
              Karwoche gelesen werden. Das vierte dieser Lieder geh&ouml;rt in
              de Karfreitagsliturgie. (s. u.) In diesem Text finden die Christen
              eine Antwort, warum der Messias leiden mu&szlig;te. Nicht Gott
              verurteilt ihn zum Kreuz, sondern das B&ouml;se wird durch den
              Gerechten provoziert und sucht, ihn zu vernichten. Dies kann psychologisch
              durch den <a href="suendenbock.php">S&uuml;ndenbockmechanismus</a> erkl&auml;rt werden.            </font></P>
            <P align="left"><font face="Arial, Helvetica, sans-serif">Aus dem vierten Lied vom Gottesknecht<br>
              Seht, mein Knecht hat Erfolg, er wird gro&szlig; sein und hoch
              erhaben.<br>
              Viele haben sich &uuml;ber ihn entsetzt, denn er sah entstellt
              aus, nicht wie ein Mensch, seine Gestalt war nicht mehr die eines
              Menschen.<br>
              Jetzt aber setzt er viele V&ouml;lker in Staunen, K&ouml;nige m&uuml;ssen
              vor ihm verstummen.<br>
              .........<br>
              Wer hat geglaubt, was uns berichtet wurde?<br>
              Die Hand des Herrn - wer hat ihr Wirken erkannt?<br>
              Vor den Augen des Herrn wuchs er auf wie ein junger Spro&szlig;,
              wie der Trieb einer Wurzel aus trockenem Boden.<br>
              Er hatte keine sch&ouml;ne und edle Gestalt, und niemand von uns
              blickte ihn an.<br>
              Er sah nicht so aus, da&szlig; er unser Gefallen erregte.<br>
              Er wurde verachtet und von den Menschen gemieden,<br>
              ein Mann voller Schmerzen, mit der Krankheit vertraut.<br>
              Wie ein Mensch, vor dem man das Gesicht verh&uuml;llt,<br>
              war er bei uns verfemt und verachtet.<br>
              Aber er hat unsere Krankheiten getragen<br>
              und unsere Schmerzen auf sich genommen.<br>
              Wir meinten, er sei vom Unheil getroffen, von Gott gebeugt und
              geschlagen. <br>
              Doch er wurde durchbohrt wegen unserer Verbrechen,<br>
              wegen unserer S&uuml;nden mi&szlig;handelt.<br>
              Weil die Strafe auf ihm lag, sind wir gerettet,<br>
              .........<br>
              Der Herr warf all unsere S&uuml;nden auf ihn. <br>
              Er wurde geplagt und niedergedr&uuml;ckt, aber er tat seinen Mund
              nicht auf.<br>
              Wie ein Lamm, das man wegf&uuml;hrt, um es zu schlachten,<br>
              und wie ein Schaf, das verstummt, wenn man es schert,<br>
              so tat auch er seinen Mund nicht auf. <br>
              .....<br>
              Bei den Gottlosen gab man ihm sein Grab, bei den Verbrechern seine
              Ruhest&auml;tte,<br>
              obwohl er kein Unrecht getan hat und aus seinem Mund kein unwahres
              Wort kam. <br>
              Doch der Herr fand Gefallen an seinem mi&szlig;handelten (Knecht),<br>
              er rettete den, der sein Leben als S&uuml;hneopfer hingab.<br>
              ....... <br>
              Nachdem er so vieles ertrug, erblickt er wieder das Licht<br>
              und wird erf&uuml;llt von Erkenntnis.<br>
              Mein Knecht ist gerecht, darum macht er viele gerecht;<br>
              er nimmt ihre Schuld auf sich. <br>
              ......<br>
              denn er gab sein Leben hin und wurde zu den Verbrechern gerechnet.<br>
              Er trug die S&uuml;nden von vielen und trat f&uuml;r die Schuldigen
              ein. <br>
              Jesaja 52,13-53,12 </font></P>
            <p align="left"><font face="Arial, Helvetica, sans-serif">Vgl. auch die anderen Gottesknechtslieder (Jesaia 42,1-9; 49,1-9;
              50,4-9). <br>
            </font></p>
            <P align="left"><font size="2" face="Arial, Helvetica, sans-serif">Text: Eckhard Bieger S.J.</font> </P>
            <P align="left"><font face="Arial, Helvetica, sans-serif">&copy;<a href="http://www.kath.de"> www.kath.de</a></font></P>
            </TD>
          <TD background=boxright.gif><IMG height=8 alt="" 
            src="boxright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=8 alt="" src="boxbottomleft.gif" 
            width=8></TD>
          <TD background=boxbottom.gif><IMG height=8 alt="" 
            src="boxbottom.gif" width=8></TD>
          <TD><IMG height=8 alt="" src="boxbottomright.gif" 
            width=8></TD></TR></TBODY></TABLE></TD><td style='vertical-align:top;'>
 <script type="text/javascript"><!--
        google_ad_client = "pub-9537752113242914";
        google_ad_width = 336;
        google_ad_height = 280;
        google_ad_format = "336x280_as";
        google_ad_type = "text_image";
        //2006-10-23: Lexikon Symbole
        google_ad_channel = "8495253357";
        //--></script>
        <script type="text/javascript"
          src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
        </script>
</td>
</TR>
  <TR>
    <TD vAlign=top align=left>&nbsp; </TD>
  </TR></TBODY></TABLE></BODY></HTML>
