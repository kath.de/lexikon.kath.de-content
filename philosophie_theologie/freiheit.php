<HTML><HEAD><TITLE>Freiheit</TITLE>
<META http-equiv=Content-Type content="text/html; charset=iso-8859-1"><LINK 
title=fonts href="kaltefleiter.css" type=text/css 
rel=stylesheet>
<META content="MSHTML 5.50.4134.600" name=GENERATOR></HEAD>
<BODY bgColor=#ffffff leftMargin=6 topMargin=6 marginheight="6" marginwidth="6">
<TABLE cellSpacing=0 cellPadding=6 width="100%" border=0>
  <TBODY>
  <TR>
    <TD vAlign=top align=left width=100> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b>Philosophie&amp;Theologie</b></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td> <?php include("logo.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10"> <?php include("az.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </TD>
    <TD vAlign=top rowSpan=2>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
        <TBODY>
        <TR vAlign=top align=left>
          <TD width=8><IMG height=8 alt="" 
            src="boxtopleftcorner.gif" width=8></TD>
          <TD background=boxtop.gif><IMG height=8 alt="" 
            src="boxtop.gif" width=8></TD>
          <TD width=8><IMG height=8 alt="" 
            src="boxtoprightcorner.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxtopleft.gif><IMG height=8 alt="" 
            src="boxtopleft.gif" width=8></TD>
          <TD bgColor=#e2e2e2> 
            <H1><font face="Arial, Helvetica, sans-serif">Freiheit</font></H1>
          </TD>
          <TD background=boxtopright.gif><IMG height=8 
            alt="" src="boxtopright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=13 alt="" src="boxdividerleft.gif" 
            width=8></TD>
          <TD background=boxdivider.gif><IMG height=13 
            alt="" src="boxdivider.gif" width=8></TD>
          <TD><IMG height=13 alt="" 
            src="boxdividerright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxleft.gif><IMG height=8 alt="" 
            src="boxleft.gif" width=8></TD>
          <TD class=L12>
            <P><STRONG><font face="Arial, Helvetica, sans-serif">Gegen&uuml;ber unserer
                  Freiheit sind wir nicht frei</font></STRONG></P>
            <p><font face="Arial, Helvetica, sans-serif">Die Freiheit zeichnet
                den Menschen aus. Sein Leben ist nicht wie bei Tieren von Instinkten
                geleitet, vielmehr steuert der Mensch
              sein Leben selbst durch seine <a href="freiheit_entscheidung.php">Entscheidungen</a>.  Die Freiheit ist
                so mit der Person des einzelnen verbunden, da&szlig; er sich
                nicht von seiner Freiheit einfach distanzieren kann, es sei denn,
                er wird krank oder ist durch eine
              Hirnverletzung so eingeschr&auml;nkt, da&szlig; er nicht mehr &uuml;ber
              sich verf&uuml;gen kann. Die Freiheit ist auch nicht dadurch abzustreifen,
              da&szlig; der einzelne andere &uuml;ber sein Leben entscheiden
              l&auml;&szlig;t. Tut er das, ist seine Freiheit im Spiel, denn
              es ist seine Entscheidung, nicht selbst, sondern andere entscheiden
              zu lassen. Auch derjenige, der nicht entscheidet, tut das nicht
              ohne seine Freiheit, er oder sie entscheiden n&auml;mlich, da&szlig; das
              eigene Lebensschiff auf dem Kurs weiterfahren soll, auf dem es
              bereits unterwegs ist. Die eigene Freiheit verlangt sozusagen von
              jedem, da&szlig; er selbst den Kurs seines Lebens bestimmt. Mit
              der Freiheit verf&uuml;gt der einzelne &uuml;ber sein Leben.<br>
              Da keiner seiner Freiheit entkommt, ist niemand gegen&uuml;ber
              seiner Freiheit frei. Die Freiheit hat etwas unerbittliches, sie
              fordert sich selbst ein. Da&szlig; jemand vor dieser Forderung
              der Freiheit zur&uuml;ckschreckt, ist verst&auml;ndlich, denn mit
              der eigenen Freiheit &uuml;bernimmt der einzelne die Verantwortung
               f&uuml;r die Entscheidungen,
              die er bzw. sie getroffen hat. <br>
              Freiheit ist abh&auml;ngig von den M&ouml;glichkeiten, die sich
              bieten. In einer Gesellschaft, in der viel vorgegeben ist, ist
              die Freiheit nicht so gefordert wie wenn eine Gesellschaft dem
              einzelnen viele Entscheidungen offen l&auml;&szlig;t &#8211; freie
              Berufswahl, freie Partnerwahl, viele Ausbildungsg&auml;nge, Religionsfreiheit
              k&ouml;nnen die Freiheit sogar als Last erscheinen lassen, denn
              dem einzelnen werden kaum Entscheidungen abgenommen. Je mehr der
              einzelne entscheiden kann, desto mehr sind er oder sie f&uuml;r
              das Gelingen  ihres
              Lebens verantwortlich. </font></p>
            <p><font face="Arial, Helvetica, sans-serif">Eckhard Bieger S.J.<br>
            </font></p>
            <P><font face="Arial, Helvetica, sans-serif"><a href="http://www.kathshop.de/wwwkathde/product_info.php?cPath=&products_id=195&anbieter=17&page=&">Buchtipp</a></font></P>
            <P>&copy;<a href="http://www.kath.de"> <font face="Arial, Helvetica, sans-serif">www.kath.de</font></a></P>
            </TD>
          <TD background=boxright.gif><IMG height=8 alt="" 
            src="boxright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=8 alt="" src="boxbottomleft.gif" 
            width=8></TD>
          <TD background=boxbottom.gif><IMG height=8 alt="" 
            src="boxbottom.gif" width=8></TD>
          <TD><IMG height=8 alt="" src="boxbottomright.gif" 
            width=8></TD></TR></TBODY></TABLE></TD><td style='vertical-align:top;'>
            <p><a href="http://www.update-seele.de" target="_blank"><img src="banner-update-seele-02.jpg" width="286" height="70" border="0"></a> </p>
            <p>
              <script type="text/javascript"><!--
        google_ad_client = "pub-9537752113242914";
        google_ad_width = 336;
        google_ad_height = 280;
        google_ad_format = "336x280_as";
        google_ad_type = "text_image";
        //2006-10-23: Lexikon Symbole
        google_ad_channel = "8495253357";
        //--></script>
              <script type="text/javascript"
          src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
                  </script>
                        </p></td>
</TR>
  <TR>
    <TD vAlign=top align=left>&nbsp; </TD>
  </TR></TBODY></TABLE></BODY></HTML>
