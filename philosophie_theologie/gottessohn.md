---
title: Gottessohn
author: 
tags: 
created_at: 
images: []
---


# **Hyios Theou, Filius Dei*
# **Jesus hat eine besondere Nähe zu Gott. Dazu nur zwei Zitate aus dem Neuen Testament. Als Jesus sich von Johannes taufen ließ, „sah er, daß der Himmel sich öffnete und der Geist wie eine Taube auf ihn herabkam. Und eine Stimme aus dem Himmel sprach: Du bist mein geliebter Sohn, an dir habe ich Gefallen gefunden.“ Markus 1,11. **
 Jesus nennt Gott seinen Vater, nicht nur im Vater unser. „Mir ist von meinem Vater alles übergeben worden, niemand kennt den Sohn, nur der Vater, und niemand kennt den Vater, nur der Sohn und der, dem es der Sohn offenbaren will.“ (Matthäus 11,27) Was soll das genau heißen? Bezeichnet sich Jesus als wirklichen Sohn, der mehr ist als ein besonders geliebtes Geschöpf Gottes oder ist er von Gott adoptiert, wie es z.B. von jüdischen Königen im Krönungsritual heißt: „Mein Sohn bist du, heute habe ich dich gezeugt.“ Dieser Vers Psalm 2 besagt, daß mit der Inthronisation Gott den König zu seinem Sohn erhebt. Als Jesus lebte und predigte, entstand bereits die Frage. Er konfrontierte die Menschen nicht nur mit seiner Predigt und heilte sie – er sprach ihnen die Vergebung der Sünden zu. Als er das öffentlich bei der Heilung eines Gelähmten sagte, stutzen die jüdischen Schriftgelehrten: „Wie kann dieser Mensch so reden? Er lästert Gott. Wer kann Sünden vergeben außer dem einen Gott?“ Markus 2,7 Hingerichtet wurde Jesus wegen Gotteslästerung, weil er im Verhör auf die Frage „Bist du der Messias, der Sohn des lebendigen Gottes?“ antwortete: „Du hast es gesagt.“ Matthäus 26,63***

 Im griechischen Kulturraum konnte die Frage noch präziser gestellt werden. Wird Jesus nur als Sohn Gottes bezeichnet oder ist er nach seiner „Natur“ nach seinem „Wesen“ wirklich Gott? Die Frage konnte nicht einfach unentschieden beiseite gelegt werden, denn nur wenn dieser Jesus von Nazareth wirklich Gott ist, konnte er ein für alle mal die Menschen erlöst haben. Ansonsten wäre er wie die vielen Priester und Propheten gewesen, die in ihrer Zeit gewirkt haben, aber nach ihrem Tod Nachfolger brauchten, weil ihre Predigt und die von ihnen dargebrachten Opfer nicht endgültig waren.  

# **Unzulängliche Antworten ****
 Eine Antwort auf die Frage besteht darin, Jesus als einen Propheten zu  bezeichnen, vielleicht als den wichtigsten - aber nicht als wirklichen Sohn Gottes. Die ***[Ebioniten](christologische_streitigkeiten.php)  und andere vertraten diese Auffassung. Die Juden hatten vor den Griechen die Vielgötterei überwunden. Die Griechen waren nicht zuletzt durch philosophische Überlegungen zu der Überzeugung gekommen, daß es nicht mehrere Götter geben kann, wenn man von Gott spricht. Für das griechische Denken war es eine große Erkenntnis gewesen, daß Gott nur einer sein kann, denn nur wenn etwas in sich eins und unteilbar ist, kann es das höchste Wesen sein. So konnten sie den Polytheismus der alten griechischen Götterwelt überwinden. Denn wenn es mehrere Götter gibt, dann sind sie nicht wirklich göttlich. Das war auch die Position des Priesters ***[Arius.](arianismus.php)              Wie für das griechische Denken heißt für ihn Vielheit Mangel an Vollkommenheit. Was man teilen kann, kann nicht an der obersten Stelle im Kosmos stehen. Das Eine kann Ursprung des Vielen sein. Dann bleibt das Viele immer unterhalb des Einen. Vielheit hat die Tendenz zu zerfallen. Wenn in Gott schon Vielheit vorgefunden wird, dann wird das Göttliche auch einmal zerfallen, so wie die Gestalten, die der Mensch kennt. Sie zerfallen alle, auch der menschliche Leib. Deshalb braucht der Leib etwas, das nur „eins“ ist, womit die Griechen die Seele bezeichnen, das unteilbar Letzte des Menschen, das, da es nicht teilbar ist, über den Tod hinaus Bestand hat. ***

 Da Arius viel Zustimmung erfahren hat und viele Christen die gedankliche Klarheit der griechischen Philosophie anerkannten, war es zu einem tiefen Dissens gekommen. Solange die Christen verfolgt wurden, konnte die Frage nicht abschließend geklärt werden. Als Kaiser Konstantin im Mailänder Edikt im Jahr 313 den Christen nicht nur Religionsfreiheit gewährte, sondern die christliche Religion zur geistigen Grundlage des römischen Staates erklärte, bestand auch ein politisches Interesse an der Bereinigung dieser Frage. 325 ludt der Kaiser die Bischöfe des Reiches nach Nicäa, heute ein Vorort von Konstantinopel, ein. Wie konnte das Konzil eine Lösung finden, die von den Christen auch akzeptiert werden würde? Einen Beweis, daß Jesus von Nazareth wirklich der Sohn Gottes war, konnte mit philosophischer Argumentation nicht gefunden werden. Es gab und gibt auch keinen naturwissenschaftlichen Beweis im heutigen Sinn. Man kann Gott keinem Experiment unterziehen. Beweismittel ist allein das Zeugnis der biblischen Schriften. Die Evangelien und die Apostelbriefe kommen in ihren Aussagen den Anforderungen des griechischen Denkens nicht nahe genug. Das oben zitierte Wort: „Du bist mein geliebter Sohn, an dir habe ich Gefallen gefunden.“ könnte auch so verstanden werden, daß Jesus bei der Taufe im Jordan von Gott gleichsam ***[adoptiert](christologische_streitigkeiten.php)  wurde. Ob Gott ihm damit Anteil an seinem göttlichen Wesen gegeben hat, muß offen bleiben. Da es aber eindeutige Aussagen (Texte s.u.) gibt, die besagen, daß Jesus nicht erst Mensch und dann von Gott als Sohn angenommen wurde, sondern schon vor seiner Geburt bei Gott war, konnte das Konzil nur entscheiden, daß Jesus mit Gott wesensgleich ist. Das entspricht der Tauftradition der jungen Kirche. Der Aufschwung des ***[Weihnachtsfestes](http://www.kath.de/Kirchenjahr/weihnachten.php) ist eine Antwort auf die Infragestellung der Gottessohnschaft Jesu Christi. 

# **Theologische Aussagen in der Taufliturgie****
 In der Taufe wird der Glaube an den dreieinigen Gott schon nach den Schriften des Neuen Testaments bekannt. Bei Matthäus gibt Jesus, bevor in den Himmel aufgenommen wird, den Jüngern folgenden Auftrag:***

 „ Mir ist alle Macht gegeben im Himmel und auf der Erde. Darum geht zu allen Völkern und macht alle Menschen zu meinen Jüngern; tauft sie auf den Namen des Vaters und des Sohnes und des Heiligen Geistes, und lehrt sie, alles zu befolgen, was ich euch geboten habe. Seid gewiß: Ich bin bei euch alle Tage bis ans Ende der Welt.“ Matthäus 28,18-20 

# **Theologische Lösung****
 Da Jesus sich selbst als ***[Sohn](sohn_gottes.php)  bezeichnete, ist es offensichtlich, daß der Sohn aus dem Vater hervorgegangen ist. Deshalb unterscheidet das Konzil, daß Jesus „gezeugt“ aber nicht „geschaffen“ ist. Wäre der Sohn Gottes Geschöpf, dann wäre er geschaffen. Mit dem Verb „zeugen“ kann ausgedrückt werden, daß es ein innergöttlicher Vorgang ist, wenn der Sohn aus dem Vater hervorgeht. Das innergöttliche Leben ist nicht nur das Hervorgehen des Sohnes aus dem Vater, sondern im Heiligen Geist wird diese Beziehung personhaft Wirklichkeit. Das Konzil war so wichtig, daß es in den Gottesdiensten präsent gehalten wird, durch das sog. Grosse***

 Glaubensbekenntnis. Gebetet wird es heute in der Fassung, die es auf dem Konzil von Konstantinopel von 381, dem sog. 2. ökumenischen Konzil, erhalten hat. Dieses Bekenntnis ist allen christlichen Kirchen gemeinsam.***

# *
 Zitate*****

 Und das Wort ist Fleisch geworden,***

 und hat unter uns gewohnt,***

 und wir haben seine Herrlichkeit gesehen,***

 die Herrlichkeit des einzigen Sohnes vom Vater,***

 voll Gnade und Wahrheit .....***

 Niemand hat Gott je gesehen.***

 Der Einzige, der Gott ist und am Herzen des Vaters ruht,***

 er hat Kunde gebracht.***

 Johannes 1,14,18             

# **In jener Zeit sprach Jesus:**
 Ich preise dich, Vater, Herr des Himmels und der Erde, weil du all das den Weisen und Klugen verborgen, den Unmündigen aber offenbart hast. Ja, Vater, so hat es dir gefallen. Mir ist von meinem Vater alles übergeben worden; Niemand kennt den Sohn, nur der Vater, und niemand kennt den Vater, nur der Sohn und der, dem der Sohn es offenbaren will.“***

 Matthäus 11,25-27             

# **Jesus wird vom Hohen Rat wegen Gotteslästerung verurteilt:**
 Sie sagten: Wenn du der Messias bist, dann sag es uns! Er antwortete ihnen: auch wenn ich es euch sage – ihr glaubt mir ja doch nicht. Von nun an wird der Menschensohn zur Rechten des allmächtigen Gottes sitzen. Da sagten alle: du bist also der Sohn Gottes. Er antwortete ihnen: Ihr sagt es, ich bin es. Da riefen sie: Was brauchen wir noch Zeugenaussagen? Wir haben es selbst aus seinem eigenen Mund gehört.***

 Lukas 22,67-71 

# **Jesus, als er Maria von Magdala am Ostermorgen begegnet:**
 Jesus sagte zu ihr: Halte mich nicht fest; denn ich bin noch nicht zum Vater hinaufgegangen. Geh aber zu meinen Brüdern und sage ihnen: Ich gehe hinauf zu meinem Vater und zu eurem Vater, zu meinem Gott und eurem Gott.***

 Maria von Magdala ging zu den Jüngern und verkündete ihnen: Ich habe den Herrn gesehen. Und sie richtete aus, was er gesagt hatte.***

 Johannes 20,17-18 

# **Vor allen Geschöpfen als Anfang hat Gott aus sich eine vernünftige Kraft erzeugt, welche vom Heiligen Geist auch Herrlichkeit des Herrn, ein andermal Sohn, dann Weisheit, bald Engel, bald Gott, bald Herr und Logos genannt wird … Alle Attribute kommen derselben zu, weil sie dem väterlichen Willen dient, und weil sie aus dem Vater durch das Wollen erzeugt worden ist. Doch sehen wir nicht ähnliche Vorgänge auch bei uns? Wenn wir nämlich ein Wort aussprechen, erzeugen wir ein Wort, ohne damit etwas zu verlieren, ohne daß also die Vernunft in uns weniger wird.**
 Justin Dialog mit dem Juden Tryphon etwas 150 erfaßt 

# **Einer ist Arzt, aus dem Fleische zugleich und aus Geist gezeugt und ungezeugt, im Fleische erschienener Gott, im Tod wahrhaftiges Leben aus Maria, sowohl wie aus Gott, leidensfähig und leidensunfähig, Jesus Christus, unser Herr. …**
 Denn unser Gott, Jesus, der Christus, wurde von Maria im Schoße getragen, nach Gottes Heilsplan aus Davids Stamm und doch aus heiligem Geist.***

 Ignatius von Antiochien an die Kirche von Ephesus m 110 

# ****
 Wir glauben an den einen Gott, den Vater, den allmächtigen, den Schöpfer aller sichtbaren und unsichtbaren Dinge, ***

 und an den einen Herrn Jesus Christus, den Sohn Gottes, gezeugt aus dem Vater als Einziggeborener, aus dem Wesen des Vaters, Gott aus Gott, Licht aus Licht, wahrer Gott aus wahrem Gott,***

 gezeugt, nicht geschaffen, eines Wesens (homoousios) mit dem Vater, durch alles geworden ist, sowohl was im Himmel als auch auf der Erde ist, …..***

 diejenigen aber, die sagen: Es war einmal eine Zeit, da er nicht war, und bevor er gezeugt wurde, war er nicht, und daß er aus dem Nichts geworden sei, und die behaupten, aus einer anderen Hypostase (Substanz) oder Wesenheit oder veränderlich oder wandelbar sei der Sohn Gottes, diese schließt die katholische und apostolische Kirche aus.***

 Konzil von Nicäa, 325***

# *
 Denn der eine Christus ist selbst sowohl immer Gottes Sohn durch seine Natur als auch Menschensohn, der durch die Gnade in der Zeit angenommen wurde. Keinesfalls wurde er auf solche Weise angenommen, daß er zuerst erschaffen und dann angenommen wurde, sondern er (der Mensch Jesus) wurde durch die Annahme selbst erschaffen.***

 Augustinus Gegen die Lehre der Arianer, um 418***

  

***Text: Eckhard Bieger S.J.  

***                ***©***[ www.kath.de](http://www.kath.de) 
