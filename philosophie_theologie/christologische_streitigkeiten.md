---
title: Christologische Streitigkeiten
author: 
tags: 
created_at: 
images: []
---


# **Die Logik der theologischen Streitigkeiten der ersten Jahrhunderte *
# **Über Jesus von Nazareth, den Gesalbten, den Christus, wurde über Jahrhunderte gestritten. Theologische Schulen bekämpften sich. Konzilien konnten nur nach langen Debatten eine Klärung herbeiführen. Heute müssen sich Studenten der Theologie mühsam einen Denkweg durch die Theologiegeschichte der ersten Jahrhunderte erarbeiten. Welche Logik steckt hinter den Auseinandersetzungen, die als christologische Streitigkeiten in die Geschichte eingegangen sind?**
 Es geht um die Frage, was Jesus für den einzelnen Christen bedeutet. Daß er für Maria, seine Jünger, Maria und Martha und viele andere etwas bedeutet hat, bezweifelt niemand. Auch ist es unstrittig, daß er eine Bewegung ausgelöst hat, so daß die ***[jüdische Obrigkeit](suendenbock.php) Angst vor den Auswirkungen seiner Predigt und seiner Heilungswunder  ergriff. Mit der Kreuzigung hatten sie das Problem nicht aus der Welt geschafft. Seine Anhänger behaupteten, daß er lebt und viele Juden wie auch Nichtjuden ließen sich taufen, um so Anhänger des Mannes aus Nazareth zu werden. In ihren Predigten behaupteten seine Jünger, daß er der verheißene***[ Messias](messias_christus.php), der Gesalbte, der Christus sei, der Israel befreien werde. Als die christlichen Prediger Anhänger außerhalb Israels gewannen und das Christentum eine geistige Kraft in der griechisch geprägten Kultur des römischen Reiches wurde, wollten die Intellektuellen der damaligen Zeit genauer wissen, wer er wirklich sei. Sie wandten zur Klärung ihre philosophisch geprägten Begriffe an. Die christlichen Theologen mußten die Botschaft von Jesus in eine Denkwelt einbringen, die mehr verlangte als eine Übersetzung der biblischen Texte. War aber eine Frage einigermaßen gelöst, entstand aus der Antwort jeweils wenigstens eine neue. Der Denkweg, der zu den Lehraussagen der Konzilien über Jesus Christus führte, hat eine innere Logik, die mit der Frage beginnt: Was heißt es, daß Jesus von Nazareth der Sohn Gottes ist – wie es mehrfach in den biblischen Schriften steht: 

# **Jesus Christus: Als Sohn Gottes geschaffen oder aus Gott hervorgegangen? Konzil von Nicäa 325****
 Ist Jesus Gottes Sohn in dem Sinne, daß Gott ihn geschaffen und dann als Sohn adoptiert hat, so wie die sog. Adoptianer es behaupteten? Das würde bedeuten, daß Jesus zuerst als Mensch geboren wurde und dann, etwa bei der Taufe im Jordan, von Gott als Sohn adoptiert wurde. Diese Lehre war überwunden, als Arius auftrat. Er geht davon aus, daß der Sohn vor der Erschaffung der Welt als Weisheit Gottes existierte, jedoch nicht Gott gleich, sondern als Geschöpf.***[ Arius](arianismus.php) war Priester und Theologe. Er behauptete, daß Jesus geschaffen sei und konnte sich damit der Unterstützung der Intellektuellen seiner Zeit sicher sein. Für diese war völlig evident, daß Gott nur einer sein kann, denn vollkommen kann etwas sein, das es nur einmal und ungetielt gibt. Das Konzil von Nicäa  stellte 325 auf der Basis des Neuen Testaments fest, daß der Sohn Gottes nicht geschaffen sein kann:***

 Wir glauben an den einen Gott, der aber dreifaltig in drei Personen existiert. Jesus kommt vom Vater her, er ist als Sohn des Vaters kein Geschöpf, er ist in einem innergöttlichen Hervorgehen gezeugt und nicht außerhalb Gottes geschaffen.***


„              Gezeugt nicht geschaffen“ ist die Formel von Nicäa, die im Glaubensbekenntnis fest verankert ist. 

# **Die Menschheit Jesu nur eine Erscheinung?****
 Wenn Jesus wahrer Gott ist, wie ist dann zu verstehen, daß er auch wahrer Mensch ist?***

 Ein erster Antwortversuch: Dann ist er nicht wirklicher Mensch, sondern als wahrer Gott in menschlicher Gestalt erschienen. Das ist die These der Gnostiker ***

 Wenn die menschliche Gestalt aber nur ein Erscheinungsbild ist, dann hat die Kreuzigung Jesu nicht wirklich stattgefunden, er wäre dann auch nicht gestorben und nicht mit seinem Leib in den Himmel aufgefahren. Ohne Konzil war für die Theologen evident: Jesus Christus ist wirklicher Mensch und leiblich auferstanden. 

# **Ist Jesus nur mit dem Leib Mensch, hat aber anstatt einer geschaffenen Seele den Logos, den Sohn Gottes?**
 Wenn Jesus wirklicher Mensch gewesen sein muß und zugleich der ungeschaffene ***[Sohn Gottes](sohn_gottes.php) ist, wie ist dann die Einheit von Sohn Gottes und Mensch zu sehen? In der griechischen Philosophie ist das organisierende Prinzip des Leibes die Seele. Eine einfache Lösung wäre, daß der Sohn Gottes, der Logos, anstelle einer geschaffenen Seele den Körper beseelt. Das vertrat Apollinaris von Laodicäa.***

 Aber: Wäre die Menschheit erlöst, wenn die Seele des Menschen nicht durch Jesus mit erlöst worden wäre? Da die Sünde aus dem Herzen, also aus der Seele kommt, wäre die Erlösung fraglich. Daraus folgt: ***

 Jesus, der Christus, muß ganz Mensch sein, mit Leib und  Seele und zugleich Sohn Gottes. 

# **Wenn Jesus Christus eine eigene, geschaffene menschliche Seele hat, ist er dann „Zwei“; Konzil von Ephesus 431****
 Wenn Jesus aber Mensch mit Leib und Seele ist, wie kann er da noch einer sein. Gibt es dann nicht „zwei Söhne“, d.h. zwei Individuen? Diese Frage führte zum nächsten wichtigen Konzil, 100 Jahre nach Nicäa. 431 trafen sich Bischöfe in Ephesus und erklärten, daß Jesus, der Christus, als Sohn Gottes geboren ist. Maria ist Gottesgebärerin. Damit wird klargestellt, daß Jesus nur einer ist und von Anfang an Sohn Gottes war und nicht später erst von Gott als Sohn adoptiert wurde. ***

 Jesus Christus ist von seiner Geburt an Sohn Gottes und Wenn Jesus aber Mensch mit Leib und Seele ist, wie kann er da noch einer sein. Gibt es dann nicht „zwei Söhne“, d.h. zwei Individuen? Diese Frage führte zum nächsten wichtigen Konzil 100 Jahre nach Nicäa. 431 trafen sich Bischöfe in Ephesus und erklärten, daß Jesus, der Christus, als Sohn Gottes geboren ist. Maria ist Gottesgebärerin. Damit wird klargestellt, daß Jesus nur einer ist und von Anfang an Sohn Gottes war und nicht später erst von Gott als Sohn adoptiert wurde. ***

 Jesus Christus ist von seiner Geburt an Sohn Gottes und wahrer Mensch. 

***Das Konzil verurteilte den damaligen Patriarchen von Konstantinopel, Nestorius. Die Verurteilung war nach heutiger Kenntnis der Texte nicht gerechtfertigt, denn Nestorius hat die Lehre von den sog. „Zwei Söhnen“ in Jesus nicht vertreten. Ein Teil der Bischöfe hat sich dem Konzil nicht angeschlossen, weil sie eine Vermischung von Göttlichem und Menschlichem befürchteten. So ist der Eindruck entstanden, sie würden in Jesus Christus „zwei Söhne“ verehren. Diese in Persien beheimatete Kirche wurde mit dem Schimpfwort ***[„Nestorianer“](nestorianer.php) belegt. Diese Kirche hatte Gemeinden entlang der Seidenstraße gegründet und war mit ihren Missionaren lange vor Marco Polo in China. Timur Lenk hat diese Kirche praktisch vernichtet, viele sind bei der Ausrottung der Armenier Anfang des 20. Jahrhunderts getötet worden, es gibt nur noch kleine Reste dieser Kirche.  

# **Ist Jesus eine neue Natur, die sozusagen aus Gottheit und Menschheit vereinigt ist oder gibt es ein einigendes Prinzip, das beide Naturen umfaßt?****
 Wenn Jesus Christus wahrer Mensch und wahrer Gott ist, was ist das einigende Prinzip? Wenn man den Begriff „Physis“ - Natur benutzt, kann man von göttlicher und menschlicher Natur sprechen. Benutzt man das gleiche Wort, um das einigende Prinzip von Gottheit und Menschheit in Jesus zu benennen, dann drückt der gleiche Begriff sowohl die Verschiedenheit aus wie auch die Einheit. Die Theologenschule von Alexandrien, die das Konzil von Ephesus bestimmt hatte, ist diesen Weg gegangen. Sie befürchtete, daß  der Mensch Jesus und der Sohn Gottes nur als lose verbunden gedacht werden, ohne daß die Einigung zwischen Gottheit und Menschheit als endgültig gesehen wird. Weil dieser Schule die Durchdringung der menschlichen Natur durch das Göttliche bis heute als inneres Prinzip auch der christlichen Spiritualität sieht, spricht sie von einer neuen Physis, einer neuen Natur, um die Einheit zwischen Gottheit und Menschheit zu betonen. Sie werden von ihren Gegnern als ***[ Monophysiten ](monophysiten.php) bezeichnet. So verstehen sich die Kopten und Teile der syrischen Kirche, sowie die äthiopische und armenische Kirche bis heute. Zum Bruch kam es nach dem Konzil von Chalcedon, das den ersten Schritt einer Klärung einleitete.  

# **Neben den zwei Naturen „Gottheit“ und „Menschheit“ gibt es die Person, Jesus Christus ist nur einer, aber in zwei Naturen. Konzil von Chalzedon 451****
 Um das Einigende in Jesus Christus zu benennen, reicht der Begriff „Physis“ nicht. Langsam entwickelte sich eine Begrifflichkeit, die in dem ***[Personbegriff](person.php) mündete. Aber erst im 6. Jahrhundert war dieser Denkweg einigermaßen abgeschlossen. Im Konzil von Chalzedon 451 wird der Begriff bereits gebraucht. Die Formel des Konzils ist ***


- unvermischt und ungetrennt***


- zwei Naturen, die in einer Person geeinigt sind***


Die Naturen vermischen sich
nicht, aber sie führen auch nicht zu zwei Individuen. Das Einigende ist die Person. Damit ist für das Menschenbild des Abendlandes Neues in Gang gesetzt: Der Mensch ist nicht nur in seiner Seele wichtig, die den Körper nach dem Tod verläßt, vielmehr wird der Leib eine himmlische Existenz haben. Unser Leib gehört zur Person gleicherweise wie das geistige Prinzip, die Seele. 

# **Hatte Jesus neben seinem göttlichen auch einen menschlichen Willen?**
 Der Monothelethismus verneinte das.*****

 Es bleibt noch eine Frage: Wenn Jesus nur eine Person ist, der Sohn Gottes, der die Menschennatur angenommen hat, dann kann er nur einen Willen gehabt haben, denn eine Person zeichnet sich gerade dadurch aus, daß sie nur einem Willen folgt. Das legt die Überlegung nahe, daß Jesus sich in seiner Menschheit nicht gegen den Sohn Gottes aufgelehnt haben kann, er war immer einer, der auf Gott hörte. Seine oberste Richtschnur war, den Willen Gottes zu erfüllen. Das ist die Position der Monotheleten, der Verfechter nur eines Willens in Jesus.  

# **Jesus hat auch einen menschlichen Willen. Das 3. Konzil von Konstantinopel 680 ****
 Wenn Jesus wirklich ***[Mensch](menschwerdung_jesu.php) war, mit Seele und Leib, mußte er auch einen menschlichen Willen haben. Die Menschheit Jesu durfte ja nicht amputiert sein.***

 Das 3. Konzils von Konstantinopel 680 verurteilte die Lehre des Monotheletismus. Jesus Christus hat nicht nur zwei Naturen, sondern auch zwei Willen, einen göttlichen und einen menschlichen. Der menschliche Wille ist dem göttlichen untergeordnet.***

# **Zitate****
 Konzil von Nicäa 325***


… . Jesus Christus, .. Sohn Gottes, geboren vom Vater, eingeboren, das heißt von des Vaters Wesen, Gott von Gott, Licht von Licht, wahrhaftiger Gott vom wahrhaftigen Gott, geboren, nicht geschaffen, mit dem Vater eines Wesens, durch den alles geschaffen ist, was im Himmel und auf Erden ist, der für uns Menschen und um unsrer Seligkeit willen herabgekommen und Mensch geworden ist ….***

 Die da sagen: es gab eine Zeit, da er nicht war, und ehe er geboren ward, war er nicht, und daß er aus dem ward, was nicht ist, oder die ihn für eine andere Hypostase oder Wesen halten oder sagen, Gottes Sohn sei geschaffen oder veränderlich, die verdammt die allgemeine Kirche. 

# ** Brief des Patriarchen Johannes von Antiochien an Cyrill von Alexandrien, 433**
 Wir bekennen, daß unser Herr Jesus Christus, der eingeborene Sohn Gottes, vollkommener Gott und vollkommener Mensch ist mit einer Vernunftseele und einem Leib. Er ist von Ewigkeit her vom Vater gezeugt der Gottheit nach. Am Ende der Tage aber ist derselbe Christus für uns und unseres Heiles willen der Menschheit nach geboren worden aus Maria der Jungfrau. Er ist wesengleich mit dem Vater der Gottheit und wesensgleich der Menschheit nach. Es hat nämlich eine Vereinigung beider Naturen stattgefunden und deshalb bekennen wir einen Christus, einen Sohn, einen Herrn. Wegen der Vereinigung ohne Vermischung bekennen wir, daß die heilige Jungfrau Gottesgebärerin (Theotokos) ist, weil das göttliche Wort Fleisch und Mensch geworden ist und schon von der Empfängnis an den aus ihr genommenen Tempel mit sich selbst vereinigt hat.  

# ** Konzil von Chalzedon 451**
 In der Nachfolge der heiligen Väter also lehren wir alle übereinstimmend, unseren Herrn Jesus Christus als ein und denselben Sohn zu bekennen: derselbe ist vollkommen in der Gottheit und derselbe ist vollkommen in der Menschheit: derselbe ist wahrhaft Gott und wahrhaft Mensch aus vernunftbegabter Seele und Leib; derselbe ist der Gottheit nach dem Vater wesensgleich und der Menschheit nach uns wesensgleich, in allem uns gleich außer der Sünde; derselbe wurde einerseits der Gottheit nach vor den Zeiten aus dem Vater gezeugt, andererseits der Menschheit nach in den letzten Tagen unsertwegen und um unseres Heiles willen aus Maria, der Jungfrau (und) Gottesgebärerin, geboren; ein und derselbe ist Christus, der einziggeborene Sohn und Herr, der in zwei Naturen unvermischt, unveränderlich, ungetrennt und unteilbar erkannt wird, wobei nirgends wegen der Einung der Unterschied der Naturen aufgehoben ist, vielmehr die Eigentümlichkeit jeder der beiden Naturen gewahrt bleibt und sich in einer Person und einer Hypostase vereinigt; der einziggeborene Sohn, Gott, das Wort, der Herr Jesus Christus, ist nicht in zwei Personen geteilt oder getrennt, sondern ist ein und derselbe, wie es früher die Propheten über ihn und Jesus Christus selbst es uns gelehrt und das Bekenntnis der Väter es uns überliefert hat. 

# **3. Konzil von Konstantinopel 680/81**
 Die zwei Willen in Jesus***

 Ebenso verkünden wir gemäß der Lehre der heiligen Väter, daß sowohl zwei natürliche Weisen des Wollens bzw. Willen als auch zwei natürliche Tätigkeiten ungetrennt, unveränderlich, unteilbar und unvermischt in ihm sind; und die zwei natürlichen Willen sind einander nicht entgegengesetzt - das sei ferne! -, wie die ruchlosen Häretiker behaupteten; vielmehr ist sein menschlicher Wille folgsam und widerstrebt und widersetzt sich nicht, sondern ordnet sich seinem göttlichen und allmächtigen Willen unter; denn der Wille des Fleisches mußte sich regen, sich aber nach dem allweisen Athanasius dem göttlichen Willen unterordnen; denn wie sein Fleisch des Wortes Gottes genannt wird und ist, so wird auch der natürliche Wille seines Fleisches als dem Wort Gottes eigen bezeichnet und ist es, wie er selbst sagt: „Denn ich bin herabgestiegen aus dem Himmel, nicht um meinen eigenen Willen zu tun, sondern den Willen des Vaters, der mich gesandt hat" (Johannesevangelium 6,38); dabei nannte er den Willen des Fleisches seinen eigenen Willen, da auch das Fleisch ihm eigen geworden ist; denn wie sein ganzheiliges und makelloses beseeltes Fleisch trotz seiner Vergöttlichung nicht aufgehoben wurde, sondern in der ihm eigenen Abgrenzung und dem ihm eigenen Begriff verblieb, so wurde auch sein menschlicher Wille trotz seiner Vergöttlichung nicht aufgehoben, sondern ist vielmehr gewahrt, wie der Gottesgelehrte Gregor sagt: „Denn sein Wollen, verstanden in Bezug auf den Erlöser, ist Gott nicht entgegengesetzt, da es ganz vergöttlicht ist". 

# **              Origines v. Alexandrien, 185-254 von den Prinzipien IV,4,4**
 Als nun der Sohn Gottes zum Heile der Menschheit sich den Menschen offenbaren und unter den Menschen wandeln wollte, nahm er nicht, wie manche glauben, nur einen menschlichen Leib an, sondern auch eine Seele an. Diese war zwar ihrer Natur nach unseren Seelen gleich, aber nach ihrer Willensrichtung und ihrem sittlichen Verhalten ihm selber gleich und so beschaffen, daß sie alle Entschlüsse und Heilsvorhaben des Logos und der Weisheit unverfälscht ausführen konnte. 

# **4. Synode von Toledo 633**
 Derselbe Christus, unser Herr Jesus, einer aus der Dreifaltigkeit, hat – abgesehen von der Sünde – einen an Seele und Leib vollkommenen Menschen angenommen, er blieb dabei, was er war, er nahm an, war er nicht war; er war gleich dem Vater in seiner Göttlichkeit, er war geringer als der Vater in seiner Menschlichkeit und besaß in einer Person die Eigenschaften zweier Naturen; Naturen waren nämlich in ihm zwei, Gott und Mensch, nicht aber zwei Söhne und zwei Götter, sondern die eine und selbe Person in beiden Naturen. 

# **Konzil von Frankfurt 794**
 Wir bekennen unseren Herrn Jesus Christus als wahren Gott und wahren Mensch in einer Person. Er blieb also die Person des Sohnes in der Trinität, zu welcher Person die menschliche Natur hinzukam, damit auch eine Person sei Gott und Mensch – nicht ein vergöttlichter Mensch oder ein erniedrigter Gott, sondern Gott-Mensch und Mensch-Gott; wegen der Einheit der Person ist der eine Sohn Gottes zugleich Menschensohn, vollkommener Gott und vollkommener Mensch. Vollkommen ist der Mensch aber nur mit Leib und Seele. 

# **Text: Eckhard Bieger S.J.**
 ©***[ ***www.kath.de](http://www.kath.de) 
