---
title: Person
author: 
tags: 
created_at: 
images: []
---


# **Die Entwicklugn des Personbegriffs*
# **Die Vorstellung vom Menschen als einem Wesen, das auf Grund seiner Geistigkeit und Freiheit innerlich unabhängig ist und über sein Leben verfügt, begründet die Würde dieses Wesens und damit die Menschenrechte. Person ist das Innerste und zugleich das Ganze des Menschen. Urteilsfähigkeit, freie Entscheidung, das Verhältnis zur Umwelt und anderen Menschen sehen wir zugleich in dem Personkern des einzelnen, wie auch der Leib zur Person gehört. Wir können den anderen in seiner Person „treffen“, wenn wir ihn liebkosen oder schlagen. Diese auf die Person zielenden Berührungen sind von denen zu unterscheiden, die nur einen Teil des Körpers betreffen, wenn z.B. ein Arzt eine Wunde behandelt oder die Kosmetikerin das Gesicht einkremt.**
 Wie kam es zu der Vorstellung, daß das Innerste, was den Menschen ausmacht, auch den Leib umfaßt. Es sind theologische Auseinandersetzungen im 5. und 6. Jahrhundert, die zum Pesonbegriff führten, der zur Grundlage der europäischen Kultur wurde. „Jesus von Nazareth“, der menschgewordene Sohn Gottes, forderte das Denken heraus, so daß das Menschenbild der Antike weiter entwickelt werden mußte. Die Frage, die sich damals stellte war folgende: Wenn Jesus wirklicher Mensch war und zugleich die zweite Person des dreifaltigen Gottes, wie kann er dann „einer“ sein? Der Personbegriff entwickelte sich aus der Frage, was die Seele des Menschen Jesus war und ob Jesus überhaupt eine menschliche Seele hatte. 

# **Die Vorstellung von Leib und Seele reicht nicht aus****
 Die Vorstellung, daß der Mensch eine Geistseele besitzt, die vom Körper zu unterschieden ist, hatten die Griechen entwickelt. Den Juden war an einer solchen begrifflichen Klarheit nicht so sehr gelegen. Sie sahen im Atem das belebende Prinzip. Gott erschuf den ersten Menschen, indem er Lehm formte und ihm Leben einhauchte. (s.u. Zitate)***

 Die Griechen erkannten deutlicher, daß der Mensch über ein geistiges Prinzip verfügt, das sein ganzes Wesen organisiert. Sie nennen es Seele, „Psyche“, das lateinische Wort ist „anima“. Wenn aber Jesus eine menschliche Seele hat, dann ist er auch ein menschliches Individuum. Das entspricht den Berichten der Bibel, die Jesus als einen Menschen zeigen, der Hunger und Durst hat, der Zuneigung, Zorn und Angst kennt, der die Gefühle und die Befindlichkeit anderer Menschen versteht. Wenn Jesus aber zugleich der Sohn Gottes ist, dann ist er auch ein „göttliches Ich“. Hat er aber dann daneben noch ein ***[menschliches „Ich“](menschwerdung_jesu.php)? ***

 Eine Lösung des Problems konnte im griechischen Denken so gefunden werden, daß Jesus keine menschliche Seele hat, sondern der göttliche Logos an die Stelle der menschlichen Seele  als organisierendes Prinzip des Körpers tritt. Das konnte aber nicht angenommen werden, denn Jesus fühlte wie ein Mensch. Damit war die griechische Vorstellung vom Menschen an ihre Grenze geraten. Das sieht man auch an einem anderen Lösungsmodell. Cyrill von Alexandrien sah das Neue, das uns in Jesus entgegen tritt, daß ein Mensch zugleich wirklich Gott ist, als eine ***["neue Natur"](monophysiten.php).  Es wäre also aus der Verbindung von Gott und Mensch eine neue Natur entstanden, ein Gottmenschentum könnte man in einer uns mehr geläufigen Begrifflichkeit sagen. Aber kann man durch Mischung erklären, was so verschieden ist. Es ist deutlich, daß mit dem Begriff Natur aber gerade das bezeichnet wird, was nicht „Ich“ ist, nämlich das Göttliche und Menschliche in Jesus. Es wird auch deutlich, daß Begriffe nur Verstehensmöglichkeiten beinhalten und nicht einfachhin die Wirklichkeit wiedergeben. Deshalb ist der Kampf um die Begriffe, der im 4. und 5. Jahrhundert mit aller Schärfe ausgetragen wurde, ein Ringen, wie man in der Sprache der damaligen Zeit erklären kann, wer Jesus war. Die Konzilien haben die Grenzen markiert, was kein angemessenes Sprechen über Jesus, den Sohn Gottes ist. Das Geheimnis selbst haben sie nicht erklärt. 

# **Das Antlitz des Menschen repräsentiert seine Person****
 Wenn Seele und Natur nicht mehr die Begriffe sein können, womit das bezeichnet wird, was die ***[beiden Naturen](zweinaturenlehre.php)  einigt, so daß Jesus nicht zweimal „Ich“ ist, mußte ein neuer Begriff gefunden werden. Indem man sich auf das Antlitz des Menschen bezog, konnte man Leiblichkeit und Geistigkeit in einem verstehen, denn in seinem Antlitz zeigt sich der andere als der, der er ist. Das lateinische Wort für Person kommt von personare, hindurchschallen. Es bezieht sich auf die Maske, die Theaterschauspieler trugen. Aus diesem Spezialbegriff hat sich das Wort entwickelt, mit dem im Abendland das Besondere des Menschen bezeichnet wird und das in dem Wort „Personwürde“ das Unantastbare jedes einzelnenausdrückt. ***

 Denn mit dem Begriff „Person“ kann etwas ausgesagt werden, das mit dem Wort „Seele“ nicht deutlich wird. Die Seele ist nach griechischem Denken das organisierende und damit das Lebens-Prinzip des Körpers. Verläßt die Seele den Körper, ist dieser ohne Leben und nur noch Materie. Da die Christen durch die leibliche ***[Auferstehung Jesu](auferstehung_jesu.php) erkannt hatten, daß der Leib ebenso wie das geistige Prinzip, die Seele, zur himmlischen Existenz gehören, mußte die Einheit von Leib und Seele genauer beschrieben werden. Für die Griechen besteht die Existenz des Menschen nach dem Tod darin, daß die Seele im Himmel ist und der Körper in seine Materieteile zerfällt. Eine leib-seelische Existenz braucht aber ein einigendes Prinzip, das anders gedacht werden muß als die griechische Vorstellung von der Seele. Die Person ist dieses einigende Prinzip. Jesus ist eine Person und doch zugleich ganz Gott und ganz Mensch. Das heißt dann auch, daß der Mensch Person ist und damit sein Leib eine ganz neue Bedeutung erhält. Der Leib ist mehr als eine zufällige Ansammlung von Molekülen, er gehört zur Person und muß genauso geachtet werden wie der Geist des Menschen. 

# **Person - Antlitz****
 Persona bezeichnet im Lateinischen die Maske des Theaterschauspielers. Im Griechischen entspricht dem das Wort Prosopon für Antlitz. In seinem Antlitz tritt uns der andere als Person gegenüber, wir lesen aus seinen Gesichtszügen, was er fühlt, wie er reagiert, wie er uns begegnet. Die Person als einigendes Prinzip ermöglicht es, den Sohn Gottes als Person in der Dreifaltigkeit zu benennen und zugleich sagen zu können, daß der Sohn ganz Mensch geworden ist, mit Leib und Seele, ohne daß die menschliche Seele aus Jesus einen zweiten Sohn Gottes machen würde. Jesus ist nur einer, ganz Gott und ganz Mensch. 

# **Zitate****
 Da formte Gott, der Herr den Menschen aus Erde vom Ackerboden und blies in seine Nase den Lebensatem. So wurde der Mensch zu einem lebendigen Wesen. (Buch Genesis, 2,7) 

# **Es muß aber gefragt werden, auf welche Weise das Wort Fleich wurde, ob es gewissermaßen im Fleisch sich verwandelt hat oder es das Fleisch angezogen hat. Alles nämlich, das in etwas anderes verwandelt wird, hört auf zu sein, was es war, und beginnt zu sein, was es nicht war. Gott aber hört nicht auf zu sein, noch kann er etwas anderes sein. ... Wenn das Wort auf Grund der Verwandlung und Veränderung seiner Substanz Fleisch geworden wäre, dann würde Jesus ein Substanz aus zweien sein, aus Fleisch und Geist, ein gewisses Gemisch, wie eines aus Gold und Silber, das weder Gold ist, das ist der Geist, noch Silber, das ist das Fleisch, so daß das eine durch das andere verändert wird und etwas Drittes entsteht. Dann ist Jesus auf keinen Fall Gott, das Wort, das Fleisch wurde, hat dann aufgehört zu sein. Fleisch im eigentlichen Sinne ist es auch nicht mehr, weil es (das Fleisch) zu Wort wurde. ....**
 (Jesus Christus wird aber gelehrt) als Gottessohn und Menschensohn, wobei Gott und Mensch ohne Zweifel gemäß beiden Substanzen in ihrer Eigentümlichkeit getrennt sind, weil weder das Wort etwas anderes als Gott noch das Fleisch etwas anderes als Mensch ist.***

 Wir erkennen einen doppelten Seinsstand, unvermischt, aber verbunden in einer Person, den Gott und den Menschen Jesus. ...***

 So sehr ist die Eigentümlichkeit jeder der beiden Substanzen gewahrt, daß sowohl der Geist in ihm das, was ihm zukommt, tut, nämlich die Tugenden, Werke und Wunder, als auch das Fleisch seine Leiden erduldet....***

 Tertullian, Adversus Praxean, um 210 

***Boethius, ein Denker des 6. Jahrhunderts, der am Hof des Ostgotenkönigs Theoderichs tätig war, ist das Bindeglied zwischen der Antike und dem Mittelalter. Er hat sich um klare Begrifflichkeiten bemüht, indem er für griechische Worte lateinische Äquivalente entwickelte und definierte. Er legte damit die Grundlagen für die mittelalterliche Philosophie und Theologie. Boethius definiert Person als "rationabilis naturae individua substantia – die individuelle Substanz einer vernunftbegabten Natur". 

# **                  In all diesen Fällen kann Person niemals als in den allgemeinen (universellen) Seienden bestehend ausgesagt werden, sondern nur in den einzelnen und in den Individuen. Als Lebewesen oder als Gattung ist der Mensch keine Person, sondern man redet von einzelnen Personen, nämlich von Cicero, Plato und der einzelnen Individuen…..**
 Natur ist die spezifische Eigentümlichkeit einer beliebigen Substanz, Person aber die individuelle Substanz einer vernunftfähigen Natur. ***

 Opusculum contra Eutychen et Nestorium um 512            

# **Hugo von St. Viktor (Pariser Augustinerstift), aus Halberstadt stammender Theologe des 12. Jahrhunderts**
 So wurde Gott wahrer Mensch. Wahrer Mensch wäre er aber nicht gewesen nur im Fleische oder nur in der der Geistseele, weil der Mensch sowohl Fleisch wie Geist ist. Als Gott den Menschen annahm, hat er deswegen beides angenommen. Er nahm aber Fleisch und Seele an, das heißt: den Menschen, seine Natur, aber nicht seine Person. Denn er hat nicht den Menschen als Person angenommen, sondern er nahm den Menschen in seine Person auf. Deswegen hat er den Menschen angenommen, weil er Fleisch und Geist des Menschen angenommen hat. Darum hat er nicht eine menschliche Person angenommen, weil jenes Fleisch und jene Seele noch nicht zu einer menschlichen Person vereint waren, bevor sie vom Wort (dem Sohn Gottes) zu einer Person geeint wurden. … Das Wort war schon vor dieser Vereinigung eine Person, weil es der Sohn war, der Person war, wie auch der Vater Person war und der Heilige Geist.***

 De sacramentis christianae fidei, 1134 

# **Eckhard Bieger **

# *
# *
# *
©***[ ***www.kath.de](http://www.kath.de) 
