---
title: Erbsünde und Augustinus
author: 
tags: 
created_at: 
images: []
---


# **Augustinus hat den Begriff Erbsünde zuerst entwickelt*
# **Erbsünde besagt, daß Sünde sich vererbt. Kann Gott das wollen? Wie kam es überhaupt zu dem Begriff. Augustinus von Hippo, (354-430), der große Theologe der Westkirche, von dem Luther sich inspirieren ließt, entwickelte die Vorstellung der Erbsünde.**
 Der „geistigen Vater“ der Erbsündenlehre war zuerst Anhänger der Manichäer, ehe er sich zum Christentum bekehrte. Seine Mutter Monika, selbst Christin, hatte Jahre für die Bekehrung des Sohnes gebet. Augistinung stammte aus Nordafrika und wurde Bischof von Hippo, wo er u.a. den Sturm der Vandalen erlebte.***

 Am Anfang seiner Lehre steht nicht – wie man vielleicht erwarten könnte – eine abstrakte Überlegung über die Sündhaftigkeit der ganzen Menschheit o.ä. im Mittelpunkt, sondern ein ganz konkretes exegetisches Problem: die Auslegung von Röm. 9,10-13 ***

# *
„ [10] So war es aber nicht nur bei ihr [Sara], sondern auch bei Rebecca: Sie hatte von einem einzigen Mann empfangen, von unserem Vater Isaak, [11] und ihre Kinder waren noch nicht geboren und hatten weder Gutes noch Böses getan; damit aber Gottes freie Wahl und Vorherbestimmung gültig bleibe, [12] nicht abhängig von Werken, sondern von ihm, der beruft, wurde ihr gesagt: Der Ältere muß dem Jüngern dienen; [13] denn es steht in der Schrift: Jakob habe ich geliebt, Esau aber gehaßt.“ 

# ****
 1. Schritt der Überlegung: gott weiß, wie ein Menshc sich entscheiden wird***

 Die entscheidende Frage in Bezug auf Röm 9,10-13 war für Augustinus, wie man erklären könne, dass Gott von zwei Ungeborenen den einen liebe, den anderen aber hasse, ohne Gott vorwerfen zu müssen, er sei ungerecht, was in Augustins Augen selbstverständlich absurd wäre. In seiner Schrift „Expositio quarundam propositionum ex epistola ad Romanos“ (394) löst der Kirchenvater das Problem dadurch, dass er als Grund für die Erwählung des Jakob und die Verwerfung des Esau Gottes Vorherwissen, wie beide einmal beschaffen sein werden, angibt. Fest steht für den Kirchenvater, dass es ein Unterscheidungskriterium zwischen beiden geben müsse, sonst könne es keine gerechtfertigte Erwählung bzw. Verwerfung geben. Fragt man nun, was konkret Gott erwähle, so lautet die Antwort, nicht bestimmte Werke (vgl. Röm. 9,12), sondern den vorausgewussten Glauben, denn dieser sei das Verdienst des Menschen. Gott wusste also voraus, dass Jakob glauben würde, deshalb erwählte und liebte Gott ihn, genauso, wie er vorauswusste, dass Esau nicht glauben würde, weshalb er diesen verwarf und hasste. Es ist somit letztlich der Mensch selbst, der über sein Heil oder Unheil entscheidet, denn er ist frei zu Glauben oder nicht zu glauben.  

# **Es gibt keine Verdienste des Menschen**
 Circa zwei Jahre später in seiner Schrift „Ad Simplicianum“ (396) beschäftigt sich Augustinus erneut mit Röm 9,10-13 und kommt dabei zu dem Ergebnis, dass auch der Glaube des Menschen nicht als dessen Verdienst betrachtet werden dürfe, da der Mensch alles der zuvorkommenden Gnade Gottes verdanke. Wenn aber der Mensch somit überhaupt keine Verdienstmöglichkeit mehr besitzt und deshalb alle Menschen gleich sind, dann stellt sich erneut die Frage nach dem Grund der Erwählung Jakobs. Diese lässt sich noch relativ leicht als reines und absolut freies Gnadengeschenk Gottes erklären, allem Tun und Wollen des Menschen zuvorkommend, wodurch Augustinus die Souveränität und Größe Gottes gebührend gewürdigt sieht. Schwieriger verhält es sich jedoch bei Esau. Wie lässt sich dessen Verwerfung einsichtig machen, ohne Gott den Vorwurf der Ungerechtigkeit aussetzten zu müssen? Naheliegend wäre nun der Erklärungsversuch, Esau habe das freie Gnadenangebot Gottes abgelehnt und deshalb seine Verwerfung verdient. Augustinus erkennt aber, dass eine solche Argumentation nicht greift, da der Text explizit von Ungeborenen spricht, die weder etwas wollen, noch etwas nicht wollen konnten, als der eine bereits erwählt, der andere schon verworfen war. Der Kirchenvater sah sich somit dem Dilemma ausgesetzt, einerseits einen (gerechten) Grund für die Verwerfung des ungeborenen Esau finden zu müssen, andererseits aber bereits erklärt zu haben, dass die Gnadenwahl Gottes völlig frei und unabhängig von allem Tun und Wollen des Menschen erfolge. ***

 Um dieses Dilemma lösen zu können entwickelt er sukzessive die Lehre von der Erbsünde, die besagt, dass die ganze Menschheit eine „einzige Sündenmasse“ (massa peccati) darstelle. Begründet sieht Augustinus dies in Röm 5,12 („in quo omnes peccaverunt“), was er fälschlich mit „in dem (= in Adam) alle sündigten“ übersetzt, anstatt korrekt mit „weil alle sündigten“, wie aus der griechischen Übersetzung des Alten Testaments, der Septuaginta, hervorgeht. Alle Menschen haben „in Adam“, dem Stammvater der ganzen Menschheit gesündigt und verdienen damit als gerechte Strafe die ewige Verdammnis. Es steht dabei Gott selbstverständlich frei, diese Strafe den einen nachzulassen, sie aber von den anderen einzufordern. Das eine ist Ausdruck seiner Barmherzigkeit, das andere ist Ausdruck seiner Gerechtigkeit. Stellt man nun die Frage, warum Gott aus der großen Sündenmasse der Menschheit nur einige wenige zum Heil erwählt und vorherbestimmt, während er dem größten Teil seine rechtfertigende Gnade verweigert – wovon der Kirchenvater überzeugt ist – dann könne nach Augustinus nur geantwortet werden, dass der Mensch dies nicht einsehen könne, sondern darauf vertrauen müsse, dass dies einer nur Gott einsichtigen Gerechtigkeit entspreche. ***


„ Es bilden also alle Menschen – zumal da nach dem Wort des Apostels ‚in Adam alle sterben’ (1Kor 15,22), von dem sich für das gesamte Menschengeschlecht der Ursprung der Beleidigung Gottes herleitet – eine einzige Sündenmasse, die der höchsten, göttlichen Gerechtigkeit Strafe schuldet. Wenn sie eingetrieben oder nachgelassen wird, so bedeutet beides kein Unrecht.“ ***

 [Augustinus, Simpl. 1,2,16; Aurelius Augustinus. Der Lehrer der Gnade. Gesamtausgabe seiner antipelagianischen Schriften. Prolegomena 3 (Lateinisch- Deutsch). Eingel., übertr. und erl. von Thomas Gerhard Ring (=ALG 3), Würzburg 1991]***


„ Ist Gott nicht ungerecht, wenn er trotz seiner Allmacht nur einige erwählt? Ausgehend von dem unerschütterlichen Axiom, daß es in Gott keinerlei Ungerechtigkeit gibt, kann Augustinus eine Lösung nur darin finden, daß er den Menschen völlig rechtlos macht. Das Instrument zur Entmündigung des Menschen ist ein – von ihm neu geschaffenes – biologisches Verständnis der Erbsünde und eine krasse Schlußfolgerung daraus: Die Menschen sind ein Masse der Sünde, die rechtens einzig und allein Verdammung verdient.“ ***

 [Georg Kraus, Gnadenlehre – Das Heil als Gnade, in: Wolfgang Beinert (Hg.), Glaubenszugänge. Lehrbuch der katholischen Dogmatik (Bd. 3), Paderborn 1995, 157-305] 

# **Zitate**
„ Der tragende Gedankengang, den Augustinus in „Ad Simplicianum“ ausführt, ist daher der: Wenn alles Positive, Berufung und Erwählung, von Gott abhängen, und wenn zudem nicht alle erwählt werden, so muß der Grund hierfür in der Menschheit selber liegen, denn sonst müßte gegen Gott der Vorwurf der Ungerechtigkeit erhoben werden. So wenig aber ein Gläubiger ungerecht ist, wenn er dem einen Schuldner seine Schuld erläßt aus Gnade, dem anderen aber nicht, so wenig kann Gott ein Vorwurf daraus gemacht werden, daß er nicht alle rechtfertigt und nicht allen das Heil gibt. […] Wenn Gott den einen erwählen, den anderen aber verwerfen wollte, er aber dabei nicht auf Verdienstunterschiede sehen konnte, weil sie ja letztlich von Gott kommen, dann blieb offensichtlich nur die Möglichkeit der Annahme, daß alle Menschen von Anfang an, und das heißt: durch Adams Sünde der Verwerfung schuldig sind – so daß Gott nun dadurch seine Gnade zeigen könne, daß er aus dieser „massa“ einige zum Heile erwählte, andere nicht“. ***

 [Walter Simonis, Heilsnotwendigkeit der Kirche und Erbsünde bei Augustinus, in: Carl Andresen (Hg.), Zum Augustin-Gespräch der Gegenwart (WdF 327), Darmstadt 1981, 301-328] 

# **Robert Walz**
©***[ ***www.kath.de](http://www.kath.de) 
