<HTML><HEAD><TITLE>Glauben und Wissen</TITLE>
<meta name="title" content="Philosophie&amp;Theologie">
<meta name="author" content="Redaktion kath.de">
<meta name="publisher" content="kath.de">
<meta name="copyright" content="kath.de">
<meta name="description" content="">
<meta name="abstract" content="Lexikon &uuml;ber Philosophie&amp;Theologie">
<meta http-equiv="content-language" content="de">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="date" content="2006-00-01">
<meta name="robots" content="index,follow">
<meta name="revisit-after" content="10 days">
<meta name="revisit" content="after 10 days">
<meta name="DC.Title" content="Philosophie&amp;Theologie">
<meta name="DC.Creator" content="Redaktion kath.de">
<meta name="DC.Contributor" content="J�rgen Pelzer">
<meta name="DC.Rights" content="kath.de">
<meta name="DC.Publisher" content="kath.de">
<meta name="DC.Date" content="2006-00-01">
<meta name="DC.Description" content="Lexikon &uuml;ber Philosophie&amp;Theologie">
<meta name="DC.Language" content="de">
<meta name="DC.Type" content="Text">
<meta name="DC.Format" content="text/html">
<meta name="DC.Identifier" content="http://www.kath.de/lexikon/philosophie_theologie/">
<meta name="keywords" lang="de" content="Gott, Wissen, Glauben, Annahme, Naturwissenschaften, Physik, Skeptiker, Lichtjahre, Milchstra�e, Freiheit">
</HEAD>
<BODY bgColor=#ffffff leftMargin=6 topMargin=6 marginheight="6" marginwidth="6">
<TABLE cellSpacing=0 cellPadding=6 width="100%" border=0>
  <TBODY>
  <TR>
    <TD vAlign=top align=left width=100> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b>Philosophie&amp;Theologie</b></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td> <?php include("logo.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10"> <?php include("az.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </TD>
    <TD vAlign=top rowSpan=2>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
        <TBODY>
        <TR vAlign=top align=left>
          <TD width=8><IMG height=8 alt="" 
            src="boxtopleftcorner.gif" width=8></TD>
          <TD background=boxtop.gif><IMG height=8 alt="" 
            src="boxtop.gif" width=8></TD>
          <TD width=8><IMG height=8 alt="" 
            src="boxtoprightcorner.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxtopleft.gif><IMG height=8 alt="" 
            src="boxtopleft.gif" width=8></TD>
          <TD bgColor=#e2e2e2> 
            <H1><font face="Arial, Helvetica, sans-serif">Glauben und Wissen</font></H1>
          </TD>
          <TD background=boxtopright.gif><IMG height=8 
            alt="" src="boxtopright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=13 alt="" src="boxdividerleft.gif" 
            width=8></TD>
          <TD background=boxdivider.gif><IMG height=13 
            alt="" src="boxdivider.gif" width=8></TD>
          <TD><IMG height=13 alt="" 
            src="boxdividerright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxleft.gif><IMG height=8 alt="" 
            src="boxleft.gif" width=8></TD>
          <TD class=L12>
            <P><STRONG><font face="Arial, Helvetica, sans-serif">Folgt
                  der Glaube auf das Wissen oder ruht das Wissen auf dem Glauben?</font></STRONG></P>
            <P><font face="Arial, Helvetica, sans-serif">Wissen scheint das Sichere
                zu sein, Glauben gilt als Annahme und daher nicht als so sicher.
                Die Religionen verlangen aber nicht
              nur ein Wissen, sondern einen sicheren Glauben, der sich nicht
              nur auf das Erfahrbare in dieser Welt bezieht, sondern die eigene
              Existenz &uuml;ber den Tod hinaus als sicher annimmt. Glaube scheint &uuml;berhaupt
              dem, was &uuml;ber das Gelingen des Lebens entscheidet, n&auml;her
              zu sein. So wird eine Partnerschaft oder Freundschaft nur gl&uuml;cken,
              wenn sich die Freunde und Partner vertrauen. Vertrauen fordert
              aber mehr als nur Wissen. Umstritten ist auch, ob die Existenz
              Gottes geglaubt werden mu&szlig; oder gewu&szlig;t werden kann.
              Wie kann man in der Frage einen Schritt weiter kommen? Kommt zuerst
              das Wissen und sozusagen als Zugabe der Glaube? Oder liegt allem
              ein Vertrauen zugrunde, au dem das Wissen erst aufbauen kann. Gehen
              wir vom Wissen aus. Was leistet es?</font></P>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Erkenntnis ist nie direkt, weder in den Naturwissenschaften noch
              im Alltag</strong><br>
              Wissen hat Gro&szlig;es geleistet, vor allem die Naturwissenschaften.
              Viele Krankheiten k&ouml;nnen geheilt werden und ohne Wissenschaft
              w&auml;re der Mensch nicht zum Mond gekommen. Wir k&ouml;nnen uns
              auf die Wissenschaft verlassen, auch wenn sie uns die Atombombe
              und das Sportdoping gebracht hat. Wie kommen wir aber zu sicherem
              Wissen? Wissenschaftliche Erkenntnisse kommen nicht einfachhin
              zustande. Theorien, wie z.B. die Quanten- und Relativit&auml;tstheorie
              m&uuml;ssen entwickelt werden, um auf Grund der Theorien entsprechende
              Experimente anstellen zu k&ouml;nnen. Es mu&szlig; viel gerechnet
              und experimentiert werden. Denn wissenschaftliche Erkenntnisse
              liegen nicht einfach auf der Hand. Das k&ouml;nnen wir aus unserem
              allt&auml;glichen Umgang mit Wissen bereits verst&auml;ndlich machen.
              Wir wissen, wo der T&uuml;rgriff ist, mit welchem Schalter wir
              das Licht im Wohnzimmer anmachen, wo etwas im Kleiderschrank h&auml;ngt.
              Das scheint uns unmittelbar erkennbar zu sein. Jedoch k&ouml;nnen
              wir uns vertun, wenn wir das Licht im Bad anz&uuml;nden wollen
              oder meinen, der Autoschl&uuml;ssel liege da, aber wir finden ihn
              nicht, denn er liegt woanders. So wie bei den gro&szlig;en physikalischen
              Theorien ist auch unser allt&auml;gliches Wissen ist zusammengesetzt.
              Meist &#8222;wissen&#8220; wir nur durch Vergleich mit unserer
              Erinnerung. Den welcher Schalter f&uuml;r das Licht im Flur ist
              und welcher f&uuml;r die Toilette, das wissen wir nur, indem wir
              das, was wir gerade sehen, mit unserer Erinnerung abgleichen. Wir
              k&ouml;nnen uns das auch an unseren Tr&auml;umen klar machen. Manchmal
              habe ich etwas &uuml;ber einen anderen Mensch getr&auml;umt. Wenn
              ich ihn nach einiger Zeit treffe, mu&szlig; ich mich erst versichern:
              Habe ich getr&auml;umt, da&szlig; der andere am Meer oder in Italien
              war? Kann ich ihn dazu fragen oder war es nur ein Traum? Das kommt
              sicher ganz selten vor, zeigt uns aber, da&szlig; wir uns vergewissern
              m&uuml;ssen, ob wir etwas getr&auml;umt oder ob wir es wirklich
              erlebt haben. Wenn wir etwas Schreckliches getr&auml;umt haben,
              sind wir emotional davon manchmal so besetzt, da&szlig; wir selbst
              sagen m&uuml;ssen: &#8222;Das war doch blo&szlig; getr&auml;umt.&#8220; Wissen
              funktioniert also nicht direkt, sondern immer aus dem Zusammenhang
              heraus. Wir ordnen Neues in bisherige Erfahrungen ein und schlie&szlig;en
              daraus, da&szlig; es so ein mu&szlig;. Das funktioniert im Alltag
              wie von selbst, so da&szlig; wir uns dessen nicht bewu&szlig;t
              sind. In der Wissenschaft braucht es dazu ausgefeilte Methoden.
              Dort hat man deshalb das Prinzip aufgestellt, da&szlig; jede Beobachtung,
              soll sie in der wissenschaftlichen Welt G&uuml;ltigkeit haben,
              von anderen Wissenschaftlern nachgerechnet und durch Experimente
              wiederholt werden k&ouml;nnen mu&szlig;. Ob in der Wissenschaft
              oder im Alltag: wir erkennen nicht direkt, sondern immer in mehreren
              Stufen und indem wir die neuen Eindr&uuml;cke mit anderen Erkenntnissen
              verkn&uuml;pfen. Denn unser Erkennen geht von Sinneseindr&uuml;cken
              aus. Diese m&uuml;ssen interpretiert werden. Das wird im zwischenmenschlichen
              Bereich noch wichtiger.</font></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Keine direkte Erkenntnis des anderen Menschen</strong><br>
  Wir sind in unserem Leben zu einem gro&szlig;en Teil von anderen
              Menschen abh&auml;ngig. Wir wollen wissen, wer der andere ist.
              Um andere auf den ersten Blick einzusch&auml;tzen, nutzen wir unbewu&szlig;t
              viele Vorurteile. Hat jemand schwarze Hautfarbe, sind wir meist
              vorsichtiger als wenn wir einem Menschen mit blonden Haaren begegnen.
              Erkennen wir an der Farbe der Augenbrauen, da&szlig; die Haare
              nur blond gef&auml;rbt sind, werden wir evtl. mi&szlig;trauisch,
              ob der Gegen&uuml;ber vielleicht unehrlich ist. Die Beispiele lie&szlig;en
              sich beliebig vermehren. Sie zeigen, da&szlig; wir vom anderen
              Menschen, wer er wirklich ist, keine direkte Erkenntnis haben.
              Wir m&uuml;ssen ihn n&auml;her kennenlernen. Erst wenn er von sich
              erz&auml;hlt, kommen wir ihm n&auml;her. Er wird aber nur erz&auml;hlen,
              wenn er uns vertraut, da&szlig; wir das, was er uns von sich erz&auml;hlt
              hat, nicht einfach weiter verbreiten. Wenn der andere sich traut,
              von einem Mi&szlig;erfolg zu berichten, mu&szlig; er uns vertrauen
              k&ouml;nnen, da&szlig; wir das nicht seinen Kollegen oder Nachbarn
              weiter erz&auml;hlen.</font></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Alternative des Skeptizismus</strong><br>
  Die Philosophen besch&auml;ftigen sich in der Erkenntnistheorie
              mit den M&ouml;glichkeiten und Grenzen der menschlichen Erkenntnisf&auml;higkeit.
              Nicht wenige sind dabei zu Skeptikern geworden. Zwar gen&uuml;gen
              unsere Erkenntniskr&auml;fte, um das Licht im Bad anzuschalten
              und beim B&auml;cker Br&ouml;tchen einzukaufen. &Uuml;ber die grundlegenden
              Fragen der menschlichen Existenz ist jedoch mit den Mitteln unseres
              Erkenntnisverm&ouml;gens kaum etwas Sicheres auszumachen. Worin
              besteht der Sinn des Lebens? Soll ich wirklich die sittlichen Gebote
              befolgen oder ist es nicht besser f&uuml;r mich, den eigenen Vorteil
              an die erste Stelle zu setzen? Was geschieht mit mir, wenn ich
              sterbe? Verlangt dann etwa Gott Rechenschaft von meinem Leben?
              Mu&szlig; ich, wenn ich mich mit diesen Fragen besch&auml;ftige,
              in die Welt des Glaubens springen und die Sicherheit der wissenschaftlichen
              Erkenntnisse verlassen? Im Folgenden wird gezeigt, da&szlig; der
              Glaube nicht erst bei den Lebensthemen anf&auml;ngt, sondern jede
              Wissenschaft beruht auf bestimmten Annahmen, hat einen Glauben
              als Fundament. Nicht erst, wenn wir unser Inneres einem anderen
              Menschen er&ouml;ffnen, &#8222;glauben&#8220; wir, indem wir ihm
              vertrauen. Wir vertrauen bereits der Wirklichkeit, wenn wir Physik
              treiben.</font></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Jedes Wissen
                  ruht auf Annahmen &uuml;ber die Wirklichkeit</strong><br>
  Die Naturwissenschaften, wie sie sich seit Galilei entwickelt haben,
                gehen von der Pr&auml;misse aus, da&szlig; den Naturvorg&auml;ngen
                gleichbleibende Gesetze unterliegen, die mit mathematischen Formeln
                beschrieben werden k&ouml;nnen. Bisher mu&szlig;te die Wissenschaft
                diese, ihre Voraussetzungen, nicht in Zweifel ziehen. Es l&auml;&szlig;t
                sich allerdings wissenschaftlich nicht &#8222;beweisen&#8220;,
                da&szlig; morgen die Sonne aufgeht. Aber wir k&ouml;nnen davon
                ausgehen. Da&szlig; die Physik Annahmen machen mu&szlig;, die
                sie nicht beweisen kann, zeigt folgende &Uuml;berlegung: Wenn
                wir das Licht von einer Milchstra&szlig;e registrieren, die 10
                Milliarden Lichtjahre entfernt liegt, nehmen wir an, da&szlig; die
                physikalischen Gesetze auch dort heute noch so gelten wie vor
                10 Milliarden Lichtjahre, als die Milchstra&szlig;e das Licht
                losschickte. Es gibt keine begr&uuml;ndeten Zweifel gegen diese
                Annahme, aber beweisen kann das niemand, ob die physikalischen
                Gesetze heute genau so gelten. Denn wir wissen nur etwas, was
                vor 10 Milliarden dort ablief. &Uuml;ber das Geschehen, das sich
                heute in der Lichtstra&szlig;e abspielt, k&ouml;nnen wir nichts
                sagen. Die Naturgesetze k&ouml;nnten morgen aufh&ouml;ren zu
                gelten. Das ist sicher unwahrscheinlich, aber m&ouml;glich. Nicht
                nur die Naturwissenschaftler gehen von der Annahme aus, da&szlig; die
                Gesetze der Gravitation morgen genauso gelten wie heute, die
                Erde sich weiter um ihre Achse dreht und die Sonne die Erde durch
                die Gravitation in der Umlaufbahn h&auml;lt. <br>
                Da&szlig; die strikt empirisch arbeitenden Naturwissenschaften
              Vieles leisten, jedoch schon f&uuml;r das menschliche Zusammenleben
              keine ausreichende Basis liefern, zeigen folgende Beispiele. In
              unserem Erfahrungsbereich gibt es Vieles, das nicht mathematisch
              berechenbar ist. Die menschliche Seele l&auml;&szlig;t sich nicht
              mathematisieren. Was man messen kann, ist das Gewicht des Toten
              im Vergleich zum lebenden K&ouml;rper. Es sind etwa 21 g, die der
              Tote leichter ist. Was wird aber damit gemessen? Wir k&ouml;nnen
              mit den naturwissenschaftlichen Methoden auch nicht nachpr&uuml;fen,
              ob eine b&ouml;se Tat in unserem K&ouml;rper etwas ver&auml;ndert.
              Aber unsere Taten haben offensichtlich nicht nur nachpr&uuml;fbare
              Wirkungen nach au&szlig;en, sondern wirken auf uns selbst zur&uuml;ck.
              Ver&auml;ndern sie unseren K&ouml;rper me&szlig;bar oder nur unsere
              Seele? <br>
              Die wenigen &Uuml;berlegungen zeigen, da&szlig; wir immer von Annahmen
              ausgehen, ehe wir etwas wissen. Nur im Horizont der Annahme, da&szlig; die
              physikalischen Gesetze im ganzen Weltall gelten, machen wir einzelne
              Beobachtungen, f&uuml;hren Experimente durch, konstruieren Raumsonden,
              die wir ins Weltall losschicken. Da&szlig; wir davon ausgehen,
              in einer Welt zu existieren, in der alles aufeinander abgestimmt
              ist, setzt die Annahme voraus, da&szlig; diese Welt eine Einheit
              ist, in der an jedem Ort die gleichen Naturgesetze gelten. Die
              Naturwissenschaften beweisen uns diese Annahme nicht, sie funktionieren
              in dieser Welt, ohne die Welt jemals als Ganze erkl&auml;ren zu
              k&ouml;nnen. Die Annahmen sind bereits ein Glaube, der unser Fragen
              weiterleitet, n&auml;mlich wo die Welt herkommt, was der Ursprung
              des geistigen Lebens ist, welchen Sinn das Ganze haben soll. Diese
              Grundannahme, da&szlig; die Welt etwas Sinnvolles ist, best&auml;tigt
              sich bereits in den Naturgesetzen. Sie weckt die Frage nach einem
              Urheber und f&uuml;hrt damit zu den Versuchen, Gott mit den menschlichen
              Verstandeskr&auml;ften zu erkennen. Die <a href="gottesbeweise.php">Gottesbeweise</a> sind die
              Denkwege, die nach dem Ursprung und der letzten Sinngebung fragen.<br>
              Es ist deutlich: wir gehen zuerst von Annahmen aus, d.h. wir &#8222;glauben&#8220; zuerst
              etwas und kommen durch Zusammenf&uuml;gen von Beobachtungen und &Uuml;berlegen
              zu Wissen. In den Naturwissenschaften geschieht das &Uuml;berlegen
              in der Form der Mathematik. Auch das Wissen &uuml;ber Gott baut
              sich in unserem Leben langsam auf. </font></p>
            <p><font face="Arial, Helvetica, sans-serif">Eckhard Bieger</font><br>&copy;
              <a href="http://www.kath.de"> <font face="Arial, Helvetica, sans-serif">www.kath.de</font></a></p>
            </TD>
          <TD background=boxright.gif><IMG height=8 alt="" 
            src="boxright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=8 alt="" src="boxbottomleft.gif" 
            width=8></TD>
          <TD background=boxbottom.gif><IMG height=8 alt="" 
            src="boxbottom.gif" width=8></TD>
          <TD><IMG height=8 alt="" src="boxbottomright.gif" 
            width=8></TD></TR></TBODY></TABLE></TD><td style='vertical-align:top;'>
 <script type="text/javascript"><!--
        google_ad_client = "pub-9537752113242914";
        google_ad_width = 336;
        google_ad_height = 280;
        google_ad_format = "336x280_as";
        google_ad_type = "text_image";
        //2006-10-23: Lexikon Symbole
        google_ad_channel = "8495253357";
        //--></script>
        <script type="text/javascript"
          src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
        </script>
</td>
</TR>
  <TR>
    <TD vAlign=top align=left>&nbsp; </TD>
  </TR></TBODY></TABLE></BODY></HTML>
