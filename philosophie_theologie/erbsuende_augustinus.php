<HTML><HEAD><TITLE>Erbs&uuml;nde und Augustinus</TITLE>
<META http-equiv=Content-Type content="text/html; charset=iso-8859-1"><LINK 
title=fonts href="kaltefleiter.css" type=text/css 
rel=stylesheet>
<META content="MSHTML 5.50.4134.600" name=GENERATOR></HEAD>
<BODY bgColor=#ffffff leftMargin=6 topMargin=6 marginheight="6" marginwidth="6">
<TABLE cellSpacing=0 cellPadding=6 width="100%" border=0>
  <TBODY>
  <TR>
    <TD vAlign=top align=left width=100> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b>Philosophie&amp;Theologie</b></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td> <?php include("logo.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10"> <?php include("az.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </TD>
    <TD vAlign=top rowSpan=2>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
        <TBODY>
        <TR vAlign=top align=left>
          <TD width=8><IMG height=8 alt="" 
            src="boxtopleftcorner.gif" width=8></TD>
          <TD background=boxtop.gif><IMG height=8 alt="" 
            src="boxtop.gif" width=8></TD>
          <TD width=8><IMG height=8 alt="" 
            src="boxtoprightcorner.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxtopleft.gif><IMG height=8 alt="" 
            src="boxtopleft.gif" width=8></TD>
          <TD bgColor=#e2e2e2> 
            <H1><font face="Arial, Helvetica, sans-serif">Erbs&uuml;nde und Augustinus</font></H1>
          </TD>
          <TD background=boxtopright.gif><IMG height=8 
            alt="" src="boxtopright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=13 alt="" src="boxdividerleft.gif" 
            width=8></TD>
          <TD background=boxdivider.gif><IMG height=13 
            alt="" src="boxdivider.gif" width=8></TD>
          <TD><IMG height=13 alt="" 
            src="boxdividerright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxleft.gif><IMG height=8 alt="" 
            src="boxleft.gif" width=8></TD>
          <TD class=L12>
            <P><STRONG><font face="Arial, Helvetica, sans-serif">Augustinus hat
                den Begriff Erbs&uuml;nde zuerst entwickelt</font></STRONG></P>
            <P><font face="Arial, Helvetica, sans-serif">Erbs&uuml;nde besagt, da&szlig; S&uuml;nde sich vererbt. Kann
              Gott das wollen? Wie kam es &uuml;berhaupt zu dem Begriff. Augustinus
              von Hippo, (354-430), der gro&szlig;e Theologe der Westkirche,
              von dem Luther sich inspirieren lie&szlig;t, entwickelte die Vorstellung
              der Erbs&uuml;nde.<br>
              Der &#8222;geistigen Vater&#8220; der Erbs&uuml;ndenlehre war zuerst
              Anh&auml;nger der Manich&auml;er, ehe er sich zum Christentum bekehrte.
              Seine Mutter Monika, selbst Christin, hatte Jahre f&uuml;r die
              Bekehrung des Sohnes gebet. Augistinung stammte aus Nordafrika
              und wurde Bischof von Hippo, wo er u.a. den Sturm der Vandalen
              erlebte.<br>
              Am Anfang seiner Lehre steht nicht &#8211; wie man vielleicht erwarten
              k&ouml;nnte &#8211; eine abstrakte &Uuml;berlegung &uuml;ber die
              S&uuml;ndhaftigkeit der ganzen Menschheit o.&auml;. im Mittelpunkt,
              sondern ein ganz konkretes exegetisches Problem: die Auslegung
              von R&ouml;m. 9,10-13 <br>
              <br>
&#8222;
              [10] So war es aber nicht nur bei ihr [Sara], sondern auch bei
              Rebecca: Sie hatte von einem einzigen Mann empfangen, von unserem
              Vater Isaak, [11] und ihre Kinder waren noch nicht geboren und
              hatten weder Gutes noch B&ouml;ses getan; damit aber Gottes freie
              Wahl und Vorherbestimmung g&uuml;ltig bleibe, [12] nicht abh&auml;ngig
              von Werken, sondern von ihm, der beruft, wurde ihr gesagt: Der &Auml;ltere
              mu&szlig; dem J&uuml;ngern dienen; [13] denn es steht in der Schrift:
              Jakob habe ich geliebt, Esau aber geha&szlig;t.&#8220;</font></P>
            <p><font face="Arial, Helvetica, sans-serif"><br>
  1. Schritt der &Uuml;berlegung: gott wei&szlig;, wie ein Menshc
              sich entscheiden wird<br>
              Die entscheidende Frage in Bezug auf R&ouml;m 9,10-13 war f&uuml;r
              Augustinus, wie man erkl&auml;ren k&ouml;nne, dass Gott von zwei
              Ungeborenen den einen liebe, den anderen aber hasse, ohne Gott
              vorwerfen zu m&uuml;ssen, er sei ungerecht, was in Augustins Augen
              selbstverst&auml;ndlich absurd w&auml;re. In seiner Schrift &#8222;Expositio
              quarundam propositionum ex epistola ad Romanos&#8220; (394) l&ouml;st
              der Kirchenvater das Problem dadurch, dass er als Grund f&uuml;r
              die Erw&auml;hlung des Jakob und die Verwerfung des Esau Gottes
              Vorherwissen, wie beide einmal beschaffen sein werden, angibt.
              Fest steht f&uuml;r den Kirchenvater, dass es ein Unterscheidungskriterium
              zwischen beiden geben m&uuml;sse, sonst k&ouml;nne es keine gerechtfertigte
              Erw&auml;hlung bzw. Verwerfung geben. Fragt man nun, was konkret
              Gott erw&auml;hle, so lautet die Antwort, nicht bestimmte Werke
              (vgl. R&ouml;m. 9,12), sondern den vorausgewussten Glauben, denn
              dieser sei das Verdienst des Menschen. Gott wusste also voraus,
              dass Jakob glauben w&uuml;rde, deshalb erw&auml;hlte und liebte
              Gott ihn, genauso, wie er vorauswusste, dass Esau nicht glauben
              w&uuml;rde, weshalb er diesen verwarf und hasste. Es ist somit
              letztlich der Mensch selbst, der &uuml;ber sein Heil oder Unheil
              entscheidet, denn er ist frei zu Glauben oder nicht zu glauben. </font></p>
            <p><font face="Arial, Helvetica, sans-serif">Es gibt keine Verdienste des Menschen<br>
  Circa zwei Jahre sp&auml;ter in seiner Schrift &#8222;Ad Simplicianum&#8220; (396)
              besch&auml;ftigt sich Augustinus erneut mit R&ouml;m 9,10-13 und
              kommt dabei zu dem Ergebnis, dass auch der Glaube des Menschen
              nicht als dessen Verdienst betrachtet werden d&uuml;rfe, da der
              Mensch alles der zuvorkommenden Gnade Gottes verdanke. Wenn aber
              der Mensch somit &uuml;berhaupt keine Verdienstm&ouml;glichkeit
              mehr besitzt und deshalb alle Menschen gleich sind, dann stellt
              sich erneut die Frage nach dem Grund der Erw&auml;hlung Jakobs.
              Diese l&auml;sst sich noch relativ leicht als reines und absolut
              freies Gnadengeschenk Gottes erkl&auml;ren, allem Tun und Wollen
              des Menschen zuvorkommend, wodurch Augustinus die Souver&auml;nit&auml;t
              und Gr&ouml;&szlig;e Gottes geb&uuml;hrend gew&uuml;rdigt sieht.
              Schwieriger verh&auml;lt es sich jedoch bei Esau. Wie l&auml;sst
              sich dessen Verwerfung einsichtig machen, ohne Gott den Vorwurf
              der Ungerechtigkeit aussetzten zu m&uuml;ssen? Naheliegend w&auml;re
              nun der Erkl&auml;rungsversuch, Esau habe das freie Gnadenangebot
              Gottes abgelehnt und deshalb seine Verwerfung verdient. Augustinus
              erkennt aber, dass eine solche Argumentation nicht greift, da der
              Text explizit von Ungeborenen spricht, die weder etwas wollen,
              noch etwas nicht wollen konnten, als der eine bereits erw&auml;hlt,
              der andere schon verworfen war. Der Kirchenvater sah sich somit
              dem Dilemma ausgesetzt, einerseits einen (gerechten) Grund f&uuml;r
              die Verwerfung des ungeborenen Esau finden zu m&uuml;ssen, andererseits
              aber bereits erkl&auml;rt zu haben, dass die Gnadenwahl Gottes
              v&ouml;llig frei und unabh&auml;ngig von allem Tun und Wollen des
              Menschen erfolge. <br>
              Um dieses Dilemma l&ouml;sen zu k&ouml;nnen entwickelt er sukzessive
              die Lehre von der Erbs&uuml;nde, die besagt, dass die ganze Menschheit
              eine &#8222;einzige S&uuml;ndenmasse&#8220; (massa peccati) darstelle.
              Begr&uuml;ndet sieht Augustinus dies in R&ouml;m 5,12 (&#8222;in
              quo omnes peccaverunt&#8220;), was er f&auml;lschlich mit &#8222;in
              dem (= in Adam) alle s&uuml;ndigten&#8220; &uuml;bersetzt, anstatt
              korrekt mit &#8222;weil alle s&uuml;ndigten&#8220;, wie aus der
              griechischen &Uuml;bersetzung des Alten Testaments, der Septuaginta,
              hervorgeht. Alle Menschen haben &#8222;in Adam&#8220;, dem Stammvater
              der ganzen Menschheit ges&uuml;ndigt und verdienen damit als gerechte
              Strafe die ewige Verdammnis. Es steht dabei Gott selbstverst&auml;ndlich
              frei, diese Strafe den einen nachzulassen, sie aber von den anderen
              einzufordern. Das eine ist Ausdruck seiner Barmherzigkeit, das
              andere ist Ausdruck seiner Gerechtigkeit. Stellt man nun die Frage,
              warum Gott aus der gro&szlig;en S&uuml;ndenmasse der Menschheit
              nur einige wenige zum Heil erw&auml;hlt und vorherbestimmt, w&auml;hrend
              er dem gr&ouml;&szlig;ten Teil seine rechtfertigende Gnade verweigert &#8211; wovon
              der Kirchenvater &uuml;berzeugt ist &#8211; dann k&ouml;nne nach
              Augustinus nur geantwortet werden, dass der Mensch dies nicht einsehen
              k&ouml;nne, sondern darauf vertrauen m&uuml;sse, dass dies einer
              nur Gott einsichtigen Gerechtigkeit entspreche. <br>
&#8222;
              Es bilden also alle Menschen &#8211; zumal da nach dem Wort des
              Apostels &#8218;in Adam alle sterben&#8217; (1Kor 15,22), von dem
              sich f&uuml;r das gesamte Menschengeschlecht der Ursprung der Beleidigung
              Gottes herleitet &#8211; eine einzige S&uuml;ndenmasse, die der
              h&ouml;chsten, g&ouml;ttlichen Gerechtigkeit Strafe schuldet. Wenn
              sie eingetrieben oder nachgelassen wird, so bedeutet beides kein
              Unrecht.&#8220; <br>
              [Augustinus, Simpl. 1,2,16; Aurelius Augustinus. Der Lehrer der
              Gnade. Gesamtausgabe seiner antipelagianischen Schriften. Prolegomena
              3 (Lateinisch- Deutsch). Eingel., &uuml;bertr. und erl. von Thomas
              Gerhard Ring (=ALG 3), W&uuml;rzburg 1991]<br>
&#8222;
              Ist Gott nicht ungerecht, wenn er trotz seiner Allmacht nur einige
              erw&auml;hlt? Ausgehend von dem unersch&uuml;tterlichen Axiom,
              da&szlig; es in Gott keinerlei Ungerechtigkeit gibt, kann Augustinus
              eine L&ouml;sung nur darin finden, da&szlig; er den Menschen v&ouml;llig
              rechtlos macht. Das Instrument zur Entm&uuml;ndigung des Menschen
              ist ein &#8211; von ihm neu geschaffenes &#8211; biologisches Verst&auml;ndnis
              der Erbs&uuml;nde und eine krasse Schlu&szlig;folgerung daraus:
              Die Menschen sind ein Masse der S&uuml;nde, die rechtens einzig
              und allein Verdammung verdient.&#8220; <br>
              [Georg Kraus, Gnadenlehre &#8211; Das Heil als Gnade, in: Wolfgang
              Beinert (Hg.), Glaubenszug&auml;nge. Lehrbuch der katholischen
              Dogmatik (Bd. 3), Paderborn 1995, 157-305]</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Zitate<br>
&#8222;
              Der tragende Gedankengang, den Augustinus in &#8222;Ad Simplicianum&#8220; ausf&uuml;hrt,
              ist daher der: Wenn alles Positive, Berufung und Erw&auml;hlung,
              von Gott abh&auml;ngen, und wenn zudem nicht alle erw&auml;hlt
              werden, so mu&szlig; der Grund hierf&uuml;r in der Menschheit selber
              liegen, denn sonst m&uuml;&szlig;te gegen Gott der Vorwurf der
              Ungerechtigkeit erhoben werden. So wenig aber ein Gl&auml;ubiger
              ungerecht ist, wenn er dem einen Schuldner seine Schuld erl&auml;&szlig;t
              aus Gnade, dem anderen aber nicht, so wenig kann Gott ein Vorwurf
              daraus gemacht werden, da&szlig; er nicht alle rechtfertigt und
              nicht allen das Heil gibt. [&#8230;] Wenn Gott den einen erw&auml;hlen,
              den anderen aber verwerfen wollte, er aber dabei nicht auf Verdienstunterschiede
              sehen konnte, weil sie ja letztlich von Gott kommen, dann blieb
              offensichtlich nur die M&ouml;glichkeit der Annahme, da&szlig; alle
              Menschen von Anfang an, und das hei&szlig;t: durch Adams S&uuml;nde
              der Verwerfung schuldig sind &#8211; so da&szlig; Gott nun dadurch
              seine Gnade zeigen k&ouml;nne, da&szlig; er aus dieser &#8222;massa&#8220; einige
              zum Heile erw&auml;hlte, andere nicht&#8220;. <br>
              [Walter Simonis, Heilsnotwendigkeit der Kirche und Erbs&uuml;nde
              bei Augustinus, in: Carl Andresen (Hg.), Zum Augustin-Gespr&auml;ch
              der Gegenwart (WdF 327), Darmstadt 1981, 301-328]</font></p>
            <p> </p>
            <p><font size="2" face="Arial, Helvetica, sans-serif">Robert Walz</font><br>
            </p>
            <P>&copy;<a href="http://www.kath.de"> <font face="Arial, Helvetica, sans-serif">www.kath.de</font></a></P>
            </TD>
          <TD background=boxright.gif><IMG height=8 alt="" 
            src="boxright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=8 alt="" src="boxbottomleft.gif" 
            width=8></TD>
          <TD background=boxbottom.gif><IMG height=8 alt="" 
            src="boxbottom.gif" width=8></TD>
          <TD><IMG height=8 alt="" src="boxbottomright.gif" 
            width=8></TD></TR></TBODY></TABLE></TD><td style='vertical-align:top;'>
 <script type="text/javascript"><!--
        google_ad_client = "pub-9537752113242914";
        google_ad_width = 336;
        google_ad_height = 280;
        google_ad_format = "336x280_as";
        google_ad_type = "text_image";
        //2006-10-23: Lexikon Symbole
        google_ad_channel = "8495253357";
        //--></script>
        <script type="text/javascript"
          src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
        </script>
</td>
</TR>
  <TR>
    <TD vAlign=top align=left>&nbsp; </TD>
  </TR></TBODY></TABLE></BODY></HTML>
