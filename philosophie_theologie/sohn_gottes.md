---
title: Sohn Gottes
author: 
tags: 
created_at: 
images: []
---


# **Filius Dei, Hyios Theou*
***Gott hat viele Söhne und Töchter, denn seit Jesus seinen Jüngern das „Vater unser“ vorgebetet hat, verstehen sich die Christen als Kinder Gottes. Ist Jesus aber in gleicher Weise Sohn wie die in der Taufe von Gott als Kinder Angenommenen?  

# **Kann der Sohn Gottes geschaffen sein?**
# *Es gibt eine Begebenheit am Beginn des öffentlichen Wirkens Jesu, die schon früh so interpretiert wurde, daß Jesus von Gott in eine besondere Sohnschaft hinein genommen wurde, sozusagen adoptiert worden ist. Nach den vier Evangelien begibt sich der erwachsen gewordene Jesus zu dem Bußprediger Johannes, der unten im Jordantal den Menschen ins Gewissen redet, sie zur Umkehr auffordert und eine Bußtaufe spendet. Johannes erkennt in Jesus den Messias und will ihn nicht taufen. Doch Jesus unterzieht sich der Bußtaufe und hört danach eine Stimme. Markus berichtet: „Und als er aus dem Wasser steigt, sah er, daß der Himmel sich öffnete und der Geist wie eine Taube auf ihn herabkam. Und eine Stimme aus dem Himmel sprach: Du bist mein geliebter Sohn, an dir habe ich Gefallen.“ Kap 1,10-11**
 Diese Szene wurde schon früh so interpretiert, daß Gott Jesus wie einen Propheten beruft und ihn ***[als Sohn annimmt](christologische_streitigkeiten.php). Hintergrund sind auch Inthronisationsriten jüdischer Könige, in denen der König „Sohn Gottes“ genannt wird. Im 2. Psalm spricht Gott:***


„ Ich selbst habe meinen König eingesetzt, auf Zion, meinem heiligen Berg.“ Der Beter fährt dann fort: „Den Beschluß des Herrn will ich kundtun. Er sprach zu mir: Mein Sohn bist du. Heute habe ich dich gezeugt. Fordere von mir und ich gebe dir die Völker zum Erbe, die Enden der Erde zum Eigentum.“ (Verse 6-8) Dieser Psalm wurde schon von den Jüngern auf Jesus übertragen, daß Gott ihn mit der Auferstehung zum Herrn der ganzen Welt inthronisiert hat. Er wird heute noch in der Osterliturgie gebetet. ***

 Die Ebioniten, eine aus dem Judentum hervorgegangene christliche Gruppe, hat diese Auffassung vertreten. Jesus wäre demnach ein besonderer Prophet, herausgehoben sogar gegenüber den Großen des Alten Testaments, aber nicht wesenhaft von ihnen verschieden. Diese Frage brach im Streit mit Arius neu auf. Dieser betrachtete Jesus als den menschgewordenen Logos. Der Logos existierte schon vor der Geburt Jesu, aber er war ein Geschöpf. Bei ***[Arius](arianismus.php) stand weniger das Modell der Prophetenberufung im Hintergrund als die philosophische Überzeugung der Griechen, daß Gott nur einer sein kann und daher in Gott nicht mehrere Personen gedacht werden können.  

# **Jesus nennt Gott seinen Vater**
# *Die Frage wurde auf dem Konzil von ***[Nicäa 325](gottessohn.php) entschieden, daß Jesus kein Geschöpf ist, sondern aus dem Vater hervorgegangen und damit Gott wesensgleich ist. Wie können wir Zugang zu dieser Aussage des Konzils finden, das sich auf Aussagen der biblischen Schriften stützt. Matthäus überliefert das Bekenntnis des Petrus, der auf die Frage Jesu „Ihr aber, für wen haltet ihr mich?“ antwortet:**
 "Du bist der Messias, der Sohn des lebendigen Gottes! Jesus sagte zu ihm: Selig bist du, Simon, Sohn des Jona; denn nicht Fleisch und Blut haben dir das offenbart, sondern mein Vater im Himmel.“ (Kap. 16,15-17)***

 Dieses Bekenntnis ist Petrus nicht in diesem Moment eingefallen, sondern daß Jesus der Sohn des lebendigen Gottes ist, hat er am Sprechen und der inneren Beziehung Jesu zu Gott abgelesen. Jesus hat sich selbst nur selten Sohn genannt, aber sehr häufig von Gott als seinem Vater gesprochen.***

 In den Evangelien nennt Jesus 174mal Gott Abba, die im Aramäischen gebräuchliche vertraute Anrede für den Vater, so wie unser Papa. Weil er mit Gott wie ein Sohn mit seinem Vater lebt und spricht, kann er seine Jünger in seine Beziehung zu Gott hinein nehmen, so daß diese bis heute das „Vater unser“ sprechen können. So kann er in seiner Einführung in das Beten Gott als den Vater vorstellen:***

 Die Gläubigen sollen sich nicht an die Straßenecken stellen, damit die anderen sehen, wie sie beten. „Du aber geh in deine Kammer, wenn du betest, und schließ die Tür zu; dann bete zu deinem Vater, der im Verborgenen ist.“ Weiter heißt es bei Matthäus:***


„ Wenn ihr betet, sollt ihr nicht plappern wie die Heiden, die meinen, sie werden nur erhört, wenn sie viele Worte machen. Macht es nicht wie sie; denn euer Vater weiß, was ihr braucht, noch ehe ihr bittet. So sollt ihr beten:***

 Vater unser im Himmel, dein Name werde geheiligt;***

 Dein Reich komme, dein Wille geschehe, wie im Himmel, so auf Erden.***

 Gib uns heute das Brot, das wir brauchen.***

 Und erlaß uns unsere Schuld, wie auch wir sie unseren Schuldnern erlassen haben.***

 Und führe uns nicht in Versuchung, sondern rette uns vor dem Bösen.“***

 Kap. 6, 6-13***

 Die Jünger Jesu haben ihn in dieser unmittelbaren Beziehung zu Gott erlebt und ihn so als Sohn erkennen können. Weil Jesus die Menschen in diese Beziehung hinein nimmt, können wir das „Vater unser“ als Kinder Gottes beten.***

 Paulus führt diese Gebetspraxis mit dem gleichen Wort „Abba“ weiter. Er schreibt an die Galater:***


„ Weil ihr aber Söhne seid, sandte Gott den Geist seines Sohnes in unser Herz, den Geist, der ruft: Abba, Vater. Daher bist du nicht mehr Sklave, sondern Sohn; bist du aber Sohn, dann auch Erbe, Erbe durch Gott." Kap. 4, 6-7 

# **Zitate****
 In jener Zeit sprach Jesus:***

 Ich preise dich, Vater, Herr des Himmels und der Erde, weil du all das den Weisen und Klugen verborgen, den Unmündigen aber offenbart hast. Ja, Vater, so hat es dir gefallen. Mir ist von meinem Vater alles übergeben worden; Niemand kennt den Sohn, nur der Vater, und niemand kennt den Vater, nur der Sohn und der, dem der Sohn es offenbaren will.“***

 Matthäus 11,25-27 

# **Das Gebet Jesu am Ölberg, als er in Todesangst fiel, überliefert Markus:**
 Und er nahm Petrus, Jakobus und Johannes mit sich. Da ergriff ihn Furcht und Angst, und er sagte zu ihnen: Meine Seele ist zu Tode betrübt. Bleibt hier und wacht. Und er ging ein Stück weiter, warf sich auf die Erde nieder und betete, daß die Stunde, wenn möglich, an ihm vorübergehe. Er sprach: Abba, Vater, alles ist dir möglich. Nimm diesen Kelch von mir. Aber nicht, was ich will, sondern was du willst, soll geschehen. Kap. 14,33-36 

# **Markus im Bericht über die Hinrichtung Jesu:**
 Jesus aber schrie laut auf. Dann hauchte er den Geist aus. Da riß der Vorhang im Tempel von oben bis unten entzwei Als der Hauptmann, der Jesus gegenüberstand, ihn auf diese Weise sterben sah, sagte er: Wahrhaftig, dieser Mensch war Gottes Sohn. Kap 15,37-39 

# **  Wenn also der Sohn, bevor die Welt entstand, die Herrlichkeit inne hatte und der höchste Herr der Herrlichkeit war, er vom Himmel herabstieg und weiterhin anbetungswürdig ist, so wurde er, indem er herabstieg, nicht besser, vielmehr verbesserte er das Besserungsbedürftige. Und wenn er, um zu verbessern, herabgestiegen ist, so wurde ihm dafür nicht die Belohnung als Sohn und Gott zuerkannt, sondern er machte vielmehr uns zu Söhnen des Vaters. Selbst Mensch geworden, vergöttlichte er die Menschen.**
 Athanasius, Reden gegen die Arianer (um 335) Nr. 1,38 

# **              Cyrill von Jerusalem (313-387)**
 Glaube aber auch an den Sohn Gottes, den einen und einzigen, unseren Herrn Jesus Christus, der aus Gott als Gott gezeugt wurde, der aus dem Leben als Leben erzeugt, aus dem Licht als Licht erzeugt wurde; er ist in allem dem ähnlich, der ihn gezeugt hat. Er hat nicht in der Zeit das Sein empfangen, sondern vor aller Zeit wurde er ewig und auf unbegreifliche Weise gezeugt. ***

 Katechesen 4,7 

# **Eckhard Bieger*** **
 ©***[ ***www.kath.de](http://www.kath.de)
