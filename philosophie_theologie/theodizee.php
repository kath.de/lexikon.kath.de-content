<HTML><HEAD><TITLE>Theodizee</TITLE>
<meta name="title" content="Philosophie&amp;Theologie">
<meta name="author" content="Redaktion kath.de">
<meta name="publisher" content="kath.de">
<meta name="copyright" content="kath.de">
<meta name="description" content="">
<meta name="abstract" content="Lexikon &uuml;ber Philosophie&amp;Theologie">
<meta http-equiv="content-language" content="de">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="date" content="2006-00-01">
<meta name="robots" content="index,follow">
<meta name="revisit-after" content="10 days">
<meta name="revisit" content="after 10 days">
<meta name="DC.Title" content="Philosophie&amp;Theologie">
<meta name="DC.Creator" content="Redaktion kath.de">
<meta name="DC.Contributor" content="J�rgen Pelzer">
<meta name="DC.Rights" content="kath.de">
<meta name="DC.Publisher" content="kath.de">
<meta name="DC.Date" content="2006-00-01">
<meta name="DC.Description" content="Lexikon &uuml;ber Philosophie&amp;Theologie">
<meta name="DC.Language" content="de">
<meta name="DC.Type" content="Text">
<meta name="DC.Format" content="text/html">
<meta name="DC.Identifier" content="http://www.kath.de/lexikon/philosophie_theologie/">
<meta name="keywords" lang="de" content="�bel, Hiob, Leibniz, Allmacht, Gutheit, Gott, das B�se">
</HEAD>
<BODY bgColor=#ffffff leftMargin=6 topMargin=6 marginheight="6" marginwidth="6">
<table cellspacing=0 cellpadding=6 width="100%" border=0>
  <tbody>
    <tr>
      <td valign=top align=left width=100>
        <table width="216" border="0" cellpadding="0" cellspacing="0">
          <tr valign="top" align="left">
            <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
            <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
            <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
            <td bgcolor="#E2E2E2"><b>Philosophie&amp;Theologie</b></td>
            <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
            <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
            <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
            <td>
              <?php include("logo.html"); ?>
            </td>
            <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
            <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
            <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
          </tr>
        </table>
        <br>
        <table width="216" border="0" cellpadding="0" cellspacing="0">
          <tr valign="top" align="left">
            <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
            <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
            <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
            <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
            <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
            <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
            <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
            <td class="V10">
              <?php include("az.html"); ?>
            </td>
            <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
          </tr>
          <tr valign="top" align="left">
            <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
            <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
            <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
          </tr>
        </table>
      </td>
      <td valign=top rowspan=2>
        <table cellspacing=0 cellpadding=0 width="100%" border=0>
          <tbody>
            <tr valign=top align=left>
              <td width=8><img height=8 alt="" 
            src="boxtopleftcorner.gif" width=8></td>
              <td background=boxtop.gif><img height=8 alt="" 
            src="boxtop.gif" width=8></td>
              <td width=8><img height=8 alt="" 
            src="boxtoprightcorner.gif" width=8></td>
            </tr>
            <tr valign=top align=left>
              <td background=boxtopleft.gif><img height=8 alt="" 
            src="boxtopleft.gif" width=8></td>
              <td bgcolor=#e2e2e2>
                <h1><font face="Arial, Helvetica, sans-serif">Theodizee</font></h1>
              </td>
              <td background=boxtopright.gif><img height=8 
            alt="" src="boxtopright.gif" width=8></td>
            </tr>
            <tr valign=top align=left>
              <td><img height=13 alt="" src="boxdividerleft.gif" 
            width=8></td>
              <td background=boxdivider.gif><img height=13 
            alt="" src="boxdivider.gif" width=8></td>
              <td><img height=13 alt="" 
            src="boxdividerright.gif" width=8></td>
            </tr>
            <tr valign=top align=left>
              <td background=boxleft.gif><img height=8 alt="" 
            src="boxleft.gif" width=8></td>
              <td class=L12>
                <p><strong><font face="Arial, Helvetica, sans-serif">Die Rechtfertigung
                      Gottes angesichts des &Uuml;bels in der Welt</font></strong></p>
                <p><font face="Arial, Helvetica, sans-serif">Wie kann Gott angesichts
                    des B&ouml;sen und des &Uuml;bels gerechtfertigt werden.
                    Der Begriff wurde von Leibniz erstmals in seinem Buch von
                    1710 &uuml;ber die Theodizee, die Gutheit Gottes, die Freiheit
                    des Menschen und die Ursache des B&ouml;sen. Theodizee leitet
                    sich vom Griechischen Theos (Gott) und Dikae (Gerechtigkeit)
                    her.</font></p>
                <p><font face="Arial, Helvetica, sans-serif"><strong>Der Widerspruch
                      zwischen der Allmacht Gottes und seiner Gutheit</strong><br>
                  Das Problem entsteht nicht nur denkerisch, sondern f&uuml;r
                  den Menschen, der an Gott glaubt und von einem gro&szlig;en
                  Leid oder sogar von einem Verbrechen heimgesucht wird. &#8222;Wie
                  kann Gott das zulassen?&#8220; ist die Frage. Denkerisch ergibt
                  sich das Problem, wenn man von Gott sagt, da&szlig; er nur
                  das Gute will. Wenn Gott allm&auml;chtig ist, m&uuml;&szlig;te
                  er das B&ouml;se, das &Uuml;ble, das einem Menschen widerf&auml;hrt,
                  verhindern. Entweder ist Gott gut und nicht allm&auml;chtig.
                  Dann g&auml;be es einen M&auml;chtigeren als Gott, also das
                  B&ouml;se: Das war die Bef&uuml;rchtung der Germanen, die von
                  einem Kampf zwischen dem Guten und dem B&ouml;sen ausgingen
                  und damit rechneten, da&szlig; das B&ouml;se gewinnt. Da Gott
                  nur als allm&auml;chtig gedacht werden kann, sonst ist er nicht
                  gut, mu&szlig; er offensichtlich das B&ouml;se einplanen. Wie
                  kann aber Gott dann der Gute sein. Der Widerspruch ist philosophisch
                  nicht zu l&ouml;sen. Der franz&ouml;sische Schriftsteller Stendal
                  folgert: &#8222;Die einzige Entschuldigung Gottes (angesichts
                  des &Uuml;bels in der Welt) ist, da&szlig; er nicht existiert.&#8220;<br>
                  Allerdings kann aus der Tatsache des &Uuml;bels nur dann die
                  Frage der Existenz Gottes abgeleitet werden, wenn man eine
                  h&ouml;chste Macht annimmt, die zugleich in sich gut ist. Ohne
                  die Idee eines guten Gottes entsteht die Frage nachdem &Uuml;bel
                  erst gar nicht. Dann ist die Welt eben so, wie sie ist. Denn
                  ohne das Gute k&ouml;nnte man das B&ouml;se gar nicht erkennen.
                  B&ouml;ses gibt es nur, wenn es auch Gutes gibt. Das &Uuml;bel
                  setzt das Gute voraus. Dann mu&szlig; es auch eine Macht geben,
                  die das Gute will. Die Erfahrungen des 20. Jahrhunderts mit
                  den Diktaturen zeigt dann, da&szlig; atheistische Systeme,
                  die ja eine bessere Welt herbeif&uuml;hren wollen, immer mehr
                  in das &Uuml;ble geraten. Ohne Gott scheint die W&uuml;rde
                  des Menschen nicht gesch&uuml;tzt &#8211; wenn er sich n&auml;mlich
                  gegen die Machthaber wendet. So wurden Menschen, die dem Rassenideal
                  der Nationalsozialisten sehr wohl entsprachen und damit lebenswert
                  waren, hingerichtet, wenn sie das Regime ablehnten. Nicht wenige
                  der Widerstandsk&auml;mpfer waren gro&szlig; gewachsen und
                  blond. Das hat sie aber nicht davor bewahrt, von den Nationalsozialisten
                  get&ouml;tet zu werden.</font></p>
                <p><font face="Arial, Helvetica, sans-serif"><strong>Versuche,
                      das &Uuml;bel zu erkl&auml;ren</strong><br>
                  Bereits die griechische Morallehre der Stoa hat versucht, Gott
                  gegen&uuml;ber der Tatsache des &Uuml;bels zu rechtfertigen.
                  So wurde der erzieherische Wert des Leidens f&uuml;r den herausgestellt,
                  der ohne eigene Schuld leidet. F&uuml;r den, der B&ouml;ses
                  tut, ist das Leiden Folge seiner Fehler und Vergehen. Leibniz
                  f&uuml;hrt weitere Erkl&auml;rungen an. Zum einen geht er davon
                  aus, da&szlig; Gott nur die beste aller Welten schaffen konnte
                  und da&szlig; daher das B&ouml;se gar nicht vorherrschend werden
                  kann. Zudem w&uuml;rde ein &Uuml;bel ein Gut bewirken, aus
                  der Erfahrung des Krieges erw&auml;chst der Wille zum Frieden.
                  Zudem m&uuml;sse Gott das &Uuml;bel zulassen, wenn er die Freiheit
                  des Menschen wirklich wolle.<br>
                  Diese Versuche, das &Uuml;bel als Teil der an sich guten Sch&ouml;pfung
                  zu erkl&auml;ren, befriedigen nicht. Vor allem kann das B&ouml;se,
                  das den Unschuldigen trifft, nicht damit erkl&auml;rt werden,
                  da&szlig; es ihm zur Erziehung gereicht. Das hat im &Uuml;brigen
                  das Christentum auch nie getan. Dann h&auml;tte es den Kreuzestod
                  Jesu als Erziehungsma&szlig;nahme Gottes f&uuml;r seinen Messias
                  deuten m&uuml;ssen. Vielmehr hat es das dem Menschen angetane
                  Leid als Anteilnahme an dem Schicksal Jesu gedeutet, der unschuldig
                  verurteilt wurde.</font></p>
                <p><font face="Arial, Helvetica, sans-serif"><strong>Das Buch
                      Hiob</strong><br>
                  Im Alten Testament ist ein einzelnes Buch der Frage gewidmet,
                  wie der Mensch sein Leiden verstehen kann. Hiob hei&szlig;t
                  die Figur, die viele Schriftsteller inspiriert hat. Gott erlaubt
                  dem Satan, Hiob zu versuchen, indem er ihm alles nimmt, was
                  sein Leben ausmacht. Seine Kinder kommen um und er verliert
                  seinen ganzen Besitz. Trotz des Leids, das ihm zugef&uuml;gt
                  wurde, zweifelt Hiob nicht an Gott. Die Freunde Hiobs versuchen
                  ihm zu erkl&auml;ren, da&szlig; er sich verfehlt haben mu&szlig; und
                  daher sein Leiden als Strafe zu verstehen sind. Diese Erkl&auml;rung
                  wird im Hiobbuch als nicht tragf&auml;hig erwiesen. Eine Antwort
                  erh&auml;lt Hiob allerdings nicht. Der Leser des Hiobbuches
                  lernt, da&szlig; der Mensch seine Klage vor Gott bringen kann,
                  ohne da&szlig; damit Gott beleidigt w&uuml;rde. </font></p>
                <p><font face="Arial, Helvetica, sans-serif"><strong>Der Leidensweg
                      Jesu</strong><br>
                  Die Antwort, die Hiob nicht erhalten hat, gibt der Leidensweg
                  Jesu. Gott selbst solidarisiert sich mit den vielen unschuldig
                  Verurteilten und Verfolgten. Der Sohn Gottes erleidet das B&ouml;se,
                  ohne da&szlig; Gott r&auml;chend eingreift. So &uuml;berwindet
                  er das B&ouml;se von innen her. Gott will das B&ouml;se nicht
                  einfach vernichten, indem er den B&ouml;sen umbringt. Jesus
                  nimmt das B&ouml;se ohne Widerstand hin. (<a href="suendenbock.php">S&uuml;ndenbock</a>)
                  Die &Uuml;berwindung des B&ouml;sen liegt nicht in der Vernichtung
                  der Gegner Jesu, sondern da&szlig; der Hingerichtete in ein
                  neues Leben auferweckt wird. <a href="auferstehung_jesu.php">Auferstehung</a> hei&szlig;t
                  die Neusch&ouml;pfung einer Welt, in der &Uuml;bel und Tod
                  nicht mehr herrschen, sondern Gott allein. <br>
                  Auch wenn Gott die Welt nicht in Krieg und Unrecht versinken
                  l&auml;&szlig;t, sondern die Geschichte zu einem guten Ende
                  f&uuml;hrt, die Tatsache des B&ouml;sen mit all seinen &Uuml;beln
                  bleibt dem Menschen ein R&auml;tsel, das immer wieder dazu
                  herausfordert, das B&ouml;se in Krimis, Thrillern zu zeigen,
                  um es anf&auml;nglich erkl&auml;ren, aber nicht letztlich ergr&uuml;nden
                  zu k&ouml;nnen.</font></p>
                <p><font face="Arial, Helvetica, sans-serif"><br>
                      <strong>Zitate</strong></font></p>
                <p><font face="Arial, Helvetica, sans-serif">&#8222;Wenn es Gott
                    gibt, woher kommt das B&ouml;se? Doch woher kommt das Gute,
                    wenn es ihn nicht gibt.&#8220;<br>
                  Boethius (cons. I 4p, 100f)<br>
                  <br>
&#8222; Es ist der Irrtum derer auszuschlie&szlig;en, die aus den &Uuml;beln
der Welt folgern, da&szlig; Gott nicht ist. &#8230; Sie fragen: Wenn Gott ist,
woher dann das &Uuml;bel? Aber man mu&szlig; sagen: Wenn es das &Uuml;bel gibt,
dann gibt es Gott. Denn das &Uuml;bel w&auml;re nicht, wenn die Ordnung des Guten
nicht best&uuml;nde, dessen Beraubung das &Uuml;bel ist. Diese Ordnung w&auml;re
aber nicht, wenn Gott nicht w&auml;re.&#8220;<br>
                  Thomas von Aquin, Summa contra Gentiles III, 71</font></p>
                <p><font face="Arial, Helvetica, sans-serif">&#8222;Es ist nur
                    eine Welt m&ouml;glich, eine durchaus gute. Alles, was in
                    der Welt sich ereignet, dient zur Verbesserung und Bildung
                    des Menschen und vermittels dieser zur Herbeif&uuml;hrung
                    ihres irdischen Zieles. Dieser h&ouml;here Weltplan ist es,
                    was wir Natur nennen, wenn wir sagen: Die Natur f&uuml;hret
                    den Menschen durch Mangel zum Flei&szlig;e, durch die Drangsale
                    ihrer unaufh&ouml;rlichen Kriege zum endlichen ewigen Frieden.
                    Dein Wille, Unendlicher, deine Vorsehung allen ist diese
                    h&ouml;here Natur.&#8220;<br>
                  Johann Gottlieb Fichte, Die Bestimmung des Menschen, Berlin
                  1800, S. 147 </font></p>
                <p><font face="Arial, Helvetica, sans-serif">&#8222;Wenn kein
                    Gott ist, dann gibt es &#8211; letztlich &#8211; kein Gutes.
                    Nicht in der Zukunft f&uuml;r die Zu-kurz-Gekommenen, Erniedrigten,
                    Beleidigten; erst recht nicht f&uuml;r die Henker &#8211; oder
                    auch tatenloser Genie&szlig;er. Von dorther aber auch nicht
                    ernstlich im Heute. Gl&uuml;ck w&auml;re dann nur auf Grund
                    von Wegsehen, Vergessen, denkbar. <br>
                  J&ouml;rg Splett, 1996, Denken vor Gott, Frankfurt 1996, S.
                  309 </font></p>
                <p><font face="Arial, Helvetica, sans-serif">&#8222;Leiden, Schuld
                    und Tr&auml;nen schreien nach realer &Uuml;berwindung des &Uuml;bels.
                    Daher erm&ouml;glicht erst die Einheit von Sch&ouml;pfung
                    und Erl&ouml;sung im Horizont der Eschatologie eine haltbare
                    Antwort auf die Frage der Theodizee, die Frage nach der Gerechtigkeit
                    Gottes in seinen Werken. Genauer gesagt, es ist allein Gott
                    selbst, der eine wirklich befreiende Antwort auf diese Frage
                    zu geben vermag, und er gibt sie durch die Geschichte seines
                    Handelns in der Welt und insbesondere durch dessen Vollendung
                    mit der Aufrichtung seines Reiches in der Sch&ouml;pfung.
                    Solange die Welt isoliert im Blick auf ihre unvollendete
                    und unerl&ouml;ste Gegenwart einerseits, unter dem Gesichtspunkt
                    ihres anf&auml;nglichen Hervorgangs aus den H&auml;nden des
                    Sch&ouml;pfers andererseits, betrachtet wird, bleibt die
                    Tatsache des B&ouml;sen und des &Uuml;bels in der Sch&ouml;pfung
                    ein auswegloses R&auml;tsel.&#8220;<br>
                  Wolfhart Pannenberg, Sch&ouml;pfungsglaube und Theodizee, in &#8222;Systematische
                  Theologe&#8220; Bd. 2, G&ouml;ttingen 1991, S. 193</font></p>
                <p></p>
                <p><font size="2" face="Arial, Helvetica, sans-serif">Eckhard
                    Bieger S.J.</font></p>
                <p>&copy;<a href="http://www.kath.de"> <font face="Arial, Helvetica, sans-serif">www.kath.de</font></a></p>
              </td>
              <td background=boxright.gif><img height=8 alt="" 
            src="boxright.gif" width=8></td>
            </tr>
            <tr valign=top align=left>
              <td><img height=8 alt="" src="boxbottomleft.gif" 
            width=8></td>
              <td background=boxbottom.gif><img height=8 alt="" 
            src="boxbottom.gif" width=8></td>
              <td><img height=8 alt="" src="boxbottomright.gif" 
            width=8></td>
            </tr>
          </tbody>
        </table>
      </td>
      <td style='vertical-align:top;'>
        <p><a href="http://www.update-seele.de" target="_blank"><img src="banner-update-seele-02.jpg" width="286" height="70" border="0"></a> </p>
        <p>
          <script type="text/javascript"><!--
        google_ad_client = "pub-9537752113242914";
        google_ad_width = 336;
        google_ad_height = 280;
        google_ad_format = "336x280_as";
        google_ad_type = "text_image";
        //2006-10-23: Lexikon Symbole
        google_ad_channel = "8495253357";
        //--></script>
          <script type="text/javascript"
          src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
                  </script>
        </p>
      </td>
    </tr>
    <tr>
      <td valign=top align=left>&nbsp; </td>
    </tr>
  </tbody>
</table>
</BODY></HTML>
