<HTML><HEAD><TITLE>Zweinaturenlehre</TITLE>
<META http-equiv=Content-Type content="text/html; charset=iso-8859-1"><LINK 
title=fonts href="kaltefleiter.css" type=text/css 
rel=stylesheet>
<META content="MSHTML 5.50.4134.600" name=GENERATOR></HEAD>
<BODY bgColor=#ffffff leftMargin=6 topMargin=6 marginheight="6" marginwidth="6">
<TABLE cellSpacing=0 cellPadding=6 width="100%" border=0>
  <TBODY>
  <TR>
    <TD vAlign=top align=left width=100> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b>Philosophie&amp;Theologie</b></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td> <?php include("logo.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10"> <?php include("az.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </TD>
    <TD vAlign=top rowSpan=2>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
        <TBODY>
        <TR vAlign=top align=left>
          <TD width=8><IMG height=8 alt="" 
            src="boxtopleftcorner.gif" width=8></TD>
          <TD background=boxtop.gif><IMG height=8 alt="" 
            src="boxtop.gif" width=8></TD>
          <TD width=8><IMG height=8 alt="" 
            src="boxtoprightcorner.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxtopleft.gif><IMG height=8 alt="" 
            src="boxtopleft.gif" width=8></TD>
          <TD bgColor=#e2e2e2> 
            <H1><font face="Arial, Helvetica, sans-serif"> Zweinaturenlehre</font></H1>
          </TD>
          <TD background=boxtopright.gif><IMG height=8 
            alt="" src="boxtopright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=13 alt="" src="boxdividerleft.gif" 
            width=8></TD>
          <TD background=boxdivider.gif><IMG height=13 
            alt="" src="boxdivider.gif" width=8></TD>
          <TD><IMG height=13 alt="" 
            src="boxdividerright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxleft.gif><IMG height=8 alt="" 
            src="boxleft.gif" width=8></TD>
          <TD class=L12>
            <P><STRONG><font face="Arial, Helvetica, sans-serif">Wie kann man
                  Jesus als den Sohn Gottes verstehen?</font></STRONG></P>
            <P><font face="Arial, Helvetica, sans-serif">Als das Christentum
                in die griechische Kultur gelangte, stie&szlig; es
              auf eine entwickelte Philosophie, die begriffliche Klarheit verlangte.
              Die Christen mu&szlig;ten Antwort geben, wer dieser Jesus von Nazareth
              ist. Er ist Heilbringer. Das war in der griechischen Welt nichts
              Besonderes. Viele Prediger sind aufgetreten und haben sich als
              Heilbringer ausgegeben. &Uuml;ber solche M&auml;nner wurde auch
              erz&auml;hlt, da&szlig; sie Wunder vollbringen w&uuml;rden oder
              vollbracht h&auml;tten. Von Jesus behaupteten die Christen, da&szlig; er
              Gottes Sohn und zugleich Mensch sei. Das Konzil von Nic&auml;a
              hatte 325 festgestellt, da&szlig; er wirklich Gott ist, ungeschaffen,
              Gott gleich, der <a href="gottessohn.php">Sohn Gottes</a>. Wie sollte man das verstehen?</font></P>
            <P><font face="Arial, Helvetica, sans-serif">Wenn dieser Jesus von
                Nazareth wirklich Gott ist, dann konnte er in den Augen der griechischen
                Welt nicht wirklich Mensch sein.
              Sie dachten von Jesus so, wie es aus griechischen G&ouml;ttersagen &uuml;berliefert,
              war, da&szlig; ein Gott auf der Erde erschienen ist. Seine Gestalt
              w&auml;re nur eine Art H&uuml;lle, um sich den Menschen sichtbar
              zu machen, die jedoch nur als <a href="christologische_streitigkeiten.php">Form
              der Erscheinung</a> zu verstehen
              war, nicht wirklich ein Mensch.<br>
              Im griechischen Denken lag noch eine andere Verstehensformel bereit.
              Der Philosoph Aristoteles hatte schon 300 Jahre vor den christlichen
              Predigern dem Menschen nicht nur eine Seele zugesprochen, sondern
              auch erkl&auml;rt, wie die Seele mit dem K&ouml;rper verbunden
              ist. Er hatte nicht die Vorstellung, die der heutigen Medizin zugrunde
              liegt, da&szlig; n&auml;mlich der K&ouml;rper nach chemisch-biologischen
              Gesetzen funktioniert und irgendwo im Brustkorb oder Kopf die Seele
              in dem K&ouml;rper haust. Aristoteles hatte eine Vorstellung entwickelt,
              die viel mehr mit unseren Erfahrungen &uuml;bereinstimmt: Die Seele
              ist das organisierende Prinzip, das Lebensprinzip jedes Lebewesens.
              Auch Tiere haben eine Seele, die dem materiellen Substrat Leben
              gibt, so da&szlig; wir von einem K&ouml;rper als belebter Materie
              sprechen k&ouml;nnen.
              Die menschliche Seele hat dar&uuml;ber hinaus noch eine besondere
              geistige Qualit&auml;t, sie steht in unmittelbarer Beziehung zur
              Vernunft, die dem ganzen Kosmos zugrunde liegt. Der Mensch ist
              das Wesen, das Vernunft hat und &uuml;ber Sprache verf&uuml;gt.
              Eine M&ouml;glichkeit, den Heilbringer Jesus zu erkl&auml;ren,
              liegt in diesem griechischen Modell: Da der menschliche Geist unmittelbar
              auf die Weltvernunft bezogen ist, k&ouml;nnte doch der Logos, die
              Weltvernunft an die Stelle der Seele treten. Jesus von Nazareth
              h&auml;tte dann keine menschliche Seele. Wie reagierten die christlichen
              Denker auf diese Vorstellung?</font></P>
            <P><font face="Arial, Helvetica, sans-serif">Zuerst einmal stimmte
                die griechische Vorstellung von einem Scheinleib nicht mit den
                Berichten &uuml;ber Jesus &uuml;berein. W&auml;re
              sein Leib nur eine H&uuml;lle, w&auml;re der Tod Jesu nicht wirklich
              geschehen. Wenn das geistige organisierende Prinzip der g&ouml;ttliche
              Logos w&auml;re, w&auml;ren die menschlichen Reaktionen nicht verst&auml;ndlich,
              denn er wird als einer geschildert, der sich freut, der Trauer
              empfindet, der sogar in gro&szlig;e Angst ger&auml;t, als er seine
              Verurteilung unausweichlich kommen sieht.<br>
              Ein gewichtiger theologischer Grund f&uuml;r die Annahme, da&szlig; bei
              Jesus nicht anstelle der menschlichen Seele der Logos das organisierende
              Prinzip des K&ouml;rpers ist, sondern da&szlig; er eine menschliche
              Seele hatte, ist die Dimension der Erl&ouml;sung. Denn mit der
              Menschwerdung des Sohnes Gottes wird die Menschennatur insgesamt
              in den Erl&ouml;sungsproze&szlig; einbezogen. Nachdem Jesus im
              Tod am Kreuz mit allen ungerecht Verurteilten solidarisch geworden
              und als Mensch mit Leib und Seele eine neue, himmlische Existenz
              gefunden hatte, ist er der erste der von den Toten <a href="auferstehung_jesu.php">Auferstandenen</a>.
              Mit ihm sind alle Menschen anf&auml;nglich aus dem Tod gerettet.
              Wie kann das f&uuml;r den griechischen Kulturraum sprachlich deutlich
              gemacht werden?</font></P>
            <P><font face="Arial, Helvetica, sans-serif"><strong>Begriffliche Kl&auml;rung</strong><br>
              Wenn in Jesus nicht zwei Personen gemeint sind, sondern uns einer
              entgegentritt, dann mu&szlig; die Zweiheit auf einer anderen Ebene
              liegen. Um die Zweiheit
              von Gottheit und Menschheit  zu unterscheiden, bezeichnen
              die Theologen des 5. und 6. Jahrhunderts mit dem Naturbegriff die
              g&ouml;ttliche und die menschliche Natur. (Physis
              ist das griechische Wort f&uuml;r Natur) Die <a href="monophysiten.php">Monophysiten</a> sprechen
              von einer neuen Natur, die durch die Einigung von menschlicher
              und g&ouml;ttlicher Natur entsteht. Diese Bezeichnung ist jedoch
              nicht klar genug, denn der Begriff Natur - Physis bzeichnet gerade
              die Zweiheit in Jesus. 
              Der <a href="person.php">Personbegriff</a> 
              ist eine bessere begriffliche Beschreibung,  denn er bezieht
              sich auf den einen Jesus von Nazareth, den menschgewordenen Sohn
              Gottes.
              Der
              Personbegriff
              bezeichnet
              den
              Einen, der Naturberiff die Zweiheit in Jesus. </font></P>
            <P><font face="Arial, Helvetica, sans-serif">Zitate<br>
&#8222;Er war Gott gleich, hielt aber nicht daran fest, wie Gott
              zu sein, sondern er ent&auml;u&szlig;erte sich und wurde wie ein
              Sklave und den Menschen gleich. Sein Leben war das eines Menschen,
              er erniedrigte sich und war gehorsam bis zum Tod am Kreuz.&#8220; Paulus
              zitiert in seinem Brief an die Philipper 2,6-8 einen Hymnus aus
              der christlichen Liturgie.</font></P>
            <p><font face="Arial, Helvetica, sans-serif">&quot;Und das Wort ist Fleisch
                geworden und hat unter uns gewohnt, und wir haben seine Herrlichkeit
                gesehen, die Herrlichkeit des einzigen
              Sohnes vom Vater, voll Gnade und Wahrheit.&quot; Johannesevangelium
                1, 14</font></p>
            <p><font face="Arial, Helvetica, sans-serif">&quot;Es ist zu bekennen,
                da&szlig; die Weisheit selbst, das Wort, der
              Sohn Gottes einen menschlichen Leibe, eine Geist-Seele und eine
              sensitive Seele angenommen hat, d.h. den ganzen Adam &#8230; unseren
              ganzen alten Menschen au&szlig;er der S&uuml;nde. &#8230; Wenn
              aber einer sagen w&uuml;rde, das Wort habe anstelle der menschlichen
              sensitiven Seele im Fleisch des Herrn verweilt, den schlie&szlig;t
              die katholische Kirche aus, ebenso auch die, die zwei S&ouml;hne
              in unserem Heiland bekennen, einen vor der Inkarnation und einen
              anderen nach der Annahme des Fleisches aus der Jungfrau.&quot; (Die
              sensitive Seele ist nach der damaligen Anthropologie der Sitz der
              Gef&uuml;hle
              und Empfindungen, diese wird von der Geist-Seele unterschieden.)<br>
              Papst Damasus I. im Brief an Paulinus v. Antiochien 375</font></p>
            <p><font size="2" face="Arial, Helvetica, sans-serif">Eckhard Bieger</font><font face="Arial, Helvetica, sans-serif"> <font size="2">S.J.</font><br>
            </font>            &copy;<a href="http://www.kath.de"> <font face="Arial, Helvetica, sans-serif">www.kath.de</font></a></p>
            </TD>
          <TD background=boxright.gif><IMG height=8 alt="" 
            src="boxright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=8 alt="" src="boxbottomleft.gif" 
            width=8></TD>
          <TD background=boxbottom.gif><IMG height=8 alt="" 
            src="boxbottom.gif" width=8></TD>
          <TD><IMG height=8 alt="" src="boxbottomright.gif" 
            width=8></TD></TR></TBODY></TABLE></TD><td style='vertical-align:top;'>
 <script type="text/javascript"><!--
        google_ad_client = "pub-9537752113242914";
        google_ad_width = 336;
        google_ad_height = 280;
        google_ad_format = "336x280_as";
        google_ad_type = "text_image";
        //2006-10-23: Lexikon Symbole
        google_ad_channel = "8495253357";
        //--></script>
        <script type="text/javascript"
          src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
        </script>
</td>
</TR>
  <TR>
    <TD vAlign=top align=left>&nbsp; </TD>
  </TR></TBODY></TABLE></BODY></HTML>
