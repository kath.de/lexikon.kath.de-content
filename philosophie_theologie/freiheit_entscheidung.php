<HTML><HEAD><TITLE>Freiheit und Entscheidung</TITLE>
<META http-equiv=Content-Type content="text/html; charset=iso-8859-1"><LINK 
title=fonts href="kaltefleiter.css" type=text/css 
rel=stylesheet>
<META content="MSHTML 5.50.4134.600" name=GENERATOR></HEAD>
<BODY bgColor=#ffffff leftMargin=6 topMargin=6 marginheight="6" marginwidth="6">
<TABLE cellSpacing=0 cellPadding=6 width="100%" border=0>
  <TBODY>
  <TR>
    <TD vAlign=top align=left width=100> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b>Philosophie&amp;Theologie</b></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td> <?php include("logo.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10"> <?php include("az.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </TD>
    <TD vAlign=top rowSpan=2>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
        <TBODY>
        <TR vAlign=top align=left>
          <TD width=8><IMG height=8 alt="" 
            src="boxtopleftcorner.gif" width=8></TD>
          <TD background=boxtop.gif><IMG height=8 alt="" 
            src="boxtop.gif" width=8></TD>
          <TD width=8><IMG height=8 alt="" 
            src="boxtoprightcorner.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxtopleft.gif><IMG height=8 alt="" 
            src="boxtopleft.gif" width=8></TD>
          <TD bgColor=#e2e2e2> 
            <H1><font face="Arial, Helvetica, sans-serif">Freiheit und Entscheidung</font></H1>
          </TD>
          <TD background=boxtopright.gif><IMG height=8 
            alt="" src="boxtopright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=13 alt="" src="boxdividerleft.gif" 
            width=8></TD>
          <TD background=boxdivider.gif><IMG height=13 
            alt="" src="boxdivider.gif" width=8></TD>
          <TD><IMG height=13 alt="" 
            src="boxdividerright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxleft.gif><IMG height=8 alt="" 
            src="boxleft.gif" width=8></TD>
          <TD class=L12>
            <P><STRONG><font face="Arial, Helvetica, sans-serif"> Friheit verwirklich
                sich durch Entscheidungen</font></STRONG></P>
            <P><font face="Arial, Helvetica, sans-serif">Die Freiheit ist erst
                einmal nur ein Verm&ouml;gen. Damit aus
              diesem Verm&ouml;gen Wirklichkeit wird, mu&szlig; noch etwas hinzukommen.
              Da Freiheit  in der Zeit &#8222;geschehen&#8220; mu&szlig;,
              braucht es etwas, was die Zeit organisiert. Mit einer Entscheidung
              verwirklicht sich die Freiheit in der Zeit, denn sie bestimmt mit
              der Entscheidung, wie das Leben verlaufen soll. Konkret ist es
              die Entscheidung f&uuml;r einen bestimmten Beruf, die Partnerschaft
              mit einem bestimmten Menschen, das Eingehen von Freundschaften,
              aber auch die Entscheidung f&uuml;r eine Weltanschauung. In der
              Entscheidung wird das &#8222;wirklich&#8220;, was die Freiheit
              will. <br>
              Von einer Entscheidung kann man nur sprechen, wenn es mehr als
              eine Alternative gibt. Das ist bei der Berufs- wie bei der Partnerwahl
              offenkundig. Es gilt auch f&uuml;r die Weltanschauung, f&uuml;r
              die sich die Freiheit entscheidet. Denn keine Weltanschauung l&auml;&szlig;t
              sich in der Weise empirisch beweisen wie ein Naturgesetz. Da es
              von seiner Weltanschauung abh&auml;ngt, wie der einzelne sich in
              bestimmten Situationen verh&auml;lt, beeinflu&szlig;t die Entscheidung
              f&uuml;r eine bestimmte Weltanschauung den Verlauf des Lebens in
              hohem Ma&szlig;. Die Weltanschauung, da&szlig; es auf Erfolg und
              Sich-Durchsetzen ankommt, macht aus demjenigen einen anderen Menschen
              als wenn er sich f&uuml;r die Weltanschauung entschieden h&auml;tte,
              da&szlig; Leben in der Gemeinschaft gelingt und daher Solidarit&auml;t
              und der Blick f&uuml;r diejenigen, die am Rand stehen, f&uuml;r
              das Gelingen des eigenen Lebens entscheidend sind. Die Weltanschauung,
              f&uuml;r die sich jemand entschieden hat, steuert das Leben bis
              in die allt&auml;glichen kleinen Entscheidungen. Da die Freiheit
              sich <a href="freiheit.php">unerbittlich</a> einfordert
              und wir gegen&uuml;ber
              unserer Freiheit nicht frei sind, ist die Entscheidung f&uuml;r
              eine Weltanschauung die, die unser Leben am nachhaltigsten beeinflu&szlig;t. </font></P>
            <p><font face="Arial, Helvetica, sans-serif">Eckhard Bieger S.J.<br>
            </font>            </p>
            <p><font face="Arial, Helvetica, sans-serif"><a href="http://www.kathshop.de/wwwkathde/product_info.php?cPath=&products_id=195&anbieter=17&page=&">Buchtipp</a></font></p>
            <P>&copy;<a href="http://www.kath.de"> <font face="Arial, Helvetica, sans-serif">www.kath.de</font></a></P>
            </TD>
          <TD background=boxright.gif><IMG height=8 alt="" 
            src="boxright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=8 alt="" src="boxbottomleft.gif" 
            width=8></TD>
          <TD background=boxbottom.gif><IMG height=8 alt="" 
            src="boxbottom.gif" width=8></TD>
          <TD><IMG height=8 alt="" src="boxbottomright.gif" 
            width=8></TD></TR></TBODY></TABLE></TD><td style='vertical-align:top;'>
 <script type="text/javascript"><!--
        google_ad_client = "pub-9537752113242914";
        google_ad_width = 336;
        google_ad_height = 280;
        google_ad_format = "336x280_as";
        google_ad_type = "text_image";
        //2006-10-23: Lexikon Symbole
        google_ad_channel = "8495253357";
        //--></script>
        <script type="text/javascript"
          src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
        </script>
</td>
</TR>
  <TR>
    <TD vAlign=top align=left>&nbsp; </TD>
  </TR></TBODY></TABLE></BODY></HTML>
