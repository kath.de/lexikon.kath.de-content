<HTML><HEAD><TITLE>Person</TITLE>
<META http-equiv=Content-Type content="text/html; charset=iso-8859-1"><LINK 
title=fonts href="kaltefleiter.css" type=text/css 
rel=stylesheet>
<META content="MSHTML 5.50.4134.600" name=GENERATOR></HEAD>
<BODY bgColor=#ffffff leftMargin=6 topMargin=6 marginheight="6" marginwidth="6">
<TABLE cellSpacing=0 cellPadding=6 width="100%" border=0>
  <TBODY>
  <TR>
    <TD vAlign=top align=left width=100> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b>Philosophie&amp;Theologie</b></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td> <?php include("logo.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10"> <?php include("az.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </TD>
    <TD vAlign=top rowSpan=2>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
        <TBODY>
        <TR vAlign=top align=left>
          <TD width=8><IMG height=8 alt="" 
            src="boxtopleftcorner.gif" width=8></TD>
          <TD background=boxtop.gif><IMG height=8 alt="" 
            src="boxtop.gif" width=8></TD>
          <TD width=8><IMG height=8 alt="" 
            src="boxtoprightcorner.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxtopleft.gif><IMG height=8 alt="" 
            src="boxtopleft.gif" width=8></TD>
          <TD bgColor=#e2e2e2> 
            <H1><font face="Arial, Helvetica, sans-serif">Person </font></H1>
          </TD>
          <TD background=boxtopright.gif><IMG height=8 
            alt="" src="boxtopright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=13 alt="" src="boxdividerleft.gif" 
            width=8></TD>
          <TD background=boxdivider.gif><IMG height=13 
            alt="" src="boxdivider.gif" width=8></TD>
          <TD><IMG height=13 alt="" 
            src="boxdividerright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxleft.gif><IMG height=8 alt="" 
            src="boxleft.gif" width=8></TD>
          <TD class=L12>
            <P><STRONG><font face="Arial, Helvetica, sans-serif">Die Entwicklugn
                  des Personbegriffs</font></STRONG></P>
            <P><font face="Arial, Helvetica, sans-serif">Die Vorstellung vom
                Menschen als einem Wesen, das auf Grund seiner Geistigkeit und
                Freiheit innerlich unabh&auml;ngig ist und &uuml;ber
              sein Leben verf&uuml;gt, begr&uuml;ndet die W&uuml;rde dieses Wesens
              und damit die Menschenrechte. Person ist das Innerste und zugleich
              das Ganze des Menschen. Urteilsf&auml;higkeit, freie Entscheidung,
              das Verh&auml;ltnis zur Umwelt und anderen Menschen sehen wir zugleich
              in dem Personkern des einzelnen, wie auch der Leib zur Person geh&ouml;rt.
              Wir k&ouml;nnen den anderen in seiner Person &#8222;treffen&#8220;,
              wenn wir ihn liebkosen oder schlagen. Diese auf die Person zielenden
              Ber&uuml;hrungen sind von denen zu unterscheiden, die nur einen
              Teil des K&ouml;rpers betreffen, wenn z.B. ein Arzt eine Wunde
              behandelt oder die Kosmetikerin das Gesicht einkremt.<br>
              Wie kam es zu der Vorstellung, da&szlig; das Innerste, was den
              Menschen ausmacht, auch den Leib umfa&szlig;t. Es sind theologische
              Auseinandersetzungen im 5. und 6. Jahrhundert, die zum Pesonbegriff
              f&uuml;hrten, der zur Grundlage der europ&auml;ischen Kultur wurde. &#8222;Jesus
              von Nazareth&#8220;, der menschgewordene Sohn Gottes, forderte
              das Denken heraus, so da&szlig; das Menschenbild der Antike weiter
              entwickelt werden mu&szlig;te. Die Frage, die sich damals stellte
              war folgende: Wenn Jesus wirklicher Mensch war und zugleich die
              zweite Person des dreifaltigen Gottes, wie kann er dann &#8222;einer&#8220; sein?
              Der Personbegriff entwickelte sich aus der Frage, was die Seele
              des Menschen Jesus war und ob Jesus &uuml;berhaupt eine menschliche
              Seele
              hatte.</font></P>
            <P><font face="Arial, Helvetica, sans-serif"><strong>Die Vorstellung von Leib und Seele reicht nicht aus</strong><br>
              Die Vorstellung, da&szlig; der Mensch eine Geistseele besitzt,
              die vom K&ouml;rper zu unterschieden ist, hatten die Griechen entwickelt.
              Den Juden war an einer solchen begrifflichen Klarheit nicht so
              sehr gelegen. Sie sahen im Atem das belebende Prinzip. Gott erschuf
              den ersten Menschen, indem er Lehm formte und ihm Leben einhauchte.
              (s.u. Zitate)<br>
              Die Griechen erkannten deutlicher, da&szlig; der Mensch &uuml;ber
              ein geistiges Prinzip verf&uuml;gt, das sein ganzes Wesen organisiert.
              Sie nennen es Seele, &#8222;Psyche&#8220;, das lateinische Wort
              ist &#8222;anima&#8220;. Wenn aber Jesus eine menschliche Seele
              hat, dann ist er auch ein menschliches Individuum. Das entspricht
              den Berichten der Bibel, die Jesus als einen Menschen zeigen, der
              Hunger und Durst hat, der Zuneigung, Zorn und Angst kennt, der
              die Gef&uuml;hle und die Befindlichkeit anderer Menschen versteht.
              Wenn Jesus aber zugleich der Sohn Gottes ist, dann ist er auch
              ein &#8222;g&ouml;ttliches Ich&#8220;. Hat er aber dann daneben
              noch ein <a href="menschwerdung_jesu.php">menschliches &#8222;Ich&#8220;</a>? <br>
              Eine L&ouml;sung des Problems konnte im griechischen Denken so
              gefunden werden, da&szlig; Jesus keine menschliche Seele hat, sondern
              der g&ouml;ttliche Logos an die Stelle der menschlichen Seele 
              als organisierendes Prinzip des K&ouml;rpers tritt. Das konnte
              aber nicht angenommen werden, denn Jesus f&uuml;hlte wie ein Mensch.
              Damit war die griechische Vorstellung vom Menschen an ihre Grenze
              geraten. Das sieht man auch an einem
              anderen L&ouml;sungsmodell. Cyrill von Alexandrien sah das Neue,
              das uns in Jesus entgegen tritt, da&szlig; ein Mensch zugleich
              wirklich Gott ist, als eine <a href="monophysiten.php">&quot;neue
              Natur&quot;</a>. 
              Es w&auml;re also aus der Verbindung von Gott und Mensch eine neue
              Natur entstanden, ein Gottmenschentum k&ouml;nnte man in einer
              uns mehr gel&auml;ufigen Begrifflichkeit sagen. Aber kann man durch
              Mischung erkl&auml;ren, was so verschieden ist. Es ist deutlich,
              da&szlig; mit dem Begriff Natur aber gerade das bezeichnet wird,
              was nicht &#8222;Ich&#8220; ist, n&auml;mlich das G&ouml;ttliche
              und Menschliche in Jesus. Es wird auch deutlich, da&szlig; Begriffe
              nur Verstehensm&ouml;glichkeiten beinhalten und nicht einfachhin
              die Wirklichkeit wiedergeben. Deshalb ist der Kampf um die Begriffe,
              der im 4. und 5. Jahrhundert mit aller Sch&auml;rfe ausgetragen
              wurde, ein Ringen, wie man in der Sprache der damaligen Zeit erkl&auml;ren
              kann, wer Jesus war. Die Konzilien haben die Grenzen markiert,
              was kein angemessenes Sprechen &uuml;ber Jesus, den Sohn Gottes
              ist. Das Geheimnis selbst haben sie nicht erkl&auml;rt.</font></P>
            <P><font face="Arial, Helvetica, sans-serif"><strong>Das Antlitz
                  des Menschen repr&auml;sentiert seine Person</strong><br>
              Wenn Seele und Natur nicht mehr die Begriffe sein k&ouml;nnen,
              womit das bezeichnet wird, was die <a href="zweinaturenlehre.php">beiden
              Naturen</a> 
              einigt, so da&szlig; Jesus nicht zweimal &#8222;Ich&#8220; ist,
              mu&szlig;te ein neuer Begriff gefunden werden. Indem man sich auf
              das Antlitz des Menschen bezog, konnte man Leiblichkeit und Geistigkeit
              in einem verstehen, denn in seinem Antlitz zeigt sich der andere
              als der, der er ist. Das lateinische Wort f&uuml;r Person kommt
              von personare, hindurchschallen. Es bezieht sich auf die Maske,
              die Theaterschauspieler trugen. Aus diesem Spezialbegriff hat sich
              das Wort entwickelt, mit dem im Abendland das Besondere des Menschen
              bezeichnet wird und das in dem Wort &#8222;Personw&uuml;rde&#8220; das
              Unantastbare jedes einzelnenausdr&uuml;ckt. <br>
              Denn mit dem Begriff &#8222;Person&#8220; kann etwas ausgesagt
              werden, das mit dem Wort &#8222;Seele&#8220; nicht deutlich wird.
              Die Seele ist nach griechischem Denken das organisierende und damit
              das Lebens-Prinzip des K&ouml;rpers. Verl&auml;&szlig;t die Seele
              den K&ouml;rper, ist dieser ohne Leben und nur noch Materie. Da
              die Christen durch die leibliche <a href="auferstehung_jesu.php">Auferstehung
              Jesu</a> erkannt hatten,
              da&szlig; der Leib ebenso wie das geistige Prinzip, die Seele,
              zur himmlischen Existenz geh&ouml;ren, mu&szlig;te die Einheit
              von Leib und Seele genauer beschrieben werden. F&uuml;r die Griechen
              besteht die Existenz des Menschen nach dem Tod darin, da&szlig; die
              Seele im Himmel ist und der K&ouml;rper in seine Materieteile zerf&auml;llt.
              Eine leib-seelische Existenz braucht aber ein einigendes Prinzip,
              das anders gedacht werden mu&szlig; als die griechische Vorstellung
              von der Seele. Die Person ist dieses einigende Prinzip. Jesus ist
              eine Person und doch zugleich ganz Gott und ganz Mensch. Das hei&szlig;t
              dann auch, da&szlig; der Mensch Person ist und damit sein Leib
              eine ganz neue Bedeutung erh&auml;lt. Der Leib ist mehr als eine
              zuf&auml;llige Ansammlung von Molek&uuml;len, er geh&ouml;rt zur
              Person und mu&szlig; genauso geachtet werden wie der Geist des
              Menschen.</font></P>
            <P><font face="Arial, Helvetica, sans-serif"><strong>Person - Antlitz</strong><br>
              Persona bezeichnet im Lateinischen die Maske des Theaterschauspielers.
              Im Griechischen entspricht dem das Wort Prosopon f&uuml;r Antlitz.
              In seinem Antlitz tritt uns der andere als Person gegen&uuml;ber,
              wir lesen aus seinen Gesichtsz&uuml;gen, was er f&uuml;hlt, wie
              er reagiert, wie er uns begegnet. Die Person als einigendes Prinzip
              erm&ouml;glicht es, den Sohn Gottes als Person in der Dreifaltigkeit
              zu benennen und zugleich sagen zu k&ouml;nnen, da&szlig; der Sohn
              ganz Mensch geworden ist, mit Leib und Seele, ohne da&szlig; die
              menschliche Seele aus Jesus einen zweiten Sohn Gottes machen w&uuml;rde.
              Jesus ist nur einer, ganz Gott und ganz Mensch.</font></P>
            <P><font face="Arial, Helvetica, sans-serif"><strong>Zitate</strong><br>
              Da formte Gott, der Herr den Menschen aus Erde vom Ackerboden und
              blies in seine Nase den Lebensatem. So wurde der Mensch zu einem
              lebendigen Wesen. (Buch Genesis, 2,7)</font></P>
            <p><font face="Arial, Helvetica, sans-serif">Es mu&szlig; aber gefragt
                werden, auf welche Weise das Wort Fleich wurde, ob es gewisserma&szlig;en
                im Fleisch sich verwandelt hat oder es das Fleisch angezogen
                hat. Alles n&auml;mlich, das in etwas
              anderes verwandelt wird, h&ouml;rt auf zu sein, was es war, und
              beginnt zu sein, was es nicht war. Gott aber h&ouml;rt nicht auf
              zu sein, noch kann er etwas anderes sein. ... Wenn das Wort auf
              Grund der Verwandlung und Ver&auml;nderung seiner Substanz Fleisch
              geworden w&auml;re, dann w&uuml;rde Jesus ein Substanz aus zweien
              sein, aus Fleisch und Geist, ein gewisses Gemisch, wie eines aus
              Gold und Silber, das weder Gold ist, das ist der Geist, noch Silber,
              das ist das Fleisch, so da&szlig; das eine durch das andere ver&auml;ndert
              wird und etwas Drittes entsteht. Dann ist Jesus auf keinen Fall
              Gott, das Wort, das Fleisch wurde, hat dann aufgeh&ouml;rt zu sein.
              Fleisch im eigentlichen Sinne ist es auch nicht mehr, weil es (das
              Fleisch) zu Wort wurde. ....<br>
              (Jesus Christus wird aber gelehrt) als Gottessohn und Menschensohn,
              wobei Gott und Mensch ohne Zweifel gem&auml;&szlig; beiden Substanzen
              in ihrer Eigent&uuml;mlichkeit getrennt sind, weil weder das Wort
              etwas anderes als Gott noch das Fleisch etwas anderes als Mensch
              ist.<br>
              Wir erkennen einen doppelten Seinsstand, unvermischt, aber verbunden
              in einer Person, den Gott und den Menschen Jesus. ...<br>
              So sehr ist die Eigent&uuml;mlichkeit jeder der beiden Substanzen
              gewahrt, da&szlig; sowohl der Geist in ihm das, was ihm zukommt,
              tut, n&auml;mlich die Tugenden, Werke und Wunder, als auch das
              Fleisch seine Leiden erduldet....<br>
              Tertullian, Adversus Praxean, um 210</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Boethius, ein Denker
                des 6. Jahrhunderts, der am Hof des Ostgotenk&ouml;nigs
              Theoderichs t&auml;tig war, ist das Bindeglied zwischen der Antike
              und dem Mittelalter. Er hat sich um klare Begrifflichkeiten bem&uuml;ht,
              indem er f&uuml;r griechische Worte lateinische &Auml;quivalente
              entwickelte und definierte. Er legte damit die Grundlagen f&uuml;r
              die mittelalterliche Philosophie und Theologie. Boethius definiert
              Person als &quot;rationabilis naturae individua substantia &#8211; die
              individuelle Substanz einer vernunftbegabten Natur&quot;.</font></p>
            <p><font face="Arial, Helvetica, sans-serif">                  In all diesen F&auml;llen kann Person niemals als in den allgemeinen
                (universellen) Seienden bestehend ausgesagt werden, sondern nur
                in den einzelnen und in den Individuen. Als Lebewesen oder als
                Gattung ist der Mensch keine Person, sondern man redet von einzelnen
                Personen, n&auml;mlich von Cicero, Plato und der einzelnen Individuen&#8230;..<br>
                Natur ist die spezifische Eigent&uuml;mlichkeit einer beliebigen
                Substanz, Person aber die individuelle Substanz einer vernunftf&auml;higen
                Natur. <br>
                Opusculum contra Eutychen et Nestorium um 512</font></p>            <p><font face="Arial, Helvetica, sans-serif">Hugo von St. Viktor (Pariser Augustinerstift), aus Halberstadt
              stammender Theologe des 12. Jahrhunderts<br>
              So wurde Gott wahrer Mensch. Wahrer Mensch w&auml;re er aber nicht
              gewesen nur im Fleische oder nur in der der Geistseele, weil der
              Mensch sowohl Fleisch wie Geist ist. Als Gott den Menschen annahm,
              hat er deswegen beides angenommen. Er nahm aber Fleisch und Seele
              an, das hei&szlig;t: den Menschen, seine Natur, aber nicht seine
              Person. Denn er hat nicht den Menschen als Person angenommen, sondern
              er nahm den Menschen in seine Person auf. Deswegen hat er den Menschen
              angenommen, weil er Fleisch und Geist des Menschen angenommen hat.
              Darum hat er nicht eine menschliche Person angenommen, weil jenes
              Fleisch und jene Seele noch nicht zu einer menschlichen Person
              vereint waren, bevor sie vom Wort (dem Sohn Gottes) zu einer Person
              geeint wurden. &#8230; Das Wort war schon vor dieser Vereinigung
              eine Person, weil es der Sohn war, der Person war, wie auch der
              Vater Person war und der Heilige Geist.<br>
              De sacramentis christianae fidei, 1134</font></p>
            <p><font size="2" face="Arial, Helvetica, sans-serif">Eckhard Bieger
            </font><font size="2">            </font> </p>
            <P>                    <br>
                  <br>
                  <br>
            </P>
            <P>&copy;<a href="http://www.kath.de"> <font face="Arial, Helvetica, sans-serif">www.kath.de</font></a></P>
            </TD>
          <TD background=boxright.gif><IMG height=8 alt="" 
            src="boxright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=8 alt="" src="boxbottomleft.gif" 
            width=8></TD>
          <TD background=boxbottom.gif><IMG height=8 alt="" 
            src="boxbottom.gif" width=8></TD>
          <TD><IMG height=8 alt="" src="boxbottomright.gif" 
            width=8></TD></TR></TBODY></TABLE></TD><td style='vertical-align:top;'>
 <script type="text/javascript"><!--
        google_ad_client = "pub-9537752113242914";
        google_ad_width = 336;
        google_ad_height = 280;
        google_ad_format = "336x280_as";
        google_ad_type = "text_image";
        //2006-10-23: Lexikon Symbole
        google_ad_channel = "8495253357";
        //--></script>
        <script type="text/javascript"
          src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
        </script>
</td>
</TR>
  <TR>
    <TD vAlign=top align=left>&nbsp; </TD>
  </TR></TBODY></TABLE></BODY></HTML>
