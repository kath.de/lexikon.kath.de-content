<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <title>Das B&ouml;se - seine Darstellung</title>


  <meta name="title" content="Philosophie&amp;Theologie">

  <meta name="author" content="Redaktion kath.de">

  <meta name="publisher" content="kath.de">

  <meta name="copyright" content="kath.de">

  <meta name="description" content="">

  <meta name="abstract" content="Lexikon &uuml;ber Philosophie&amp;Theologie">

  <meta http-equiv="content-language" content="de">

  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

  <meta name="date" content="2006-00-01">

  <meta name="robots" content="index,follow">

  <meta name="revisit-after" content="10 days">

  <meta name="revisit" content="after 10 days">

  <meta name="DC.Title" content="Philosophie&amp;Theologie">

  <meta name="DC.Creator" content="Redaktion kath.de">

  <meta name="DC.Contributor" content="J&uuml;rgen Pelzer">

  <meta name="DC.Rights" content="kath.de">

  <meta name="DC.Publisher" content="kath.de">

  <meta name="DC.Date" content="2006-00-01">

  <meta name="DC.Description" content="Lexikon &uuml;ber Philosophie&amp;Theologie">

  <meta name="DC.Language" content="de">

  <meta name="DC.Type" content="Text">

  <meta name="DC.Format" content="text/html">

  <meta name="DC.Identifier" content="http://www.kath.de/lexikon/philosophie_theologie/">

  <meta name="keywords" content="Freiheit, Stimme Gottes, Gewissen, Tiefenpsychologie, Ueber-Ich, Newman, Schelling, Johann Gottlieb Fichte" lang="de">

</head>
<body leftmargin="6" topmargin="6" style="background-color: rgb(255, 255, 255);" marginheight="6" marginwidth="6">

<table border="0" cellpadding="6" cellspacing="0" width="100%">

  <tbody>

    <tr>

      <td align="left" valign="top" width="100">
      <table border="0" cellpadding="0" cellspacing="0" width="216">

        <tbody>

          <tr align="left" valign="top">

            <td width="8"><img src="boxtopleftcorner.gif" alt="" height="8" width="8"></td>

            <td background="boxtop.gif" width="200"><img src="boxtop.gif" alt="" height="8" width="8"></td>

            <td width="8"><img src="boxtoprightcorner.gif" alt="" height="8" width="8"></td>

          </tr>

          <tr align="left" valign="top">

            <td background="boxtopleft.gif"><img src="boxtopleft.gif" alt="" height="8" width="8"></td>

            <td bgcolor="#e2e2e2"><b>Philosophie&amp;Theologie</b></td>

            <td background="boxtopright.gif"><img src="boxtopright.gif" alt="" height="8" width="8"></td>

          </tr>

          <tr align="left" valign="top">

            <td><img src="boxdividerleft.gif" alt="" height="13" width="8"></td>

            <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" height="13" width="8"></td>

            <td><img src="boxdividerright.gif" alt="" height="13" width="8"></td>

          </tr>

          <tr align="left" valign="top">

            <td background="boxleft.gif"><img src="boxleft.gif" alt="" height="8" width="8"></td>

            <td><?php include("logo.html"); ?> </td>

            <td background="boxright.gif"><img src="boxright.gif" alt="" height="8" width="8"></td>

          </tr>

          <tr align="left" valign="top">

            <td><img src="boxbottomleft.gif" alt="" height="8" width="8"></td>

            <td background="boxbottom.gif"><img src="boxbottom.gif" alt="" height="8" width="8"></td>

            <td><img src="boxbottomright.gif" alt="" height="8" width="8"></td>

          </tr>

        </tbody>
      </table>

      <br>

      <table border="0" cellpadding="0" cellspacing="0" width="216">

        <tbody>

          <tr align="left" valign="top">

            <td width="8"><img src="boxtopleftcorner.gif" alt="" height="8" width="8"></td>

            <td background="boxtop.gif" width="200"><img src="boxtop.gif" alt="" height="8" width="8"></td>

            <td width="8"><img src="boxtoprightcorner.gif" alt="" height="8" width="8"></td>

          </tr>

          <tr align="left" valign="top">

            <td background="boxtopleft.gif"><img src="boxtopleft.gif" alt="" height="8" width="8"></td>

            <td bgcolor="#e2e2e2"><strong>Begriff
anklicken</strong></td>

            <td background="boxtopright.gif"><img src="boxtopright.gif" alt="" height="8" width="8"></td>

          </tr>

          <tr align="left" valign="top">

            <td><img src="boxdividerleft.gif" alt="" height="13" width="8"></td>

            <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" height="13" width="8"></td>

            <td><img src="boxdividerright.gif" alt="" height="13" width="8"></td>

          </tr>

          <tr align="left" valign="top">

            <td background="boxleft.gif"><img src="boxleft.gif" alt="" height="8" width="8"></td>

            <td class="V10"><?php include("az.html"); ?>
            </td>

            <td background="boxright.gif"><img src="boxright.gif" alt="" height="8" width="8"></td>

          </tr>

          <tr align="left" valign="top">

            <td><img src="boxbottomleft.gif" alt="" height="8" width="8"></td>

            <td background="boxbottom.gif"><img src="boxbottom.gif" alt="" height="8" width="8"></td>

            <td><img src="boxbottomright.gif" alt="" height="8" width="8"></td>

          </tr>

        </tbody>
      </table>

      </td>

      <td rowspan="2" valign="top">
      <table border="0" cellpadding="0" cellspacing="0" width="100%">

        <tbody>

          <tr align="left" valign="top">

            <td width="8"><img alt="" src="boxtopleftcorner.gif" height="8" width="8"></td>

            <td background="boxtop.gif"><img alt="" src="boxtop.gif" height="8" width="8"></td>

            <td width="8"><img alt="" src="boxtoprightcorner.gif" height="8" width="8"></td>

          </tr>

          <tr align="left" valign="top">

            <td background="boxtopleft.gif"><img alt="" src="boxtopleft.gif" height="8" width="8"></td>

            <td bgcolor="#e2e2e2">
            <h1><font face="Arial, Helvetica, sans-serif">Das
B&ouml;se - seine Darstellung<br>

            </font></h1>

            </td>

            <td background="boxtopright.gif"><img alt="" src="boxtopright.gif" height="8" width="8"></td>

          </tr>

          <tr align="left" valign="top">

            <td><img alt="" src="boxdividerleft.gif" height="13" width="8"></td>

            <td background="boxdivider.gif"><img alt="" src="boxdivider.gif" height="13" width="8"></td>

            <td><img alt="" src="boxdividerright.gif" height="13" width="8"></td>

          </tr>

          <tr align="left" valign="top">

            <td background="boxleft.gif"><img alt="" src="boxleft.gif" height="8" width="8"></td>

            <td style="font-family: Helvetica,Arial,sans-serif;" class="L12"><span style="font-weight: bold;">Krimi-
und Gangsterfilm</span><br>

Neben dem <a href="boese_drache.php">Drachen</a>
und anderen Bildern f&uuml;r den Teufel wird auch das B&ouml;se
in Krimi- und Gangsterfilm immer wieder dargestellt, hier an
menschlichen Protagonisten.<br>

            <br>

Vergleicht man deutsche mit amerikanischen Krimis, gibt es einen
bedeutsamen Unterschied: Im deutschen Krimi, schon in Zeiten der UFA,
beginnt das Verbrechen mit kleinen
Unregelm&auml;&szlig;igkeiten. Jemand f&auml;ngt an, kleine
Betr&uuml;gereien oder Diebst&auml;hle zu begehen. Es geht
immer so weiter, bis er sich pl&ouml;tzlich eine
Bl&ouml;&szlig;e gegeben hat oder durch eine Ungeschicklichkeit
beobachtet wurde. Er muss dann denjenigen umbringen, der ihm auf die
Schliche gekommen ist. Oder der Mann hat eine Geleibte, ehe die Frau
davon erf&auml;hrt, bringt er sie um. Zugespitzt wird die Situation
dadurch, dass z.B. die Frau das Hotel mit in die Ehe gebracht und der
Mann ein Verh&auml;ltnis mit einer Angestellten angefangen hat. Ob
das der Mehrzahl der Mordtaten entspricht, sei dahin gestellt. Wichtig
ist hier, dass den Deutschen ein bestimmtes moralisches Weltbild
vermittelt wird, n&auml;mlich dass das B&ouml;se im
T&auml;ter langsam heranreift, bis er in eine ausweglose Situation
ger&auml;t, aus der er sich nur durch einen Mord befreien kann.<br>

            <br>

Der amerikanische Krimi und Gangsterfilm l&auml;sst das
B&ouml;se von au&szlig;en in eine an sich friedliche
Gemeinschaft hereinbrechen. Gangster tauchen in einer idyllischen
Farmlandschaft auf, ein Hai kommt aus den Tiefen des Meeres an einen
Urlauberstrand, Au&szlig;erirdische bedrohen die
Bev&ouml;lkerung. Es braucht dann den beherzten Amerikaner, der
sich mit einer Flinte bewaffnet und sich dem B&ouml;sen
entgegenstellt, um es in einem Schlusskampf zu besiegen. Nach dem
amerikanischen Weltbild ist das B&ouml;se urspr&uuml;nglich da,
allerdings au&szlig;erhalb der Grenzen. Es versucht
st&auml;ndig, in den durch Recht und B&uuml;rgersinn
abgegrenzten Raum einzudringen, um die Menschen mit dem Tod zu bedrohen.<br>

            <br>

Genau dieses Szenario hat Al-Quaida perfekt in die Realit&auml;t
umgesetzt. Das B&ouml;se drang von au&szlig;en, sogar mit
Flugzeugen, in die amerikanische Gesellschaft und hatte nur Vernichtung
im Sinn gehabt. Dass es einen Helden brauchte, um das B&ouml;se
wieder aus dem Gelobten Land herauszuschaffen, war innerhalb des
Weltbildes der Amerikaner nur logisch. <br>

Das amerikanische Weltbild wurde aberweniger durch den Horrorfilm
gepr&auml;gt, sondern durch den Western. Hier wird eine
Ursprungssituation inszeniert, in der das Recht noch nicht, wie im
Krimi, errichtet ist, sondern der Western zeigt, wie aus einem
rechtlosen Zustand ein durch Recht geordneter wird. Wie im Horrorfilm
braucht es den beherzten Helden, der die Gangster zur Strecke bringt.
Meist werden sie erschossen. <br>

            <br>

            <span style="font-weight: bold;">Western</span><br>

Im Anklang an die Kain- und Abel-Geschichte gibt es in vielen Western
den Gegensatz zwischen Vierz&uuml;chtern und Landbauern. Die Guten
sind hier allerdings die Nachfahren von Kain. Wie kommt es zur
Etablierung des Rechts, in dessen Auftrag der Sheriff t&auml;tig
werden kann?<br>

F&uuml;r den Western gibt es bereits eine Rechtsgemeinschaft,
jedoch noch nicht im Wilden Westen. Im Westen herrscht der, der
&uuml;ber die meisten Waffen verf&uuml;gt und die
unbedenklichsten Gaunern angeheuert hat. Es kommt f&uuml;r den
Helden darauf an, auch im Westen dem Recht Geltung zu verschaffen, das
im Osten des Landes bereits durchgesetzt ist. <br>

            <br>

            <span style="font-weight: bold;">Der
S&uuml;ndenbock (erkl&auml;rt nach Ren&eacute; Girard)</span><br>

Das, was wir heute als <a href="boese_begehren.php">Mobbing</a>
bezeichnen, liegt am Beginn der menschlichen Kultur und damit auch vor
einer Verhaltensordnung. Ren&eacute; Girard versucht mit seinen
Analysen m&ouml;glichst nahe an den &Uuml;bergang von
tierischem Verhalten zur menschlichen Kultur zu kommen. Er sieht die
ersten menschlichen Sippen dem nachahmenden Begehren schutzlos
ausgeliefert. Die Zeit ohne kulturelle Regeln war
&auml;u&szlig;erst bedrohlich f&uuml;r den Fortbestand des
Menschen, denn nur diejenigen Gruppen haben &uuml;berlebt, deren
Mitglieder sich nicht gegenseitig umgebracht haben. Das ist gelungen,
indem sie die angestauten Unzufriedenheiten, die sich zu Aggressionen
verdichten, auf einen einzelnen S&uuml;ndenbock leiten konnten,
diesen ausstie&szlig;en. Erst nach dem Mord am S&uuml;ndenbock
konnten Verbote gegen das Begehren aufgestellt werden. Girard geht
davon aus, dass es eine reale T&ouml;tung am Anfang der
menschlichen Kultur gibt, die sich in den Opferriten niedergeschlagen
hat. Wie sind die Schritte in dem psychischen Prozess zu sehen?<br>

            <br>

Der Effekt des Mobbing, der realen T&ouml;tung ist erst einmal eine
Entlastung von den negativen Gef&uuml;hlen. Jetzt ist die Gruppe
wieder handlungsf&auml;hig. Sie hat aber auch die
Gef&auml;hrlichkeit der durch Stimmungen und Gef&uuml;hle
bewirkten Gewaltbereitschaft erlebt und konnte sie gerade noch einmal
loswerden. Es werden jetzt Regeln eingef&uuml;hrt, die die Objekte
der Nachahmung mit einem Verbot umgeben. So k&ouml;nnen
M&auml;nner nicht mehr mit Gewaltandrohung um eine Frau
rivalisieren. Eigentumsrechte werden eingef&uuml;hrt. Auch eine
Instanz, die f&uuml;r Streitf&auml;lle eine Autorit&auml;t
hat, kann etabliert werden. Dass es ein Gewaltmonopol der
Stammesf&uuml;hrung gibt, l&auml;sst sich noch nicht
verwirklichen. Nicht nur im Western bleibt der Anh&auml;nger des
Rechtsstaates Waffenbesitzer, es gilt noch heute f&uuml;r jeden
US-B&uuml;rger. Blicken wir auf Europa, hat es z.B. in Deutschland
das Duell bis ins 20. Jahrhundert gegeben. In Frankreich hatte bereits
Richelieu mit drakonischen Ma&szlig;nahmen das Duellverbot
durchgesetzt. Er wollte die Adeligen lieber gegen den Feind ins Gefecht
schicken. Rockerbanden setzen das Verbot immer wieder au&szlig;er
Kraft.<br>

Die Hinrichtung des S&uuml;ndenbocks wird rituell in Opfern
darstellend wiederholt. Es k&ouml;nnen wie bei den Azteken
Menschenopfer sein, oder Tiere werden stellvertretend geopfert. Mit dem
rituellen Opfer werden die negativen Energien, die sich jedes Mal
wieder ansammeln, herausgeschafft.<br>

            <br>

            <br>

Eckhard Bieger S.J.<br>

            <br>

            <p><br>

&copy; <a href="http://www.kath.de">www.kath.de</a></p>

            </td>

            <td background="boxright.gif"><img alt="" src="boxright.gif" height="8" width="8"></td>

          </tr>

          <tr align="left" valign="top">

            <td><img alt="" src="boxbottomleft.gif" height="8" width="8"></td>

            <td style="font-family: Helvetica,Arial,sans-serif;" background="boxbottom.gif"><img alt="" src="boxbottom.gif" height="8" width="8"></td>

            <td><img alt="" src="boxbottomright.gif" height="8" width="8"></td>

          </tr>

        </tbody>
      </table>

      </td>

      <td style="vertical-align: top;">
      <script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
google_ad_width = 336;
google_ad_height = 280;
google_ad_format = "336x280_as";
google_ad_type = "text_image";
//2006-10-23: Lexikon Symbole
google_ad_channel = "8495253357";
//--></script>
      <script type="text/javascript" src="show_ads.js">
      </script></td>

    </tr>

    <tr>

      <td align="left" valign="top">&nbsp; </td>

    </tr>

  </tbody>
</table>

</body>
</html>
