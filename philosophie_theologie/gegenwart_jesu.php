<HTML><HEAD><TITLE>Gegenwart Jesu</TITLE>
<META http-equiv=Content-Type content="text/html; charset=iso-8859-1"><LINK 
title=fonts href="kaltefleiter.css" type=text/css 
rel=stylesheet>
<META content="MSHTML 5.50.4134.600" name=GENERATOR></HEAD>
<BODY bgColor=#ffffff leftMargin=6 topMargin=6 marginheight="6" marginwidth="6">
<TABLE cellSpacing=0 cellPadding=6 width="100%" border=0>
  <TBODY>
  <TR>
    <TD vAlign=top align=left width=100> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b>Philosophie&amp;Theologie</b></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td> <?php include("logo.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10"> <?php include("az.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </TD>
    <TD vAlign=top rowSpan=2>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
        <TBODY>
        <TR vAlign=top align=left>
          <TD width=8><IMG height=8 alt="" 
            src="boxtopleftcorner.gif" width=8></TD>
          <TD background=boxtop.gif><IMG height=8 alt="" 
            src="boxtop.gif" width=8></TD>
          <TD width=8><IMG height=8 alt="" 
            src="boxtoprightcorner.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxtopleft.gif><IMG height=8 alt="" 
            src="boxtopleft.gif" width=8></TD>
          <TD bgColor=#e2e2e2> 
            <H1><font face="Arial, Helvetica, sans-serif">Gegenwart Jesu</font></H1>
          </TD>
          <TD background=boxtopright.gif><IMG height=8 
            alt="" src="boxtopright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=13 alt="" src="boxdividerleft.gif" 
            width=8></TD>
          <TD background=boxdivider.gif><IMG height=13 
            alt="" src="boxdivider.gif" width=8></TD>
          <TD><IMG height=13 alt="" 
            src="boxdividerright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxleft.gif><IMG height=8 alt="" 
            src="boxleft.gif" width=8></TD>
          <TD class=L12>
            <P><STRONG><font face="Arial, Helvetica, sans-serif">Die Gegenwart
                  Jesu nach seinem Tod</font></STRONG></P>
            <P><font face="Arial, Helvetica, sans-serif">Im Gottesdienst wendet
                sich die Gemeinde an Gott, z.B. im Vater unser, aber auch an
                Jesus Christus. Zu Beginn einer Messe wird er im Kyrie (Link
                zu Kyrios) als der Herr angeredet. Das ist nur m&ouml;glich,
                wenn Jesus nicht tot ist, sondern als lebendig gegenw&auml;rtig
                geglaubt wird.</font></P>
            <P><font face="Arial, Helvetica, sans-serif">Eine Gegenwart Verstorbener
                kennen wir z.B. durch Tonaufzeichnungen oder Briefe, die wir
                von einem Menschen erhalten haben, der inzwischen
              verstorben ist. Gegenw&auml;rtig mit seinen Gedanken sind Platon
              oder Kant. Ein Komponist wie Franz Schubert bleibt uns gegenw&auml;rtig,
              wenn seien Lieder gesungen oder seine Klaviersonaten gespielt werden.
              Die Gegenwart Jesu wird auch in der Liturgie in seinem Wort gefeiert.
              Bevor das Evangelium des jeweiligen Tages vorgelesen wird, tr&auml;gt
              der Diakon oder der Priester das Evangeliar zum Lesepult, an Feiertagen
              wird es mit Weihrauch inzensiert. Die Gl&auml;ubigen h&ouml;ren
              das Evangelium nicht wie einen Bericht von l&auml;ngst vergangenen
              Ereignissen, sondern da&szlig; Jesus jetzt zu ihnen spricht. <br>
              Gegenw&auml;rtig in besonderer Weise ist Jesus in Brot und Wein
              der Eucharistiefeier. Dieses Mahl hat sich aus dem j&uuml;dischen
              Passahmahl entwickelt. Jesus hat es am Vorabend seiner Hinrichtung
              mit den 12 ausgew&auml;hlten J&uuml;ngern gefeiert. Im Mittelpunkt
              des j&uuml;dischen Passahmahles stand das gebratene Lamm. Jesus
              hat zwei andere Elemente des Mahls,
              das <a href="http://www.kath.de/Kirchenjahr/gruendonnerstag.php">Brot und den Becher Wein</a>, der am Ende des Mahles herumgereicht
              wurde, herausgegriffen und diese Elemente mit einer neuen Bedeutung
              ausgestattet:<br>
&#8222;
              W&auml;hrend des Mahls nahm Jesus das Brot und sprach den Lobpreis;
              dann brach er das Brot, reichte es ihnen und sagte: Nehmt, das
              ist mein Leib. Dann nahm er den Kelch, sprach das Dankgebet, reichte
              ihn den J&uuml;ngern und sie tranken alle daraus. Und er sagte
              zu ihnen: Das ist mein Blut, das Blut des Bundes, das f&uuml;r
              viele vergossen wird. Amen, ich sage euch: Ich werde nicht mehr
              von der Frucht des Weinstocks trinken bis zu dem Tag, an dem ich
              von neuem davon trinke im Reich Gottes&#8220; Markus 14,22-25<br>
              Dieses Ged&auml;chtnismahl haben die J&uuml;nger weiter gefeiert.
              Lukas und Johannes berichten, da&szlig; Jesus sich nach seiner
              Hinrichtung w&auml;hrend solcher Mahlfeiern seinen J&uuml;ngern
              zu erkennen gegeben  hat. Es ist der Glaube
              der Christen, da bis heute Jesus wirklich in dem Brot und dem Wein
              gegenw&auml;rtig, real pr&auml;sent  
              ist.</font></P>
            <P><font face="Arial, Helvetica, sans-serif">Diese Gegenw&auml;rtigkeit setzt voraus, da&szlig; Jesus lebt
                und in einer Weise existiert, die ihn anders gegenw&auml;rtig
                sein l&auml;&szlig;t als wenn er weiter hier als Teil der menschlichen
                Geschichte aufgesucht werden k&ouml;nnte und mit den heutigen
                Kommunikationsmitteln z.B. im Fernsehen zu sehen w&auml;re. Sein
                Tod ist aber endg&uuml;ltig. <a href="auferstehung_jesu.php">Auferstehung</a> bedeutet,
                da&szlig; Jesus
                lebt, aber in einer von uns verschiedenen Existenzweise, die
                wir &quot;Himmel&quot;
                nennen. Wir lokalisieren den Himmel zwar &uuml;ber uns, aber
                eigentlich lebt Jesus &#8222;neben&#8220; uns, ist gegenw&auml;rtig,
                ohne Teil der Welt zu sein. Diese besondere Gegenw&auml;rtigkeit
                w&auml;re
                ohne die Auferstehung nicht denkbar.</font></P>            <p><font face="Arial, Helvetica, sans-serif">Als
                    Petrus und Johannes einen Gel&auml;hmten heilten, kam es
                    unter den Jerusalemern zu einer Diskussion. Petrus antwortet
                    darauf:<br>
&#8222;
              Israeliten, was wundert ihr euch dar&uuml;ber? Was starrt ihr uns
              an, als h&auml;tten wir aus eigener Kraft oder Fr&ouml;mmigkeit
              bewirkt, da&szlig; dieser gehen kann? Der Gott Abrahams, Isaaks
              und Jakobs, der Gott unserer V&auml;ter, hat seinen Knecht Jesus
              verherrlicht, den ihr verraten und vor Pilatus verleugnet habt,
              obwohl dieser entschieden hatte, ihn freizulassen. Ihr aber habt
              den Heiligen und <a href="gottesknecht.php">Gerechten</a> verleugnet
              und die Freilassung eines M&ouml;rders gefordert. Den Urheber des
              Lebens hat ihr get&ouml;tet, aber Gott hat ihn von den Toten auferweckt.
              Daf&uuml;r sind wir Zeugen. Und weil er an seinen (Jesu) Namen
              geglaubt hat, hat dieser Name den Mann hier, den ihr seht und kennt,
              zu Kr&auml;ften gebracht; der Glaube, der von ihm kommt, hat ihm
              vor euer aller Augen die volle Gesundheit geschenkt.&#8220; Apostelgeschichte
              3,12-16<br>
              Petrus vollbringt Heilungen wie Jesus, erkl&auml;rt aber nicht
              sich, sondern Jesus als den, der heilt.<br>
              Gegenw&auml;rtig ist Jesus nicht nur in seinem Wort und in den
              Gestalten von Brot und Wein, sondern durch seinen Heiligen Geist.
              Hierf&uuml;r finden sich im Neuen Testament
              eine F&uuml;lle von Hinweisen.<br>
              Die Gegenw&auml;rtigkeit Jesu hat bei Menschen aller Generationen
              den Glaube als Entsprechung. Dieser Glaube ist kein historischer,
              auch wenn er den Lebensweg Jesu als historische Basis hat. Menschen
              f&uuml;hlen sich von Jesus unmittelbar ber&uuml;hrt und beten zum
              ihm als einem Gegenw&auml;rtigen.</font></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Zitat</strong><br>
  Als Jesus bei seiner Himmelfahrt den J&uuml;ngern den Auftrag
              zur Mission gibt, verspricht er ihnen seine st&auml;ndige Gegenwart:<br>
&#8222;
              Mir ist alle Macht gegeben im Himmel und auf der Erde. Darum geht
              zu allen V&ouml;lkern und macht alle Menschen zu meinen J&uuml;ngern;
              tauft sie auf den Namen des Vaters und des Sohnes und des Heiligen
              Geistes, und lehrt sie, alles zu befolgen, was ich euch geboten
              habe. Seid gewi&szlig;: Ich bin bei euch alle Tage bis ans Ende
              der Welt.&#8220; Matth&auml;us 28,18-20<br>
            </font>            </p>
            <p><font size="2" face="Arial, Helvetica, sans-serif">Text: Eckhard Bieger
                S.J.<br>
            </font>&copy;<a href="http://www.kath.de"> <font face="Arial, Helvetica, sans-serif">www.kath.de</font></a></p>
            </TD>
          <TD background=boxright.gif><IMG height=8 alt="" 
            src="boxright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=8 alt="" src="boxbottomleft.gif" 
            width=8></TD>
          <TD background=boxbottom.gif><IMG height=8 alt="" 
            src="boxbottom.gif" width=8></TD>
          <TD><IMG height=8 alt="" src="boxbottomright.gif" 
            width=8></TD></TR></TBODY></TABLE></TD><td style='vertical-align:top;'>
 <script type="text/javascript"><!--
        google_ad_client = "pub-9537752113242914";
        google_ad_width = 336;
        google_ad_height = 280;
        google_ad_format = "336x280_as";
        google_ad_type = "text_image";
        //2006-10-23: Lexikon Symbole
        google_ad_channel = "8495253357";
        //--></script>
        <script type="text/javascript"
          src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
        </script>
</td>
</TR>
  <TR>
    <TD vAlign=top align=left>&nbsp; </TD>
  </TR></TBODY></TABLE></BODY></HTML>
