---
title: Auferstehung Jesu
author: 
tags: 
created_at: 
images: []
---


# **Der historische Hintergrund des christlichen Auferstehungsglaubens*
***Als Jesus am Kreuz hingerichtet worden war, hatten seine Anhänger keine Erwartungen mehr. Sie waren deprimiert, ihrer Hoffnungen beraubt und verließen Jerusalem. Nach einiger Zeit erklärten sie in öffentlichen Predigten, daß Jesus von Gott aus dem Tod auferweckt und in den Himmel erhöht worden sei. Wie ist dieser Umschwung erklärbar? 

# ** Erklärungen, die zu kurz reichen**
# *Die älteste Erklärung findet sich beim Evangelisten Matthäus. Im 27. Kapitel seines Evangeliums wird folgende Begebenheit nach der Grablegung Jesu berichtet:**
„ Am nächsten Tag gingen die Hohenpriester und Pharisäer gemeinsam zu Pilatus; es war der Tag nach dem Rüsttag. Sie sagten: Herr, es fiel uns ein, daß dieser Betrüger, als er noch lebte, behauptet hat: Ich werde nach drei Tagen auferstehen. Gib also den Befehl, daß das Grab bis zum dritten Tag sicher bewacht wird. Sonst könnten seine Jünger kommen, ihn stehlen und dem Volk sagen: Er ist von den Toten auferstanden. Und dieser letzte Betrug wäre noch schlimmer als alles zuvor. Pilatus antwortete ihnen: Ihr sollt eine Wache haben. Geht und sichert das Grab, so gut ihr könnt. Darauf gingen sie, um das Grab zu sichern. Sie versiegelten den Eingang und ließen die Wache dort.“ Kap. 27, 62-66***

 Nachdem Jesus sich am Ostermorgen den Frauen zu erkennen gegeben hatte und die Wächter Zeugen der Auferstehung geworden waren, „kamen einige (der Wächter) in die Stadt und berichteten den Hohenpriestern alles, was geschehen war. Diese faßten gemeinsam mit den Ältesten den Beschluß, die Soldaten zu bestechen. Sie gaben ihnen viel Geld und sagten: Erzählt den Leuten: Seine Jünger sind bei Nacht gekommen und haben ihn gestohlen, während wir schliefen. Falls der Statthalter davon hört, werden wir ihn beschwichtigen und dafür sorgen, daß ihr nichts zu befürchten habt. Die Soldaten nahmen das Geld und machten alles so, wie man es ihnen gesagt hatte. So kommt es, daß das Gerücht bis heute verbreitet ist.“ Matthäus 28,11-15***

 Die Befürchtung der jüdischen Obrigkeit, die Jünger würden unmittelbar nach der Kreuzigung behaupten, Jesus lebe, war unbegründet. Die Evangelien berichten dann auch, daß die Jünger voller Angst in der Stadt bleiben und nur die Frauen zum Grab gehen, um den Leichnam endgültig zu versorgen, der am Abend des Karfreitages schnell begraben werden mußte, denn die Sabbatruhe, die mit der Dämmerung begann, mußte beachtet werden. Eine Absicht der Jünger, den Leichnam zu stehlen, war nicht zu erkennen. ***

 Eine andere Version wird im Islam erzählt: Jesus sei nicht am Kreuz gestorben, sondern wieder losgekommen, wäre mit Maria von Magdala nach Persien gegangen und sei dort gestorben. Eine andere Version wird auch schon früh von den Gnostikern  verbreitet: Jesus, der nicht wirklich Mensch war, sondern nur in einem Scheinleib sich zeigte, verließt die Erde vor der Kreuzigung, gekreuzigt wurde an seiner Stelle Simon von Cyrene.***

# *
 Der Innere Prozeß der Jünger*****

# **Die Bibel berichtet davon, daß die Anhänger Jesu nur langsam zum Glauben an die Auferstehung gekommen sind, so daß es historisch unwahrscheinlich ist, daß sie selbst die Idee entwickelt hätten, Jesus sei von den Toten auferstanden. Mit seinem Tod waren ihre Hoffnungen, Jesus würde das messianische Reich aufrichten, zerbrochen. Für einen Juden war es besonders schlimm, daß Jesus nicht nur von der jüdischen Obrigkeit verurteilt wurde, sondern daß er an die Römer, d.h. Heiden, ausgeliefert worden war. Gott mußte seinen Gesalbten, den sie als den ***[Messias](messias_christus.php)  erkannt hatten, verlassen haben. Sie hatten sich Jesus deshalb angeschlossen, weil sie durch seine Predigt und seine Wundertaten überzeugt wurden, daß er der Gesandte Gottes sein mußte. Gott konnte seinen Gesandten nicht so schmählich enden lassen.**
 Wenn sie von sich aus nicht in der Lage waren, das Scheitern des Messias in eine Frohe Botschaft umzuinterpretieren, dann müssen sie von außen zu der Vorstellung gebracht worden sein, daß Jesus nicht bei den Toten geblieben ist, sondern lebt. Es war dann auch die Botschaft von der Auferstehung und nicht der Bericht über die Hinrichtung am Kreuz, die Ausgangspunkt des „Neuen Weges“ wurde, den die Anhänger Jesu innerhalb des Judentums gehen wollten, ehe es zur Trennung von Christen und Juden kam. Der Sonntag als Feiertag innerhalb der Woche hat von Anfang an die Auferstehung zum Inhalt, ***[Ostern](http://www.kath.de/Kirchenjahr/ostern_ewiges-leben.php)  ist das zentrale Fest der christlichen Kirche, das von Anfang an gefeiert wurde, während unser Weihnachtsfest erst im 4. Jahrhundert entstand.***


„ Wenn aber verkündet wird, daß Christus von den Toten auferweckt worden ist, wie können dann einige von euch sagen: eine Auferstehung der Toten gibt es nicht. Wenn es keine Auferstehung der Toten gibt, ist auch Christus nicht auferweckt worden. Ist aber Christus nicht auferweckt worden, dann ist unsere Verkündigung leer und euer Glaube sinnlos. .... Wenn wir unsere Hoffnung nur in diesem Leben auf Christus gesetzt haben, sind wir erbärmlicher daran als alle anderen Menschen. Nun aber ist Christus von den Toten auferweckt worden als der Erste der Entschlafenen.“ schreibt Paulus im 1. Korintherbrief 15,12-14, 19-20***

 Dieser Brief ist älter als die Evangelien, er ist zwischen 53 und 55 geschrieben, also 20 Jahre nach der Kreuzigung Jesu. In ihm findet sich der älteste schriftliche Bericht über die Auferstehung. Paulus faßt die Gute Botschaft, das Evangelium, in einem frühen Credo zusammen:***


„ Christus ist für unsere Sünden gestorben, gemäß der Schrift,***

 und ist begraben worden.                Er ist am dritten Tag auferweckt worden, gemäß der Schrift.                Und er erschien dem Kephas, dann den Zwölf. Danach erschien er mehr als fünfhundert Brüdern zugleich, die meisten von ihnen sind noch am Leben, einige sind entschlafen. Danach erschien er dem Jakobus, dann allen Aposteln. Als letztem von allen erschien er mir, dem Unerwarteten, der 'Mißgeburt‘. Denn ich bin der Geringste von den Aposteln; ich bin nicht wert, Apostel genannt zu werden, weil ich die Kirche Gottes verfolgt habe. Doch durch die Gnade Gottes bin ich, was ich bin.“ Kap 15,3-10***

 Die von Paulus aufgezählte Liste derjenigen, die eine Auferstehungserfahrung gemacht haben, umfaßt mehr Namen als in den Auferstehungsberichten der Evangelien zu finden sind.***

# **Die Überzeugung, daß Jesus lebt****
 Der Bericht des Paulus wie auch die der Evangelisten zeigen, daß Jesus sich seinen Anhängern gezeigt hat und sie durch diese Erscheinungen zur Überzeugung kamen, daß er nicht im Tod geblieben, sondern auferweckt worden ist, daß er lebt.***

 Um die in den Berichten beschriebenen Phänomene einordnen zu können, muß im Auge behalten werden, daß Jesus mit der Auferstehung nicht wie der aus dem Grab gerufene Lazarus in dieses Leben und damit in die menschliche Geschichte zurückgekehrt ist. Jesus bleibt im Sinne der menschlichen Existenz tot. Die Auferstehung bezieht sich auf eine andere Wirklichkeit, Jesus ist mit seinem Leib (und seiner Seele) in eine himmlische Existenz hinüber gegangen. Er bleibt dadurch ***[gegenwärtig](gegenwart_jesu.php), nicht nur im Gedächtnis seiner Anhänger oder so wie Mozart gegenwärtig bleibt, wenn seine Kompositionen erklingen. Aus dem neuen Leben kann Jesus sich seinen Anhängern zeigen, so wie er Paulus auf der Straße von Jerusalem nach Damaskus begegnet ist. Den Christen ist verheißen, daß sie wie Jesus auch in einer himmlische Existenz, mit Leib und Seele, aufgenommen werden. Dieser Glaube auf einen Leben nach dem Tod bezieht ausdrücklich die Existenz des Leibes ein.***

# **Zitate*
# **Von den Begegnungen Jesu mit seinen Jüngern gibt es verschiedene Berichte. In der Apostelgeschichte wird über Paulus berichtet:**
 Saulus wütete immer noch mit Drohung und Mord gegen die Jünger des Herrn. Er ging zum Hohenpriester und erbat von ihm Briefe an die Synagogen in Damaskus, um die Anhänger des Neues Weges, Männer und Frauen, die er dort finde, zu fesseln und nach Jerusalem zu bringen. Unterwegs aber, als er sich bereits Damaskus näherte, geschah es, daß ihn plötzlich ein Licht vom Himmel umstrahlte. Er stürzte zu Boden und hörte, wie eine Stimme zu ihm sagte: Saul, Saul, warum verfolgst du mich? Er antwortete: Wer bist du Herr? Dieser sagte: Ich bin Jesus, den du verfolgst. Steh auf und geh in die Stadt; dort wird dir gesagt werden, was du tun sollst. Seine Begleiter standen sprachlos da; sie hörten zwar die Stimme, sahen aber niemand.“ Kap. 9,1-7***

 Offensichtlich hatte Saulus eine Vision. In dem Bericht ist zu lesen, daß er die Augen geschlossen hatte und danach nicht mehr sehen konnte. In Damaskus wird er von einem Mann namens Hananias besucht und kann wieder sehen.***

 Der Autor der Apostelgeschichte, Lukas, berichtet in seinem Evangelium von zwei Jüngern, die Jerusalem enttäuscht in Richtung Emmaus verlassen. Sie erkennen Jesus nicht, der sich ihnen zugesellt und ihnen an Schriftstellen des Alten Testaments aufzeigt, daß der „Messias all das erleiden mußte, um so in seine Herrlichkeit zu gelangen.“ Die beiden bitten ihn, mit in das Gasthaus zu gehen. „und als er mit ihnen bei Tisch war, nahm er das Brot, sprach den Lobpreis, brach das Brot und gab es ihnen. Da gingen ihnen die Augen auf und sie erkannten ihn; dann sahen sie ihn nicht mehr.“ Kap. 24, 30-31***

 Mehrere andere Berichte von einer Begegnung mit Jesus finden ebenfalls bei einem Mahl statt. Lukas 24,36-49 Johannes 20,19-23, dann der Bericht über den ungläubigen Thomas 20,24-29 und das Mahl am See Genesareth 21,1-14 

# **Paul Tillich zeigt, wie die Auferstehung nicht nur ein geschichtliches Ereignis ist, sondern ein Neues Sein herbeiführt. **
 Als Petrus Jesus „den Christus“ nannte, erwartete er das Kommen eines neuen Standes der Dinge durch ihn. Diese Erwartung liegt im Titel „Christus“ beschlossen. Aber das, was die Jünger erwartet hatten, erfüllte sich nicht. Der Stand der Dinge – in der Natur wie in der Geschichte –blieb unverändert, und der, von dem man geglaubt hatte, daß er den neuen Äon bringen würde, wurde durch die Mächte des alten Äons zerbrochen. Das bedeutete für die Jünger, daß sie entweder den Zusammenbruch ihrer Hoffnung hinnehmen oder deren Inhalt radikal verwandeln mußten. Sie wählten den zweite Weg und setzten das Neue Sein mit dem Sein Jesu als des Gekreuzigten gleich. Die synoptischen Bereichte (d.h. die Evangelien von Matthäus, Markus und Lukas) wollen zeigen, daß Jesus selbst darin vorangegangne war und seinen messianischen Anspruch mit der Voraussicht seines gewaltsamen Todes vereinigt hatte. Die gleichen Berichte zeigen, daß die Jünger zunächst dieser Verbindung Widerstand leisteten. Erst die Erfahrungen, die als Ostern und Pfingsten beschrieben werden, haben ihren Glauben an den paradoxen Charakter des messianischen Anspruchs geschaffen, und es war Paulus, der den theologischen Rahmen gab, innerhalb dessen das Paradox verstanden und gerechtfertigt werden konnte. Einer der Wege zur Lösung des Problems war die Unterscheidung zwischen dem ersten und dem zweiten Kommen des Christus. Der neue Stand der Dinge wird mit dem zweiten Kommen, nämlich der Wiederkunft Christi in Herrlichkeit, geschaffen werden. In der Periode zwischen dem ersten und dem zweiten Kommen ist das Neue Sein nur in ihm gegenwärtig, nicht in der Welt.***

 Systematische Theologie 1957, S. 129 

# **Wolfhart Pannenberg über die Bedeutung der Auferstehung für Jesus uns seine Jünger:**
 Die Erwartung des irdischen Jesus richtete sich … aller Wahrscheinlichkeit nach nicht auf eine nur ihm sozusagen privat widerfahrende Auferweckung der Toten, sondern auf die nahe bevorstehende allgemeine Totenauferweckung, die natürlich auch ihm selbst, falls er zuvor sterben sollte, widerfahren wäre. Als den Jüngern Jesu dann der Auferstandene begegnete, da haben sie das zweifellos ebenfalls als den Beginn der Endereignisse verstanden. ….***

 Die endgültige göttliche Bestätigung Jesu wird erst durch das Geschehen seiner Wiederkunft erfolgen. Dann erst wird die Offenbarung Gottes in Jesus in ihrer endgültigen, unwiderstehlichen Herrlichkeit sichtbar werden. ***

 Grundzüge der Christologie, 1964 S. 61 und 105 

# **Text: Eckhard Bieger S.J.*****
 ©***[ ***www.kath.de](http://www.kath.de) 
