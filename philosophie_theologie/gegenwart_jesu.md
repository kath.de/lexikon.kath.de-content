---
title: Gegenwart Jesu
author: 
tags: 
created_at: 
images: []
---


# **Die Gegenwart Jesu nach seinem Tod*
***Im Gottesdienst wendet sich die Gemeinde an Gott, z.B. im Vater unser, aber auch an Jesus Christus. Zu Beginn einer Messe wird er im Kyrie (Link zu Kyrios) als der Herr angeredet. Das ist nur möglich, wenn Jesus nicht tot ist, sondern als lebendig gegenwärtig geglaubt wird. 

# **Eine Gegenwart Verstorbener kennen wir z.B. durch Tonaufzeichnungen oder Briefe, die wir von einem Menschen erhalten haben, der inzwischen verstorben ist. Gegenwärtig mit seinen Gedanken sind Platon oder Kant. Ein Komponist wie Franz Schubert bleibt uns gegenwärtig, wenn seien Lieder gesungen oder seine Klaviersonaten gespielt werden. Die Gegenwart Jesu wird auch in der Liturgie in seinem Wort gefeiert. Bevor das Evangelium des jeweiligen Tages vorgelesen wird, trägt der Diakon oder der Priester das Evangeliar zum Lesepult, an Feiertagen wird es mit Weihrauch inzensiert. Die Gläubigen hören das Evangelium nicht wie einen Bericht von längst vergangenen Ereignissen, sondern daß Jesus jetzt zu ihnen spricht. **
 Gegenwärtig in besonderer Weise ist Jesus in Brot und Wein der Eucharistiefeier. Dieses Mahl hat sich aus dem jüdischen Passahmahl entwickelt. Jesus hat es am Vorabend seiner Hinrichtung mit den 12 ausgewählten Jüngern gefeiert. Im Mittelpunkt des jüdischen Passahmahles stand das gebratene Lamm. Jesus hat zwei andere Elemente des Mahls, das ***[Brot und den Becher Wein](http://www.kath.de/Kirchenjahr/gruendonnerstag.php), der am Ende des Mahles herumgereicht wurde, herausgegriffen und diese Elemente mit einer neuen Bedeutung ausgestattet:***


„ Während des Mahls nahm Jesus das Brot und sprach den Lobpreis; dann brach er das Brot, reichte es ihnen und sagte: Nehmt, das ist mein Leib. Dann nahm er den Kelch, sprach das Dankgebet, reichte ihn den Jüngern und sie tranken alle daraus. Und er sagte zu ihnen: Das ist mein Blut, das Blut des Bundes, das für viele vergossen wird. Amen, ich sage euch: Ich werde nicht mehr von der Frucht des Weinstocks trinken bis zu dem Tag, an dem ich von neuem davon trinke im Reich Gottes“ Markus 14,22-25***

 Dieses Gedächtnismahl haben die Jünger weiter gefeiert. Lukas und Johannes berichten, daß Jesus sich nach seiner Hinrichtung während solcher Mahlfeiern seinen Jüngern zu erkennen gegeben  hat. Es ist der Glaube der Christen, da bis heute Jesus wirklich in dem Brot und dem Wein gegenwärtig, real präsent   ist. 

***Diese Gegenwärtigkeit setzt voraus, daß Jesus lebt und in einer Weise existiert, die ihn anders gegenwärtig sein läßt als wenn er weiter hier als Teil der menschlichen Geschichte aufgesucht werden könnte und mit den heutigen Kommunikationsmitteln z.B. im Fernsehen zu sehen wäre. Sein Tod ist aber endgültig. ***[Auferstehung](auferstehung_jesu.php) bedeutet, daß Jesus lebt, aber in einer von uns verschiedenen Existenzweise, die wir "Himmel" nennen. Wir lokalisieren den Himmel zwar über uns, aber eigentlich lebt Jesus „neben“ uns, ist gegenwärtig, ohne Teil der Welt zu sein. Diese besondere Gegenwärtigkeit wäre ohne die Auferstehung nicht denkbar.            

# **Als Petrus und Johannes einen Gelähmten heilten, kam es unter den Jerusalemern zu einer Diskussion. Petrus antwortet darauf:**
„ Israeliten, was wundert ihr euch darüber? Was starrt ihr uns an, als hätten wir aus eigener Kraft oder Frömmigkeit bewirkt, daß dieser gehen kann? Der Gott Abrahams, Isaaks und Jakobs, der Gott unserer Väter, hat seinen Knecht Jesus verherrlicht, den ihr verraten und vor Pilatus verleugnet habt, obwohl dieser entschieden hatte, ihn freizulassen. Ihr aber habt den Heiligen und ***[Gerechten](gottesknecht.php) verleugnet und die Freilassung eines Mörders gefordert. Den Urheber des Lebens hat ihr getötet, aber Gott hat ihn von den Toten auferweckt. Dafür sind wir Zeugen. Und weil er an seinen (Jesu) Namen geglaubt hat, hat dieser Name den Mann hier, den ihr seht und kennt, zu Kräften gebracht; der Glaube, der von ihm kommt, hat ihm vor euer aller Augen die volle Gesundheit geschenkt.“ Apostelgeschichte 3,12-16***

 Petrus vollbringt Heilungen wie Jesus, erklärt aber nicht sich, sondern Jesus als den, der heilt.***

 Gegenwärtig ist Jesus nicht nur in seinem Wort und in den Gestalten von Brot und Wein, sondern durch seinen Heiligen Geist. Hierfür finden sich im Neuen Testament eine Fülle von Hinweisen.***

 Die Gegenwärtigkeit Jesu hat bei Menschen aller Generationen den Glaube als Entsprechung. Dieser Glaube ist kein historischer, auch wenn er den Lebensweg Jesu als historische Basis hat. Menschen fühlen sich von Jesus unmittelbar berührt und beten zum ihm als einem Gegenwärtigen. 

# **Zitat****
 Als Jesus bei seiner Himmelfahrt den Jüngern den Auftrag zur Mission gibt, verspricht er ihnen seine ständige Gegenwart:***


„ Mir ist alle Macht gegeben im Himmel und auf der Erde. Darum geht zu allen Völkern und macht alle Menschen zu meinen Jüngern; tauft sie auf den Namen des Vaters und des Sohnes und des Heiligen Geistes, und lehrt sie, alles zu befolgen, was ich euch geboten habe. Seid gewiß: Ich bin bei euch alle Tage bis ans Ende der Welt.“ Matthäus 28,18-20***

# **Text: Eckhard Bieger S.J.**
 ©***[ ***www.kath.de](http://www.kath.de) 
