<HTML><HEAD><TITLE>Gottesbeweis aus der Freiheit</TITLE>
<meta name="title" content="Philosophie&amp;Theologie">
<meta name="author" content="Redaktion kath.de">
<meta name="publisher" content="kath.de">
<meta name="copyright" content="kath.de">
<meta name="description" content="">
<meta name="abstract" content="Lexikon &uuml;ber Philosophie&amp;Theologie">
<meta http-equiv="content-language" content="de">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="date" content="2006-00-01">
<meta name="robots" content="index,follow">
<meta name="revisit-after" content="10 days">
<meta name="revisit" content="after 10 days">
<meta name="DC.Title" content="Philosophie&amp;Theologie">
<meta name="DC.Creator" content="Redaktion kath.de">
<meta name="DC.Contributor" content="J�rgen Pelzer">
<meta name="DC.Rights" content="kath.de">
<meta name="DC.Publisher" content="kath.de">
<meta name="DC.Date" content="2006-00-01">
<meta name="DC.Description" content="Lexikon &uuml;ber Philosophie&amp;Theologie">
<meta name="DC.Language" content="de">
<meta name="DC.Type" content="Text">
<meta name="DC.Format" content="text/html">
<meta name="DC.Identifier" content="http://www.kath.de/lexikon/philosophie_theologie/">
<meta name="keywords" lang="de" content="Anspruch, Gott, Entscheidung, Freiheit, Trotzalter, Sarte">
</HEAD>
<BODY bgColor=#ffffff leftMargin=6 topMargin=6 marginheight="6" marginwidth="6">
<TABLE cellSpacing=0 cellPadding=6 width="100%" border=0>
  <TBODY>
  <TR>
    <TD vAlign=top align=left width=100> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b>Philosophie&amp;Theologie</b></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td> <?php include("logo.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10"> <?php include("az.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </TD>
    <TD vAlign=top rowSpan=2>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
        <TBODY>
        <TR vAlign=top align=left>
          <TD width=8><IMG height=8 alt="" 
            src="boxtopleftcorner.gif" width=8></TD>
          <TD background=boxtop.gif><IMG height=8 alt="" 
            src="boxtop.gif" width=8></TD>
          <TD width=8><IMG height=8 alt="" 
            src="boxtoprightcorner.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxtopleft.gif><IMG height=8 alt="" 
            src="boxtopleft.gif" width=8></TD>
          <TD bgColor=#e2e2e2> 
            <H1><font face="Arial, Helvetica, sans-serif"> Gottesbeweis aus
                der Freiheit</font></H1>
          </TD>
          <TD background=boxtopright.gif><IMG height=8 
            alt="" src="boxtopright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=13 alt="" src="boxdividerleft.gif" 
            width=8></TD>
          <TD background=boxdivider.gif><IMG height=13 
            alt="" src="boxdivider.gif" width=8></TD>
          <TD><IMG height=13 alt="" 
            src="boxdividerright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxleft.gif><IMG height=8 alt="" 
            src="boxleft.gif" width=8></TD>
          <TD class=L12>
            <P><STRONG><font face="Arial, Helvetica, sans-serif">  Das Unbedingte der Freiheit kann nicht aus dem
                  Menschen selbst kommen</font></STRONG></P>
            <P><font face="Arial, Helvetica, sans-serif">In unserem Alltag f&uuml;hlen wir uns meist von Aufgaben bestimmt,
              die wir f&uuml;r andere bzw. aus Sorge f&uuml;r unsere Gesundheit
              und unseren Lebensunterhalt erledigen m&uuml;ssen. Wenn wir morgens
              aufwachen, haben wir daher selten ein Gef&uuml;hl der Freiheit,
              sondern des M&uuml;ssens, denn uns f&auml;llt ein, was wir heute
              alles erledigen m&uuml;ssen. Aber in allem, was uns von au&szlig;en
              zu tun aufgetragen scheint, gibt es aber immer die &Uuml;berlegung:
              M&uuml;ssen wir das eigentlich tun? In dem &Uuml;berlegen, ob wir
              wirklich &#8222;m&uuml;ssen&#8220;, meldet sich unsere Freiheit.
              Wenn wir uns fragen &#8222;Kann man das von uns fordern?&#8220; vergleichen
              wir die Forderung mit unserer Lebensrichtung. Bringen uns die Forderungen
              nicht von unserem Lebensweg ab? Wenn wir so fragen, erscheint die
              Freiheit, denn diese verlangt von uns, da&szlig; wir den Kurs unseres
              Lebensschiffes nicht von anderen bestimmen lassen. Die Freiheit
              verlangt, da&szlig; wir selbst entscheiden, was wir mit unserem
              Leben wollen und anstreben. Wir d&uuml;rfen das nicht an andere
              delegieren, weder an unsere Eltern noch an unseren Lehrer, an einen
              Priester oder einen Guru eines esoterischen Zirkels. Denn wenn
              wir es anderen &uuml;berlassen, den Kurs unseres Lebens zu bestimmen,
              dann ist in dieser Entscheidung bereits unsere Freiheit im Spiel.
              Wir entscheiden, mehr oder weniger frei, da&szlig; ein anderer
              entscheiden soll. Das sollen wir aber nicht, denn auch wenn andere
              entscheiden, k&ouml;nnen wir nicht &uuml;ber unser Gewissen hinweggehen,
              das uns abverlangt, nichts B&ouml;ses zu tun. In Krimis und Reportagen
              wird dieser Anspruch immer wieder verdeutlicht, wenn Menschen sich
              zu einem Verbrechen oder mit Geldangeboten zur Prostitution &uuml;berreden
              lassen. <br>
              In der Freiheit liegt der Anspruch, unsere Verantwortung nicht
              an andere zu delegieren, sondern selbst unser Leben in die Hand
              zu nehmen. Dieser <a href="freiheit.php">Anspruch der Freiheit</a> ist unerbittlich.</font></P>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Die Unbedingtheit der Freiheit</strong><br>
  Wir sind zu vielem frei und in Gedanken k&ouml;nnen wir uns noch
              mehr vorstellen, was wir tun k&ouml;nnten. So k&ouml;nnen dem Spruch
              unseres Gewissens folgen oder auch nicht. In Bezug auf unsere Freiheit
              sind wir nicht frei. Selbst wenn wir unsere Freiheit aufgeben,
              indem wir andere &uuml;ber uns entscheiden lassen, auch in diesem
              Lassen war unsere Freiheit im Spiel. Als kleines Kind erproben
              wir die Freiheit bereits im Trotzalter, in der Pubert&auml;t wird
              sie uns voll bewu&szlig;t. Dieser Anspruch der Freiheit ist unbedingt.
              Das bedeutet, da&szlig; die Freiheit keine Bedingung gelten l&auml;&szlig;t,
              die uns offen l&auml;&szlig;t, ob wir die Verantwortung f&uuml;r
              unser Leben &uuml;bernehmen oder nicht. Denn wenn sie uns entscheiden
              lie&szlig;e, ob wir frei sein wollen oder nicht, w&auml;re das
              schon wieder eine <a href="freiheit_entscheidung.php">freie Entscheidung</a>. Aber die Freiheit will ohne
              Wenn und Aber, da&szlig; wir uns als freie Menschen entfalten,
              d.h. eine Ausbildung und einen Beruf w&auml;hlen, uns f&uuml;r
              oder gegen eine Partnerschaft, f&uuml;r oder gegen Kinder entscheiden.
              Wir sollen uns auch f&uuml;r bestimmte Werte entscheiden, an denen
              wir unsere weiteren Entscheidungen ausrichten. Diese Werte k&ouml;nnen
              Einsatz f&uuml;r den Frieden, soziales Engagement, beruflicher
              Erfolg, interessante Urlaube, k&ouml;rperliche Leistungsf&auml;higkeit
              u.a. sein. <br>
              Das &#8222;Ohne Wenn und Aber&#8220; der Freiheit ist nicht etwas
              blo&szlig; Gedachtes. Es ragt vielmehr in unser Leben hinein und
              wir m&uuml;ssen uns dem unbedingten Anspruch der Freiheit stellen. <br>
              Wo kommt dieser Anspruch her? Wir haben unsere Freiheit nicht von
              unseren Eltern noch vom Staat geschenkt bekommen. Sie kann auch
              nicht aus der Evolution kommen, denn diese ist nach dem Prinzip
              des Zufalls organisiert. Zuf&auml;llig entstehen Mutationen und
              im Kampf ums &Uuml;berleben zeigt sich, was eine gute Mutation
              war. Aus Zufall kann aber nichts Notwendiges entstehen, zumal der
              Mensch auch in sich nicht notwendig ist. Er k&ouml;nnte ja auch
              nicht sein. Das Unbedingte ohne Wenn und Aber meldet sich in einem
              aus Zufall entstandenen Wesen, das auch nicht sein k&ouml;nnte.
              Woher hat der Mensch seine Freiheit, die ja in sich alles andere
              als beliebig ist, sondern den Menschen unter einen unbedingten
              Anspruch stellt? Er mu&szlig; diese Freiheit von einer anderen
              Freiheit haben. Diese Macht, woher die Freiheit mit ihrem unbedingten
              Anspruch kommt, mu&szlig; die Freiheit des Menschen wollen. Dieses
              Wollen der menschlichen Freiheit zeigt sich deutlich in dem der
              Freiheit mitgegebenen Anspruch, n&auml;mlich seine Freiheit auch
              auszu&uuml;ben. Eigentlich k&ouml;nnten wir weiter folgern, da&szlig; die
              Freiheit von dem herkommt, der den Menschen als geistiges Wesen
              geschaffen hat. Aber der Sch&ouml;pfer wird als Rivale der menschlichen
              Freiheit verd&auml;chtigt.</font></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Gott, Feind oder Freund der menschlichen Freiheit?</strong><br>
  Wenn Kinder, die im Glauben an Gott aufgewachsen sind, in die Pubert&auml;t
              kommen, ver&auml;ndert sich auch ihre Beziehung zu Gott grundlegend.
              War Gott bisher der gro&szlig;e Schutzherr, der die B&ouml;sen
              bestraft und die Guten belohnt, tritt er in der Pubert&auml;t als
              Garant der sittlichen Gebote auf. Stammen die Zehn Gebote nicht
              von Gott und pocht er nicht unerbittlich auf deren Einhaltung.
              Zudem stand Gott bisher hinter den Autorit&auml;ten, ob Eltern,
              Kindergarten, Schule oder Polizei. F&uuml;r das Kind ist es unproblematisch,
              da&szlig; diese Autorit&auml;ten im Auftrag Gottes handeln. Wer
              aber seine Freiheit entdeckt, st&ouml;&szlig;t st&auml;ndig an
              Grenzen und sei es nur im Kampf mit den Eltern, um 22 Uhr abends
              zu Hause zu sein. F&uuml;r den franz&ouml;sischen Philosophen Jean
              Paul Sartre und viele andere bilden Freiheit und Gott einen unvers&ouml;hnlichen
              Gegensatz. Wer sich f&uuml;r die eigene Freiheit entscheidet, der
              mu&szlig; sich von Gott abwenden. Sartre geht sogar davon aus,
              da&szlig; der Mensch die Freiheit von Gott hat, aber er mu&szlig; sich
              von Gott abwenden, wenn er die eigene Freiheit leben will. <br>
              Nun sind die menschlichen Gebote, gegen die man in der Pubert&auml;t
              rebelliert, nicht das Thema der Freiheit, auch wenn es unsere Freiheit
              ist, die uns die &Uuml;bertretung der Gebote erm&ouml;glicht. Das
              Thema unserer Freiheit liegt hinter den Geboten, denn diese weisen
              mich nicht an, welchen Beruf ich w&auml;hlen, f&uuml;r welchen
              Partner, Partnerin ich mich entscheiden soll. Der Kurs meines Lebensschiffes
              mu&szlig; nur deshalb die Gebote im Auge behalten, weil sie meine
              Freiheit sch&uuml;tzen. Denn die Gebote, &#8222;du sollst nicht
              t&ouml;ten&#8220;, &#8222;du sollst nicht falsches Zeugnis geben&#8220; schlagen
              auf meine Freiheit zur&uuml;ck. Wenn z.B. andere es f&uuml;r unproblematisch
              halten, mich zu bestehlen oder meine Partnerin zu verf&uuml;hren,
              bin ich faktisch unfrei. Deshalb ist die Frage berechtigt: Warum
              sollte die Macht, die uns unsere Freiheit geschenkt und die Einl&ouml;sung
              der Freiheit fordert, uns unsere Freiheit neiden? Einige Textausz&uuml;ge
              aus einem Theaterst&uuml;ck Sartres zeigen, da&szlig; man einen
              Gott zeichnen mu&szlig;, dessen Bild &uuml;berrascht. <br>
              <br>
              <strong>Zitate</strong><br>
              Sartre hat in seinem Drama die Fliegen die Frage nach der Freiheit
              behandelt. Er verlegt das Drama in die griechische Antike. Der
              Sohn Orest kommt in die Stadt seiner Eltern. Die Mutter, Klyt&auml;mnestra
              hat, da sie sich einem anderen Mann, &Auml;gist, zugewandt hatte,
              den Vater Orests, ihren Mann Agamemnon ermordet. Jean Paul Sartre, &#8222;Die
              Fliegen&#8220;; Schmutzige H&auml;nde&#8220;, Zwei Dramen, Hamburg
              1991:<br>
&#8222;
              Wenn einmal die Freiheit in einer Menschenseele aufgebrochen ist,
              k&ouml;nnen die G&ouml;tter nichts mehr gegen diese Menschen. Denn
              das ist eine Menschenangelegenheit, und es ist Sache der anderen
              Menschen &#8211; und nur ihre &#8211;, ihn laufen zu lassen oder
              ihn zu erw&uuml;rgen.&#8220; Szene 5 im 2. Akt.</font></p>
            <p><font face="Arial, Helvetica, sans-serif">&#8222;Wenn einmal die Freiheit in einer Menschenseele aufgebrochen
              ist, k&ouml;nnen die G&ouml;tter nichts mehr gegen diesen Menschen.
              Denn das ist eine Menschenangelegenheit, und es ist Sache der anderen
              Menschen &#8211; und nur ihre &#8211;, ihn laufen zu lassen oder
              ihn zu erw&uuml;rgen.&#8220; Szene 5 im 2. Akt.</font></p>
            <p><font face="Arial, Helvetica, sans-serif">&#8222;Ich bin weder Herr noch Knecht, Jupiter, ich bin meine
              Freiheit! Kaum hast du mich erschaffen, so habe ich auch schon
              aufgeh&ouml;rt, dein eigen zu sein; ........... pl&ouml;tzlich
              ist die Freiheit auf mich herabgest&uuml;rzt, und ich erstarrte,
              die Natur tat einen Sprung zur&uuml;ck, und ich hatte kein Alter
              mehr, und ich habe mich ganz alleine gef&uuml;hlt, inmitten deiner
              kleinen, harmlosen Welt, wie einer, der seinen Schatten verloren
              hat, und es war nichts mehr am Himmel, weder Gut noch B&ouml;se,
              noch irgendeiner, um mir Befehle zu geben.&#8220;<br>
              2. Szene im 3. Akt</font></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Reflexion zur
                  Alternative &#8222;menschliche
                Freiheit oder Gott&#8220;</strong><br>
  Sartre zeichnet einen Gott, der den Menschen mit einer Freiheit
                so ausgestattet hat, da&szlig; diese Freiheit mit Notwendigkeit
                dazu f&uuml;hrt, da&szlig; der Mensch sich von seinem Sch&ouml;pfer
                verabschiedet. Dann w&auml;re dieser Gott aber nicht mehr in
                sich das h&ouml;chste Gut, wenn die Freiheit den Menschen dazu
                zwingt, sich von diesem Gott loszusagen. Denn das Lossagen m&uuml;&szlig;te
                durch ein Gut motiviert sein, das h&ouml;her ist als Gott. Das w&auml;re
                dann ein anderer Gott oder ein anderes h&ouml;chstes
                Gut m&uuml;&szlig;te existieren, auf das sich der Mensch ausrichtet.
                Oder, das w&auml;re auch eine weitere Konsequenz, die Freiheit
                w&auml;re selbst das h&ouml;chste Gut. Wenn aber die Freiheit
                des Menschen das h&ouml;chste Gut w&auml;re, dann m&uuml;&szlig;te
                Gott, insofern er das Gute will, sich der Freiheit des Menschen
                unterwerfen.
                Diese Konsequenz hat Nietzsche mit seiner &#8222;Gott ist tot&#8220; -These
                durchdacht. Er hat eine menschliche Freiheit erdacht, die nicht
                mehr dem h&ouml;chsten Guten verpflichtet ist, sondern ihrem &#8222;Willen
                zur Macht&#8220;. Es ist aber so, da&szlig; die Freiheit nur in sich
                frei bleibt, wenn sie sich auf das <a href="gottesbeweis_hoechstes_gut.php">h&ouml;chste
                Gute</a> ausrichtet.</font></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Zitate</strong><br>
&#8222;
              Der Mensch verliert seine Freiheit, wenn es kein ihn &uuml;berschreitendes
              Unbedingtes gibt. Abh&auml;ngig ist er in jedem Fall, und das Unbedingte
              schlechthin ist er auch nicht. Wenn es ein solches &uuml;berhaupt
              nicht gibt, dann regieren und herrschen nur relative Gr&ouml;&szlig;en,
              Abh&auml;ngigkeiten und Verflechtungen dieser Welt. Das Unbedingte
              allein kann dem Menschen eine gewisse, aber entscheidende &Uuml;berlegenheit
              und damit Freiheit garantieren. Nur das in sich vollkommen Unbeschr&auml;nkte
              kann eine relative, endliche Unbeschr&auml;nktheit gew&auml;hren
              und begr&uuml;nden. Mit dem Bezug zu diesem Unbedingten, oder anders
              gesagt: mit der Pr&auml;senz dieses Unbedingten im eigenen Inneren,
              steht und f&auml;llt die Freiheit des Menschen.&#8220;<br>
              Josef Schmidt, Philosophische Theologie, Stuttgart 2003, S. 165<br>
              <br>
              Eckhard Bieger</font>            </p>
            <P>&copy;<a href="http://www.kath.de"> <font face="Arial, Helvetica, sans-serif">www.kath.de</font></a></P>
            </TD>
          <TD background=boxright.gif><IMG height=8 alt="" 
            src="boxright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=8 alt="" src="boxbottomleft.gif" 
            width=8></TD>
          <TD background=boxbottom.gif><IMG height=8 alt="" 
            src="boxbottom.gif" width=8></TD>
          <TD><IMG height=8 alt="" src="boxbottomright.gif" 
            width=8></TD></TR></TBODY></TABLE></TD><td style='vertical-align:top;'>
 <script type="text/javascript"><!--
        google_ad_client = "pub-9537752113242914";
        google_ad_width = 336;
        google_ad_height = 280;
        google_ad_format = "336x280_as";
        google_ad_type = "text_image";
        //2006-10-23: Lexikon Symbole
        google_ad_channel = "8495253357";
        //--></script>
        <script type="text/javascript"
          src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
        </script>
</td>
</TR>
  <TR>
    <TD vAlign=top align=left>&nbsp; </TD>
  </TR></TBODY></TABLE></BODY></HTML>
