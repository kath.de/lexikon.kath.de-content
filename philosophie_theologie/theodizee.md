---
title: Theodizee
author: 
tags: 
created_at: 
images: []
---


# **Die Rechtfertigung Gottes angesichts des Übels in der Welt*
***Wie kann Gott angesichts des Bösen und des Übels gerechtfertigt werden. Der Begriff wurde von Leibniz erstmals in seinem Buch von 1710 über die Theodizee, die Gutheit Gottes, die Freiheit des Menschen und die Ursache des Bösen. Theodizee leitet sich vom Griechischen Theos (Gott) und Dikae (Gerechtigkeit) her. 

# **Der Widerspruch zwischen der Allmacht Gottes und seiner Gutheit****
 Das Problem entsteht nicht nur denkerisch, sondern für den Menschen, der an Gott glaubt und von einem großen Leid oder sogar von einem Verbrechen heimgesucht wird. „Wie kann Gott das zulassen?“ ist die Frage. Denkerisch ergibt sich das Problem, wenn man von Gott sagt, daß er nur das Gute will. Wenn Gott allmächtig ist, müßte er das Böse, das Üble, das einem Menschen widerfährt, verhindern. Entweder ist Gott gut und nicht allmächtig. Dann gäbe es einen Mächtigeren als Gott, also das Böse: Das war die Befürchtung der Germanen, die von einem Kampf zwischen dem Guten und dem Bösen ausgingen und damit rechneten, daß das Böse gewinnt. Da Gott nur als allmächtig gedacht werden kann, sonst ist er nicht gut, muß er offensichtlich das Böse einplanen. Wie kann aber Gott dann der Gute sein. Der Widerspruch ist philosophisch nicht zu lösen. Der französische Schriftsteller Stendal folgert: „Die einzige Entschuldigung Gottes (angesichts des Übels in der Welt) ist, daß er nicht existiert.“***

 Allerdings kann aus der Tatsache des Übels nur dann die Frage der Existenz Gottes abgeleitet werden, wenn man eine höchste Macht annimmt, die zugleich in sich gut ist. Ohne die Idee eines guten Gottes entsteht die Frage nachdem Übel erst gar nicht. Dann ist die Welt eben so, wie sie ist. Denn ohne das Gute könnte man das Böse gar nicht erkennen. Böses gibt es nur, wenn es auch Gutes gibt. Das Übel setzt das Gute voraus. Dann muß es auch eine Macht geben, die das Gute will. Die Erfahrungen des 20. Jahrhunderts mit den Diktaturen zeigt dann, daß atheistische Systeme, die ja eine bessere Welt herbeiführen wollen, immer mehr in das Üble geraten. Ohne Gott scheint die Würde des Menschen nicht geschützt – wenn er sich nämlich gegen die Machthaber wendet. So wurden Menschen, die dem Rassenideal der Nationalsozialisten sehr wohl entsprachen und damit lebenswert waren, hingerichtet, wenn sie das Regime ablehnten. Nicht wenige der Widerstandskämpfer waren groß gewachsen und blond. Das hat sie aber nicht davor bewahrt, von den Nationalsozialisten getötet zu werden. 

# **Versuche, das Übel zu erklären****
 Bereits die griechische Morallehre der Stoa hat versucht, Gott gegenüber der Tatsache des Übels zu rechtfertigen. So wurde der erzieherische Wert des Leidens für den herausgestellt, der ohne eigene Schuld leidet. Für den, der Böses tut, ist das Leiden Folge seiner Fehler und Vergehen. Leibniz führt weitere Erklärungen an. Zum einen geht er davon aus, daß Gott nur die beste aller Welten schaffen konnte und daß daher das Böse gar nicht vorherrschend werden kann. Zudem würde ein Übel ein Gut bewirken, aus der Erfahrung des Krieges erwächst der Wille zum Frieden. Zudem müsse Gott das Übel zulassen, wenn er die Freiheit des Menschen wirklich wolle.***

 Diese Versuche, das Übel als Teil der an sich guten Schöpfung zu erklären, befriedigen nicht. Vor allem kann das Böse, das den Unschuldigen trifft, nicht damit erklärt werden, daß es ihm zur Erziehung gereicht. Das hat im Übrigen das Christentum auch nie getan. Dann hätte es den Kreuzestod Jesu als Erziehungsmaßnahme Gottes für seinen Messias deuten müssen. Vielmehr hat es das dem Menschen angetane Leid als Anteilnahme an dem Schicksal Jesu gedeutet, der unschuldig verurteilt wurde. 

# **Das Buch Hiob****
 Im Alten Testament ist ein einzelnes Buch der Frage gewidmet, wie der Mensch sein Leiden verstehen kann. Hiob heißt die Figur, die viele Schriftsteller inspiriert hat. Gott erlaubt dem Satan, Hiob zu versuchen, indem er ihm alles nimmt, was sein Leben ausmacht. Seine Kinder kommen um und er verliert seinen ganzen Besitz. Trotz des Leids, das ihm zugefügt wurde, zweifelt Hiob nicht an Gott. Die Freunde Hiobs versuchen ihm zu erklären, daß er sich verfehlt haben muß und daher sein Leiden als Strafe zu verstehen sind. Diese Erklärung wird im Hiobbuch als nicht tragfähig erwiesen. Eine Antwort erhält Hiob allerdings nicht. Der Leser des Hiobbuches lernt, daß der Mensch seine Klage vor Gott bringen kann, ohne daß damit Gott beleidigt würde.  

# **Der Leidensweg Jesu****
 Die Antwort, die Hiob nicht erhalten hat, gibt der Leidensweg Jesu. Gott selbst solidarisiert sich mit den vielen unschuldig Verurteilten und Verfolgten. Der Sohn Gottes erleidet das Böse, ohne daß Gott rächend eingreift. So überwindet er das Böse von innen her. Gott will das Böse nicht einfach vernichten, indem er den Bösen umbringt. Jesus nimmt das Böse ohne Widerstand hin. (***[Sündenbock](suendenbock.php)) Die Überwindung des Bösen liegt nicht in der Vernichtung der Gegner Jesu, sondern daß der Hingerichtete in ein neues Leben auferweckt wird. ***[Auferstehung](auferstehung_jesu.php) heißt die Neuschöpfung einer Welt, in der Übel und Tod nicht mehr herrschen, sondern Gott allein. ***

 Auch wenn Gott die Welt nicht in Krieg und Unrecht versinken läßt, sondern die Geschichte zu einem guten Ende führt, die Tatsache des Bösen mit all seinen Übeln bleibt dem Menschen ein Rätsel, das immer wieder dazu herausfordert, das Böse in Krimis, Thrillern zu zeigen, um es anfänglich erklären, aber nicht letztlich ergründen zu können. 

# ****
 Zitate** 

# **„Wenn es Gott gibt, woher kommt das Böse? Doch woher kommt das Gute, wenn es ihn nicht gibt.“**
 Boethius (cons. I 4p, 100f)***

# *
„ Es ist der Irrtum derer auszuschließen, die aus den Übeln
der Welt folgern, daß Gott nicht ist. … Sie fragen: Wenn Gott ist,
woher dann das Übel? Aber man muß sagen: Wenn es das Übel gibt,
dann gibt es Gott. Denn das Übel wäre nicht, wenn die Ordnung des Guten
nicht bestünde, dessen Beraubung das Übel ist. Diese Ordnung wäre
aber nicht, wenn Gott nicht wäre.“***

 Thomas von Aquin, Summa contra Gentiles III, 71 

# **„Es ist nur eine Welt möglich, eine durchaus gute. Alles, was in der Welt sich ereignet, dient zur Verbesserung und Bildung des Menschen und vermittels dieser zur Herbeiführung ihres irdischen Zieles. Dieser höhere Weltplan ist es, was wir Natur nennen, wenn wir sagen: Die Natur führet den Menschen durch Mangel zum Fleiße, durch die Drangsale ihrer unaufhörlichen Kriege zum endlichen ewigen Frieden. Dein Wille, Unendlicher, deine Vorsehung allen ist diese höhere Natur.“**
 Johann Gottlieb Fichte, Die Bestimmung des Menschen, Berlin 1800, S. 147  

# **„Wenn kein Gott ist, dann gibt es – letztlich – kein Gutes. Nicht in der Zukunft für die Zu-kurz-Gekommenen, Erniedrigten, Beleidigten; erst recht nicht für die Henker – oder auch tatenloser Genießer. Von dorther aber auch nicht ernstlich im Heute. Glück wäre dann nur auf Grund von Wegsehen, Vergessen, denkbar. **
 Jörg Splett, 1996, Denken vor Gott, Frankfurt 1996, S. 309  

# **„Leiden, Schuld und Tränen schreien nach realer Überwindung des Übels. Daher ermöglicht erst die Einheit von Schöpfung und Erlösung im Horizont der Eschatologie eine haltbare Antwort auf die Frage der Theodizee, die Frage nach der Gerechtigkeit Gottes in seinen Werken. Genauer gesagt, es ist allein Gott selbst, der eine wirklich befreiende Antwort auf diese Frage zu geben vermag, und er gibt sie durch die Geschichte seines Handelns in der Welt und insbesondere durch dessen Vollendung mit der Aufrichtung seines Reiches in der Schöpfung. Solange die Welt isoliert im Blick auf ihre unvollendete und unerlöste Gegenwart einerseits, unter dem Gesichtspunkt ihres anfänglichen Hervorgangs aus den Händen des Schöpfers andererseits, betrachtet wird, bleibt die Tatsache des Bösen und des Übels in der Schöpfung ein auswegloses Rätsel.“**
 Wolfhart Pannenberg, Schöpfungsglaube und Theodizee, in „Systematische Theologe“ Bd. 2, Göttingen 1991, S. 193 

 

***Eckhard Bieger S.J. 

©***[ ***www.kath.de](http://www.kath.de) 
