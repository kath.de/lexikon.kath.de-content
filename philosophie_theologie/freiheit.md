---
title: Freiheit
author: 
tags: 
created_at: 
images: []
---


# **Gegenüber unserer Freiheit sind wir nicht frei*
# **Die Freiheit zeichnet den Menschen aus. Sein Leben ist nicht wie bei Tieren von Instinkten geleitet, vielmehr steuert der Mensch sein Leben selbst durch seine ***[Entscheidungen](freiheit_entscheidung.php).  Die Freiheit ist so mit der Person des einzelnen verbunden, daß er sich nicht von seiner Freiheit einfach distanzieren kann, es sei denn, er wird krank oder ist durch eine Hirnverletzung so eingeschränkt, daß er nicht mehr über sich verfügen kann. Die Freiheit ist auch nicht dadurch abzustreifen, daß der einzelne andere über sein Leben entscheiden läßt. Tut er das, ist seine Freiheit im Spiel, denn es ist seine Entscheidung, nicht selbst, sondern andere entscheiden zu lassen. Auch derjenige, der nicht entscheidet, tut das nicht ohne seine Freiheit, er oder sie entscheiden nämlich, daß das eigene Lebensschiff auf dem Kurs weiterfahren soll, auf dem es bereits unterwegs ist. Die eigene Freiheit verlangt sozusagen von jedem, daß er selbst den Kurs seines Lebens bestimmt. Mit der Freiheit verfügt der einzelne über sein Leben.**
 Da keiner seiner Freiheit entkommt, ist niemand gegenüber seiner Freiheit frei. Die Freiheit hat etwas unerbittliches, sie fordert sich selbst ein. Daß jemand vor dieser Forderung der Freiheit zurückschreckt, ist verständlich, denn mit der eigenen Freiheit übernimmt der einzelne die Verantwortung für die Entscheidungen, die er bzw. sie getroffen hat. ***

 Freiheit ist abhängig von den Möglichkeiten, die sich bieten. In einer Gesellschaft, in der viel vorgegeben ist, ist die Freiheit nicht so gefordert wie wenn eine Gesellschaft dem einzelnen viele Entscheidungen offen läßt – freie Berufswahl, freie Partnerwahl, viele Ausbildungsgänge, Religionsfreiheit können die Freiheit sogar als Last erscheinen lassen, denn dem einzelnen werden kaum Entscheidungen abgenommen. Je mehr der einzelne entscheiden kann, desto mehr sind er oder sie für das Gelingen  ihres Lebens verantwortlich.  

# **Eckhard Bieger S.J.**
******[Buchtipp](http://www.kathshop.de/wwwkathde/product_info.php?cPath=&products_id=195&anbieter=17&page=&) 

©***[ ***www.kath.de](http://www.kath.de) 
