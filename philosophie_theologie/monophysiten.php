<HTML><HEAD><TITLE>Monophysiten</TITLE>
<META http-equiv=Content-Type content="text/html; charset=iso-8859-1"><LINK 
title=fonts href="kaltefleiter.css" type=text/css 
rel=stylesheet>
<META content="MSHTML 5.50.4134.600" name=GENERATOR></HEAD>
<BODY bgColor=#ffffff leftMargin=6 topMargin=6 marginheight="6" marginwidth="6">
<TABLE cellSpacing=0 cellPadding=6 width="100%" border=0>
  <TBODY>
  <TR>
    <TD vAlign=top align=left width=100> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b>Philosophie&amp;Theologie</b></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td> <?php include("logo.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10"> <?php include("az.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </TD>
    <TD vAlign=top rowSpan=2>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
        <TBODY>
        <TR vAlign=top align=left>
          <TD width=8><IMG height=8 alt="" 
            src="boxtopleftcorner.gif" width=8></TD>
          <TD background=boxtop.gif><IMG height=8 alt="" 
            src="boxtop.gif" width=8></TD>
          <TD width=8><IMG height=8 alt="" 
            src="boxtoprightcorner.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxtopleft.gif><IMG height=8 alt="" 
            src="boxtopleft.gif" width=8></TD>
          <TD bgColor=#e2e2e2> 
            <H1><font face="Arial, Helvetica, sans-serif">Monophysiten
                Monophysitismus</font></H1>
          </TD>
          <TD background=boxtopright.gif><IMG height=8 
            alt="" src="boxtopright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=13 alt="" src="boxdividerleft.gif" 
            width=8></TD>
          <TD background=boxdivider.gif><IMG height=13 
            alt="" src="boxdivider.gif" width=8></TD>
          <TD><IMG height=13 alt="" 
            src="boxdividerright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxleft.gif><IMG height=8 alt="" 
            src="boxleft.gif" width=8></TD>
          <TD class=L12>
            <P><STRONG><font face="Arial, Helvetica, sans-serif">Die theologische
                  Konzeption der Kopten, Armenier und anderer Kirchen</font></STRONG></P>
            <P><strong><font face="Arial, Helvetica, sans-serif">Die Einheit
                  des menschgewordenen Sohnes Gottes</font></strong><br>
                  <font face="Arial, Helvetica, sans-serif">Wenn Gott Mensch
                  wird und nicht nur mit einem Scheinleib den Eindruck erweckt,
                  Menschen w&uuml;rden einem von Ihresgleichen
                  begegnen, dann stellt sich die Frage, wie diese Tatsache angemessen
                  zum Ausdruck gebracht werden kann. F&uuml;r den gr&ouml;&szlig;eren
                  Teil der Christen in Ost und West ist die Frage mit dem Konzil
                  von Chalzedon gekl&auml;rt. Aber nicht f&uuml;r alle. Vor allem
                  die koptische Kirche in &Auml;gypten h&auml;lt an der Theologie
                  des Cyrill von Alexandrien fest, der die Einheit von Gottheit
                  und Menschheit betonte. Cyrill entwickelte f&uuml;r das Neue,
                  das in Christus geschehen ist, den Begriff &#8222;Hypostatische
                  Union&#8220;, Mit Hypostase ist das gemeint, das ein Individuum
                  tr&auml;gt, man k&ouml;nnte es in etwa it Substanz &uuml;bersetzen. Um die
                  Einheit von Menschheit und Gottheit in Jesus zu betonen, sprach
                  er von einer neuen Wirklichkeit
                  und nannte diese eine Natur, nur &quot;eine&quot; Natur, weil die Einheit
                  nicht mehr aufgel&ouml;st und in Jesus Christus nicht Zwei,
                  sondern nur einer verehrt werden kann. Mia Physis &#8211; eine
                  Natur.<br>
                  Da aber die Begrifflichkeit <a href="christologische_streitigkeiten.php">&#8222;Natur&#8220;</a> 
                  in der theologischen Tradition  das
                  Menschliche und das G&ouml;ttliche in Christus bezeichnet,
                  also gerade f&uuml;r die Gegebenheit, in der Jesus als &#8222;Zwei&#8220;,
                  eben in zwei Naturen beschrieben werden kann, ist es verwirrend,
                  den Begriff Natur zugleich f&uuml;r das zu verwenden, was wir
                  im Unterschied zu den Naturen als <a href="person.php">Person</a>  bezeichnen.
                  Die g&ouml;ttliche udn die menschliche Natur brauchen ein Einheitsprinzip.
                  Die Auseinandersetzungen begannen im 5. Jahrhundert und f&uuml;hrten
                  zu einem ersten Ergebnis beim Konzil von Ephesus 433. Hier
                  setze sich die Richtung durch, die der Patriarch von Alexandrien,
                  Cyrill vertrat. Um die Einheit in Christus zu betonen, nennt
                  das Konzil von Ephesus Maria
                  nicht nur Christusgeb&auml;rerin, sondern Gottesgeb&auml;rerin
                  (griechisch Theotokos) genannt wird. Damit sollte deutlich
                  werden, da&szlig; der
                  Mensch Jesus nicht zuerst f&uuml;r sich existiert hatte und
                  ihm sp&auml;ter der Sohn Gottes einwohnte. Im Augenblick der
                  Empf&auml;ngnis nimmt das Wort, der ewige Sohn Gottes, Fleisch
                  an und damit konstituiert sich erst die menschliche Person
                  Jesu. Aber mu&szlig; man Gottheit und Menschheit in Jesus nicht
                  doch deutlicher auseinanderhalten? Auch wenn man mit Cyrill
                  darin &uuml;bereinstimmt, da&szlig; Jesus Christus nicht zwei
                  Personen sind, es nicht zwei S&ouml;hne gab, den von Ewigkeit
                  gezeugten Sohn Gottes und den vom Heiligen Geist gezeugten
                  Menschen Jesus, vermischen sich die Naturen nicht. <br>
                  Man kann die Einheit in Christus auch anders denken. Das war
                  der Ansatz der Schule von Antiochien in Syrien. Sie betont
                  die Verschiedenheit von Gottheit und Menschheit. (<a href="nestorianer.php">s.
                  Nestorianer</a>)                  <br>
                  Da&szlig; Nestorius sich nicht mit dem Titel &#8222;Gottesgeb&auml;rerin&#8220; f&uuml;r
                  Maria einverstanden erkl&auml;ren konnte, liegt auch daran,
                  da&szlig; er ein Mi&szlig;verst&auml;ndnis bef&uuml;rchtete,
                  n&auml;mlich da&szlig; der ewige Logos selbst aus Maria geboren
                  wurde, also f&uuml;r den Logos ein menschlicher Geburtsvorgang
                  behauptet werden k&ouml;nnte. In seinem Brief an Cyrill v.
                  Alexandrien aus dem Jahr 430 schreibt er:<br>
&#8222;&Uuml;berall, wo die heiligen Schriften vom Heilswirken des Herrn
                  berichten, schreiben sie Geburt und Leiden nicht der Gottheit,
                  sondern der Menschheit Christi zu. Will man sich daher m&ouml;glichst
                  exakt ausdr&uuml;cken, dann mu&szlig; man die heilige Jungfrau &#8222;Christusgeb&auml;rerin&#8220; (Christotokos)
                  und nicht &#8222;Gottesgeb&auml;rerin&#8220; (Theotokos) nenne.
                  H&ouml;ren wir das Evangelium, das laut verk&uuml;ndet: &#8222;Stammbaum
                  Jesu Christi, des Sohnes Davids, des Sohnes Abrahams (Mt 1,1).
                  Nat&uuml;rlich war das g&ouml;ttliche Wort nicht Sohn Davids.
                  ..... Wer k&ouml;nnte auf den Gedanken kommen, da&szlig; die
                  Gottheit des eingeborenen Sohnes ein Gesch&ouml;pf des Geistes
                  w&auml;re? ..... &#8222;Und dies ist mein Leib (nicht meine
                  Gottheit), der f&uuml;r euch hingegeben wird.&#8220; Und noch
                  Tausende von anderen Aussagen bezeugen dem Menschengeschlecht,
                  da&szlig; man nicht annehmen kann, die Gottheit des Sohnes
                  sei erst vor einiger Zeit geboren worden oder sie sei k&ouml;rperlicher
                  Leiden f&auml;hig. Vielmehr war das die mit der Gottheit vereinigte
                  Menschennatur.&#8220;</font></P>
            <P><font face="Arial, Helvetica, sans-serif"><strong>Die Unterscheidung
                von Natur und Person</strong><br>
             Wenn
                wir heute vom <a href="person.php">Personbegriff</a> her
                die Einheit von Gottheit und Menschheit ganz anders denken k&ouml;nnen,
                wird deutlich, da&szlig; mit den Begriffen, die die griechische
                Philosophie damals bereitstellte, ein Ausweg aus dem Streit der
                theologischen
                Schulen nicht zu finden war, denn die Griechen kannten nur Seele
                udn Leib udn kein Prinzip, das die geistige und die k&ouml;rperliche
                Natur des Menschen eint.<br>
            Ein wichtiger Schritt
            zu einer theologischen Weiterentwicklung wurde durch das <a href="christologische_streitigkeiten.php">Konzil
            von Chalcedon</a> erreicht (456).  Es vermittelt zwischen
            den beiden Positionen, die einmal die Einheit und zum anderen die
            Getrenntheit der Naturen
              betonen durch die Formel:</font></P>
            <p><font face="Arial, Helvetica, sans-serif"> Unvermischt - bezogen auf Gottheit und Menschheit und<br>
  Ungetrennt - bezogen auf den einen, die Person Jesu.<br>
            </font><font face="Arial, Helvetica, sans-serif">Das Konzil unterscheidet
            dann auch zwischen Natur (Physis) und Person (Prosopon - griechisch
            Antlitz).<br>
              Einige Kirchen haben sich der Konzilsentscheidung bis heute nicht
              angeschlossen, so die koptische und mit ihr die &auml;thiopische,
              die syrische und die armenische Kirche. Das war nicht nur in den
              unterschiedlichen Akzentsetzungen der theologischen Schulen von
              Alexandrien und Antiochien begr&uuml;ndet, sondern auch in regionalen
              Differenzen und einer starken Tendenz, sich von der damaligen Hauptstadt,
              Byzanz nicht dominieren zu lassen. So hatte der Patriarch von
              Byzanz beim 1. Konzil von Konstantinopel 381 durchgesetzt, da&szlig; in
              der Rangfolge Alexandrien vom 2. Platz verdr&auml;ngt wurde. Konstantinopel
              r&uuml;ckte auf den 2. Platz, Alexandrien mu&szlig;te mit dem 3.
              Platz vorlieb nehmen. Zwischen der koptischen und der chald&auml;ischen
              Kirche im Irak wird die Diskussion, die das 5., 6. und 7. Jahrhundert
              beherrschte, immer noch fortgef&uuml;hrt.
              Die orthodoxen Kirchen Griechenlands, Ru&szlig;lands und die anderen
              europ&auml;ischen Kirchen stehen auf dem Boden des Konzils von
              Chalcedon.<br>
              Dieser komplizierte Streit um Begriffe verdeckt, da&szlig; die
              getrennten Kirchen in dem Verst&auml;ndnis der Person Jesu Christi
              nicht entscheidend auseinander liegen. Denn diese Kirchen behaupten
              von Jesus Christus &#8222;nicht eine einfache Natur, sondern eher
              eine einzige zusammengesetzte Natur, in der Gottheit und Menschheit
              ungetrennt und unvermischt vereinigt sind.&#8220; (Wiener christologische
              Erkl&auml;rung vom 29.8.1976) Da aber die Theologie des Cyrill
              von Alexandrien f&uuml;r die koptische Kirche identit&auml;tsbildend
              ist und die syrische Kirche sich sp&auml;ter der Sicht Cyrills
              anschlo&szlig;, bleiben die Gegens&auml;tze bis heute bestehen. </font></p>
            <p><font face="Arial, Helvetica, sans-serif">Zitate <br>
              Die Idee von der neuen Natur, die durch die Menschwerdung des Sohnes
              entstanden sein soll, findet sich bei Appolinaris von Laodizea
              der zwischen 310 und 390 lebte, also  vor Cyrill von Alexandrien:<br>
&#8222;
              Es wird aber in ihm (Jesus Christus) das Geschaffene in Einheit
              mit dem Ungeschaffenen bekannt, das Ungeschaffene in Vermischung
              mit dem Geschaffenen, wobei eine einzige Natur aus den beiden Teilen
              konstituiert wird, da n&auml;mlich der Logos mit seiner g&ouml;ttlichen
              Vollkommenheit eine Teilkraft zu dem Ganzen beitr&auml;gt. &Auml;hnlich
              wie beim Menschen aus zwei unvollkommenen Teilen die eine Natur
              entsteht.&#8220; Fragmente</font></p>
            <p><font face="Arial, Helvetica, sans-serif"> Er
                hat sich dessen ent&auml;u&szlig;ert, was er war, und hat angenommen,
                was er nicht war. Er wurde nicht zu Zweien, sondern hat es auf
                sich genommen, eins aus Zweien zu werden. Gott ist n&auml;mlich
                beides, das Empfangende und das Empfagnene. Zwei Naturen flie&szlig;en
                in eins zusammen, es gibt nicht zwei S&ouml;hne.<br>
                Gregor von Nanzianz , Oratio37,2 um 380</font></p>            <p><font face="Arial, Helvetica, sans-serif"><br>
  Die Einheit der Naturen ist nicht getrennt ... Das Getrenntsein
                besteht n&auml;mlich nicht in der Aufhebung der Einheit, sondern
                in der Vorstellung des Fleisches und der Gottheit. .... Christus
                ist unteilbar in seinem Christsein, er ist aber zweifach in dem
                Gott- und Menschsein. Er ist einfach in der Sohnschaft.... In
                dem Antlitz (dem Prosopon) des Sohnes ist er ein einziger, aber
                ... geschieden in den Naturen der Menschheit und der Gottheit.
                Denn wir kennen nicht zwei Christus&#8216; noch zwei S&ouml;hne
                oder Eingeborene oder Herren, nicht den einen und den anderen
                Sohn, .... sondern ein und denselben, der erblickt worden ist
                in seiner erschaffenen und unerschaffenen Natur.<br>
                Predigt des Nestorius &uuml;ber Matth&auml;us 22,2,, aus dem
                Jahr 429</font></p>
            <p><font face="Arial, Helvetica, sans-serif">So bekennen wir einen
                Christus und Herrn. Dabei beten wir nicht etwa einen Menschen
                mit dem Wort (gemeint ist der ewige Sohn Gottes)
              zusammen an, damit nicht durch die W&ouml;rter &#8222;mit&#8220; und &#8222;zusammen&#8220; die
              Vorstellung einer Scheidung eingef&uuml;hrt wird. Wir beten vielmehr
              einen und denselben Christus an. Denn sein Leib ist dem g&ouml;ttlichen
              Wort nicht fremd. Mit diesem Leibe thront er ja auch zur Rechten
              des Vaters, weil wiederum nicht zwei S&ouml;hne an der Seite des
              Vaters sitzen, sondern ein Sohn entsprechend der Einigung mit dem
              eigenen Fleisch. Wollten wir aber die Einigung der Person nach
              als unverst&auml;ndlich oder nicht angemessen ablehnen, so w&auml;ren
              wir gezwungen, zwei S&ouml;hne anzunehmen. Dann k&auml;men wir
              n&auml;mlich nicht umhin, zwischen einem Menschen einerseits, der
              durch den Namen &#8222;Sohn&#8220; allenfalls geehrt w&auml;re,
              und andererseits dem Wort Gottes, dem von Natur aus Name und Wirklichkeit
              der Sohnschaft zueigen ist. Es darf demnach der eine Herr Jesus
              Christus nicht in zwei S&ouml;hne gespalten werden. &#8230;. Die
              Schrift (d.h. die Bibel) sagt ja nicht, das Wort habe sich mit
              der Person eines Menschen vereinigt, sondern es sei selber Fleisch
              geworden. &#8230;&#8230;
                <br>
                Die Worte des Heilandes aber, die sich im Evangelium finden,
                verteilen wir nicht auf zwei Hypostasen (Subjekte) oder Personen.
                Der eine und einzige Christus ist nicht zwiesp&auml;ltig,
                obwohl er aus zwei und zwar zwei verschiedenen Wirklichkeiten
                besteht. Diese sind aber zu einer unteilbaren Einheit verbunden,
                wie etwa auch der Mensch aus Leib und Seele besteht und doch
                nicht zweifach, sondern aus beiden Teilen ist. Wir lassen daher
                gem&auml;&szlig; dem rechten Glauben die menschlichen wie auch
                g&ouml;ttlichen Aussagen von einem gesprochen sein.<br>
                Cyrill von Alexandrien, aus dem Brief an Nestorius, 430</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Anathemata des Konzils von Ephesus<br>
  Nr. 3: Wer nach erfolgter Vereinigung in dem einen Christus die
                Hypostasen (Substanzen) auseinanderrei&szlig;t, indem er sie
                nur durch eine &auml;u&szlig;ere Verbindung der W&uuml;rde nach,
                durch ihre Hoheit und Macht, verbunden sein l&auml;&szlig;t und
                nicht vielmehr durch eine Vereinigung im Sinne einer physischen
                Einswerdung, der sei ausgeschlossen.</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Der mittelalterliche
                Theologe Thomas v. Aquin hat die Frage noch einmal behandelt.
                Aus dem Text wird deutlich, da&szlig; die Person
              als das einigende Prinzip von Leib und Seele gedacht wird, die
                Person also nicht mit der Seele identisch ist:<br>
              Weil bei uns Leib und Seele zusammen eine Person bilden, glaubten
              manche, die Vereinigung von Leib und Seele in Christus leugnen
              zu m&uuml;ssen. Sie f&uuml;rchteten n&auml;mlich, sonst die Annahme
              einer zweiten Person nicht umgehen zu k&ouml;nnen, weil sie sahen,
              da&szlig; bei den &uuml;brigen Menschen aus der Verbindung von
              Leib und Seele die Person erstellt wird. Nun verbinden sich aber
              gerade bei allen anderen Menschen Leib und Seele, um f&uuml;r sich
              zu bestehen, im Gegensatz zu Christus, wo sie sich vereinigen,
              um von einer h&ouml;heren Person getragen zu werden. Deshalb ersteht
              in Christus aus Leib und Seele kein neuer Tr&auml;ger seiner menschlichen
              Natur, sondern beide gehen zusammen in eine Person ein, die schon
              vorher bestand. Daraus, da&szlig; Leib und Seele nicht wie bei
              uns eine eigene Person ergeben, folgt jedoch nicht, da&szlig; ihre
              Vereinigung in Christus weniger kraftvoll ist. Eine Vereinigung
              mit einer vornehmeren, h&ouml;heren Person mindert n&auml;mlich
              in keiner Weise Kraft und W&uuml;rde, sie erh&ouml;ht sie. <br>
              Summa theologica II q.2 a 5, Nr. 54-55</font></p>
            <p><font size="2" face="Arial, Helvetica, sans-serif">Eckhard Bieger</font></p>
            <P>&nbsp;</P>
            <P>&copy;<a href="http://www.kath.de"> <font face="Arial, Helvetica, sans-serif">www.kath.de</font></a></P>
            </TD>
          <TD background=boxright.gif><IMG height=8 alt="" 
            src="boxright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=8 alt="" src="boxbottomleft.gif" 
            width=8></TD>
          <TD background=boxbottom.gif><IMG height=8 alt="" 
            src="boxbottom.gif" width=8></TD>
          <TD><IMG height=8 alt="" src="boxbottomright.gif" 
            width=8></TD></TR></TBODY></TABLE></TD><td style='vertical-align:top;'>
 <script type="text/javascript"><!--
        google_ad_client = "pub-9537752113242914";
        google_ad_width = 336;
        google_ad_height = 280;
        google_ad_format = "336x280_as";
        google_ad_type = "text_image";
        //2006-10-23: Lexikon Symbole
        google_ad_channel = "8495253357";
        //--></script>
        <script type="text/javascript"
          src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
        </script>
</td>
</TR>
  <TR>
    <TD vAlign=top align=left>&nbsp; </TD>
  </TR></TBODY></TABLE></BODY></HTML>
