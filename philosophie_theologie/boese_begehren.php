<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <title>Das B&ouml;se - Begehren die Wurzel des B&ouml;sen</title>


  <meta name="title" content="Philosophie&amp;Theologie">

  <meta name="author" content="Redaktion kath.de">

  <meta name="publisher" content="kath.de">

  <meta name="copyright" content="kath.de">

  <meta name="description" content="">

  <meta name="abstract" content="Lexikon &uuml;ber Philosophie&amp;Theologie">

  <meta http-equiv="content-language" content="de">

  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

  <meta name="date" content="2006-00-01">

  <meta name="robots" content="index,follow">

  <meta name="revisit-after" content="10 days">

  <meta name="revisit" content="after 10 days">

  <meta name="DC.Title" content="Philosophie&amp;Theologie">

  <meta name="DC.Creator" content="Redaktion kath.de">

  <meta name="DC.Contributor" content="J&uuml;rgen Pelzer">

  <meta name="DC.Rights" content="kath.de">

  <meta name="DC.Publisher" content="kath.de">

  <meta name="DC.Date" content="2006-00-01">

  <meta name="DC.Description" content="Lexikon &uuml;ber Philosophie&amp;Theologie">

  <meta name="DC.Language" content="de">

  <meta name="DC.Type" content="Text">

  <meta name="DC.Format" content="text/html">

  <meta name="DC.Identifier" content="http://www.kath.de/lexikon/philosophie_theologie/">

  <meta name="keywords" content="Freiheit, Stimme Gottes, Gewissen, Tiefenpsychologie, Ueber-Ich, Newman, Schelling, Johann Gottlieb Fichte" lang="de">

</head>
<body leftmargin="6" topmargin="6" style="background-color: rgb(255, 255, 255);" marginheight="6" marginwidth="6">

<table border="0" cellpadding="6" cellspacing="0" width="100%">

  <tbody>

    <tr>

      <td align="left" valign="top" width="100">
      <table border="0" cellpadding="0" cellspacing="0" width="216">

        <tbody>

          <tr align="left" valign="top">

            <td width="8"><img src="boxtopleftcorner.gif" alt="" height="8" width="8"></td>

            <td background="boxtop.gif" width="200"><img src="boxtop.gif" alt="" height="8" width="8"></td>

            <td width="8"><img src="boxtoprightcorner.gif" alt="" height="8" width="8"></td>

          </tr>

          <tr align="left" valign="top">

            <td background="boxtopleft.gif"><img src="boxtopleft.gif" alt="" height="8" width="8"></td>

            <td bgcolor="#e2e2e2"><b>Philosophie&amp;Theologie</b></td>

            <td background="boxtopright.gif"><img src="boxtopright.gif" alt="" height="8" width="8"></td>

          </tr>

          <tr align="left" valign="top">

            <td><img src="boxdividerleft.gif" alt="" height="13" width="8"></td>

            <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" height="13" width="8"></td>

            <td><img src="boxdividerright.gif" alt="" height="13" width="8"></td>

          </tr>

          <tr align="left" valign="top">

            <td background="boxleft.gif"><img src="boxleft.gif" alt="" height="8" width="8"></td>

            <td><?php include("logo.html"); ?> </td>

            <td background="boxright.gif"><img src="boxright.gif" alt="" height="8" width="8"></td>

          </tr>

          <tr align="left" valign="top">

            <td><img src="boxbottomleft.gif" alt="" height="8" width="8"></td>

            <td background="boxbottom.gif"><img src="boxbottom.gif" alt="" height="8" width="8"></td>

            <td><img src="boxbottomright.gif" alt="" height="8" width="8"></td>

          </tr>

        </tbody>
      </table>

      <br>

      <table border="0" cellpadding="0" cellspacing="0" width="216">

        <tbody>

          <tr align="left" valign="top">

            <td width="8"><img src="boxtopleftcorner.gif" alt="" height="8" width="8"></td>

            <td background="boxtop.gif" width="200"><img src="boxtop.gif" alt="" height="8" width="8"></td>

            <td width="8"><img src="boxtoprightcorner.gif" alt="" height="8" width="8"></td>

          </tr>

          <tr align="left" valign="top">

            <td background="boxtopleft.gif"><img src="boxtopleft.gif" alt="" height="8" width="8"></td>

            <td bgcolor="#e2e2e2"><strong>Begriff
anklicken</strong></td>

            <td background="boxtopright.gif"><img src="boxtopright.gif" alt="" height="8" width="8"></td>

          </tr>

          <tr align="left" valign="top">

            <td><img src="boxdividerleft.gif" alt="" height="13" width="8"></td>

            <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" height="13" width="8"></td>

            <td><img src="boxdividerright.gif" alt="" height="13" width="8"></td>

          </tr>

          <tr align="left" valign="top">

            <td background="boxleft.gif"><img src="boxleft.gif" alt="" height="8" width="8"></td>

            <td class="V10"><?php include("az.html"); ?>
            </td>

            <td background="boxright.gif"><img src="boxright.gif" alt="" height="8" width="8"></td>

          </tr>

          <tr align="left" valign="top">

            <td><img src="boxbottomleft.gif" alt="" height="8" width="8"></td>

            <td background="boxbottom.gif"><img src="boxbottom.gif" alt="" height="8" width="8"></td>

            <td><img src="boxbottomright.gif" alt="" height="8" width="8"></td>

          </tr>

        </tbody>
      </table>

      </td>

      <td rowspan="2" valign="top">
      <table border="0" cellpadding="0" cellspacing="0" width="100%">

        <tbody>

          <tr align="left" valign="top">

            <td width="8"><img alt="" src="boxtopleftcorner.gif" height="8" width="8"></td>

            <td background="boxtop.gif"><img alt="" src="boxtop.gif" height="8" width="8"></td>

            <td width="8"><img alt="" src="boxtoprightcorner.gif" height="8" width="8"></td>

          </tr>

          <tr align="left" valign="top">

            <td background="boxtopleft.gif"><img alt="" src="boxtopleft.gif" height="8" width="8"></td>

            <td bgcolor="#e2e2e2">
            <h1><font face="Arial, Helvetica, sans-serif">Das
B&ouml;se - Begehren die Wurzel des B&ouml;sen<br>

            </font></h1>

            </td>

            <td background="boxtopright.gif"><img alt="" src="boxtopright.gif" height="8" width="8"></td>

          </tr>

          <tr align="left" valign="top">

            <td><img alt="" src="boxdividerleft.gif" height="13" width="8"></td>

            <td background="boxdivider.gif"><img alt="" src="boxdivider.gif" height="13" width="8"></td>

            <td><img alt="" src="boxdividerright.gif" height="13" width="8"></td>

          </tr>

          <tr align="left" valign="top">

            <td background="boxleft.gif"><img alt="" src="boxleft.gif" height="8" width="8"></td>

            <td style="font-family: Helvetica,Arial,sans-serif;" class="L12">
            <p class="MsoNormal"> <span style="font-weight: bold;">Ren&eacute; Girards Theorie
vom S&uuml;ndenbock</span><br>

Wenn es jede Woche neue Krimis gibt, muss das B&ouml;se immer
wieder nachwachsen. Durchschnittlich sind es 600 Morde pro Jahr in der
Bundesrepublik. Diese allein sind jedoch nicht allein Motiv, dass so
viele
Krimis im Fernsehen geschaut werden. Im Mord verdichtet sich nur, was
t&auml;glich
neu entsteht. Die Zuschauer m&uuml;ssen nicht nur mit den
Alltagsschwierigkeiten
fertig werden, sondern auch mit Unfreundlichkeiten, mit
Zur&uuml;cksetzung, &uuml;bler
Nachrede, mit Anfeindungen anderer und auch mit Mobbing.</p>

            <p class="MsoNormal">Eine
aussagef&auml;hige Antwort auf die Frage nach dem Mord hat
Ren&eacute;
Girard darin gefunden, dass Menschen sich miteinander vergleichen, sich
nachahmen, er nennt das Mimesis. Mimesis, ein griechisches Wort, wurde
zuerst
f&uuml;r ein Kunstwerk gebraucht, das, ob in der Bildhauerei oder
im Theater,
menschliches Verhalten &bdquo;nachahmt&ldquo;. Girard gebraucht
das Wort entsprechend dem
jetzigen Bedeutungsrahmens von &bdquo;Nachahmung&ldquo;. Warum
f&uuml;hren das &bdquo;Sich mit anderen
Vergleichen&ldquo; und die Nachahmung anderer zum B&ouml;sen? </p>

            <p class="MsoNormal"><span style="font-weight: bold;">Der Mensch braucht
Objekte, um zu handeln</span><br>

Anders als das Tier und auch anders als die Triebtheorie von
Sigmund Freud es beschreibt, folgt der Mensch nicht einfach seinem
Begehren, sondern
er sucht f&uuml;r sein Begehren erst einmal ein Objekt. Da er nicht
allein lebt,
sondern auf der Basis der Sprache in das soziale System wie auch in die
Handlungsoptionen anderer verflochten ist, orientiert er sein Begehren
nicht
direkt an seinen Trieben, sondern an dem, was die anderen wollen.
&bdquo;Ich will
etwas, nicht weil ich es mir ausgew&auml;hlt habe, sondern weil ein
anderer es will.&ldquo;
Man kann das an einem einfachen Experiment
&uuml;berpr&uuml;fen:&nbsp;</p>

            <p style="margin-left: 40px;" class="MsoNormal"><cite><em>Vier
Kindergartenkinder werden
in einen Raum gef&uuml;hrt. In jeder der vier Ecken des Raumes
liegt das gleiche
Spielzeug. Eigentlich ist zu erwarten,&nbsp;<br>

dass jedes Kind in eine der Ecken geht
und spielt. In den meisten F&auml;llen werden die Kinder sich
jedoch beobachten und
dann das Spielzeug haben wollen,<br>

das das erste Kind f&uuml;r sich ausgew&auml;hlt hat</em></cite>.<br>

            </p>

            <p class="MsoNormal">Die unbestimmte Offenheit
des Handlungsrahmens auf der einen
Seite und die Sozialit&auml;t des Menschen auf der anderen Seite
sind nach Girard
die Situation, aus der erst einmal ein st&auml;ndiges
Sich-Vergleichen folgt, das
dann leicht in Eifersucht, Neid, Streit, Entt&auml;uschung
umschl&auml;gt. In der Bibel
steht daf&uuml;r exemplarisch die Erz&auml;hlung vom
Brudermord.
            <a href="boese_brudermord.php">Das
B&ouml;se - der Brudermord</a></p>

            <p class="MsoNormal">Weil der Mensch das
begehrt, was der andere will, also
gerade nicht durch den Trieb gesteuert wird, sondern durch das
Auswahlverhalten
anderer, funktioniert z.B. Mode. &Auml;hnlich leben das harte
Training von Sportlern
oder die Lernanstrengungen von Studenten von der Mimesis. Denn durch
den
Vergleich mit anderen und der daraus folgenden Nachahmung gewinnt der
einzelne Energie.
Neben vielen positiven Auswirkungen der Mimesis gibt es auch viele
hemmende.
Die deutsche Einheit ist von dieser Gesetzlichkeit belastet. Die
Menschen in
den Neuen Bundesl&auml;ndern vergleichen ihre Situation
st&auml;ndig mit der der alten
Bundesl&auml;nder. In anderen, ehemaligen kommunistischen
L&auml;ndern war das nicht
gegeben, man konnte sich leichter, d.h. ohne durch Vergleichen
abgelenkt zu
werden, um das Notwendige k&uuml;mmern.</p>

            <p class="MsoNormal">Weil der einzelne
st&auml;ndig andere beobachtet und Entscheidungen
danach trifft, was andere anstreben, entsteht nicht nur Gerede
&uuml;ber andere. Es
verhaken sich die Beziehungen. Das geschieht schon allein dadurch, dass
&uuml;ber
einen anderen in dessen Abwesenheit &bdquo;hergezogen&ldquo;
wird. Unzufriedenheit wird so
gesch&uuml;rt, die Stimmung wird schlechter. Aus diesem Sumpf
heraus entstehen die
Untaten. Es f&auml;ngt nicht mit b&ouml;sen Absichten an,
sondern mit schlechter
Stimmung. Diese entsteht durch Neid, Eifersucht, durch sich
Zur&uuml;ckgesetzt
f&uuml;hlen, durch entt&auml;uschte Erwartungen. </p>

            <p class="MsoNormal"><span style="font-weight: bold;">Der Abbau der
schlechten Gef&uuml;hle</span><br>

Wenn die Beteiligten sich in ihren negativen Gef&uuml;hlen verhakt
haben, muss
irgendetwas geschehen, um die Stimmung zu &auml;ndern. Das gelingt
nun nicht
dadurch, dass man zusammen etwas Sch&ouml;nes macht, z.B. ein Fest
feiert. Dadurch
werden die negativen Gef&uuml;hle nicht weggeblasen, sie verdichten
sich eher in
Formen der Gewalt. Diese Gewalt muss sich allerdings kanalisieren. Denn
die
menschliche Sippe w&auml;re in ihrem Bestand gef&auml;hrdet,
wenn die Gewalt sich wahllos
gegen jeden richten w&uuml;rde. Das passiert nur in
Umbruchsituationen, dass jeder
mit seinen Feinden abrechnet. B&uuml;rgerkriege erkl&auml;ren
sich in ihrer Heftigkeit
dadurch, dass alte nachbarschaftliche Konflikte so beglichen werden.
Wir haben
solche Kettenreaktionen auch im Faschismus wie im Kommunismus gehabt.
Es traf
nicht nur Ausl&auml;nder und Juden, sondern auch sog. Arier. In
Deutschland war der
blonde, 1,80 m gro&szlig;e Mann trotz der Rassenlehre nicht sicher
vor KZ oder
Erschie&szlig;ung. Die wahllose Verurteilung und
Erschie&szlig;ung von Sowjetb&uuml;rgern unter
Stalin zeigt ein ausuferndes Gewaltsyndrom, dessen Prinzip war, dass
keiner vor
einem Schauprozess sicher sein konnte. </p>

            <p class="MsoNormal"><span style="font-weight: bold;">Der S&uuml;ndenbock
reduziert die Gewalt auf ein Opfer</span><br>

H&auml;tte die Menschheit nach dem Muster dieser Kettenreaktion
von Gewaltanwendung die mit Notwendigkeit entstehenden schlechten
Gef&uuml;hle
herausgeschafft, h&auml;tten die ersten Sippen nicht
&uuml;berlebt. Girard stellt dazu
eine einfache &Uuml;berlegung an: Wenn in einem Stamm, in dem alle
M&auml;nner f&uuml;r die
Jagd mit Waffen ausger&uuml;stet sind, diese nicht f&uuml;r die
Jagd eingesetzt, sondern
gegeneinander erhoben w&uuml;rden, w&auml;re der Stamm in
seiner Existenz gef&auml;hrdet. Das
Gleiche gilt aber auch f&uuml;r eine Abteilung in einem
Unternehmen, einer
Institution &ndash; wenn jeder gegen jeden &bdquo;Rechnungen
begleichen&ldquo; w&uuml;rde, w&auml;re das
Ganze gef&auml;hrdet. Die L&ouml;sung besteht darin, dass man
einen sucht, auf den sich
alle unguten Gef&uuml;hle richten k&ouml;nnen. Wenn dieser
gefunden und ausgesto&szlig;en oder
vernichtet wurde, hat er die schlechten Gef&uuml;hle weggetragen.
Wir nennen das
heute Mobbing. Die Luft ist wieder klarer, man kann atmen. Zudem sind
alle
&uuml;berzeugt, dass der Gemobbte tats&auml;chlich der
Schuldige gewesen sein muss, denn
die Gef&uuml;hle haben sich zum Positiven ver&auml;ndert. Wenn
die Gef&uuml;hle besser sind,
wird das als nachtr&auml;glicher Beweis erlebt, dass die
Aussto&szlig;ung rechtm&auml;&szlig;ig
erfolgte. </p>

            <p class="MsoNormal">Zum Mobbingopfer wird
meist derjenige, der sich am wenigsten
wehren konnte. Oder es wird, Trotzki ist daf&uuml;r ein klassisches
Beispiel,
derjenige zum Opfer, der der Rivale des St&auml;rkeren war. Bei
Trotzki kann man den
Mechanismus, der meist verdeckt ist, besonders deutlich beobachten: Es
gen&uuml;gte
nicht, dass er in Mexiko im Exil lebte, er musste nicht nur entmachtet,
sondern
physisch umgebracht werden. Manche Morde sind so zu verstehen, vor
allem in
Gruppen, die von sich sagen &ldquo;Wir haben ein Gesetzt und nach
diesem Gesetz muss
er sterben.&ldquo; Ein solches Delikt ist bei Rockergruppen z.B.,
sich an der &bdquo;Braut&ldquo;
des anderen zu vergreifen.<br>

In den meisten Bev&ouml;lkerungskreisen wird das Mobbingopfer
nicht physisch umgebracht, es gen&uuml;gt die Absetzung, die
allerdings eine Art &bdquo;soziale
Enthauptung&ldquo; darstellt.</p>

            <p class="MsoNormal"><span style="font-weight: bold;">Das B&ouml;se muss
herausgeschafft
werden</span><br>

Eine Gesellschaft ist darauf angewiesen, dass das B&ouml;se nicht
als verborgener Giftherd weiter wirkt. Die Mittel, die zur
Verf&uuml;gung stehen,
sind begrenzt. Ein einfacher Weg, die zersetzende Kraft der
b&ouml;sen Taten nach
au&szlig;en zu lenken, ist ein Krieg. Dieser er&ouml;ffnet die
M&ouml;glichkeit, die unguten
Gef&uuml;hle auf einen &auml;u&szlig;eren Feind zu lenken.
Der Falklandkrieg ist ein
klassisches Beispiel im Miniaturformat. Beide Kriegsparteien mussten
mit inneren
Schwierigkeiten fertig werden. Der 1. Weltkrieg wurde zwar durch das
Attentat
von Sarajewo ausgel&ouml;st worden, die Voraussetzung, dass der
Funke einen Brand
ausl&ouml;sen konnte, war eine Phase politischer Stagnation und
wachsenden
Misstrauens, f&uuml;r deren Bew&auml;ltigung alle Staaten
gro&szlig;e R&uuml;stungsanstrengungen
unternahmen.</p>

            <p class="MsoNormal">Die j&uuml;dische
Religion hatte f&uuml;r die Entlastung von dem B&ouml;sen
einen j&auml;hrlichen Ritus, von dem sich der Begriff
&bdquo;S&uuml;ndenbock&ldquo; herleitet<span style="font-style: italic;">:</span><cite><a name="a21"></a></cite></p>

            <p class="MsoNormal"><cite><a name="a21">&bdquo;Aaron soll
seine beiden H&auml;nde auf den Kopf des lebenden Bockes legen und
&uuml;ber ihm alle
S&uuml;nden der Israeliten, alle ihre Frevel und alle ihre Fehler
bekennen. Nachdem
er sie so auf den Kopf des Bockes geladen hat, soll er ihn durch einen
bereitstehenden Mann in die W&uuml;ste treiben lassen</a> </cite><a name="a22"><cite>und
der Bock soll alle ihre S&uuml;nden mit sich in die Ein&ouml;de
tragen.&ldquo;</cite><br>

Levitikus, 16,
21-22 </a>&nbsp;</p>

            <p class="MsoNormal">Setzen wir bei den
fr&uuml;hen
menschlichen Gesellschaftsformen an, dann wird deutlich, dass es einen
Ents&uuml;hnungsritus
geben muss. Denn man konnte Verbrecher nicht in Gef&auml;ngnissen
absondern. Einige
M&ouml;glichkeiten, sich zu festgelegten Zeiten des B&ouml;sen
zu entledigen, seien kurz
beschrieben.</p>

            <ol style="margin-top: 0cm;" start="1" type="1">

              <li class="MsoNormal" style="">Girard
hat durch eine Analyse der kultischen Opfer gezeigt, dass das
urspr&uuml;nglich reale Opfer rituell wiederholt wird, damit es so
seine Wirkung beh&auml;lt, ohne dass neu ein Mobbingopfer gefunden
werden muss. Aber es gab auch regelm&auml;&szlig;ig
wiederkehrende Menschenopfer.</li>

              <li class="MsoNormal" style="">Die
Azteken haben Kriegsgefangene geopfert. Ein anderer Ausweg sind
Tieropfer. Das sind die g&auml;ngigsten Formen. Es gibt aber auch
andere.</li>

              <li class="MsoNormal" style="">In
einer afrikanischen Kultur wird der K&ouml;nig einmal im Jahr mit
den schlimmsten Verbrechen beschuldigt, so, seinen Vater umgebracht und
seine Mutter geschw&auml;ngert zu haben. Der K&ouml;nig muss
nicht sterben, sondern wird einem Spie&szlig;rutenlauf ausgesetzt.
Die Anschuldigungen mit den schlimmsten Verbrechen sollen beweisen,
dass er sich schuldig gemacht hat und daher umgebracht werden muss.
Wenn der K&ouml;nig angespuckt, mit Ruten geschlagen und mit
Anschuldigungen &uuml;berh&auml;uft worden ist, hat er die
Schuld aller weggetragen. Wir haben &auml;hnliche Riten im
Karneval. Seine Urspr&uuml;nge liegen in der Konzeption der zwei
Reiche, die Augustinus in seinem Gottesstaat beschreibt, das Reich des
B&ouml;sen und das des Guten. Der Karneval stellt das Reich des
B&ouml;sen dar, die Masken sind die Laster, der Narr der Dummkopf,
der Gott nicht in den Werken der Sch&ouml;pfung erkennt. Wenn im
Mainzer Karneval die Politiker auf Korn genommen werden, ist das mit
den rituellen Beschimpfungen afrikanischer K&ouml;nige
vergleichbar. Der Umgang mit Helmut Kohl hatte auch
&Auml;hnlichkeiten mit diesem afrikanischen Ritual.</li>

            </ol>

            <p class="MsoNormal">Sind Rituale
wirksam, die j&auml;hrlich oder h&auml;ufiger das
&bdquo;B&ouml;se&ldquo; herausschaffen, baut sich
jeweils die schlechte Stimmung ab, die durch Missgunst, &uuml;ble
Nachrede,
Sich-Zur&uuml;ckgesetzt-F&uuml;hlen st&auml;ndig neu
hervorgebracht wird. </p>

            <p class="MsoNormal">Ren&eacute; Girard<br>

Der franz&ouml;sische
Literaturwissenschaftler hat zuerst an Romanen des 19. Jahrhunderts,
vor
allem bei Dostojewski, aufgezeigt, wie stark menschliches Verhalten
durch
Nachahmung und daraus entstehenden Neid gepr&auml;gt ist. An den
Opferkulten und den
&uuml;ber ihre Entstehung berichtenden Mythen hat er gezeigt, dass
das erste Opfer
nach dem S&uuml;ndenbock-Mechanismus erfolgte. </p>

            <p class="MsoNormal" style="margin-left: 18pt; text-indent: -18pt;"><!--[if !supportLists]-->-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<!--[endif]-->Figuren
des Begehrens. Das Selbst und der Andere in der fiktionalen
Realit&auml;t. LIT,
M&uuml;nster 1999; </p>

            <p class="MsoNormal" style="margin-left: 18pt; text-indent: -18pt;"><!--[if !supportLists]-->-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<!--[endif]-->Das
Heilige und die Gewalt. Fischer, Frankfurt a. M. 1994, zuletzt
D&uuml;sseldorf,
Patmos 2006 (La Violence et le sacr&eacute;, 1972,)</p>

            <p class="MsoNormal" style="margin-left: 18pt; text-indent: -18pt;"><!--[if !supportLists]-->-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<!--[endif]-->Das
Ende der Gewalt. Analyse des Menschheitsverh&auml;ngnisses.
Erkundungen zu Mimesis
und Gewalt mit Jean-Michel Oughourlian und Guy Lefort. Herder, Freiburg
2009;</p>

            <p class="MsoNormal" style="margin-left: 18pt; text-indent: -18pt;"><!--[if !supportLists]-->-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<!--[endif]-->Ich
sah den Satan vom Himmel fallen wie einen Blitz. Eine kritische
Apologie des
Christentums. Hanser, M&uuml;nchen 2002, <br>

&nbsp;&nbsp;Girard zeigt auf, dass die Evangelisten den Bericht
&uuml;ber den Prozess Jesu nach
dem S&uuml;ndenbockmechanismus strukturieren. </p>

            <p class="MsoNormal">Siehe auch: <a href="boese_ueberwindung.php">die &Uuml;berwindung des
B&ouml;sen</a></p>

            <p class="MsoNormal">Eckhard Bieger S.J.</p>

            <p><br>

&copy; <a href="http://www.kath.de">www.kath.de</a></p>

            </td>

            <td background="boxright.gif"><img alt="" src="boxright.gif" height="8" width="8"></td>

          </tr>

          <tr align="left" valign="top">

            <td><img alt="" src="boxbottomleft.gif" height="8" width="8"></td>

            <td style="font-family: Helvetica,Arial,sans-serif;" background="boxbottom.gif"><img alt="" src="boxbottom.gif" height="8" width="8"></td>

            <td><img alt="" src="boxbottomright.gif" height="8" width="8"></td>

          </tr>

        </tbody>
      </table>

      </td>

      <td style="vertical-align: top;">
      <script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
google_ad_width = 336;
google_ad_height = 280;
google_ad_format = "336x280_as";
google_ad_type = "text_image";
//2006-10-23: Lexikon Symbole
google_ad_channel = "8495253357";
//--></script>
      <script type="text/javascript" src="show_ads.js">
      </script></td>

    </tr>

    <tr>

      <td align="left" valign="top">&nbsp; </td>

    </tr>

  </tbody>
</table>

</body>
</html>
