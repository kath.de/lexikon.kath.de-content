---
title: Hypostatische Union
author: 
tags: 
created_at: 
images: []
---


# **Unio hypostatica, Einigung nach der Substanz*
# **Als die christlichen Prediger in die griechisch denkende Welt wanderten, predigten sie zuerst in den jüdischen Synagogen. Sie waren ja Juden und genossen dort Gastrecht. Wie eine christliche Gemeinde heute freute sich auch die jüdische Gemeinde, von einem fremden Prediger etwas Neues zu hören. Diese verkündeten, daß der Messias gekommen sei. Obwohl er von der jüdischen Obrigkeit abgelehnt worden und den Römern zur Hinrichtung übergeben worden sei, habe Gott sich zu ihm bekannt. Er sei auferweckt und in den Himmel zur Rechten Gottes erhoben. Am Ende der Zeit werde er als Weltenrichter wiederkommen. Das war mehr als die Auslegung der Bücher des Moses und der Propheten. In der Apostelgeschichte ist beschrieben, wie sich ein Teil der Juden den christlichen Predigern anschlossen, die meisten aber den Neuen Weg ablehnten. Über diese Predigtmethode erreichten die christlichen Boten auch Nicht-Juden, die sich dem jüdischen Glauben angeschlossen hatten wie auch solche griechisch sprechenden Bewohner des römischen Reiches, die noch keinen Zugang zum einen Gott gefunden hatten. **
# *
 D*****ie christliche Botschaft in der griechischen Welt** ***

# **Die sich für die Botschaft von dem einen Gott ansprechen ließen, hatten sich meist von der Vielgötterei abgewandt und glaubten, im Gefolge der griechischen Philosophie, daß es nur einen Gott geben kann. Platon, der zwischen 427 und 347 v. Chr. lebte, war auf gedanklichem Weg zu der Erkenntnis gekommen, daß die griechischen Mythen wenig über das Wesen Gottes aussagen, daß vor allem nicht mehrere Gottheiten nebeneinander existieren konnten. Gott konnte nur einer sein. Das Göttliche muß aber auch verschieden von dem Nicht-Göttlichen gedacht werden, sonst wäre die Gottheit ein Teil der Welt. Die Christen brachten vom jüdischen Denken diesen Schöpfungsglauben mit, daß nämlich die Materie einen Anfang hat und alles, was der Mensch beobachten kann, die Sterne, das Meer, Pflanzen und Tiere und sogar er selbst von Gott geschaffen worden sind. Für das griechische Denken hatte Aristoteles die Vorstellung entwickelt, daß alle Bewegung von Gott ausgehe und den Begriff vom „unbewegten Beweger“ geprägt. Es zeigt sich also im Denken schon, daß es nicht das Göttliche als ein unbeschreibbares Etwas gibt, sondern daß dieses Göttliche in einem Subjekt existiert. Das ist nicht so selbstverständlich, Gott als ein Subjekt zu denken, der handelt, einen Willen hat, sich in Beziehung zu seiner Schöpfung setzt, den Menschen persönlich anspricht. Viele Menschen auch damals sehen im Göttlichen eine Macht, eine Kraft, aber nicht unbedingt ein persönliches Gegenüber des Menschen, wie die Juden den sich offenbarenden Gott erfahren hatten. **
# *
 Der Begriff Hypostasis*****

 Mit dem griechischen Wort „Hypostasis“, wird das bezeichnet, was darunter liegt, was das Ganze trägt. Wenn Jesus wirklich Gott ist, dann ist Gott dem Menschen nicht als bloß unfaßbares Wesen entgegen getreten, sondern mit einem Gesicht. Das Wort ***[Person](person.php) leitet sich im Griechischen von Antlitz, „Prosopon“ ab. Aber war dieser Mensch, den die Christen als Messias verkündeten, wirklich Gott? Dann würde es ja wieder mehr als einen Gott geben. ***

# **Ewiger Sohn Gottes und wahrer Mensch**
# ****D er strikte Monotheismus der griechischen Philosophie wurde für die christliche Theologie ein Problem. Als die christliche Kirche unter Kaiser Konstantin die äußere Freiheit erhielt, brach das Problem auf. Im Konzil von Nicäa (325) ging es um die Frage, ob Jesus nur Mensch, oder ob er wirklich Gott war. In der Treue zu den Aussagen der christlichen Bibel hielt das Konzil gegen die Vorgaben der griechischen Philosophie daran fest, daß Jesus von Nazareth wirklich ***[Gottes Sohn](gottessohn.php) ist. Es stellte fest, daß er nicht geschaffen, sondern der aus dem Vater gezeugte Sohn und damit wirklich Gott ist. Gibt es also in Gott doch mehrere Subjekte? Um zu einer weiteren begrifflichen Klärung zu kommen, hat das I.  Konzil von Konstantinopel  im Jahr 381 die Gottheit, die allen göttlichen Personen gleichermaßen zugeschireben wird, mit Ousia (Wesen, Sein) benannt und die Unterschiedenheit der drei Personen mit Hypostase. Eine Ousia in drei Hypostasen war also die Begrifflichkeit, die man ein halbes Jahrhundert nach dem Konzil von Nicäa gewählt hat. Wenn Jesus aber Gott ist, dann ergibt sich sofort das Problem, wie in Jesus Christus Gottheit/Gott-Sein und Menschheit/Mensch-Sein unterschieden werden können. War die Menschheit Jesu nur „Schein“ oder war nur der Körper Jesu menschlich und die göttliche Hypostase, nämlich die zweite göttliche Person,  an die Stelle der menschlichen Seele getreten? Alle Varianten wurden durchgespielt. s. ***[christologische Streitigkeiten](christologische_streitigkeiten.php). **
 Konnte der Begriff Hypostase bei der Klärung weiterhelfen? Da man die Erlösung in Frage stellte, wenn nur der Körper des Jesus von Nazareth wirklich menschlich war, mußte man zu der Überzeugung kommen, daß Jesus eine menschliche Seele mit einem eigenen Willen hat. Denn wenn die Seele nicht in Jesus erlöst wird, dann hat keine wirkliche Erlösung stattgefunden.***

 Wenn Jesus wirklicher Mensch mit Leib und Seele war, dann war er ein menschliches Subjekt und damit eine menschliche Hypostase. Dabei wird im 4. Jahrhundert der Begriff Hypostase noch sehr allgemein gebraucht, er konnte z.B. auch materiellen Besitz bedeuten; erst durch die christologischen Auseinandersetzungen ist er dann im 6. Jh. verfeinert worden. Unser heutiger Subjektbegriff hat sich in dieser Ziet erst entwickeln können. ***

 Theodoret, Bischof von Cyrus in den Jahren 423-466, schreibt, „daß die Hypostasis des Gott-Logos vor den Weltzeiten vollkommen da war und daß von ihr eine vollkommene Knechtsgestalt angenommen wurde. ... Wenn nur jede Natur ihre Vollkommenheit besitzt, aber beide in eins zusammengekommen sind, ... dann ist es fromm, eine Person und ebenso einen Sohn und Christus zu bekennen, und es ist nicht unvernünftig, sondern ganz folgerichtig, von zwei vereinigten Hypostasen oder Naturen zu sprechen.“ ***

 Wenn aber in Jesus Christus zwei Hypostasen unterschieden werden, dann muß man auch von zwei Söhnen sprechen. Dagegen wendet sich die Schule von Alexandrien, die heute noch in der koptischen Kirche weiterlebt. Sie sehen das große Erlösungsgeschehen auseinandergerissen, wenn zwischen dem Sohn Gottes und dem Menschen Jesus nur eine lose Verbindung bestehen sollte. Deshalb schreibt der maßgebliche Vertreter dieser Theologenschule, Cyrill von Alexandrien, im Jahr 431 in der Verteidigung seiner 12 Kapitel gegen Theodoret von Cyrus, zu Kap.2:***


„ Es erfolgt die Einigung nach der Hypostasis, wobei der Ausdruck „nach der Hypostasis“ nichts anderes bedeutet als nur dies, daß die Natur oder Hypostasis des Logos, d.h. der Logos selbst, mit seiner menschlichen Natur wahrhaft geeint wird ohne jede Veränderung und Vermischung und … als Ein-Christus gedacht wird und es auch ist, derselbe Gott und Mensch.“***

 Hypostatische Union bedeutet dann, daß die Einheit von Gottheit und Menschheit in Jesus durch die Hypostase zustande kommt.***

 Cyrill spricht sogar von einer neuen Natur, die durch die Menschwerdung entsteht (***[s. Monophysitismus](monophysiten.php)), weil die alexandrinische Theologie die Einheit herausstellen will. Die Schule von Antiochien, vertreten durch Nestorius und Theodoret, betont, daß Gottheit und Menschheit in Jesus Christus nicht vermischt sind (was auch die Alexandriner nicht leugnen wollen) und daher unterschieden bleiben müssen. Das erklärt warum Theodoret „Hypostase“ mehr im Sinne von Natur gebraucht. Seine Schule sieht in Christus zwei Naturen, die unterschieden bleiben. Hypostase wird von Cyrill dagegen mehr im Sinne von Person gebraucht. Das ist problematisch: für Theodoret wie für Cyrill ist eine Natur ohne Hypostasis nicht vorstellbar. Weil Cyrill die Einheit der Person Christi so wichtig ist und er die Vermischung nicht fürchtet, bleibt er bei Hypostasis oder Physis, die Antiochener dagegen vermeiden den Begriff Hypostasis, um die einheit in Jesus christus zu bezeichnen und verwenden statt dessen den unbelasteten Begriff ***[Prosopon](person.php). Erst im 6. Jh. kommt es im Gefolge des Konzils von Chalcedon zur Überlegung, dass die menschliche Natur Christi wohl eine Hypostase habe, aber eben vom ersten Moment der Empfängnis an die der zweiten göttlichen Person, und so ist sie enhypostatisch, d.h. hat ihre Hypostase in einer anderen. Diese Begriffsbildung von der Enhypostasie hat dann die strikt alexandrinische Richtung und bis heute die koptische und armenische Kirche nicht mitvollzogen.***

 Die theologische Diskussion zwischen der alexandrinischen und antiochenischen Schule zeigt, daß der Begriff Hypostase nicht deutlich genug das einigende Prinzip in Jesus Christus bezeichnen kann. Der Begriff leistet jedoch in dieser Zeit noch etwas, nämlich daß er die subjekthafte Wirklichkeit bezeichnet, das, was trägt, während der griechische Begriff Antlitz (Prosopon) die Einheit noch nicht deutlich genug als wirkliche, in sich selbst existierende deutlich machen kann. In der Diskussion bildet sich aber der Begriff Person immer mehr heraus, so daß er wie für uns heute die Wirklichkeit des Subjektes bezeichnen kann. So schreibt Theodoret: ***

 "Als wahrhaftigen Gott und wahrhaftigen Menschen bekennen wir uns Herrn Jesus Christus, und wir teilen den Einen nicht in zwei Prosopa (Personen), sondern glauben, daß zwei Naturen unvermischt geeint sind.“             

# **In der sog. Unionsformel zwischen beiden Schulen aus dem Jahr 433 wird nicht mehr von Hypostasen gesprochen, sondern diese läuft auf den Begriff Prosopon zu und zeigt, wie sich in den Auseinandersetzungen um das Verständnis der Menschwerdung sich der Personbegriff langsam herausgebildet hat.**
„ Wir bekennen, daß unser Herr Jesus Christus, der einziggeborene Sohn Gottes, vollkommener Gott und vollkommener Mensch aus vernünftiger Seele und Leib, vor den Weltzeiten aus dem Vater nach der Gottheit, aber am Ende der Tage als derselbe um unseretwillen und um unserer Errettung willen aus Maria der Jungfrau nach der Menschheit geboren wurde, dem Vater wesensgleich nach der Gottheit und derselbe uns wesensgleich nach der Menschheit. Denn es ist eine Einigung der Naturen erfolgt. Einen Christus, Einen Sohn, Einen Herrn bekennen wir daher.***

 Gemäß dieser Vorstellung von der unvermischten Einigung bekennen wir die heilige Jungfrau als Gottesmutter, weil der Gott-Logos Fleisch und Mensch geworden ist und unmittelbar von der Empfängnis an den aus ihr genommenen Tempel (d.i. sein Leib) mit sich vereinigt hat.***

 Wir wissen aber, daß die von Gott lehrenden Männer die evangelischen und apostolischen Worte über unseren Herrn teils gemeinsam auf eine Person (Prosopon) beziehen, teils gleichsam auf zwei Naturen verteilen und die gottgemäßen Worte der Gottheit Christi, die niedrigen aber entsprechend seiner Menschheit erklären.“***

 Mit den hier „niedrigen Worten“ sind u.a. die im Garten Gethsemane gemeint, als Jesus den Vater bittet, „daß der Kelch an ihm vorübergehe.“***

# *
 Zur Begriffsentwicklung*****

 Der Begriff „Hypostatische Union“ geht auf Cyrill von Alexandrien zurück. Der Theologe Maximus Confessor ging 200 Jahre nach den oben beschriebenen theologischen Diskussionen der Frage nach, ob Jesus einen menschlichen Willen hatte. (s. ***[Monotheletismus](monotheletismus_monergetismus.php))            ***


„ Von gleicher Hypostase (Subjektsein) ist, was mit einem anderen zu einer und derselben Hypostase zusammengefügt ist, wie es sich verhält bei Seele und Leib und allem anderen, was bei unterschiedlicher und andersartiger Natur gemäß der Hypostase geeint ist. Wenn also etwas mit einem anderen in Einheit verbunden ist, dann existiert es als dasselbe gemäß der Hypostase … es ist also die hypostatische Union, die die unterschiedlichen Wesenheiten oder Naturen zu einer Person und ein und derselben Hypostase (Subjektsein) zusammenführt und bindet. ….***

 Wenn wir von einer Einigung gemäß der Hypostase sprechen, dann erkennen und bekennen wir, daß die Einigung der Naturen zu einer Hypostase erfolgt ist. …***

 Durch seine Gottheit wurde er nicht gehindert, Mensch zu werden, noch wurde er durch seine Menschwerdung im Gottsein gemindert, der eine und selbe besteht ganz in beiden Naturen …***

 Opuscula theologica, Ambiguorum liber II um 640 bzw. um 630***

  

***Eckhard Bieger              

  

***©***[ www.kath.de](http://www.kath.de) 
