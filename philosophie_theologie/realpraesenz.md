---
title: Realpräsenz
author: 
tags: 
created_at: 
images: []
---


# **Die reale Gegenwart Jesu in Brot und Wein**
 **  

# **Realpräsenz ist das lateinische Wort für „wirkliche Gegenwart“. Dieser Begriff ist für eine theologische Fragestellung entwickelt worden: Wie ist Jesus Christus in Brot und Wein gegenwärtig, wenn die Christen sein Gedächtnismahl feiern? **
 Im Mittelalter wurde um diese Frage heftig gerungen. Man wollte genauer beschreiben können, was das für ein Brot ist, das die Christen beim Kommuniongang empfangen und was sie verehren, wenn die Hostien im Tabernakel aufbewahrt werden.***

 Jesus hat selbst im ***[Abendmahlssaal](http://www.kath.de/Kirchenjahr/gruendonnerstag.php) das Brot mit den Worten gebrochen: „Dies ist mein Leib“ und den Kelch mit der Erklärung herum gereicht: „Dies ist mein Blut“. [Mt 26,26-28;Mk 14,22-24;Lk 22,19f;1 Kor 11,23-26] Wenn diese Worte vom Priester bei der Eucharistiefeier gesprochen werden, dann spricht man von Wandlung. Was geschieht in der Wandlung? Etwas ändert sich. Offensichtlich werden das Brot und der Wein gewandelt. Wie kann man sich das aber vorstellen, denn Brot und Wein verändern sich nach außen nicht, sie schmecken wie Brot und wie Wein. Wenn aber Brot und Wein nicht nur Hinweise auf den Leib und das Blut Jesu sein sollen, muß man die offensichtliche Beobachtung mit der Aussage der Sätze Jesu in Übereinstimmung bringen „Dies ist mein Leib“, „Dies ist mein Blut“.***

 Wenn Jesus Christus wirklich und wesenhaft und nicht rein gedacht unter den Gestalten von Brot und Wein gegenwärtig sein soll, genügt es nicht zu sagen, daß etwas in die Gaben hineingekommen (Impanation) und bzw. etwas hinzugekommen ist (Konsubstantiation), sondern daß Brot und Wein in ihrem Wesen verwandelt sind. Dafür wurde der Begriff „Transsubstantiation“ als geeignet gesehen. Wenn sich das Wesen der Gaben verändert, also eine andere Substanz wird, dann ist Jesus Christus in Brot und Wein wirklich gegenwärtig, real präsent. Die Substanz selbst ist verwandelt, daher Trans-Substantiation. ***

 Die wirkliche ***[Gegenwart](gegenwart_jesu.php) heißt dann, daß Jesus in jeder Hostie und jedem Schluck Wein unzertrennt gegenwärtig ist. Wenn die Gläubigen das Brot essen und den Wein trinken, wird Jesus nicht zerkaut und auch nicht verdaut, sondern von den Gläubigen „empfangen“, aufgenommen. Das zeigt, daß man das Physische, das Brot und den Wein, nicht physikalisch und chemisch bis zum Ende durchdenken kann, sondern daß die Gegenwart Jesu sich physikalischen Begriffen entzieht. Deshalb ist der Begriff „Transubstantiation“ ein Begriff, mit dem die neue Realität von Brot und Wein gedacht werden kann. Es gibt auch neuere Vorstellungsmodelle, die heutige Denkkategorien zur Verfügung stellen, z.B. Transfinalisation. Für die Teilnahme an der Eucharistiefeier ist daher nur notwendig, daß man eine wirkliche Gegenwart Jesu in den Gestalten von Brot und Wein annimmt, nicht aber eine bestimmten Vorstellung folgen muß, wie diese Gegenwart in Beziehung zu der bleibenden Gestalt von Brot und Wein stehen.***

 Jesus Christus selbst ist der Handelnde, er ist Gabe und Geber zugleich. Die Kommunion heißt Begegnung mit ihm.***

 Nach dieser Lehre ist Jesus Christus unter den konsekrierten Mahlgaben auch über die Eucharistiefeier hinaus wirklich gegenwärtig (im Gegensatz zum Glaubensverständnis Luthers, das die wirkliche Gegenwart Christi auf den Augenblick des Empfanges der eucharistischen Gaben einschränkt). Deshalb wird das eucharistische Brot an einem besonderen Ort (tabernaculum, Tabernakel) einer Kirche oder Kapelle zur Speisung der Kranken (Krankenkommunion) und Todesgefährdeten aufbewahrt, da das eigentliche Sakrament in der Sterbestunde nicht die Krankensalbung (unctio infirmorum) darstellt, sondern der Kommunionempfang (viaticum, Wegzehrung). Darüber hinaus wird das eucharistische Brot im Tabernakel auch zur stillen Verehrung und Anbetung aufbewahrt. Deshalb ist es katholischer Brauch, dass sich die Gläubigen beim Passieren einer Kirche mit dem Kreuz bezeichnen und beim Betreten einer Kirche ihre Knie in Richtung des Tabernakels beugen. Auch das sogenannte „ewige Licht“, eine in unmittelbarer Nähe zum Tabernakel fortwährend (rötlich) brennende Öllampe, verweist in katholischen Kirchengebäuden auf die Präsenz des in den eucharistischen Gaben gegenwärtigen Herrn. Das Herausnehmen aus dem Tabernakel und Einsetzen des eucharistischen Brotes (Hostie) in ein Zeigegefäß (Monstranz) nennt man „Aussetzung des Allerheiligsten“ (Expositio). Durch diese Zur-Schau-Stellung können die Gläubigen den eucharistischen Herrn anbeten und verehren. Der „eucharistische Segen“, in dem mit der Monstranz ein Kreuzzeichen gemacht wird, schließt eine solche Anbetungsandacht feierlich ab. ***

 Im Mittelalter, zur Zeit der Gotik, entwickelte sich eine starke Verehrung des im eucharistischen Brot gegenwärtigen Herrn. Ausdruck dieser Verehrung ist das Fronleichnamsfest. Es wurde von der Reformation nicht weiter geführt, so daß es seitdem als Besonderheit der katholischen Frömmigkeit gilt. Fronleichnam, von mittelhochdeutsch fron: herrlich, heer, Lichnam bedeutet entgegen dem heutigen Sprachgebrauch den lebendigen Leib. Es wird zehn Tage nach Pfingsten, je nach dem Ostertermin im Frühsommer Ende Mai bis Mitte Juni gefeiert. An diesem Festtag wird das eucharistische Brot in einer Monstranz in einer Prozession durch den Stadtteil und auf dem Land auch durch die Felder getragen. (***[Fronleichnamsprozession](http://www.kath.de/Kirchenjahr/fronleichnam.php)). Singend und betend schreitet die Gemeinde auf dem von Blumen(-teppichen), frischem Grün, beflaggten Häusern und traditionell vier Stationsaltären (Himmelsrichtungen) gesäumten Prozessionsweg und bittet um Segen für die Menschen und die Fluren.              

# **„Wir glauben, dass in der Weise, wie Brot und Wein vom Herrn beim letzten Abendmahl konsekriert und in Seinen Leib und Sein Blut verwandelt worden sind, die Er für uns am Kreuze geopfert hat, auch Brot und Wein, wenn sie vom Priester konsekriert werden, in den Leib und das Blut Christi verwandelt werden, der glorreich in den Himmel aufgefahren ist. Und wir glauben, dass die geheimnisvolle Gegenwart des Herrn unter den äußeren Gestalten, die für unsere Sinne in derselben Weise wie vorher fortzubestehen scheinen, eine wahre, wirkliche und wesentliche Gegenwart ist.“ **
 Paul VI., Credo des Gottesvolkes, Feierliches Glaubensbekenntnis vom 30. Juni 1968, Nr. 24-26, AAS 60 (1968), S. 442-443***

# *
„ Es gehört zum christ- katholischen Glauben, dass Jesus Christus mit Gottheit und Menschheit unter den eucharistischen Gestalten wahrhaft gegenwärtig ist. Gewiss ist diese Gegenwart unter den Symbolen menschlicher Nahrung ausgerichtet auf den wirklichen Empfang und Genuss dieser eucharistischen Speise. Aber das ändert nichts daran, dass in dieser Speise Jesus Christus mit Gottheit und Menschheit nicht nur gegenwärtig ist, indem er empfangen wird, sondern zuvor gegenwärtig ist, damit er leibhaftig empfangen werden könne. Und darum kann der katholische Christ Jesus, das göttliche Unterpfand seines Heiles, unter diesen eucharistischen Zeichen anbeten. Solche Anbetung ist im Vergleich zum wirklichen Empfang des himmlischen Brotes zwar nicht der Höhepunkt des sakramentalen Geschehens, wohl aber eine legitime Konsequenz aus dem katholischen Glauben an die wahre Gegenwart des Herrn im Sakrament...Wir katholischen Christen wollen in Gemeinschaft und als Einzelne auf das Zeichen der Gegenwart dessen blicken, der uns geliebt hat und sich für uns dahingegeben hat. Es sollte für uns nicht fremd sein, auch einmal in privatem Gebet vor dem Herrn zu knien, der uns erlöst hat.“***

 Karl Rahner, Geist und Leben 54 (1981), 188-191 

# **"Seit mehr als einem halben Jahrhundert sind meine Augen jeden Tag auf die weiße Hostie gerichtet, in der Zeit und Raum in gewisser Weise zusammenfallen und in der das Drama von Golgota lebendig gegenwärtig wird."**
 Johannes Paul II., Ecclesia de eucharistia vom 17. April 2003, Nr. 52: AAS 95 (2003) 468 

# ** „ Empfangt, was ihr seht: Leib Christi. Werdet, was ihr empfangt: Leib Christi.“**
 Augustinus, Sermo 272 (PL 38) Corpus Augustinianum Gissense a C. Mayer editum 

# ****
 Peter Münch ***

  

©***[ ***www.kath.de](http://www.kath.de) 
