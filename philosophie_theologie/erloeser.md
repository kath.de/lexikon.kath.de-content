---
title: Erlöser
author: 
tags: 
created_at: 
images: []
---


# **Jesus Christus, der Erlöser, Redemptor*
# **Wie wurde Jesus zum Erlöser?****
 Jesus wird von Paulus als der bezeichnet, der wegen unserer Sünden hingerichtet und wegen unserer Rettung auferweckt wurde. (Römerbrief 4,25) Die Sünden der Menschen machen den Tod Jesu notwendig. Was das bedeutet, ist für den christlichen Glauben wichtig, denn jeden Sonntag werden Tod und Auferstehung Jesu in der Eucharistiefeier vergegenwärtigt, damit die Gläubigen von Neuem an ihrer Erlösung teilhaben. Auch in anderen Gottesdiensten und in vielen biblischen Lesungen wird häufig auf die Erlösung Bezug genommen. Daß die Erlösung einer Sehnsucht der Menschen entspricht, deutet sich nicht nur in individuellen Schuldgefühlen an, sondern zeigt sich heute in der Hoffnung auf Frieden. Immer wieder berichten die Nachrichten über Morde, Überfälle, Auseinandersetzungen zwischen Straßenbanden, von Bürgerkriegen und blutigen Aufständen. Offensichtlich hat der Mensch etwas in sich, das ihn immer wieder vom Frieden abbringt.  Aber mußte Jesus wegen der Erlösung überhaupt sterben und was nutzt sein Tod im Hinblick auf Frieden und Versöhnung? Sind nicht nach ihm viele Menschen umgebracht worden, ohne daß sich die Situation irgendwie verbessert hätte? 

# **Jesus hat bereits vor seinem Tod Sünden vergeben**
# ****Die Vergebung der Sünde macht den Tod des Messias nicht notwendig.**
 Jesus hatte selbst einen Neuanfang angekündigt. „er verkündete das Evangelium Gottes und sprach: die Zeit ist erfüllt, das Reich Gottes ist nahe. Kehrt um und glaubt an das Evangelium,“ heißt es bei Markus (1,14-15) Der Inhalt dieser guten Nachricht ist die Nähe des Reiches Gottes, die sich in den Heilungen und der Predigt Jesu erweist. Zu der Predigt Jesu gehört ausdrücklich die Vergebung der Sünden. Einige Zeilen später im Markusevangelium wird berichtet, daß ein Gelähmter zu Jesus getragen wurde. „als Jesus ihren Glauben sah, sagte er zu dem Gelähmten: Mein Sohn, deine Sünden sind dir vergeben.“ (Markus 2,5) Die Schriftgelehrten reagieren mit Recht: "Einige Schriftgelehrten aber, die dort saßen, dachten im Stillen: Wie kann dieser Mensch so reden. Er lästert Gott. Wer kann Sünden vergeben außer dem einen Gott?“ Um seine Vollmacht auszuweisen, heilt Jesus den Gelähmten. Die Reich-Gottes-Botschaft war eine Botschaft der Heilung und Erlösung, die die Sündenvergebung einschloß. Jesus mußte nicht sterben, damit die Menschen Erlösung empfangen konnten, eben sich das zusprechen lassen konnten, was untereinander nur anfanghaft möglich ist, zu verzeihen. Gott kann Sünden wirklich vergeben. Weil Gott die Schuld des Menschen vergibt, soll der Mensch ebenfalls Nachsicht üben und verzeihen. Das verdeutlicht Jesus im Gleichnis von dem Verwalter, der seinem Herrn eine Riesensumme schuldet. Er bekommt sie erlassen, kaum ist er frei, begegnet er einem, der ihm einen viel geringeren Betrag schuldet und läßt ihn ins Gefängnis werfen. „Da ließ ihn sein Herr zu isch rufen und sagte zu ihm: Du elender Diener! Deine ganze Schuld habe ich dir erlassen, weil du mich so angefleht hast. Hättest nicht auch du mit jenem, der gemeinsam mit dir in meinem Dienst steht, Erbarmen haben müssen, so wie ich Erbarmen mit dir hatte? Und in seinem Zorn übergab ihn der Herr den Folterknechten, bis er die ganze Schuld bezahlt habe. Ebenso wird mein himmlischer Vater jeden von euch behandeln, der seinem Bruder nicht von ganzem Herzen vergibt.“ Matthäus 18,32-35 Dieses Gleichnis Jesu beinhaltet nicht nur die Forderung, dem Mitmenschen zu verzeihen, sondern geht auch davon aus, daß Gott den Menschen bereits verziehen hat. Dazu gibt es viele Aussagen im Alten Testament. Warum war der Tod Jesu dann aber noch notwendig?***

# *
 Jesu öffentliches Auftreten führt zu Konflikten***

# *                Die Bibel berichtet davon, daß Jesus zuerst große Zustimmung erfuhr.**
 Johannes der Täufer hat als Vorläufer den Messias, also den, auf den die Juden sehnsüchtig warteten, angekündigt. Viele Menschen sind Jesus gefolgt, haben sich seine Predigt zu Herzen genommen und ihn durch die Heilungswunder als von Gott legitimierten Boten gesehen. Als sich politische Erwartungen auf Jesus richteten, hat er sich nach dem Einzug n Jerusalem (***[Palmsonntag](http://www.kath.de/Kirchenjahr/palmsonntag.php)) entzogen. Er ist nicht zum Anführer eines Aufstandes gegen die Römer geworden. Einer der von ihm Enttäuschten war Judas, der sein Verräter wurde. Obwohl Jesus keine politische Macht anstrebte, war er zu einem politischen Faktor geworden. Der Hohe Rat in Jerusalem schickte Beobachter aus, Schriftgelehrte verwickelten ihn theologische Dispute. Der Druck auf Jesus wurde größer, so daß er sich in das außerjüdische Gebiet von Tyrus zurückzog. Der Rückhalt im jüdischen Volk ließ nach, als deutlich wurde, daß Jesus seine Sendung nicht politisch verstand, nämlich mit der Hilfe Gottes die Fremdherrschaft der Römer abzuschütteln. Jesus entschließt sich dann doch, zum Paschafest wie die anderen Juden aus dem nördlichen Galiläa nach Jerusalem zu gehen. 

# **Es läuft auf den Tod Jesu zu****
 Der Hohe Rat der Juden befürchtete Aufruhr, als Jesus zum Passahfest erschien. Aus den Beratungen des Hohen Rates berichtet Johannes folgende Überlegung: „Sie sagten: Was sollen wir tun. Dieser Mensch tut viele Zeichen. Wenn wir ihn gewähren lassen, werden alle an ihn glauben. Dann werden die Römer kommen und uns die heiligen Stätten und das ganze Volk nehmen.“ (Kap. 11,47-48)***

 Jesus wird nach einem längeren Hin und Herr zwischen jüdischem Hohen Rat, dem obersten römischen Beamten, Pilatus, und Herodes zum Tode verurteilt. Er läßt seine Anhänger in tiefer Enttäuschung und ratlos zurück. Für die damaligen Machthaber schien die Sache ausgestanden. ***

 Als dann seine Jünger 50 Tage nach den Ereignissen im Zusammenhang mit dem jüdischen Fünfwochenfest (***[Pfingsten](http://www.kath.de/Kirchenjahr/pfingsten.php)) ihr Predigt begannen, selbst Heilungen wirkten und große Zulauf des Volkes erhielten, war der Prediger aus Nazareth wieder präsent. Und er wurde als Erlöser verkündet. Als die Leute nach der Predigt des Petrus am jüdischen Pfingstfest fragten: „Was sollen wir tun, Brüder?“ antwortet Petrus: „Kehrt um, und jeder von euch lasse sich auf den Namen Jesu Christi taufen zur Vergebung seiner Sünden. Dann werdet ihr die Gabe des Heiligen Geistes empfangen.“ Apostelgeschichte 2,38 

# **anders als für die Christen war für die Juden schnell deutlich, daß Jesus nicht der Messias gewesen sein konnte. Bis heute erwarten sie den wirklichen Messias, der das Reich Davids wieder herstellt und Israel als selbständigen Gottesstaat aufrichtet. Ein Messias, der allenfalls die Herzen der Menschen gewinnt, kann nicht der erhoffte Nachkomme des Königs David sein, wenn er nicht imstande ist, die äußeren Verhältnisse grundlegend zum Besseren zu wenden. **
 Zweifellos stellt die Hinrichtung Jesu am Kreuz eine Herausforderung dar, der sich die Anhänger Jesu stellen mußten. Aber ist Jesus nicht selbst von seinem Schicksal überrascht worden und hat er überhaupt selbst damit gerechnet, daß seine Mission nicht nur mit dem Risiko des Scheiterns, sondern auch des gewaltsamen Todes befrachtet war?***

 Jesus rechnete mit seinem gewaltsamen Ende. Auf einen ernsthaften Konflikt deutet hin, daß er sich einer zunehmenden feindlichen Beobachtung und Streitgesprächen durch die jüdische Obrigkeit und die Schriftgelehrten ausgesetzt sah. Überliefert sind Leidensweissagungen, gegen die sich die Jünger heftig wehren. (S.u. bei Zitaten)***

 Offensichtlich hat Jesus dann das Mahl zum Paschahfest der Juden im Bewußtsein seines baldigen Todes gefeiert. Das Wort über den Becher Wein wird von Matthäus mit folgenden Worten überliefert: „Trinkt alle daraus, das ist mein Blut, das Blut des Bundes, das für viele vergossen wird zur Vergebung der Sünden. Ich sage euch: von jetzt an werde ich nicht mehr von der Frucht des Weinstocks trinken, bis zu dem Tag, an dem ich mit euch von neuem davon trinke im Reich meines Vaters.“ (Matth. 26, 27-29) Offensichtlich mündet die Mission Jesu in einem tödlichen Konflikt. Jesus geht diesen Weg. Er betet am Ölberg „Meine Seele ist bis zum Tode betrübt ... Abba, Vater, alles ist dir möglich. Nimm diesen Kelch von mir! Aber nicht, was ich will, geschehe, sondern was du willst.“ (Markus 14,34 u 36) Jesus akzeptiert seine Verurteilung und Hinrichtung als Willen Gottes. Was aber bedeutet dieser Wille Gottes, welcher Sinn liegt in dem Sterben Jesu? 

# **Der Sinn des Todes Jesu****
 Die erste Deutung, die sich den Jüngern eröffnet, ist die Begegnung mit dem Auferstandenen. Jesus ist nicht im Tod geblieben, sondern Gott hat ihn auferweckt und damit endgültig als Messias erwiesen. Dies wird in der Predigt des Petrus direkt formuliert: „mit Gewißheit erkenne also das ganze Haus Israel: Gott hat ihn zum Herrn und Messias gemacht, diesen Jesus, den ihr gekreuzigt habt.“ Apg 2,36 ***

 Für den Tod Jesu fanden die Jünger im Alten Testament Deutungen, vor allem in den Liedern vom Gottesknecht. Das 4. dieser Lieder sagt vom Gottesknecht: „Er hat unsere Krankheiten getragen und unsere Schmerzen auf sich genommen. Wir meinten, er sei vom Unheil getroffen, von Gott gebeugt und geschlagen. Doch er wurde durchbohrt wegen unserer Verbrechen, wegen unserer Sünden mißhandelt.“***

 Von Gott heißt es: „Doch der Herr warf all unsere Sünden auf ihn.“ (Text s.u. bei Zitate) 

# **Die Sünde führt zum Tod** - der Krimi erklärt es****
 Der Tod des Unschuldigen ist durch die Sünde verursacht. Hier entsteht eine neue Frage: Warum macht die Sünde den Tod des Gottesknechtes notwendig? Anselm von Canterbury, ein englischer Theologe des 11. Jahrhunderts, geht davon aus, daß die Sünde des Menschen eine solche Entehrung Gottes darstellt, daß kein Geschöpf dafür Genugtuung leisten, sondern allein der menschgewordene Sohn Gottes die Ehre Gottes wieder herstellen kann. Darin spiegeln sich die Vorstellungen des germanischen Ehrenkodexes. Für eine solche Interpretation finden sich in der Bibel keine stichhaltigen Anhaltspunkte. Es ist nicht die vom Menschen gekränkte Ehre Gottes, die ein solches Opfer verlangt, sondern die Sünde selbst, die Bosheit des Menschen verbunden mit seiner Angst, führen mit innerer Logik zum Tod des Unschuldigen.***

# **Daß das Böse zum Tod führt, erklärt uns jeder Krimi. Immer gibt es etwas im Menschen, das ihn töten läßt – Eifersucht, Neid, nicht erwiderte Liebe, Spielleidenschaft oder weil ein Verbrechen entdeckt werden könnte, bringt der Verbrecher den unschuldigen Zeugen um. Was viele Sagen in mythischen Bildern zeigen, ist offensichtlich nicht nur Phantasie, sondern eine Tatsache in der realen, der Welt der Menschen: Der Held wird erst zum Held, wenn er den Kampf mit dem Bösen bestanden hat. Offensichtlich war das auch für den Lebensweg Jesu eine unausweichliche Notwendigkeit. Im Unterschied zu vielen Helden der Sagen und Mythen überwindet Jesus das Böse aber nicht durch körperliche Überlegenheit und Mut, sondern indem er es erleidet. Offensichtlich kann das Böse nicht von außen einfach nur bekämpft werden. Die meisten Kriege, die das Böse beseitigen sollten, sind selbst „böse“ geworden. Offensichtlich ist das Böse auch nicht mit dem Sieg Jesu durch sein Sterben am Kreuz und seine Auferstehung aus der Welt verschwunden. Deshalb kann das Kino die Sagen, ob „Starwars“, „Herr der Ringe“ oder „König von Narnia“ weiter erzählen. **
 Im Leben Jesu geht es aber nicht nur um die Überwindung des Bösen, so wie das jedem Menschen als Aufgabe gestellt ist. Die Sagen und die Kinofilme malen das in großen Bildern, was oft im Herzen des einzelnen stattfindet. Jesus hatte eine definitive Botschaft: Das Reich Gottes ist nahe und die Nähe zeigt sich in seiner Predigt, in der von ihm zugesprochenen Vergebung der Sünden, in der Heilung von Blinden und Aussätzigen, im Zurückweichen der bösen Geister und der Speisung der Zuhörer, die ihm in großer Zahl, die Bibel spricht von 5000, gefolgt waren. Nach anfänglicher Zustimmung stößt Jesus auf Ablehnung, vor allem bei der jüdischen Obrigkeit und den Theologen, die über die richtige Auslegung der Bücher des Moses wachten. Jesus reagiert auf diese sich abzeichnende Ablehnung mit teils scharfen Worten. Am deutlichsten geschieht das in dem Gleichnis vom Weinbergsbesitzer. Dieser hatte seinen Weinberg verpachtet und schickte Angestellte, um die Pacht einzufordern. Die Pächter verweigerten nicht nur die Pachtzahlung, sondern mißhandelten die Abgesandten des Besitzers. „Zuletzt sandte er seinen Sohn zu ihnen; denn er dachte: Vor meinem Sohn werden sie Achtung haben. Als die Winzer den Sohn sahen, sagten sie zueinander: Das ist der Erbe. Auf, wir wollen ihn töten, damit wir seinen Besitz erben. Und sie packten ihn, warfen ihn aus dem Weinberg hinaus und brachten ihn um.“ Matthäus 21,37-39 ***

 Die jüdischen Zuhörer verstanden, daß mit dem Weinberg das Volk Israel gemeint ist. Die Pacht, die Gott erwartet, sind nicht Brandopfer, sondern die Befolgung seiner Gebote, Gerechtigkeit und daß die Armen und Witwen unterstützt werden.***

 Jede Generation erkennt im Sterben des Messias den Tod der Gerechten ihrer Kriege und Verfolgungen. Offensichtlich laden sich im menschliche Zusammenleben durch Mißgunst, Eifersucht, durch Rücksichtslosigkeit und nicht zuletzt durch Angst Schuldpotentiale auf, die sich einen Ungerechten als Opfer suchen. René Girard hat in neuerer Zeit diesen Mechanismus beschrieben und dafür das Bild des ***[Sündenbocks](suendenbock.php)              gewählt. Die Notwendigkeit des Todes Jesu kommt nicht aus dem Willen Gottes, sondern aus der Bosheit des Menschen. Die klassischen Mythen wie auch die Hollywoodfilme zeigen das bleibende Interesse an der Frage, wie das Böse überwunden werden kann und die Sehnsucht nach einem Helden, der das mit dem Einsatz seiner Kräfte erreicht. Allerdings ist die Lösung, die Jesus gelebt und erlitten hat, anders als die der Sagen, seien sie von Herkules oder Anakin Skywalker in Starwars. Die Überwindung des Bösen wird im Christentum nicht durch die größere Kraft des Helden gewährleistet, sondern im Erleiden des Bösen. Das Christentum hätte keinen Antwort auf das Böse gegeben, wenn Jesu nicht bis aufs Letzte mit der Mißgunst, dem Haß, der Folter, der Gewalt konfrontiert worden wäre. ***

 Daß der Tod des Messias den Menschen zum Heil werden konnte, das ist nach durchgehender Aussage der neutestamentlichen Texte von Gott so gewirkt. Damit erhellt sich auch der Zusammenhang von Reich Gottes mit der Hinrichtung und Auferstehung des Messias: Das Reich Gottes kann nur anbrechen, wenn das Böse wirklich, d.h. von innen überwunden ist.***

 Daß das Sterben Jesu tatsächlich das Böse überwunden hat, zeigt sich an der Reaktion seiner Anhänger. Sie rächen den Tod Jesu nicht, sondern interpretieren die Hinrichtung des Messias als neues Angebot Gottes zur Vergebung der Schuld. Eine auf den ersten Blick sehr gewagte, wenn nicht abenteuerliche Auslegung. Die Betrachtung des Leidens Jesu soll den Beter in diese Haltung Jesu einüben. Viele Christen haben diesen Weg Jesu verstanden, auch wenn viele andere auch heute noch meinen, Gewalt im Namen Jesu ausüben zu dürfen.***

# **Zitate****
 Jesus kündigt sein Leiden an: Markus 8,31-33***

 Dann begann er, sie darüber zu belehren, der Menschensohn müsse vieles erleiden und von den Ältesten und den Hohenpriestern verworfen werden; er werde getötet, aber nach drei Tagen werde er auferstehen. Und er redete ganz offen darüber. Da nahm ihn Petrus beiseite und machte ihm Vorwürfe. Jesus wandte sich um, sah seine Jünger an und wies Petrus mit den Worten zurecht: Weg mit dir, Satan, geh mir aus den Augen. Denn du hast nicht im Sinn, was Gott will, sondern was die Menschen wollen.  

# **Die Predigt Johannes des Täufers nach Matthäus:**
 In jenen Tagen trat Johannes der Täufer auf und verkündete in der Wüste von Juda: Kehrt um! Denn das Himmelreich ist nahe.....***

 Die Leute von Jerusalem und ganz Judäa und aus der ganzen Jordangegend zogen zu ihm hinaus, sie bekannten ihre Sünden und ließen sich im Jordan von ihm taufen. Als Johannes sah, daß viele Pharisäer und Sadduzäer zur Taufe kamen, sagte er zu ihnen: Ihr Schlangenbrut, wer hat euch denn gelehrt, daß ihr dem kommenden Gericht entkommen könnt. Bringt Frucht hervor, die eure Umkehr zeigt.***

 Kap 3, 1-2, 5-7 

# **Das 4. Lied vom Gottesknecht:**
 Seht, mein Knecht hat Erfolg, er wird groß sein und hoch erhaben.***

 Viele haben sich über ihn entsetzt, denn er sah entstellt aus, nicht wie ein Mensch, seine Gestalt war nicht mehr die eines Menschen.***

 Jetzt aber setzt er viele Völker in Staunen, Könige müssen vor ihm verstummen.***

 Denn wovon ihnen kein Mensch je erzählt hat, das sehen sie nun;***

 was sie niemals hörten, das erfahren sie jetzt.***

 Wer hat geglaubt, was uns berichtet wurde?***

 Die Hand des Herrn - wer hat ihr Wirken erkannt?***

 Vor den Augen des Herrn wuchs er auf wie ein junger Sproß, wie der Trieb einer Wurzel aus trockenem Boden.***

 Er hatte keine schöne und edle Gestalt, und niemand von uns blickte ihn an.***

 Er sah nicht so aus, daß er unser Gefallen erregte.***

 Er wurde verachtet und von den Menschen gemieden, ***

 ein Mann voller Schmerzen, mit der Krankheit vertraut.***

 Wie ein Mensch, vor dem man das Gesicht verhüllt,***

 war er bei uns verfemt und verachtet.***

 Aber er hat unsere Krankheiten getragen ***

 und unsere Schmerzen auf sich genommen. ***

 Wir meinten, er sei vom Unheil getroffen,***

 von Gott gebeugt und geschlagen. ***

 Doch er wurde durchbohrt wegen unserer Verbrechen,***

 wegen unserer Sünden mißhandelt.***

 Weil die Strafe auf ihm lag, sind wir gerettet,***

 durch seine Wunden sind wir geheilt. ***

 Wir hatten uns alle verirrt wie die Schafe, jeder ging für sich seinen Weg.***

 Doch der Herr warf all unsere Sünden auf ihn. ***

 Er wurde geplagt und niedergedrückt, aber er tat seinen Mund nicht auf.***

 Wie ein Lamm, das man wegführt, um es zu schlachten,***

 und wie ein Schaf, das verstummt, wenn man es schert,***

 so tat auch er seinen Mund nicht auf. ***

 Durch Haft und Gericht kam er ums Leben, doch wen kümmerte sein Geschick?***

 Er wurde aus dem Land der Lebenden verstoßen***

 und wegen der Verbrechen seines Volkes getötet. ***

 Bei den Gottlosen gab man ihm sein Grab,***

 bei den Verbrechern seine Ruhestätte,***

 obwohl er kein Unrecht getan hat,***

 und aus seinem Mund kein unwahres Wort kam. ***

 Doch der Herr fand Gefallen an seinem mißhandelten (Knecht),***

 er rettete den, der sein Leben als Sühneopfer hingab.***

 Er wird lange leben und viele Nachkommen sehen.***

 Durch ihn setzt der Wille des Herrn sich durch. ***

 Nachdem er so vieles ertrug, erblickt er wieder das Licht***

 Und wird erfüllt von Erkenntnis.***

 Mein Knecht ist gerecht, darum macht er viele gerecht;***

 er nimmt ihre Schuld auf sich. ***

 Deshalb gebe ich ihm seinen Anteil unter den Großen***

 und mit den Mächtigen teilt er die Beute;***

 denn er gab sein Leben hin und wurde zu den Verbrechern gerechnet.***

 Er trug die Sünden von vielen und trat für die Schuldigen ein.	(Jesaia 52,13-53, 12) 

# **Karl Barth, evangelischer Theologe, in seinem Römerbriefkommentar:**
 Jesus, erkannt als der Christus, bestätigt, bewährt und bekräftigt alles menschliche Harren. Er ist die Mitteilung, daß es nicht der Mensch, sondern Gott in seiner Treue ist, der harrt. – Daß wir gerade in Jesus von Nazareth den Christus gefunden haben, bewährt sich darin, daß alle Kundgebungen der Treue Gottes Hinweise und Weissagungen sind auf das, was uns eben in Jesus begegnet ist. Die verborgene Kraft des Gesetzes und der Propheten (das sind die Bücher des Alten Testaments) ist der Christus, der uns in Jesus begegnet. Der Sinn aller Religion ist die Erlösung, die Zeitenwende, die Auferstehung, das Unanschauliche Gottes, das uns eben in Jesus zum Stillstehen zwingt. Der Gehalt allen menschlichen Geschehens ist die Vergebung, unter der es steht, wie sie eben von Jesus verkündigt, in ihm verkörpert ist, Daß diese Kraft, dieser Sinn, dieser Gehalt auch anderswo als in Jesus gefunden werde, das braucht uns niemand vorzuhalten, wir selbst sind es ja, die ja gerade das behaupten, gerade wir können es behaupten.  Denn daß Gott allenthalben gefunden wird, daß die Menschheit vor und nach Jesus von Gott gefunden ist, den Maßstab, an dem alles Finden Gottes, alles Vor-Gott-Gefunden-Werden als solches erkennbar wird, die Möglichkeit dieses Findens und Gefunden Werdens zu begreifen als Wahrheit ewiger Ordnung – das eben wird in Jesus erkannt und gefunden. Viele wandeln im Lichte der Erlösung, der Vergebung, der Auferstehung; daß wir sie wandeln sehen, daß wir Augen dafür haben, das verdanken wir dem Einen. In seinem Licht sehen wir das Licht. – Und daß es der Christus ist, was wir in Jesus gefunden, das bewährt sich darin, daß Jesus das letzte, das alle andern erklärende und auf den schärfsten Ausdruck bringende Wort der vom Gesetz und den Propheten bezeugten Treue Gottes ist. Die Treue Gottes ist sein Hineingehen und Verharren in der tiefsten menschlichen Fragwürdigkeit und Finsternis.***

 Der Römerbrief, 1922, S. 73***

# *
 ***Eckhard Bieger  

***©***[ www.kath.de](http://www.kath.de) 
