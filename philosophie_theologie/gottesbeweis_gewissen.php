<HTML><HEAD><TITLE>Gottesbeweis aus dem Gewissen</TITLE>
<meta name="title" content="Philosophie&amp;Theologie">
<meta name="author" content="Redaktion kath.de">
<meta name="publisher" content="kath.de">
<meta name="copyright" content="kath.de">
<meta name="description" content="">
<meta name="abstract" content="Lexikon &uuml;ber Philosophie&amp;Theologie">
<meta http-equiv="content-language" content="de">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="date" content="2006-00-01">
<meta name="robots" content="index,follow">
<meta name="revisit-after" content="10 days">
<meta name="revisit" content="after 10 days">
<meta name="DC.Title" content="Philosophie&amp;Theologie">
<meta name="DC.Creator" content="Redaktion kath.de">
<meta name="DC.Contributor" content="J�rgen Pelzer">
<meta name="DC.Rights" content="kath.de">
<meta name="DC.Publisher" content="kath.de">
<meta name="DC.Date" content="2006-00-01">
<meta name="DC.Description" content="Lexikon &uuml;ber Philosophie&amp;Theologie">
<meta name="DC.Language" content="de">
<meta name="DC.Type" content="Text">
<meta name="DC.Format" content="text/html">
<meta name="DC.Identifier" content="http://www.kath.de/lexikon/philosophie_theologie/">
<meta name="keywords" lang="de" content="Freiheit, Stimme Gottes, Gewissen, Tiefenpsychologie, Ueber-Ich, Newman, Schelling, Johann Gottlieb Fichte">
</HEAD>
<BODY bgColor=#ffffff leftMargin=6 topMargin=6 marginheight="6" marginwidth="6">
<TABLE cellSpacing=0 cellPadding=6 width="100%" border=0>
  <TBODY>
  <TR>
    <TD vAlign=top align=left width=100> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b>Philosophie&amp;Theologie</b></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td> <?php include("logo.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10"> <?php include("az.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </TD>
    <TD vAlign=top rowSpan=2>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
        <TBODY>
        <TR vAlign=top align=left>
          <TD width=8><IMG height=8 alt="" 
            src="boxtopleftcorner.gif" width=8></TD>
          <TD background=boxtop.gif><IMG height=8 alt="" 
            src="boxtop.gif" width=8></TD>
          <TD width=8><IMG height=8 alt="" 
            src="boxtoprightcorner.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxtopleft.gif><IMG height=8 alt="" 
            src="boxtopleft.gif" width=8></TD>
          <TD bgColor=#e2e2e2> 
            <H1><font face="Arial, Helvetica, sans-serif">Gottesbeweis aus
                dem Gewissen</font></H1>
          </TD>
          <TD background=boxtopright.gif><IMG height=8 
            alt="" src="boxtopright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=13 alt="" src="boxdividerleft.gif" 
            width=8></TD>
          <TD background=boxdivider.gif><IMG height=13 
            alt="" src="boxdivider.gif" width=8></TD>
          <TD><IMG height=13 alt="" 
            src="boxdividerright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxleft.gif><IMG height=8 alt="" 
            src="boxleft.gif" width=8></TD>
          <TD class=L12>
            <P><STRONG><font face="Arial, Helvetica, sans-serif">Die
                  Stimme, die im Inneren des Menschen spricht</font></STRONG></P>
            <P><font face="Arial, Helvetica, sans-serif">Im Gewissen werden wir
                von innen angesprochen. Es spricht allerdings nicht unsere Stimme
                zu uns, sondern die der sittlichen Verpflichtung.
              Das Gewissen stellt an uns Forderungen, in bestimmter Weise zu
              handeln. Diese Forderungen beziehen sich nicht darauf, da&szlig; wir
              unseren Vorteil optimal ausnutzen, sondern widerstreiten gerade
              dem, was wir als vorteilhaft oder angenehm erstreben. Im Gewissen
              meldet sich eine Norm, die uns auf andere hin und auf deren Vorteil
              orientiert. Das Gewissen fordert uns unbedingt, also &#8222;Ohne
              Wenn und Aber&#8220;. Mit dem Gewissen ist auch erst die Voraussetzung
              gegeben, da&szlig; wir uns schuldig f&uuml;hlen k&ouml;nnen. Denn
              eigentlich m&uuml;&szlig;ten wir mit uns zufrieden sein, wenn wir
              z.B. jemanden erfolgreich betrogen haben. Wir waren ja erfolgreich.
              Wenn wir aber trotz des &auml;u&szlig;eren Erfolges nicht zufrieden
              sind, weil wir uns im Unrecht f&uuml;hlen, dann beurteilen wir
              unsere Handlung nicht nur unter dem Gesichtspunkt des Erfolges,
              sondern auch danach, ob sie ethisch gerechtfertigt ist. Dieses
              Wissen um ethische Werte konkretisiert sich im Gewissen auf die
              Beurteilung einzelner Handlungen. Wir kennen nicht nur die sittlichen
              Vorschriften, wie sie z.B. in den 10 Geboten zusammengestellt sind,
              sondern &#8222;wissen&#8220; auch um den ethischen Wert bzw. Unwert
              einer Tat. Das Gewissen hat eine urteilende Funktion, es beurteilt
              unsere &Uuml;berlegungen, wenn wir etwas vorhaben und f&auml;llt
              auch ein Urteil &uuml;ber die durchgef&uuml;hrte Handlung. Da uns
              der Spruch des Gewissens wie von au&szlig;en fordert und uns auch
              keine Wahl l&auml;&szlig;t, weil er uns unbedingt verpflichtet,
              sehen nicht wenige Philosophen im Gewissen die am deutlichsten
              erkennbare Gegenwart Gottes, die dem Menschen zug&auml;nglich ist. </font></P>
            <p><font face="Arial, Helvetica, sans-serif">Johann Gottlieb Fichte<br>
&#8222;
              Die Stimme des Gewissens, die jedem seine besondere Pflicht auflegt,
              ist der Strahl, an welchem wir aus dem Unendlichen ausgehen, und
              als einzelne und besondere Wesen hingestellt werden; sie zieht
              die Grenzen unserer Pers&ouml;nlichkeit; sie also ist unserer wahrer
              Urbestandteil, der Grund und Stoff alles Lebens, welches wir leben.
              Die absolute Freiheit des Willens, die wir gleichfalls aus dem
              Unendlichen mit herab nehmen in die Welt der Zeit, ist als Prinzip
              dieses unseres Lebens.&#8220; <br>
              Die Bestimmung des Menschen, Berlin 1800, S. 139f</font></p>
            <p><font face="Arial, Helvetica, sans-serif">&#8222;Wir sind in seiner Hand, und bleiben in derselben, und
              niemand kann uns daraus rei&szlig;en. Wir sind ewig, weil Er es
              ist. Erhabener lebendiger Wille, den kein Name nennt und kein Begriff
              umfa&szlig;t, wohl darf ich mein Gem&uuml;t zu dir erheben: denn
              du und ich sind nicht getrennt. Deine Stimme ert&ouml;nt in mir,
              die meinige t&ouml;nt in dir wieder; und alle meine Gedanken, wenn
              sie nur wahr und gut sind, sind in dir gedacht. &#8211; In dir,
              dem Unbegreiflichen, werde ich mir selbst, und wird mir die Welt
              vollkommen begreiflich, alle R&auml;tsel meines Daseins werden
              gel&ouml;st, und die vollendete Harmonie entsteht in meinem Geiste.&#8220; ebd.
              S. 143f; zitiert nach Philosophische Bibliothek, Hamburg 2000</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Friedrich Wilhelm Schelling<br>
&#8222;
              Wir haben in uns einen einzigen offenen Punkt, durch den der Himmel
              hereinscheint. Dieser ist unser Herz oder, richtiger zu reden,
              unser Gewissen. Wir finden in diesem ein Gesetz und eine Bestimmung,
              die nicht von dieser Welt sein kann, mit der sie vielmehr gew&ouml;hnlich
              im Kampf ist, und so dient es uns zu dem Unterpfand einer h&ouml;heren
              Welt, und erhebt den, der ihm folgen gelernt hat, zu dem trostreichen
              Gedanken der Unsterblichkeit.&#8220;<br>
&Uuml;
              ber den Zusammenhang der Natur mit der Geisterwelt, 1810, S&auml;mtliche
              Werke Bd. 1,S. 17, zitiert nach Philosophische Bibliothek, Hamburg
              1992</font></p>
            <p><font face="Arial, Helvetica, sans-serif">  John Henry Newman hat in seien Schrift &#8222;An Essay in Aid of
              a Grammar of Assent&#8220; aus dem Jahre 1870 am deutlichsten die
              Stimme des Gewissens als Hinweis auf Gott herausgearbeitet; deutsch: &#8222;Entwurf
              einer Zustimmungslehre&#8220;, Mainz 1961<br>
&#8222;
              Das Gewissen aber ruht nicht in sich selbst, sondern langt in vager
              Weise vor zu etwas jenseits seiner selbst und erkennt undeutlich
              eine Billigung seiner Entscheidungen, die h&ouml;her ist als es
              selbst und bewiesen ist in jenem scharfen Sinn f&uuml;r Verpflichtung
              und Verantwortung, der sie tr&auml;gt. Daher kommt es, da&szlig; wir
              gewohnt sind, vom Gewissen zu sprechen als von einer Stimme &#8211; ein
              Ausdruck, den auf den Sinn f&uuml;r das Sch&ouml;ne anzuwenden,
              uns niemals einfallen w&uuml;rde. Und &uuml;berdies ist es eine
              Stimme oder das Echo einer Stimme, herrisch und n&ouml;tigend wie
              kein anderer Befehl im ganzen Bereich unserer Erfahrung.&#8220; S.75<br>
&#8222;
              Wenn wir, wie es ja der Fall ist, uns verantwortlich f&uuml;hlen,
              besch&auml;mt sind, erschreckt sind bei einer Verfehlung gegen
              die Stimme des Gewissens, so schlie&szlig;t das ein, da&szlig; hier
              Einer ist, dem wir verantwortlich sind; vor dem wir besch&auml;mt
              sind, dessen Anspr&uuml;che an uns wir f&uuml;rchten....<br>
              Wenn die Ursachen dieser Gem&uuml;tsbewegungen nicht dieser sichtbaren
              Welt angeh&ouml;ren, so mu&szlig; der Gegensand, auf den seine
              Wahrnehmung gerichtet ist, &uuml;bernat&uuml;rlich und g&ouml;ttlich
              sein. So ist das Ph&auml;nomen des Gewissens als das eines Befehls
              dazu geeignet, dem Geist das Bild eines h&ouml;chsten Herrschers
              einzupr&auml;gen, eines Richters, heilig, gerecht, m&auml;chtig,
              allsehend, vergeltend. Es ist das sch&ouml;pferische Prinzip der
              Religion, wie der Sinn f&uuml;r das Sittliche das Prinzip der Ethik
              ist.&#8220; S. 77</font></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Die Einw&auml;nde
                  der Tiefenpsychologe</strong><br>
  So unbestreitbar das Gewissen im einzelnen Menschen spricht, so
                wenig deutlich kann es sein, da&szlig; im Gewissen sich die Stimme
                Gottes meldet. Siegmund Freud ist bei der Erforschung des Unbewu&szlig;ten
                darauf gesto&szlig;en, da&szlig; Menschen in sich st&auml;ndig
                die Stimme ihrer Eltern oder ihrer Umwelt h&ouml;ren. Freud ordnet
                das Gewissen daher nicht dem Personkern des Menschen zu, sondern
                seinem &Uuml;ber-Ich. Das &Uuml;ber-Ich ist eine Instanz im Menschen,
                aber sie ist von au&szlig;en verinnerlicht. Der Mensch h&ouml;rt
                die Stimmen, die ihm von au&szlig;en Vorschriften machen, ihn
                in eine bestimmte Richtung dr&auml;ngen wollen, in sich sprechen.
                Da diese Stimmen Schuldgef&uuml;hle einfl&ouml;&szlig;en, sind
                die Anh&auml;nger von Freud sofort skeptisch, wenn es um Schuldgef&uuml;hle
                geht. Diese leiten sie erst einmal vom &Uuml;ber-Ich her, also
                von einer Instanz, die der Mensch zu seinem Schaden verinnerlicht
                hat. <br>
                Da&szlig; Gewissen aber nicht einfach die stimme der Eltern ist,
              zeigen die Menschen, die sich gegen die Meinungstrends ihrer Umwelt
              ein eigenes Urteil erhalten haben und sich z.B. in der Zeit des
              Nationalsozialismus der Rassenideologie widersetzt haben. Nicht
              wenige haben ihren Widerstand mit dem Leben bezahlt. <br>
              Es zeigt sich, da&szlig; eine Stimme, die der einzelne in sich
              h&ouml;rt, nicht unbesehen mit der Stimme des Gewissens identifiziert
              werden kann. Das ist zudem eine alte Erfahrung der christlichen
              Spiritualit&auml;t, da&szlig; der Mensch verschiedene Stimmen in
              seinem Inneren h&ouml;rt und manche Stimmen Einfl&uuml;sterungen
              des b&ouml;sen Geistes sind. Deshalb geh&ouml;rt es zum Grundbestand
              einer christlichen Spiritualit&auml;t, die Unterscheidung der Geister
              einzu&uuml;ben. <br>
              Da&szlig; neben der Stimme des Gewissens sich auch andere im Menschen
              melden, zeigt weiter die Notwendigkeit, eine &auml;u&szlig;ere
              Norm zu finden, an der der einzelne &uuml;berpr&uuml;fen kann,
              ob eine Eingebung vom guten oder b&ouml;sen Geist kommt. Die sittlichen
              Werte, die in den 10 Geboten oder in den Menschenrechten zusammengestellt
              sind, dienen als ein solcher Ma&szlig;stab. Der Vorteil besteht
              darin, da&szlig; diese Normen nicht allein intuitiv erfa&szlig;t
              und begr&uuml;ndet werden k&ouml;nnen, sondern der Argumentation
              zug&auml;nglich sind. Diese Argumentation leistet die philosophische
              Ethik.<br>
              Wenn auf der einen Seite die Stimme Gottes im Gewissen zu vernehmen
              ist, auf der anderen Seite aber der Mensch auch andere Stimmen
              h&ouml;rt, zeigt sich der Gottesbeweis, der aus dem Gewissen erschlossen
              wird, als ein gangbarer Weg, der jedoch der Erg&auml;nzung bedarf.
              Diese findet sich in der Analyse der Freiheit. Da das Gewissen
              nur auf Grund der Freiheit &uuml;berhaupt m&ouml;glich ist, ist
              ein enger Zusammenhang von Gewissen und <a href="gottesbeweis_freiheit.php">Freiheit</a> gegeben, wenn
              der Mensch sich seiner Herkunft von Gott vergewissern will.</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Eckhard Bieger</font><br>
              <br>&copy;
              <a href="http://www.kath.de"><font face="Arial, Helvetica, sans-serif">www.kath.de</font></a></p>
            </TD>
          <TD background=boxright.gif><IMG height=8 alt="" 
            src="boxright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=8 alt="" src="boxbottomleft.gif" 
            width=8></TD>
          <TD background=boxbottom.gif><IMG height=8 alt="" 
            src="boxbottom.gif" width=8></TD>
          <TD><IMG height=8 alt="" src="boxbottomright.gif" 
            width=8></TD></TR></TBODY></TABLE></TD><td style='vertical-align:top;'>
 <script type="text/javascript"><!--
        google_ad_client = "pub-9537752113242914";
        google_ad_width = 336;
        google_ad_height = 280;
        google_ad_format = "336x280_as";
        google_ad_type = "text_image";
        //2006-10-23: Lexikon Symbole
        google_ad_channel = "8495253357";
        //--></script>
        <script type="text/javascript"
          src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
        </script>
</td>
</TR>
  <TR>
    <TD vAlign=top align=left>&nbsp; </TD>
  </TR></TBODY></TABLE></BODY></HTML>
