---
title: Menschensohn
author: 
tags: 
created_at: 
images: []
---


# **Ben Adam - Der Menschensohn, der am Ende der Zeiten wiederkehren wird**
# ******
 Jesus hat das Reich Gottes verkündet. Dieses Reich wird endgültig den Frieden beinhalten. Es wird von Gott geschaffen und ist damit frei von den menschlichen Unzulänglichkeiten. Durch seine Predigt und die Wundertaten hat Jesus seine Jünger überzeugt, daß das Reich mit ihm gekommen ist. Sie sahen in Jesus den ***[Messias](messias_christus.php), den Gesalbten, Christus. Es gibt im Neuen Testament danbeben noch den Titel "Menschensohn" für Jesus. In den Gleichnissen vom Samenkorn und Sauerteig weist er darauf hin, daß dieses Reich im Verborgenen wächst. Irgendwann muß es aber zum Durchbruch kommen. Die Offenbarung des Johannes bestätigt diese Erwartung. Dieser Text und andere Stellen des Neuen Testaments sprechen von dem Menschensohn, der am Ende der Zeiten das Reich Gottes durchsetzen wird – mit dem die Geschichte abschließenden Gericht. Wer ist dieser Menschensohn und wird es Jesus Christus sein?   

# **Endzeiterwartung der ersten Christen****
 Die Christen der ersten Generation erwarteten noch zu ihren Lebzeiten die Wiederkunft Christi und damit die Vollendung der Geschichte. Paulus. Im 1. Korintherbrief schreibt er: „Wir werden nicht alle entschlafen.“ (Kap. 15,51)***

 Was später individuell für den eigenen Tod erhofft wurde, daß man ihn wachen Auges erfährt und ihm bewußt entgegengeht, war anfangs die Vorbereitung auf den „Tag des Herrn“, eben der Wiederkunft Christi. Die Christen sollen ihn nüchtern und wach erwarten, so daß sie nicht überrascht werden. In der Liturgie der frühen Kirche wurde die Wiederkunft mit dem Ruf Maranatha – Herr, komme bald, herbei gebetet. Der ***[Advent](http://www.kath.de/Kirchenjahr/advent.php) ist nicht nur die Vorbereitung auf das Geburtsfest Jesu, sondern hat zu Beginn die engültige Wiederkehr Jesu zum Thema. 

# **Jesus bezeichnete sich wahrscheinlich selbst als Menschensohn**
# ****Dieser Erwartung, daß Gott sein Reich aufrichten und die Menschen aus dem Hin- und Herr der Geschichte herausholen wird, bestimmte zur Zeit Jesu das Bewußtsein der Menschen. Man erwartete das Ende der Welt. Der Prophet Daniel hatte das in großen Visionen beschrieben, in der von einem Menschensohn die Rede ist. Diesem Menschensohn wird die Herrschaft über das Reich Gottes übergeben. **
 Für die Christen besteht kein Zweifel daran, daß Christus, der von den Toten Auferstandene, der ist, der zum Endgericht wieder kommt und mit dem Menschensohn, wie Daniel ihn in seiner Vision gesehen hat, identisch ist. Das findet sich z.B. in den Weltgerichtsszenen über den Portalen mittelalterlicher Kathedralen.***

 In den Evangelien bezieht sich Jesus auf das Kommen des Menschensohns und erklärt sich selbst auch mit dem Titel Menschensohn zum Herrn über den Sabbat. Die Bibelwissenschaftler diskutieren, ob Jesus sich selbst als Menschensohn bezeichnet hat oder ihm der Titel “Menschensohn“ von den Evangelisten in den Mund gelegt wurde. Da der Titel in den Apostelbriefen nicht gebraucht wird, die ja bereits die frühchristliche Liturgie widerspiegeln, kann man annehmen, daß die Evangelien sich auf die Aussagen Jesu beziehen. 

# **Auch Jesus erwartete das Ende der Welt****
 Jesus selbst kündigt das Ende der Welt an, läßt aber den Zeitpunkt offen:***

 Als Jesus von den Pharisäern gefragt wurde, wann das Reich Gottes komme, antworte er: "Das Reich kommt nicht so, daß man es an äußeren Zeichen erkennen könnte. Man kann auch nicht sagen: Seht, hier ist es, oder: Dort ist es. Denn: Das Reich Gottes ist schon mitten unter euch. ***

 Er sagte zu den Jüngern: Es wird eine Zeit kommen, in der ihr auch danach sehnt, auch nur einen von den Tagen des Menschensohnes zu erleben; aber ihr werdet ihn nicht erleben. Und wenn man zu euch sagt: Dort ist er! Hier ist er!, so geht nicht hin und lauft nicht hinterher. Denn wie der Blitz von einem Ende des Himmels bis zum anderen leuchtet, so wird der Menschensohn an seinem Tag erscheinen."***

 Lukas 17,20-24 

# **Als seine Jünger am Sabbat Ähren zupfen und die Pharisäer sich darüber beschweren, daß sie damit die strengen Regelungen für die ruhe an diesem heiligen Tag brechen, antwortet Jesus:**
 "Habt ihr nie gelesen, was David getan hat, als er und seine Begleiter hungrig waren und nichts zu essen hatten – wie er zur Zeit des Hohenpriesters Abjatar in das Haus Gottes ging und die heiligen Brote aß, die außer den Priestern niemand essen darf, auch seinen Begleitern davon gab? Und Jesus fügte hinzu: Der Sabbat ist für den Menschen da, nicht der Mensch für den Sabbat. Deshalb ist auch der Menschensohn Herr auch über den Sabbat."***

 Markus 2, 25-28 

# **Daß das Reich Gottes schon angebrochen ist und der Menschensohn in Jesus aufgetreten ist, zeit sich in folgenden Aussagen Jesu:**
 "Selig seid ihr, wenn euch die Menschen hassen und aus ihrer Gemeinschaft ausschließen, wenn sie euch beschimpfen und euch in Verruf bringen um des Menschensohnes willen. Freut euch und jauchzt an jenem Tag; euer Lohn im Himmel wird groß sein.***

 Lukas 6,22-23 

# **In der dritten Person sind verschiedene Aussagen gefaßt, wenn Jesus über sein Leiden spricht:**
 " ***Dann begann er, sie darüber zu belehren, der Menschensohn müsse vieles erleiden und von den Ältesten, den Hohenpriestern und den Schriftgelehrten verworfen werden; er werde getötet, aber nach drei Tagen werde er auferstehen."***

 Markus 8,31 

# **Am Ölberg weckt er die Jünger mit folgenden Worten:**
 "Schlaft ihr immer noch und ruht euch aus? Es ist genug. Die Stunde ist gekommen; jetzt wird der Menschensohn den Sündern ausgeliefert. Steht auf, wir wollen gehen. Seht, der Verräter, der mich ausliefert, ist da."***

 Markus 14, 41-42 

# **Provokativ identifiziert sich Jesus in seinem Verhör vor dem Hohen Rat mit dem Menschensohn:**
 "Da wandte sich der Hohepriester nochmals an ihn und fragte: Bist du der Messias, der Sohn des Hochgelobten?***

 Jesus sagte: Ich bin es. Und ihr werdet den Menschensohn zur Rechten der Macht sitzen und mit den Wolken des Himmels kommen sehen.***

 Da zerriß der Hohepriester sein Gewand und rief: Wozu brauchen wir noch Zeugen? Ihr habe die Gotteslästerung gehört."***

 Markus 14, 60-63 

# **Text: Eckhard Bieger S.J.*****
©***[ ***www.kath.de](http://www.kath.de) 
