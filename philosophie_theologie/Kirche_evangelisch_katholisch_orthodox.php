<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title>Text</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/kaltefleiter.css" title="fonts">
</head>
<body bgcolor="#ffffff" leftmargin="6" topmargin="6" marginwidth="6" marginheight="6">
<table width="100%" border="0" cellspacing="0" cellpadding="6">
  <tr>
    <td width="100" align="left" valign="top" height="3"> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxtop.gif"><img src="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxtopleft.gif"><img src="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b><font face="Arial, Helvetica, sans-serif">Philosophie&amp;Theologie</font></b></td>
          <td background="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxtopright.gif"><img src="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxdivider.gif"><img src="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxleft.gif"><img src="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxleft.gif" width="8" height="8" alt=""></td>
          <td> <?php include("logo.html"); ?> </td>
          <td background="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxright.gif"><img src="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxbottom.gif"><img src="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxtop.gif"><img src="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxtopleft.gif"><img src="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><font face="Arial, Helvetica, sans-serif"><strong>Inhaltsverzeichnis 
            </strong></font></td>
          <td background="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxtopright.gif"><img src="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxdivider.gif"><img src="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxleft.gif"><img src="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10"> <?php include("az.html"); ?> </td>
          <td background="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxright.gif"><img src="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxbottom.gif"><img src="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
    <td rowspan="2" valign="top">
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td background="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxtop.gif" colspan="2"><img src="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxtopleft.gif" width="8"><img src="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2" colspan="2"> 
            <h1><font face="Arial, Helvetica, sans-serif">Kirche,
            evangelisch - katholisch - orthodox</font></h1>
          </td>
          <td background="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxtopright.gif" width="8"><img src="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8"><img src="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxdivider.gif" colspan="2"><img src="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxdivider.gif" alt="" width="8" height="13"></td>
          <td width="8"><img src="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxleft.gif" width="8"><img src="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxleft.gif" width="8" height="8" alt=""></td>
          <td class="L12" width="70%">		  <p><font face="Arial, Helvetica, sans-serif">Sollen
                die Kirchen wieder zusammenwachsen und sich als eine verstehen?
                Oder sind die
		        verschiedenen Konfessionen auf Dauer angelegt? Da
              sich keine Verst&auml;ndigung zwischen dem Kirchenverst&auml;ndnis
              abzeichnet, gibt es immer mehr Stimmen, die die Konfessionen in
              einem verst&auml;ndnisvollen Nebeneinander sehen. Man bek&auml;mpft
              sich nicht mehr, aber man vereinigt sich auch nicht. Diese Konzeption
              von &Ouml;kumene entspricht aber nicht dem ausdr&uuml;cklichen
              Auftrag Jesu. Er mahnt seine J&uuml;nger, die Einheit zu bewahren.
              Aus dem Neuen Testament kann nicht abgelesen werden, dass es mehrere
              Kirchen geben soll. Wie kommen die Kirchen aber zu einer gr&ouml;&szlig;eren
              N&auml;he, wie werden sie wieder &#8222;eine Kirche&#8220;, wenn
              sie ein so verschiedenes Verst&auml;ndnis von Kirche haben? Wenn
              die Bibel das Basisdokument aller Kirchen ist, dann muss man in
            der Bibel eine Antwort suchen. </font></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Die evangelische Position: Kirche kommt aus dem Glauben</strong><br>
  Die evangelische Theologie m&uuml;sste eigentlich berufen sein,
              aus der Bibel heraus ein Verst&auml;ndnis von Kirche vorzulegen,
              auf das sich die katholische wie die orthodoxe Kirche hinbewegen
              m&uuml;ssten. Denn die Reformation entstand aus dem Grundimpuls,
              die Kirche wieder auf das Neue Testament zur&uuml;ckzuf&uuml;hren.
              Da die Reformation aus der Spannung mit einer stark institutionalisierten
              sp&auml;tmittelalterlichen Kirche entstanden ist, wird sie bis
              heute von einer gro&szlig;en Skepsis gegen&uuml;ber allem Institutionellen
              bestimmt. Luther betont, gegen die Institution Kirche, den Glauben
              des einzelnen. Dieser Glaube ist etwas Inneres, das erst einmal
              nichts mit der Kirche zu tun zu haben scheint. Von der Institution
              kann nur etwas &Auml;u&szlig;eres kommen, n&auml;mlich die Verk&uuml;ndigung
              des Wortes Gottes und die Sakramente. So begr&uuml;ndet sich f&uuml;r
              den Protestantismus Kirche in der Notwendigkeit, dass das Wort
              Gottes verk&uuml;ndet und die Sakramente gespendet werden m&uuml;ssen.
              Die eigentliche Kirche entsteht im Inneren der Menschen, wenn sie
              zum Glauben kommen. In einem neueren Dokument stellt die Evangelische
              Kirche in Deutschland (EKD) fest, dass das Kirchenverst&auml;ndnis
              aus dem Glaubensverst&auml;ndnis folgt. Je nachdem, wie das Zusammenspiel
              von menschlicher Suche nach Erl&ouml;sung und dem Zuspruch Gottes
              interpretiert werde, entstehe die eine oder andere Konfession.
              Die evangelische Tradition lehne ausdr&uuml;cklich ab, dass die
              Kirche &#8222;eine &#8222;Instanz der Heilsvermittlung&#8220; sei.
              (s. Zitat Nr. ) Die Kirche ist etwas von Menschen zu Gestaltendes,
              die eigentliche Heilsvermittlung vollzieht sich im Inneren der
              zum Glauben Gekommenen. Diese bilden dann die wahre, jedoch unsichtbare
              Kirche. Die katholische Kirche setzt dem entgegen, dass die Kirche
              sehr wohl in der Verk&uuml;ndigung des Wortes und der Spendung
              der Sakramente &#8222;Heilsanstalt&#8220; sei und beansprucht,
              dass Gott durch die Kirche wirkt. Denn nicht nur die Glaubenden
              sind geheiligt, auch die Kirche ist bei aller S&uuml;ndhaftigkeit
              eine &#8222;heilige Institution&#8220;. Was sagt aber die Bibel
              selbst.</font></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Kirche ist Volk Gottes</strong><br>
  Die Kirche ist nicht einfach von den Aposteln gegr&uuml;ndet, weil
              es nach dem Tod Jesu irgendwie weitergehen musste. Die Kirche kommt
              vom Volk Israel her. Gott hat sich dieses Volk erw&auml;hlt und
              Jesus war zu diesem, Gottes Volk, gesandt. Auch wenn er von seinem
              Volk abgelehnt wurde, er hat seine Kirche auf dieses Volk aufgebaut.
              Die 12 Apostel sind nicht nur alle Juden gewesen, sie repr&auml;sentieren
              in ihrer Zw&ouml;lfzahl die 12 St&auml;mme des Volkes Israel. Paulus
              sagt deshalb, dass die Nicht-Juden, die zur Kirche gekommen sind,
              wie ein Ast in den Baum des Volkes Israel eingepfropft wurden.
              Wer also zum Glauben an Jesus als seinen Erl&ouml;ser kommt, der
              wird durch die Taufe in dieses Volk Gottes eingegliedert.</font></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Gott schlie&szlig;t
                einen Bund</strong><br>
  Gott hat das j&uuml;dische Volk nicht einfach vereinnahmt, sondern
              mit ihm einen Bund geschlossen. Ein Bund jedoch setzt das Einverst&auml;ndnis
              der Bundespartner voraus. Basis des Bundes ist das Gesetz, auf
              das die Mitglieder des Bundes sich verpflichten. Dieser Bund, den
              Gott am Sinai mit seinem Volk geschlossen hat, wird immer wieder
              erneuert. Jesus erneuert diesen Bund. Im Abendsmahlssaal schlie&szlig;t
              Jesus mit dem Volk Gottes, repr&auml;sentiert durch die 12 Apostel,
              einen Neuen Bund. Dieser Bundesschluss wird im Zusammenhang mit
              dem Wort Jesu &uuml;ber den Wein &uuml;berliefert.</font></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Leib Christi</strong><br>
  Die Kirche ist eng mit Jesus verbunden. Paulus gebraucht daf&uuml;r
              das Bild des Leibes. Jesus ist das Haupt und leitet die Kirche,
              nicht zuerst durch den Papst und die Bisch&ouml;fe, sondern durch
              den Heiligen Geist. In Bezug auf das Haupt sind die Gl&auml;ubigen,
              nicht nur die Bisch&ouml;fe und Priester, &#8222;Glieder des Leibes
              Christi.&#8220;<br>
              Diese Verbundenheit mit Jesus Christus wird durch die Feier der
              Eucharistie dargestellt, vertieft und immer wieder erneuert. W&auml;hrend
              die anderen Sakramente, Taufe, Bu&szlig;sakrament, Firmung, Hochzeit,
              Weihe, Krankensalbung auf den einzelnen zielen, ist die Gemeinschaft
              der Glaubenden, ist die Kirche Thema der Eucharistie, denn die
              einzelnen kommen in dem Mahl zusammen. Die Eucharistie zeigt in
              besonderer Weise, dass die Kirche erst einmal mit Christus verbunden
              sein und bleiben muss. Erst dann entfaltet sie sich in ihre verschiedenen
              Aufgaben hinein. Die Eucharistie ist das Sakrament der Einheit.</font></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Die Kirche entsteht aus dem Geist</strong><br>
  Jesus hatte die zw&ouml;lf J&uuml;nger stellvertretend f&uuml;r
              die 12 St&auml;mme Israel ausgew&auml;hlt und damit das Fundament
              der Kirche gelegt. &#8222;Wirklich&#8220; ist die Kirche aber erst
              am Pfingstfest geworden, denn erst der Geist Gottes inspiriert
              die Apostel und gibt ihnen die Kraft, furchtlos von Jesus und seiner
              Auferstehung Zeugnis zu geben. Deshalb gilt Pfingsten als der eigentliche
              Geburtstag der Kirche.</font></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Die Kirche ist apostolisch</strong><br>
  Die Apostel haben im Auftrag Jesu am Pfingstfest begonnen, sein
                Evangelium zu verk&uuml;nden. Sie werden Zeugen genannt, vor
                allem, weil ihnen der Auferstandene erschienen ist. Sie bezeugen,
                dass er, obwohl am Kreuz hingerichtet, lebt und vom Himmel aus
                herrscht. Ihr Zeugnis ist Basis der Kirche. Im Neuen Testament
                hat sich ihr Zeugnis, vor allem in den vier Evangelien, niedergeschlagen.
                Deshalb ist das Neue Testament das Basisdokument der Kirche.
                Auch wenn die Bisch&ouml;fe von den Aposteln als ihre Nachfolger
                eingesetzt sind, k&ouml;nnen sie die urspr&uuml;ngliche Verk&uuml;ndigung
                der Apostel nicht &uuml;berholen, korrigieren. Das tut der Islam.
                Er kommt aus j&uuml;disch-christlichen Wurzeln und korrigiert
                diese Tradition, indem er in Jesus nicht mehr den Sohn Gottes
                erkennt, sondern ihn zu einem Propheten herabstuft. Auch Mohamed
                ist nur ein Prophet. <br>
              Zum Neuen Testament ist zu sagen, dass es die &Uuml;berlieferung
              der Apostel enth&auml;lt, die die Predigt Jesu weiter gegeben haben.
              Die Evangelien sind also von Menschen geschrieben, unter dem Einfluss
              des Heiligen Geistes. Anders der Koran, der im Islam als direktes
              Wort Gottes gesehen wird, das nur in Arabisch richtig verstanden
              werden kann. Die Bisch&ouml;fe wie die Theologen und &uuml;berhaupt
              alle Gl&auml;ubigen sind an die &Uuml;berlieferung der Apostel
              gebunden. Den Bisch&ouml;fen als Nachfolgern der Apostel kommt
              eine Aufgabe zu, die sie nicht delegieren k&ouml;nnen: sie sollen
              f&uuml;r das Wort Gottes Zeugnis geben. Deshalb werden Bisch&ouml;fe
              h&auml;ufig mit einem Buch in der Hand dargestellt. Diese apostolische,
              bisch&ouml;fliche Grundstruktur ist bereits im Neuen Testament
              belegt. Es sei nur auf die Paulussch&uuml;ler Timotheus und Titus
              hingewiesen, die Paulus in Ephesus bzw. f&uuml;r Kreta einsetzt
              und zu deren Auftrag es geh&ouml;rt, &#8222;&Auml;lteste&#8220; als
              Gemeindeleiter durch Handauflegung einzusetzen. In dieser apostolischen
              Grundstruktur liegt der entscheidende theologische Dissens zwischen
              Katholiken und Protestanten.</font></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Kirche in der Theologie der Orthodoxie</strong><br>
  Das Programm &#8222;Orthodox&#8220;, das die Kirchen des Ostens
              in ihrem Namen haben, l&auml;sst sie an der urspr&uuml;nglichen
              Struktur der Kirche noch entschiedener festhalten. Da die Orthodoxie
              zwar Patriarchen kennt, den von Konstantinopel, den von Alexandrien,
              den Papst der Kopten, den von Patriarchen von Moskau, setzt sie
              mehr als die katholische Kirche auf das Bischofsamt, denn die Patriarchen
              haben in den orthodoxen Kirchen nicht die bestimmende Funktion
              wie der Papst, der &#8222;Patriarch des Westens&#8220;, in der
              katholischen. Die Orthodoxie ist mit der katholischen Kirche &uuml;ber
              eine theologische Dialogkommission in st&auml;ndigem Gespr&auml;ch.
              Hier wurde &Uuml;bereinstimmung in der Frage der apostolischen
              Grundstruktur der Kirche gefunden, der Wille zu einer einzigen
              Kirche Jesu Christi bekundet. Strittig ist die Rolle des Papstes,
              der von der Orthodoxie als &#8222;Erster&#8220; unter den Patriarchen,
              jedoch nicht mit den Rechten, die er in der katholischen Kirche
              aus&uuml;bt, anerkannt wird. (Zitate aus einem Dokument der Dialogkommission
              s.u.)</font></p>
            <p><font face="Arial, Helvetica, sans-serif"><br>
                <strong>Zitate</strong></font></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Evangelische Kirche</strong><br>
  Die Spannung zwischen dem Glauben als menschlichem Suchen und dem
                Vertrauen auf das Geschenk g&ouml;ttlicher G&uuml;te pr&auml;gt
                jedes christliche Kirchenverst&auml;ndnis. Unterschiedliche konfessionelle
                Profile ergeben sich daraus, welche Konsequenzen aus dieser Spannung
                f&uuml;r die sichtbare, von Menschen zu gestaltende Kirche gezogen
                werden. Im evangelischen Verst&auml;ndnis wird diese Spannung
                nicht in ein Verst&auml;ndnis der Kirche als Instanz der Heilsvermittlung
                hinein aufgel&ouml;st, die selbst als Institution Gottes Gegenwart
                und G&uuml;te verb&uuml;rgt.<br>
              Kirche der Freiheit, S. 32 http://www.ekd.de/download/kirche-der-freiheit.pdf</font></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Die Kirche kommt vom Volk Israel</strong><br>
  Paulus vergleicht im Brief an die R&ouml;mer Israel mit einem &Ouml;lbaum,
              in den die aus dem Heidentum kommenden Christen eingepfropft sind:<br>
              Euch, den Heiden, sage ich: Gerade als Apostel der Heiden preise
              ich meinen Dienst, weil ich hoffe, die Angeh&ouml;rigen meines
              Volkes eifers&uuml;chtig zu machen und wenigstens einige von ihnen
              zu retten. Denn wenn schon ihre Verwerfung f&uuml;r die Welt Vers&ouml;hnung
              gebracht hat, dann wird ihre Annahme nichts anderes sein als Leben
              aus dem Tod. Ist die Erstlingsgabe vom Teig heilig, so ist es auch
              der ganze Teig; ist die Wurzel heilig, so sind es auch die Zweige.
              Wenn aber einige Zweige herausgebrochen wurden und wenn du als
              Zweig vom wilden &Ouml;lbaum in den edlen &Ouml;lbaum eingepfropft
              wurdest und damit Anteil erhieltest an der Kraft seiner Wurzel,
              so erhebe dich nicht &uuml;ber die anderen Zweige. Wenn du es aber
              tust, sollst du wissen: Nicht du tr&auml;gst die Wurzel, sondern
              die Wurzel tr&auml;gt dich. Nun wirst du sagen: Die Zweige wurden
              doch herausgebrochen, damit ich eingepfropft werde. Gewiss, sie
              wurden herausgebrochen, weil sie nicht glaubten. Du aber stehst
              an ihrer Stelle, weil du glaubst. Sei daher nicht &uuml;berheblich,
              sondern f&uuml;rchte dich! Hat Gott die Zweige, die von Natur zum
              edlen Baum geh&ouml;ren, nicht verschont, so wird er auch dich
              nicht verschonen. Erkenne die G&uuml;te Gottes und seine Strenge!
              Die Strenge gegen jene, die gefallen sind, Gottes G&uuml;te aber
              gegen dich, sofern du in seiner G&uuml;te bleibst; sonst wirst
              auch du herausgehauen werden. Ebenso werden auch jene, wenn sie
              nicht am Unglauben fest halten, wieder eingepfropft werden; denn
              Gott hat die Macht, sie wieder einzupfropfen.<br>
              Wenn du aus dem von Natur wilden &Ouml;lbaum herausgehauen und
              gegen die Natur in den edlen &Ouml;lbaum eingepfropft wurdest,
              dann werden erst recht sie als die von Natur zugeh&ouml;rigen Zweige
              ihrem eigenen &Ouml;lbaum wieder eingepfropft werden. <br>
              Damit ihr euch nicht auf eigene Einsicht verlasst, Br&uuml;der,
              sollt ihr dieses Geheimnis wissen: Verstockung liegt auf einem
              Teil Israels, bis die Heiden in voller Zahl das Heil erlangt haben;
              dann wird ganz Israel gerettet werden, wie es in der Schrift hei&szlig;t:
              Der Retter wird aus Zion kommen, er wird alle Gottlosigkeit von
              Jakob entfernen. Das ist der Bund, den ich ihnen gew&auml;hre,
              wenn ich ihre S&uuml;nden wegnehme. <br>
              R&ouml;merbrief 11,13-27</font></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Leib Christi</strong><br>
  Der Gott Jesu Christi, unseres Herrn, der Vater der Herrlichkeit,
                gebe euch den Geist der Weisheit und Offenbarung, damit ihr ihn
                erkennt. Er erleuchte die Augen eures Herzens, damit ihr versteht,
                zu welcher Hoffnung ihr durch ihn berufen seid, welchen Reichtum
                die Herrlichkeit seines Erbes den Heiligen schenkt und wie &uuml;berragend
                gro&szlig; seine Macht sich an uns, den Gl&auml;ubigen, erweist
                durch das Wirken seiner Kraft und St&auml;rke. Er hat sie an
                Christus erwiesen, den er von den Toten auferweckt und im Himmel
                auf den Platz zu seiner Rechten erhoben hat, hoch &uuml;ber alle
                F&uuml;rsten und Gewalten, M&auml;chte und Herrschaften und &uuml;ber
                jeden Namen, der nicht nur in dieser Welt, sondern auch in der
                zuk&uuml;nftigen genannt wird. Alles hat er ihm zu F&uuml;&szlig;en
                gelegt und ihn, der als Haupt alles &uuml;berragt, &uuml;ber
                die Kirche gesetzt. Sie ist sein Leib und wird von ihm erf&uuml;llt,
                der das All ganz und gar beherrscht. <br>
              Epheserbrief 1,17-23</font></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Christus ist das Haupt der Kirche</strong><br>
  Die Autorit&auml;t der Kirche kommt von ihrem Herrn und Haupt,
              Jesus Christus. Christus, der<br>
              seine Autorit&auml;t von Gott dem Vater erhalten hat, teilte sie
              nach seiner Auferstehung durch den Heiligen Geist den Aposteln
              mit (vgl. Joh 20,22). Durch die Apostel wurde sie den Bisch&ouml;fen,
              ihren Nachfolgern, &uuml;bermittelt und durch sie der gesamten
              Kirche. Jesus Christus unser Herr &uuml;bte diese Autorit&auml;t
              auf verschiedene Weisen aus, wodurch sich das Reich Gottes bis
              zu seiner eschatologischen Vollendung (vgl. 1 Kor 15,24-28) in
              der Welt<br>
              kundtut: durch Lehren (vgl. Mt 5,2; Lk 5,3), durch Wunderwirken
              (vgl. Mk 1,30-34; Mt 14,35-36), durch Austreiben von unreinen Geistern
              (vgl. Mk 1,27; Lk 4,35-36), in der Vergebung von S&uuml;nden (vgl.
              Mk 2,10; Lk 5,24) und dadurch, dass er seine J&uuml;nger auf den
              Wegen der Erl&ouml;sung f&uuml;hrt (vgl. Mt 16,24). In &Uuml;bereinstimmung
              mit dem von Christus erhaltenen Auftrag (vgl. Mt 28,18-20) schlie&szlig;t
              die Aus&uuml;bung der Autorit&auml;t, die den Aposteln und sp&auml;ter
              den Bisch&ouml;fen eigen ist, die Verk&uuml;ndigung und Lehre des
              Evangeliums, Heiligung durch die Sakramente, die im Namen der Dreifaltigkeit
              gespendet werden, und die pastorale Leitung derjenigen, die glauben,
              ein (vgl. Lk 10,16).<br>
              Nr. 12 der orthodox-katholischen Dialogkommission 2007 http://www.oecumene.radiovaticana.org/ted/Articolo.asp?c=167506</font></p>
            <p><font face="Arial, Helvetica, sans-serif"><br>
                <strong>Neuer Bund</strong><br>
              Und er nahm Brot, sprach das Dankgebet, brach das Brot und reichte
              es ihnen mit den Worten: Das ist mein Leib, der f&uuml;r euch hingegeben
              wird. Tut dies zu meinem Ged&auml;chtnis! Ebenso nahm er nach dem
              Mahl den Kelch und sagte: Dieser Kelch ist der Neue Bund in meinem
              Blut, das f&uuml;r euch vergossen wird.<br>
              Lukas 22, 19-20 </font></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Der Geist Jesu</strong><br>
  Wir sind zum Lob seiner Herrlichkeit bestimmt, die wir schon fr&uuml;her
              auf Christus gehofft haben. Durch ihn habt auch ihr das Wort der
              Wahrheit geh&ouml;rt, das Evangelium von eurer Rettung; durch ihn
              habt ihr das Siegel des verhei&szlig;enen Heiligen Geistes empfangen,
              als ihr den Glauben annahmt. Der Geist ist der erste Anteil des
              Erbes, das wir erhalten sollen, der Erl&ouml;sung, durch die wir
              Gottes Eigentum werden, zum Lob seiner Herrlichkeit. Epheserbrief
              1,12-14</font></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Apostel &#8211; Bisch&ouml;fe</strong></font></p>
            <p><font face="Arial, Helvetica, sans-serif">Sendung der Apostel<br>
  Die elf J&uuml;nger gingen nach Galil&auml;a auf den Berg, den
              Jesus ihnen genannt hatte. Und als sie Jesus sahen, fielen sie
              vor ihm nieder. Einige aber hatten Zweifel. Da trat Jesus auf sie
              zu und sagte zu ihnen: Mir ist alle Macht gegeben im Himmel und
              auf der Erde. Darum geht zu allen V&ouml;lkern und macht alle Menschen
              zu meinen J&uuml;ngern; tauft sie auf den Namen des Vaters und
              des Sohnes und des Heiligen Geistes, und lehrt sie, alles zu befolgen,
              was ich euch geboten habe. Seid gewiss: Ich bin bei euch alle Tage
              bis zum Ende der Welt. <br>
              Matth&auml;us 28,16-20</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Aufgaben des Bischofs<br>
  Sei den Gl&auml;ubigen ein Vorbild in deinen Worten, in deinem
              Lebenswandel, in der Liebe, im Glauben, in der Lauterkeit. Lies
              ihnen eifrig (aus der Schrift) vor, ermahne und belehre sie, bis
              ich komme. Vernachl&auml;ssige die Gnade nicht, die in dir ist
              und die dir verliehen wurde, als dir die &Auml;ltesten aufgrund
              prophetischer Worte gemeinsam die H&auml;nde auflegten. Daf&uuml;r
              sollst du sorgen, darin sollst du leben, damit allen deine Fortschritte
              offenbar werden. Achte auf dich selbst und auf die Lehre; halte
              daran fest! Wenn du das tust, rettest du dich und alle, die auf
              dich h&ouml;ren. <br>
              1. Brief an Timotheus 4,12-16, Timotheus wird nicht Apostel genannt,
              sondern &#8222;Episkopus&#8220;, Bischof</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Im Verk&uuml;ndigen des Glaubens der Kirche und im Kl&auml;ren
              der Normen christlicher Lebensf&uuml;hrung haben die Bisch&ouml;fe
              aufgrund g&ouml;ttlicher Einsetzung eine besondere Aufgabe.<br>
              Als Nachfolger der Apostel sind die Bisch&ouml;fe f&uuml;r die
              Gemeinschaft (Communio) im apostolischen Glauben und f&uuml;r die
              Treue zu den Forderungen eines Lebens nach dem Evangelium verantwortlich.<br>
              Nr. 8 des Dokuments der orthodox- katholischen Dialogkommission</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Der Bischof und die &Auml;ltesten<br>
&Auml;
              lteste, die das Amt des Vorstehers gut versehen, verdienen doppelte
              Anerkennung, besonders solche, die sich mit ganzer Kraft dem Wort
              und der Lehre widmen. Denn die Schrift sagt: Du sollst dem Ochsen
              zum Dreschen keinen Maulkorb anlegen, und: Wer arbeitet, hat ein
              Recht auf seinen Lohn. Nimm gegen einen &Auml;ltesten keine Klage
              an, au&szlig;er wenn zwei oder drei Zeugen sie bekr&auml;ftigen.
              Wenn sich einer verfehlt, so weise ihn in Gegenwart aller zurecht,
              damit auch die anderen sich f&uuml;rchten. Ich beschw&ouml;re dich
              bei Gott, bei Christus Jesus und bei den auserw&auml;hlten Engeln:
              Befolge dies alles ohne Vorurteil und vermeide jede Bevorzugung!
              Lege keinem vorschnell die H&auml;nde auf und mach dich nicht mitschuldig
              an fremden S&uuml;nden; bewahre dich rein! <br>
              1. Timotheus 5, 17-22, F&uuml;r die &Auml;ltesten, das w&auml;ren
              heute die Pfarrer, hat der Bischof eine Art richterliche Funktion.</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Apostel und Heilige Schrift<br>
  Die Schrift ist das offenbarte Wort Gottes, wie die Kirche es durch
                den in ihr gegenw&auml;rtigen und aktiven Heiligen Geist in der
                lebendigen, von den Aposteln empfangenen Tradition, erkannt hat.
                Nr. 15 der orthodox-katholischen Dialogkommission 2007 </font></p>
            <p><font face="Arial, Helvetica, sans-serif">Eckhard Bieger S.J.</font></p>
            <p></p>
            <p><font face="Arial, Helvetica, sans-serif"><b></b></font></p>
            <p>&nbsp;</p>
            <p><font face="Arial, Helvetica, sans-serif"><b><font size="2"><br>
              </font></b><font size="2">&copy; kath.de<b><br>
              </b></font></font><font size="2" face="Arial, Helvetica, sans-serif">Feedback
              bitte an redaktion@kath.de</font></p>
          </td>
          <td class="L12" width="30%">            <div align="right"> 
              <p align="right">
			  
			  
			 <script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
google_ad_width = 336;
google_ad_height = 280;
google_ad_format = "336x280_as";
google_ad_type = "text_image";
//2006-10-23: Lexikon Symbole
google_ad_channel = "8495253357";
//--></script>
<script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
			  
			  
			  <br>
                <br>
                <font size="2" face="Arial, Helvetica, sans-serif"><br>
                <img src="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/photocase219413723.jpg" width="370" height="285"><br>
                Beispielbild</font></p>
              <p>&nbsp;</p>
              <p><br>
              </p>
            </div>
          </td>
          <td background="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxright.gif" width="8"><img src="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td width="8"><img src="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxbottom.gif" colspan="2"><img src="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxbottom.gif" width="8" height="8" alt=""></td>
          <td width="8"><img src="file:///C|/Dokumente%20und%20Einstellungen/medien/Lokale%20Einstellungen/Temp/lexikon/liturgie/boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td width="100" align="left" valign="top">&nbsp;</td>
  </tr>
</table>
</body>
</html>
