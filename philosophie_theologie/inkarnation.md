---
title: Inkarnation
author: 
tags: 
created_at: 
images: []
---


# **"Fleischwerdung" des Sohnes Gottes*
***An Weihnachten feiern wir, daß der Sohn Gottes Mensch geworden ist. Das ist für das Denken eine Provokation. Muß Gott Mensch werden, wenn er die Menschheit erlösen will? Wenn Gott allmächtig ist, dann genügt dazu eine Willensentscheidung. Was das Geheimnis und den Charme des Weihnachtsfestes ausmacht, das Kind in Krippe, endet, wenn dieses Kind erwachsen geworden ist, mit dem Kreuzestod. Der Evangelist Johannes spitzt die Aussage noch zu, er sagt nämlich, daß das Wort Fleisch geworden ist. Text s.u. Muß das Fleisch so betont werden? 

# **Unzulängliche Antworten****
 Wenn man es wirklich ernst nimmt, daß Jesus von Nazareth der Sohn Gottes ist, dann besteht die Gefahr, daß die Menschheit Jesu so in den Vordergrund rückt, daß die Gottheit in der Wahrnehmung der Menschen undeutlich wird. Um das zu verhindern, schwächt man die Menschheit Jesu ab und sagt, daß diese nur eine Art Gewand ist, in dem Gott erscheint. In der frühen Kirche waren es die Doketisten, die die menschliche Gestalt Jesu nur als Schein bezeichneten. Von „scheinen“ haben sie ihren Namen, denn dokein heißt im Griechischen „scheinen“. Der frühen Kirche wurde bald klar, daß die Theorie der Doketisten nicht nur den biblischen Berichten nicht gerecht wird, die Jesus als wirklichen Menschen mit Gefühlen zeigen, der körperlich gelitten hat und leiblich von den Toten auferstanden ist. Denkt man die doketistische Sehweise bis zum Ende durch, stellt man die Tatsächlichkeit der Erlösung in Frage, denn es ist gerade der Tod am Kreuz, den Jesus wirklich erlitten hat, der für uns Menschen die Umkehr gebracht und zur Auferstehung als Zielpunkt der Existenz Jesu geführt hat.  

# **Theologische Argumentation****
 Der Evangelist Johannes hat sich wahrscheinlich in seinem Evangelium bereits mit der doketistischen Sichtweise  auseinandergesetzt und betont an mehreren Stellen, daß es wirklich passiert ist, was er von Jesus berichtet. Als Thomas zweifelt, daß Jesus              leiblich auferstanden ist und den Jüngern unterstellt, sie hätten ein Gespenst gesehen, läßt Jesus ihn seine Hände berühren und fordert Thomas auf, daß dieser seine Hand in die Seitenwunde Jesu legt. (Kap. 20,29)***

 Erst wenn der Sohn Gottes leiblich Mensch geworden ist, wird er zu einem Teil der Menschheitsgeschichte und erst dann ereignet sich die Erlösung der Menschen in der Geschichte. Deshalb sagt Irenäus von Lyon „caro cardo salutis“, „das Fleisch ist die Türangel der Erlösung“. Es ist wie mit dem, was wir denken. Gedachtes gibt es viel, viele gute Gedanken, sie werden aber nur wirklich, wenn sie Fleisch werden.***

 Die Menschwerdung, die Fleischwerdung des Logos ist der Inhalt des ***[Weihnachtsfestes.](http://www.kath.de/Kirchenjahr/weihnachten.php)            ******

# *
 Theologische Lösung *****

 Inkarnation, Fleischwerdung bedeutet Existenz in der Geschichte. Wenn Gott sich zum Teil der menschlichen Geschichte macht, dann können wir die Hoffnung haben, daß er die Geschichte, unsere Geschichte, zu einem guten Ende bringt. Wenn Jesus mit Leib und Seele auferstanden ist, dann werden auch wir nicht nur mit unserer Seele ewig leben, sondern unser Leib gehört zu unserer ***[himmlischen Existenz](person.php). Die Bedeutung, die der Leib durch die Fleischwerdung des Sohnes Gottes bekommt, führt zur Erkenntnis, daß er nicht nur eine Seele hat, für die der Leib allenfalls ein Anhängsel wäre, das im Tod abgestoßen wird. Nicht nur die Seele, sondern auch der Leib sind von unersetzlichem Wert. Deshalb muß sich die christliche Nächstenliebe um den Leib kümmern. Daß das nicht nur Theorie ist, zeigen die Krankenhäuser, die schon in der Frühzeit von Christen gegründet wurden.  

# **Zitate**
 Im Anfang war das Wort, und das Wort war bei Gott und das Wort war Gott. Im Anfang war es bei Gott. Alles ist durch das Wort geworden, und ohne das Wort wurde nichts. .....***

 Und das Wort ist Fleisch geworden und hat unter uns gewohnt, und wir haben seine Herrlichkeit gesehen, die Herrlichkeit des einzigen Sohnes vom Vater, voll Gnade und Wahrheit. Kap. 1,1-3, 14 

# **Er (Jesus) war Gott gleich, **
 hielt aber nicht daran fest, wie Gott zu sein,***

 sondern entäußerte sich und wurde wie ein Sklave,***

 und den Menschen gleich.***

 Sein Leben war das eines Menschen,***

 er erniedrigte sich und war gehorsam bis zum Tod,***

 bis zum Tod am Kreuz.***

 Darum hat ihn Gott über alle erhoben,***

 und ihm einen Namen verliehen, ***

 der größer ist als alle Namen,***

 damit alle im Himmel, auf der Erde und unter der Erde,***

 ihre Knie beugen vor dem Namen Jesu,***

 und jeder Mund bekennt:***

 Jesus Christus ist der Herr,***

 zur Ehre Gottes des Vaters.***

 Philipperbrief 2,6-11 

# **Werden wir wie Christus, da Christus gleich uns geworden ist. Werden wir seinetwillen Götter, da er unsertwegen Mensch geworden ist. Das Geringere nahm er an, um das Bessere zu geben. Er wurde arm, damit wir durch seine Armut reich würden. Er nahm die Gestalt eines Knechtes an, damit wir die Freiheit erhielten. Er stieg auf die Erde hinab, damit wir erhöht würden.**
 Gregor von Nazaninz, Oratio 1,5 etwa 363 

# **Wir bekennen unseren Herrn Jesus Christus als wahren Gott und wahren Menschen in einer Person. Er bleib also die Person des Sohnes in der Trinität, zu welcher Person die menschliche Natur hinzukam, damit auch eine Person sei Gott und Mensch – nicht ein vergöttlichter Mensch oder ein erniedrigter Gott, sondern Gott-Mensch und Mensch-Gott. Wegen der Einheit der eine Sohn Gottes und zugleich auch Menschensohn, vollkommener Gott und vollkommener Mensch. Vollkommen aber ist der Mensch mit Leib und Seele…**
 Konzil von Frankfurt, 794 

# **Paul Tillich über die Wirklichkeit der Menschwerdung**
 Wenn die christliche Theologie das historische Faktum ignoriert, auf das der Name Jesus von Nazareth hinweist, dann ignoriert sie damit die grundlegende christliche Aussage, daß die wesenhafte Gott-Mensch-Einheit in der Existenz erschienen ist und sich den Bedingungen der Existenz unterworfen hat, ohne von ihnen überwunden zu werden. Gäbe es kein personhaftes Leben, in dem die existentielle Entfremdung überwunden ist, dann würde das Neue Sein eine Forderung und eine Erwartung sein und nicht Wirklichkeit in Raum und Zeit. … Das ist der Grund dafür, daß die christliche Theologie auf der Anerkennung der historischen Faktizität des Jesus von Nazareth bestehen muß.***

 Systematische Theologie, 1957, S. 108 

***Text: Eckhard Bieger S.J. 

©***[ ***www.kath.de](http://www.kath.de) 
