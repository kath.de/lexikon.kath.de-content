<HTML><HEAD><TITLE>Leib Jesu</TITLE>
<META http-equiv=Content-Type content="text/html; charset=iso-8859-1"><LINK 
title=fonts href="kaltefleiter.css" type=text/css 
rel=stylesheet>
<META content="MSHTML 5.50.4134.600" name=GENERATOR></HEAD>
<BODY bgColor=#ffffff leftMargin=6 topMargin=6 marginheight="6" marginwidth="6">
<TABLE cellSpacing=0 cellPadding=6 width="100%" border=0>
  <TBODY>
  <TR>
    <TD vAlign=top align=left width=100> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b>Philosophie&amp;Theologie</b></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td> <?php include("logo.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10"> <?php include("az.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </TD>
    <TD vAlign=top rowSpan=2>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
        <TBODY>
        <TR vAlign=top align=left>
          <TD width=8><IMG height=8 alt="" 
            src="boxtopleftcorner.gif" width=8></TD>
          <TD background=boxtop.gif><IMG height=8 alt="" 
            src="boxtop.gif" width=8></TD>
          <TD width=8><IMG height=8 alt="" 
            src="boxtoprightcorner.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxtopleft.gif><IMG height=8 alt="" 
            src="boxtopleft.gif" width=8></TD>
          <TD bgColor=#e2e2e2> 
            <H1><font face="Arial, Helvetica, sans-serif">Leib Jesu</font></H1>
          </TD>
          <TD background=boxtopright.gif><IMG height=8 
            alt="" src="boxtopright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=13 alt="" src="boxdividerleft.gif" 
            width=8></TD>
          <TD background=boxdivider.gif><IMG height=13 
            alt="" src="boxdivider.gif" width=8></TD>
          <TD><IMG height=13 alt="" 
            src="boxdividerright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxleft.gif><IMG height=8 alt="" 
            src="boxleft.gif" width=8></TD>
          <TD class=L12>
            <P><STRONG><font face="Arial, Helvetica, sans-serif">Jesus hatte
                nicht nur einen Scheinleib</font></STRONG></P>
            <P><font face="Arial, Helvetica, sans-serif">Jesus ist ein wirklicher
                Mensch, der predigend durch Pal&auml;stina
              gezogen ist, der Menschen ber&uuml;hrt hat, um sie zu heilen oder
              zu tr&ouml;sten. Sein Leib wurde geschunden, gegei&szlig;elt und
              gekreuzigt. Aber er ist nirgendwo begraben. Zwar wurde er nach
              seinem Tod am Kreuz in einer Grabkammer beigesetzt. Als die Frauen
              nach dem Sabbat, am ersten Tag der j&uuml;dischen Woche, den Leichnam
              endg&uuml;ltig versorgen wollten, fanden sie das Grab offen, der
              Leichnam war verschwunden. Gibt es den Leib Jesu? Zumindest beim
              Kommuniongang wird den Gl&auml;ubigen die Hostie mit den Wort: &#8222;Leib
              Christi&#8220; in die Hand oder auf die Zunge gelegt. </font></P>
            <P><font face="Arial, Helvetica, sans-serif"><strong>Unzul&auml;ngliche Antworten</strong><br>
              In den ersten christlichen Jahrhunderten wurden unterschiedliche
              Meinungen vertreten. Wenn Jesus von Nazareth wirklich Gott war,
              konnte er dann wirklich Mensch sein? Jesus, der Mensch, war geschaffen,
              Jesus, der Sohn Gottes, ist nicht geschaffen. Wie kann aber Geschaffenes
              mit dem G&ouml;ttlichen eine Einheit bilden? In den <a href="christologische_streitigkeiten.php">christologischen
              Streitigkeiten</a> werden
              die unterschiedlichsten Denkmodelle durchgespielt. Eine erste L&ouml;sung
              war die der Doketen, die sich vom griechischen Wort f&uuml;r &quot;Schein&quot;
              herleiten. Der Leib Jesu war nicht wirklich, Gott hat sich in einem
              Scheinleib gezeigt. Die Gnostiker hatten eine &auml;hnliche Vorstellung,
              weil f&uuml;r sie die Leiblichkeit eine Art Verbannung der Seele
              in die Materie bedeutet. Erl&ouml;sung ist f&uuml;r die Gnostiker
              Befreiung der Seele von allem K&ouml;rperlichen - durch Erkenntnis,
              im Griechischen Gnosis.</font></P>
            <P><font face="Arial, Helvetica, sans-serif"><strong>Die Realit&auml;t
                  der Passion</strong><br>
              Wenn man die Realit&auml;t des Leibes Jesu nicht anerkennt, dann
              sind auch Gei&szlig;elung und Kreuzigung Jesu nicht wirklich geschehen.
              Verschiedene Reaktionen auf den Film &#8222;Die Passion Christi&#8220; im
              Jahr 2004 zeigen, wie schwer es f&auml;llt, die Realit&auml;t dieser
              grausamen Hinrichtung zu akzeptieren. Den <a href="http://www.kath.de/Kirchenjahr/karfreitag.php">Passionsberichten</a> der
              Evangelien wird man nur gerecht, wenn der Leib Jesu nicht ein Scheinleib
              war, sondern den Qualen ausgesetzt war, die in den n&uuml;chternen
              Berichten der Bibel angedeutet werden. Der Theologe Iren&auml;us
              von Lyon im 3. Jahrhundert bringt das auf die Formel &#8222;Caro
              Cardo Salutis&#8220; &#8222;Das Fleisch ist die T&uuml;rangel des
              Heils&#8220;.<br>
              <br>
              <strong>Theologische L&ouml;sung</strong><br>
              Die Bedeutung des Leibes wird noch einmal mehr in der <a href="auferstehung_jesu.php">Auferstehung </a>deutlich.
              Wenn man das Leiden Jesu als k&ouml;rperlich
              real akzeptieren mu&szlig;, dann k&ouml;nnten die Begegnungen der
              J&uuml;nger Jesu mit dem Auferstandenen als Visionen gesehen werden,
              in denen sie sich nur eines Scheinleibes ansichtig wurden. Um dieser
              Interpretation vorzubeugen, schildern Lukas und Johannes in ihren
              Evangelien die reale leibliche Anwesenheit des Auferstandenen.
              Beide Evangelisten waren wahrscheinlich schon mit gnostischen Vorstellungen
              konfrontiert. (Texte s.u.)<br>
              Mit seinem Leib wird Jesus erst zu einem Teil der menschlichen
              Geschichte. Durch seinen <a href="inkarnation.php">Leib</a> wird sein Leiden und Sterben real.
              Durch die Auferstehung des Leibes wird der Tod erst &uuml;berwunden.
              W&auml;hrend das griechische Denken im Tod die Befreiung der Seele
              von allem Materiellen sah, sehen die Christen in der Auferstehung
              die Vollendung des Materiellen. Jesus ist leiblich in den Himmel
              aufgefahren und seine Wundmale bleiben im Himmel leiblich pr&auml;sent.
              Gerade die Auferstehung des Leibes gibt dem K&ouml;rper des Menschen
              eine letzte Bedeutung, macht ihn unantastbar.Die Bedeutung des
              Leibes Jesu setzt sich fort in einer Beschreibung der Kirche. Sie
              wird im Epheserbrief der Leib Christi genannt.</font></P>
            <P><font face="Arial, Helvetica, sans-serif">Zitate<br>
              Lukas 24,36-40<br>
            </font><font face="Arial, Helvetica, sans-serif">W&auml;hrend sie noch miteinander redeten, trat er selbst in ihre
              Mitte und sagte zu ihnen: Friede sei mit euch! Sie erschraken und
              hatten gro&szlig;e Angst, denn sie meinten, einen Geist zu sehen.
              Da sagte er zu ihnen: Was seid ihr so best&uuml;rzt? Warum la&szlig;t
              ihr in euren Herzen solche Zweifel aufkommen, Seht meine H&auml;nde
              und meine F&uuml;&szlig;e an: Ich bin es selbst. Fa&szlig;t mich
              doch an und begreift: Kein Geist hat Fleisch und Knochen, wie ihr
              es an mir seht. Bei diesen Worten zeigte er ihnen seine H&auml;nde
              und F&uuml;&szlig;e. </font></P>
            <p><font face="Arial, Helvetica, sans-serif">Johannes zeigt in der
                Begegnung des Auferstandenen mit dem Zweifler Thomas die Realit&auml;t
                des Leibes Jesu auf:<br>
              Acht Tage darauf waren die J&uuml;nger wieder versammelt und Thomas
              war dabei. Die T&uuml;ren waren verschlossen. Da kam Jesus, trat
              in ihre Mitte und sagte: Friede sei mit euch. Dann sagte er zu
              Thomas: Streck deine Finger aus &#8211; hier sind meine H&auml;nde.
              Streck deine Hand aus und leg sie in meine Seite, und sei nicht
              ungl&auml;ubig, sondern gl&auml;ubig. Thomas antwortete ihm: Mein
              Herr und mein Gott. Kap. 20,26-28</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Gott hat seine Macht
                an Christus erwiesen, den er von den Toten auferweckt und im
                Himmel auf den Platz zu seiner Rechten erhoben
              hat, hoch &uuml;ber alle F&uuml;rsten und Gewalten, M&auml;chte
              und Herrschaften und &uuml;ber jeden Namen, der nicht nur in dieser
              Welt, sondern auch in der zuk&uuml;nftigen genannt wird. Alles
              hat er ihm zu F&uuml;&szlig;en gelegt und ihn, der als Haupt alles &uuml;berragt, &uuml;ber
              die Kirche gesetzt. Sie ist sein Leib und wird von ihm erf&uuml;llt,
              der das All ganz und gar beherrscht. Epheserbrief 1,20-23</font></p>
            <p><font face="Arial, Helvetica, sans-serif">.. da&szlig; wir vollkommen &uuml;berzeugt sind von der Geburt,
              dem Leiden und der Auferstehung, die w&auml;hrend der Regierungszeit
              von Pontius Pilatus erfolgt ist; wirklich und gewi&szlig; wurde
              dies vollbracht von Jesus Christus, unserer Hoffnung&#8230;.<br>
              Brief des Ignatius v. Antiochien an die Gemeinde in Ephesus 117
            </font></p>
            <p><font size="2" face="Arial, Helvetica, sans-serif">Text: Eckhard Bieger
            </font></p>
            <P><font face="Arial, Helvetica, sans-serif">&copy;<a href="http://www.kath.de"> www.kath.de</a></font></P>
            </TD>
          <TD background=boxright.gif><IMG height=8 alt="" 
            src="boxright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=8 alt="" src="boxbottomleft.gif" 
            width=8></TD>
          <TD background=boxbottom.gif><IMG height=8 alt="" 
            src="boxbottom.gif" width=8></TD>
          <TD><IMG height=8 alt="" src="boxbottomright.gif" 
            width=8></TD></TR></TBODY></TABLE></TD><td style='vertical-align:top;'>
 <script type="text/javascript"><!--
        google_ad_client = "pub-9537752113242914";
        google_ad_width = 336;
        google_ad_height = 280;
        google_ad_format = "336x280_as";
        google_ad_type = "text_image";
        //2006-10-23: Lexikon Symbole
        google_ad_channel = "8495253357";
        //--></script>
        <script type="text/javascript"
          src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
        </script>
</td>
</TR>
  <TR>
    <TD vAlign=top align=left>&nbsp; </TD>
  </TR></TBODY></TABLE></BODY></HTML>
