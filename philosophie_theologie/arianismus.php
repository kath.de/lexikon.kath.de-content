<HTML><HEAD><TITLE>Arianismus</TITLE>
<META http-equiv=Content-Type content="text/html; charset=iso-8859-1"><LINK 
title=fonts href="kaltefleiter.css" type=text/css 
rel=stylesheet>
<META content="MSHTML 5.50.4134.600" name=GENERATOR></HEAD>
<BODY bgColor=#ffffff leftMargin=6 topMargin=6 marginheight="6" marginwidth="6">
<TABLE cellSpacing=0 cellPadding=6 width="100%" border=0>
  <TBODY>
  <TR>
    <TD vAlign=top align=left width=100> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b>Philosophie&amp;Theologie</b></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td> <?php include("logo.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10"> <?php include("az.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </TD>
    <TD vAlign=top rowSpan=2>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
        <TBODY>
        <TR vAlign=top align=left>
          <TD width=8><IMG height=8 alt="" 
            src="boxtopleftcorner.gif" width=8></TD>
          <TD background=boxtop.gif><IMG height=8 alt="" 
            src="boxtop.gif" width=8></TD>
          <TD width=8><IMG height=8 alt="" 
            src="boxtoprightcorner.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxtopleft.gif><IMG height=8 alt="" 
            src="boxtopleft.gif" width=8></TD>
          <TD bgColor=#e2e2e2> 
            <H1><font face="Arial, Helvetica, sans-serif">Arianismus<br>
              <br>
            </font></H1>
          </TD>
          <TD background=boxtopright.gif><IMG height=8 
            alt="" src="boxtopright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=13 alt="" src="boxdividerleft.gif" 
            width=8></TD>
          <TD background=boxdivider.gif><IMG height=13 
            alt="" src="boxdivider.gif" width=8></TD>
          <TD><IMG height=13 alt="" 
            src="boxdividerright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxleft.gif><IMG height=8 alt="" 
            src="boxleft.gif" width=8></TD>
          <TD class=L12>
            <P><STRONG><font face="Arial, Helvetica, sans-serif"> Motiv f&uuml;r
                das Weihnachtsfest</font></STRONG></P>
            <P><font face="Arial, Helvetica, sans-serif">F&uuml;r die junge
                christliche Kirche war es eine gro&szlig;e Herausforderung, die
                Botschaft von Jesus, dem Sohn Gottes und Erl&ouml;ser aller Menschen,
                gegen&uuml;ber der griechischen Philosophie zu vertreten. Die
                Theologen stie&szlig;en nicht mehr auf eine religi&ouml;se Kultur
                mit vielen G&ouml;tterkulten, so wie sp&auml;ter die Missionare bei
                den germanischen St&auml;mmen. Die Griechen hatten schon seit
                der Zeit Platons auf denkerischem Weg die Einsicht gewonnen,
                da&szlig; das
                G&ouml;ttliche
                nur als Eines gedacht werden kann. Der Grundgedanke, der zu dieser
                Einsicht f&uuml;hrte, war der der Vollkommenheit. Etwas, das
                teilbar ist, kann sich ver&auml;ndern und kann auch zerst&ouml;rt
                werden. Auch die menschliche Seele wird als unteilbar gedacht
                und damit als unsterblich, wie es Platon in seinem Dialog Phaidon
                entwickelt. Die Frage spitze sich Anfang des 4. Jahrhunderts
                zu, ob Jesus wirklich Gottes Sohn sein konnte. Diese Auseinandersetzugn
                hat sicher Weihanchten, das Fest der Menschwerdung Gottes, befl&uuml;gelt.</font></P>
            <P><font face="Arial, Helvetica, sans-serif"><strong>Ungen&uuml;gende Antworten
                auf die Herausforderung der griechsichen Philosophie:</strong><br>
                Wenn die christliche Predigt auf das griechische Denken trifft,
                dann wird sie an den Pr&auml;missen dieses Denkens gemessen.
                Den Griechen war nicht zuzumuten, ein Gottesbild zu &uuml;bernehmen,
                das sie wieder zu den G&ouml;ttervorstellungen zur&uuml;ckzuf&uuml;hren
                schien, die der volkst&uuml;mlichen Religiosit&auml;t zwar entgegenkommen,
                aber als R&uuml;ckschritt gegen&uuml;ber dem erreichten Stand
                des Denkens gesehen werden konnten. Der Priester Arius aus &Auml;gypten
                machte sich zum Sprecher der griechischen &Uuml;berzeugungen.
                Da der <a href="christologische_streitigkeiten.php">Doketismus</a>  wie
                auch der   <a href="gottessohn.php">Adoptianismus </a>&uuml;berwunden
                waren, konnte Arius nicht erkl&auml;ren,
                Jesus habe nur einen Scheinleib angenommen  noch
                da&szlig; er nach seiner Geburt erst von Gott als Messias berufen
                und als Sohn &#8222;adoptiert&#8220; worden sei. Er nahm die
                Br&uuml;cke, die der Evangelist Johannes zum griechischen Denken
                geschlagen hatte, auf und sagte, da&szlig; der Logos Mensch geworden
                ist. Aber dieser Logos ist nicht der ewige Sohn Gottes, der mit
                Gott gleich ist, sondern ein geschaffenes Wesen.            </font></P>
            <P><font face="Arial, Helvetica, sans-serif"><strong>Theologische Argumente</strong></font><br>
              <font face="Arial, Helvetica, sans-serif">Was auf den ersten Blick
              als eine Aussage &uuml;ber den Sohn erscheint,
              ist eigentlich eine Aussage &uuml;ber den Vater. Wenn &uuml;ber
              den Messias Jesus Christus gesagt wird, er ist &#8222;Nicht-Gott&#8220;,
              dann ist das zuerst eine Festlegung Gottes: Gott kann, soll er
              wirklich Gott sein, keinen Sohn aus sich gezeugt haben, der kein
              Gesch&ouml;pf, sondern wie der Vater Gott ist. Aus der inneren
              Logik des Denkens schien es den griechischen Intellektuellen unausweichlich,
              da&szlig; Gott nur einer ist. Wenn das Denken den wirklichen Gott
              erreichen will, dann kann es ihn nur als unteilbar, als Einen denken.
              Genau das bekennen wir bis heute im Glaubensbekenntnis: Ich glaube
              an den einen Gott. Aber kann Gott vom Denken so festgelegt werden?<br>
              Dieser eine Gott wurde von den Griechen als ein Wesen gedacht,
              das seine G&ouml;ttlichkeit verlieren mu&szlig;, wenn er mit dem
              Endlichen in Ber&uuml;hrung kommt. Deshalb hat Gott vor der Erschaffung
              der Welt den Logos geschaffen. Dieser entspricht dem inneren Logos,
              dem Denken in Gott, ist aber endlich und die vermittelnde Instanz,
              um die sichtbare Welt zu schaffen. Zu dieser sichtbaren Welt hat
              Gott keine Beziehung. Er kann deshalb keine haben, weil er sich
              dann an die Vielheit verlieren w&uuml;rde und damit sein Gott-Sein
              in Frage st&uuml;nde. In den Texten, die von Arius &uuml;berliefert
              sind, kommt diese Grundintention zum Ausdruck: Gott ist eine in
              sich geschlossene Monade. (s.u. Zitate) Er kann etwas schaffen,
              aber das Geschaffene hat keinen Zugang zu ihm. Dieser weltenferne
              Gott,
              der in seiner
              Gottheit sozusagen eingeschlossen ist und, um Gott zu bleiben,
              in absoluter Distanz zur Welt stehen mu&szlig;, war nicht der Gott,
              den das j&uuml;dische Volk in seiner Geschichte kennengelernt hatte
              und den Jesus in seinen Gleichnisreden und seiner Predigt den Menschen
              nahe gebracht hatte. Arius stellt also nicht nur einen Teil der
              christlichen Botschaft in Frage, sondern zeichnet einen ganz anderen
              Gott als
              er von den Juden erfahren und im Geist von den Christen erlebt
              wurde. Gott ist nicht fern, sondern den Menschen ganz nah. Er
              hat sich
              den Menschen zugewandt. Er selbst ist gekommen, um die Menschen
              zu retten. Das <a href="gottessohn.php">Konzil
              von Nic&auml;a</a>              hat deshalb die biblische &Uuml;berlieferung best&auml;tigt und
              daran festgehalten, da&szlig; Jesus Christus der Sohn Gottes ist.
              Er offenbart den Vater, weil er allein den Vater kennt. Er ist
              in seinem Sohnsein nicht geschaffen, sondern in einem innerg&ouml;ttlichen
              Vorgang gezeugt. Daraus entwickelten sich neue Fragen, n&auml;mlich
              wie in Jesus Christus das Verh&auml;ltnis zwischen dem g&ouml;ttlichen
              <a href="christologische_streitigkeiten.php">Logos und dem Menschen
              Jesus</a>              gedacht werden kann.</font></P>
            <P><font face="Arial, Helvetica, sans-serif"><strong>Zitate</strong><br>
              Die Thesen des Arius finden sich in einem von ihm verfa&szlig;ten
              Glaubensbekenntnis, das er 320 in einem Brief an den Patriarchen
              Alexander von Alexandrien niedergeschrieben hat:<br>
              Wir kennen nur einen Gott, den allein ungewordenen, den allein
              ewigen, allein ursprungslosen, allein wahren, allein die Unsterblichkeit
              Besitzenden, allein weisen, allein guten; den Alleinherrscher,
              den Richter aller, den Ordner und Verwalter, unwandelbar und unver&auml;nderlich,
              gerecht und gut, den Gott des Gesetzes, der Propheten und des Neuen
              Bundes, der den eingeborenen Sohn vor ewigen Zeiten hervorgebracht
              hat, durch den er auch die &Auml;onen und das All machte; er hat
              ihn nicht dem Scheine nach hervorgebracht, sondern in Wahrheit,
              als in eigenem Willen Wesenden, als Unwandelbaren und Unver&auml;nderlichen,
              als Gottes vollkommenes Gesch&ouml;pf, aber nicht wie eines der
              Gesch&ouml;pfe &#8230;..<br>
              Durch den Willen Gottes vor den Zeiten und &Auml;onen geschaffen,
              der vom Vater des Leben hat sowie die Ehre, so da&szlig; der Vater
              mit ihm zusammen existiert. Denn der Vater hat sich nicht selbst
              beraubt, da er ihm als Erbe all jenes gegeben hat, was er ungeschaffen
              in sich tr&auml;gt. Er ist die Quelle von allem. So gibt es drei
              Subjekte (Hypostasen). Gott der Vater ist die Ursache aller, ganz
              allein ohne Ursprung, der Sohn aber, vom Vater vor der Zeit hervorgebracht
              und vor den &Auml;onen geschaffen und gegr&uuml;ndet, war nicht,
              bevor er hervorgebracht wurde, &#8230;. Er hat vom Vater seien
              Existenz erhalten. Denn er ist nicht ewig oder gleich ewig oder
              gleich ungeworden wie der Vater, noch hat er zugleich mit dem Vater
              das Sein &#8230; der Vater ist, da er wie eine Monade oder ein
              Prinzip von allem ist, auch Gott vor allem. So ist er auch vor
              dem Sohn. &#8230;.</font></P>            
            <p><font face="Arial, Helvetica, sans-serif">Athanasius hat die sog
                Blasphemien des Arius in seiner Schrift &uuml;ber
              das Konzil von Nic&auml;a formuliert:<br>
              Gott selbst, wie er ist, ist unaussprechlich f&uuml;r alle.<br>
              Er allein hat weder Seinesgleichen noch einen ihm &Auml;hnlichen,
              noch einen von gleicher Herrlichkeit.<br>
              Wir nennen ihn den Ungewordenen wegen des von Natur aus Gewordenen &#8230;.<br>
              Der Ursprungslose setzte den Sohn als Anfang der Gewordenen und<br>
              bestimmte sich zum Sohn den, den er als Kind gemacht hatte; <br>
              dieser hat nichts Gott-Eigenes in dem, was ihm auf Grund seines
              Subjektseins (Hypostase) eigen ist;<br>
              denn er ist ihm nicht gleich und auch nicht wesensgleich. &#8230;.<br>
              Sicher ist die Aussage, da&szlig; Gott f&uuml;r alle unsichtbar
              ist;<br>
              er selbst ist unsichtbar allen, die durch den Sohn sind, auch dem
              Sohn selber;<br>
              Ausdr&uuml;cklich sage ich aber, wie dem Sohn der Unsichtbare sichtbar
              ist:<br>
              Entsprechend der Kraft und dem eigenen Ma&szlig;, mit dem der Logos
              schaut kann der Sohn den Vater sehen, wie es rechtens ist.<br>
              Es gibt also eine Trias, nicht aber von gleichen Ehren, denn ihre
              Hypostasen sind einander nicht vermischt;<br>
              denn die eine hat gegen&uuml;ber der anderen unendlich mehr Ehre.<br>
              Dem Wesen nach ist der Vater dem Sohn gegen&uuml;ber fremd, da
              Er ursprungslos west. &#8230;<br>
              Solange der Sohn nicht ist, ist der Gott nicht Vater.<br>
              Zuvor war der Sohn nicht, trat aber ins Dasein durch den v&auml;terlichen
              Willen; &#8230;..<br>
              Durch Gottes Willen hat der Sohn dieses Alter und diese Gr&ouml;&szlig;e;<br>
              Seine Existenz aus Gott ist bestimmt durch ein &#8222;seit wann&#8220; und
              ein &#8222;von woher&#8220; und ein &#8222;von da ab&#8220;; &#8230;<br>
              Um zusammenzufassen: Der Gott west f&uuml;r den Sohn als Unaussprechlicher.<br>
              er ist n&auml;mlich f&uuml;r sich, was er ist, das hei&szlig;t
              er ist unsagbar,<br>
              so da&szlig; der Sohn selbst nichts von dem Gesagten ad&auml;quat
              auszusagen versteht. &#8230;.<br>
              auch der Sohn hat seine (des Vaters) Wesenheit nicht gesehen,<br>
              da er als Sohn in Wirklichkeit nur durch den Willen des Vaters
              besteht.<br>
              Wem ist also gestattet zu sagen, da&szlig; der, der aus dem Vater
              ist,<br>
              den, der ihn hervorgebracht hat, erkennen oder begreifen kann?<br>
              Es ist also deutlich, da&szlig; der, der einen Anfang hat, den
              Anfanglosen, so wie er ist, nicht umgreifen und nicht erfahren
              kann.</font></p>
            <p><font face="Arial, Helvetica, sans-serif"><br>
  Wie Arius sind auch das Evangelium des Johannes und seine Briefe
                sehr zur&uuml;ckhaltend in der Frage, ob der Mensch Zugang zu
                Gott finden kann. Bereits im Prolog des Evangeliums hei&szlig;t
                es: &#8222;Niemand hat Gott je gesehen.&#8220; (Kap. 1,18) In
                dem 1. Johannesbrief hei&szlig;t es: &#8222;niemand hat Gott
                je geschaut.&#8220; Kap 4,12<br>
                Aber anders als f&uuml;r Arius ist die Distanz zwischen dem Menschen
              und Gott &uuml;berwunden, durch den Sohn. Im Prolog endet der Satz
              nicht damit &#8222;Niemand hat Gott je gesehen.&#8220;, sondern
              wird fortgesetzt: &#8222;Der Einzige, der Gott ist und am Herzen
              des Vaters ruht, er hat Kunde gebracht.&#8220; (Kap. 1,18)<br>
              Der oben zitierte Satz aus dem 1. Johannesbrief beinhaltet ein
              kurzes Glaubensbekenntnis: &#8222;Niemand hat Gott je geschaut.
              Wenn wir einander lieben, bleibt Gott in uns, und seine Liebe ist
              in uns vollendet. Daran erkennen wir, da&szlig; wir in ihm bleiben
              und er in uns bleibt: Er hat uns seinen Geist gegeben. Wir haben
              gesehen und bezeugen, da&szlig; der Vater den Sohn gesandt hat
              als den Retter der Welt. Wer bekennt, da&szlig; Jesus der Sohn
              Gottes ist, in dem bleibt Gott, und er bleibt in Gott.&#8220; (Kap.
              4,12-15)<br>
              Im Evangelium findet sich ein Abschnitt, der wir in einer Lehrstunde
              aufzeigt, da&szlig; wir Menschen Gott in seinem menschgewordenen
              Sohn erkennen.<br>
&#8222;
              Philippus sagte zu ihm: Herr, zeig uns den Vater; das gen&uuml;gt
              uns. Jesus antworte ihm: Schon so lange bin ich bei euch, und du
              hast mich nicht erkannt, Philippus? Wer mich gesehen hat, hat den
              Vater gesehen. Wie kannst du sagen: Zeig uns den Vater? Glaubst
              du nicht, da&szlig; ich im Vater bin und der Vater in mir ist?
              Die Worte, die ich euch sage, habe ich nicht aus mir selbst. Der
              Vater, der in mir bleibt, vollbringt seine Werke. Glaubt mir doch,
              da&szlig; ich im Vater bin und da&szlig; der Vater in mir ist.&#8220; (Kap.
              14,8-11)<br>
              Gott ist nicht ein Gott, der in sich als Monade abgeschlossen,
              unzug&auml;nglich, nur f&uuml;r sich allein existiert, sondern
              der die Beziehung zum Menschen will.</font></p>
            <p><font face="Arial, Helvetica, sans-serif"><br>
                <font size="2">Eckhard Bieger S.J.</font></font></p>
            <P>&copy;<a href="http://www.kath.de"> <font face="Arial, Helvetica, sans-serif">www.kath.de</font></a></P>
            </TD>
          <TD background=boxright.gif><IMG height=8 alt="" 
            src="boxright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=8 alt="" src="boxbottomleft.gif" 
            width=8></TD>
          <TD background=boxbottom.gif><IMG height=8 alt="" 
            src="boxbottom.gif" width=8></TD>
          <TD><IMG height=8 alt="" src="boxbottomright.gif" 
            width=8></TD></TR></TBODY></TABLE></TD><td style='vertical-align:top;'>
 <script type="text/javascript"><!--
        google_ad_client = "pub-9537752113242914";
        google_ad_width = 336;
        google_ad_height = 280;
        google_ad_format = "336x280_as";
        google_ad_type = "text_image";
        //2006-10-23: Lexikon Symbole
        google_ad_channel = "8495253357";
        //--></script>
        <script type="text/javascript"
          src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
        </script>
</td>
</TR>
  <TR>
    <TD vAlign=top align=left>&nbsp; </TD>
  </TR></TBODY></TABLE></BODY></HTML>
