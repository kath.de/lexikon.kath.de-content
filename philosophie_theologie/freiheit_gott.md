---
title: Freiheit und Gott
author: 
tags: 
created_at: 
images: []
---


# **Ist Gott Konkurrent der menschlichen Freiheit?*
***Kinder erleben die Welt genauso unsicher wie Erwachsene und brauchen daher einen Garanten, der die Welt im Lot hält. Die Moderne hat diese Vorstellung als Täuschung entlarvt und Gott als den hingestellt, der der Freiheit mißtrauisch gegenüber steht. Die Kirche hat dieser Vorstellung Vorschub geleistet. Entwicklungspychologisch ist diese Sicht verständlich. Denn mit der Entdeckung, daß es Böses gibt, entsteht sofort das Verlangen, daß jemand Starkes das Gute nicht nur will, sondern auch die Macht hat, dem Guten zum Sieg zu verhelfen. Mit der Entdeckung der Freiheit im Jugendalter wird diese alles bestimmende Macht zum Konkurrenten der eigenen Freiheit, denn Gott besteht auf der Einhaltung seiner Gebote und scheint an der Verwirklichung der Freiheit des einzelnen kein Interesse zu haben. Erst wenn das Leben auf eigenen Entscheidungen aufgebaut ist, Berufsziele erreicht, eine Familie gegründet, ein Haus gebaut ist, wird der Blick offener für all das, was zum Gelingen der Freiheit beigetragen hat. Es waren andere Menschen, die Zuneigung geschenkt, die geholfen und Chancen eröffnet haben. Die Freiheit selbst aber konnten andere nicht geben, auch nicht die Gesellschaft oder der Staat – sie kommt von dem, der Leben schenkt. Gott schenkt nicht nur die Freiheit, sondern auch eine einmalige Berufung, damit das Leben nicht die Kopie eines vorgegebenen Musters wird, sondern etwas Unersetzliches. 

***Eckhard Bieger S.J. 

******[Buchtipp](http://www.kathshop.de/wwwkathde/product_info.php?cPath=&products_id=195&anbieter=17&page=&) 

©***[ ***www.kath.de](http://www.kath.de) 
