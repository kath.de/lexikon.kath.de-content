<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <title>Das B&ouml;se und das Gute</title>


  <meta name="title" content="Philosophie&amp;Theologie">

  <meta name="author" content="Redaktion kath.de">

  <meta name="publisher" content="kath.de">

  <meta name="copyright" content="kath.de">

  <meta name="description" content="">

  <meta name="abstract" content="Lexikon &uuml;ber Philosophie&amp;Theologie">

  <meta http-equiv="content-language" content="de">

  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

  <meta name="date" content="2006-00-01">

  <meta name="robots" content="index,follow">

  <meta name="revisit-after" content="10 days">

  <meta name="revisit" content="after 10 days">

  <meta name="DC.Title" content="Philosophie&amp;Theologie">

  <meta name="DC.Creator" content="Redaktion kath.de">

  <meta name="DC.Contributor" content="J&uuml;rgen Pelzer">

  <meta name="DC.Rights" content="kath.de">

  <meta name="DC.Publisher" content="kath.de">

  <meta name="DC.Date" content="2006-00-01">

  <meta name="DC.Description" content="Lexikon &uuml;ber Philosophie&amp;Theologie">

  <meta name="DC.Language" content="de">

  <meta name="DC.Type" content="Text">

  <meta name="DC.Format" content="text/html">

  <meta name="DC.Identifier" content="http://www.kath.de/lexikon/philosophie_theologie/">

  <meta name="keywords" content="Anspruch, Gott, Entscheidung, Freiheit, Trotzalter, Sarte" lang="de">

</head>
<body leftmargin="6" topmargin="6" style="background-color: rgb(255, 255, 255);" marginheight="6" marginwidth="6">

<table border="0" cellpadding="6" cellspacing="0" width="100%">

  <tbody>

    <tr>

      <td align="left" valign="top" width="100">
      <table border="0" cellpadding="0" cellspacing="0" width="216">

        <tbody>

          <tr align="left" valign="top">

            <td width="8"><img src="boxtopleftcorner.gif" alt="" height="8" width="8"></td>

            <td background="boxtop.gif" width="200"><img src="boxtop.gif" alt="" height="8" width="8"></td>

            <td width="8"><img src="boxtoprightcorner.gif" alt="" height="8" width="8"></td>

          </tr>

          <tr align="left" valign="top">

            <td background="boxtopleft.gif"><img src="boxtopleft.gif" alt="" height="8" width="8"></td>

            <td bgcolor="#e2e2e2"><b>Philosophie&amp;Theologie</b></td>

            <td background="boxtopright.gif"><img src="boxtopright.gif" alt="" height="8" width="8"></td>

          </tr>

          <tr align="left" valign="top">

            <td><img src="boxdividerleft.gif" alt="" height="13" width="8"></td>

            <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" height="13" width="8"></td>

            <td><img src="boxdividerright.gif" alt="" height="13" width="8"></td>

          </tr>

          <tr align="left" valign="top">

            <td background="boxleft.gif"><img src="boxleft.gif" alt="" height="8" width="8"></td>

            <td><?php include("logo.html"); ?> </td>

            <td background="boxright.gif"><img src="boxright.gif" alt="" height="8" width="8"></td>

          </tr>

          <tr align="left" valign="top">

            <td><img src="boxbottomleft.gif" alt="" height="8" width="8"></td>

            <td background="boxbottom.gif"><img src="boxbottom.gif" alt="" height="8" width="8"></td>

            <td><img src="boxbottomright.gif" alt="" height="8" width="8"></td>

          </tr>

        </tbody>
      </table>

      <br>

      <table border="0" cellpadding="0" cellspacing="0" width="216">

        <tbody>

          <tr align="left" valign="top">

            <td width="8"><img src="boxtopleftcorner.gif" alt="" height="8" width="8"></td>

            <td background="boxtop.gif" width="200"><img src="boxtop.gif" alt="" height="8" width="8"></td>

            <td width="8"><img src="boxtoprightcorner.gif" alt="" height="8" width="8"></td>

          </tr>

          <tr align="left" valign="top">

            <td background="boxtopleft.gif"><img src="boxtopleft.gif" alt="" height="8" width="8"></td>

            <td bgcolor="#e2e2e2"><strong>Begriff
anklicken</strong></td>

            <td background="boxtopright.gif"><img src="boxtopright.gif" alt="" height="8" width="8"></td>

          </tr>

          <tr align="left" valign="top">

            <td><img src="boxdividerleft.gif" alt="" height="13" width="8"></td>

            <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" height="13" width="8"></td>

            <td><img src="boxdividerright.gif" alt="" height="13" width="8"></td>

          </tr>

          <tr align="left" valign="top">

            <td background="boxleft.gif"><img src="boxleft.gif" alt="" height="8" width="8"></td>

            <td class="V10"><?php include("az.html"); ?> </td>

            <td background="boxright.gif"><img src="boxright.gif" alt="" height="8" width="8"></td>

          </tr>

          <tr align="left" valign="top">

            <td><img src="boxbottomleft.gif" alt="" height="8" width="8"></td>

            <td background="boxbottom.gif"><img src="boxbottom.gif" alt="" height="8" width="8"></td>

            <td><img src="boxbottomright.gif" alt="" height="8" width="8"></td>

          </tr>

        </tbody>
      </table>

      </td>

      <td rowspan="2" valign="top">
      <table border="0" cellpadding="0" cellspacing="0" width="100%">

        <tbody>

          <tr align="left" valign="top">

            <td width="8"><img alt="" src="boxtopleftcorner.gif" height="8" width="8"></td>

            <td background="boxtop.gif"><img alt="" src="boxtop.gif" height="8" width="8"></td>

            <td width="8"><img alt="" src="boxtoprightcorner.gif" height="8" width="8"></td>

          </tr>

          <tr align="left" valign="top">

            <td background="boxtopleft.gif"><img alt="" src="boxtopleft.gif" height="8" width="8"></td>

            <td bgcolor="#e2e2e2">
            <h1><font face="Arial, Helvetica, sans-serif">
Das B&ouml;se - und das Gute</font></h1>

            </td>

            <td background="boxtopright.gif"><img alt="" src="boxtopright.gif" height="8" width="8"></td>

          </tr>

          <tr align="left" valign="top">

            <td><img alt="" src="boxdividerleft.gif" height="13" width="8"></td>

            <td background="boxdivider.gif"><img alt="" src="boxdivider.gif" height="13" width="8"></td>

            <td><img alt="" src="boxdividerright.gif" height="13" width="8"></td>

          </tr>

          <tr align="left" valign="top">

            <td class="" background="boxleft.gif"><img alt="" src="boxleft.gif" height="8" width="8"></td>

            <td style="font-family: Helvetica,Arial,sans-serif;" class="L12"><br>

            <p class="MsoNormal">Was das Gute ist, wissen
wir erst, weil wir das B&ouml;se kennen.
Das unterscheidet uns vom Tier. Dieses kennt Widrigkeiten, Gefahr,
Wohlbefinden, N&uuml;tzliches, aber nicht das mit Absicht
zugef&uuml;gte B&ouml;se, die
Verleumdung, den Diebstahl, die unter Gewalt erpresste Zustimmung. Der
Mensch
tr&auml;gt auch eine Vorstellung mit sich, wie es sein sollte, eben
gut, d.h.
gerecht, gewaltfrei, h&ouml;flich und vielleicht sogar liebevoll im
Umgang. Diese Vorstellung
malt ein Paradies aus. Dass diese Bilder wirksam sind, zeigt uns die
Urlaubswerbung. Sie stellt uns die Bilder vom Paradies vor. Im Urlaub
soll
alles gut sein, nicht nur das Hotelbett und das Essen, sondern auch die
Stimmung, die erlebte Freundlichkeit, eine Welt ohne Armut und Krieg.</p>

            <p class="MsoNormal"><o:p></o:p>Wahrscheinlich
haben auch Antilopen, L&ouml;wen oder Affen
&auml;hnliche Bilder, aber sie kennen das B&ouml;se nur als
Gefahr, als Hunger, als
Unterliegen im Kampf um den Rangplatz in der Horde. Beim Menschen kommt
ein
neues Bewusstsein hinzu. Hunger, verletzt, ungerecht behandelt zu
werden, das
ist nicht nur unangenehm, man weicht ihm aus, es soll auch nicht sein.
Zudem
hat der einzelne das Bewusstsein, dass es eine Ordnung gibt, die das
verbietet.
Dieses Bewusstsein nennen wir Gewissen. Es mag unterschiedlich geformt
sein,
dass z.B. der Hass auf ein anderes Volk als geboten gilt. Aber es
bleibt das
Bewusstsein, etwas zu sollen, nicht nur f&uuml;r das eigene
&Uuml;berleben, sondern weil
es eine Ordnung gibt, die L&uuml;ge und Mobbing verbietet. Das
Gewissen ist ein
Indiz daf&uuml;r, dass wir zwischen zwei Alternativen zu
w&auml;hlen haben.</p>

            <p class="MsoNormal"><o:p></o:p><b style="">Das Gute entsteht
erst, genauso wie das B&ouml;se<o:p></o:p></b><br>

Die Entscheidung zwischen Gut und B&ouml;se zeigt, dass beides
erst im Werden begriffen ist. W&auml;re das Gute schon da,
k&ouml;nnte man sich nicht
mehr daf&uuml;r entscheiden, denn was gut ist, das wollen wir,
n&auml;mlich das sch&ouml;ne
Hotelzimmer, den Sandstrand wie auch die Zuneigung andrer. Wenn das
B&ouml;se sich verfestigt
h&auml;tte, dann h&auml;tte das Gute keine Chance mehr. Zwar
wirken Gutes und B&ouml;ses nach,
aber das Neue, wenn es um Gut oder B&ouml;se geht, soll erst
entstehen. Es entsteht
aus der Entscheidung. Worum geht es aber in der Entscheidung?</p>

            <p class="MsoNormal"><o:p></o:p><b style="">Das Gute er&ouml;ffnet
Leben, das B&ouml;se vernichtet Leben<o:p></o:p></b></p>

            <p class="MsoNormal">Mit unserer Vorstellung
vom Guten ist immer Erhalt und
Zuwachs an Leben verbunden. Es geht um das eigene Leben, das durch
eigenes
Verhalten, z.B. Alkoholexzesse oder mangelnder Einsatz beim Lernen
Einbu&szlig;en
erleidet.<br>

In Bezug auf die anderen gelten die 10 Gebote, die Familie
hochsch&auml;tzen, f&uuml;r Kinder und Alte sorgen, den anderen
nicht k&ouml;rperlich
sch&auml;digen, nicht bel&uuml;gen, seine Familie, sein
Eigentum achten. &Uuml;ber die 10
Gebote hinaus gibt es die Forderung, Arme zu unterst&uuml;tzen,
Kranke zu versorgen
und sogar, Feinden zu vergeben.<br>

Das B&ouml;se mindert und vernichtet Leben, eben im Gegensatz zu
den 10 Geboten. Der Mord ist deshalb die nachhaltigste Wirkung des
B&ouml;sen, weil
dem anderen Leben nicht erschwert, sondern ihm g&auml;nzlich
verweigert wird. </p>

            <p class="MsoNormal"><o:p></o:p>Weil
unser Leben sich in der Zeit verwirklicht und sich
dabei entwickeln soll, ist alles, was Leben mindert oder es in seiner
Entwicklung hindert, b&ouml;se.</p>

            <p class="MsoNormal">Das B&ouml;se gibt es
allerdings nur deshalb, weil Leben
beeintr&auml;chtigt, gehindert, vernichtet werden kann.
W&auml;re alles fertig, dann g&auml;be
es nur das, was geworden ist. Dann w&auml;re aber das B&ouml;se
verschwunden, denn es ist
ja auf Minderung und letztlich Vernichtung aus. Das B&ouml;se, so
zeigen es schon
unsere Lebenserfahrungen, hat keinen Bestand. Zwar hat der
&Uuml;belt&auml;ter einen Vorteil,
aber der Schaden, den er angerichtet hat, kommt auf ihn
zur&uuml;ck. Andere
misstrauen ihm und einige f&uuml;hlen sich berechtigt, ihm
B&ouml;ses anzutun, z.B. ihn
zu ermorden, um selbst die Herrschaft &uuml;bernehmen zu
k&ouml;nnen. Mord hat immer zur
Folge, dass der M&ouml;rder sich selbst in Lebensgefahr bringt. Ein
Beispiel ist
Russland: Wegen der Enteignungs- und Hinrichtungswellen fehlen dem Land
heute Millionen
von Menschen, vielleicht sogar Hundertmillionen. Zudem ist der
Alkoholismus ein
nicht gel&ouml;stes Problem. Die Folgen des B&ouml;sen sind auf
jeden Fall Minderung. Das
B&ouml;se schickt Lebendiges auf die Reis eins Nichts.</p>

            <p class="MsoNormal">Daraus folgt aber, dass
nur etwas, das auch nicht sein kann,
vom B&ouml;sen affiziert werden kann. Etwas, das nicht
&bdquo;nicht&ldquo; sein kann, ist frei
vom B&ouml;sen. Siehe auch: <span style="color: red;"></span><a href="boese_gott.php"><u>das B&ouml;se und Gott</u></a></p>

            <p class="MsoNormal"><o:p>&nbsp;</o:p></p>

            <p class="MsoNormal">Eckhard Bieger S.J.</p>

            <p class="MsoNormal"></p>

            <p class="MsoNormal"></p>

            <p>&copy;<a href="http://www.kath.de">
www.kath.de</a></p>

            </td>

            <td background="boxright.gif"><img alt="" src="boxright.gif" height="8" width="8"></td>

          </tr>

          <tr align="left" valign="top">

            <td><img alt="" src="boxbottomleft.gif" height="8" width="8"></td>

            <td background="boxbottom.gif"><img alt="" src="boxbottom.gif" height="8" width="8"></td>

            <td><img alt="" src="boxbottomright.gif" height="8" width="8"></td>

          </tr>

        </tbody>
      </table>

      </td>

      <td style="vertical-align: top;">
      <script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
google_ad_width = 336;
google_ad_height = 280;
google_ad_format = "336x280_as";
google_ad_type = "text_image";
//2006-10-23: Lexikon Symbole
google_ad_channel = "8495253357";
//--></script>
      <script type="text/javascript" src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
      </script></td>

    </tr>

    <tr>

      <td align="left" valign="top">&nbsp; </td>

    </tr>

  </tbody>
</table>

</body>
</html>
