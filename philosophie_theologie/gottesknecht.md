---
title: Gottesknecht
author: 
tags: 
created_at: 
images: []
---


# **Eine Erklärung für die Hinrichtung Jesu aus dem Alten Testament*
***Die Jünger hatten sich Jesus angeschlossen, weil sie in ihm den***[ Messias](messias_christus.php), erkannt hatten, den das Volk Israel erwartete. Die Wunder, die Jesus wirkte und seine überzeugende Predigt hatten bestätigt, daß er im Namen Gottes aufgetreten konnte. Als Jesus zum bevorstehenden ***[Paschafest](http://www.kath.de/Kirchenjahr/palmsonntag.php) in Jerusalem einzog und die Menschen ihm zujubelten, konnten seine Anhänger eigentlich damit rechnen, daß er das Reich Gottes, von dem er oft gesprochen hatte, aufrichten werde. Statt dessen wurde er an die Römer, also Heiden, ausgeliefert und von diesen auf die erniedrigenste Form, durch das ***[Kreuz](http://www.kath.de/Kirchenjahr/karfreitag.php), hingerichtet. Die Jünger mußten sich fragen, ob Gott diesen Messias im Stich gelassen hatte. Von zwei Jüngern, die von Jerusalem weggingen, wird berichtet, daß sie sehr betrübt und enttäuscht waren. Durch die Begegnung mit dem Auferstandenen kamen zuerst die Frauen und dann auch die Apostel zum Glauben, daß Jesus lebt. Als Juden mußten sich die Jünger erklären können, warum Jesus leiden mußte, denn in vielen Psalmen und Berichten der Bibel wird immer wieder betont, daß Gott den Gerechten aus der Hand seiner Feinde rettet. Die ersten Christen schauten in ihrer Bibel nach, weil sie als gläubige Juden nach dem Willen Gottes suchten. Da die Evangelien noch nicht geschrieben waren, stand ihnen nur die jüdische Bibel zur Verfügung. 

# **Die Verurteilung Jesu war kein Versehen****
 Versuche, den Tod Jesus als ein Versehen der jüdischen Obrigkeit zu deuten, konnten für die junge christliche Gemeinde nicht überzeugen, denn sie wurden ja selbst verfolgt, weil sie sich zu Jesus als Messias bekannten. (Messias heißt der Gesalbte, griechisch Christos) Da sie aber keinen anderen Gott verehrten als die Juden, mußten sie eine Kontinuität erkennen können.  

# **Eine Antwort im Buch Jesaja****
 Im Prophetenbuch des Jesaja finden sich Texte, die vom Leiden des Gerechten sprechen und ausdrücklich sein Leiden als Sühne verstehen. Es sind die Gottesknechtslieder, die heute noch in der Karwoche gelesen werden. Das vierte dieser Lieder gehört in de Karfreitagsliturgie. (s. u.) In diesem Text finden die Christen eine Antwort, warum der Messias leiden mußte. Nicht Gott verurteilt ihn zum Kreuz, sondern das Böse wird durch den Gerechten provoziert und sucht, ihn zu vernichten. Dies kann psychologisch durch den ***[Sündenbockmechanismus](suendenbock.php) erklärt werden.             

# **Aus dem vierten Lied vom Gottesknecht**
 Seht, mein Knecht hat Erfolg, er wird groß sein und hoch erhaben.***

 Viele haben sich über ihn entsetzt, denn er sah entstellt aus, nicht wie ein Mensch, seine Gestalt war nicht mehr die eines Menschen.***

 Jetzt aber setzt er viele Völker in Staunen, Könige müssen vor ihm verstummen.***

 .........***

 Wer hat geglaubt, was uns berichtet wurde?***

 Die Hand des Herrn - wer hat ihr Wirken erkannt?***

 Vor den Augen des Herrn wuchs er auf wie ein junger Sproß, wie der Trieb einer Wurzel aus trockenem Boden.***

 Er hatte keine schöne und edle Gestalt, und niemand von uns blickte ihn an.***

 Er sah nicht so aus, daß er unser Gefallen erregte.***

 Er wurde verachtet und von den Menschen gemieden,***

 ein Mann voller Schmerzen, mit der Krankheit vertraut.***

 Wie ein Mensch, vor dem man das Gesicht verhüllt,***

 war er bei uns verfemt und verachtet.***

 Aber er hat unsere Krankheiten getragen***

 und unsere Schmerzen auf sich genommen.***

 Wir meinten, er sei vom Unheil getroffen, von Gott gebeugt und geschlagen. ***

 Doch er wurde durchbohrt wegen unserer Verbrechen,***

 wegen unserer Sünden mißhandelt.***

 Weil die Strafe auf ihm lag, sind wir gerettet,***

 .........***

 Der Herr warf all unsere Sünden auf ihn. ***

 Er wurde geplagt und niedergedrückt, aber er tat seinen Mund nicht auf.***

 Wie ein Lamm, das man wegführt, um es zu schlachten,***

 und wie ein Schaf, das verstummt, wenn man es schert,***

 so tat auch er seinen Mund nicht auf. ***

 .....***

 Bei den Gottlosen gab man ihm sein Grab, bei den Verbrechern seine Ruhestätte,***

 obwohl er kein Unrecht getan hat und aus seinem Mund kein unwahres Wort kam. ***

 Doch der Herr fand Gefallen an seinem mißhandelten (Knecht),***

 er rettete den, der sein Leben als Sühneopfer hingab.***

 ....... ***

 Nachdem er so vieles ertrug, erblickt er wieder das Licht***

 und wird erfüllt von Erkenntnis.***

 Mein Knecht ist gerecht, darum macht er viele gerecht;***

 er nimmt ihre Schuld auf sich. ***

 ......***

 denn er gab sein Leben hin und wurde zu den Verbrechern gerechnet.***

 Er trug die Sünden von vielen und trat für die Schuldigen ein. ***

 Jesaja 52,13-53,12  

# **Vgl. auch die anderen Gottesknechtslieder (Jesaia 42,1-9; 49,1-9; 50,4-9). **
***Text: Eckhard Bieger S.J.  

***©***[ www.kath.de](http://www.kath.de) 
