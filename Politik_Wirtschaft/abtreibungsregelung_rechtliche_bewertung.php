<HTML><HEAD><TITLE>Abtreibungsregelung - rechtliche Bewertung</TITLE>
<META http-equiv=Content-Type content="text/html; charset=iso-8859-1"><LINK 
title=fonts href="kaltefleiter.css" type=text/css 
rel=stylesheet>
<META content="MSHTML 5.50.4134.600" name=GENERATOR></HEAD>
<BODY bgColor=#ffffff background="dstone1.gif" leftMargin=6 topMargin=6 marginwidth="6" marginheight="6">
<TABLE cellSpacing=0 cellPadding=6 width="100%" border=0>
  <TBODY>
  <TR>
    <TD vAlign=top align=left width=100> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif" height="14"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2" height="14"><b>Politik und Wirtschaft</b></td>
          <td background="boxtopright.gif" height="14"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#FFFFFF"> <?php include("logo.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#FFFFFF" class="V10"> <?php include("az.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </TD>
    <TD vAlign=top rowSpan=2>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
        <TBODY>
        <TR vAlign=top align=left>
          <TD width=8><IMG height=8 alt="" 
            src="boxtopleftcorner.gif" width=8></TD>
          <TD background=boxtop.gif><IMG height=8 alt="" 
            src="boxtop.gif" width=8></TD>
          <TD width=8><IMG height=8 alt="" 
            src="boxtoprightcorner.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxtopleft.gif><IMG height=8 alt="" 
            src="boxtopleft.gif" width=8></TD>
          <TD bgColor=#e2e2e2> 
            <H1><strong><font face="Arial, Helvetica, sans-serif">Abtreibungsregelung
                  - Rechtliche Bewertung</font></strong></H1>
          </TD>
          <TD background=boxtopright.gif><IMG height=8 
            alt="" src="boxtopright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=13 alt="" src="boxdividerleft.gif" 
            width=8></TD>
          <TD background=boxdivider.gif><IMG height=13 
            alt="" src="boxdivider.gif" width=8></TD>
          <TD><IMG height=13 alt="" 
            src="boxdividerright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxleft.gif height="124"><IMG height=8 alt="" 
            src="boxleft.gif" width=8></TD>
          <TD height="124" bgcolor="#FFFFFF" class=L12> 
            <P><STRONG><font size="3" face="Arial, Helvetica, sans-serif"><br>
              Der
                  Paragraf 281 des Strafrechts und seine Umsetzung</font></STRONG><font size="3" face="Arial, Helvetica, sans-serif">:</font></P>
            <p><font face="Arial, Helvetica, sans-serif"><strong>&sect; 218 Abs.
                  S. 1 StGB lautet:</strong><br>
            </font><font face="Arial, Helvetica, sans-serif">Wer eine Schwangerschaft abbricht, wird mit Freiheitsstrafe bis
              zu drei Jahren oder mit Geldstrafe bestraft.</font></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>&sect; 218 a
                  Abs 1 StGB lautet:</strong><br>
            </font><font face="Arial, Helvetica, sans-serif">Der Tatbestand
                des &sect; 218
                ist nicht verwirklicht, wenn<br>
            </font><font face="Arial, Helvetica, sans-serif">1.
                die Schwangere den Schwangerschaftsabbruch verlangt und dem Arzt
                durch eine Bescheinigung
                nach &sect; 219 Abs. 2 S. 2 nachgewiesen
              hat, da&szlig; sie sich mindestens drei Tage vor dem Eingriff hat
              beraten lassen,<br>
              2. der Schwangerschaftsabbruch von einem Arzt vorgenommen wird
              und<br>
              3.	seit der Empf&auml;ngnis nicht mehr als zw&ouml;lf Wochen vergangen
              sind.</font></p>
            <p><font face="Arial, Helvetica, sans-serif">W&auml;hrend die erstgenannte Norm noch klar erscheint, ist die
              Bewertung der zweiten (beratene Abtreibung) alles andere als das.
              Aus folgenden Gr&uuml;nden:<br>
            </font><font face="Arial, Helvetica, sans-serif">&#8226; Gemeinhin wird gesagt, die Abtreibung nach Beratung sei &#8222;rechtswidrig,
              aber straflos&#8220;. <br>
              Die Aufrechterhaltung des Postulats &#8222;rechtswidrig&#8220; ist
              in der Tat verfassungsrechtlich geboten. In seiner Entscheidung
              vom 28.5.1993 hat das Bundesverfassungsgericht erkl&auml;rt, der
              Schwangerschaftsabbruch m&uuml;sse &#8222;f&uuml;r die ganze Dauer
              der Schwangerschaft grunds&auml;tzlich als Unrecht angesehen und
              demgem&auml;&szlig; rechtlich verboten sein&#8220; (Leitsatz 4).
              Sollte sich im Rahmen der rechtlichen Bewertung herausstellen,
              da&szlig; die Rechtsordnung den beratenen Schwangerschaftsabbruch
              gar nicht als Unrecht ansieht, so d&uuml;rfte dieser kaum mit unserem
              Grundgesetz vereinbar sein.</font></p>
            <p><font face="Arial, Helvetica, sans-serif">&#8226; <strong>&#8222;Rechtswidrig,
                  aber straflos&#8220;</strong> <br>
                  Das klingt
              nach einer nach wie vor bestehenden Mi&szlig;billigung dieses Verhaltens
              durch die Rechtsordnung. Tats&auml;chlich aber suchen wir im Gesetzestext
              vergeblich nach dieser Mi&szlig;billigung. &sect; 218 a Abs. 1
              sagt vielmehr, der &#8222;Tatbestand&#8220; des &sect; 218 sei &#8222;nicht
              verwirklicht&#8220;. Nimmt man das w&ouml;rtlich, so ist die Abtreibung
              nach &sect; 218 a &uuml;berhaupt keine Abtreibung im Sinne des
              Gesetzes. Eine solche Tat ist auch nicht rechtswidrig. Denn rechtswidirg
              kann nur eine Tat sein, die den Tatbestand eines Strafgesetzes
              verwirklicht (&sect; 11 Nr. 5 StGB). Wenn aber schon keine rechtswidrige
              Tat vorliegt, so kann eine solche auch nicht f&uuml;r &#8222;straflos&#8220; erkl&auml;rt
              werden, was au&szlig;er in der &Uuml;berschrift der Norm auch nicht
              geschieht</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Tats&auml;chlich mi&szlig;billigt
                unsere Rechtsordnung die &#8222;Abtreibung
              nach Beratung&#8220; keineswegs. Der Gesetzgeber tut das Gegenteil.
              Er f&ouml;rdert diese Art der Abtreibung. Dazu 4 Argumente:</font></p>
            <p><font face="Arial, Helvetica, sans-serif">&#8226; Wenngleich das Bundesverfassungsgericht eine Beratung &#8222;zugunsten
              des ungeborenen Lebens&#8220; fordert (Leits&auml;tze 11, 12 und
              13), hat der Gesetzgeber im krassen Gegensatz dazu im<br>
              Schwangerschaftskonfliktgesetz festgelegt, die Beratung m&uuml;sse &#8220;ergebnisoffen&#8220; gef&uuml;hrt
              werden.<br>
              <br>
&#8226; 
              Weiter tut unser Staat allerhand, um abtreibungswilligen Frauen
              die Abtreibung zu erm&ouml;glichen. Er sieht es als seine Aufgabe
              an, Abtreibungsm&ouml;glichkeiten zu schaffen und zwar fl&auml;chendeckend
              und in einer Entfernung, &#8222;die von der Frau nicht die Abwesenheit &uuml;ber
              einen Tag hinaus verlangt&#8220;. Er sieht es zudem als seine Aufgabe
              an, Frauen die Abtreibung auch dann gro&szlig;z&uuml;gig zu finanzieren,
              wenn sie nicht hilfebed&uuml;rftig im Sinne etwa der Sozialhilfe
              sind. Die Abrechnung der Abtreibungskosten erfolgt nach &sect; 3
              des Gesetzes &quot;zur Hilfe f&uuml;r Frauen bei Schwangerschaftsabbr&uuml;chen
              in besonderen F&auml;llen&quot; vom 21.8.1995 auf Antrag der Frau
              durch die gesetzlichen Krankenkassen und zwar auch dann, wenn die
              Frau gar nicht Mitglied einer solchen Kasse ist. Den Kassen werden
              diese Kosten sp&auml;ter von den Bundesl&auml;ndern erstattet.<br>
              <br>
&#8226; 
              Die Beratung selbst mu&szlig; keine wirkliche Beratung sein. Die
              Schwangere hat einen Anspruch auf Erteilung des die straflose Abtreibung
              erm&ouml;glichenden Beratungsscheines auch dann, wenn sie eine
              Beratungsstelle aufsucht und sich auf eine Darlegung ihrer Gr&uuml;nde
              und eine Beratung gar nicht einl&auml;&szlig;t.<br>
              <br>&#8226; 
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              Noch 1993 hat das Bundesverfassungsgericht festgestellt, da&szlig; der
              an einer Abtreibung mitwirkende Arzt keinen <a href="arzthaftung_bei_abtreibung.php">Honoraranspruch</a>              hat. Der auf Abtreibung gerichtete Vertrag des Arztes sei, so das
              Gericht, wegen der Rechtswidrigkeit der Abtreibung sittenwidrig
              und damit nichtig. In einer sp&auml;teren umstrittenen Entscheidung
              hat das gleiche Gericht jedoch erkl&auml;rt, die rechtswidrige
              T&ouml;tung des Kindes unterliege ausnahmsweise dem Schutz der
              Berufsfreiheit des Art. 12 GG. Denn &#8222;die T&auml;tigkeit des
              Arztes ist notwendiger Bestandteil des gesetzlichen Schutzkonzeptes&#8220;.
              So kommt auch der Abtreibungsarzt zu seinem Honorar. Und wenn die
              T&auml;tigkeit des Arztes &#8211; und das ist die Abtreibung &#8211; &#8222;notwendiger
              Bestandteil des gesetzlichen Schutzkonzeotes ist.&quot;</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Ergebnis: Die beratene
                Abtreibung kann nach geltender Rechtslage nicht als &#8222;rechtswidrig&#8220; bezeichnet werden. Der Gesetzgeber
              hat das nirgendwo getan und sein Handeln spricht eher f&uuml;r
              das Gegenteil.<br>
              Die beratene Abtreibung ist aber auch nie ausdr&uuml;cklich als &#8222;rechtm&auml;&szlig;ig&#8220; bezeichnet
              worden. Sie w&auml;re dann auch verfassungswidrig.<br>
              Bezeichnet man eine Handlung dann als &#8222;rechtswidrig&#8220;,
              wenn sie von der Rechtsordnung mi&szlig;billigt wird und als &#8222;rechtm&auml;&szlig;ig&#8220; dann,
              wenn sie von der Rechtsordnung gebilligt wird, dann kann man aufgrund
              intensiver staatlicher F&ouml;rderung der Abtreibung nur zu dem
              Ergebnis kommen: <br>
              Die Rechtsordnung sieht die beratene Abtreibung tats&auml;chlich
              als &#8222;rechtm&auml;&szlig;ig&#8220; an. Damit billigt sie der
              abtreibungswilligen Frau mindestens de facto ein Recht auf Abtreibung
              zu, somit ein Recht auf T&ouml;tung menschlichen Lebens. Die derzeitige
              Regelung d&uuml;rfte damit verfassungswidrig sein.<br>
              An sich w&auml;re deshalb eine neue Entscheidung des Bundesverfassungsgerichts
              f&auml;llig. Dieses Gericht wird aber keine Chance zur Entscheidung
              erhalten. Denn: Alle Parteien des Deutschen Bundestages haben 1995
              die derzeitige Abtreibungsregelung in seltener Einm&uuml;tigkeit
              beschlossen. Alle Parteien wollen erkl&auml;rterma&szlig;en an
              dieser Regelung festhalten, unabh&auml;ngig von ihrer rechtlichen
              Bewertung. Ein Normenkontrollverfahren vor dem Bundesverfassungsgericht
              aber kann nur von der Bundesregierung, einer Landeregierung oder
              einem Drittel der Mitglieder des Bundestages beantragt werden.
              All diese Institutionen werden von den Parteien beherrscht.</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Welche Konsequenzen
                das Festhalten an diesem Umgang mit dem Rechtsgut Leben langfristig
                haben wird, ist in Ans&auml;tzen schon jetzt
              zu sehen:<br>
              Das Recht pr&auml;gt auch das Rechtsbewu&szlig;tsein der Menschen.
              Schon heute, nur 10 Jahre nach Einf&uuml;hrung der Beratungsregelung,
              wird Abtreibung in weiten Teilen der Bev&ouml;lkerung und namentlich
              der Jugend als &#8222;erlaubt&#8220; angesehen. Die Zahl der Abtreibungen
              steigt Jahr f&uuml;r Jahr. Hierzu hat sicher auch das allzu lange
              Festhalten der deutschen Bisch&ouml;fe am Beratungsschein und damit
              der Beteiligung am staatlichen Abtreibungssystem beigetragen. Das
              Zentralkommitee der deutschen Katholiken h&auml;lt heute noch daran
              fest.<br>
              Weite Teile der Bev&ouml;lkerung bef&uuml;rworten eine &#8222;Liberalisierung&#8220; des
              Lebensschutzes am Ende des Lebens. Mit welcher Argumentation wollen
              wir der Forderung nach aktiver Sterbehilfe (T&ouml;tung leidenden
              Lebens) und der nach der Zul&auml;ssigkeit der T&ouml;tung auf
              Verlangen entgegentreten, wenn wir uns mit der T&ouml;tung gegebenenfalls
              auch gesunden menschlichen Lebens l&auml;ngst abgefunden haben
              ? Der Tabubruch liegt bereits hinter uns.</font></p>
            <p><font face="Arial, Helvetica, sans-serif" size="3"><br>
              &copy; </font><font face="Arial, Helvetica, sans-serif"><a href="http://www.hmk-leben.de/">Dr. Konrad
              Schneller</a></font></p>            </TD>
          <TD background=boxright.gif height="124"><IMG height=8 alt="" 
            src="boxright.gif" width=8></TD>
        </TR>
        <TR vAlign=top align=left>
          <TD><IMG height=8 alt="" src="boxbottomleft.gif" 
            width=8></TD>
          <TD background=boxbottom.gif><IMG height=8 alt="" 
            src="boxbottom.gif" width=8></TD>
          <TD><IMG height=8 alt="" src="boxbottomright.gif" 
            width=8></TD></TR></TBODY></TABLE></TD></TR>
  <TR>
    <TD vAlign=top align=left height="2">&nbsp; </TD>
</TR></TBODY></TABLE></BODY></HTML>
