<HTML><HEAD><TITLE>Eine-Welt-Arbeit</TITLE>
<META http-equiv=Content-Type content="text/html; charset=iso-8859-1"><LINK
title=fonts href="kaltefleiter.css" type=text/css
rel=stylesheet>
<META content="MSHTML 5.50.4134.600" name=GENERATOR></HEAD>
<BODY bgColor=#ffffff background="dstone1.gif" leftMargin=6 topMargin=6 marginwidth="6" marginheight="6">
<TABLE cellSpacing=0 cellPadding=6 width="100%" border=0>
  <TBODY>
  <TR>
    <TD vAlign=top align=left width=100 height="72">
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif" height="14"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2" height="14"><b>Politik und Wirtschaft</b></td>
          <td background="boxtopright.gif" height="14"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#FFFFFF"> <?php include("logo.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#FFFFFF" class="V10"> <?php include("az.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </TD>
    <TD vAlign=top rowSpan=2>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
        <TBODY>
        <TR vAlign=top align=left>
          <TD width=8><IMG height=8 alt=""
            src="boxtopleftcorner.gif" width=8></TD>
          <TD background=boxtop.gif><IMG height=8 alt=""
            src="boxtop.gif" width=8></TD>
          <TD width=8><IMG height=8 alt=""
            src="boxtoprightcorner.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxtopleft.gif><IMG height=8 alt=""
            src="boxtopleft.gif" width=8></TD>
          <TD bgColor=#e2e2e2>
            <H1>Eine-Welt-Arbeit</H1>
          </TD>
          <TD background=boxtopright.gif><IMG height=8
            alt="" src="boxtopright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=13 alt="" src="boxdividerleft.gif"
            width=8></TD>
          <TD background=boxdivider.gif><IMG height=13
            alt="" src="boxdivider.gif" width=8></TD>
          <TD><IMG height=13 alt=""
            src="boxdividerright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxleft.gif height="124"><IMG height=8 alt=""
            src="boxleft.gif" width=8></TD>
          <TD height="124" bgcolor="#FFFFFF" class=L12>
            <P><br>
              <font face="Arial, Helvetica, sans-serif" size="3">
		 <img SRC="http://www.kath.de/lexikon/Politik_Wirtschaft/eine_welt_arbeit.jpg" style="border: double 4px #009999" align="right" hspace="10" vspace="10" name="Eine Welt Arbeit" alt="Eine Welt Arbeit">			  <b>Eine-Welt-Arbeit</b> ist
                die Verbindung von Entwicklungspolitik und EntwicklungspÃ¤dagogik,
              also von entwicklungspolitischem Handeln und entwicklungspolitischer
              Bildung. Das Spektrum der TrÃ¤ger ist breit - es reicht von den Ã¶rtlichen
              Aktionsgruppen Ã¼ber die mit Hauptberuflichen versehen Zentren und
              Koordinationsstellen hin bis zu den groÃen Nichtregierungsorganisationen.<br>
              <br>
              </font><font size="3" face="Arial, Helvetica, sans-serif"><b>Eine-Welt-Arbeit
              </b>ist seit vilen Jahren ein Schwerpunkt im Angebot der Akademie
              Franz Hitze Haus, MÃ¼nster. </font></P>
            <P><font size="3" face="Arial, Helvetica, sans-serif">Das <a href="http://www.franz-hitze-haus.de">Franz
              Hitze Haus </a> ,die Akademie des Bistums MÃ¼nster, hat einen eigenen
              <a href="http://www.franz-hitze-haus.de/index.php?myELEMENT=34113">Fachbereich
              Politik und Zeitgeschichte, Internationale Gerechtigkeit</a>. Dort
              geht es unter anderem darum, die <b>Eine-Welt-Arbeit</b> in den
              Gruppen, VerbÃ¤nden und Organisationen zu reflektieren und ihr neue
              Impulse zu geben. "Globales Lernen" erweist sich dabei als tragfÃ¤higer
              aktueller Leitbegriff der entwicklungspolitischen Bildung, denn
              "Globales Lernen" wird verstanden als die pÃ¤dagogische Reaktion
              auf die im Theorem Globalisierung beschriebene Entwicklung zur Weltgesellschaft.</font></P>
            <P><font size="3" face="Arial, Helvetica, sans-serif">Ein markantes
              Beispiel aus dem Angebot der Akademie Franz Hitze Haus sind die
              Jahrestagungen Entwicklungspolitik, die in Kooperation mit der Arbeitsgemeinschaft
              <b>Eine-Welt-Gruppen</b> im Bistum MÃ¼nster und in der Evangelischen
              Kirche von Westfalen veranstaltet werden. Aktive aus den zumeist
              kirchlichen <b>Eine-Welt-Gruppen</b> kommen hier zusammen, um sich
              auszutauschen, um sich fÃ¼rs AlltagsgeschÃ¤ft weiter qualifizieren
              zu lassen und um politische Perspektiven zu diskutieren. Ein anderes
              Beispiel sind die Multiplikatorenschulungen, in denen Hauptberufliche
              und Ehrenamtliche aus dem ganzen Bundesgebiet ihre Kompetenzen erweitern
              kÃ¶nnen.</font></P>
            <P><font size="3" face="Arial, Helvetica, sans-serif">Vom zeitlichen
              Umfang her reichen die Bildungsveranstaltungen im Fachbereich Politik
              und Zeitgeschichte, Internationale Gerechtigkeit vom Abendforum
              Ã¼ber den Studientag bis zur Wochenendtagung. Aus dem Grundauftrag
              der Akademie, eine Verbindung zwischen dem gesellschaftlichen Teilsystem
              Religion und den anderen Systemen, wie beispielsweise Wirtschaft,
              Wissenschaft und Politik, herzustellen, ergibt sich, dass zum Dialog
              in gleicher Weise Fachleute wie auch allgemein politisch Interessierte
              eingeladen sind. GroÃen Wert wird darauf gelegt, Veranstaltungen
              gemeinsam mit Kooperationspartner aus dem kirchlichen und auÃerkirchlichen
              Spektrum anzubieten; entwicklungspolitischen Nichtregierungsorganisationen
              kommt dabei eine herausragende Bedeutung zu.<br>
              <br>
              <b>Weitere Informationen:<br>
              </b></font><font size="3" face="Arial, Helvetica, sans-serif"><br>
              <a href="http://www.franz-hitze-haus.de/index.php?myELEMENT=34113">Fachbereich
              &quot;Politik und Zeitgeschichte, Internationale Gerechtigkeit&quot;
              der Akademie Franz Hitze Haus</a><br>
              </font><font face="Arial, Helvetica, sans-serif" size="3"><br>
              &copy; Akademie Franz Hitze Haus. Bild: &copy; Mikael Damkier -
              Fotolia.com</font></P>
            </TD>
          <TD background=boxright.gif height="124"><IMG height=8 alt=""
            src="boxright.gif" width=8></TD>
        </TR>
        <TR vAlign=top align=left>
          <TD><IMG height=8 alt="" src="boxbottomleft.gif"
            width=8></TD>
          <TD background=boxbottom.gif><IMG height=8 alt=""
            src="boxbottom.gif" width=8></TD>
          <TD><IMG height=8 alt="" src="boxbottomright.gif"
            width=8></TD></TR></TBODY></TABLE></TD></TR>
  <TR>
    <TD vAlign=top align=left height="2">&nbsp; </TD>
</TR></TBODY></TABLE></BODY></HTML>
