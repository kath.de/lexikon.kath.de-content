---
title: Wirtschaftsethik und Moralökonomik
author: 
tags: 
created_at: 
images: ["http://www.kath.de/lexikon/Politik_Wirtschaft/banner_internetoekonomie_1.jpg", "http://www.kath.de/lexikon/Politik_Wirtschaft/wirtschaftsethik_6.jpg"]
---


# *
# *
			  ***[***](http://www.franz-hitze-haus.de/index.php?cat_id=10844&myELEMENT=Kursanzeige&kursnummer=07-836%20AT)			  Moral** - ein Thema für die Ökonomik? Diese von vielen lange Zeit zweifelnd vorgebrachte Frage wird inzwischen eindeutig affirmativ beantwortet. 

***Die veränderte  Einschätzung ist wohl nicht zufällig im zeitlichen Zusammenhang  mit neueren theoretischen Ansätzen der Ökonomik zu sehen, die als  Neue Institutionenökonomik oder Konstitutionenökonomik Buchananscher  Prägung in die Debatte Einzug gehalten haben: Durch ihre konzeptionelle  Unterscheidung zwischen individuellen Handlungen und den ihnen zugrunde  liegenden formellen wie informellen Regeln wurde die methodische  Voraussetzung für eine ertragreiche ökonomische Auseinandersetzung  mit dem "Phänomen Moral" geschaffen.

# *
***Im Zuge dieser  Entwicklung hat die ökonomische Analyse moralischer Normen - oder  kurz: die Moralökonomik** - neue oder erweiterte Einsichten  in die Rolle solcher Normen in einer modernen Marktwirtschaft beisteuern  können. Wie ein Blick in die jüngste Literatur zeigt, werden nicht  wenige der zum Teil neuen, zum Teil wiederentdeckten alten Erkenntnisse  über die Entstehung und (Nicht-) Befolgung solcher Normen, über  ihre Ursachen und Folgen zunehmend auch von Theologen und Moralphilosophen  anerkannt und (aus kritischer Distanz) in ihrer eigenen Forschungsarbeit  berücksichtigt. Außerdem werden vor dem Hintergrund der gewonnenen  Erkenntnisse auch normative Fragen mit der institutionenökonomischen  Methode in wohlverstandener Weise neu angegangen. 

***Auch können  im Zuge des neu begonnenen Diskurses zwischen Ethik und Ökonomik,  zwischen Ökonomen und Theologen bzw. Moralphilosophen** sowie  Vertretern anderer Disziplinen auch einige bisher als gesichert  geltende Hypothesen wirtschaftswissenschaftlicher Theorien in einem  anderen Licht erscheinen als zuvor: Lernen im interdisziplinären  Diskurs ist im Idealfall eben ein auf Gegenseitigkeit beruhender  Prozess. Ohne Zweifel sind nicht wenige der bisher vorliegenden  Beiträge von Ökonomen, insbesondere in Begründungsfragen, heftig  umstritten; die Frage einer Vernunft- oder Vorteilsbegründung der  Moral oder die, inwiefern denn nun in einer marktwirtschaftlich  verfassten Ordnung die Moral systematisch in den institutionellen  Rahmenbedingungen zu verorten sei, mögen hier nur als prägnante  Beispiele dienen.  

***Grundsätzlich  unbestritten ist gleichwohl die Fruchtbarkeit des neueren ökonomischen  Beitrages: Der Ansatz hat sich binnen weniger Jahre im interdisziplinären  Diskurs fest etabliert. Die Tatsache, dass um zahlreiche konzeptionelle  Fragen, um konkrete Anwendungen und mögliche Weiterentwicklungen  ebenso noch gerungen wird wie um angemessene Begriffe, darf dabei  nicht überraschen. Vielmehr fügt sich diese Beobachtung in das Bild  eines viel versprechenden neuen Forschungsprogramms. 

***In mehreren  interdisziplinären Fachtagungen hat die katholisch-soziale Akademie  ***[Franz Hitze Haus](http://www.franz-hitze-haus.de) in  Kooperation mit der wirtschaftswissenschaftlichen Fakultät der Universität  Münster konkrete wirtschaftsethische bzw. moralökonomische Fragestellungen  aufgegriffen, die in folgenden Tagungsbänden dokumentiert sind: 

***Aufderheide,  Detlef; Dabrowski, Martin** (Hg./1997): Wirtschaftsethik und Moralökonomik.  Normen, soziale Ordnung und der Beitrag der Ökonomik, Duncker &  Humblot, Berlin  

***Aufderheide,  Detlef; Dabrowski, Martin** (Hg./2000): Internationaler Wettbewerb  - Nationale Sozialpolitik? Wirtschaftsethische und moralökonomische  Perspektiven der Globalisierung, Duncker & Humblot, Berlin.  

***Aufderheide,  Detlef; Dabrowski, Martin** (Hg./2002): Gesundheit - Ethik - Ökonomik.  Wirtschaftsethische und moralökonomische Perspektiven des Gesundheitswesens,  Duncker & Humblot, Berlin  

# **Aufderheide,  Detlef; Dabrowski, Martin** (Hg./2005): Corporate Governance und  Korruption. Wirtschaftsethische und moralökonomische Perspektiven  der Bestechung und ihrer Bekämpfung, Duncker & Humblot, Berlin (im  Erscheinen) **
# ****
# ****
 © Akademie Franz Hitze Haus. Bild: © fred goldstein - Fotolia.com 
