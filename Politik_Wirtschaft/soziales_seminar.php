<HTML><HEAD><TITLE>Soziales Seminar</TITLE>
<META http-equiv=Content-Type content="text/html; charset=iso-8859-1"><LINK 
title=fonts href="kaltefleiter.css" type=text/css 
rel=stylesheet>
<META content="MSHTML 5.50.4134.600" name=GENERATOR></HEAD>
<BODY bgColor=#ffffff background="dstone1.gif" leftMargin=6 topMargin=6 marginwidth="6" marginheight="6">
<TABLE cellSpacing=0 cellPadding=6 width="100%" border=0>
  <TBODY>
  <TR>
    <TD vAlign=top align=left width=100 height="7"> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif" height="14"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2" height="14"><b>Politik und Wirtschaft</b></td>
          <td background="boxtopright.gif" height="14"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#FFFFFF"> <?php include("logo.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#FFFFFF" class="V10"> <?php include("az.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </TD>
    <TD vAlign=top rowSpan=2>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
        <TBODY>
        <TR vAlign=top align=left>
          <TD width=8><IMG height=8 alt="" 
            src="boxtopleftcorner.gif" width=8></TD>
          <TD background=boxtop.gif><IMG height=8 alt="" 
            src="boxtop.gif" width=8></TD>
          <TD width=8><IMG height=8 alt="" 
            src="boxtoprightcorner.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxtopleft.gif><IMG height=8 alt="" 
            src="boxtopleft.gif" width=8></TD>
          <TD bgColor=#e2e2e2> 
            <H1>Soziales Seminar</H1>
          </TD>
          <TD background=boxtopright.gif><IMG height=8 
            alt="" src="boxtopright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=13 alt="" src="boxdividerleft.gif" 
            width=8></TD>
          <TD background=boxdivider.gif><IMG height=13 
            alt="" src="boxdivider.gif" width=8></TD>
          <TD><IMG height=13 alt="" 
            src="boxdividerright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxleft.gif height="327"><IMG height=8 alt="" 
            src="boxleft.gif" width=8></TD>
          <TD height="327" bgcolor="#FFFFFF" class=L12> 
            <P><STRONG><font face="Arial, Helvetica, sans-serif" size="3"> </font></STRONG></P>
            <P><font face="Arial, Helvetica, sans-serif" size="3">Das Soziale 
              Seminar bietet langfristige und systematische politisch-soziale 
              Bildung in katholischer Trägerschaft an. Das Ziel ist die Bildung 
              von Menschen, die auf der Grundlage der Katholischen Soziallehre 
              zur konkreten Mitarbeit an der Gestaltung der Gesellschaft befähigt 
              und ermutigt werden sollen.</font></P>
            <P><font face="Arial, Helvetica, sans-serif" size="3">Die Umsetzung 
              der Katholischen Soziallehre setzt Menschen voraus, die sich - sachkundig 
              und den konkreten Herausforderungen bewusst - für sie einsetzen 
              und so zu einem gerechten und menschenwürdigen Leben beitragen. 
              Die Ortskirchen stehen in der Verantwortung, ihren Gläubigen die 
              nötige politisch-soziale Bildung - das gesellschaftliche, wirtschaftliche 
              und politische Sachwissen, die Erarbeitung der Werthaltungen und 
              Handlungsmaßstäbe und die Befähigung zum gesellschaftlichen Engagement 
              - anzubieten und sie zur Teilnahme an diesem Bildungsangebot zu 
              ermutigen.</font></P>
            <P><font face="Arial, Helvetica, sans-serif" size="3">Im Sozialen 
              Seminar wird diese Bildungsarbeit seit den frühen fünfziger Jahren 
              in Trägerschaft der jeweiligen Diözese geleistet. Die Langfristigkeit 
              der Angebote des Seminars, die Systematik und die gemeinsame Arbeit 
              in festen Gruppen gewährleisten, dass die Kirche ihren wichtigen 
              Beitrag zu einer sozial gerechteren Gestaltung unserer Gesellschaft 
              einbringen kann. Die Angebote des Sozialen Seminars sind offen für 
              alle Menschen, die sich auf den Weg sozialer Verantwortung begeben 
              wollen.</font></P>
            <P><font face="Arial, Helvetica, sans-serif" size="3">Die katholisch-soziale 
              Akademie <a href="http://www.franz-hitze-haus.de">Franz Hitze Haus</a>, 
              die zu den <a href="http://www.franz-hitze-haus.de/index.php?cat_id=1218&amp;menuid=1218">Gründungsmitgliedern 
              der Bundesarbeitsgemeinschaft der Sozialen Seminare</a> gehört, 
              gibt seit 1952 den <a href="http://www.franz-hitze-haus.de/index.php?cat_id=1218&amp;menuid=1218">Informationsbrief 
              "Soziales Seminar"</a> heraus, der aktuelle wirtschafts- und sozialpolitische 
              Themen behandelt. Alle Absolventen der Sozialen Seminare erhalten 
              diesen Rundbrief kostenlos, Einzelexemplare können bei Interesse 
              über die Akademie <a href="http://www.franz-hitze-haus.de">Franz 
              Hitze Haus</a> kostenlos angefordert werden.</font><font size="3" face="Arial, Helvetica, sans-serif"><br>
              </font><font size="3" face="Arial, Helvetica, sans-serif"><br>
              </font><font face="Arial, Helvetica, sans-serif" size="3"><br>
              &copy; Akademie Franz Hitze Haus. </font></P>
            </TD>
          <TD background=boxright.gif height="327"><IMG height=8 alt="" 
            src="boxright.gif" width=8></TD>
        </TR>
        <TR vAlign=top align=left>
          <TD><IMG height=8 alt="" src="boxbottomleft.gif" 
            width=8></TD>
          <TD background=boxbottom.gif><IMG height=8 alt="" 
            src="boxbottom.gif" width=8></TD>
          <TD><IMG height=8 alt="" src="boxbottomright.gif" 
            width=8></TD></TR></TBODY></TABLE></TD></TR>
  <TR>
    <TD vAlign=top align=left height="2">&nbsp; </TD>
</TR></TBODY></TABLE></BODY></HTML>
