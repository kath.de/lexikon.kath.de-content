<HTML><HEAD><TITLE>Arzthaftung bei Abtreibung</TITLE>
<META http-equiv=Content-Type content="text/html; charset=iso-8859-1"><LINK 
title=fonts href="kaltefleiter.css" type=text/css 
rel=stylesheet>
<META content="MSHTML 5.50.4134.600" name=GENERATOR></HEAD>
<BODY bgColor=#ffffff background="dstone1.gif" leftMargin=6 topMargin=6 marginwidth="6" marginheight="6">
<TABLE cellSpacing=0 cellPadding=6 width="100%" border=0>
  <TBODY>
  <TR>
    <TD vAlign=top align=left width=100> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif" height="14"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2" height="14"><b>Politik und Wirtschaft</b></td>
          <td background="boxtopright.gif" height="14"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#FFFFFF"> <?php include("logo.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#FFFFFF" class="V10"> <?php include("az.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </TD>
    <TD vAlign=top rowSpan=2>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
        <TBODY>
        <TR vAlign=top align=left>
          <TD width=8><IMG height=8 alt="" 
            src="boxtopleftcorner.gif" width=8></TD>
          <TD background=boxtop.gif><IMG height=8 alt="" 
            src="boxtop.gif" width=8></TD>
          <TD width=8><IMG height=8 alt="" 
            src="boxtoprightcorner.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxtopleft.gif><IMG height=8 alt="" 
            src="boxtopleft.gif" width=8></TD>
          <TD bgColor=#e2e2e2> 
            <H1><strong><font face="Arial, Helvetica, sans-serif" size="6">Arzthaftung
                bei Abtreibung</font></strong></H1>
          </TD>
          <TD background=boxtopright.gif><IMG height=8 
            alt="" src="boxtopright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=13 alt="" src="boxdividerleft.gif" 
            width=8></TD>
          <TD background=boxdivider.gif><IMG height=13 
            alt="" src="boxdivider.gif" width=8></TD>
          <TD><IMG height=13 alt="" 
            src="boxdividerright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxleft.gif height="124"><IMG height=8 alt="" 
            src="boxleft.gif" width=8></TD>
          <TD height="124" bgcolor="#FFFFFF" class=L12> 
            <P><STRONG><font face="Arial, Helvetica, sans-serif">Wenn
                  die Abtreibung nicht gelingt, 
haftet der Arzt</font></STRONG></P>
            <P><font face="Arial, Helvetica, sans-serif">Folgender Fall: Eine
                Mutter will ihr Kind abtreiben lassen. Sie schlie&szlig;t mit
                dem Arzt einen Abtreibungsvertrag. Dieser Vertrag ist, wie das
                Bundesverfassungsgericht noch 1993 festgestellt
                hat, wegen der Rechtswidrigkeit der Abtreibung sittenwidrig und
                der Arzt hat deshalb keinen Anspruch auf ein Honorar. In einer
                sp&auml;teren Entscheidung hat dasselbe Gericht jedoch erkl&auml;rt,
                die rechtswidrige T&ouml;tung des Kindes unterliege ausnahmsweise
                dem Schutz der Berufsfreiheit des Art 12 GG. Denn &#8222;die
                T&auml;tigkeit des Arztes ist notwendiger Bestandteil des gesetzlichen
                Schutzkonzepts&#8220;. So kommt auch der Abtreibungsarzt zu seinem
                Honorar. </font><font face="Arial, Helvetica, sans-serif">Das
                gilt jedenfalls, wenn die Abtreibung &#8222;gelingt&#8220;.</font></P>
            <p><font face="Arial, Helvetica, sans-serif">&#8222;Gelingt&#8220; sie nicht und kommt das Kind lebend zur
              Welt &#8211; und das kommt immer wieder mal vor -, so verliert
              der Arzt nicht nur seinen Honoraranspruch. Er ist dar&uuml;ber
              hinaus den Eltern des Kindes zum Schadensersatz verpflichtet. Er
              hat f&uuml;r die Unterhaltskosten des Kindes so lange aufzukommen,
              wie die Eltern daf&uuml;r aufkommen m&uuml;ssen. </font><font face="Arial, Helvetica, sans-serif">Der
              Pflichtenkreis des Arztes stellt sich bei einer Abtreibung folgenderma&szlig;en
                dar: </font></p>
            <p><font face="Arial, Helvetica, sans-serif">&#8226; Aufgrund seines wirksamen Vertrages mit der Mutter ist
              er verpflichtet, den Tod des ungeborenen Kindes im Mutterleib herbeizuf&uuml;hren.
              Gelingt ihm das nicht und kommt das Kind lebend zur Welt, hat er
              diese Verpflichtung schuldhaft verletzt. Ihm drohen m&ouml;glicherweise
              hohe Schadensersatzforderungen, sofern das Kind nicht doch noch
              eines nat&uuml;rlichen Todes stirbt und damit der Schadensgrund
              nachtr&auml;glich entf&auml;llt.</font></p>
            <p><font face="Arial, Helvetica, sans-serif">&#8226; Vom Zeitpunkt
                der Geburt an &auml;ndert sich die Aufgabe
              des Arztes. Seine vertragliche Pflicht ist gescheitert und damit
              erledigt. Aufgrund seiner &auml;rztlichen Pflicht mu&szlig; er
              nun alles tun, um das Leben des zu diesem Zeitpunkt oft schwer
              gesch&auml;digten Kindes zu erhalten. Anders formuliert: Er mu&szlig; alles
              tun, da&szlig; der Schadensgrund erhalten bleibt und Schadensersatzforderungen
              gegen ihn bestehen bleiben. Da&szlig; ein Arzt in dieser Situation
              der Versuchung erliegt, dem Tod des bereits geborenen Kindes ein
              wenig nachzuhelfen, ist leider kein Einzelfall.</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Von Schadensersatzzahlungen
                gleicher Art ist auch der Arzt bedroht, der nach einer pr&auml;natalen Diagnose (PND) eine Sch&auml;digung
              des Kindes nicht zuverl&auml;ssig erkennt, die Mutter nicht auf
              die M&ouml;glichkeit der Abtreibung hinweist, diese deshalb das
              Kind nicht abtreibt und das Kind schlie&szlig;lich behindert zur
              Welt kommt. Und das, obwohl die Abtreibung allein wegen einer Sch&auml;digung
              des ungeborenen Kindes (eugenische Indikation) im Gesetz nicht
              vorgesehen ist. Konsequenz daraus: Der Arzt ist zur Vermeidung
              von Schadensersatzanspr&uuml;chen &#8222;gut&#8220; beraten, einer
              werdenden Mutter &#8222;im Zweifel&#8220; vorsorglich zur Abtreibung
              zu raten.</font></p>
            <p><font face="Arial, Helvetica, sans-serif"> Diese wenigen Beispiele
                zeigen die ganze Widerspr&uuml;chlichkeit
              und Paradoxie der <a href="abtreibungsregelung_rechtliche_bewertung.php">Abtreibungsregelung</a> in
              Deutschland (rechtswidrig, aber straflos; vom Staate mi&szlig;billigt,
              aber gleichzeitig gef&ouml;rdert
              und finanziert). Abgeholfen wird dieser Widerspr&uuml;chlichkeit
              nicht durch Hinzuf&uuml;gung einer zweiten Widerspr&uuml;chlichkeit,
              die darin besteht, da&szlig; man das Kind als Schadensgrund ausschlie&szlig;t.
              Denn wenn jemandem eine Unterhaltspflicht aufgeb&uuml;rdet wird,
              dann ist das nun einmal ein finanzieller Nachteil und damit ein
              Schaden. Hilfe kann nur durch eine &Auml;nderung der Abtreibungsregelung
              kommen.</font></p>
            <p><font face="Arial, Helvetica, sans-serif">Wenn unser
                Staat das Leben sch&uuml;tzen will, dann bitte
                    konsequent, wenn er ein Handeln mi&szlig;billigt, dann bitte
                  nicht nur in Worten, sondern auch in Werken.</font></p>            <p></p>
            <p><font face="Arial, Helvetica, sans-serif"><br>
              </font><font face="Arial, Helvetica, sans-serif" size="3"><a href="http://www.hmk-leben.de/">&copy;</a></font><a href="http://www.hmk-leben.de/"> <font face="Arial, Helvetica, sans-serif">Dr.
              Konrad Schneller</font></a><br>
            </p>
            <P><font face="Arial, Helvetica, sans-serif" size="3"><br>
              </font></P>
            </TD>
          <TD background=boxright.gif height="124"><IMG height=8 alt="" 
            src="boxright.gif" width=8></TD>
        </TR>
        <TR vAlign=top align=left>
          <TD><IMG height=8 alt="" src="boxbottomleft.gif" 
            width=8></TD>
          <TD background=boxbottom.gif><IMG height=8 alt="" 
            src="boxbottom.gif" width=8></TD>
          <TD><IMG height=8 alt="" src="boxbottomright.gif" 
            width=8></TD></TR></TBODY></TABLE></TD></TR>
  <TR>
    <TD vAlign=top align=left height="2">&nbsp; </TD>
</TR></TBODY></TABLE></BODY></HTML>
