<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Inspirierend Leiten</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="kaltefleiter.css" title="fonts">
</head>
<body bgcolor="#ffffff" leftmargin="6" topmargin="6" marginwidth="6" marginheight="6">
<table width="100%" border="0" cellspacing="0" cellpadding="6">
  <tr>
    <td width="100" height="78" align="left" valign="top"><table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><b>Leitung und Training</b></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td>
            <?php include("logo.html"); ?>
          </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
    <td rowspan="2" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2">
            <h1> <font face="Arial, Helvetica, sans-serif">Inspirierend Leiten
              </font> </h1>
          </td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="L12">

            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td valign="top" class="L12"><P STYLE="margin-bottom: 0cm"><a href="http://cms.weiterbildung-live.de/qualitaet.html"><font face="Arial, Helvetica, sans-serif">&#034;Inspirierend
                        Leiten&#034;</font></a><font face="Arial, Helvetica, sans-serif"> meint,
                        dass F&uuml;hrungskr&auml;fte die vielf&auml;ltigen Leitungsaufgaben
                        kompetent, engagiert<FONT COLOR="#008000">, </FONT>konstruktiv
                        und sicher leisten k&ouml;nnen und das so, da&szlig; das
                        Team, die Gruppe inspiriert bleiben.. </font></P>
                  <P> <font face="Arial, Helvetica, sans-serif">F&uuml;r die
                      Leitung stehen einzelne Verfahren zur Verf&uuml;gung, z.b.
                      die Konferenztechnik, um <a href="http://cms.weiterbildung-live.de/profil.html">Entscheidungsprozesse</a> moderieren. &Uuml;berhaupt
                      geht es darum, durch Leitungsverfahren, Gespr&auml;che
                      zu strukturieren, Entscheidungen zu treffen, Mitarbeitergespr&auml;che
                      zu f&uuml;hren, Ziele zu finden und zu verfolgen, <a href="http://cms.weiterbildung-live.de/42.html">Konflikte</a> zu
                      moderieren sowie die F&auml;higkeit, Angebote und Produkte
                      so zu entwickeln, so dass die Philosophie, die Werte des
                      Unternehmens mit den Erwartungen der Kunden verbunden werden.
                      F&uuml;hrungskr&auml;fte ben&ouml;tigen Fortbildungen und
                      Coachings, um sich diese F&auml;higkeiten anzueignen. In
                      alle diese Leitungsaufgaben flie&szlig;t &#034;Inspiration&#034;,
                      wenn sie sich an den Werten der Einrichtung, des Unternehmens,
                      der Institution orientieren. Solche Werte sind z.B. in
                      Erziehungseinrichtungen &#034;selbst&auml;ndig werden&#034;, &#034;zusammen
                      spielen und arbeiten lernen&#034;. Wenn die Leitung diese
                      Werte im Team pr&auml;sent h&auml;lt und die Entscheidungsfindung
                      danach ausrichtet, da&szlig; Kinder diese Werte verwirklichen
                      k&ouml;nnen, ist das inspirierend. In einer Textilabteilung
                      eines Kaufhauses sind die Werte, die die Mitarbeiter inspirieren,
                      da&szlig; der Kunde, die Kundin sich &#034;gut angezogen&#034; f&uuml;hlen.
                      In Arztpraxen und Krankenh&auml;usern sind es die Werte,
                      die mit der Gesundheit zu tun haben. Religi&ouml;se Einrichtungen
                      k&ouml;nnen jeweils auf eine Spiritualit&auml;t zur&uuml;ckgreifen,
                      die ein Team inspiriert und von der Leitung im Blick behalten
                      werden soll. Die Inspiration wird nicht zuletzt dadurch
                      wirksam, da&szlig; die Zielgruppen und Kunden die Werte
                      erleben und die Mitarbeiter davon erfahren. &#034;Inspirierend
                      Leiten&#034; meint auch die MitarbeiterInnen darin zu unterst&uuml;tzen,
                      ihre Potentiale auszusch&ouml;pfen und sich weiter zu entwickeln. </font></P>
                  <P><font size="3" face="Arial, Helvetica, sans-serif">Sinnvoll
                      sind Fortbildungen, die an den Anforderungen der Leitungskr&auml;fte
                      ansetzen, die mehrteilig angelegt sind, die ein zus&auml;tzliches
                      individuelles <a href="http://cms.weiterbildung-live.de/coaching.html">Coaching</a> anbieten,
                      die &Uuml;bungsraum und R&uuml;ckmeldungen erm&ouml;glichen,
                      in denen die Rolle und die Leitungsaufgaben reflektiert
                      werden.</font></P>
                  <P><font face="Arial, Helvetica, sans-serif" size="2">Buchtipp: <a href="http://www.kathshop.de/wwwkathde/product_info.php?cPath=&amp;products_id=42&amp;anbieter=8&amp;page=&amp;">Inspirierend
                        leiten</a></font></P>
                  <P> </P>
                  <P></P>
                  <P> </P>
                  <P></P>
                  <P></P>
                  <P> &copy; <a href="http://cms.weiterbildung-live.de/qualitaet.html"><font size="2" face="Arial, Helvetica, sans-serif">weiterbildung
                lieve</font></a></P></td>
                 <td valign="top" align=Right>

<p><a href="http://www.neufeld-verlag.de/" target="_blank"><img src="http://www.kath.de/lexikon/b_hinweise/neufeld.jpg" width="286" height="238" border="0"></a><br>

                <script type="text/javascript"><!--
google_ad_client = "pub-9537752113242914";
google_ad_width = 160;
google_ad_height = 600;
google_ad_format = "160x600_as";
google_ad_type = "text_image";
//2007-09-05: Bildung und Erziehung
google_ad_channel = "3955521420";
google_ui_features = "rc:6";
//-->
            </script>
                    <script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
              </script>
                </td>
              </tr>
            </table>            <P STYLE="margin-bottom: 0cm">&nbsp;</P>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td align="left" valign="top"><table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td class="V10">
            <?php include("az.html"); ?>
          </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</body>
</html>