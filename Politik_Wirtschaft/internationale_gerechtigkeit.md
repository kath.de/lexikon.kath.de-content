---
title: Internationale Gerechtigkeit
author: 
tags: 
created_at: 
images: []
---


# ** *
***"Internationale  Gerechtigkeit"** ist das allgemeine und zentrale Ziel aller entwicklungspolitischen  Anstrengungen. Dieser Begriff steht für die Schaffung und Sicherung  von Lebensbedingungen, die allen Menschen in Lateinamerika, Afrika  und Asien optimale Entwicklungsmöglichkeiten eröffnen. Grobziele  sind Politische Stabilität, Arbeit, Wirtschaftliche Leistungsfähigkeit,  Partizipation, Ökologische Tragfähigkeit und Eigenständigkeit.  

***Das ***[Franz  Hitze Haus](http://www.franz-hitze-haus.de) - die Akademie des Bistums Münster - hat einen eigenen  ***[Fachbereich  "Politik und Zeitgeschichte, Internationale Gerechtigkeit"](http://www.franz-hitze-haus.de/index.php?myELEMENT=34113).  Für die Praxis dieses Fachbereiches gilt, dass in einer großen Zahl  von Bildungsveranstaltungen die Situation in Afrika, Asien und Lateinamerika  zum Thema gemacht wird und dass aus einem entwicklungspolitischen  Motiv globale Zusammenhänge zur Diskussion kommen. Denn die wichtigsten  politischen Faktoren zu kennen - national und international -, das  ist die unverzichtbare Voraussetzung, sich für eine Veränderung  von ungerechten Verhältnissen einsetzen zu können.  

***Seit Jahren  schon sind dabei die Jahrestagungen der beiden münsterschen Diözesan-Partnerschaften  Münster mit den fünf Bistümern in Nordghana und mit dem Bistum Tula  in Mexiko Fixpunkte im Veranstaltungskalender des Franz Hitze Hauses.  Ebenso regelmäßig werden die Jahresaktionen der Hilfswerke Adveniat,  Misereor und Missio mit ihren aktuellen Bildungsschwerpunkten und  Beispielländern vorgestellt. Seit der weltpolitischen Zäsur 1989  hat sich die Aufmerksamkeit stärker als bisher auch auf die Staaten  Mittel- und Osteuropas gerichtet.  

***Aus dem Grundauftrag  der Akademie, eine Verbindung zwischen dem gesellschaftlichen Teilsystem  Religion und den anderen Systemen, wie beispielsweise Wirtschaft,  Wissenschaft und Politik, herzustellen, ergibt sich, dass zum Dialog  in gleicher Weise Fachleute wie auch allgemein politisch Interessierte  eingeladen sind. Großen Wert wird darauf gelegt, Veranstaltungen  gemeinsam mit Kooperationspartner aus dem kirchlichen und außerkirchlichen  Spektrum anzubieten; entwicklungspolitischen Nichtregierungsorganisationen  kommt dabei eine herausragende Bedeutung zu. 

# **Politische Bildung  in der Akademie Franz Hitze Haus und andernorts leistet einen spezifischen,  unersetzbaren Beitrag zur Identitätsentwicklung: Sie formt das kritische,  demokratische Bewusstsein und sie fördert solidarische, partizipationsorientierte  Verhaltensweisen. Im christlichen Kontext stellt sie die Situation  von Benachteiligten weltweit in den Mittelpunkt. Leitziel: Internationale  Gerechtigkeit. *****
# *
 Weitere Informationen:***

# ******
# **[Fachbereich  "Politik und Zeitgeschichte, Internationale Gerechtigkeit"  der Akademie Franz Hitze Haus](http://www.franz-hitze-haus.de/index.php?myELEMENT=34113)**
# ****
 © Akademie Franz Hitze Haus.  
