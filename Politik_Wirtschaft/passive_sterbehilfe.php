<HTML><HEAD><TITLE>Passive Sterbehilfe</TITLE>
<META http-equiv=Content-Type content="text/html; charset=iso-8859-1"><LINK 
title=fonts href="kaltefleiter.css" type=text/css 
rel=stylesheet>
<META content="MSHTML 5.50.4134.600" name=GENERATOR></HEAD>
<BODY bgColor=#ffffff background="dstone1.gif" leftMargin=6 topMargin=6 marginwidth="6" marginheight="6">
<TABLE cellSpacing=0 cellPadding=6 width="100%" border=0>
  <TBODY>
  <TR>
    <TD vAlign=top align=left width=100> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif" height="14"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2" height="14"><b>Politik und Wirtschaft</b></td>
          <td background="boxtopright.gif" height="14"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#FFFFFF"> <?php include("logo.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#FFFFFF" class="V10"> <?php include("az.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </TD>
    <TD vAlign=top rowSpan=2>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
        <TBODY>
        <TR vAlign=top align=left>
          <TD width=8><IMG height=8 alt="" 
            src="boxtopleftcorner.gif" width=8></TD>
          <TD background=boxtop.gif><IMG height=8 alt="" 
            src="boxtop.gif" width=8></TD>
          <TD width=8><IMG height=8 alt="" 
            src="boxtoprightcorner.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxtopleft.gif><IMG height=8 alt="" 
            src="boxtopleft.gif" width=8></TD>
          <TD bgColor=#e2e2e2> 
            <H1><strong><font face="Arial, Helvetica, sans-serif">Passive
                Sterbehilfe</font></strong></H1>
          </TD>
          <TD background=boxtopright.gif><IMG height=8 
            alt="" src="boxtopright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=13 alt="" src="boxdividerleft.gif" 
            width=8></TD>
          <TD background=boxdivider.gif><IMG height=13 
            alt="" src="boxdivider.gif" width=8></TD>
          <TD><IMG height=13 alt="" 
            src="boxdividerright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxleft.gif height="124"><IMG height=8 alt="" 
            src="boxleft.gif" width=8></TD>
          <TD height="124" bgcolor="#FFFFFF" class=L12> 
            <P><font face="Arial, Helvetica, sans-serif"><strong>Passive &quot;Sterbehilfe&quot;<br>
  - besser: Aktive Sterbebegleitung. </strong></font></P>
            <p><font face="Arial, Helvetica, sans-serif">Grundidee: Jeder Mensch
                hat das Recht auf seinen nat&uuml;rlichen
              Tod </font></p>
            <p><font face="Arial, Helvetica, sans-serif">Grenze: Es gibt kein
                Recht zur Selbstt&ouml;tung (Suizidverbot) </font></p>
            <p><font face="Arial, Helvetica, sans-serif">- Der Verzicht auf m&ouml;gliche weiterf&uuml;hrende
                medizinische Behandlung bzw. deren Abbruch ist erlaubt. </font></p>
            <p><font face="Arial, Helvetica, sans-serif">- Dieses Selbstbestimmungsrecht
                ist die unbestrittene Grundlage einer wirksamen Patientenverf&uuml;gung. </font></p>
            <p><font face="Arial, Helvetica, sans-serif">Grunds&auml;tzlich versteht man unter passiver/indirekter &#8222;Sterbehilfe&#8220; den
              Einsatz leidvermindernder Mittel bei Sterbenden/Todkranken - unter
              Inkaufnahme des Risikos der Beschleunigung des Todes. Dazu geh&ouml;rt
              auch der vom Arzt angeordnete Behandlungsverzicht/-abbruch, auch
              durch aktives Tun (z.B. Abschalten eines Ger&auml;tes). Ein spezieller
              Fall ist die im Rahmen der Palliativmedizin angewandte modulierbare &#8222;palliative&#8220; Sedierung. </font></p>
            <p><font face="Arial, Helvetica, sans-serif">Frage: Rechtlich anders
                zu bewerten ist die mi&szlig;gl&uuml;ckte
              Operation mit Todesfolge. </font></p>
            <p><font face="Arial, Helvetica, sans-serif">Entscheidend ist bei
                jeder Ma&szlig;nahme die Intention: das Ziel
              ist nicht der (fr&uuml;here) Tod des Patienten, sondern durch Leidverminderung
              die Verbesserung der Lebensqualit&auml;t des Sterbenden in der
              letzten Phase des &#8222;Lebens vor dem Lebensende&#8220;. </font></p>
            <p><font face="Arial, Helvetica, sans-serif">Deshalb handelt es sich
                nicht um &#8222;Sterbehilfe&#8220;, sondern
              um Hilfe &#8222;im&#8220; Sterben &#8211; um aktive Sterbebegleitung. </font></p>
            <p><font face="Arial, Helvetica, sans-serif">- Da &Auml;rzte diese therapeutischen Abw&auml;gungen regelm&auml;&szlig;ig
              treffen m&uuml;ssen, wehren sie sich (zu Recht) gegen den Begriff
              der &#8222;passiven/indirekten Sterbehilfe&#8220;. </font></p>
            <p><font face="Arial, Helvetica, sans-serif">Der Begriff der &#8222;Behandlungsbed&uuml;rftigkeit&#8220; (Kirchhof)
              k&ouml;nnte ein guter Ma&szlig;stab f&uuml;r die Beurteilung der
              zu treffenden Entscheidung sein. </font></p>
            <p> </p>
            <p><font face="Arial, Helvetica, sans-serif">Es gibt &Uuml;berlegungen, die strafrechtlichen Folgen der passiven/indirekten &#8222;Sterbehilfe&#8220; zu
              kl&auml;ren: Dem &sect; 216 StGB (&#8222;T&ouml;tung auf Verlangen&#8220;)
              k&ouml;nnte ein dritter Absatz beigef&uuml;gt werden: </font></p>
            <p><font face="Arial, Helvetica, sans-serif">&#8222;Nicht strafbar
                ist </font></p>
            <p><font face="Arial, Helvetica, sans-serif">1. die Anwendung einer
                medizinisch angezeigten leidmindernden Ma&szlig;nahme, die das Leben als nicht beabsichtigte Nebenwirkung
              verk&uuml;rzt (= indirekte SH) </font></p>
            <p><font face="Arial, Helvetica, sans-serif">2. das Unterlassen oder
                Beenden einer lebenserhaltenden medizinischen Ma&szlig;nahme, wenn dies dem Willen des Patienten entspricht&#8220; (=
              passive SH). </font></p>
            <p><font face="Arial, Helvetica, sans-serif">Dezember 2008 </font> </p>
            <p></p>
            <P><font face="Arial, Helvetica, sans-serif" size="3"><br>
              &copy; </font><font face="Arial, Helvetica, sans-serif">J. Beckermann,
              <a href="http://www.hmk-leben.de/">Hilfe f&uuml;r Mutter und Kind
              e.V.</a></font></P>
            </TD>
          <TD background=boxright.gif height="124"><IMG height=8 alt="" 
            src="boxright.gif" width=8></TD>
        </TR>
        <TR vAlign=top align=left>
          <TD><IMG height=8 alt="" src="boxbottomleft.gif" 
            width=8></TD>
          <TD background=boxbottom.gif><IMG height=8 alt="" 
            src="boxbottom.gif" width=8></TD>
          <TD><IMG height=8 alt="" src="boxbottomright.gif" 
            width=8></TD></TR></TBODY></TABLE></TD></TR>
  <TR>
    <TD vAlign=top align=left height="2">&nbsp; </TD>
</TR></TBODY></TABLE></BODY></HTML>
