---
title: Hospiz
author: 
tags: 
created_at: 
images: []
---


# **HOSPIZ: - „Menschenwürdiges Leben vor dem Lebensende – vor dem Sterben“ *
# **Antwort auf geäußerte Sterbewünsche - und die „Angebote“ von DIGNITAS *
***Wegen als unerträglich empfundener Schmerzen aber auch aus anderen Gründen suchen einige Schwerstkranke nach einer sicheren Möglichkeit, ihr Leben selbstbestimmt zu beenden. Gerade weil wir als Christen diesen Wünschen nicht (einfach?) im Wege der „Beihilfe zum Suizid“ entsprechen können, bedarf es um so mehr des behutsamen Eingehens auf diese Bitten. Sie müssen ernstgenommen, aber sie müssen auch interpretiert, d.h. in der Tiefe verstanden, werden. Bei behutsamem Nachfragen sind in den „Sterbewünschen“ nur allzu oft verschiedene Ängste enthalten, u.a. vor: unerträglichen Schmerzen, dem Alleinsein, sozialer Ausgrenzung, als zu stark empfundener Belastung der Angehörigen.  

***Eine mögliche Antwort ist die intensive, persönliche Betreuung - entweder im Wege der palliativen (wärmenden) Medizin und/oder bei Sterbenskranken in speziell für diesen Zweck eingerichteten Hospizen. Soweit möglich, personell und sachlich, können deren Dienstleistungen zumindest teilweise auch zuhause angeboten werden. Erfahrungsgemäß kann in diesen Einrichtungen der ursprünglich geäußerte „Sterbewunsch“ gut „aufgefangen“ und beantwortet werden.  

***Die Begleitung und Hilfe für die Betroffenen – Patienten und deren Angehörige – im Hospiz im Sinne einer umfassenden (rounded care“) Betreuung auf dem Weg zu einem menschenwürdigen Sterben hat seit dem Mittelalter eine lange christliche Tradition. (lat. hospitium: Gastfreundschaft, Herberge).  

***Die meisten Menschen möchten zu Hause, zumindest in ihrer vertrauten Umgebung sterben (aber: 70% sterben in einem Krankenhaus). Dazu bedarf es einer weit größeren Zahl in der Palliativmedizin ausgebildeter (Haus)-Ärzte – für die ambulante und/oder die stationäre Betreuung.  

***Wohl auch als Antwort auf die Auflösung der Familienbande und die zunehmende Anzahl der Single-Haushalte hat sich bei uns (die Wurzeln der modernen Hospizbewegung liegen in England) die Zahl der Hospize und der Palliativstationen erfreulich vermehrt – auf über 75 Palliativstationen und mehr als 100 Hospize. Diese Erhöhung ist zwar erfreulich, aber noch lange nicht ausreichend; in der Stadt Frankfurt gibt es z. Zt. nur ein Hospiz (Sankt Katharina).  

***Um dem Sterbenden die Möglichkeit zu geben, in der gewohnten Atmospähre seines Zimmers – im Alten- bzw. Pflegeheim – Abschied vom Leben nehmen zu können, gibt es Überlegungen, den Hospizgedanken in die Heime/Stationen zu integrieren – integriertes Hospiz.  

***Damit Angehörige die Pflege schwerstkranker Angehöriger übernehmen können, schlägt das Zentralkomitee der Deutschen Katholiken (ZdK) die Einführung einer „Pflegezeit“ (mit Arbeitsplatzgarantie) vor. In Österreich erleichtert der Gesetzgeber schon jetzt die Betreuung sterbenskranker Familienangehöriger für die Dauer von bis zu sechs Monaten. Dazu hat er die „Familienhospizkarenz“ geschaffen.  

***Aus den USA kommen ernüchternde Nachrichten. Die hohe emotionale Belastung der Betreuer führt in den Staaten, welche den assistierten Suizid gesetzlich erlaubt haben, zu einem meßbaren Anstieg dieses „verweigerten Mit-Leidens“.  

***Da zunehmend Palliativ-/Hospiz-Stationen (direkt oder indirekt) in christlicher Trägerschaft geführt werden, ergibt sich die große Chance, einen gesellschaftlich akzeptierten Gegenentwurf entweder zu der geforderten Legalisierung der aktiven Sterbehilfe und/oder Angeboten wie denen von DIGNITAS nicht nur zu fordern, sondern auch konkret zu leben.  

***Gesundheit ist zwar ein erstrebenswertes Ziel, aber nicht der Maßstab für den Wert des Lebens. Auch Leiden kann einen Sinn haben. In einer Gesellschaft, in der Jugend, Gesundheit und blendendes (wen?) Aussehen einen hohen Stellenwert haben, muß jemand wie Papst Johannes Paul II in dem offenen Umgang mit seiner Krankheit und seinem Tod ein Stein des Anstoßes sein – aber auch für viele Kranke ein Grund der Hoffnung. Im Tod traf auf ihn die uns heute fast nicht mehr geläufige/verständliche Beschreibung zu: „Er hat das Zeitliche gesegnet“.  

***Dezember 2008 

# ****
 © ***J. Beckermann,„***[Hilfe für Mutter und Kind e.V.](http://www.hmk-leben.de/) 
