<HTML><HEAD><TITLE>Hospiz</TITLE>
<META http-equiv=Content-Type content="text/html; charset=iso-8859-1"><LINK 
title=fonts href="kaltefleiter.css" type=text/css 
rel=stylesheet>
<META content="MSHTML 5.50.4134.600" name=GENERATOR></HEAD>
<BODY bgColor=#ffffff background="dstone1.gif" leftMargin=6 topMargin=6 marginwidth="6" marginheight="6">
<TABLE cellSpacing=0 cellPadding=6 width="100%" border=0>
  <TBODY>
  <TR>
    <TD vAlign=top align=left width=100> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif" height="14"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2" height="14"><b>Politik und Wirtschaft</b></td>
          <td background="boxtopright.gif" height="14"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#FFFFFF"> <?php include("logo.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#FFFFFF" class="V10"> <?php include("az.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </TD>
    <TD vAlign=top rowSpan=2>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
        <TBODY>
        <TR vAlign=top align=left>
          <TD width=8><IMG height=8 alt="" 
            src="boxtopleftcorner.gif" width=8></TD>
          <TD background=boxtop.gif><IMG height=8 alt="" 
            src="boxtop.gif" width=8></TD>
          <TD width=8><IMG height=8 alt="" 
            src="boxtoprightcorner.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxtopleft.gif><IMG height=8 alt="" 
            src="boxtopleft.gif" width=8></TD>
          <TD bgColor=#e2e2e2> 
            <H1><strong><font face="Arial, Helvetica, sans-serif">Hospiz</font></strong></H1>
          </TD>
          <TD background=boxtopright.gif><IMG height=8 
            alt="" src="boxtopright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=13 alt="" src="boxdividerleft.gif" 
            width=8></TD>
          <TD background=boxdivider.gif><IMG height=13 
            alt="" src="boxdivider.gif" width=8></TD>
          <TD><IMG height=13 alt="" 
            src="boxdividerright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxleft.gif height="124"><IMG height=8 alt="" 
            src="boxleft.gif" width=8></TD>
          <TD height="124" bgcolor="#FFFFFF" class=L12> 
            <P><font face="Arial, Helvetica, sans-serif"><strong>HOSPIZ: - &#8222;Menschenw&uuml;rdiges Leben vor dem Lebensende &#8211; vor
              dem Sterben&#8220; </strong></font></P>
            <p><strong><font face="Arial, Helvetica, sans-serif">Antwort auf
                  ge&auml;u&szlig;erte Sterbew&uuml;nsche - und die &#8222;Angebote&#8220; von
              DIGNITAS </font></strong></p>
            <p><font face="Arial, Helvetica, sans-serif">Wegen als unertr&auml;glich empfundener Schmerzen aber auch aus
              anderen Gr&uuml;nden suchen einige Schwerstkranke nach einer sicheren
              M&ouml;glichkeit, ihr Leben selbstbestimmt zu beenden. Gerade weil
              wir als Christen diesen W&uuml;nschen nicht (einfach?) im Wege
              der &#8222;Beihilfe zum Suizid&#8220; entsprechen k&ouml;nnen,
              bedarf es um so mehr des behutsamen Eingehens auf diese Bitten.
              Sie m&uuml;ssen ernstgenommen, aber sie m&uuml;ssen auch interpretiert,
              d.h. in der Tiefe verstanden, werden. Bei behutsamem Nachfragen
              sind in den &#8222;Sterbew&uuml;nschen&#8220; nur allzu oft verschiedene &Auml;ngste
              enthalten, u.a. vor: unertr&auml;glichen Schmerzen, dem Alleinsein,
              sozialer Ausgrenzung, als zu stark empfundener Belastung der Angeh&ouml;rigen. </font></p>
            <p><font face="Arial, Helvetica, sans-serif">Eine m&ouml;gliche Antwort ist die intensive, pers&ouml;nliche
              Betreuung - entweder im Wege der palliativen (w&auml;rmenden) Medizin
              und/oder bei Sterbenskranken in speziell f&uuml;r diesen Zweck
              eingerichteten Hospizen. Soweit m&ouml;glich, personell und sachlich,
              k&ouml;nnen deren Dienstleistungen zumindest teilweise auch zuhause
              angeboten werden. Erfahrungsgem&auml;&szlig; kann in diesen Einrichtungen
              der urspr&uuml;nglich ge&auml;u&szlig;erte &#8222;Sterbewunsch&#8220; gut &#8222;aufgefangen&#8220; und
              beantwortet werden. </font></p>
            <p><font face="Arial, Helvetica, sans-serif">Die Begleitung und Hilfe
                f&uuml;r die Betroffenen &#8211; Patienten
              und deren Angeh&ouml;rige &#8211; im Hospiz im Sinne einer umfassenden
              (rounded care&#8220;) Betreuung auf dem Weg zu einem menschenw&uuml;rdigen
              Sterben hat seit dem Mittelalter eine lange christliche Tradition.
              (lat. hospitium: Gastfreundschaft, Herberge). </font></p>
            <p><font face="Arial, Helvetica, sans-serif">Die meisten Menschen
                m&ouml;chten zu Hause, zumindest in ihrer
              vertrauten Umgebung sterben (aber: 70% sterben in einem Krankenhaus).
              Dazu bedarf es einer weit gr&ouml;&szlig;eren Zahl in der Palliativmedizin
              ausgebildeter (Haus)-&Auml;rzte &#8211; f&uuml;r die ambulante
              und/oder die station&auml;re Betreuung. </font></p>
            <p><font face="Arial, Helvetica, sans-serif">Wohl auch als Antwort
                auf die Aufl&ouml;sung der Familienbande
              und die zunehmende Anzahl der Single-Haushalte hat sich bei uns
              (die Wurzeln der modernen Hospizbewegung liegen in England) die
              Zahl der Hospize und der Palliativstationen erfreulich vermehrt &#8211; auf &uuml;ber
              75 Palliativstationen und mehr als 100 Hospize. Diese Erh&ouml;hung
              ist zwar erfreulich, aber noch lange nicht ausreichend; in der
              Stadt Frankfurt gibt es z. Zt. nur ein Hospiz (Sankt Katharina). </font></p>
            <p><font face="Arial, Helvetica, sans-serif">Um dem Sterbenden die
                M&ouml;glichkeit zu geben, in der gewohnten
              Atmosp&auml;hre seines Zimmers &#8211; im Alten- bzw. Pflegeheim &#8211; Abschied
              vom Leben nehmen zu k&ouml;nnen, gibt es &Uuml;berlegungen, den
              Hospizgedanken in die Heime/Stationen zu integrieren &#8211; integriertes
              Hospiz. </font></p>
            <p><font face="Arial, Helvetica, sans-serif">Damit Angeh&ouml;rige die Pflege schwerstkranker Angeh&ouml;riger &uuml;bernehmen
              k&ouml;nnen, schl&auml;gt das Zentralkomitee der Deutschen Katholiken
              (ZdK) die Einf&uuml;hrung einer &#8222;Pflegezeit&#8220; (mit Arbeitsplatzgarantie)
              vor. In &Ouml;sterreich erleichtert der Gesetzgeber schon jetzt
              die Betreuung sterbenskranker Familienangeh&ouml;riger f&uuml;r
              die Dauer von bis zu sechs Monaten. Dazu hat er die &#8222;Familienhospizkarenz&#8220; geschaffen. </font></p>
            <p><font face="Arial, Helvetica, sans-serif">Aus den USA kommen ern&uuml;chternde Nachrichten. Die hohe emotionale
              Belastung der Betreuer f&uuml;hrt in den Staaten, welche den assistierten
              Suizid gesetzlich erlaubt haben, zu einem me&szlig;baren Anstieg
              dieses &#8222;verweigerten Mit-Leidens&#8220;. </font></p>
            <p><font face="Arial, Helvetica, sans-serif">Da zunehmend Palliativ-/Hospiz-Stationen
                (direkt oder indirekt) in christlicher Tr&auml;gerschaft gef&uuml;hrt werden, ergibt sich
              die gro&szlig;e Chance, einen gesellschaftlich akzeptierten Gegenentwurf
              entweder zu der geforderten Legalisierung der aktiven Sterbehilfe
              und/oder Angeboten wie denen von DIGNITAS nicht nur zu fordern,
              sondern auch konkret zu leben. </font></p>
            <p><font face="Arial, Helvetica, sans-serif">Gesundheit ist zwar
                ein erstrebenswertes Ziel, aber nicht der Ma&szlig;stab f&uuml;r den Wert des Lebens. Auch Leiden kann einen
              Sinn haben. In einer Gesellschaft, in der Jugend, Gesundheit und
              blendendes (wen?) Aussehen einen hohen Stellenwert haben, mu&szlig; jemand
              wie Papst Johannes Paul II in dem offenen Umgang mit seiner Krankheit
              und seinem Tod ein Stein des Ansto&szlig;es sein &#8211; aber auch
              f&uuml;r viele Kranke ein Grund der Hoffnung. Im Tod traf auf ihn
              die uns heute fast nicht mehr gel&auml;ufige/verst&auml;ndliche
              Beschreibung zu: &#8222;Er hat das Zeitliche gesegnet&#8220;. </font></p>
            <p><font face="Arial, Helvetica, sans-serif">Dezember 2008</font></p>
            <P><font face="Arial, Helvetica, sans-serif" size="3"><br>
              &copy; </font><font face="Arial, Helvetica, sans-serif">J. Beckermann,&#8222;<a href="http://www.hmk-leben.de/">Hilfe
              f&uuml;r Mutter und Kind e.V.</a></font></P>
            </TD>
          <TD background=boxright.gif height="124"><IMG height=8 alt="" 
            src="boxright.gif" width=8></TD>
        </TR>
        <TR vAlign=top align=left>
          <TD><IMG height=8 alt="" src="boxbottomleft.gif" 
            width=8></TD>
          <TD background=boxbottom.gif><IMG height=8 alt="" 
            src="boxbottom.gif" width=8></TD>
          <TD><IMG height=8 alt="" src="boxbottomright.gif" 
            width=8></TD></TR></TBODY></TABLE></TD></TR>
  <TR>
    <TD vAlign=top align=left height="2">&nbsp; </TD>
</TR></TBODY></TABLE></BODY></HTML>
