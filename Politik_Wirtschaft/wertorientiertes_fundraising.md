---
title: Wertorientiertes Fundraising
author: 
tags: 
created_at: 
images: []
---


# **Nicht Methoden, sondern die Orientierung an Werten ist die Baisis des Fundraisings*
# **                Fundraising bedeutet die Beschaffung von Mitteln, Geld, Know-how, Dienstleistungen, Zeit, für gemeinnützige Organisationen. Wie aber kann dies gelingen?**
 Jede Nonprofit-Organisation gründet auf Werten. Sie ist wegen bestimmter Werte, z.B. Förderung der Kultur, Unterstützung von Kranken, Erziehung, die Förderung der Stadt, des Dorfes z.B. durch Erforschung der örtlichen Geschichte u.a. gegründet worden. Wenn neue Ressourcen erschlossen werden sollen, dann sind es diese Werte, derentwegen Menschen sich finanziell oder auf andere Weise engagieren. Wertorientiertes Fundraising baut auf die Überzeugungskraft der Werte, die von der sozialen Einrichtung, dem Bildungswerk, der kirchlichen Institution, der Schule oder dem Verein umgesetzt werden. ***

 Entscheidend für das Fundraising ist eine dauerhafte und möglichst individuelle Beziehung zwischen einer Organisation und ihren Förderern, die dadurch aufgebaut wird, daß die Förderer die Umsetzung der Werte unterstützen wollen, für die die Einrichtung, der Verband stehen. Über nichts besser als über die Werte baut sich schrittweise eine intensive und dauerhafte Bindung der Förderer an die Einrichtung auf. Dies gelingt um so besser, wie beide Seiten gemeinsame Werte und Überzeugungen teilen.  

# ****
 ***[© Wertorientiertes Qualitätsmanagement](http://www.wqmanagement.de/cms/324.html) 
