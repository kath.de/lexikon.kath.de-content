---
title: Arzthaftung bei Abtreibung
author: 
tags: 
created_at: 
images: []
---

  

***Wenn die Abtreibung nicht gelingt, 
haftet der Arzt** 

***Folgender Fall: Eine Mutter will ihr Kind abtreiben lassen. Sie schließt mit dem Arzt einen Abtreibungsvertrag. Dieser Vertrag ist, wie das Bundesverfassungsgericht noch 1993 festgestellt hat, wegen der Rechtswidrigkeit der Abtreibung sittenwidrig und der Arzt hat deshalb keinen Anspruch auf ein Honorar. In einer späteren Entscheidung hat dasselbe Gericht jedoch erklärt, die rechtswidrige Tötung des Kindes unterliege ausnahmsweise dem Schutz der Berufsfreiheit des Art 12 GG. Denn „die Tätigkeit des Arztes ist notwendiger Bestandteil des gesetzlichen Schutzkonzepts“. So kommt auch der Abtreibungsarzt zu seinem Honorar. ***Das gilt jedenfalls, wenn die Abtreibung „gelingt“. 

***„Gelingt“ sie nicht und kommt das Kind lebend zur Welt – und das kommt immer wieder mal vor -, so verliert der Arzt nicht nur seinen Honoraranspruch. Er ist darüber hinaus den Eltern des Kindes zum Schadensersatz verpflichtet. Er hat für die Unterhaltskosten des Kindes so lange aufzukommen, wie die Eltern dafür aufkommen müssen. ***Der Pflichtenkreis des Arztes stellt sich bei einer Abtreibung folgendermaßen dar:  

***• Aufgrund seines wirksamen Vertrages mit der Mutter ist er verpflichtet, den Tod des ungeborenen Kindes im Mutterleib herbeizuführen. Gelingt ihm das nicht und kommt das Kind lebend zur Welt, hat er diese Verpflichtung schuldhaft verletzt. Ihm drohen möglicherweise hohe Schadensersatzforderungen, sofern das Kind nicht doch noch eines natürlichen Todes stirbt und damit der Schadensgrund nachträglich entfällt. 

***• Vom Zeitpunkt der Geburt an ändert sich die Aufgabe des Arztes. Seine vertragliche Pflicht ist gescheitert und damit erledigt. Aufgrund seiner ärztlichen Pflicht muß er nun alles tun, um das Leben des zu diesem Zeitpunkt oft schwer geschädigten Kindes zu erhalten. Anders formuliert: Er muß alles tun, daß der Schadensgrund erhalten bleibt und Schadensersatzforderungen gegen ihn bestehen bleiben. Daß ein Arzt in dieser Situation der Versuchung erliegt, dem Tod des bereits geborenen Kindes ein wenig nachzuhelfen, ist leider kein Einzelfall. 

***Von Schadensersatzzahlungen gleicher Art ist auch der Arzt bedroht, der nach einer pränatalen Diagnose (PND) eine Schädigung des Kindes nicht zuverlässig erkennt, die Mutter nicht auf die Möglichkeit der Abtreibung hinweist, diese deshalb das Kind nicht abtreibt und das Kind schließlich behindert zur Welt kommt. Und das, obwohl die Abtreibung allein wegen einer Schädigung des ungeborenen Kindes (eugenische Indikation) im Gesetz nicht vorgesehen ist. Konsequenz daraus: Der Arzt ist zur Vermeidung von Schadensersatzansprüchen „gut“ beraten, einer werdenden Mutter „im Zweifel“ vorsorglich zur Abtreibung zu raten. 

*** Diese wenigen Beispiele zeigen die ganze Widersprüchlichkeit und Paradoxie der ***[Abtreibungsregelung](abtreibungsregelung_rechtliche_bewertung.php) in Deutschland (rechtswidrig, aber straflos; vom Staate mißbilligt, aber gleichzeitig gefördert und finanziert). Abgeholfen wird dieser Widersprüchlichkeit nicht durch Hinzufügung einer zweiten Widersprüchlichkeit, die darin besteht, daß man das Kind als Schadensgrund ausschließt. Denn wenn jemandem eine Unterhaltspflicht aufgebürdet wird, dann ist das nun einmal ein finanzieller Nachteil und damit ein Schaden. Hilfe kann nur durch eine Änderung der Abtreibungsregelung kommen. 

***Wenn unser Staat das Leben schützen will, dann bitte konsequent, wenn er ein Handeln mißbilligt, dann bitte nicht nur in Worten, sondern auch in Werken.            

# ****
# *****[©](http://www.hmk-leben.de/)***[ ***Dr. Konrad Schneller](http://www.hmk-leben.de/)**
# ****
