---
title: Patientenverfügung
author: 
tags: 
created_at: 
images: []
---


# **PATIENTENVERFÜGUNG: Hilfe oder Falle? *
# **Für eine aktive Sterbebegleitung *
# **Ziel: Leben bis zuletzt unterstützen und/aber am Sterben nicht hindern *
# **Für eine Kultur der Endlichkeit des Lebens *
***Was ist der Tod? Only a preventable desease, also nur eine (weitere) zu vermeidende Krankheit? Oder das natürliche Ende - und damit Teil - des Lebens? Mehr als 50% der Befragten einer Studie bei uns (2006) halten es für realistisch, daß sie über 80 Jahre alt werden. Aber: Ein langes Leben um jeden Preis wollen die wenigsten.  

***Die dankenswerten Fortschritte der Medizin – insbesondere zum Lebensende hin – haben ungeahnte (Heilungs-)Chancen eröffnet, sie haben den Prozeß des „Lebens vor dem Sterben“ besser steuerbar gemacht, ihn sozusagen „entschleunigt“. Sie haben aber auch neue Probleme/Fragen aufgeworfen. Die zunehmende Bedeutung von Demenz ist nur ein Aspekt. Die Angst, der „Apparatemedizin“ allein und hilflos ausgesetzt zu sein, ein anderer. Nicht zu leugnen ist auch die Sorge, man könne Anderen (gerade auch Verwandten) ungebührlich zur Last fallen.  

***„Auch in Krankheit und Sterben ist die zentrale Richtschnur allen Handelns die unverfügbare Würde des betroffenen Menschen“ (ZdK). Das schließt das Recht auf Selbstbestimmung ein, auch das Recht auf Selbstbeschränkung, nicht aber das Recht zur Selbsttötung. 80% der Befragten wollen selbst bestimmen, unter welchen Umständen sie sterben (Ethik der gesteigerten Verantwortung). Der aktuelle oder der mutmaßliche Wille des Patienten bindet – den Arzt und die Behörden. Dem Arzt steht kein Zwangsbehandlungs-, kein Letztentscheidungsrecht zu (gegen den Paternalismus der „Götter in Weiß).  

***Was gilt, wenn der Patient sich nicht (mehr) zu äußern vermag? Eine größere Klarheit/Sicherheit erhoffen sich nicht wenige durch die Patientenverfügung (PV). Wegen der Komplexität der zu beachtenden Fragen (ethisch, medizinisch, rechtlich) ist sie trotz mehrfacher Versuche bei uns* noch nicht gesetzlich geregelt. Der Klärungsbedarf betrifft neben eher formalen Aspekten vor allem die Reichweite und die Verbindlichkeit – aber auch die Grundsatzfrage, ob eine gesetzliche Regelung überhaupt sinnvoll/notwendig ist.  

****In Österreich ist das Gesetz über die Patientenverfügung verabschiedet worden. Es unterscheidet zwischen „verbindlicher“ und (lediglich) „beachtlicher“ Verfügung. Die „verbindliche“ PV muß nach umfassender Beratung in schriftlicher Form unter Beachtung weiterer Formalitäten abgefaßt werden.  

***Wenn man den geringen Anteil der unterzeichneten Verfügungen betrachtet (unter 10%), könnte man die „Notwendigkeit, eine Regelung zur Stärkung der Verbindlichkeit von PVs treffen zu müssen“ – wie von der Politik behauptet – anzweifeln. Liegt es nur an den noch bestehenden Unklarheiten? Oder versucht der Staat, angesichts seiner eigenen Unfähigkeit, die Menschenwürde des Sterbenden sicher zu stellen, die Verantwortung abzuschieben – auf die Kranken, die weitgehend Hilflosen, gar die Sterbenden? Was gilt, wenn im Extremfall der Patientenwille gegen das von Staat zu schützende Leben steht?  

***Die noch immer andauernden Diskussionen im Bundestag und außerhalb hat die große Unsicherheit bestätigt. „Eine rundum überzeugende Lösung gibt es wohl nicht“, sagte der Präsident des Bundestages, Dr. Lammert.  

***Im folgenden werden einige Kern-Fragen kurz besprochen:  

# **Bindungswirkung: *
***Der BGH hat den grundsätzlichen Vorrang/Bindungswirkung des Patientenwillens - auch durch Verfügung - bestätigt. Das Vormundschaftsgericht darf nicht zum „Herrn über Leben und Tod“ werden. Es wird nur eingeschaltet, um den eindeutigen Patientenwillen (etwa gegenüber dem Pflegepersonal) durchzusetzen oder aber in Fällen von Unsicherheit/Streit („Konfliktmodell“).  

***Bei der Berücksichtigung des aktuellen/mutmaßlichen Patientenwillens muß immer der Gesichtspunkt des „Perspektivenwechsels“ bedacht werden: d.h. Menschen denken um, im Leben und insbesondere angesichts bedrohlicher Krankheiten und des Sterbens. Auch Patienten z.B. im Wachkoma können sich (neu) äußern. Schwierig sind Fälle von LIS (Locked-in-Syndrom), bei dem der Patient infolge eines Stamm- und Kleinhirninfarktes (z.B. nach Schlaganfall) bei Bewußtsein, aber doch vollständig gelähmt ist.  

# **Formalien: *
***Eine Patientenverfügung sollte grundsätzlich schriftlich abgefaßt werden. Sie ist aber auch mündlich erklärbar – und ebenso widerrufbar.  

***Die formularmäßig vorgefertigten PVs sind gute Formulierungshilfen, sie sind aber oft so pauschal gehalten, daß sie für den konkreten Einzelfall nicht anwendbar sind. Es besteht Gefahr der „heimlichen Euthanasie“. Patientenverfügungen müßten daher so konkret, situations-, d.h. krankheitsspezifisch wie möglich sein.  

# **Organspende: *
***Verfügung und grundsätzlich vorhandene Organspendebereitschaft können sich (zeitlich) ausschließen. Was soll gelten?  

# **Reichweite: *
***Die frühere Enquetekommission „Ethik und Recht der Modernen Medizin“ hat sich für eine Einschränkung der Reichweite ausgesprochen; sie soll nur für Krankheiten gelten, „die zum Tode führen“, also als Letztverfügung. Sie gilt daher (zunächst einmal) nicht für Demenzkranke und Wachkomapatienten. Aber: Man muß darauf achten, daß der alte ärztliche Paternalismus nicht durch einen neuen ethischen Paternalismus – Dogma des Lebenszwangs – abgelöst wird.  

***Frage: Was gilt für die Fälle, in denen man ausschließen kann, daß das Bewußtsein jemals wieder erlangt wird?  

# **Vollmachten: *
***Es bietet sich an, zusätzlich eine Vorsorge- (für den Gesundheitsbereich) und eine Betreuungsvollmacht (sofern eine Betreuung notwendig ist) zu erteilen.  

# **ZUSAMMENFASSUNG: *
***Die Fragen des „Lebens vor dem Sterben“ („unerledigte Geschäfte“), incl. einer möglichen Patientenverfügung, sollten wenn möglich in einem eingehenden, offenen Gespräch mit Verwandten und Ärzten erörtert werden.  

***Da jeder PV ein Element des Nichtwissens innewohnt, sie damit auch von einer verständlichen Ängstlichkeit geprägt ist, muß man darauf achten, daß sie nicht lediglich Ausdruck des Mißtrauens gegenüber den Ärzten (und/oder den Angehörigen) ist. Gerade in Grenzsituationen Abwägungen zu treffen, gehört zu der täglich geforderten „ärztlichen Kunst“ - insbesondere im Hinblick auf die auch weiterhin zu erwartenden medizinischen Fortschritte.  

***In diesem Zusammenhang ist auch an das in den USA präferierte Konzept des „Advanced Care Planning“, des „vorausschauenden Versorgungsplans“ zu denken. Beteiligt sind die Fachleute, aber auch die Angehörigen. Gemeinsam werden je nach Symptomen die erforderlichen Behandlungsmöglichkeiten festgehalten, incl. der Ernährung.  

***Wegen der Unwägbarkeiten/Risiken der Patientenverfügung ist daher zu überlegen, auf das Basis des bestehenden Rechts eine eher „offene“ Verfügung anzustreben – verbunden mit der Vorsorgevollmacht an (zunächst) eine Vertrauensperson.  

***Dezember 2008 

# ****
 © ***J. Beckermann ***[Hilfe für Mutter und Kind e.V.](http://www.hmk-leben.de/) 
