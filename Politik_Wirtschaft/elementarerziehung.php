<HTML><HEAD><TITLE>Elementarerziehung</TITLE>
<META http-equiv=Content-Type content="text/html; charset=iso-8859-1"><LINK 
title=fonts href="kaltefleiter.css" type=text/css 
rel=stylesheet>
<META content="MSHTML 5.50.4134.600" name=GENERATOR></HEAD>
<BODY bgColor=#ffffff background="dstone1.gif" leftMargin=6 topMargin=6 marginwidth="6" marginheight="6">
<TABLE cellSpacing=0 cellPadding=6 width="100%" border=0>
  <TBODY>
  <TR>
    <TD vAlign=top align=left width=100 height="72"> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif" height="14"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2" height="14"><b>Politik und Wirtschaft</b></td>
          <td background="boxtopright.gif" height="14"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#FFFFFF"> <?php include("logo.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#FFFFFF" class="V10"> <?php include("az.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </TD>
    <TD vAlign=top rowSpan=2>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
        <TBODY>
        <TR vAlign=top align=left>
          <TD width=8><IMG height=8 alt="" 
            src="boxtopleftcorner.gif" width=8></TD>
          <TD background=boxtop.gif><IMG height=8 alt="" 
            src="boxtop.gif" width=8></TD>
          <TD width=8><IMG height=8 alt="" 
            src="boxtoprightcorner.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxtopleft.gif><IMG height=8 alt="" 
            src="boxtopleft.gif" width=8></TD>
          <TD bgColor=#e2e2e2> 
            <H1>Elementarerziehung</H1>
          </TD>
          <TD background=boxtopright.gif><IMG height=8 
            alt="" src="boxtopright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=13 alt="" src="boxdividerleft.gif" 
            width=8></TD>
          <TD background=boxdivider.gif><IMG height=13 
            alt="" src="boxdivider.gif" width=8></TD>
          <TD><IMG height=13 alt="" 
            src="boxdividerright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxleft.gif height="124"><IMG height=8 alt="" 
            src="boxleft.gif" width=8></TD>
          <TD height="124" bgcolor="#FFFFFF" class=L12> <font face="Arial, Helvetica, sans-serif" size="3"><br>
            Die <b>Elementarerziehung </b>in Kindergärten und Kindertagesstätten 
            hat durch die internationalen Bildungsstudien wie PISA als eigenständiges 
            Bildungssystem erfreulicherweise neue Aufmerksamkeit erfahren. Bislang 
            wurden Kindergärten eher als Betreuungseinrichtungen mit einem spielerischen 
            Schwerpunkt gesehen. Der gesetzliche Anspruch auf einen Kindergartenplatz 
            sollte vor allem dazu dienen, Mütter zu entlasten und die Vereinbarkeit 
            von Familie und Beruf für Frauen zu verbessern. Die Vergleichs-Studien 
            und die aktuelle Diskussion haben zweierlei deutlich werden lassen: 
            schon sehr junge Kinder sind äußerst lernfähig und lernbegierig, und 
            zum anderen: Bildungschancen und -karrieren werden schon vor der Schule 
            gebahnt. </font> 
            <P><font face="Arial, Helvetica, sans-serif" size="3">Im bildungspolitischen 
              Diskurs zeigt sich eine neue Wertschätzung und Gewichtung der <b>vorschulischen 
              Erziehung</b>. Damit verbunden ist eine stärkere Betonung des Bildungsauftrages 
              der Kindertagesstätten, an dessen konkreter Formulierung und Ausgestaltung 
              als Bildungsvereinbarungen die Bundesländer und die Träger der vorschulischen 
              Erziehung z.Zt. arbeiten. Konsequenterweise ergeben sich dabei aber 
              auch Diskussionen um eine Neu- und Umverteilung der finanziellen 
              Mittel wie der personellen Ausstattung und Diskussionen um eine 
              Aufwertung und Neugestaltung der Erzieherinnenausbildung.</font></P>
            <P><font face="Arial, Helvetica, sans-serif" size="3">Die Neubewertung 
              des Elementarbereichs und die veränderten Ansprüche in der vorschulischen 
              Erziehung machen es zum jetzigen Zeitpunkt unbedingt erforderlich, 
              den Erzieherinnen in der Praxis Fortbildungen anzubieten, die sie 
              in die Lage versetzen, den Anforderungen und Aufgaben gerecht zu 
              werden. Die katholische <a href="http://www.franz-hitze-haus.de">Akademie 
              Franz Hitze Haus</a> bietet deshalb Veranstaltungen und Fortbildungen 
              an, die die Professionalisierung der Erzieherinnen in verschiedenen 
              Aufgabenbereichen verbessert und stärkt.</font></P>
            <P><font face="Arial, Helvetica, sans-serif" size="3">Schwerpunkte 
              des Angebots sind zum einen Seminare zur differenzierten Einschätzung 
              und Förderung der Kinder vom 3. Lebensjahr bis zum Übergang in die 
              Grundschule, zur Sprachdiagnostik und Sprachförderung und zu Entwicklungsverzögerungen 
              und Verhaltensauffälligkeiten. Im Rahmen der Konzeptdebatte und 
              Neuausrichtung der Einrichtungen stellen wir in einem mehrteiligen 
              Kurs Theorie und Praxis der Reggio-Pädagogik vor. In diesem Kontext 
              bieten wir schon länger Kurse an, wie jungen Kindern Anregungen 
              zu naturkundlichem Entdecken und Forschen vermittelt werden können. 
              Ein weiterer Schwerpunkt ist ein Zertifikatskurs als Fachkraft in 
              der Begabtenförderung, der mit dem Internationalen Centrum für Begabungsforschung 
              der Universität Münster entwickelt wurde. Neben diesen meist mehrteiligen 
              Kursen, die der Qualifizierung dienen, geht es uns auch um sensorische 
              Integration, um Wahrnehmungsschulung, um die Balance von Konzentration 
              und Entspannung, um Entwicklung kreativer Potenzen bei den Kindern 
              und um die Computernutzung bei Projekten im Kindergarten. Der Stärkung 
              der eigenen Persönlichkeit und der kommunikativen Kompetenzen dienen 
              Veranstaltungen zur Gesprächsführung mit Eltern, zum Umgang mit 
              aggressiven Kindern und zum Abbau von Stresserfahrungen. Die Veränderungen 
              in der professionellen Elementarerziehung und -bildung werden natürlich 
              unser fortlaufendes Angebot beeinflussen und verändern. </font><font size="3" face="Arial, Helvetica, sans-serif"><br>
              <br>
              <b>Weitere Informationen:<br>
              </b></font><font size="3" face="Arial, Helvetica, sans-serif"><br>
              <a href="http://www.franz-hitze-haus.de">Akademie Franz Hitze Haus</a><br>
              </font><font face="Arial, Helvetica, sans-serif" size="3"><br>
              &copy; Akademie Franz Hitze Haus. </font></P>
            </TD>
          <TD background=boxright.gif height="124"><IMG height=8 alt="" 
            src="boxright.gif" width=8></TD>
        </TR>
        <TR vAlign=top align=left>
          <TD><IMG height=8 alt="" src="boxbottomleft.gif" 
            width=8></TD>
          <TD background=boxbottom.gif><IMG height=8 alt="" 
            src="boxbottom.gif" width=8></TD>
          <TD><IMG height=8 alt="" src="boxbottomright.gif" 
            width=8></TD></TR></TBODY></TABLE></TD></TR>
  <TR>
    <TD vAlign=top align=left height="2">&nbsp; </TD>
</TR></TBODY></TABLE></BODY></HTML>
