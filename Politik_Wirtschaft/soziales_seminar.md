---
title: Soziales Seminar
author: 
tags: 
created_at: 
images: []
---


# ** *
***Das Soziale  Seminar bietet langfristige und systematische politisch-soziale  Bildung in katholischer Trägerschaft an. Das Ziel ist die Bildung  von Menschen, die auf der Grundlage der Katholischen Soziallehre  zur konkreten Mitarbeit an der Gestaltung der Gesellschaft befähigt  und ermutigt werden sollen. 

***Die Umsetzung  der Katholischen Soziallehre setzt Menschen voraus, die sich - sachkundig  und den konkreten Herausforderungen bewusst - für sie einsetzen  und so zu einem gerechten und menschenwürdigen Leben beitragen.  Die Ortskirchen stehen in der Verantwortung, ihren Gläubigen die  nötige politisch-soziale Bildung - das gesellschaftliche, wirtschaftliche  und politische Sachwissen, die Erarbeitung der Werthaltungen und  Handlungsmaßstäbe und die Befähigung zum gesellschaftlichen Engagement  - anzubieten und sie zur Teilnahme an diesem Bildungsangebot zu  ermutigen. 

***Im Sozialen  Seminar wird diese Bildungsarbeit seit den frühen fünfziger Jahren  in Trägerschaft der jeweiligen Diözese geleistet. Die Langfristigkeit  der Angebote des Seminars, die Systematik und die gemeinsame Arbeit  in festen Gruppen gewährleisten, dass die Kirche ihren wichtigen  Beitrag zu einer sozial gerechteren Gestaltung unserer Gesellschaft  einbringen kann. Die Angebote des Sozialen Seminars sind offen für  alle Menschen, die sich auf den Weg sozialer Verantwortung begeben  wollen. 

# **Die katholisch-soziale  Akademie ***[Franz Hitze Haus](http://www.franz-hitze-haus.de),  die zu den ***[Gründungsmitgliedern  der Bundesarbeitsgemeinschaft der Sozialen Seminare](http://www.franz-hitze-haus.de/index.php?cat_id=1218&menuid=1218) gehört,  gibt seit 1952 den ***[Informationsbrief  "Soziales Seminar"](http://www.franz-hitze-haus.de/index.php?cat_id=1218&menuid=1218) heraus, der aktuelle wirtschafts- und sozialpolitische  Themen behandelt. Alle Absolventen der Sozialen Seminare erhalten  diesen Rundbrief kostenlos, Einzelexemplare können bei Interesse  über die Akademie ***[Franz  Hitze Haus](http://www.franz-hitze-haus.de) kostenlos angefordert werden.*****
# ****
# ****
 © Akademie Franz Hitze Haus.  
