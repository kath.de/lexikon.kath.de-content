---
title: Aktive Sterbehilfe
author: 
tags: 
created_at: 
images: []
---


# **„Von der Würde - Dignitas - des Sterbenden“ *
***„Die Forderung, schwer kranke Menschen auf ihren Wunsch hin und Menschen, die einer klaren Willensäußerung nicht mehr fähig sind, auch ohne ihren Willen zu töten, wird nach einer Latenzzeit von etwa fünfzig Jahren nun wieder energisch vorgetragen“ (Robert Spaemann, Oktober 2005).  

***In seiner Weihnachtsansprache 2008 hat Henri I., der Großherzog von Luxemburg, die Unterzeichnung eines „Sterbehilfegesetzes abgelehnt. Wie in Belgien und Holland sollen Ärzte damit das Recht erhalten, bei unheilbar Kranken in der letzten Phase, aber auch bei drohendem Persönlichkeitsverlust durch Demenz die Tötung auf Verlangen durchzuführen; auch der assistierte Suizid soll ermöglicht werden.  

# **Lebensschutz als umfassendes Thema: Abgrenzungskriterien *
***Fragen zur „Sterbehilfe“ sind Teil der Gesamtdebatte zum Thema Lebensschutz; sie müssen daher möglichst umfassend, differenziert und in sich stimmig diskutiert werden. Unklarheiten (u.a. durch – bewußt? - unpräzise Gesetzesformulierungen) zu Beginn des Lebens (Abtreibungsregelung: „rechtswidrig, aber straffrei“) führen zu zwar „logisch“ scheinenden, aber dennoch falschen Forderungen auf Legalisierung der Sterbehilfe zum Lebensende. (vergl. Kusch).  

***Wenn es als Teil der Selbstbestimmungsrechts ein Recht zur Selbsttötung gäbe (s.u.) und darüber hinaus einen Anspruch auf die Mithilfe „Berufener“ – z.B. eines Arztes -, müßte dieser dann nicht bei Tätigwerden straffrei gestellt werden? („unzumutbare Grauzone der Illegalität“). Tötung als ärztliche Leistung? Und weiter: Müßte der Arzt evtl. sogar zur Mithilfe verpflichtet werden, d.h. gibt es einen Anspruch gegen den Arzt? Mit welchen Folgen? Schadensersatz? Wann?  

***Es gibt viele Abgrenzungsprobleme. Grundsätzlich: Worauf beruht das Verbot zur Selbsttötung? Was ist Tötung auf Verlangen? Abgrenzung zum assistierten Suizid?Darf aktive Sterbehilfe nur ggü. Todkranken oder auch bei Alzheimerpatienten gewährt werden? Stichwort: „Sozialverträgliches Frühversterben“. (vergl. dazu auch die Orientierungshilfe der EKD, November 2008; HK 1/2009, S. 9).  

# **Erläuternde Vorbemerkungen: *
***(1) Gemäß einer im November 2008 veröffentlichten Umfrage bei Ärzten bejahen 35% der Befragten die Möglichkeit des ärztlich assistierten Suizids; etwa jeder sechste spricht sich sogar für die (Legalisierung der) aktiven Sterbehilfe aus. (vergl. HK 1/2009, S. 9)  

***(2) Mit dem Hinweis auf den Gleichheitsgrundsatz wird europaweit für die Zulassung der aktiven Sterbehilfe (übrigens auch der Abtreibung) geworben. Das Thema „aktive Sterbehilfe“ ist auf Antrag von Belgien, Holland und der Schweiz im Oktober 2003 auf die Tagesordnung der Parlamentarischen Versammlung des Europarates gesetzt worden - unter dem bewußt harmlos scheinenden Titel: „Unterstützung von Patienten am Lebensende“.  

***Das Ziel ist die europaweite Zulassung der „Euthanasie“ – basierend auf Analysen der holländischen Erfahrungen. Dies ist ein Versuch, in Europa geltendes Recht – was dann auch in den einzelnen Ländern gelten würde - neu zu definieren und zu verändern. Im April 2005 hat der Europarat gegen die Empfehlung gestimmt.  

***(3) Mit der „CREDO CARD“ – „Maak mij niet dood, Doktor“ - wenden sich in Holland zunehmend Patienten gegen lebensverkürzende Maßnahmen, die gegen ihren Willen angeordnet werden. Aus Angst vor dem ungewollten Tod gehen ältere Menschen in Holland in Altenheime ins benachbarte Deutschland.  

***(4) Die Schweizer Gesellschaft DIGNITAS hat am 26. September 2005 in Hannover eine deutsche Tochtergesellschaft gegründet, um vor Ort für ihre Dienstleistungen (Beihilfe zur Selbsttötung) werben zu können. Sollte das deutsche Recht – für eine Straffreiheit dieser bezahlten Leistungen - nicht “ausreichen“, will man durch Präzedenzfälle auf eine entsprechende „Rechtsfortbildung“ hinwirken.  

# **BEGRIFFE/DEFINITIONEN: *
***Es ist zu beachten, daß manche Begriffe unterschiedlich genutzt/verstanden werden – je nachdem ob es sich um Ärzte oder um Juristen handelt.  

***Suizidverbot: In der Diskussion wird die bisher als klassisch angesehene Berufung des Selbsttötungsverbots auf die Souveränität des Schöpfers allein als nicht mehr ausreichend angesehen. Auch nicht der irreversible Charakter der Suizidhandlung.  

***Gibt es eine Begründung, die ansatzweise auch von Nichtgläubigen nachvollzogen und akzeptiert werden kann? Zunehmend wird auf die unbedingte Solidarität ggü. den Mitmenschen verwiesen. Die Einräumung des Rechts zur Selbsttötung hätte notwendig eine entsolidarisierende Wirkung. Aufgrund des dadurch entstehenden Rechtfertigungsdrucks könnte aus dem Recht leicht eine Pflicht werden.  

***Aktive (direkte) Sterbehilfe/Euthanasie: in Deutschland strafbar.  

***“Unter Euthanasie im eigentlichen Sinne versteht man eine Handlung oder Unterlassung, die ihrer Natur nach und aus bewußter Absicht den Tod herbeiführt, um auf diese Weise jeden Schmerz zu beenden“ (Evangelium vitae, 9. April 2004) - Das Ziel ist der Tod des Patienten - im Tausch gegen Leidbeendigung.  

***Der Klarheit wegen ist zu überlegen, den Begriff „Euthanasie“ nur auf die Fälle zu beschränken, bei denen wie in der NS-Zeit die Tötung ganzer Gruppen von Menschen (z.B. der Geisteskranken) bewußt und systematisch angestrebt wird.  

***- Juristisch vergleichbar (und strafbar) ist der Fall der „Tötung auf Verlagen“ gem. § 216 StGB d.h. die Vernichtung fremden menschlichen Lebens mit Einwilligung (auf Wunsch) des Betroffenen. § 216 StGB markiert die Grenze des Selbstbestimmungsrechts.  

***- Juristisch vergleichbar (und strafbar) ist der assistierte Suizid.  

***- Juristisch vergleichbar (und strafbar) ist die „terminale“ Sedierung, wenn der Tod des Patienten das erklärte bzw. implizierte Ziel der „Behandlung“ ist.  

***- Nicht voll vergleichbar ist das Angebot von DIGNITAS: es handelt sich (juristisch) „nur“ um Beihilfe zur Selbsttötung durch die Beschaffung schnell und schmerzlos wirkender Barbiturate. Das totbringende Gift muß der Patient selber einnehmen, zumeist allein, d.h. ohne Anwesenheit eines Dritten. Es handelt sich nicht um „begleitetes Sterben“, sondern um „Begleitung zum Tod“.  

***In Deutschland ist diese Beihilfe grundsätzlich straffrei; in der Schweiz ist sie nur unter bestimmten Gesichtspunkten strafbar – bei Vorliegen niedriger Beweggründe.  

***Bedenken: Die organisierte (irgendwie doch) bezahlte Vermittlung von Suizid-Beihilfe (wie bei DIGNITAS) verändert die Einstellung zum Leben. Am Anfang stehen gefühlsbetonte und daher nicht leicht abzuwehrende Begriffe wie „Mitleid“ und „Wegräumen unnötiger Hindernisse“ (medizinische Unkenntnis).  

***Zumindest, wenn diese Dienstleistungen gewerblich (und aus Verdienstgründen) angeboten werden, sollte ein Verbot angestrebt werden (§ 217 StGB).  

***...die Diskussion um die Legalisierung der Sterbehilfe geht weiter  

***Die aktive (direkte) Sterbehilfe ist in Deutschland verboten, aber es gab im Bundestag (SPD, Grüne und FDP) immer wieder Bemühungen, sie zu legalisieren - unter dem Stichwort „AUTONOMIE AM LEBENSENDE“.  

***Im Oktober 2005 hat zum ersten Mal ein prominenter Politiker, der CDU-Justizsenator von Hamburg Kusch, für die Zulassung der aktiven Sterbehilfe plädiert. Diese Auffassung ist auf heftigen Widerstand gestoßen – sowohl aus der Politik als auch von der Hospizbewegung und den Kirchen.  

***Kusch begründet seine Auffassung konkret mit der seit über zehn Jahren geltenden  

***Abtreibungsregelung („rechtswidrig, aber straffrei“), die „fester und unangefochtener Bestandteil unserer Rechts- und Gesellschaftsordnung sei“. Danach habe die „Autonomie der Schwangeren drei Monate lang absoluten Vorrang vor dem Lebens-Recht des Embryos“. Das müsse doch um so mehr für das eigene Leben gelten. So wird aus einem Unrecht die Begründung für weiteres Unrecht! Und: Kusch übersieht die graduelle Verminderung des Unrechtsbewußtseins bei der Abtreibung – warum sollte das im Falle der Sterbehilfe anders verlaufen?  

***Der aus der Politik ausgeschiedene Kusch bietet seither öffentlich (mehrfach in 2008) seine aktive Mithilfe zur Selbsttötung an.  

***In einigen Nachbarländern ist die aktive Sterbehilfe unter bestimmten Auflagen erlaubt, z.B. in Belgien und Holland (seit dem 1. April 2002). In Luxemburg hat das Parlament in 2008 ein vergleichbares Gesetz beschlossen.  

***Im November 2005 wurde in England ein Gesetzentwurf eingebracht, der es „dem Arzt erlaubt, einen Erwachsenen, der aufgrund einer unheilbaren Krankheit unerträglich leiden muß und ausdrücklich um Beendigung der Qual und des Lebens bittet, zu töten“.  

***In Holland werden die Fälle der ärztlich assistierten Sterbehilfe dokumentiert; sie liegen jährlich bei 3.500, bei allerdings nur 1.800 gemeldeten Fällen.  

***Im Dezember 2004 hat eine holländische Ärzte-Kommission empfohlen, die Regeln über die aktive Sterbehilfe sogar dann anzuwenden, wenn der Todeswunsch allein dem „Lebensüberdruß“ entspringt. Regierung und Oberstes Gericht haben sich dagegen ausgesprochen. - Die holländische Euthanasie-Kontroll-Kommission hat zum 1. Mal (Mai 2005) den Fall eines 65-jährigen Alzheimer Patienten als rechtmäßig anerkannt. Sein Leiden sei aussichtslos und unerträglich gewesen. Das ist unter den behandelnden Ärzten umstritten. - Auch Demenz wird als Grund für die aktive Sterbehilfe anerkannt.  

***In Belgien können Ärzte „Euthanasie-Kits“ zum Preis von ca. € 60 in Apotheken erwerben (incl. Pentothal und Norcuron).  

***Argumentationshilfen gegen die Straffreiheit der aktiven Sterbehilfe:  

***- Auf den Schutz des Lebens zu verzichten – zu Beginn und am Ende -, bedeutet die Verleugnung des Rechtsstaates. Er gibt sich selbst auf.  

***- Die Legalisierung der aktiven Sterbehilfe ist ein Tabubruch. Damit wird eine Grenze überschritten, die der Mensch zum Schutz vor sich selbst aufgerichtet hat.  

***„Maak mij niet dood, Doktor“ (CREDO CARD): Wesentliches Argument: Gefahr des Mißbrauchs. Die legalisierte Sterbehilfe entwickelt eine Eigendynamik.  

***- Gegen die – aktive – Sterbehilfe spricht, daß der Patient zwar vordergründig seinen Sterbewunsch ausdrückt, bei behutsamen Nachfragen aber in erster Linie seine Ängste äußert: vor unerträglichen Schmerzen, dem Alleinsein, zu starker Belastung der Angehörigen, „unfinished business“ usw. Eine umfassende Sterbebegleitung (wie sie zunehmend in Hospizen und Palliativstationen angeboten wird,) kann erfahrungsgemäß diese Ängste aufnehmen und lindern.  

***- Gefahr der Projektion. „So kann die Tötung aus Mitleid zu einer Tötung aus verweigertem Mitleiden werden“ (P. Prof. Josef Schuster SJ, Sankt Georgen, Frankfurt)  

***Januar 2009 

# ****
 ©*** J. Beckermann, ***[Hilfe für Mutter und Kind e.V.](http://www.hmk-leben.de/) 
