<HTML><HEAD><TITLE>Patientenverf&uuml;gung</TITLE>
<META http-equiv=Content-Type content="text/html; charset=iso-8859-1"><LINK 
title=fonts href="kaltefleiter.css" type=text/css 
rel=stylesheet>
<META content="MSHTML 5.50.4134.600" name=GENERATOR></HEAD>
<BODY bgColor=#ffffff background="dstone1.gif" leftMargin=6 topMargin=6 marginwidth="6" marginheight="6">
<TABLE cellSpacing=0 cellPadding=6 width="100%" border=0>
  <TBODY>
  <TR>
    <TD vAlign=top align=left width=100> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif" height="14"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2" height="14"><b>Politik und Wirtschaft</b></td>
          <td background="boxtopright.gif" height="14"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#FFFFFF"> <?php include("logo.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#FFFFFF" class="V10"> <?php include("az.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </TD>
    <TD vAlign=top rowSpan=2>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
        <TBODY>
        <TR vAlign=top align=left>
          <TD width=8><IMG height=8 alt="" 
            src="boxtopleftcorner.gif" width=8></TD>
          <TD background=boxtop.gif><IMG height=8 alt="" 
            src="boxtop.gif" width=8></TD>
          <TD width=8><IMG height=8 alt="" 
            src="boxtoprightcorner.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxtopleft.gif><IMG height=8 alt="" 
            src="boxtopleft.gif" width=8></TD>
          <TD bgColor=#e2e2e2> 
            <H1><strong><font face="Arial, Helvetica, sans-serif">Patientenverf&uuml;gung</font></strong></H1>
          </TD>
          <TD background=boxtopright.gif><IMG height=8 
            alt="" src="boxtopright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=13 alt="" src="boxdividerleft.gif" 
            width=8></TD>
          <TD background=boxdivider.gif><IMG height=13 
            alt="" src="boxdivider.gif" width=8></TD>
          <TD><IMG height=13 alt="" 
            src="boxdividerright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxleft.gif height="124"><IMG height=8 alt="" 
            src="boxleft.gif" width=8></TD>
          <TD height="124" bgcolor="#FFFFFF" class=L12> 
            <P><font face="Arial, Helvetica, sans-serif"><strong>PATIENTENVERF&Uuml;GUNG:
                Hilfe oder Falle? </strong></font></P>
            <p><strong><font face="Arial, Helvetica, sans-serif">F&uuml;r eine
                aktive Sterbebegleitung </font></strong></p>
            <p><strong><font face="Arial, Helvetica, sans-serif">Ziel: Leben
                  bis zuletzt unterst&uuml;tzen und/aber am Sterben
              nicht hindern </font></strong></p>
            <p><strong><font face="Arial, Helvetica, sans-serif">F&uuml;r eine
                Kultur der Endlichkeit des Lebens </font></strong></p>
            <p><font face="Arial, Helvetica, sans-serif">Was ist der Tod? Only
                a preventable desease, also nur eine (weitere) zu vermeidende
                Krankheit? Oder das nat&uuml;rliche Ende - und damit
              Teil - des Lebens? Mehr als 50% der Befragten einer Studie bei
              uns (2006) halten es f&uuml;r realistisch, da&szlig; sie &uuml;ber
              80 Jahre alt werden. Aber: Ein langes Leben um jeden Preis wollen
              die wenigsten. </font></p>
            <p><font face="Arial, Helvetica, sans-serif">Die dankenswerten Fortschritte
                der Medizin &#8211; insbesondere
              zum Lebensende hin &#8211; haben ungeahnte (Heilungs-)Chancen er&ouml;ffnet,
              sie haben den Proze&szlig; des &#8222;Lebens vor dem Sterben&#8220; besser
              steuerbar gemacht, ihn sozusagen &#8222;entschleunigt&#8220;. Sie
              haben aber auch neue Probleme/Fragen aufgeworfen. Die zunehmende
              Bedeutung von Demenz ist nur ein Aspekt. Die Angst, der &#8222;Apparatemedizin&#8220; allein
              und hilflos ausgesetzt zu sein, ein anderer. Nicht zu leugnen ist
              auch die Sorge, man k&ouml;nne Anderen (gerade auch Verwandten)
              ungeb&uuml;hrlich zur Last fallen. </font></p>
            <p><font face="Arial, Helvetica, sans-serif">&#8222;Auch in Krankheit und Sterben ist die zentrale Richtschnur
              allen Handelns die unverf&uuml;gbare W&uuml;rde des betroffenen
              Menschen&#8220; (ZdK). Das schlie&szlig;t das Recht auf Selbstbestimmung
              ein, auch das Recht auf Selbstbeschr&auml;nkung, nicht aber das
              Recht zur Selbstt&ouml;tung. 80% der Befragten wollen selbst bestimmen,
              unter welchen Umst&auml;nden sie sterben (Ethik der gesteigerten
              Verantwortung). Der aktuelle oder der mutma&szlig;liche Wille des
              Patienten bindet &#8211; den Arzt und die Beh&ouml;rden. Dem Arzt
              steht kein Zwangsbehandlungs-, kein Letztentscheidungsrecht zu
              (gegen den Paternalismus der &#8222;G&ouml;tter in Wei&szlig;). </font></p>
            <p><font face="Arial, Helvetica, sans-serif">Was gilt, wenn der Patient
                sich nicht (mehr) zu &auml;u&szlig;ern
              vermag? Eine gr&ouml;&szlig;ere Klarheit/Sicherheit erhoffen sich
              nicht wenige durch die Patientenverf&uuml;gung (PV). Wegen der
              Komplexit&auml;t der zu beachtenden Fragen (ethisch, medizinisch,
              rechtlich) ist sie trotz mehrfacher Versuche bei uns* noch nicht
              gesetzlich geregelt. Der Kl&auml;rungsbedarf betrifft neben eher
              formalen Aspekten vor allem die Reichweite und die Verbindlichkeit &#8211; aber
              auch die Grundsatzfrage, ob eine gesetzliche Regelung &uuml;berhaupt
              sinnvoll/notwendig ist. </font></p>
            <p><font face="Arial, Helvetica, sans-serif">*In &Ouml;sterreich ist das Gesetz &uuml;ber die Patientenverf&uuml;gung
              verabschiedet worden. Es unterscheidet zwischen &#8222;verbindlicher&#8220; und
              (lediglich) &#8222;beachtlicher&#8220; Verf&uuml;gung. Die &#8222;verbindliche&#8220; PV
              mu&szlig; nach umfassender Beratung in schriftlicher Form unter
              Beachtung weiterer Formalit&auml;ten abgefa&szlig;t werden. </font></p>
            <p><font face="Arial, Helvetica, sans-serif">Wenn man den geringen
                Anteil der unterzeichneten Verf&uuml;gungen
              betrachtet (unter 10%), k&ouml;nnte man die &#8222;Notwendigkeit,
              eine Regelung zur St&auml;rkung der Verbindlichkeit von PVs treffen
              zu m&uuml;ssen&#8220; &#8211; wie von der Politik behauptet &#8211; anzweifeln.
              Liegt es nur an den noch bestehenden Unklarheiten? Oder versucht
              der Staat, angesichts seiner eigenen Unf&auml;higkeit, die Menschenw&uuml;rde
              des Sterbenden sicher zu stellen, die Verantwortung abzuschieben &#8211; auf
              die Kranken, die weitgehend Hilflosen, gar die Sterbenden? Was
              gilt, wenn im Extremfall der Patientenwille gegen das von Staat
              zu sch&uuml;tzende Leben steht? </font></p>
            <p><font face="Arial, Helvetica, sans-serif">Die noch immer andauernden
                Diskussionen im Bundestag und au&szlig;erhalb
              hat die gro&szlig;e Unsicherheit best&auml;tigt. &#8222;Eine rundum &uuml;berzeugende
              L&ouml;sung gibt es wohl nicht&#8220;, sagte der Pr&auml;sident
              des Bundestages, Dr. Lammert. </font></p>
            <p><font face="Arial, Helvetica, sans-serif">Im folgenden werden einige Kern-Fragen kurz besprochen: </font></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Bindungswirkung: </strong></font></p>
            <p><font face="Arial, Helvetica, sans-serif">Der BGH hat den grunds&auml;tzlichen Vorrang/Bindungswirkung des
              Patientenwillens - auch durch Verf&uuml;gung - best&auml;tigt.
              Das Vormundschaftsgericht darf nicht zum &#8222;Herrn &uuml;ber
              Leben und Tod&#8220; werden. Es wird nur eingeschaltet, um den
              eindeutigen Patientenwillen (etwa gegen&uuml;ber dem Pflegepersonal)
              durchzusetzen oder aber in F&auml;llen von Unsicherheit/Streit
              (&#8222;Konfliktmodell&#8220;). </font></p>
            <p><font face="Arial, Helvetica, sans-serif">Bei der Ber&uuml;cksichtigung des aktuellen/mutma&szlig;lichen
              Patientenwillens mu&szlig; immer der Gesichtspunkt des &#8222;Perspektivenwechsels&#8220; bedacht
              werden: d.h. Menschen denken um, im Leben und insbesondere angesichts
              bedrohlicher Krankheiten und des Sterbens. Auch Patienten z.B.
              im Wachkoma k&ouml;nnen sich (neu) &auml;u&szlig;ern. Schwierig
              sind F&auml;lle von LIS (Locked-in-Syndrom), bei dem der Patient
              infolge eines Stamm- und Kleinhirninfarktes (z.B. nach Schlaganfall)
              bei Bewu&szlig;tsein, aber doch vollst&auml;ndig gel&auml;hmt ist. </font></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Formalien: </strong></font></p>
            <p><font face="Arial, Helvetica, sans-serif">Eine Patientenverf&uuml;gung sollte grunds&auml;tzlich schriftlich
              abgefa&szlig;t werden. Sie ist aber auch m&uuml;ndlich erkl&auml;rbar &#8211; und
              ebenso widerrufbar. </font></p>
            <p><font face="Arial, Helvetica, sans-serif">Die formularm&auml;&szlig;ig vorgefertigten PVs sind gute Formulierungshilfen,
              sie sind aber oft so pauschal gehalten, da&szlig; sie f&uuml;r
              den konkreten Einzelfall nicht anwendbar sind. Es besteht Gefahr
              der &#8222;heimlichen Euthanasie&#8220;. Patientenverf&uuml;gungen
              m&uuml;&szlig;ten daher so konkret, situations-, d.h. krankheitsspezifisch
              wie m&ouml;glich sein. </font></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Organspende: </strong></font></p>
            <p><font face="Arial, Helvetica, sans-serif">Verf&uuml;gung und grunds&auml;tzlich vorhandene Organspendebereitschaft
              k&ouml;nnen sich (zeitlich) ausschlie&szlig;en. Was soll gelten? </font></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Reichweite: </strong></font></p>
            <p><font face="Arial, Helvetica, sans-serif">Die fr&uuml;here Enquetekommission &#8222;Ethik und Recht der
              Modernen Medizin&#8220; hat sich f&uuml;r eine Einschr&auml;nkung
              der Reichweite ausgesprochen; sie soll nur f&uuml;r Krankheiten
              gelten, &#8222;die zum Tode f&uuml;hren&#8220;, also als Letztverf&uuml;gung.
              Sie gilt daher (zun&auml;chst einmal) nicht f&uuml;r Demenzkranke
              und Wachkomapatienten. Aber: Man mu&szlig; darauf achten, da&szlig; der
              alte &auml;rztliche Paternalismus nicht durch einen neuen ethischen
              Paternalismus &#8211; Dogma des Lebenszwangs &#8211; abgel&ouml;st
              wird. </font></p>
            <p><font face="Arial, Helvetica, sans-serif">Frage: Was gilt f&uuml;r die F&auml;lle, in denen man ausschlie&szlig;en
              kann, da&szlig; das Bewu&szlig;tsein jemals wieder erlangt wird? </font></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Vollmachten: </strong></font></p>
            <p><font face="Arial, Helvetica, sans-serif">Es bietet sich an, zus&auml;tzlich eine Vorsorge- (f&uuml;r
                den Gesundheitsbereich) und eine Betreuungsvollmacht (sofern
                eine Betreuung
              notwendig ist) zu erteilen. </font></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>ZUSAMMENFASSUNG: </strong></font></p>
            <p><font face="Arial, Helvetica, sans-serif">Die Fragen des &#8222;Lebens vor dem Sterben&#8220; (&#8222;unerledigte
              Gesch&auml;fte&#8220;), incl. einer m&ouml;glichen Patientenverf&uuml;gung,
              sollten wenn m&ouml;glich in einem eingehenden, offenen Gespr&auml;ch
              mit Verwandten und &Auml;rzten er&ouml;rtert werden. </font></p>
            <p><font face="Arial, Helvetica, sans-serif">Da jeder PV ein Element
                des Nichtwissens innewohnt, sie damit auch von einer verst&auml;ndlichen &Auml;ngstlichkeit gepr&auml;gt
              ist, mu&szlig; man darauf achten, da&szlig; sie nicht lediglich
              Ausdruck des Mi&szlig;trauens gegen&uuml;ber den &Auml;rzten (und/oder
              den Angeh&ouml;rigen) ist. Gerade in Grenzsituationen Abw&auml;gungen
              zu treffen, geh&ouml;rt zu der t&auml;glich geforderten &#8222;&auml;rztlichen
              Kunst&#8220; - insbesondere im Hinblick auf die auch weiterhin
              zu erwartenden medizinischen Fortschritte. </font></p>
            <p><font face="Arial, Helvetica, sans-serif">In diesem Zusammenhang
                ist auch an das in den USA pr&auml;ferierte
              Konzept des &#8222;Advanced Care Planning&#8220;, des &#8222;vorausschauenden
              Versorgungsplans&#8220; zu denken. Beteiligt sind die Fachleute,
              aber auch die Angeh&ouml;rigen. Gemeinsam werden je nach Symptomen
              die erforderlichen Behandlungsm&ouml;glichkeiten festgehalten,
              incl. der Ern&auml;hrung. </font></p>
            <p><font face="Arial, Helvetica, sans-serif">Wegen der Unw&auml;gbarkeiten/Risiken der Patientenverf&uuml;gung
              ist daher zu &uuml;berlegen, auf das Basis des bestehenden Rechts
              eine eher &#8222;offene&#8220; Verf&uuml;gung anzustreben &#8211; verbunden
              mit der Vorsorgevollmacht an (zun&auml;chst) eine Vertrauensperson. </font></p>
            <p><font face="Arial, Helvetica, sans-serif">Dezember 2008</font></p>
            <P><font face="Arial, Helvetica, sans-serif" size="3"><br>
              &copy; </font><font face="Arial, Helvetica, sans-serif">J. Beckermann
              <a href="http://www.hmk-leben.de/">Hilfe f&uuml;r Mutter und Kind
              e.V.</a></font></P>
            </TD>
          <TD background=boxright.gif height="124"><IMG height=8 alt="" 
            src="boxright.gif" width=8></TD>
        </TR>
        <TR vAlign=top align=left>
          <TD><IMG height=8 alt="" src="boxbottomleft.gif" 
            width=8></TD>
          <TD background=boxbottom.gif><IMG height=8 alt="" 
            src="boxbottom.gif" width=8></TD>
          <TD><IMG height=8 alt="" src="boxbottomright.gif" 
            width=8></TD></TR></TBODY></TABLE></TD></TR>
  <TR>
    <TD vAlign=top align=left height="2">&nbsp; </TD>
</TR></TBODY></TABLE></BODY></HTML>
