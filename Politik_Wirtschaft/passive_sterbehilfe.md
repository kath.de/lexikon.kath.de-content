---
title: Passive Sterbehilfe
author: 
tags: 
created_at: 
images: []
---


# **Passive "Sterbehilfe"**
 - besser: Aktive Sterbebegleitung. ** 

***Grundidee: Jeder Mensch hat das Recht auf seinen natürlichen Tod  

***Grenze: Es gibt kein Recht zur Selbsttötung (Suizidverbot)  

***- Der Verzicht auf mögliche weiterführende medizinische Behandlung bzw. deren Abbruch ist erlaubt.  

***- Dieses Selbstbestimmungsrecht ist die unbestrittene Grundlage einer wirksamen Patientenverfügung.  

***Grundsätzlich versteht man unter passiver/indirekter „Sterbehilfe“ den Einsatz leidvermindernder Mittel bei Sterbenden/Todkranken - unter Inkaufnahme des Risikos der Beschleunigung des Todes. Dazu gehört auch der vom Arzt angeordnete Behandlungsverzicht/-abbruch, auch durch aktives Tun (z.B. Abschalten eines Gerätes). Ein spezieller Fall ist die im Rahmen der Palliativmedizin angewandte modulierbare „palliative“ Sedierung.  

***Frage: Rechtlich anders zu bewerten ist die mißglückte Operation mit Todesfolge.  

***Entscheidend ist bei jeder Maßnahme die Intention: das Ziel ist nicht der (frühere) Tod des Patienten, sondern durch Leidverminderung die Verbesserung der Lebensqualität des Sterbenden in der letzten Phase des „Lebens vor dem Lebensende“.  

***Deshalb handelt es sich nicht um „Sterbehilfe“, sondern um Hilfe „im“ Sterben – um aktive Sterbebegleitung.  

***- Da Ärzte diese therapeutischen Abwägungen regelmäßig treffen müssen, wehren sie sich (zu Recht) gegen den Begriff der „passiven/indirekten Sterbehilfe“.  

***Der Begriff der „Behandlungsbedürftigkeit“ (Kirchhof) könnte ein guter Maßstab für die Beurteilung der zu treffenden Entscheidung sein.  
  

***Es gibt Überlegungen, die strafrechtlichen Folgen der passiven/indirekten „Sterbehilfe“ zu klären: Dem § 216 StGB („Tötung auf Verlangen“) könnte ein dritter Absatz beigefügt werden:  

***„Nicht strafbar ist  

***1. die Anwendung einer medizinisch angezeigten leidmindernden Maßnahme, die das Leben als nicht beabsichtigte Nebenwirkung verkürzt (= indirekte SH)  

***2. das Unterlassen oder Beenden einer lebenserhaltenden medizinischen Maßnahme, wenn dies dem Willen des Patienten entspricht“ (= passive SH).  

***Dezember 2008   

# ****
 © ***J. Beckermann, ***[Hilfe für Mutter und Kind e.V.](http://www.hmk-leben.de/) 
