---
title: Abtreibungsregelung - rechtliche Bewertung
author: 
tags: 
created_at: 
images: []
---


# ****
 Der Paragraf 281 des Strafrechts und seine Umsetzung*****: 

# **§ 218 Abs. S. 1 StGB lautet:****
 ***Wer eine Schwangerschaft abbricht, wird mit Freiheitsstrafe bis zu drei Jahren oder mit Geldstrafe bestraft. 

# **§ 218 a Abs 1 StGB lautet:****
# **Der Tatbestand des § 218 ist nicht verwirklicht, wenn**
# **1. die Schwangere den Schwangerschaftsabbruch verlangt und dem Arzt durch eine Bescheinigung nach § 219 Abs. 2 S. 2 nachgewiesen hat, daß sie sich mindestens drei Tage vor dem Eingriff hat beraten lassen,**
 2. der Schwangerschaftsabbruch von einem Arzt vorgenommen wird und***

 3.	seit der Empfängnis nicht mehr als zwölf Wochen vergangen sind. 

# **Während die erstgenannte Norm noch klar erscheint, ist die Bewertung der zweiten (beratene Abtreibung) alles andere als das. Aus folgenden Gründen:**
# **• Gemeinhin wird gesagt, die Abtreibung nach Beratung sei „rechtswidrig, aber straflos“. **
 Die Aufrechterhaltung des Postulats „rechtswidrig“ ist in der Tat verfassungsrechtlich geboten. In seiner Entscheidung vom 28.5.1993 hat das Bundesverfassungsgericht erklärt, der Schwangerschaftsabbruch müsse „für die ganze Dauer der Schwangerschaft grundsätzlich als Unrecht angesehen und demgemäß rechtlich verboten sein“ (Leitsatz 4). Sollte sich im Rahmen der rechtlichen Bewertung herausstellen, daß die Rechtsordnung den beratenen Schwangerschaftsabbruch gar nicht als Unrecht ansieht, so dürfte dieser kaum mit unserem Grundgesetz vereinbar sein. 

# **• „Rechtswidrig, aber straflos“** **
 Das klingt nach einer nach wie vor bestehenden Mißbilligung dieses Verhaltens durch die Rechtsordnung. Tatsächlich aber suchen wir im Gesetzestext vergeblich nach dieser Mißbilligung. § 218 a Abs. 1 sagt vielmehr, der „Tatbestand“ des § 218 sei „nicht verwirklicht“. Nimmt man das wörtlich, so ist die Abtreibung nach § 218 a überhaupt keine Abtreibung im Sinne des Gesetzes. Eine solche Tat ist auch nicht rechtswidrig. Denn rechtswidirg kann nur eine Tat sein, die den Tatbestand eines Strafgesetzes verwirklicht (§ 11 Nr. 5 StGB). Wenn aber schon keine rechtswidrige Tat vorliegt, so kann eine solche auch nicht für „straflos“ erklärt werden, was außer in der Überschrift der Norm auch nicht geschieht 

***Tatsächlich mißbilligt unsere Rechtsordnung die „Abtreibung nach Beratung“ keineswegs. Der Gesetzgeber tut das Gegenteil. Er fördert diese Art der Abtreibung. Dazu 4 Argumente: 

# **• Wenngleich das Bundesverfassungsgericht eine Beratung „zugunsten des ungeborenen Lebens“ fordert (Leitsätze 11, 12 und 13), hat der Gesetzgeber im krassen Gegensatz dazu im**
 Schwangerschaftskonfliktgesetz festgelegt, die Beratung müsse “ergebnisoffen“ geführt werden.***

# *
•  Weiter tut unser Staat allerhand, um abtreibungswilligen Frauen die Abtreibung zu ermöglichen. Er sieht es als seine Aufgabe an, Abtreibungsmöglichkeiten zu schaffen und zwar flächendeckend und in einer Entfernung, „die von der Frau nicht die Abwesenheit über einen Tag hinaus verlangt“. Er sieht es zudem als seine Aufgabe an, Frauen die Abtreibung auch dann großzügig zu finanzieren, wenn sie nicht hilfebedürftig im Sinne etwa der Sozialhilfe sind. Die Abrechnung der Abtreibungskosten erfolgt nach § 3 des Gesetzes "zur Hilfe für Frauen bei Schwangerschaftsabbrüchen in besonderen Fällen" vom 21.8.1995 auf Antrag der Frau durch die gesetzlichen Krankenkassen und zwar auch dann, wenn die Frau gar nicht Mitglied einer solchen Kasse ist. Den Kassen werden diese Kosten später von den Bundesländern erstattet.***

# *
•  Die Beratung selbst muß keine wirkliche Beratung sein. Die Schwangere hat einen Anspruch auf Erteilung des die straflose Abtreibung ermöglichenden Beratungsscheines auch dann, wenn sie eine Beratungsstelle aufsucht und sich auf eine Darlegung ihrer Gründe und eine Beratung gar nicht einläßt.***

# *
•                              Noch 1993 hat das Bundesverfassungsgericht festgestellt, daß der an einer Abtreibung mitwirkende Arzt keinen ***[Honoraranspruch](arzthaftung_bei_abtreibung.php)              hat. Der auf Abtreibung gerichtete Vertrag des Arztes sei, so das Gericht, wegen der Rechtswidrigkeit der Abtreibung sittenwidrig und damit nichtig. In einer späteren umstrittenen Entscheidung hat das gleiche Gericht jedoch erklärt, die rechtswidrige Tötung des Kindes unterliege ausnahmsweise dem Schutz der Berufsfreiheit des Art. 12 GG. Denn „die Tätigkeit des Arztes ist notwendiger Bestandteil des gesetzlichen Schutzkonzeptes“. So kommt auch der Abtreibungsarzt zu seinem Honorar. Und wenn die Tätigkeit des Arztes – und das ist die Abtreibung – „notwendiger Bestandteil des gesetzlichen Schutzkonzeotes ist." 

# **Ergebnis: Die beratene Abtreibung kann nach geltender Rechtslage nicht als „rechtswidrig“ bezeichnet werden. Der Gesetzgeber hat das nirgendwo getan und sein Handeln spricht eher für das Gegenteil.**
 Die beratene Abtreibung ist aber auch nie ausdrücklich als „rechtmäßig“ bezeichnet worden. Sie wäre dann auch verfassungswidrig.***

 Bezeichnet man eine Handlung dann als „rechtswidrig“, wenn sie von der Rechtsordnung mißbilligt wird und als „rechtmäßig“ dann, wenn sie von der Rechtsordnung gebilligt wird, dann kann man aufgrund intensiver staatlicher Förderung der Abtreibung nur zu dem Ergebnis kommen: ***

 Die Rechtsordnung sieht die beratene Abtreibung tatsächlich als „rechtmäßig“ an. Damit billigt sie der abtreibungswilligen Frau mindestens de facto ein Recht auf Abtreibung zu, somit ein Recht auf Tötung menschlichen Lebens. Die derzeitige Regelung dürfte damit verfassungswidrig sein.***

 An sich wäre deshalb eine neue Entscheidung des Bundesverfassungsgerichts fällig. Dieses Gericht wird aber keine Chance zur Entscheidung erhalten. Denn: Alle Parteien des Deutschen Bundestages haben 1995 die derzeitige Abtreibungsregelung in seltener Einmütigkeit beschlossen. Alle Parteien wollen erklärtermaßen an dieser Regelung festhalten, unabhängig von ihrer rechtlichen Bewertung. Ein Normenkontrollverfahren vor dem Bundesverfassungsgericht aber kann nur von der Bundesregierung, einer Landeregierung oder einem Drittel der Mitglieder des Bundestages beantragt werden. All diese Institutionen werden von den Parteien beherrscht. 

# **Welche Konsequenzen das Festhalten an diesem Umgang mit dem Rechtsgut Leben langfristig haben wird, ist in Ansätzen schon jetzt zu sehen:**
 Das Recht prägt auch das Rechtsbewußtsein der Menschen. Schon heute, nur 10 Jahre nach Einführung der Beratungsregelung, wird Abtreibung in weiten Teilen der Bevölkerung und namentlich der Jugend als „erlaubt“ angesehen. Die Zahl der Abtreibungen steigt Jahr für Jahr. Hierzu hat sicher auch das allzu lange Festhalten der deutschen Bischöfe am Beratungsschein und damit der Beteiligung am staatlichen Abtreibungssystem beigetragen. Das Zentralkommitee der deutschen Katholiken hält heute noch daran fest.***

 Weite Teile der Bevölkerung befürworten eine „Liberalisierung“ des Lebensschutzes am Ende des Lebens. Mit welcher Argumentation wollen wir der Forderung nach aktiver Sterbehilfe (Tötung leidenden Lebens) und der nach der Zulässigkeit der Tötung auf Verlangen entgegentreten, wenn wir uns mit der Tötung gegebenenfalls auch gesunden menschlichen Lebens längst abgefunden haben ? Der Tabubruch liegt bereits hinter uns. 

# ****
 © ******[Dr. Konrad Schneller](http://www.hmk-leben.de/)            
