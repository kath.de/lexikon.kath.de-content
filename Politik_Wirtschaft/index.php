<HTML><HEAD><TITLE>Wirtschaftsethik und Moral&ouml;konomik</TITLE>
<META http-equiv=Content-Type content="text/html; charset=iso-8859-1"><LINK
title=fonts href="kaltefleiter.css" type=text/css
rel=stylesheet>
<META content="MSHTML 5.50.4134.600" name=GENERATOR></HEAD>
<BODY bgColor=#ffffff background="dstone1.gif" leftMargin=6 topMargin=6 marginwidth="6" marginheight="6">
<TABLE cellSpacing=0 cellPadding=6 width="100%" border=0>
  <TBODY>
  <TR>
    <TD vAlign=top align=left width=100 height="63">
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif" height="14"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2" height="14"><b>Politik und Wirtschaft</b></td>
          <td background="boxtopright.gif" height="14"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#FFFFFF"> <?php include("logo.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left">
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#FFFFFF" class="V10"> <?php include("az.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left">
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </TD>
    <TD vAlign=top rowSpan=2>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
        <TBODY>
        <TR vAlign=top align=left>
          <TD width=8><IMG height=8 alt=""
            src="boxtopleftcorner.gif" width=8></TD>
          <TD background=boxtop.gif><IMG height=8 alt=""
            src="boxtop.gif" width=8></TD>
          <TD width=8><IMG height=8 alt=""
            src="boxtoprightcorner.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxtopleft.gif><IMG height=8 alt=""
            src="boxtopleft.gif" width=8></TD>
          <TD bgColor=#e2e2e2>
            <H1>Wirtschaftsethik und Moralökonomik</H1>
          </TD>
          <TD background=boxtopright.gif><IMG height=8
            alt="" src="boxtopright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=13 alt="" src="boxdividerleft.gif"
            width=8></TD>
          <TD background=boxdivider.gif><IMG height=13
            alt="" src="boxdivider.gif" width=8></TD>
          <TD><IMG height=13 alt=""
            src="boxdividerright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxleft.gif height="327"><IMG height=8 alt=""
            src="boxleft.gif" width=8></TD>
          <TD height="327" bgcolor="#FFFFFF" class=L12>
            <P>&nbsp;</P>
            <P><font face="Arial, Helvetica, sans-serif" size="3">
            <a href="http://www.neufeld-verlag.de/" target="_blank"><img src="http://www.kath.de/lexikon/b_hinweise/neufeld.jpg" width="286" height="238" border="0" align="right"></a><p><p></p></p><b>Moral</b>
              - ein Thema für die Ökonomik? Diese von vielen lange Zeit zweifelnd
              vorgebrachte Frage wird inzwischen eindeutig affirmativ beantwortet.</font></P>
            <P><font face="Arial, Helvetica, sans-serif" size="3">Die veränderte
              Einschätzung ist wohl nicht zufällig im zeitlichen Zusammenhang
              mit neueren theoretischen Ansätzen der Ökonomik zu sehen, die als
              Neue Institutionenökonomik oder Konstitutionenökonomik Buchananscher
              Prägung in die Debatte Einzug gehalten haben: Durch ihre konzeptionelle
              Unterscheidung zwischen individuellen Handlungen und den ihnen zugrunde
              liegenden formellen wie informellen Regeln wurde die methodische
              Voraussetzung für eine ertragreiche ökonomische Auseinandersetzung
              mit dem "Phänomen Moral" geschaffen.</font></P>

            <P><font face="Arial, Helvetica, sans-serif" size="3">Im Zuge dieser
              Entwicklung hat die ökonomische Analyse moralischer Normen - oder
              kurz: die <b>Moralökonomik</b> - neue oder erweiterte Einsichten
              in die Rolle solcher Normen in einer modernen Marktwirtschaft beisteuern
              können. Wie ein Blick in die jüngste Literatur zeigt, werden nicht
              wenige der zum Teil neuen, zum Teil wiederentdeckten alten Erkenntnisse
              über die Entstehung und (Nicht-) Befolgung solcher Normen, über
              ihre Ursachen und Folgen zunehmend auch von Theologen und Moralphilosophen
              anerkannt und (aus kritischer Distanz) in ihrer eigenen Forschungsarbeit
              berücksichtigt. Außerdem werden vor dem Hintergrund der gewonnenen
              Erkenntnisse auch normative Fragen mit der institutionenökonomischen
              Methode in wohlverstandener Weise neu angegangen.</font></P>
          <img SRC="http://www.kath.de/lexikon/Politik_Wirtschaft/wirtschaftsethik_6.jpg" alt="Internetökonomie&Ethik" name="Internetökonomie" hspace="10" vspace="10" border="0"  align="right">

                    <P><font face="Arial, Helvetica, sans-serif" size="3">Auch können
              im Zuge des neu begonnenen Diskurses zwischen Ethik und Ökonomik,
              zwischen Ökonomen und Theologen bzw. <b>Moralphilosophen</b> sowie
              Vertretern anderer Disziplinen auch einige bisher als gesichert
              geltende Hypothesen wirtschaftswissenschaftlicher Theorien in einem
              anderen Licht erscheinen als zuvor: Lernen im interdisziplinären
              Diskurs ist im Idealfall eben ein auf Gegenseitigkeit beruhender
              Prozess. Ohne Zweifel sind nicht wenige der bisher vorliegenden
              Beiträge von Ökonomen, insbesondere in Begründungsfragen, heftig
              umstritten; die Frage einer Vernunft- oder Vorteilsbegründung der
              Moral oder die, inwiefern denn nun in einer marktwirtschaftlich
              verfassten Ordnung die Moral systematisch in den institutionellen
              Rahmenbedingungen zu verorten sei, mögen hier nur als prägnante
              Beispiele dienen. </font></P>
            <P><font face="Arial, Helvetica, sans-serif" size="3">Grundsätzlich
              unbestritten ist gleichwohl die Fruchtbarkeit des neueren ökonomischen
              Beitrages: Der Ansatz hat sich binnen weniger Jahre im interdisziplinären
              Diskurs fest etabliert. Die Tatsache, dass um zahlreiche konzeptionelle
              Fragen, um konkrete Anwendungen und mögliche Weiterentwicklungen
              ebenso noch gerungen wird wie um angemessene Begriffe, darf dabei
              nicht überraschen. Vielmehr fügt sich diese Beobachtung in das Bild
              eines viel versprechenden neuen Forschungsprogramms.</font></P>
            <P><font face="Arial, Helvetica, sans-serif" size="3">In mehreren
              interdisziplinären Fachtagungen hat die katholisch-soziale Akademie
              <a href="http://www.franz-hitze-haus.de">Franz Hitze Haus</a> in
              Kooperation mit der wirtschaftswissenschaftlichen Fakultät der Universität
              Münster konkrete wirtschaftsethische bzw. moralökonomische Fragestellungen
              aufgegriffen, die in folgenden Tagungsbänden dokumentiert sind:</font></P>
            <P><font face="Arial, Helvetica, sans-serif" size="3"><b>Aufderheide,
              Detlef; Dabrowski, Martin</b> (Hg./1997): Wirtschaftsethik und Moralökonomik.
              Normen, soziale Ordnung und der Beitrag der Ökonomik, Duncker &
              Humblot, Berlin </font></P>
            <P><font face="Arial, Helvetica, sans-serif" size="3"><b>Aufderheide,
              Detlef; Dabrowski, Martin</b> (Hg./2000): Internationaler Wettbewerb
              - Nationale Sozialpolitik? Wirtschaftsethische und moralökonomische
              Perspektiven der Globalisierung, Duncker & Humblot, Berlin. </font></P>
            <P><font face="Arial, Helvetica, sans-serif" size="3"><b>Aufderheide,
              Detlef; Dabrowski, Martin</b> (Hg./2002): Gesundheit - Ethik - Ökonomik.
              Wirtschaftsethische und moralökonomische Perspektiven des Gesundheitswesens,
              Duncker & Humblot, Berlin </font></P>
            <P><font face="Arial, Helvetica, sans-serif" size="3"><b>Aufderheide,
              Detlef; Dabrowski, Martin</b> (Hg./2005): Corporate Governance und
              Korruption. Wirtschaftsethische und moralökonomische Perspektiven
              der Bestechung und ihrer Bekämpfung, Duncker & Humblot, Berlin (im
              Erscheinen) <br>
              </font><font size="3" face="Arial, Helvetica, sans-serif"><br>
              </font><font face="Arial, Helvetica, sans-serif" size="3"><br>
              &copy; Akademie Franz Hitze Haus. </font></P>
            </TD>
          <TD background=boxright.gif height="327"><IMG height=8 alt=""
            src="boxright.gif" width=8></TD>
        </TR>
        <TR vAlign=top align=left>
          <TD><IMG height=8 alt="" src="boxbottomleft.gif"
            width=8></TD>
          <TD background=boxbottom.gif><IMG height=8 alt=""
            src="boxbottom.gif" width=8></TD>
          <TD><IMG height=8 alt="" src="boxbottomright.gif"
            width=8></TD></TR></TBODY></TABLE></TD></TR>
  <TR>
    <TD vAlign=top align=left height="2">&nbsp; </TD>
</TR></TBODY></TABLE></BODY></HTML>