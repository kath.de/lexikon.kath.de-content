<HTML><HEAD><TITLE>Aktive Sterbehilfe</TITLE>
<META http-equiv=Content-Type content="text/html; charset=iso-8859-1"><LINK 
title=fonts href="kaltefleiter.css" type=text/css 
rel=stylesheet>
<META content="MSHTML 5.50.4134.600" name=GENERATOR></HEAD>
<BODY bgColor=#ffffff background="dstone1.gif" leftMargin=6 topMargin=6 marginwidth="6" marginheight="6">
<TABLE cellSpacing=0 cellPadding=6 width="100%" border=0>
  <TBODY>
  <TR>
    <TD vAlign=top align=left width=100> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif" height="14"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2" height="14"><b>Politik und Wirtschaft</b></td>
          <td background="boxtopright.gif" height="14"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#FFFFFF"> <?php include("logo.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#FFFFFF" class="V10"> <?php include("az.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </TD>
    <TD vAlign=top rowSpan=2>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
        <TBODY>
        <TR vAlign=top>
          <TD width=8 align="left"><IMG height=8 alt="" 
            src="boxtopleftcorner.gif" width=8></TD>
          <TD align="left" background=boxtop.gif><IMG height=8 alt="" 
            src="boxtop.gif" width=8></TD>
          <TD width=8 align="left"><IMG height=8 alt="" 
            src="boxtoprightcorner.gif" width=8></TD>
        </TR>
        <TR vAlign=top>
          <TD align="left" background=boxtopleft.gif><IMG height=8 alt="" 
            src="boxtopleft.gif" width=8></TD>
          <TD align="left" bgColor=#e2e2e2> 
            <H1><font face="Arial, Helvetica, sans-serif">Aktive
              Sterbehilfe</font></H1>
          </TD>
          <TD align="left" background=boxtopright.gif><IMG height=8 
            alt="" src="boxtopright.gif" width=8></TD>
        </TR>
        <TR vAlign=top>
          <TD align="left"><IMG height=13 alt="" src="boxdividerleft.gif" 
            width=8></TD>
          <TD align="left" background=boxdivider.gif><IMG height=13 
            alt="" src="boxdivider.gif" width=8></TD>
          <TD align="left"><IMG height=13 alt="" 
            src="boxdividerright.gif" width=8></TD>
        </TR>
        <TR vAlign=top>
          <TD height="124" align="left" background=boxleft.gif><IMG height=8 alt="" 
            src="boxleft.gif" width=8></TD>
          <TD height="124" bgcolor="#FFFFFF" class=L12> 
            <P><font face="Arial, Helvetica, sans-serif"><strong>&#8222;Von der W&uuml;rde
                - Dignitas - des Sterbenden&#8220; </strong></font></P>
            <p><font face="Arial, Helvetica, sans-serif">&#8222;Die Forderung, schwer kranke Menschen auf ihren Wunsch
              hin und Menschen, die einer klaren Willens&auml;u&szlig;erung nicht
              mehr f&auml;hig sind, auch ohne ihren Willen zu t&ouml;ten, wird
              nach einer Latenzzeit von etwa f&uuml;nfzig Jahren nun wieder energisch
              vorgetragen&#8220; (Robert Spaemann, Oktober 2005). </font></p>
            <p><font face="Arial, Helvetica, sans-serif">In seiner Weihnachtsansprache
                2008 hat Henri I., der Gro&szlig;herzog
              von Luxemburg, die Unterzeichnung eines &#8222;Sterbehilfegesetzes
              abgelehnt. Wie in Belgien und Holland sollen &Auml;rzte damit das
              Recht erhalten, bei unheilbar Kranken in der letzten Phase, aber
              auch bei drohendem Pers&ouml;nlichkeitsverlust durch Demenz die
              T&ouml;tung auf Verlangen durchzuf&uuml;hren; auch der assistierte
              Suizid soll erm&ouml;glicht werden. </font></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Lebensschutz als umfassendes Thema: Abgrenzungskriterien </strong></font></p>
            <p><font face="Arial, Helvetica, sans-serif">Fragen zur &#8222;Sterbehilfe&#8220; sind Teil der Gesamtdebatte
              zum Thema Lebensschutz; sie m&uuml;ssen daher m&ouml;glichst umfassend,
              differenziert und in sich stimmig diskutiert werden. Unklarheiten
              (u.a. durch &#8211; bewu&szlig;t? - unpr&auml;zise Gesetzesformulierungen)
              zu Beginn des Lebens (Abtreibungsregelung: &#8222;rechtswidrig,
              aber straffrei&#8220;) f&uuml;hren zu zwar &#8222;logisch&#8220; scheinenden,
              aber dennoch falschen Forderungen auf Legalisierung der Sterbehilfe
              zum Lebensende. (vergl. Kusch). </font></p>
            <p><font face="Arial, Helvetica, sans-serif">Wenn es als Teil der
                Selbstbestimmungsrechts ein Recht zur Selbstt&ouml;tung
              g&auml;be (s.u.) und dar&uuml;ber hinaus einen Anspruch auf die
              Mithilfe &#8222;Berufener&#8220; &#8211; z.B. eines Arztes -, m&uuml;&szlig;te
              dieser dann nicht bei T&auml;tigwerden straffrei gestellt werden?
              (&#8222;unzumutbare Grauzone der Illegalit&auml;t&#8220;). T&ouml;tung
              als &auml;rztliche Leistung? Und weiter: M&uuml;&szlig;te der Arzt
              evtl. sogar zur Mithilfe verpflichtet werden, d.h. gibt es einen
              Anspruch gegen den Arzt? Mit welchen Folgen? Schadensersatz? Wann? </font></p>
            <p><font face="Arial, Helvetica, sans-serif">Es gibt viele Abgrenzungsprobleme.
                Grunds&auml;tzlich: Worauf
              beruht das Verbot zur Selbstt&ouml;tung? Was ist T&ouml;tung auf
              Verlangen? Abgrenzung zum assistierten Suizid?Darf aktive Sterbehilfe
              nur gg&uuml;. Todkranken oder auch bei Alzheimerpatienten gew&auml;hrt
              werden? Stichwort: &#8222;Sozialvertr&auml;gliches Fr&uuml;hversterben&#8220;.
              (vergl. dazu auch die Orientierungshilfe der EKD, November 2008;
              HK 1/2009, S. 9). </font></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>Erl&auml;uternde
                Vorbemerkungen: </strong></font></p>
            <p><font face="Arial, Helvetica, sans-serif">(1) Gem&auml;&szlig; einer im November 2008 ver&ouml;ffentlichten
              Umfrage bei &Auml;rzten bejahen 35% der Befragten die M&ouml;glichkeit
              des &auml;rztlich assistierten Suizids; etwa jeder sechste spricht
              sich sogar f&uuml;r die (Legalisierung der) aktiven Sterbehilfe
              aus. (vergl. HK 1/2009, S. 9) </font></p>
            <p><font face="Arial, Helvetica, sans-serif">(2) Mit dem Hinweis
                auf den Gleichheitsgrundsatz wird europaweit f&uuml;r die Zulassung der aktiven Sterbehilfe (&uuml;brigens auch
              der Abtreibung) geworben. Das Thema &#8222;aktive Sterbehilfe&#8220; ist
              auf Antrag von Belgien, Holland und der Schweiz im Oktober 2003
              auf die Tagesordnung der Parlamentarischen Versammlung des Europarates
              gesetzt worden - unter dem bewu&szlig;t harmlos scheinenden Titel: &#8222;Unterst&uuml;tzung
              von Patienten am Lebensende&#8220;. </font></p>
            <p><font face="Arial, Helvetica, sans-serif">Das Ziel ist die europaweite
                Zulassung der &#8222;Euthanasie&#8220; &#8211; basierend
              auf Analysen der holl&auml;ndischen Erfahrungen. Dies ist ein Versuch,
              in Europa geltendes Recht &#8211; was dann auch in den einzelnen
              L&auml;ndern gelten w&uuml;rde - neu zu definieren und zu ver&auml;ndern.
              Im April 2005 hat der Europarat gegen die Empfehlung gestimmt. </font></p>
            <p><font face="Arial, Helvetica, sans-serif">(3) Mit der &#8222;CREDO CARD&#8220; &#8211; &#8222;Maak mij niet
              dood, Doktor&#8220; - wenden sich in Holland zunehmend Patienten
              gegen lebensverk&uuml;rzende Ma&szlig;nahmen, die gegen ihren Willen
              angeordnet werden. Aus Angst vor dem ungewollten Tod gehen &auml;ltere
              Menschen in Holland in Altenheime ins benachbarte Deutschland. </font></p>
            <p><font face="Arial, Helvetica, sans-serif">(4) Die Schweizer Gesellschaft
                DIGNITAS hat am 26. September 2005 in Hannover eine deutsche
                Tochtergesellschaft gegr&uuml;ndet, um
              vor Ort f&uuml;r ihre Dienstleistungen (Beihilfe zur Selbstt&ouml;tung)
              werben zu k&ouml;nnen. Sollte das deutsche Recht &#8211; f&uuml;r
              eine Straffreiheit dieser bezahlten Leistungen - nicht &#8220;ausreichen&#8220;,
              will man durch Pr&auml;zedenzf&auml;lle auf eine entsprechende &#8222;Rechtsfortbildung&#8220; hinwirken. </font></p>
            <p><font face="Arial, Helvetica, sans-serif"><strong>BEGRIFFE/DEFINITIONEN: </strong></font></p>
            <p><font face="Arial, Helvetica, sans-serif">Es ist zu beachten,
                da&szlig; manche Begriffe unterschiedlich
              genutzt/verstanden werden &#8211; je nachdem ob es sich um &Auml;rzte
              oder um Juristen handelt. </font></p>
            <p><font face="Arial, Helvetica, sans-serif">Suizidverbot: In der
                Diskussion wird die bisher als klassisch angesehene Berufung
                des Selbstt&ouml;tungsverbots auf die Souver&auml;nit&auml;t
              des Sch&ouml;pfers allein als nicht mehr ausreichend angesehen.
              Auch nicht der irreversible Charakter der Suizidhandlung. </font></p>
            <p><font face="Arial, Helvetica, sans-serif">Gibt es eine Begr&uuml;ndung, die ansatzweise auch von Nichtgl&auml;ubigen
              nachvollzogen und akzeptiert werden kann? Zunehmend wird auf die
              unbedingte Solidarit&auml;t gg&uuml;. den Mitmenschen verwiesen.
              Die Einr&auml;umung des Rechts zur Selbstt&ouml;tung h&auml;tte
              notwendig eine entsolidarisierende Wirkung. Aufgrund des dadurch
              entstehenden Rechtfertigungsdrucks k&ouml;nnte aus dem Recht leicht
              eine Pflicht werden. </font></p>
            <p><font face="Arial, Helvetica, sans-serif">Aktive (direkte) Sterbehilfe/Euthanasie: in Deutschland strafbar. </font></p>
            <p><font face="Arial, Helvetica, sans-serif">&#8220;Unter Euthanasie im eigentlichen Sinne versteht man eine
              Handlung oder Unterlassung, die ihrer Natur nach und aus bewu&szlig;ter
              Absicht den Tod herbeif&uuml;hrt, um auf diese Weise jeden Schmerz
              zu beenden&#8220; (Evangelium vitae, 9. April 2004) - Das Ziel
              ist der Tod des Patienten - im Tausch gegen Leidbeendigung. </font></p>
            <p><font face="Arial, Helvetica, sans-serif">Der Klarheit wegen ist
                zu &uuml;berlegen, den Begriff &#8222;Euthanasie&#8220; nur
              auf die F&auml;lle zu beschr&auml;nken, bei denen wie in der NS-Zeit
              die T&ouml;tung ganzer Gruppen von Menschen (z.B. der Geisteskranken)
              bewu&szlig;t und systematisch angestrebt wird. </font></p>
            <p><font face="Arial, Helvetica, sans-serif">- Juristisch vergleichbar
                (und strafbar) ist der Fall der &#8222;T&ouml;tung
              auf Verlagen&#8220; gem. &sect; 216 StGB d.h. die Vernichtung fremden
              menschlichen Lebens mit Einwilligung (auf Wunsch) des Betroffenen. &sect; 216
              StGB markiert die Grenze des Selbstbestimmungsrechts. </font></p>
            <p><font face="Arial, Helvetica, sans-serif">- Juristisch vergleichbar (und strafbar) ist der assistierte Suizid. </font></p>
            <p><font face="Arial, Helvetica, sans-serif">- Juristisch vergleichbar
                (und strafbar) ist die &#8222;terminale&#8220; Sedierung,
              wenn der Tod des Patienten das erkl&auml;rte bzw. implizierte Ziel
              der &#8222;Behandlung&#8220; ist. </font></p>
            <p><font face="Arial, Helvetica, sans-serif">- Nicht voll vergleichbar
                ist das Angebot von DIGNITAS: es handelt sich (juristisch) &#8222;nur&#8220; um Beihilfe zur Selbstt&ouml;tung
              durch die Beschaffung schnell und schmerzlos wirkender Barbiturate.
              Das totbringende Gift mu&szlig; der Patient selber einnehmen, zumeist
              allein, d.h. ohne Anwesenheit eines Dritten. Es handelt sich nicht
              um &#8222;begleitetes Sterben&#8220;, sondern um &#8222;Begleitung
              zum Tod&#8220;. </font></p>
            <p><font face="Arial, Helvetica, sans-serif">In Deutschland ist diese
                Beihilfe grunds&auml;tzlich straffrei;
              in der Schweiz ist sie nur unter bestimmten Gesichtspunkten strafbar &#8211; bei
              Vorliegen niedriger Beweggr&uuml;nde. </font></p>
            <p><font face="Arial, Helvetica, sans-serif">Bedenken: Die organisierte
                (irgendwie doch) bezahlte Vermittlung von Suizid-Beihilfe (wie
                bei DIGNITAS) ver&auml;ndert die Einstellung
              zum Leben. Am Anfang stehen gef&uuml;hlsbetonte und daher nicht
              leicht abzuwehrende Begriffe wie &#8222;Mitleid&#8220; und &#8222;Wegr&auml;umen
              unn&ouml;tiger Hindernisse&#8220; (medizinische Unkenntnis). </font></p>
            <p><font face="Arial, Helvetica, sans-serif">Zumindest, wenn diese
                Dienstleistungen gewerblich (und aus Verdienstgr&uuml;nden)
              angeboten werden, sollte ein Verbot angestrebt werden (&sect; 217
              StGB). </font></p>
            <p><font face="Arial, Helvetica, sans-serif">...die Diskussion um die Legalisierung der Sterbehilfe geht weiter </font></p>
            <p><font face="Arial, Helvetica, sans-serif">Die aktive (direkte)
                Sterbehilfe ist in Deutschland verboten, aber es gab im Bundestag
                (SPD, Gr&uuml;ne und FDP) immer wieder
              Bem&uuml;hungen, sie zu legalisieren - unter dem Stichwort &#8222;AUTONOMIE
              AM LEBENSENDE&#8220;. </font></p>
            <p><font face="Arial, Helvetica, sans-serif">Im Oktober 2005 hat
                zum ersten Mal ein prominenter Politiker, der CDU-Justizsenator
                von Hamburg Kusch, f&uuml;r die Zulassung
              der aktiven Sterbehilfe pl&auml;diert. Diese Auffassung ist auf
              heftigen Widerstand gesto&szlig;en &#8211; sowohl aus der Politik
              als auch von der Hospizbewegung und den Kirchen. </font></p>
            <p><font face="Arial, Helvetica, sans-serif">Kusch begr&uuml;ndet seine Auffassung konkret mit der seit &uuml;ber
              zehn Jahren geltenden </font></p>
            <p><font face="Arial, Helvetica, sans-serif">Abtreibungsregelung
                (&#8222;rechtswidrig, aber straffrei&#8220;),
              die &#8222;fester und unangefochtener Bestandteil unserer Rechts-
              und Gesellschaftsordnung sei&#8220;. Danach habe die &#8222;Autonomie
              der Schwangeren drei Monate lang absoluten Vorrang vor dem Lebens-Recht
              des Embryos&#8220;. Das m&uuml;sse doch um so mehr f&uuml;r das
              eigene Leben gelten. So wird aus einem Unrecht die Begr&uuml;ndung
              f&uuml;r weiteres Unrecht! Und: Kusch &uuml;bersieht die graduelle
              Verminderung des Unrechtsbewu&szlig;tseins bei der Abtreibung &#8211; warum
              sollte das im Falle der Sterbehilfe anders verlaufen? </font></p>
            <p><font face="Arial, Helvetica, sans-serif">Der aus der Politik
                ausgeschiedene Kusch bietet seither &ouml;ffentlich
              (mehrfach in 2008) seine aktive Mithilfe zur Selbstt&ouml;tung
              an. </font></p>
            <p><font face="Arial, Helvetica, sans-serif">In einigen Nachbarl&auml;ndern
                ist die aktive Sterbehilfe unter bestimmten Auflagen erlaubt,
                z.B. in Belgien und Holland (seit
              dem 1. April 2002). In Luxemburg hat das Parlament in 2008 ein
              vergleichbares Gesetz beschlossen. </font></p>
            <p><font face="Arial, Helvetica, sans-serif">Im November 2005 wurde
                in England ein Gesetzentwurf eingebracht, der es &#8222;dem Arzt erlaubt, einen Erwachsenen, der aufgrund
              einer unheilbaren Krankheit unertr&auml;glich leiden mu&szlig; und
              ausdr&uuml;cklich um Beendigung der Qual und des Lebens bittet,
              zu t&ouml;ten&#8220;. </font></p>
            <p><font face="Arial, Helvetica, sans-serif">In Holland werden die
                F&auml;lle der &auml;rztlich assistierten
              Sterbehilfe dokumentiert; sie liegen j&auml;hrlich bei 3.500, bei
              allerdings nur 1.800 gemeldeten F&auml;llen. </font></p>
            <p><font face="Arial, Helvetica, sans-serif">Im Dezember 2004 hat
                eine holl&auml;ndische &Auml;rzte-Kommission
              empfohlen, die Regeln &uuml;ber die aktive Sterbehilfe sogar dann
              anzuwenden, wenn der Todeswunsch allein dem &#8222;Lebens&uuml;berdru&szlig;&#8220; entspringt.
              Regierung und Oberstes Gericht haben sich dagegen ausgesprochen.
              - Die holl&auml;ndische Euthanasie-Kontroll-Kommission hat zum
              1. Mal (Mai 2005) den Fall eines 65-j&auml;hrigen Alzheimer Patienten
              als rechtm&auml;&szlig;ig anerkannt. Sein Leiden sei aussichtslos
              und unertr&auml;glich gewesen. Das ist unter den behandelnden &Auml;rzten
              umstritten. - Auch Demenz wird als Grund f&uuml;r die aktive Sterbehilfe
              anerkannt. </font></p>
            <p><font face="Arial, Helvetica, sans-serif">In Belgien k&ouml;nnen &Auml;rzte &#8222;Euthanasie-Kits&#8220; zum
              Preis von ca. &euro; 60 in Apotheken erwerben (incl. Pentothal
              und Norcuron). </font></p>
            <p><font face="Arial, Helvetica, sans-serif">Argumentationshilfen gegen die Straffreiheit der aktiven Sterbehilfe: </font></p>
            <p><font face="Arial, Helvetica, sans-serif">- Auf den Schutz des
                Lebens zu verzichten &#8211; zu Beginn und
              am Ende -, bedeutet die Verleugnung des Rechtsstaates. Er gibt
              sich selbst auf. </font></p>
            <p><font face="Arial, Helvetica, sans-serif">- Die Legalisierung
                der aktiven Sterbehilfe ist ein Tabubruch. Damit wird eine Grenze &uuml;berschritten,
                die der Mensch zum Schutz vor sich selbst aufgerichtet hat. </font></p>
            <p><font face="Arial, Helvetica, sans-serif">&#8222;Maak mij niet dood, Doktor&#8220; (CREDO CARD): Wesentliches
              Argument: Gefahr des Mi&szlig;brauchs. Die legalisierte Sterbehilfe
              entwickelt eine Eigendynamik. </font></p>
            <p><font face="Arial, Helvetica, sans-serif">- Gegen die &#8211; aktive &#8211; Sterbehilfe spricht, da&szlig; der
              Patient zwar vordergr&uuml;ndig seinen Sterbewunsch ausdr&uuml;ckt,
              bei behutsamen Nachfragen aber in erster Linie seine &Auml;ngste &auml;u&szlig;ert:
              vor unertr&auml;glichen Schmerzen, dem Alleinsein, zu starker Belastung
              der Angeh&ouml;rigen, &#8222;unfinished business&#8220; usw. Eine
              umfassende Sterbebegleitung (wie sie zunehmend in Hospizen und
              Palliativstationen angeboten wird,) kann erfahrungsgem&auml;&szlig; diese &Auml;ngste
              aufnehmen und lindern. </font></p>
            <p><font face="Arial, Helvetica, sans-serif">- Gefahr der Projektion. &#8222;So kann die T&ouml;tung aus Mitleid
              zu einer T&ouml;tung aus verweigertem Mitleiden werden&#8220; (P.
              Prof. Josef Schuster SJ, Sankt Georgen, Frankfurt) </font></p>
            <p><font face="Arial, Helvetica, sans-serif">Januar 2009</font></p>
            <P><font face="Arial, Helvetica, sans-serif" size="3"><br>
              &copy;</font><font face="Arial, Helvetica, sans-serif"> J. Beckermann,
              <a href="http://www.hmk-leben.de/">Hilfe f&uuml;r Mutter und Kind
              e.V.</a></font></P>
            </TD>
          <TD height="124" align="left" background=boxright.gif><IMG height=8 alt="" 
            src="boxright.gif" width=8></TD>
        </TR>
        <TR vAlign=top>
          <TD align="left"><IMG height=8 alt="" src="boxbottomleft.gif" 
            width=8></TD>
          <TD align="left" background=boxbottom.gif><IMG height=8 alt="" 
            src="boxbottom.gif" width=8></TD>
          <TD align="left"><IMG height=8 alt="" src="boxbottomright.gif" 
            width=8></TD>
        </TR></TBODY></TABLE></TD></TR>
  <TR>
    <TD vAlign=top align=left height="2">&nbsp; </TD>
</TR></TBODY></TABLE></BODY></HTML>
