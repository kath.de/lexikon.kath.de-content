---
title: Elementarerziehung
author: 
tags: 
created_at: 
images: []
---


# ****
 Die Elementarerziehung **in Kindergärten und Kindertagesstätten  hat durch die internationalen Bildungsstudien wie PISA als eigenständiges  Bildungssystem erfreulicherweise neue Aufmerksamkeit erfahren. Bislang  wurden Kindergärten eher als Betreuungseinrichtungen mit einem spielerischen  Schwerpunkt gesehen. Der gesetzliche Anspruch auf einen Kindergartenplatz  sollte vor allem dazu dienen, Mütter zu entlasten und die Vereinbarkeit  von Familie und Beruf für Frauen zu verbessern. Die Vergleichs-Studien  und die aktuelle Diskussion haben zweierlei deutlich werden lassen:  schon sehr junge Kinder sind äußerst lernfähig und lernbegierig, und  zum anderen: Bildungschancen und -karrieren werden schon vor der Schule  gebahnt.   

***Im bildungspolitischen  Diskurs zeigt sich eine neue Wertschätzung und Gewichtung der vorschulischen  Erziehung**. Damit verbunden ist eine stärkere Betonung des Bildungsauftrages  der Kindertagesstätten, an dessen konkreter Formulierung und Ausgestaltung  als Bildungsvereinbarungen die Bundesländer und die Träger der vorschulischen  Erziehung z.Zt. arbeiten. Konsequenterweise ergeben sich dabei aber  auch Diskussionen um eine Neu- und Umverteilung der finanziellen  Mittel wie der personellen Ausstattung und Diskussionen um eine  Aufwertung und Neugestaltung der Erzieherinnenausbildung. 

***Die Neubewertung  des Elementarbereichs und die veränderten Ansprüche in der vorschulischen  Erziehung machen es zum jetzigen Zeitpunkt unbedingt erforderlich,  den Erzieherinnen in der Praxis Fortbildungen anzubieten, die sie  in die Lage versetzen, den Anforderungen und Aufgaben gerecht zu  werden. Die katholische ***[Akademie  Franz Hitze Haus](http://www.franz-hitze-haus.de) bietet deshalb Veranstaltungen und Fortbildungen  an, die die Professionalisierung der Erzieherinnen in verschiedenen  Aufgabenbereichen verbessert und stärkt. 

# **Schwerpunkte  des Angebots sind zum einen Seminare zur differenzierten Einschätzung  und Förderung der Kinder vom 3. Lebensjahr bis zum Übergang in die  Grundschule, zur Sprachdiagnostik und Sprachförderung und zu Entwicklungsverzögerungen  und Verhaltensauffälligkeiten. Im Rahmen der Konzeptdebatte und  Neuausrichtung der Einrichtungen stellen wir in einem mehrteiligen  Kurs Theorie und Praxis der Reggio-Pädagogik vor. In diesem Kontext  bieten wir schon länger Kurse an, wie jungen Kindern Anregungen  zu naturkundlichem Entdecken und Forschen vermittelt werden können.  Ein weiterer Schwerpunkt ist ein Zertifikatskurs als Fachkraft in  der Begabtenförderung, der mit dem Internationalen Centrum für Begabungsforschung  der Universität Münster entwickelt wurde. Neben diesen meist mehrteiligen  Kursen, die der Qualifizierung dienen, geht es uns auch um sensorische  Integration, um Wahrnehmungsschulung, um die Balance von Konzentration  und Entspannung, um Entwicklung kreativer Potenzen bei den Kindern  und um die Computernutzung bei Projekten im Kindergarten. Der Stärkung  der eigenen Persönlichkeit und der kommunikativen Kompetenzen dienen  Veranstaltungen zur Gesprächsführung mit Eltern, zum Umgang mit  aggressiven Kindern und zum Abbau von Stresserfahrungen. Die Veränderungen  in der professionellen Elementarerziehung und -bildung werden natürlich  unser fortlaufendes Angebot beeinflussen und verändern. *****
# *
 Weitere Informationen:***

# ******
# **[Akademie Franz Hitze Haus](http://www.franz-hitze-haus.de)**
# ****
 © Akademie Franz Hitze Haus.  
