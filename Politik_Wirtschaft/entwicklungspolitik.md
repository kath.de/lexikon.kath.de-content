---
title: Entwicklungspolitik
author: 
tags: 
created_at: 
images: []
---


# ****
 Entwicklungspolitik **wird im allgemeinen verstanden als die Gesamtheit  aller Maßnahmen, die von verschiedenen Organisationen ergriffen  werden, um der Bevölkerung in den Staaten der Dritten Welt optimale  Entwicklungsmöglichkeiten zu schaffen und zu sichern. Grobziele  der Entwicklungspolitik** mit ihrem Leitziel "Internationale  Gerechtigkeit" sind: Politische Stabilität, Arbeit, Wirtschaftliche  Leistungsfähigkeit, Partizipation, Ökologische Tragfähigkeit und  Eigenständigkeit.  

***Das ***[Franz  Hitze Haus, ](http://www.franz-hitze-haus.de)die Akademie des Bistums Münster, thematisiert und  reflektiert im ***[Fachbereich  "Politik und Zeitgeschichte, Internationale Gerechtigkeit"](http://www.franz-hitze-haus.de/index.php?myELEMENT=34113)  die Entwicklungspolitik** hinsichtlich ihrer Ziele, Ergebnisse  und Perspektiven.  

***Vom zeitlichen  Umfang her reichen die Bildungsveranstaltungen im Fachbereich Politik  und Zeitgeschichte, Internationale Gerechtigkeit vom Abendforum  über den Studientag bis zur Wochenendtagung. Aus dem Grundauftrag  der Akademie, eine Verbindung zwischen dem gesellschaftlichen Teilsystem  Religion und den anderen Systemen, wie beispielsweise Wirtschaft,  Wissenschaft und Politik, herzustellen, ergibt sich, dass zum Dialog  in gleicher Weise Fachleute wie auch allgemein politisch Interessierte  eingeladen sind. Großen Wert wird darauf gelegt, Veranstaltungen  gemeinsam mit Kooperationspartner aus dem kirchlichen und außerkirchlichen  Spektrum anzubieten; entwicklungspolitischen Nichtregierungsorganisationen  kommt dabei eine herausragende Bedeutung zu. 

# **Einige Beispiele  aus dem Programm des Franz Hitze Hauses: "Wie erfolgreich ist die  Entwicklungspolitik?", "Entwicklungshilfe konkret: Passo Fundo",  "Jahrestagung Entwicklungspolitik", "Die deutsche Entwicklungspolitik  im Zeitalter der Globalisierung", "Entwicklungszusammenarbeit als  Beruf" und "Wirkt die Hilfe wirklich? Bilanz der Entwicklungspolitik  der großen Geber USA, Japan und EU".**
# ****
 Weitere Informationen:***

# ******
# **[Fachbereich  "Politik und Zeitgeschichte, Internationale Gerechtigkeit"  der Akademie Franz Hitze Haus](http://www.franz-hitze-haus.de/index.php?myELEMENT=34113)**
# ****
 © Akademie Franz Hitze Haus.  
