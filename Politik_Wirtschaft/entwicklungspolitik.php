<HTML><HEAD><TITLE>Entwicklungspolitik</TITLE>
<META http-equiv=Content-Type content="text/html; charset=iso-8859-1"><LINK 
title=fonts href="kaltefleiter.css" type=text/css 
rel=stylesheet>
<META content="MSHTML 5.50.4134.600" name=GENERATOR></HEAD>
<BODY bgColor=#ffffff background="dstone1.gif" leftMargin=6 topMargin=6 marginwidth="6" marginheight="6">
<TABLE cellSpacing=0 cellPadding=6 width="100%" border=0>
  <TBODY>
  <TR>
    <TD vAlign=top align=left width=100 height="36"> 
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif" height="14"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2" height="14"><b>Politik und Wirtschaft</b></td>
          <td background="boxtopright.gif" height="14"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#FFFFFF"> <?php include("logo.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
      <br>
      <table width="216" border="0" cellpadding="0" cellspacing="0">
        <tr valign="top" align="left"> 
          <td width="8"><img src="boxtopleftcorner.gif" width="8" height="8" alt=""></td>
          <td width="200" background="boxtop.gif"><img src="boxtop.gif" alt="" width="8" height="8"></td>
          <td width="8"><img src="boxtoprightcorner.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxtopleft.gif"><img src="boxtopleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#E2E2E2"><strong>Begriff anklicken</strong></td>
          <td background="boxtopright.gif"><img src="boxtopright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxdividerleft.gif" width="8" height="13" alt=""></td>
          <td background="boxdivider.gif"><img src="boxdivider.gif" alt="" width="8" height="13"></td>
          <td><img src="boxdividerright.gif" width="8" height="13" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td background="boxleft.gif"><img src="boxleft.gif" width="8" height="8" alt=""></td>
          <td bgcolor="#FFFFFF" class="V10"> <?php include("az.html"); ?> </td>
          <td background="boxright.gif"><img src="boxright.gif" width="8" height="8" alt=""></td>
        </tr>
        <tr valign="top" align="left"> 
          <td><img src="boxbottomleft.gif" width="8" height="8" alt=""></td>
          <td background="boxbottom.gif"><img src="boxbottom.gif" width="8" height="8" alt=""></td>
          <td><img src="boxbottomright.gif" width="8" height="8" alt=""></td>
        </tr>
      </table>
    </TD>
    <TD vAlign=top rowSpan=2>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
        <TBODY>
        <TR vAlign=top align=left>
          <TD width=8><IMG height=8 alt="" 
            src="boxtopleftcorner.gif" width=8></TD>
          <TD background=boxtop.gif><IMG height=8 alt="" 
            src="boxtop.gif" width=8></TD>
          <TD width=8><IMG height=8 alt="" 
            src="boxtoprightcorner.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxtopleft.gif><IMG height=8 alt="" 
            src="boxtopleft.gif" width=8></TD>
          <TD bgColor=#e2e2e2> 
            <H1>Entwicklungspolitik</H1>
          </TD>
          <TD background=boxtopright.gif><IMG height=8 
            alt="" src="boxtopright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD><IMG height=13 alt="" src="boxdividerleft.gif" 
            width=8></TD>
          <TD background=boxdivider.gif><IMG height=13 
            alt="" src="boxdivider.gif" width=8></TD>
          <TD><IMG height=13 alt="" 
            src="boxdividerright.gif" width=8></TD></TR>
        <TR vAlign=top align=left>
          <TD background=boxleft.gif height="491"><IMG height=8 alt="" 
            src="boxleft.gif" width=8></TD>
          <TD height="491" bgcolor="#FFFFFF" class=L12> 
            <p><font face="Arial, Helvetica, sans-serif" size="3"><b><br>
              Entwicklungspolitik </b>wird im allgemeinen verstanden als die Gesamtheit 
              aller Maßnahmen, die von verschiedenen Organisationen ergriffen 
              werden, um der Bevölkerung in den Staaten der Dritten Welt optimale 
              Entwicklungsmöglichkeiten zu schaffen und zu sichern. Grobziele 
              der <b>Entwicklungspolitik</b> mit ihrem Leitziel "Internationale 
              Gerechtigkeit" sind: Politische Stabilität, Arbeit, Wirtschaftliche 
              Leistungsfähigkeit, Partizipation, Ökologische Tragfähigkeit und 
              Eigenständigkeit. </font></p>
            <p><font face="Arial, Helvetica, sans-serif" size="3">Das <a href="http://www.franz-hitze-haus.de">Franz 
              Hitze Haus, </a>die Akademie des Bistums Münster, thematisiert und 
              reflektiert im <a href="http://www.franz-hitze-haus.de/index.php?myELEMENT=34113">Fachbereich 
              &quot;Politik und Zeitgeschichte, Internationale Gerechtigkeit&quot;</a> 
              die <b>Entwicklungspolitik</b> hinsichtlich ihrer Ziele, Ergebnisse 
              und Perspektiven. </font></p>
            <p><font face="Arial, Helvetica, sans-serif" size="3">Vom zeitlichen 
              Umfang her reichen die Bildungsveranstaltungen im Fachbereich Politik 
              und Zeitgeschichte, Internationale Gerechtigkeit vom Abendforum 
              über den Studientag bis zur Wochenendtagung. Aus dem Grundauftrag 
              der Akademie, eine Verbindung zwischen dem gesellschaftlichen Teilsystem 
              Religion und den anderen Systemen, wie beispielsweise Wirtschaft, 
              Wissenschaft und Politik, herzustellen, ergibt sich, dass zum Dialog 
              in gleicher Weise Fachleute wie auch allgemein politisch Interessierte 
              eingeladen sind. Großen Wert wird darauf gelegt, Veranstaltungen 
              gemeinsam mit Kooperationspartner aus dem kirchlichen und außerkirchlichen 
              Spektrum anzubieten; entwicklungspolitischen Nichtregierungsorganisationen 
              kommt dabei eine herausragende Bedeutung zu.</font></p>
            <p><font face="Arial, Helvetica, sans-serif" size="3">Einige Beispiele 
              aus dem Programm des Franz Hitze Hauses: "Wie erfolgreich ist die 
              Entwicklungspolitik?", "Entwicklungshilfe konkret: Passo Fundo", 
              "Jahrestagung Entwicklungspolitik", "Die deutsche Entwicklungspolitik 
              im Zeitalter der Globalisierung", "Entwicklungszusammenarbeit als 
              Beruf" und "Wirkt die Hilfe wirklich? Bilanz der Entwicklungspolitik 
              der großen Geber USA, Japan und EU".<br>
              </font><font size="3" face="Arial, Helvetica, sans-serif"><br>
              <b>Weitere Informationen:<br>
              </b></font><font size="3" face="Arial, Helvetica, sans-serif"><br>
              <a href="http://www.franz-hitze-haus.de/index.php?myELEMENT=34113">Fachbereich 
              &quot;Politik und Zeitgeschichte, Internationale Gerechtigkeit&quot; 
              der Akademie Franz Hitze Haus</a><br>
              </font><font face="Arial, Helvetica, sans-serif" size="3"><br>
              &copy; Akademie Franz Hitze Haus. </font></p>
          </TD>
          <TD background=boxright.gif height="491"><IMG height=8 alt="" 
            src="boxright.gif" width=8></TD>
        </TR>
        <TR vAlign=top align=left>
          <TD><IMG height=8 alt="" src="boxbottomleft.gif" 
            width=8></TD>
          <TD background=boxbottom.gif><IMG height=8 alt="" 
            src="boxbottom.gif" width=8></TD>
          <TD><IMG height=8 alt="" src="boxbottomright.gif" 
            width=8></TD></TR></TBODY></TABLE></TD></TR>
  <TR>
    <TD vAlign=top align=left height="2">&nbsp; </TD>
</TR></TBODY></TABLE></BODY></HTML>
