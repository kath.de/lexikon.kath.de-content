---
title: Eine-Welt-Arbeit
author: 
tags: 
created_at: 
images: ["http://www.kath.de/lexikon/Politik_Wirtschaft/eine_welt_arbeit.jpg"]
---


# *
# *

# **			  Eine-Welt-Arbeit** ist die Verbindung von Entwicklungspolitik und EntwicklungspÃ¤dagogik, also von entwicklungspolitischem Handeln und entwicklungspolitischer Bildung. Das Spektrum der TrÃ¤ger ist breit - es reicht von den Ã¶rtlichen Aktionsgruppen Ã¼ber die mit Hauptberuflichen versehen Zentren und Koordinationsstellen hin bis zu den groÃen Nichtregierungsorganisationen.**
# *
 ***Eine-Welt-Arbeit **ist seit vilen Jahren ein Schwerpunkt im Angebot der Akademie Franz Hitze Haus, MÃ¼nster.  

***Das ***[Franz Hitze Haus ](http://www.franz-hitze-haus.de) ,die Akademie des Bistums MÃ¼nster, hat einen eigenen ***[Fachbereich Politik und Zeitgeschichte, Internationale Gerechtigkeit](http://www.franz-hitze-haus.de/index.php?myELEMENT=34113). Dort geht es unter anderem darum, die Eine-Welt-Arbeit** in den Gruppen, VerbÃ¤nden und Organisationen zu reflektieren und ihr neue Impulse zu geben. "Globales Lernen" erweist sich dabei als tragfÃ¤higer aktueller Leitbegriff der entwicklungspolitischen Bildung, denn "Globales Lernen" wird verstanden als die pÃ¤dagogische Reaktion auf die im Theorem Globalisierung beschriebene Entwicklung zur Weltgesellschaft. 

***Ein markantes Beispiel aus dem Angebot der Akademie Franz Hitze Haus sind die Jahrestagungen Entwicklungspolitik, die in Kooperation mit der Arbeitsgemeinschaft Eine-Welt-Gruppen** im Bistum MÃ¼nster und in der Evangelischen Kirche von Westfalen veranstaltet werden. Aktive aus den zumeist kirchlichen Eine-Welt-Gruppen** kommen hier zusammen, um sich auszutauschen, um sich fÃ¼rs AlltagsgeschÃ¤ft weiter qualifizieren zu lassen und um politische Perspektiven zu diskutieren. Ein anderes Beispiel sind die Multiplikatorenschulungen, in denen Hauptberufliche und Ehrenamtliche aus dem ganzen Bundesgebiet ihre Kompetenzen erweitern kÃ¶nnen. 

# **Vom zeitlichen Umfang her reichen die Bildungsveranstaltungen im Fachbereich Politik und Zeitgeschichte, Internationale Gerechtigkeit vom Abendforum Ã¼ber den Studientag bis zur Wochenendtagung. Aus dem Grundauftrag der Akademie, eine Verbindung zwischen dem gesellschaftlichen Teilsystem Religion und den anderen Systemen, wie beispielsweise Wirtschaft, Wissenschaft und Politik, herzustellen, ergibt sich, dass zum Dialog in gleicher Weise Fachleute wie auch allgemein politisch Interessierte eingeladen sind. GroÃen Wert wird darauf gelegt, Veranstaltungen gemeinsam mit Kooperationspartner aus dem kirchlichen und auÃerkirchlichen Spektrum anzubieten; entwicklungspolitischen Nichtregierungsorganisationen kommt dabei eine herausragende Bedeutung zu.**
# *
 Weitere Informationen:***

# ******
# **[Fachbereich "Politik und Zeitgeschichte, Internationale Gerechtigkeit" der Akademie Franz Hitze Haus](http://www.franz-hitze-haus.de/index.php?myELEMENT=34113)**
# ****
 © Akademie Franz Hitze Haus. Bild: © Mikael Damkier - Fotolia.com 
